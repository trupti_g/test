import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from 'environments/environment';
import { AppModule } from 'app/app.module';
// import { KSSwiperModule } from 'angular2-swiper';
// import { MaterialModule } from '@angular/material';
// import { KSSwiperModule } from 'angular2-swiper';

import { BrowserXhr } from '@angular/http';
import { CustExtBrowserXhr } from "app/shared-services/cust-ext-browser-xhr";
// import { provide } from '@angular/core';

if (environment.production) {
  enableProdMode();
  window.console.log = function () { };
}


/* platformBrowserDynamic :
    1. It contains the client side code that processes templates (bindings, components, ...) and
    reflective dependency injection.
    2. Uses Just-in-Time compiler and make's application compile on client-side.
*/
platformBrowserDynamic().bootstrapModule(AppModule
  // , {providers: [provide(BrowserXhr, { useClass: CustExtBrowserXhr })]   }
);

// platformBrowserDynamic()
//   .bootstrapModule(AppModule)
//   .catch(err => console.log(err));
