/*  Author         : Pratik Gawand
    Description    : Reward Gallery Routing module
                    
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RewardsGalleryHomeComponent2 } from "./rewards-gallery-home.component";

import { RewardsGalleryCategoriesComponent } from "./rewards-gallery-categories.component";

import { RewardsGalleryProductDetailsComponent } from "./rewards-gallery-product-details.component";

// import { ProductCatalogDetailsComponent } from "../user/product.catalog.detail";

export const rewardGalleryRoutes: Routes = [
    {
        path: '',
        children: [
 

            //Rewards Gallery components
            {
                path: 'categories',
                component: RewardsGalleryCategoriesComponent
            },
            {
                path: 'categories:catId',
                component: RewardsGalleryCategoriesComponent
            },
            {
                path: 'product-details',
                component: RewardsGalleryProductDetailsComponent
            },
            // {
            //     path: 'productCatalogDetails',
            //     component: ProductCatalogDetailsComponent
            // },


            //RewardGalleryHome components
            {
                path: '',
                redirectTo: 'categories',
                pathMatch: 'full'
            }
        ]
    }
];



@NgModule({
    imports: [RouterModule.forChild(rewardGalleryRoutes)],
    exports: [RouterModule],

})
export class RewardsGalleryRoutingModule {

}
