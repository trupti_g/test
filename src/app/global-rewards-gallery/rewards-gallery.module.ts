/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { RewardsGalleryHomeComponent2 } from "./rewards-gallery-home.component";
import { RewardsGalleryProductDetailsComponent } from "./rewards-gallery-product-details.component";
import { RewardsGalleryCategoriesComponent } from "./rewards-gallery-categories.component";
// import { ProductCatalogDetailsComponent } from "../user/product.catalog.detail";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FooterModule } from '../footer/footer.module';
// import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';

import { RewardsGalleryRoutingModule } from "./rewards-gallery.routing.module";
import { GlobalRewardGalleryService } from "./rewards-gallery.service";
import { MaterialModule } from '@angular/material';
// import { KSSwiperModule } from 'angular2-swiper';
// import { AppModule } from "../app.module";
import { SharedModule } from "../shared-components/shared.module";
// import { SwiperModule } from 'angular2-useful-swiper'; //or for angular-cli the path will be ../../node_modules/angular2-useful-swiper 






@NgModule({
	declarations: [RewardsGalleryHomeComponent2, RewardsGalleryCategoriesComponent, RewardsGalleryProductDetailsComponent,
		// ProductCatalogDetailsComponent
	],

	imports: [CommonModule,
		ReactiveFormsModule,
		RewardsGalleryRoutingModule,
		HttpModule,
		MaterialModule.forRoot(),
		// KSSwiperModule,
		// SwiperModule,
		FooterModule,
		FormsModule,
		SharedModule
	],

	exports: [],
	providers: [GlobalRewardGalleryService]
})
export class GlobalRewardsGalleryModule {
}