/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery
	Date Created	: 	03 April 2017
	Date Modified	: 	19 April 2017
*/

import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "./rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from '../home/home.service';
import { CartService } from "../shared-services/cart.service";


@Component({
	selector: 'product-category',
	templateUrl: './rewards-gallery-categories.component.html',
	styleUrls: ['./rewards-gallery.component.css'],


})



export class RewardsGalleryCategoriesComponent implements OnInit {

	private isEligibleForRedemption: boolean;
	private filterForm: FormGroup; //Filter  form group
	public showBrand: boolean = true;
	public noProductFound = false;
	public uncheckAll = false;
	private index: number = 0;
	public cartCount: number = 0;
	public category: string;
	public subscription: Subscription;
	public categoryName: string;
	public searchProduct: string;
	public brandArr: any = [];
	public productArr: any = [];
	private productQuantity: any = [];
	public isProductAdded: boolean[] = [];
	public checkGlobalRewardGallery: boolean;
	public programInfo: any;
	public programId: string;
	public allProduct: any = [];
	private cart: any = [];
	// public allSpecificProduct: any = [];
	public pointsSlabs: any = [];
	public requestObj: any = {

	};
	public allBrands: any;
	public allCategoryAndProduct: any;
	private showLoader: boolean = false;
	public slabs: any = {};
	public isSlab: boolean;
	public isCategory: boolean;
	public isSpecific: boolean;



	public categorySubcategoryArray: any[] = [];
	public brandsArray: any[] = [];
	public slabsArray: any[] = [];
	public productsArray: any[] = [];

	public productCount: number;
	public filterText: string = "";
	public allowStarPointsConversion: boolean = false;

	/************ 	KEYS FO FILTERS 	********************************* */
	public brandIdArray: any[] = [];
	public categoryId: string = undefined;
	public subCategoryId: string = undefined;
	public search: string = undefined;
	public sort: string = "";
	/******************** 	END OF KEYS FOR FILTERS 	***************************** */


	public isCategoryHover: boolean;
	public roverElementColor: any;
	public clientColor: any;
	public selectedIndex: any;
	public selectedSubIndex: any;
	public selectedBrandIndex: any;
	public showBrandColor: boolean;

	public programUser: any = {};
	public bEInfo: any = {};
	public isBrandsToRefresh: boolean = true;
	public isdeleteBrand: boolean = false;


	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		public cartService: CartService,

		private productDetailsService: ProductDetailsService,
		private rewardsGalleryService: GlobalRewardGalleryService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private homeService: HomeService,
	) {
		this.cartService.setFields("GlobalRG");

		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
		});


		this.programInfo = configService.getloggedInProgramInfo();
		this.programUser = this.configService.getloggedInProgramUser();
		this.bEInfo = this.configService.getloggedInBEInfo();


		this.programId = this.programInfo.programId;

		if (this.programUser.programRoleInfo.eligibleForRedemption) this.isEligibleForRedemption = false;
		else this.isEligibleForRedemption = true;
	}

	ngOnInit() {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Reward Gallery Categories");
				ga('send', 'pageview');
			}
		});
		console.log('GLOABL IS CALLED');
		this.filterForm = this.formBuilder.group({
			"view": [""],
			"search": [],
			"category": ["All Categories"]
		});
		this.activatedRoute.queryParams
			.subscribe(params => {
				var catId = params['catId'];
				var catName = params['catName'];
				var subCatId = params['subCatId'];
				var subCatName = params['subCatName'];
				var product = params['searchProduct'];

				// --> Name must match wanted paramter
				console.log("PARAMS catId", catId);
				console.log("PARAMS catName", catName);
				console.log("PARAMS subCatId", subCatId);
				console.log("PARAMS subCatName", subCatName);
				console.log("PARAMS product", product);

				if (catId != undefined) {
					this.categoryId = catId;
					console.log("setting the categoryId", this.categoryId);
				}
				if (subCatId != undefined) {
					this.subCategoryId = subCatId;
					console.log("setting the subCategoryId", this.subCategoryId);
				}
				if (catName != undefined && subCatName == undefined) {
					this.filterText = catName;
				}
				if (subCatName != undefined) {
					this.filterText = subCatName;
				}
				if (product != undefined) {
					this.search = product;
					console.log("setting the searchProduct", this.search);
				}

			});
		this.getProductFormParams();
		console.log("exiting the getparam method");
		this.getAllCategoryAndSubCategory();
		if (this.categoryId) {
			this.categorySubcategoryArray = this.configService.getCategorySubCategories();
			console.log("finding the category Name", this.categoryId);
			console.log("category array for it!!!!!!!!!", this.categorySubcategoryArray);
			for (let i = 0; i < this.categorySubcategoryArray.length; i++) {
				if (this.categoryId == this.categorySubcategoryArray[i].categoryId) {
					this.categoryName = this.categorySubcategoryArray[i].categoryName;
					console.log("categoryName set to", this.categoryName);
					this.filterText = this.categoryName;
					if (this.subCategoryId != undefined) {
						for (let j = 0; j < this.categorySubcategoryArray[i].SubCategories.length; j++) {
							if (this.categorySubcategoryArray[i].SubCategories[j].subCategoryId == this.subCategoryId) {
								this.filterText = this.categorySubcategoryArray[i].SubCategories[j].subCategoryName;
							}
						}
					}

				}
			}
		}
		this.getBrandMaster();

		this.programInfo = this.configService.getloggedInProgramInfo();

		if (this.programInfo.allowStarPointsConversion && !this.programInfo.useGlobalRewardGallery) this.allowStarPointsConversion = true;
		else this.allowStarPointsConversion = false;
		this.productArr = this.productDetailsService.getProduct();
		this.cartCount = this.productArr.length;
		this.clientColor = this.configService.getThemeColor();

	}

	ngOnChanges() {

	}

	goToGlobalRG() {
		this.searchProduct = undefined;
		this.router.navigate(['./home/globalrewardsgallery/categories'],
			{ queryParams: { searchProduct: this.filterForm.value.search } });
	}
	goToProgramRG() {
		this.searchProduct = undefined;
		this.router.navigate(['./home/rewardsgallery/categories'],
			{ queryParams: { searchProduct: this.filterForm.value.search } });
	}


	ngOnDestroy() {
	}



	mouseLeaveCategory() {
		this.isCategoryHover = false;
	}
	mouseOverCategory() {
		this.isCategoryHover = true;
		this.roverElementColor = this.configService.getThemeColor();
	}

	mouseLeaveCategoryIndex() {
		this.selectedIndex = null;
	}
	mouseOverCategoryIndex(index) {

		console.log('index', index);
		this.selectedIndex = index;
		console.log('index', this.selectedIndex);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}


	mouseLeaveSubCategoryIndex() {
		this.selectedSubIndex = null;
	}
	mouseOverSubCategoryIndex(index) {

		console.log('index', index);
		this.selectedSubIndex = index;
		console.log('index', this.selectedIndex);
		// console.log('this.isCategoryHover OVER', this.isCategoryHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}
	mouseLeaveBrandIndex() {
		this.selectedBrandIndex = null;
		this.showBrandColor = true;
	}
	mouseOverBrandIndex(index) {
		// this.isCategoryHover = true;
		this.showBrandColor = false;
		console.log('index', index);
		this.selectedBrandIndex = index;
		console.log('index', this.selectedBrandIndex);
		// console.log('this.isCategoryHover OVER', this.isCategoryHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}

	// search() {
	// 	this.googleAnalyticsEventsService.emitEvent("Reward Gallery Categories", this.filterForm.value.search, "searchProducts");

	// 	this.rewardsGalleryService.searchQuery = this.filterForm.value.search;
	// 	this.rewardsGalleryService.searchCategory = this.filterForm.value.category;
	// 	this.requestObj.searchProduct = this.filterForm.value.search;
	// 	this.requestObj.categoryId = this.filterForm.value.category;
	// 	delete this.requestObj.brandIdArray;
	// 	var cat = this.filterForm.value.category;
	// 	if (cat === "All Categories") {
	// 		delete this.requestObj.categoryId;
	// 	}

	// 	this.getAllFilteredProducts(this.requestObj);
	// 	console.log("FF", this.filterForm.value.category);
	// }

	// filterBrand(brandId, brandName, index) {

	// 	this.googleAnalyticsEventsService.emitEvent("Reward Gallery Categories", brandName, "viewBrand");

	// 	console.log(brandId, index);
	// 	var brandArr = this.brandArr;
	// 	console.log(brandArr.length);
	// 	this.allBrands[index].isSelected = true;
	// 	if (brandArr.includes(brandId)) {
	// 		// this.brandArr.pop(brandId);
	// 		delete brandArr[index];
	// 		var index = brandArr.indexOf(brandId);
	// 		brandArr.splice(index);
	// 	}
	// 	else {
	// 		this.brandArr.push(brandId);
	// 	}
	// 	console.log("Brands", this.brandArr);
	// 	this.requestObj.brandIdArray = this.brandArr;
	// 	console.log("BL", this.brandArr.length);
	// 	if (this.brandArr.length === 0) {
	// 		delete this.requestObj.brandIdArray;
	// 		console.log("RQ", this.requestObj);

	// 		this.getAllProducts();
	// 	} else {
	// 		console.log("RQ", this.requestObj);


	// 		this.getAllFilteredProducts(this.requestObj);
	// 	}



	// }




	toggleBrand() {
		this.showBrand = !this.showBrand;
	}

	goToProductDetail(product) {
		console.log("ON CLIck", product);
		// this.rewardsGalleryService.setSpecificProduct(product);
		this.configService.setRewardGalleryProduct(product);
		this.router.navigate(['./home/globalrewardsgallery/product-details']);
	}



	setProduct(products) {
		console.log(products);
		//this.allProduct = products;
		console.log("Setted Product", products);
	}

	getProduct() {
		return this.allProduct;
	}

	// getSpecificCategory(category, categoryName) {
	// 	this.clearBrandFilter();
	// 	this.googleAnalyticsEventsService.emitEvent("Reward Gallery Categories", categoryName, "viewCategory");
	// 	console.log("category", category);

	// 	let reqObj = {
	// 		"categoryId": category
	// 	}
	// 	// this.requestObj.categoryId = category;	
	// 	this.getAllFilteredProducts(reqObj);
	// 	this.categoryName = categoryName
	// }
	// getSpecificCategoryAndSubCategory(category, subCategory, subCategoryName) {
	// 	this.clearBrandFilter();
	// 	console.log("category", category);
	// 	console.log("subCategory", subCategory);
	// 	this.googleAnalyticsEventsService.emitEvent("Reward Gallery Categories", subCategoryName, "viewSubCategory");
	// 	let reqObj = {
	// 		"categoryId": category,
	// 		"subCategoryId": subCategory
	// 	}
	// 	// this.requestObj.categoryId = category;
	// 	// this.requestObj.subCategoryId = subCategory;
	// 	this.getAllFilteredProducts(reqObj);
	// 	this.categoryName = subCategoryName;

	// }
	// getAllProduct() {
	// 	this.clearBrandFilter();
	// 	this.getAllProducts();
	// 	this.categoryName = "All Categories";
	// }

	// getSearchProduct(searchProduct, catId) {
	// 	console.log("category", searchProduct);
	// 	this.clearBrandFilter();
	// 	let reqObj = {
	// 		"searchProduct": searchProduct,
	// 		"categoryId": catId
	// 	}
	// 	if (catId == "") {
	// 		delete reqObj.categoryId;
	// 	}

	// 	this.getAllFilteredProducts(reqObj);

	// }
	// clearAll() {
	// 	this.brandArr = [];
	// 	var brand = this.allBrands;
	// 	for (var i = 0; i < brand.length; i++) {
	// 		brand[i].isSelected = false;
	// 	}
	// 	this.getAllProducts();
	// }

	// clearBrandFilter() {
	// 	console.log("HERE");
	// 	this.brandArr = [];

	// 	var brand = this.allBrands;
	// 	if (this.allBrands != undefined) {

	// 		for (var i = 0; i < brand.length; i++) {
	// 			brand[i].isSelected = false;
	// 		}
	// 	}
	// }

	// addProductDetai2(){

	// }

	/***********************************    API CALLS   ******************************/
	/*
	   METHOD         : getAllProducts
	   DESCRIPTION    : For Fetching All the Product in the Reward Gallery.
	*/
	// 	public getAllProducts() {
	// 		this.showLoader = true;

	// 		this.rewardsGalleryService.getAllProducts()
	// 		this.showLoader = true;

	// 		this.rewardsGalleryService.getAllProducts()
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						console.log("All Product : ", responseObject.result);
	// 						this.allProduct = responseObject.result;
	// 						for (let i = 0; i < this.allProduct.length; i++) {
	// 							this.isProductAdded.push(false);
	// 							console.log("for loop", this.isProductAdded[i]);
	// 						}
	// 						// for (let i = 0; i < this.allProduct.length; i++) {
	// 						// 	this.allProduct[i].quantity = 0;
	// 						// }
	// 						console.log("isProductAdded Array ngOnInit", this.isProductAdded);
	// 						this.noProductFound = false;
	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						this.noProductFound = true;
	// 						this.allProduct = null;
	// 						this.allProduct = [];
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	// 	/*
	// 		   METHOD         : getAllProducts
	// 		   DESCRIPTION    : For Fetching All the Product in the Reward Gallery.
	// 	   */
	// 	public getSpecificAllProducts() {
	// 		this.showLoader = true;

	// 		this.rewardsGalleryService.getSpecificAllProducts()
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.allProduct = responseObject.result;
	// 						this.noProductFound = false;
	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						this.noProductFound = true;
	// 						this.allProduct = null;
	// 						this.allProduct = [];
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}



	// 	/*
	//         METHOD         : getAllBrands()
	//         DESCRIPTION    : For Fetching All the Category and Sub Category in the Reward Gallery.
	//     */
	// 	public getAllBrands() {
	// 		this.showLoader = true;

	// 		this.rewardsGalleryService.getAllBrands()
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:

	// 						console.log("AllBrands", responseObject.result);

	// 						var brands = responseObject.result;
	// 						for (var i = 0; i < brands.length; i++) {
	// 							brands[i].isSelected = false;
	// 						}
	// 						this.allBrands = brands;
	// 						console.log(brands);
	// 						console.log(this.allBrands);


	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	// 	/*
	//    METHOD         : getAllCategoryAndSubCategory()
	//    DESCRIPTION    : For Fetching All the Category and Sub Category in the Reward Gallery.
	// */
	// 	getAllCategoryAndSubCategory() {
	// 		this.showLoader = true;

	// 		this.rewardsGalleryService.getAllCategoryAndSubCategory()
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.isCategory = false;
	// 						this.allCategoryAndProduct = responseObject.result;
	// 						this.rewardsGalleryService.setProduct(responseObject.result);

	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}


	// 	getAllSlabs() {
	// 		this.showLoader = true;
	// 		console.log("In Slab 2");
	// 		this.rewardsGalleryService.getAllSlabs()
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.isSlab = false;
	// 						this.slabs = responseObject.result;
	// 						this.pointsSlabs = this.slabs.pointsSlabs;
	// 						console.log("Slabs Response Object : ", this.slabs);
	// 						console.log("PointSlabs In slab Response Object : ", this.pointsSlabs);
	// 						// this.rewardsGalleryService.setProduct(responseObject.result);

	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	// 	/*
	//    METHOD         : getAllFilteredProducts()
	//    DESCRIPTION    : For Fetching filtered Product on the basis of Category and SubCategory in the Reward Gallery.
	// */

	// 	getAllFilteredProducts(requestObject) {
	// 		this.showLoader = true;
	// 		this.requestObj.categoryId = requestObject.categoryId;
	// 		this.requestObj.subCategoryId = requestObject.subCategoryId;
	// 		this.requestObj.searchProduct = requestObject.searchProduct;
	// 		if (this.brandArr.length == 0) {
	// 			delete this.requestObj.brandIdArray;
	// 		} else {
	// 			this.requestObj.brandIdArray = this.brandArr;

	// 		}
	// 		this.rewardsGalleryService.getAllFilteredProducts(requestObject)
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.noProductFound = false;
	// 						this.allProduct = responseObject.result;
	// 						console.log("this.requestObj.searchProduct", requestObject)
	// 						if (this.requestObj.searchProduct) {
	// 							if (this.requestObj.categoryId == undefined) {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": "All Categories"
	// 								});
	// 							} else {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": this.requestObj.categoryId
	// 								});
	// 							}

	// 						}

	// 						this.rewardsGalleryService.setProduct(responseObject.result);

	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:


	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						console.log(responseCodes.RESP_FAIL);
	// 						this.noProductFound = true;
	// 						console.log("this.requestObj.searchProduct", requestObject)
	// 						if (this.requestObj.searchProduct) {

	// 							console.log("this.requestObj.categoryId", this.requestObj.categoryId);

	// 							if (this.requestObj.categoryId == undefined) {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": "All Categories"
	// 								});
	// 							} else {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": this.requestObj.categoryId
	// 								});
	// 							}



	// 						}

	// 						this.allProduct = null;
	// 						this.allProduct = [];

	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	addProductDetail(product, index) {
		this.feedbackMessageComponent.updateMessage(true, product.productName + " added Successfully in cart", "alert-success");
		for (let i = 0; i < this.productArr.length; i++) {
			this.isProductAdded[i] = true;
		}
		product.type = "globalRewardGallery";
		console.log("isProductAdded[index]", this.isProductAdded[index]);
		this.isProductAdded[index] = true;
		console.log("isProductAdded[index]", this.isProductAdded[index]);
		this.productArr = this.productDetailsService.getProduct();
		let found = false;
		for (let i = 0; i < this.productArr.length; i++) {
			if (this.productArr[i].productId == product.productId) {
				// this.productQuantity[i]++;
				found = true;
				this.productArr[i].quantity++;
				console.log("Product Quantity", this.productArr[i].quantity);
				// this.productDetailsService.setProductQuantity(this.productQuantity);
				//this.homeService.updateCartQuantity(this.productArr);
				//return;
			}
		}
		if (!found) {
			this.productArr.push(product);
			// this.productQuantity = JSON.parse(localStorage.getItem("productQuantity"));
			this.productArr[this.productArr.length - 1].quantity = 1;
		}
		this.cartCount = this.productArr.length;
		console.log("Product Quantity", this.productArr[this.productArr.length - 1].quantity);
		console.log("ProductArr in add product details", this.productArr);
		console.log("CartCount in reward Gallery", this.cartCount);
		// this.productDetailsService.setProductQuantity(this.productQuantity);
		this.productDetailsService.setProduct(this.productArr);
		this.productDetailsService.setCartCount(this.cartCount);
		this.cartCount = this.productDetailsService.getCartCount();
		// location.assign("../home/home.component");
		// this.homeComponent.onCartClick();
	}
	// 	/*
	// METHOD         : getAllFilteredProducts()
	// DESCRIPTION    : For Fetching filtered Product on the basis of Category and SubCategory in the Reward Gallery.
	// */

	// 	getSpecificAllFilteredProducts(requestObject) {
	// 		this.showLoader = true;
	// 		this.requestObj.categoryId = requestObject.categoryId;
	// 		this.requestObj.subCategoryId = requestObject.subCategoryId;
	// 		this.requestObj.searchProduct = requestObject.searchProduct;
	// 		if (this.brandArr.length == 0) {
	// 			delete this.requestObj.brandIdArray;
	// 		} else {
	// 			this.requestObj.brandIdArray = this.brandArr;

	// 		}
	// 		this.rewardsGalleryService.getSpecificAllFilteredProducts(requestObject)
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.noProductFound = false;
	// 						this.allProduct = responseObject.result;
	// 						console.log("this.requestObj.searchProduct", requestObject)
	// 						if (this.requestObj.searchProduct) {
	// 							if (this.requestObj.categoryId == undefined) {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": "All Categories"
	// 								});
	// 							} else {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": this.requestObj.categoryId
	// 								});
	// 							}

	// 						}

	// 						this.rewardsGalleryService.setProduct(responseObject.result);

	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:


	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						console.log(responseCodes.RESP_FAIL);
	// 						this.noProductFound = true;
	// 						console.log("this.requestObj.searchProduct", requestObject)
	// 						if (this.requestObj.searchProduct) {

	// 							console.log("this.requestObj.categoryId", this.requestObj.categoryId);

	// 							if (this.requestObj.categoryId == undefined) {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": "All Categories"
	// 								});
	// 							} else {
	// 								this.filterForm.patchValue({
	// 									"search": this.requestObj.searchProduct,
	// 									"category": this.requestObj.categoryId
	// 								});
	// 							}



	// 						}

	// 						this.allProduct = null;
	// 						this.allProduct = [];

	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	// 	onSlabValueClick(slabValue) {
	// 		let reqObj: any = {};

	// 		reqObj.slabValue = slabValue;
	// 		this.getSpecificAllFilterSlabs(reqObj);
	// 	}

	// 	getSpecificAllFilterSlabs(reqObj) {
	// 		this.showLoader = true;
	// 		this.rewardsGalleryService.getSpecificAllFilterSlabs(reqObj)
	// 			.subscribe(
	// 			(responseObject) => {
	// 				this.showLoader = false;
	// 				let responseCodes = this.configService.getStatusTokens();

	// 				console.log(responseObject);
	// 				console.log("Response code", responseCodes);
	// 				switch (responseObject.statusCode) {
	// 					case responseCodes.RESP_ROLLBACK_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SERVER_ERROR:
	// 						break;
	// 					case responseCodes.RESP_SUCCESS:
	// 						this.allProduct = responseObject.result;
	// 						this.noProductFound = false;
	// 						console.log("getSpecificAllFilterSlabs response object", this.allProduct);
	// 						break;
	// 					case responseCodes.RESP_AUTH_FAIL:
	// 						break;
	// 					case responseCodes.RESP_FAIL:
	// 						this.noProductFound = true;
	// 						this.allProduct = null;
	// 						this.allProduct = [];
	// 						console.log(responseCodes.RESP_FAIL);
	// 						break;
	// 					case responseCodes.RESP_ALREADY_EXIST:
	// 						break;
	// 					case responseCodes.RESP_KYC_AWAITING:
	// 						break;
	// 					case responseCodes.RESP_BLOCKED:
	// 						break;


	// 				}
	// 			},
	// 			err => {
	// 				this.showLoader = false;

	// 			}
	// 			);
	// 	}

	/*************************** 	GET PRODUCT FORM PARAMS 	******************************* */
	clearAllBrands() {
		this.brandIdArray = [];
		for (let i = 0; i < this.brandsArray.length; i++) {
			this.brandsArray[i].isSelected = false;
		}
		this.getProductFormParams();
	}

	setCategory(categoryId, categoryName) {
		console.log(categoryId);
		this.filterText = categoryName;
		this.search = undefined;
		if (this.categoryId !== "All Categories" || this.categoryId == undefined) {
			console.log()
			this.categoryId = categoryId;
			this.subCategoryId = undefined;
		} else {
			this.categoryId = undefined;
			this.subCategoryId = undefined;
		}
		this.isBrandsToRefresh = true;
		this.isdeleteBrand = true;
		if (categoryId === undefined) {

			this.categoryId = "All Categories"
			this.filterForm.patchValue({
				"category": "All Categories"
			})
		}
		else {
			// this.isdeleteBrand = false;
			this.filterForm.patchValue({
				"category": categoryId
			})
		}
		// if (categoryId === undefined) {
		// 	// delete reqObj.brandIdArray;
		// 	this.isAllCategories = true;
		// }
		this.subCategoryId = undefined;
		this.getProductFormParams();
	}

	setSubCategory(categoryId, subCategoryId, subCategoryName) {
		this.filterForm.patchValue({
			"category": categoryId
		})
		this.filterText = subCategoryName;
		this.search = undefined;
		this.categoryId = categoryId;
		this.subCategoryId = subCategoryId;
		this.isBrandsToRefresh = true;
		this.isBrandsToRefresh = true;
		this.isdeleteBrand = true;
		this.getProductFormParams();
	}

	setBrandsArray(index) {
		this.brandsArray[index].isSelected = !this.brandsArray[index].isSelected;
		this.brandIdArray = [];
		for (let i = 0; i < this.brandsArray.length; i++) {
			if (this.brandsArray[i].isSelected) this.brandIdArray.push(this.brandsArray[i].brandId);
		}
		this.isBrandsToRefresh = false;
		this.getProductFormParams();
	}

	setSearch() {
		this.filterText = "";
		if (this.filterForm.value.category != "All Categories") {
			this.categoryId = this.filterForm.value.category;
			// this.isBrandsToRefresh = true;
			// this.isAllCategories = true;
			this.subCategoryId = undefined;
		} else {
			this.categoryId = undefined;
			this.subCategoryId = undefined;
		}
		this.isdeleteBrand = true;
		this.isBrandsToRefresh = true;
		this.search = this.filterForm.value.search;
		this.getProductFormParams();
	}

	setSort() {
		if (this.sort === "name")
			this.sort = "points";
		else
			this.sort = "name"
		this.getProductFormParams();
	}

	getProductFormParams() {
		let reqObj: any = {};

		//CATEGORY FILTER
		console.log("categoryId in getFormParams ", this.categoryId);
		if (this.categoryId && this.categoryId != "All Categories") {
			console.log("inside all categories");
			reqObj.categoryId = this.categoryId;
			delete reqObj.brandIdArray;
		}

		//SUBCATEGORY FILTER
		console.log("forming parameters subcategoryId", this.subCategoryId);
		if (this.subCategoryId != undefined) {
			reqObj.subCategoryId = this.subCategoryId;
		}
		//BRAND FILTER
		if (this.brandIdArray.length > 0) reqObj.brandIdArray = this.brandIdArray;

		//SEARCH
		if (this.search != undefined) reqObj.searchProduct = this.search;


		reqObj.sort = { "productName": 1 }
		console.log(reqObj, reqObj);
		if (this.isdeleteBrand) delete reqObj.brandIdArray;
		this.getGlobalProductMaster(reqObj);
	}
	/************************	END OF GET PRODUCT FORM PARAMS 	******************************* */

	keysrt(key) {
		return function (a, b) {
			if (a[key] > b[key]) return 1;
			if (a[key] < b[key]) return -1;
			return 0;
		}
	}



	/************************************** 	API CALLS 	 ************************************/
	//GET ALL PRODUCTS
	getGlobalProductMaster(reqObj) {
		this.showLoader = true;
		this.rewardsGalleryService.getGlobalProductMaster(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							console.log(this.sort);
							if (this.sort == "name") { this.productsArray = responseObject.result.sort(this.keysrt('pointsPerQuantity')); }
							else { this.productsArray = responseObject.result.sort(this.keysrt('productName')); }

							console.log("this.productsArray", this.productsArray);
							this.productCount = responseObject.count;
							// console.log("this.productsArray : ", this.productsArray);
							if (this.isBrandsToRefresh === true) {
								var brandIdsArr = [];
								if (this.productsArray !== undefined) {
									for (let i = 0; i < this.productsArray.length; i++) {
										brandIdsArr.push(this.productsArray[i].brandId);
									}
								}
								brandIdsArr.sort();

								var reqObj: any = {};
								reqObj.brandArray = brandIdsArr;
								this.getFilterBrandMaster(reqObj);
							}
							this.isBrandsToRefresh = false;
							this.isdeleteBrand = false;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.productsArray = [];
							this.noProductFound = true;
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	//GET ALL CATEGORIES AND SUBCATEGORIES
	getAllCategoryAndSubCategory() {
		this.showLoader = true;
		this.rewardsGalleryService.getAllCategoryAndSubCategory().subscribe(
			(responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.categorySubcategoryArray = responseObject.result;
						console.log("this.categorySubcategoryArray : ", this.categorySubcategoryArray);
						var product;
						product = this.categorySubcategoryArray;


						for (var i = 0; i < product.length; i++) {
							product[i].isHover = false;
						}

						// this.configService.setCategorySubCategories(this.categorySubcategoryArray);/
						this.configService.setCategorySubCategories(product);

						console.log("this.product : ", product);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.categorySubcategoryArray = [];
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_KYC_AWAITING:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	//GET ALL BRANDS
	getBrandMaster() {
		this.showLoader = true;
		this.rewardsGalleryService.getBrandMaster()
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.brandsArray = responseObject.result;
							console.log("this.brandsArray : ", this.brandsArray);
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.brandsArray = [];
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	getFilterBrandMaster(reqObj) {
		this.showLoader = true;
		this.rewardsGalleryService.getFilterBrandMaster(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.brandsArray = responseObject.result;
							console.log("this.brandsArray : ", this.brandsArray);
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.brandsArray = [];
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}
	/********************************* 	END OF API CALLS 	 ************************************/

	/********************************* 	CART FUNCTIONALITY 	 ************************************/
	addToCart(product) {
		this.showLoader = true;
		console.log("this.showLoader", this.showLoader);
		let cartArray: any[] = this.cartService.cartArray;

		if (cartArray.length > 0) {
			console.log("this.showLoader", this.showLoader);
			for (let i = 0; i < cartArray.length; i++) {
				console.log("this.showLoader", this.showLoader);
				if (cartArray[i].productId == product.productId) {
					this.showLoader = false;
					console.log("this.showLoader", this.showLoader);
					this.feedbackMessageComponent.updateMessage(true, "Product " + product.productName + "already added to Cart", "alert-danger");
					break;
				}
				if (i == cartArray.length - 1) {
					console.log("this.showLoader", this.showLoader);
					this.feedbackMessageComponent.updateMessage(true, product.productName + " added Successfully in cart", "alert-success");
					this.cartService.addToCartFormReqObj(product.productId, product.productName);
				}
			}
		}
		else {
			console.log("this.showLoader", this.showLoader);
			this.cartService.addToCartFormReqObj(product.productId, product.productName);
		}
	}
	/***************************** 	END OF CART FUNCTIONALITY 	 ********************************/
}