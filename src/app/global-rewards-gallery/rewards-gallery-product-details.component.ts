/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery Product Detail
	Date Created	: 	04 April 2017
	Date Modified	: 	04 April 2017
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { GlobalRewardGalleryService } from "./rewards-gallery.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
// import { KSSwiperContainer, KSSwiperSlide } from 'angular2-swiper';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from '../home/home.service';

import { CartService } from "../shared-services/cart.service";

@Component({
	templateUrl: './rewards-gallery-product-details.component.html',
	styleUrls: ['./rewards-gallery.component.css'],
	// providers: [KSSwiperContainer, KSSwiperSlide]

})


export class RewardsGalleryProductDetailsComponent implements OnInit {
	imageURL: any;
	public isZoom: boolean = false;

	slideTo: number;
	private showLoader: boolean = false;

	private imageArr: string[] = ["/assets/images/reward-gallery-image-lg.png",
		"/assets/images/reward-gallery-image-lg.png",
		"/assets/images/reward-gallery-image-lg.png",
		"/assets/images/aboutus-racold.png"
	];

	private isEligibleForRedemption: boolean;

	public allProduct: any = [];
	public product: any;
	private productImages: any = [];

	public allBrands: any;
	public allCategoryAndProduct: any;
	public parseInteger: any;

	private filterForm: FormGroup; //Filter  form group
	private highlight: string = "rg-hightlight";
	private intialIndex: number = 3;

	config: Object = {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.next',
		prevButton: '.prev',
		spaceBetween: 50,
		intialSlide: 2
	};

	public categorySubcategoriesArray: any[] = [];

	// @ViewChild(KSSwiperContainer) swiperContainer: KSSwiperContainer;
	// @ViewChild(KSSwiperContainer) swiper: KSSwiperSlide;
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	public programInfo: any = {};
	public programUser: any = {};
	public bEInfo: any = {};

	public isCategoryHover: boolean;
	public roverElementColor: any;
	public clientColor: any;
	public selectedIndex: any;
	public selectedSubIndex: any;
	public selectedBrandIndex: any;
	public showBrandColor: boolean;


	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		public cartService: CartService,

		private rewardsGalleryService: GlobalRewardGalleryService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private productDetailsService: ProductDetailsService,
		private homeService: HomeService,

	) {
		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
		});

		this.programInfo = configService.getloggedInProgramInfo();
		this.programUser = this.configService.getloggedInProgramUser();
		this.bEInfo = this.configService.getloggedInBEInfo();

		if (this.programUser.programRoleInfo.eligibleForRedemption) this.isEligibleForRedemption = false;
		else this.isEligibleForRedemption = true;

		this.categorySubcategoriesArray = this.configService.getCategorySubCategories();
		console.log("********************************************", this.categorySubcategoriesArray);

		this.product = configService.getRewardGalleryProduct();
		this.product.shippingInfo = this.product.shippingInfo !== undefined ? this.product.shippingInfo : "Shipped in 40 - 45 business days";
		this.product.quantity = 1;
		let productArr = this.productDetailsService.getProduct();
		/** cart product quantity and product quantity does not match */
		for (let i = 0; i < productArr.length; i++) {
			if (productArr[i].productId == this.product.productId && productArr[i].quantity == this.product.quantity) {
				this.product.quantity = productArr[i].quantity;
			}
		}
		if (!this.product.quantity) {
			this.product.quantity = 1;
		}
		this.parseInteger = parseInt;
		console.log("PRODUCT", this.product.image);
		var images = this.product.image;

		for (var i = 0; i < images.length; i++) {
			console.log("IMAGE", images[i]);
			if (images[i] != null) {
				this.productImages.push(images[i]);
			}
		}


	
		this.filterForm = formBuilder.group({
			"view": [""],
			"search": [""],
			"category": [""]
		});


		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", this.product.productName, "viewSpecificProduct");
		this.getAllCategoryAndSubCategory();
		this.clientColor = this.configService.getThemeColor();


	}

	ngOnInit() {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Reward Gallery Product Details");
				ga('send', 'pageview');
			}
		});
	}

	mouseLeaveCategory() {
		this.isCategoryHover = false;
	}
	mouseOverCategory() {
		this.isCategoryHover = true;
		this.roverElementColor = this.configService.getThemeColor();
	}

	mouseLeaveCategoryIndex() {
		this.selectedIndex = null;
	}
	mouseOverCategoryIndex(index) {
		// this.isCategoryHover = true;

		console.log('index', index);
		this.selectedIndex = index;
		console.log('index', this.selectedIndex);
		// console.log('this.isCategoryHover OVER', this.isCategoryHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}


	mouseLeaveSubCategoryIndex() {
		this.selectedSubIndex = null;
	}
	mouseOverSubCategoryIndex(index) {
		// this.isCategoryHover = true;

		console.log('index', index);
		this.selectedSubIndex = index;
		console.log('index', this.selectedIndex);
		// console.log('this.isCategoryHover OVER', this.isCategoryHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}



	search() {
		console.log("Go To Category in search with ", this.configService.isRewardGallery);
		if (this.filterForm.value.category != "") {
			this.router.navigate(['./home/globalrewardsgallery/categories'],
				{ queryParams: { searchProduct: this.filterForm.value.search, catId: this.filterForm.value.category } });
		} else {
			this.router.navigate(['./home/globalrewardsgallery/categories'],
				{ queryParams: { searchProduct: this.filterForm.value.search } });
		}

	}


	moveNext() {
		// console.log("asa", this.swiperContainer.swiper.realIndex);
		// if (this.swiperContainer.swiper.realIndex == this.intialIndex) {

		// }
		// this.swiperContainer.swiper.slideNext();
	}

	// movePrev() {
	// 	console.log("asa", this.swiperContainer.swiper.realIndex);
	// 	this.swiperContainer.swiper.slidePrev();
	// }

	// showImage(index) {
	// 	this.slideTo = index;
	// 	console.log("Index", index);
	// 	this.swiperContainer.swiper.slideTo(index);

	// }

	goToCategory() {

		this.router.navigate(['./home/globalrewardsgallery/categories'],
			{ queryParams: { catId: this.product.category.categoryId } });

	}
	goBackToCategory() {
		this.router.navigate(['./home/globalrewardsgallery/categories']);

	}



	getSpecificCategory(category, catName) {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", catName, "viewCategory");
		console.log("category", category);
		this.router.navigate(['./home/globalrewardsgallery/categories'],
			{ queryParams: { catId: category, catName: catName } });

	}
	getSpecificCategoryAndSubCategory(category, subCategory, subCatName) {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", subCatName, "viewSubCategory");
		console.log("category", category);
		console.log("subCategory", subCategory);
		console.log("subNAME", subCatName);
		this.router.navigate(['./home/globalrewardsgallery/categories'],
			{ queryParams: { catId: category, subCatId: subCategory, subCatName: subCatName } });

	}

	/*
	   METHOD         : getAllCategoryAndSubCategory()
	   DESCRIPTION    : For Fetching All the Category and Sub Category in the Reward Gallery.
   */
	getAllCategoryAndSubCategory() {
		this.showLoader = true;

		this.rewardsGalleryService.getAllCategoryAndSubCategory()
			.subscribe(
			(responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();

				console.log(responseObject);
				console.log("Response code", responseCodes);
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.allCategoryAndProduct = responseObject.result;
						this.rewardsGalleryService.setProduct(responseObject.result);

						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						console.log(responseCodes.RESP_FAIL);
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_KYC_AWAITING:
						break;
					case responseCodes.RESP_BLOCKED:
						break;


				}
			},
			err => {
				this.showLoader = false;
			}
			);
	}

	//workaround for production

	addProductDetail() {
		this.feedbackMessageComponent.updateMessage(true, this.product.productName + " added Successfully in cart", "alert-success");
		setTimeout(this.feedbackMessageClose, 3000);
		this.product.type = "globalRewardGallery";
		this.product.isPlaced = false;
		console.log("Product Details : ", this.product);
		let productArr: any = [];
		productArr = this.productDetailsService.getProduct();
		for (let i = 0; i < productArr.length; i++) {
			if (productArr[i].productId == this.product.productId && productArr[i].quantity == this.product.quantity) {
				// this.productQuantity[i]++;
				// productArr[i].quantity++;
				// console.log("Product Quantity", productArr[i].quantity);
				// this.productDetailsService.setProductQuantity(this.productQuantity);
				return;
			} else if (productArr[i].productId == this.product.productId && productArr[i].quantity != this.product.quantity) {
				productArr[i].quantity = this.product.quantity;
				this.productDetailsService.setProduct(productArr);
				// this.homeService.updateCartQuantity(productArr);
				return;
			}
		}
		productArr.push(this.product);
		this.productDetailsService.setProduct(productArr);
		// this.homeService.updateCartQuantity(productArr);
	}

	feedbackMessageClose() {
		this.feedbackMessageComponent.updateMessage(false, '', '', '');
	}

	// onQuantityClick() {
	// 	let stock = parseInt(this.product.stock);
	// 	console.log("stock", stock);
	// 	console.log("quantity", this.product.quantity);
	// 	if (this.product.quantity < 1 || !this.product.quantity) {
	// 		this.product.quantity = 1;
	// 	}
	// 	if (this.product.quantity > this.product.stock) {
	// 		this.product.isInStock = true;
	// 		this.product.quantity = this.product.stock;
	// 	}
	// 	console.log("productQuantity selected is", this.product.quantity);
	// 	if (this.product.quantity == stock) {
	// 		this.product.isInStock = true;
	// 		console.log("IsInStock On Quantity Click", this.product.isInStock);
	// 	} else {
	// 		this.product.isInStock = false;
	// 		console.log("IsInStock On Quantity Click", this.product.isInStock);
	// 	}
	// }

	zoom(n) {

		if (this.isZoom === false) {
			this.isZoom = true;
			this.imageURL = this.productImages[n];
		}
	}

	closeDetails() {
		if (this.isZoom === true) {
			this.isZoom = false;
		}
	}
	updateCartQuantity(productArray) {
		let cart: any = [];
		let programInfo = this.configService.getloggedInProgramInfo();

		for (let i = 0; i < productArray.length; i++) {
			if (productArray[i].quantity == 0) {
				productArray[i].quantity = 1;
				continue;
			}
			let found: boolean = false;
			for (let j = 0; j < cart.length; j++) {
				console.log("for updatation check ", cart[j].productId,
					cart[j].productId, productArray[i].productId
					, productArray[i].type, cart[j].type,
					"isPlaced", (!cart[j].isPlaced || cart[j].isPlaced == false)
					, (cart[j].programId == programInfo.programId && cart[j].productId == productArray[i].productId && productArray[i].type == cart[j].type
						&& (!cart[j].isPlaced || cart[j].isPlaced == false)));
				if (cart[j].programId == programInfo.programId && cart[j].productId == productArray[i].productId &&
					productArray[i].type == cart[j].type && (!cart[j].isPlaced || cart[j].isPlaced == false)) {
					found = true;
					cart[j].quantity = productArray[i].quantity;
				}
			}
			if (!found) {
				let cartObject: any = {};
				cartObject.programId = programInfo.programId;
				cartObject.productId = productArray[i].productId;
				cartObject.quantity = productArray[i].quantity;
				if (productArray[i].type == "globalRewardGallery") {
					cartObject.type = "globalRewardGallery";
				} else if (productArray[i].type == "programSpecificRewardGallery") {
					cartObject.type = "programSpecificRewardGallery";
				}
				cartObject.isPlaced = false;
				console.log("ADDING NEW ELEMENT IN CART DATABSE ", cartObject);
				cart.push(cartObject);
			}
		}
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.updateInfo = {};
		reqObj.updateInfo.productList = cart;
		console.log("passing this object to upate the cart on add to cart", reqObj);
		// this.homeService.updateCartProductList(reqObj);
	}

	/********************************* 	CART FUNCTIONALITY 	 ************************************/
	addToCart(product) {
		console.log("called");
		this.showLoader = true;

		// let userPoints : number;
		// let productPoints : number;
		let cartArray: any[] = this.cartService.cartArray;

		// if (this.programInfo.allowStarPointsConversion === true) userPoints = this.bEInfo.availableStarPoints;
		// else	userPoints = this.programUser.totalPointsAvailableForRedeem

		// productPoints = parseInt(product.pointsPerQuantity);
		// for (let i = 0; i < cartArray.length; i++) {
		// 	console.log("productPoints", i, productPoints);
		// 	productPoints = productPoints + parseInt(cartArray[i].productInfo.pointsPerQuantity) * parseInt(cartArray[i].quanity);
		// }
		// console.log("productPoints", productPoints, "userPoints", userPoints, parseInt(cartArray[0].productInfo.pointsPerQuantity), parseInt(cartArray[i].quanity));
		// if (productPoints < userPoints)	
		// {
		if (cartArray.length > 0) {
			for (let i = 0; i < cartArray.length; i++) {
				if (cartArray[i].productId == product.productId) {
					this.showLoader = false;

					this.feedbackMessageComponent.updateMessage(true, "Product " + product.productName + "already added to Cart", "alert-danger");
					break;
				}
				if (i == cartArray.length - 1) {
					console.log("reached here");
					this.feedbackMessageComponent.updateMessage(true, product.productName + " added Successfully in cart", "alert-success");
					this.cartService.addToCartFormReqObj(product.productId, product.productName);
				}
			}
		}
		else {
			this.cartService.addToCartFormReqObj(product.productId, product.productName);
		}
		// }
		// else{
		// 	this.feedbackMessageComponent.updateMessage(true, "You don't have sufficient points to redeem", "alert-danger");
		// }
	}

	onQuantityClick(productId, quantity) {
		this.showLoader = true;

		this.cartService.updateToCartFormReqObj(productId, "Pending", quantity);

		// this.subPoint = 0;
		// for (let i = 0; i < this.productArr.length; i++) {
		// 	if (this.productArr[i].quantity < 1) {
		// 		this.productArr[i].quantity = 1;
		// 	}
		// 	if (this.productArr[i].quantity > this.productArr[i].stock) {
		// 		this.productArr[i].isInStock = true;
		// 		this.productArr[i].quantity = this.productArr[i].stock;
		// 	}
		// 	this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
		// }
		// // console.log("Subpoint in onQuantityClick Function : ", this.subPoint);
		// // this.reedemPoints = this.configService.getloggedInProgramUser().pointsAvialableToRedeem;
		// this.reedemPoints = parseInt(localStorage.getItem("points"));
		// console.log("Redeem Points : ", this.reedemPoints);
		// console.log("Sub Point : ", this.subPoint);
		// // this.reedemPoints = 20000;
		// if (this.reedemPoints >= this.subPoint) {
		// 	this.isReedemPoints = false;
		// } else {
		// 	this.isReedemPoints = true;
		// }

		// for (let i = 0; i < this.productArr.length; i++) {
		// 	let stock = parseInt(this.productArr[i].stock);
		// 	console.log("stock", stock);
		// 	if (this.productArr[i].quantity == stock) {
		// 		this.productArr[i].isInStock = true;
		// 		console.log("IsInStock On Quantity Click", this.productArr[i].isInStock);
		// 	} else {
		// 		this.productArr[i].isInStock = false;
		// 		console.log("IsInStock On Quantity Click", this.productArr[i].isInStock);
		// 	}
		// }
		// this.updateCartQuantity(this.productArr);
	}
	/***************************** 	END OF CART FUNCTIONALITY 	 ********************************/
}
