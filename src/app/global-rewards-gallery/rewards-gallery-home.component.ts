/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery
	Date Created	: 	03 April 2017
	Date Modified	: 	03 April 2017
*/

import { Component, OnInit, OnChanges } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { GlobalRewardGalleryService } from "./rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { RewardsGalleryCategoriesComponent } from './rewards-gallery-categories.component';
import { CartService } from "../shared-services/cart.service";

@Component({
	templateUrl: './rewards-gallery-home.component.html',
	styleUrls: ['./rewards-gallery.component.css'],
	providers: [RewardsGalleryCategoriesComponent]

})
export class RewardsGalleryHomeComponent2 implements OnInit {


	public showBrand: boolean = true;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private rewardsGalleryService: GlobalRewardGalleryService,
		private rewardGalleryCategory: RewardsGalleryCategoriesComponent,
		private formBuilder: FormBuilder,
		private cartService: CartService) {

		this.cartService.RGType = "GlobalRG";
		
	}

	ngOnInit() {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', event.urlAfterRedirects);
				ga('send', 'pageview');
			}
		});
	}



	toggleBrand() {
		this.showBrand = !this.showBrand;
	}






}
