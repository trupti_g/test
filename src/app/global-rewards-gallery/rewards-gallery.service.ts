
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable, Subject, BehaviorSubject } from "rxjs";

@Injectable()
export class GlobalRewardGalleryService {
	private headers;
	private options;
	private frontendUserInfo;
	public searchQuery: string;
	public searchCategory: string;

	public allProducts = new Subject<any>();
	private product = new Subject<any>();
	public programInfo: any;
	public programId: string;
	public clientId: string;
	public loggedInUser: any;

	public userId: string = "";

	// private allProducts:any;

	constructor(private http: Http,
		private configService: ConfigService) {

		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.configService
				.getAuthenticationToken()

		});

		this.options = new RequestOptions({ headers: this.headers });
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.loggedInUser = this.configService.getloggedInProgramUser();
		if (this.programInfo) {
			this.programId = this.programInfo.programId;
		}
		if(this.loggedInUser){
			this.clientId = this.loggedInUser.clientId;
		}
		this.frontendUserInfo = configService.getFrontEndUserInfo();
		if(configService.getloggedInProgramUser())
			this.userId = configService.getloggedInProgramUser().programUserId;
	}

	setSpecificProduct(product) {
		this.product = product
	}
	getSpecificProduct(): Observable<any> {
		return this.product.asObservable();
	}

	setAllProductToService(products) {
		// this.allProducts.next({ "products" : products });
		this.allProducts.next(products);
		this.allProducts.subscribe(allProduct => {
			// this.allProducts = allProduct;
			console.log("all Product parent", allProduct);
		});
	}

	getAllProductsFromService(): Observable<any> {

		return this.allProducts.asObservable();
	}

	setProduct(pro) {
		this.product = pro;
	}

	getAllProduct(RequestObject): any {
		var reqObj: any = {};
		let url = this.configService.getApiUrls().getAllProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}

		reqObj.clientId = this.clientId;
		reqObj.serviceType = "client"

		console.log("passing this object to get all global products", reqObj);
		console.log("with options", this.options);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getAllProducts(): any {
		var reqObj: any = {};
		let url = this.configService.getApiUrls().getAllRewardGalleryProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}

		reqObj.clientId = this.clientId;
		reqObj.serviceType = "client"

		console.log("passing this object to get all global products", reqObj);
		console.log("with options", this.options);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	getSpecificAllProducts(): any {
		var reqObj: any = {};
		let url = this.configService.getApiUrls().getSpecificAllRewardGalleryProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "programSetup"
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getAllFilteredProducts(reqObj): any {

		let url = this.configService.getApiUrls().getAllRewardGalleryProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "client"


		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {

			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}
	getSpecificAllFilteredProducts(reqObj): any {

		let url = this.configService.getApiUrls().getSpecificAllRewardGalleryProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;

		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {

			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}
		reqObj.serviceType = "programSetup";

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	// getAllCategoryAndSubCategory(): any {
	// 	var reqObj: any = {};
	// 	let url = this.configService.getApiUrls().getAllCategoryAndSubCategory;
	// 	var frontEndInfo = this.configService.getFrontEndUserInfo();
	// 	console.log("info", frontEndInfo);
	// 	if (frontEndInfo != null || frontEndInfo != undefined) {

	// 		reqObj.frontendUserInfo = frontEndInfo;
	// 	} else {
	// 		reqObj.frontendUserInfo = {
	// 			"userId": this.userId
	// 		};
	// 	}
	// 	reqObj.clientId = this.clientId;
	// 	reqObj.serviceType = "client"

	// 	return this.http.post(url, reqObj, this.options)
	// 		.map((res: Response) => {
	// 			console.log("inside map", res.json());
	// 			return res.json();
	// 		})
	// 		.catch((error: any) => {
	// 			console.log("inside catch");
	// 			return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
	// 		});
	// }

	getAllBrands(): any {
		var reqObj: any = {};
		let url = this
			.configService
			.getApiUrls()
			.getAllRewardGalleryBrands;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {

			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}
		reqObj.serviceType = "client"

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getAllSlabs(): any {
		console.log("In Service slab");
		var reqObj: any = {};
		let url = this.configService.getApiUrls().getRewardsGallerySlabs;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {

			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "programSetup"

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getSpecificAllFilterSlabs(reqObj): any {
		let url = this.configService.getApiUrls().getSpecificAllRewardGalleryProducts;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("info", frontEndInfo);
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "programSetup"
		console.log("getSpecificAllFilterSlabs request object : ", reqObj);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	/************************* 	GLOBAL REWARD GALLERY API CALLS 	************************** */
	//GET ALL PRODUCTS
	getGlobalProductMaster(reqObj): any {
		let url = this.configService.getApiUrls().getGlobalProductMaster;

		// var reqObj: any = {},;
		if (this.configService.getprogramInfo().allowStarPointsConversion == true) console.log("Do nothing");
		else reqObj.programId = this.programId;

		reqObj.clientId = this.clientId;
		reqObj.serviceType = "programSetup";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	//GET ALL CATEGORIES AND SUBCATEGORIES
	getAllCategoryAndSubCategory(): any {
		let url = this.configService.getApiUrls().getAllCategoryAndSubCategory;
		console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", url);
		var reqObj: any = {};
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "programSetup";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	//GET ALL BRANDS
	getBrandMaster(): any {
		let url = this.configService.getApiUrls().getBrandMaster;

		var reqObj: any = {};
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "client";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	//GET ALL BRANDS
	getFilterBrandMaster(reqObj: any): any {
		let url = this.configService.getApiUrls().getBrandMaster;
		reqObj.programId = this.programId;
		reqObj.clientId = this.clientId;
		reqObj.serviceType = "client";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}
	updateCartProductList(requestObject): any {

		let frontendUserInfo: any = {};
		requestObject.frontendUserInfo = {};

		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("RequestObject in Add Order Service", requestObject);
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().updateCart;
		console.log("URLLLL", url);
		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("error", error);
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}

	/******************** 	END OF PROGRAM SPECIFIC REWARD GALLERY API CALLS  **************************/


	/********************************* 	SET RECORDS FOR DETAILS PAGE 	*********************** */
	public categorySubCategoriesArray: any[] = [];

	setCategorySubCategories(categorySubCategoriesArray) {
		this.categorySubCategoriesArray = categorySubCategoriesArray
		console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", this.categorySubCategoriesArray)
	}

	getCategorySubCategories() {
		console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>", this.categorySubCategoriesArray);
		return this.categorySubCategoriesArray;
	}
	/***************************** 	END OF SET RECORDS FOR DETAILS PAGE 	********************** */
}
