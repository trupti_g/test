/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { DashboardHomeComponent } from "./dashboard-home.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { FooterModule } from '../footer/footer.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { DashboardRedemptionDetailComponent } from "./dashboard-redemption-detail.component";
import { DashboardService } from "./dashboard.service";
import { SharedModule } from "../shared-components/shared.module";
import { DashBoardApprovalsRedemptionList } from "./approvals-redemption-list";
import { PerformanceGraphsComponent } from "./performance-graphs.component";

import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { StatModule } from '../shared';



@NgModule({
	declarations: [DashboardHomeComponent, DashboardRedemptionDetailComponent, DashBoardApprovalsRedemptionList, PerformanceGraphsComponent],

	imports: [
		CommonModule,
		ReactiveFormsModule,
		HttpModule,
		MaterialModule,
		FooterModule,
		ChartsModule,
		AmChartsModule,
		SharedModule,
		NgbCarouselModule,
		NgbTabsetModule.forRoot(),
		StatModule

	],
	exports: [],

	providers: [
		DashboardService
	],
	entryComponents: [DashBoardApprovalsRedemptionList]
})
export class DashboardModule {
}