/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { Component, OnInit, OnChanges, OnDestroy, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, NavigationExtras } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfigService } from "../shared-services/config.service";
import { DashboardService } from "./dashboard.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { DashBoardApprovalsRedemptionList } from "./approvals-redemption-list";
import { DashboardRedemptionDetailComponent } from "./dashboard-redemption-detail.component";
import { AppService } from "../app.service";
import { ISubscription } from "rxjs/Subscription";
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';

@Component({
	templateUrl: './performance-graphs.component.html',
	styleUrls: ['./dashboard.component.css'],
	providers: [DashboardService]

})
export class PerformanceGraphsComponent implements OnInit, OnChanges, OnDestroy {
	public graphTypeFlag: any;
	public clientUserInfo: any;
	public secondProgram: any;
	public quarterlyGraph: boolean = false;
	public yearlyGraph: boolean = true;
	public monthlyGraph: boolean = false;
	public startDate1year: any;
	public startDate1: any;
	public startDate2year: any;
	public startDate2: any;
	public dropDownPrograms: any = [];
	public loggedInProgramInfo: any;
	public programIdArray: any = [];
	public loggedInBEProgramsMapped: any;
	public programForm: FormGroup;
	public showLoader: boolean = false;
	public data1: any[] = [];
	public data2: any[] = [];
	public data3: any[] = [];
	public isChannelGraph: boolean;

	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		public appService: AppService,
		private dashboardService: DashboardService,
		private formBuilder: FormBuilder,
		public dialog: MdDialog,
		private stringService: StringService,
		private AmCharts: AmChartsService,
		private AmCharts1: AmChartsService,

	) {
		this.graphTypeFlag = this.configService.getChannelGraphFlag(); //for knowing if channel graph or my sales graph
		this.loggedInProgramInfo = this.configService.getloggedInProgramInfo();
		this.loggedInBEProgramsMapped = this.configService.getBEProgramsMapped();
		this.clientUserInfo = this.configService.getClientUserInfo();
		this.dropDownPrograms = [];
		this.startDate1 = new Date(this.loggedInProgramInfo.programStartDate);
		this.startDate1year = this.startDate1.getFullYear();
		this.programIdArray[0] = this.loggedInProgramInfo.programId;
		if (this.loggedInBEProgramsMapped !== undefined) {
			for (var b = 0; b < this.loggedInBEProgramsMapped.length; b++) {
				if (this.loggedInProgramInfo.programId !== this.loggedInBEProgramsMapped[b].programId) {
					this.dropDownPrograms.push(this.loggedInBEProgramsMapped[b])
				}
				console.log("this.programIdArray", this.programIdArray);
			}
		} else if (this.clientUserInfo !== undefined) {
			for (var b = 0; b < this.clientUserInfo.programsMapped.length; b++) {
				if (this.loggedInProgramInfo.programId !== this.clientUserInfo.programsMapped[b].programId) {
					this.dropDownPrograms.push(this.clientUserInfo.programsMapped[b])
				}
				console.log("this.programIdArray", this.programIdArray);
			}
		}
		console.log("this.dropDownPrograms", this.dropDownPrograms);
		this.programForm = this.formBuilder.group({
			"program": "",
			"graphType": "yearly"
		});
		if (this.dropDownPrograms[0]) {
			this.programForm.patchValue({
				"program": this.dropDownPrograms[0].programId,
				"graphType": "yearly"
			})
		}

	}
	ngOnInit() {
		this.getComparisonReport();
	};
	ngOnChanges() { };
	ngOnDestroy() { };

	public programIdArrayToCompare: any[] = ["PR1504603897855", "PR1493824208902"]
	public comparisonUnit: String = "monthly";

	getComparisonReport() {
		console.log("this.programIdArray", this.programForm.value.graphType);
		for (var a = 0; a < this.dropDownPrograms.length; a++) {
			if (this.programForm.value.program === this.dropDownPrograms[a].programId) {
				this.secondProgram = this.dropDownPrograms[a].programName
			}
		}
		console.log("#############################################################################");
		if (this.programForm.value.graphType === "yearly") {
			this.yearlyGraph = true;
			this.monthlyGraph = false;
			this.quarterlyGraph = false;
		}
		if (this.programForm.value.graphType === "monthly") {
			this.yearlyGraph = false;
			this.monthlyGraph = true;
			this.quarterlyGraph = false;
		}
		if (this.programForm.value.graphType === "quarterly") {
			this.yearlyGraph = false;
			this.monthlyGraph = false;
			this.quarterlyGraph = true;
		}
		var frontendUserInfo = this.configService.getFrontEndUserInfo();
		this.programIdArray[1] = this.programForm.value.program;
		console.log("this.programIdArray", this.programIdArray);
		if (frontendUserInfo === undefined) {
			frontendUserInfo = {
				userId: this.configService.getLoggedInUserInfo().userId
			}
		}
		var reqObj = {};
		if (this.graphTypeFlag.isMySalesGraph === true) {
			if (this.configService.getloggedInBEInfo()) {

				reqObj = {
					"programId": this.loggedInProgramInfo.programId,
					"frontendUserInfo": frontendUserInfo,
					"clientId": this.loggedInProgramInfo.clientId,
					"serviceType": "programOperations",
					"mySalesGraph": true,
					"queryParams": {
						"creditPartyId": this.configService.getloggedInBEInfo().businessId,
						"programIdArray": this.programIdArray,
						"comparisionUnit": this.programForm.value.graphType,
					}
				}
			} else if (this.configService.getClientUserInfo()) {
				reqObj = {
					"programId": this.loggedInProgramInfo.programId,
					"frontendUserInfo": frontendUserInfo,
					"clientId": this.loggedInProgramInfo.clientId,
					"serviceType": "programOperations",
					"mySalesGraph": true,
					"queryParams": {
						"creditPartyId": this.configService.getClientUserInfo().clientUserId,
						"programIdArray": this.programIdArray,
						"comparisionUnit": this.programForm.value.graphType
					}
				}
			}
		} else if (this.graphTypeFlag.isChannelGraph === true) {
			if (this.configService.getloggedInBEInfo()) {

				reqObj = {
					"programId": this.loggedInProgramInfo.programId,
					"frontendUserInfo": frontendUserInfo,
					"clientId": this.loggedInProgramInfo.clientId,
					"serviceType": "programOperations",
					"myChannelsGraph": true,
					"queryParams": {
						"debitPartyId": this.configService.getloggedInBEInfo().businessId,
						"programIdArray": this.programIdArray,
						"comparisionUnit": this.programForm.value.graphType
					}
				}
			} else if (this.configService.getClientUserInfo()) {
				reqObj = {
					"programId": this.loggedInProgramInfo.programId,
					"frontendUserInfo": frontendUserInfo,
					"clientId": this.loggedInProgramInfo.clientId,
					"serviceType": "programOperations",
					"myChannelsGraph": true,
					"queryParams": {
						"debitPartyId": this.configService.getClientUserInfo().clientUserId,
						"programIdArray": this.programIdArray,
						"comparisionUnit": this.programForm.value.graphType
					}
				}
			}
		}
		console.log("reqAAA", reqObj);
		this.showLoader = true;
		this.dashboardService.getComparisonReport(reqObj)
			.subscribe(
				(responseObject) => {
					console.log(responseObject)
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					console.log("claim responce", responseObject);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_SUCCESS:

							this.showCharts(responseObject.result);
							break;
						case responseCodes.RESP_FAIL:
							this.feedbackMessageComponent.updateMessage(true, "Records Not Found", "alert-danger");
							break;
						case responseCodes.RESP_AUTH_FAIL:
						case responseCodes.RESP_ROLLBACK_ERROR:
						case responseCodes.RESP_SERVER_ERROR:
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
				}
			);
	}


	private serialChart1: any;
	private serialChart2: any;
	private serialChart3: any;

	showCharts(result) {
		console.log("showcharts")

		this.getDataProvider(result, this.comparisonUnit)

		this.serialChart1 = this.AmCharts1.makeChart("piechartdiv1",
			{
				"type": "serial",
				"categoryField": "category",
				"autoMarginOffset": 30,
				"marginRight": 70,
				"marginTop": 40,
				"startDuration": 1,
				"fontSize": 13,
				"theme": "patterns",
				"categoryAxis": {
					"gridPosition": "start"
				},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-1",
						"title": this.loggedInProgramInfo.programName,
						"type": "column",
						"valueField": "program_1"
					},
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-2",
						"title": this.secondProgram ? this.secondProgram : "No program",
						"type": "column",
						"valueField": "program_2"
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"title": "Sales (₹)"
					}
				],
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true
				},
				"titles": [],
				"dataProvider": this.data1
			});

		this.serialChart2 = this.AmCharts1.makeChart("piechartdiv2",
			{
				"type": "serial",
				"categoryField": "category",
				"autoMarginOffset": 30,
				"marginRight": 70,
				"marginTop": 40,
				"startDuration": 1,
				"fontSize": 13,
				"theme": "patterns",
				"categoryAxis": {
					"gridPosition": "start"
				},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-1",
						"title": this.loggedInProgramInfo.programName,
						"type": "column",
						"valueField": "program_1"
					},
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-2",
						"title": this.secondProgram ? this.secondProgram : "No program",
						"type": "column",
						"valueField": "program_2"
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"title": "SKU"
					}
				],
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true
				},
				"titles": [],
				"dataProvider": this.data2
			});

		this.serialChart3 = this.AmCharts1.makeChart("piechartdiv3",
			{
				"type": "serial",
				"categoryField": "category",
				"autoMarginOffset": 30,
				"marginRight": 70,
				"marginTop": 40,
				"startDuration": 1,
				"fontSize": 13,
				"theme": "patterns",
				"categoryAxis": {
					"gridPosition": "start"
				},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-1",
						"title": this.loggedInProgramInfo.programName,
						"type": "column",
						"valueField": "program_1"
					},
					{
						"balloonText": "[[title]] of [[category]]:[[value]]",
						"fillAlphas": 0.9,
						"id": "AmGraph-2",
						"title": this.secondProgram ? this.secondProgram : "No program",
						"type": "column",
						"valueField": "program_2"
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"title": "Points"
					}
				],
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true
				},
				"titles": [],
				"dataProvider": this.data3
			});
	}


	getDataProvider(result, comparisonUnit) {
		this.data1 = [];
		this.data2 = [];
		this.data3 = [];
		var category: any[] = [];
		switch (this.programForm.value.graphType) {
			case "yearly":
				this.data1.push({
					"category": "Yearly",
					"program_1": 0,
					"program_2": 0
				});

				this.data2.push({
					"category": "Yearly",
					"program_1": 0,
					"program_2": 0
				});

				this.data3.push({
					"category": "Yearly",
					"program_1": 0,
					"program_2": 0
				});
				if (result[0] && result[1]) {
					this.data1[0] = {
						"category": "Yearly",
						"program_1": result[1].totalSales,
						"program_2": result[0].totalSales
					};

					this.data2[0] = {
						"category": "Yearly",
						"program_1": result[1].totalSKU,
						"program_2": result[0].totalSKU
					};

					this.data3[0] = {
						"category": "Yearly",
						"program_1": result[1].totalPoints,
						"program_2": result[0].totalPoints
					};
				} else if (result[0] !== undefined && result[1] === undefined) {
					this.data1[0] = {
						"category": "Yearly",
						"program_1": result[0].totalSales,
						"program_2": 0
					};

					this.data2[0] = {
						"category": "Yearly",
						"program_1": result[0].totalSKU,
						"program_2": 0
					};

					this.data3[0] = {
						"category": "Yearly",
						"program_1": result[0].totalPoints,
						"program_2": 0
					};
				} else if (result[0] === undefined && result[1] !== undefined) {
					this.data1[0] = {
						"category": "Yearly",
						"program_1": 0,
						"program_2": result[1].totalSales
					};

					this.data2[0] = {
						"category": "Yearly",
						"program_1": 0,
						"program_2": result[1].totalSKU
					};

					this.data3[0] = {
						"category": "Yearly",
						"program_1": 0,
						"program_2": result[1].totalPoints
					};
				}

				break;

			case "quarterly":
				category = ["Jan - Mar", "Apr - Jun", "Jul - Sep", "Oct - Dec"]

				for (var i = 0; i < category.length; i++) {
					this.data1.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});

					this.data2.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});

					this.data3.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});
				}

				for (var i = 0; i < category.length; i++) {
					for (var j = 0; j < result.length; j++) {
						if (this.programIdArray[0]) {
							if (result[j]._id.quarter == i + 1 && result[j]._id.programId == this.programIdArray[0]) {
								this.data1[i].program_1 = result[j].totalSales;
								this.data2[i].program_1 = result[j].totalSKU;
								this.data3[i].program_1 = result[j].totalPoints;
							}
						}
						if (this.programIdArray[1]) {
							if (result[j]._id.quarter == i + 1 && result[j]._id.programId == this.programIdArray[1]) {
								this.data1[i].program_2 = result[j].totalSales;
								this.data2[i].program_2 = result[j].totalSKU;
								this.data3[i].program_2 = result[j].totalPoints;
							}
						}

					}
				}
				break;

			case "monthly":
				category = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

				for (var i = 0; i < category.length; i++) {
					console.log("month", category[i]);
					this.data1.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});

					this.data2.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});

					this.data3.push({
						"category": category[i],
						"program_1": 0,
						"program_2": 0
					});
				}

				for (var i = 0; i < category.length; i++) {
					for (var j = 0; j < result.length; j++) {
						if (result[j]._id.month == i + 1 && result[j]._id.programId == this.programIdArray[0]) {
							this.data1[i].program_1 = result[j].totalSales;
							this.data2[i].program_1 = result[j].totalSKU;
							this.data3[i].program_1 = result[j].totalPoints;
						}
						if (result[j]._id.month == i + 1 && result[j]._id.programId == this.programIdArray[1]) {
							this.data1[i].program_2 = result[j].totalSales;
							this.data2[i].program_2 = result[j].totalSKU;
							this.data3[i].program_2 = result[j].totalPoints;
						}
					}
				}


				break;

			default:
				break;

		}
	}
	routing1() {
		this.router.navigate(['../home/dashboard']);
	}
};
