
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class DashboardService {
	loginInfo: any;
	private headers;
	private options;
	private frontendUserInfo: any;
	private userInfo;
	private requestObj: any;
	private clientId = this.configService.getloggedInProgramInfo().clientId;
	private programId = this.configService.getloggedInProgramInfo().programId;
	public childsIdArray: any = [];

	constructor(private http: Http,
		private configService: ConfigService) {

		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.configService
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.userInfo = this.configService.getLoggedInUserInfo();
		this.requestObj = {
			"programId": this.configService.getloggedInProgramInfo().programId,
			"clientId": this.configService.getloggedInProgramInfo().clientId
		}


	}



	/*
	*  METHOD         : getUserSalesClaimsDetails()
	*  DESCRIPTION    : Called from add claims page on form submission
	*                  Adds new claims to the database
	*/
	getUserSalesClaimsDetails(reqObj): any {
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;

		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {
		}
		reqObj.serviceType = "programOperations";
		reqObj.frontendUserInfo = frontEndInfo;
		console.log(reqObj);
		let url = this.configService.getApiUrls().getUserSalesClaimsDetails;
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}



	getProgramUser(reqObj): any {
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {
			frontEndInfo["userId"] = userIds;
		}
		reqObj.serviceType = "programSetup";
		reqObj.frontendUserInfo = frontEndInfo;
		console.log(reqObj);
		let url = this.configService.getApiUrls().getProgramUser;
		reqObj.serviceType = "programSetup";
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}



	/*******************************	API CALLS 	***********************************/
    /*
        METHOD         : getAllSecondarySales()
        DESCRIPTION    : Called from Claims list page inside ngOnInit()
                         Fetches all secondary sales i.e. claims list
    */
	getAllSecondarySales(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "programOperations";
		let url = this
			.configService
			.getApiUrls()
			.getAllSecondarySales;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	/*******************************	API CALLS 	***********************************/
    /*
	METHOD         : getAllTopFives()
	DESCRIPTION    : For fetching the top fives SKU Retailer and Distributors
    */
	getAllTopFives(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "programOperations";
		let url = this
			.configService
			.getApiUrls()
			.getAllTopFives;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getTopRetailersProductwise(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "programOperations";
		let url = this
			.configService
			.getApiUrls()
			.getTopRetailersProductwise;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	getProgramRegions(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}

		delete frontEndInfo.appType;

		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.clientId = this.clientId;
		reqobj.programId = this.programId;


		reqobj.serviceType = "programSetup";
		let url = this
			.configService
			.getApiUrls()
			.getProgramRegions;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	getMyChannels(requestObject): any {
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		requestObject.serviceType = "programSetup";
		requestObject.clientId = this.clientId;
		requestObject.programId = this.programId;
		var userIds = this.userInfo.userId;
		requestObject.userInfo = {};
		requestObject.userInfo = {
			"programId": this.configService.getloggedInProgramInfo().programId,
			"programLogin": true,
			"userId": this.configService.getloggedInProgramUser().programUserId,
			"userType": ""
		}
		this.loginInfo = this.configService.getLoggedInUserInfo();
		console.log("loginInfo>>>>>>", this.loginInfo);
		if (this.loginInfo.clientId === undefined || this.loginInfo.clientId === null) {
			requestObject.userInfo.userType = "ChannelPartner"
		} else {
			requestObject.userInfo.userType = "ClientUser"
		}
		console.log("req<><><><><>", requestObject.userInfo.userType);
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		let reqObj = requestObject;
		reqObj.frontendUserInfo = frontEndInfo;

		let url = this.configService.getApiUrls().getMyChannels;
		console.log("reqObj for channels", reqObj);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	findIfChannelExist(requestObject): any {
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		requestObject.serviceType = "programSetup";
		requestObject.clientId = this.clientId;
		requestObject.programId = this.programId;
		var userIds = this.userInfo.userId;
		requestObject.userInfo = {};
		requestObject.userInfo = {
			"programId": this.configService.getloggedInProgramInfo().programId,
			"programLogin": true,
			"userId": this.configService.getloggedInProgramUser().programUserId,
			"userType": ""
		}
		this.loginInfo = this.configService.getLoggedInUserInfo();
		console.log("loginInfo>>>>>>", this.loginInfo);
		if (this.loginInfo.clientId === undefined || this.loginInfo.clientId === null) {
			requestObject.userInfo.userType = "ChannelPartner"
		} else {
			requestObject.userInfo.userType = "ClientUser"
		}
		console.log("req<><><><><>", requestObject.userInfo.userType);
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		let reqObj = requestObject;
		reqObj.frontendUserInfo = frontEndInfo;

		let url = this.configService.getApiUrls().findIfChannelExist;
		console.log("reqObj for channels", reqObj);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}
	getBusinessEntity(requestObject: any): any {
		let url = this.configService.getApiUrls().getBusinessEntity;

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			requestObject.frontendUserInfo = frontEndInfo;
		} else {
			requestObject.frontendUserInfo = {};
		}

		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		return this.http.post(url, requestObject, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("error", error);
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getMySecondarySales(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "programOperations";
		let url = this
			.configService
			.getApiUrls()
			.getAllSecondarySales;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getChildSecondarySales(reqobj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "programOperations";
		let url = this
			.configService
			.getApiUrls()
			.getAllSecondarySales;

		return this
			.http
			.post(url, reqobj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	/************************************  Get Channel Redemptions  ************************************* */

	getAllOrders(reqObj): any {

		if (this.configService.getFrontEndUserInfo() != null || this.configService.getFrontEndUserInfo() != undefined) {
			reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
		}
		else {
			reqObj.frontendUserInfo = {};
		}
		let url = this.configService.getApiUrls().getChannelOrders;

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	/***************************************  End ****************************************************** */

	/***************************************   Update Channel Redemptions   ******************************* */

	updateOrder(requestObject) {
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

		console.log("get orders req obj ", requestObject);
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });

		let url = this.configService.getApiUrls().updateOrder;
		console.log("before entities before api call", options);

		return this.http.post(url, requestObject, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}

	/********************************************   End   ************************************************* */

	/***************************************  Get Regions Performance   ******************************* */

	getRegionsPerformance(requestObject) {
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		requestObject.serviceType = "programSetup";
		console.log("get orders req obj ", requestObject);
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });

		let url = this.configService.getApiUrls().getRegionsPerformance;
		console.log("before entities before api call", options);

		return this.http.post(url, requestObject, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}

	/********************************************   End   ************************************************* */

	getClientUser(reqobj): any {
		reqobj.serviceType = "client";
		reqobj.appType = "webApp";
		reqobj.clientId = this.configService.getprogramInfo().clientId;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			reqobj.frontendUserInfo = frontEndInfo;
		} else {
			reqobj.frontendUserInfo = {};
		}
		console.log("this.configService.getloggedInProgramUser()", this.configService.getloggedInProgramUser());
		reqobj.clientUserId = this.configService.getloggedInProgramUser().programUserId;
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});

		let options = new RequestOptions({ headers: headers });
		let url = this.configService.getApiUrls().getClientUser;

		return this.http.post(url, reqobj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}




	getComparisonReport(reqObj): any {
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;

		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {
		}

		reqObj.serviceType = "programOperations";
		reqObj.frontendUserInfo = frontEndInfo;
		var url = this.configService.getApiUrls().getComparisionReport;
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	};

	getAllProgramFromMaster(requestObject): any {
		var userIds = this.userInfo.userId;
		console.log("frontEND", this.configService.getFrontEndUserInfo());
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		if (requestObject.frontendUserInfo == undefined) {
			requestObject.frontendUserInfo = {
				"userId": userIds
			}
		} else {
		}
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("REQUEST OBJECT: ", requestObject);
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().getPrograms;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("INSIDE MAP : ", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("INSIDE CATCH");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	getTodaysBirthdayCount(reqobj): any {
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let url = this.configService.getApiUrls().getTodaysBirthdayCount;

		return this.http.post(url, reqobj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}

	getLeaderboard(requestObject): any {
		var userIds = this.userInfo.userId;
		console.log("frontEND", this.configService.getFrontEndUserInfo());
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		if (requestObject.frontendUserInfo == undefined) {
			requestObject.frontendUserInfo = {
				"userId": userIds
			}
		} else {
		}
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("REQUEST OBJECT: ", requestObject);
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().getLeaderBoard;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("INSIDE MAP : ", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("INSIDE CATCH");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}



	countLead(obj) {
		let url = this.configService.getApiUrls().countLead;
		var request = obj;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			request.frontendUserInfo = frontEndInfo;
		} else {
			request.frontendUserInfo = {};
		}
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("url" + JSON.stringify(url));
		return this.http.post(url, obj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	};


	getEvents(obj) {
		let url = this.configService.getApiUrls().getEvents;
		var request = obj;
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			request.frontendUserInfo = frontEndInfo;
		} else {
			request.frontendUserInfo = {};
		}
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("url" + JSON.stringify(url));
		return this.http.post(url, obj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	};




	/*
	*  METHOD         : getDistributorSalesDetails
	*  DESCRIPTION    : To Fetch Distributor Total Sales
	*                  
	*/
	getDistributorSalesDetails(reqObj): any {
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;

		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {
		}
		reqObj.serviceType = "programOperations";
		reqObj.frontendUserInfo = frontEndInfo;
		console.log(reqObj);
		let url = this.configService.getApiUrls().getDistributorSalesDetails;
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


}
