/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule, Component, OnInit, OnChanges, OnDestroy, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, NavigationExtras } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfigService } from "../shared-services/config.service";
import { DashboardService } from "./dashboard.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { DashBoardApprovalsRedemptionList } from "./approvals-redemption-list";
import { DashboardRedemptionDetailComponent } from "./dashboard-redemption-detail.component";
import { AppService } from "../app.service";
import { ISubscription } from "rxjs/Subscription";
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { LeadManagementService } from "../lead-management-system/lead-management.service";
import { CsvService } from "../shared-services/csv.service";
import { AppComponent } from '../app.component';
import { HomeService } from '../home/home.service';
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from '../home/header/header.component';
import { SpecialOffersService } from "../special-offers/special-offers.service";
import { CelebrationService } from "../celebrations/celebrations.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './dashboard-home.component.html',
	styleUrls: ['./dashboard-home.component.scss', '../app.component.scss'],
	providers: [DashboardService,Device]


})
export class DashboardHomeComponent implements OnInit, OnChanges, OnDestroy {
	retailersUserIdArray: any = [];
	public engineerReqCount: number = 0;
	contestObj1: { imagePath: string; label: string; text: string; };
	businessEntity: any = [];
	cityArray: any;
	stateArray: any;
	requestObj: any = {};
	contestObj: any = {};
	public dashboardPoints = 0;
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	panIndia: string;
	public currentMonthLeadCountForPending = 0;
	leadCountForPending: any;
	leaderboardRecordNotfound: boolean;
	allOverTableData: any = [];
	allOverDistributorTableData: any = [];
	allOverDealerTableData: any = [];

	public tableRegionData: any = [];
	allOverData: any = [];
	regionData: any = [];
	leaderboardRegion: any;
	public showDealerProduct1: boolean = true;
	public showDealerProduct2: boolean = false;
	public showDistributorProduct1: boolean = true;
	public showDistributorProduct2: boolean = false;
	public showDistributorProduct3: boolean = false;
	public showDistributorProduct4: boolean = false;
	public showDistributorProduct5: boolean = false;

	showDelaerProduct: boolean;
	remainingTarget: number;
	achieved: number = 0;
	dealerSalesss: number;
	achievedPercentage: number;
	public sliders1: any = [];
	public todaysBirthday: any = [];
	public totalDistributorSales: any = 0;
	public totalDealerSalesforMonth = 0;
	public totalDealerSales: any = 0;
	public lastMonthLead = 0;
	public currentMonthLeadCount = 0;
	eventGallery: boolean;
	eventslist: any;
	private leadCount: any[];
	public monthlygoal: number = 0;
	printingArray: any[];
	headerArray: string[];
	public isLeadManagementAvailable: boolean;
	public superStarsOfTheMonth: any = [];
	public showLeadApproval: boolean = false;
	public birthdayCount = 0;
	leadslist: any;
	showLeadsTable: boolean;
	selectedTab: any;
	public showDetails: boolean = false;
	public users: any = [];
	public leaderboardInfo: any = [];
	public leaderboardHeaders: any = [];
	public showBirthdayPopUp: boolean;
	public clientInfo: any = {};
	public mySalesRecordNotFound: boolean = false;
	public programArray: any;
	public programsMapped: any[];
	private feedbackFlag: boolean = false;
	private totalRedeems: number = 0;
	private pendingRedeems: number = 0;
	private subscribe: ISubscription;
	public dashboardInfo: any = {};
	private myClaimsInfo: any;
	private myClaimsInfo2: any;
	private ordersList: any = [];
	public userSalesClaimsDetails: any;
	private showLoader: boolean = false;
	private dataFound: boolean = false;
	private userDataFound: boolean = false;
	public doughnutChartLabels: string[] = ['In Progress', 'Approved', 'Rejected'];
	public doughnutChartData: number[];
	public goalsPositive: boolean = false;
	public goalsNegitive: boolean = false;
	public dataSet: any[];
	public showClaims: boolean = false;
	public showClaimsTable: boolean = false;
	public showRRTable: boolean = false;
	public showReturnsTable: boolean = false;
	private isClient: boolean;
	private loginInfo: any;
	private topFives: any;
	private channelsFound: boolean = false;
	private noChannelsFound: boolean = false;
	private interval: number;
	private fourth: number;
	private third: number;
	private second: number;
	private first: number;
	public math: any;
	private invoiceNumber: any;
	public regionPerformanceArr: any = [];
	public totalNoOfInvoices = 0;
	public allTotalPointsEarned = 0;
	public rewardGalleryRoute: any;
	public birthdayCss = "background-color: #000;background-image: none;max-height: 100vh;overflow-y: scroll;z-index: 10000;position: fixed;width: 70%;height: 70%;top: 20%;left: 15%;text-align: center;opacity:0;transition: opacity 0.5s linear;";

	public options: {
	};
	private requestObjectSales: any = {};
	private creditPartyId: string;
	private programInfo: any = {};
	private channelsInfo: any;

	private businessInfo: any = {};
	private limit: number = 10;
	private active: string = "claims";
	private todayDate: string;

	public pending: number;
	public approved: number;
	public rejected: number;
	public pendingNo: number;
	public approvedNo: number;
	public rejectedNo: number;
	public noSales: boolean = false;
	private doughnutChartColors: any[] = [{ backgroundColor: ["#498DA7", "#B3CC66", "#FF6666"] }];
	private colours: string[] = ["#498DA7", "#B3CC66", "#FF6666"];
	public doughnutChartType: string = 'doughnut';
	public loginProgramUserType: string;
	private programUserInfo: any;
	private programRoleInfo: any;
	private showClaimsUI: boolean = false;
	private showPointsUI: boolean = false;
	private showApprovalUI: boolean = false;
	private pointsAvailableToRedeem: number;
	points: string;
	private isEligibleForPointConversion: boolean;
	private chart: any;
	private chart2: any;
	private piechart: any;
	private reqObjGetRedemptionOfChannels: any;
	private topSKUS: any;
	private topDistributors: any;
	private topRetailers: any = [];
	private showTopFives: boolean = false;
	private isNotCodeBased: boolean = true;
	private serialChart: any;
	public mtdValue: number = 0;
	public ytdValue: number = 0;
	public percentAchieved: number = 0;
	
	
	public period:any;

	// 27-01-2018 Temp hardcoded for distributor - Ajeet - added for demo added the flag 
	public isDistributor: boolean = false;

	public alerts: Array<any> = [];
	public sliders: Array<any> = [];


	public userRegionInfo: any = {};

	public regionDataTable: Array<any> = [];

	public qualifiedTonnage: any ;
	public pendingTonnage: any;
	public rejectedTonnage: any;
	public previousQualifiedTonnage;

	public pointsReedemed: any=0;
	public pointsRemaining: any=0;


	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		public appService: AppService,
		private dashboardService: DashboardService,
		private formBuilder: FormBuilder,
		public dialog: MdDialog,
		private stringService: StringService,
		private AmCharts: AmChartsService,
		private AmCharts1: AmChartsService,
		public appComponent: AppComponent,
		private leadService: LeadManagementService,
		private _csvService: CsvService,
		private homeService: HomeService,
		public sidebar: SidebarComponent,
		public header: HeaderComponent,
		private specialOffersService: SpecialOffersService,
		private celebrationService: CelebrationService,
		private device: Device,
	) {


		this.programUserInfo = configService.getloggedInProgramUser();

		if (this.programUserInfo.programRole == this.stringService.getStaticContents().jswDistributorProgramRole) {
			if (this.programUserInfo.profileId === this.stringService.getStaticContents().jswDistrubutorProfileid) {

				this.router.navigate(["../../home/leads-list"]);
			}
			else {

			}
		}
		else {

		}



		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}


		let reqObj: any = {};
		let reqObjSales: any = {};
		this.active = "claims";
		this.showClaimsTable = true;
		this.showRRTable = false;
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
		reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
		reqObj.programId = this.configService.getloggedInProgramInfo().programId;
		reqObj.programUserId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo();

		reqObjSales.clientId = this.configService.getloggedInProgramInfo().clientId;
		reqObjSales.programId = this.configService.getloggedInProgramInfo().programId;
		reqObjSales.userId = this.configService.getloggedInProgramUser().programUserId;
		console.log(this.configService.getloggedInProgramInfo());

		this.programInfo = configService.getloggedInProgramInfo();
		this.limit = 5;

		console.log("Request Object Sales:", reqObjSales);
		console.log("Request Object : ", reqObj);
		this.getProgramUser(reqObj);
		this.getUserSalesClaimsDetails(reqObjSales);


		this.isEligibleForPointConversion = false;
		console.log("dashboard subscribed called constructor");
		this.configService.setRewardGalleryFlag(false);
		this.invoiceNumber = [];
		this.math = Math;
		this.loginInfo = this.configService.getLoggedInUserInfo();
		this.businessInfo = this.configService.getloggedInBEInfo();
		this.clientInfo = this.configService.getClientUserInfo();
		console.log("this.businessInfo", this.businessInfo);
		console.log("this.clientInfo", this.clientInfo);
		this.programsMapped = [];

		if (this.loginInfo !== undefined && this.loginInfo !== null) {
			if (this.loginInfo.businessEntityInfo) {

				for (var a = 0; a < this.loginInfo.businessEntityInfo[0].programsMapped.length; a++) {
					this.programsMapped.push(this.loginInfo.businessEntityInfo[0].programsMapped[a].programId)
				}
			} else if (this.loginInfo.programsMapped) {
				for (var a = 0; a < this.loginInfo.programsMapped.length; a++) {
					this.programsMapped.push(this.loginInfo.programsMapped[a].programId)
				}
			}

		}
		console.log("programs^^^", this.programUserInfo.targetnAchievementDetails);

		console.log("ProfileId", this.programUserInfo);
		// ***********************************************Added by ravi ********************************************
		// var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
		// var currentDate = new Date();
		// var currentMonth = currentDate.getMonth();
		// var currentYear = currentDate.getFullYear();
		// this.period = months[currentMonth] + currentYear;

		// if(this.programUserInfo.targetnAchievementDetails != undefined || this.programUserInfo.targetnAchievementDetails != null){
		// 	for(let j = 0;j < this.programUserInfo.targetnAchievementDetails.length;j++){
		// 		this.ytdValue = this.ytdValue + this.programUserInfo.targetnAchievementDetails[j].PointCalculated + this.programUserInfo.targetnAchievementDetails[j].pointPending;
		// 		if(this.programUserInfo.targetnAchievementDetails[j].targetPeriodName === this.period){
		// 			this.mtdValue = this.mtdValue + this.programUserInfo.targetnAchievementDetails[j].PointCalculated + this.programUserInfo.targetnAchievementDetails[j].pointPending;
					
		// 		}
		// 	}
		// }
		// this.percentAchieved = Math.round((this.ytdValue / this.programUserInfo.annualTarget) * 100); 
		// console.log("mtd sale,ytd sale ======",this.mtdValue , this.ytdValue);
		

		// ***********************************************Added by ravi ********************************************


		this.loginProgramUserType = this.programUserInfo.userType;
		console.log("loginProgramUserType", this.loginProgramUserType);
		this.pointsAvailableToRedeem = this.programUserInfo.totalPointsAvailableForRedeem;
		console.log("ProgramUserInfo", this.programUserInfo);
		this.monthlygoal = this.programUserInfo.annualTarget / 12;
		let programUser = this.programUserInfo.programRoleInfo;

		console.log("user allowed to show claims.....", this.showClaimsUI);
		console.log("userdata data found to ", this.userDataFound);
		this.loginInfo = configService.getLoggedInUserInfo();

		console.log("POINTS", this.points);
		this.programInfo = this.configService.getloggedInProgramInfo();
		var date = new Date();
		this.todayDate = date.toDateString();



		if (this.programInfo.claimEntryType === "code") {
			this.isNotCodeBased = false;
		}

		this.loginInfo = configService.getLoggedInUserInfo();


		console.log("Program User Info is", configService.getloggedInProgramUser());
		console.log("Login User Info is", this.loginInfo);



		this.businessInfo = configService.getloggedInBEInfo();
		console.log('BE INFO', this.businessInfo);

		console.log('AJJJJJJJJ   ', this.programUserInfo.programRole);



		// ----- 27-01-2018 temp hardcoded for demo purpose
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
			let requestObject = {
				programId: this.stringService.getStaticContents().jswprogramId,
				clientId: this.stringService.getStaticContents().jswClientId,
				"limit": 10,
				"sortType": "top"

			}
			this.getAllTopFives(requestObject);
			// console.log("AJJJJJJJJ this.getAllTopFives(requestObject)", this.getAllTopFives(requestObject));
		}




		if (this.businessInfo != null) {
			console.log('in be', this.businessInfo);

			this.creditPartyId = this.businessInfo.businessId;
			console.log('cp', this.creditPartyId);
			this.requestObjectSales = {
				programId: this.programInfo.programId,
				clientId: this.programInfo.clientId,
				frontendUserInfo: {},
				skip: 0,
				limit: this.limit,
				sort:
					{ createdAt: -1 }
			}

		}
		else {
			console.log('in ce');
			this.creditPartyId = this.configService.getloggedInProgramUser().programUserId;
			this.requestObjectSales = {
				programId: this.programInfo.programId,
				clientId: this.programInfo.clientId,
				frontendUserInfo: {},
				skip: 0,
				debitPartyId: this.creditPartyId,
				limit: this.limit,
				sort:
					{ createdAt: -1 }
			}
		}


		var staticData = this.stringService.getfieldLabels();
		var terminology = this.configService.getTerminology();
		if (terminology == "") {
			console.log("terminology not set in dashbord");
		}
		console.log("static String Service Data", staticData, terminology);



		// reqObj to get channel Redemptions
		this.reqObjGetRedemptionOfChannels = {
			programId: this.programInfo.programId,
			clientId: this.programInfo.clientId,
			frontendUserInfo: {},
			skip: 0,
			limit: 5
		}


		// code to show approvals tab
		if (this.programUserInfo) {
			this.programRoleInfo = programUser;

			console.log("PROGRAM ROLE INFO", this.programRoleInfo);

			if (this.programRoleInfo) {
				if (this.programRoleInfo.canMakeClaims == true) {
					console.log("user can make claims.....");
					this.showClaimsUI = true;
				} else {
					console.log("user not allowed to show claims.....");
					this.showClaimsUI = false;
				}

				if (this.programRoleInfo.canEarnPoints == true) {
					this.showPointsUI = true;
				} else {
					this.showPointsUI = false;
				}
				if (this.programRoleInfo.canApproveClaims || this.programRoleInfo.canApproveRedemptions) {
					if (this.programRoleInfo.canApproveClaims === false) {
						this.active = "redem";
						this.showClaimsTable = false;
						this.showReturnsTable = false;
						this.showRRTable = true;
					}
					this.showApprovalUI = true;
				} else {
					this.showApprovalUI = false;
				}
			}
		} else {
			this.showClaimsUI = true;
			this.showPointsUI = true;
			this.showApprovalUI = true;
		}

		// CHECK FOR ELIGIBILITY FOR POINT CONVERSION
		if (this.programInfo.useGlobalRewardGallery) {
			this.isEligibleForPointConversion = false;
		} else {
			if (this.programInfo.allowStarPointsConversion) {
				this.isEligibleForPointConversion = true;
			} else {
				this.isEligibleForPointConversion = false;
			}
		}

		//this.makeCharts();

		if (this.programRoleInfo.showProgramTiers === true && this.programInfo.programId === "PR1503824208902") {
			console.log("this.programRoleInfo.showProgramTiers", this.programRoleInfo.showProgramTiers);
			this.showProgramTiers();

		}

	}

	showProgramTiers() {
		console.log("showProgramTiers");
		this.appComponent.showHideProgramTiers();

	}

	routing() {

		if (this.programInfo.useGlobalRewardGallery) {
			this.router.navigate(['../home/globalrewardsgallery']);
		} else {
			this.router.navigate(['../home/rewardsgallery']);
		}
	}

	routing1() {
		if (this.isEligibleForPointConversion = true) {
			this.router.navigate(['../home/pointsconversion']);
		}
	}


	ngOnInit() {
		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Dashboard')
		}
		else{
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Dashboard");
					ga('send', 'pageview');
				}
			});
		}
		window.scrollTo(0, 0);
		this.sidebar.showMenu = '0';
		this.sidebar.addExpandClass('pages');
		this.sidebar.addExpandClass1('pages1');
		this.sidebar.addExpandClass2('pages2');
		this.sidebar.addExpandClass3('claims');
		this.sidebar.close();

		let getLoggedInUserInfo = this.configService.getLoggedInUserInfo();
		// console.log("getLoggedInUserInfo.mobileNumber", getLoggedInUserInfo.mobileNumber);
		console.log("this.device.device",this.device.device);
		if(this.device.device === "android" || this.device.device === "ios"){
			(<any>window).ga.setUserId(getLoggedInUserInfo.mobileNumber)
		} else{
			ga('set', 'userId', getLoggedInUserInfo.mobileNumber);
		}


		this.userRegionInfo.name = "";
		this.stringService.setImageFormats();
		this.getEvents(); 
		this.getNonTransactionalBoosters();
		this.getCelebrations();


		this.contestObj = {
			imagePath: 'assets/images/contestBannerNew.jpg',
			label: '',
			text: ''
		}
		this.contestObj1 = {
			imagePath: 'assets/images/Revised_banner.jpg',
			label: '',
			text: ''
		}
		this.sliders1 = [
			// {
			// 	imagePath: 'assets/slider_images/slider1.jpg',
			// 	label: 'First slide',
			// 	text:
			// 		'Marathon in Maleswaram - 2018'
			// },
			// {
			// 	imagePath: 'assets/slider_images/slider2.jpg',
			// 	label: 'Second slide',
			// 	text: 'Cooking Contest 2018'
			// },
			// {
			// 	imagePath: 'assets/slider_images/slider3.jpg',
			// 	label: 'Third slide',
			// 	text:
			// 		'Beauty Peagent Contest - 2018'
			// }
		]
		// if (this.programUserInfo.programRole !== this.stringService.getStaticContents().jswDistributorProgramRole) {
		// 	this.sliders1[0] = this.contestObj;
		// }
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole) {
			this.sliders1[0] = this.contestObj1;
		}
		console.log("dashboard subscribed called oninit");
		this.getAllProgramFromMaster();
		console.log("this.configService.getProgramUserInfo()", this.programUserInfo, "this.programInfo", this.programInfo.additionalConfigurations.isLeadManagementAvailable);
		console.log("this.showLeadApproval", this.showLeadApproval, "this.active", this.active);
		if (this.programInfo.additionalConfigurations.isLeadManagementAvailable === true) {
			console.log("in 1sssst if");
			this.isLeadManagementAvailable = true;
			if (this.programUserInfo.roleName === "Distributor" || this.programUserInfo.userType === "ClientUser") {
				console.log("in 2nnnnd if");
				this.getLead();

			}
		};
		console.log("this.showLeadApproval", this.showLeadApproval, "this.active", this.active);

		var todaysDate = new Date();
		var reqObj = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"kycStatus": "NA",
			"todaysDate": todaysDate.toISOString().slice(0, 10)

		}
		this.getTodaysBirthdayCount(reqObj);
		this.getTodaysBirthday();
		this.getDistributorSalesDetails();
		if (this.programRoleInfo.showHallOfFame === true) {
			console.log("Hall of Frame");
			this.getSuperStarsOfTheMonth();
		}
		if (this.programRoleInfo.showLeaderboard === true && (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole
		|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
		|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
		|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
		|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole)) {
			this.getLeaderboard();
		} else {
			this.leaderboardRecordNotfound = true
		}

		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole ||
			this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
			console.log("Inside Engineer");
			this.countLeadForQualified();
			// this.countLeadForPending();
			this.lastMonthLeadCount();
			// this.lastMonthLeadCountForPending();
		}

		this.getUserSalesClaimsDetailsforMonth();
		
		var todaysDate = new Date();

		var reqObj2 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"kycStatus": "NA",
			"todaysDate": todaysDate.toISOString().slice(0, 10)

		}
		this.header.getTodaysBirthdayCount(reqObj2);

		var reqObj1 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"programUserId": this.programUserInfo.programUserId,
			"serviceType": "programOperations",
			"isRead": false,


		}
		this.header.getUserNotifications(reqObj1);
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			this.requestObj = {
				"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
				"programId": this.configService.getprogramInfo().programId,
				"skip": 0,
				"limit": 10,
				"kycStatus": false,
				"sort": {
					"createdAt": -1
				},
				"userId": this.programUserInfo.programUserId
			}
			if (this.configService.getLoggedInUserInfo().statesMapped && this.configService.getLoggedInUserInfo().statesMapped.length > 0) {
				this.stateArray = this.configService.getLoggedInUserInfo().statesMapped;
				this.requestObj.stateArray = this.stateArray;
			}
			if (this.configService.getLoggedInUserInfo().citiesMapped && this.configService.getLoggedInUserInfo().citiesMapped.length > 0) {
				this.cityArray = this.configService.getLoggedInUserInfo().citiesMapped;
				this.requestObj.cityArray = this.cityArray;
			}

			console.log("this.configService.getLoggedInUserInfo().citiesMapped", this.configService.getLoggedInUserInfo().citiesMapped)
			console.log("this.configService.getLoggedInUserInfo().statesMapped", this.configService.getLoggedInUserInfo().statesMapped);
			console.log("Business Enirity Object", this.requestObj);
			this.getBusinessEntity(this.requestObj);
		}
		//end of ngOnInit

	}
	ngOnChanges() {
	}




	ngOnDestroy() {
		this.showLoader = false;
	}

	ngAfterViewInit() {
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
			const dom1: any = document.getElementById('HRC').classList.add('active');
		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
			const dom1: any = document.getElementById('dealerTMT').classList.add('active');
		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
			const dom1: any = document.getElementById('HRC').classList.add('active');
			const dom2: any = document.getElementById('dealerTMT').classList.add('active');
		}
	}

	claimsFiltering(claimsInfo) {
		for (let i = 0; i < claimsInfo.length; i++) {
			let invoiceDoc = claimsInfo[i].invoiceDocument;
			claimsInfo[i].invoiceDocument = invoiceDoc;
			claimsInfo[i].approvalStatus
			if (claimsInfo[i].approvalStatus == "pending") {
				claimsInfo[i].pointsEarned = "Pending";
			}
			else if (claimsInfo[i].approvalStatus == "rejected") {
				claimsInfo[i].pointsEarned = 0;
			}

		}

		this.myClaimsInfo = claimsInfo;


	}

	changeToClaims() {
		console.log("HERE");
		this.active = "claims";
		this.showClaimsTable = true;
		this.showReturnsTable = false;
		this.showRRTable = false;

	}
	changeToLeads() {
		console.log("HERE");
		this.active = "lead";
		this.showClaimsTable = false;
		this.showReturnsTable = false;
		this.showRRTable = false;
		this.showLeadsTable = true;
	}
	changeToRedem() {
		this.active = "redem";
		this.showClaimsTable = false;
		this.showReturnsTable = false;
		this.showRRTable = true;
	}
	changeToRet() {
		this.active = "returns";
		this.showClaimsTable = false;
		this.showReturnsTable = true;
		this.showRRTable = false;
	}

	routeToLeadDetails(lead) {
		this.configService.setLead(lead);
		this.configService.isDashboard = true;
		this.router.navigate(['./home/lead-details']);
	}


	/*
		METHOD         : getUserSalesClaimsDetails()
		DESCRIPTION    : To add a claim 
		*/
	getUserSalesClaimsDetails(reqObj) {

		console.log("Sales", reqObj);
		this.showLoader = true;

		this.dashboardService.getUserSalesClaimsDetails(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					console.log(responseCodes);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							// this.achieveTarget(0, 100);
							break;
						case responseCodes.RESP_SERVER_ERROR:
							// this.achieveTarget(0, 100);
							break;
						case responseCodes.RESP_SUCCESS:
							this.userSalesClaimsDetails = responseObject.result;
							console.log("use claim details RRR",this.userSalesClaimsDetails);
							this.totalDealerSales = this.userSalesClaimsDetails.salesDetails[0].totalSales;
							console.log("this.totalDealerSales", this.totalDealerSales, "	", this.programUserInfo.goalPoints);

							// this.achieveTarget(this.totalDealerSales, this.programUserInfo.goalPoints);


							this.userDataFound = true;
							console.log("userdata data found set to ", this.userDataFound, this.dataSet);
							this.noSales = true;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							// this.achieveTarget(0, 100);
							break;
						case responseCodes.RESP_FAIL:
							this.userSalesClaimsDetails = responseObject.result;

							this.userDataFound = true;
							// this.achieveTarget(0, 100);

							break;
						case responseCodes.RESP_ALREADY_EXIST:
							// this.achieveTarget(0, 100);
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}



	getUserSalesClaimsDetailsforMonth() {

		var endDate = new Date();
		var startDate = new Date();
		startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
		console.log("First Date of Month", startDate.toISOString().slice(0, 10));
		console.log("Todays Date", endDate.toISOString().slice(0, 10));

		this.showLoader = true;
		var obj = {

			"clientId": this.configService.getloggedInProgramInfo().clientId,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"userId": this.configService.getloggedInProgramUser().programUserId,
			startDate: startDate.toISOString().slice(0, 10),
			endDate: endDate.toISOString().slice(0, 10)
		}

		this.dashboardService.getUserSalesClaimsDetails(obj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					console.log(responseCodes);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.userSalesClaimsDetails = responseObject.result;
							this.totalDealerSalesforMonth = this.userSalesClaimsDetails.salesDetails[0].totalSales;

							console.log("THIS USER CLAIMS success", this.userSalesClaimsDetails);
							console.log("Dealer Current MonthSales", this.totalDealerSalesforMonth);

							this.pending = Math.round((this.userSalesClaimsDetails.claimDetails[0].noOfPendingClaims / this.userSalesClaimsDetails.claimDetails[0].noOfClaims) * 100);
							this.approved = Math.round((this.userSalesClaimsDetails.claimDetails[0].noOfApprovedClaims / this.userSalesClaimsDetails.claimDetails[0].noOfClaims) * 100);
							this.rejected = Math.round((this.userSalesClaimsDetails.claimDetails[0].noOfRejectedClaims / this.userSalesClaimsDetails.claimDetails[0].noOfClaims) * 100);

							this.pendingNo = this.userSalesClaimsDetails.claimDetails[0].noOfPendingClaims;
							this.approvedNo = this.userSalesClaimsDetails.claimDetails[0].noOfApprovedClaims;
							this.rejectedNo = this.userSalesClaimsDetails.claimDetails[0].noOfRejectedClaims;


							console.log(this.rejected);
							this.doughnutChartData = [this.pending,
							this.approved,
							this.rejected
							]


							this.dataSet = [{
								"borderWidth": [25, 25, 25],
								"data": this.doughnutChartData,
								"hoverBorderWidth": [10, 10, 10]
							}];

							this.userDataFound = true;
							console.log("userdata data found set to ", this.userDataFound, this.dataSet);
							this.noSales = true;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:

							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.userSalesClaimsDetails = responseObject.result;

							this.userDataFound = true;
							if (this.userSalesClaimsDetails.claimDetails[0].noOfClaims == 0) {

								this.pending = this.userSalesClaimsDetails.claimDetails[0].noOfPendingClaims;
								this.approved = this.userSalesClaimsDetails.claimDetails[0].noOfApprovedClaims;
								this.rejected = this.userSalesClaimsDetails.claimDetails[0].noOfRejectedClaims;

								this.pendingNo = this.userSalesClaimsDetails.claimDetails[0].noOfPendingClaims;
								this.approvedNo = this.userSalesClaimsDetails.claimDetails[0].noOfApprovedClaims;
								this.rejectedNo = this.userSalesClaimsDetails.claimDetails[0].noOfRejectedClaims;

								this.configService.setApproved(this.approved);
								this.configService.setPending(this.pending);
								this.configService.setRejected(this.rejected);

								this.doughnutChartData = [this.pending,
								this.approved,
								this.rejected
								]

								this.dataSet = [{
									"borderWidth": [25, 25, 25],
									"data": this.doughnutChartData,
									"hoverBorderWidth": [10, 10, 10]
								}];


								this.noSales = true;
							}

							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}







	/*
        METHOD         : getProgramUser()
        DESCRIPTION    : For fetching the user mapped to the program
    */
	getProgramUser(requestObject) {
		this.showLoader = true;
		this.dashboardService.getProgramUser(requestObject).subscribe((responseObject) => {
			this.showLoader = false;
			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
				case responseCodes.RESP_ROLLBACK_ERROR:
					break;
				case responseCodes.RESP_SERVER_ERROR:
					break;
				case responseCodes.RESP_SUCCESS:
					this.configService.setDashBoardInfo(responseObject.result[0]);
					this.dashboardInfo = responseObject.result[0];
					console.log("DashBoard Info", this.dashboardInfo);
					// ***********************************************Added by ravi ********************************************
					var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
					var currentDate = new Date();
					var currentMonth = currentDate.getMonth();
					var currentYear = currentDate.getFullYear();
					this.period = months[currentMonth] + currentYear;

					if(this.dashboardInfo.targetnAchievementDetails != undefined || this.dashboardInfo.targetnAchievementDetails != null){
						for(let j = 0;j < this.dashboardInfo.targetnAchievementDetails.length;j++){
							this.ytdValue = this.ytdValue + this.dashboardInfo.targetnAchievementDetails[j].PointCalculated + this.dashboardInfo.targetnAchievementDetails[j].pointPending;
							if(this.dashboardInfo.targetnAchievementDetails[j].targetPeriodName === this.period){
								this.mtdValue = this.mtdValue + this.dashboardInfo.targetnAchievementDetails[j].PointCalculated + this.dashboardInfo.targetnAchievementDetails[j].pointPending;
								
							}
						}
					}
					this.percentAchieved = Math.round((this.ytdValue / this.dashboardInfo.annualTarget) * 100); 
					this.achieveTarget(this.ytdValue,this.dashboardInfo.annualTarget);
					console.log("mtd sale,ytd sale ======",this.mtdValue , this.ytdValue);
					

					// ***********************************************Added by ravi ********************************************
					this.dashboardPoints = this.dashboardPoints + responseObject.result[0].totalPointsCredited;
					this.pointsReedemed = responseObject.result[0].pointsRedeemed;
					this.pointsRemaining = this.dashboardPoints - this.pointsReedemed;
					console.log("DashBoard Info", this.dashboardInfo);
					this.configService.setPoints(this.dashboardInfo.totalPointsAvailableForRedeem);
					this.dataFound = true;
					if (this.dashboardInfo.goalPoints != undefined || this.dashboardInfo.goalPoints != null) {
						var goals = this.dashboardInfo.goalPoints - (this.dashboardInfo.totalPointsEarned - this.dashboardInfo.pointsRedeemed);

						if (goals > 0) {
							this.goalsPositive = true;

						} else {
							this.goalsNegitive = true;
						}
					}
					console.log("Data Found : ", this.dataFound);
					break;
				case responseCodes.RESP_AUTH_FAIL:

					break;
				case responseCodes.RESP_FAIL:

					break;
				case responseCodes.RESP_ALREADY_EXIST:
					break;
			}
		}, err => {
			this.showLoader = false;
		});
	}


	getProgramRegions() {
		var requestObject = {};

		console.log("Program Regions ------------------ : ");


		this.showLoader = true;
		this.dashboardService.getProgramRegions(requestObject).subscribe((responseObject) => {
			this.showLoader = false;
			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
				case responseCodes.RESP_ROLLBACK_ERROR:
					break;
				case responseCodes.RESP_SERVER_ERROR:
					break;
				case responseCodes.RESP_SUCCESS:

					console.log("Program Regions ------------------ : ", responseObject.result.regions);
					console.log("Program Regions ------------------ : ", responseObject.result.states);



					// Logic to create Regions along with the states array
					var i, j;
					var finalRegionsArray = [];
					for (i = 0; i < responseObject.result.regions.length; i++) {
						var tmpObject: any = {};
						tmpObject.regionId = responseObject.result.regions[i].regionId;
						tmpObject.name = responseObject.result.regions[i].name;
						tmpObject.states = [];
						for (j = 0; j < responseObject.result.states.length; j++) {
							if (responseObject.result.regions[i].regionId === responseObject.result.states[j].parentRegionId) {
								var stateTmpObject: any = {};
								stateTmpObject.stateId = responseObject.result.states[j].regionId;
								stateTmpObject.stateName = responseObject.result.states[j].name;
								tmpObject.states.push(stateTmpObject);
							}
						}
						finalRegionsArray.push(tmpObject);
					}
					console.log("finalRegionsArray", finalRegionsArray);

					var userStateName = this.configService.getLoggedInUserInfo().state;
					console.log("userStateName", this.configService.getLoggedInUserInfo().state);

					var userRegionId;
					var userRegionName;

					//find region name of state
					i = 0;
					j = 0;
					for (i = 0; i < finalRegionsArray.length; i++) {
						for (j = 0; j < finalRegionsArray[i].states.length; j++) {
							if (finalRegionsArray[i].states[j].stateName === userStateName) {
								userRegionId = finalRegionsArray[i].regionId;
								userRegionName = finalRegionsArray[i].name;
								this.userRegionInfo = finalRegionsArray[i];
								break;
							}
						}
					}

					// get user region name
					console.log("this.userRegionInfo", this.userRegionInfo);

					if (this.userRegionInfo.name === "") {
						this.userRegionInfo.name = "No Region Found ";
					}

					console.log("this.topRetailers", this.topRetailers);


					if (this.topRetailers.length > 0) {
						for (var t = 0; t < this.topRetailers.length; t++) {
							var tmpOBject: any = {};
							tmpOBject.totalSales = 0;
							if (this.topRetailers[t].brandCode == "TMT") {
								// this.allOverTableData.push(this.topRetailers[t]);
								console.log("this.topRetailers[t].state", this.topRetailers[t].state);
								if (this.userRegionInfo.states !== undefined) {
									for (var m = 0; m < this.userRegionInfo.states.length; m++) {
										console.log("this.topRetailers[t].state", this.topRetailers[t].state);
										if (this.topRetailers[t].state === this.userRegionInfo.states[m].stateName) {
											console.log("this.topRetailers[t].state", this.topRetailers[t].state);
											tmpOBject.name = this.topRetailers[t].userName;
											tmpOBject.state = this.topRetailers[t].state;
											tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[t].totalSales;
										}
									}
								}
								if (this.regionDataTable.length < 5) {
									this.regionDataTable.push(tmpOBject);
								}
							}
						}
					}

					console.log("this.regionDataTable", this.regionDataTable);

					// Logic to push data into region table 

					break;
				case responseCodes.RESP_AUTH_FAIL:

					break;
				case responseCodes.RESP_FAIL:

					break;
				case responseCodes.RESP_ALREADY_EXIST:
					break;
			}
		}, err => {
			this.showLoader = false;
		});
	}


	getAllTheSecondarySales(requestObject) {
		this.showLoader = true;
		this.dashboardService.getAllSecondarySales(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.myClaimsInfo = responseObject.result;
							this.claimsFiltering(responseObject.result);
							console.log("climas", this.myClaimsInfo);
							// Phase 2 changes - Bug Fixing : For Digital and code based program please show the Sales ID as Invoice number wherever required
							for (let i = 0; i < this.myClaimsInfo.length; i++) {
								if (this.programInfo.claimEntryType == "invoice") {
									console.log("Inside Invoice");
									this.myClaimsInfo[i].invoiceNum = this.myClaimsInfo[i].invoiceNumber;
								} else if (this.programInfo.claimEntryType == "digital" || this.programInfo.claimEntryType == "code") {
									this.myClaimsInfo[i].invoiceNum = this.myClaimsInfo[i].secondarySalesId;
									console.log("Inside Digital or Code");
								}
							}
							// End of Phase 2 changes
							this.showClaims = true;

							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.showLoader = false;
							this.myClaimsInfo = null;
							this.mySalesRecordNotFound = true;

							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	/*
	METHOD         : getAllTopFives()
	DESCRIPTION    : For fetching the top fives SKU Retailer and Distributors
*/
	getAllTopFives(requestObject) {
		console.log("trupti$");
		this.showLoader = true;
		this.leaderboardRecordNotfound = false;
		this.dashboardService.getTopRetailersProductwise(requestObject).subscribe((responseObject) => {
			this.showLoader = false;
			this.getProgramRegions(); 

			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
				case responseCodes.RESP_ROLLBACK_ERROR:
					this.leaderboardRecordNotfound = true;
					break;
				case responseCodes.RESP_SERVER_ERROR:
					this.leaderboardRecordNotfound = true;
					break;
				case responseCodes.RESP_SUCCESS:
					this.leaderboardRecordNotfound = false;
					console.log("----called top 5 api", responseObject.result);
					this.topFives = responseObject.result;
					this.topSKUS = this.topFives.SKUS;
					this.topDistributors = this.topFives.distributors;
					this.topRetailers = this.topFives.retailers;
					console.log("this.topRetailers", this.topRetailers);
					for (var a = 0; a < this.topRetailers.length; a++) {
						var state = this.topRetailers.state;
						this.topRetailers.state = state;
						if (this.retailersUserIdArray.includes(this.topRetailers[a].userId) === false) {
							this.retailersUserIdArray.push(this.topRetailers[a].userId);
						}
					}

					this.allOverDistributorTableData = [];
					this.allOverDealerTableData = [];
					if (this.topRetailers.length > 0) {
						for (var t = 0; t < this.topRetailers.length; t++) {
							var tmpOBject: any = {};
							tmpOBject.totalSales = 0;
							if (this.topRetailers[t].categoryName == "TMT") {
								// this.allOverTableData.push(this.topRetailers[t]);
								tmpOBject.name = this.topRetailers[t].userName;
								tmpOBject.state = this.topRetailers[t].state;
								tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[t].totalSales;
								if (this.allOverData.length < 5) {
									// this.allOverDistributorTableData.push(tmpOBject);
									this.allOverDealerTableData.push(tmpOBject);
								}
							}
						}
					} else {
						this.leaderboardRecordNotfound = true;
					}

					this.showTopFives = true;

					console.log("AJ 631 ----------------", this.allOverDistributorTableData);
					console.log("AJ 631 ----------------", this.allOverDealerTableData);

					if (this.topRetailers.length === 0) {
						this.leaderboardRecordNotfound = true;
					}
					//if (this.topRetailers.length > 0) {
					//}

					this.getRegionsPerformance();
					break;
				case responseCodes.RESP_AUTH_FAIL:
					this.leaderboardRecordNotfound = true;
					break;
				case responseCodes.RESP_FAIL:
					this.leaderboardRecordNotfound = true;
					break;
				case responseCodes.RESP_ALREADY_EXIST:
					this.leaderboardRecordNotfound = true;
					break;
			}
		}, err => {
			this.showLoader = false;
		});
	}
	getMyChannels(requestObject) {
		this.showLoader = true;

		console.log('req', requestObject);
		if (requestObject.approvalStatus == "") {
			delete requestObject.approvalStatus;
		}

		this.subscribe = this.dashboardService.getMyChannels(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							console.log("channel Info ddkfjkasdasdfllkklk", responseObject);
							this.channelsInfo = responseObject;
							console.log(this.channelsInfo);
							this.noChannelsFound = false;
							this.channelsFound = true;
							this.configService.setIsChannelsFound(this.channelsFound);
							this.reqObjGetRedemptionOfChannels.customerIdArray = [];
							for (let i = 0; i <= responseObject.result.length - 1; i++) {
								this.reqObjGetRedemptionOfChannels.customerIdArray.push(responseObject.result[i].programUserId);
							}
							this.getMyChannelRedemptions(this.reqObjGetRedemptionOfChannels);
							this.configService.setChildsIdArray(this.reqObjGetRedemptionOfChannels.customerIdArray);
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.channelsFound = false;
							this.channelsInfo = [];
							this.noChannelsFound = true;

							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}


	gotoRedemptionList(claimsOrRedem) {
		console.log("inside gotoRedemptionList");
		this.configService.setTab(claimsOrRedem);
		this.router.navigate(['../home/dashboard-approvals-redemption-list']);
	}

	gotoLeadsListForApproval(leads) {
		console.log("inside gotoLeadsListForApproval");
		this.router.navigate(['../home/leads-list']);
	}
	/****************************************  Navigate to Order Detail Page  *********************************** */

	openRedemptionDialog(index) {
		this.configService.setMyRedemption(this.ordersList[index]);
		let dialogRef = this.dialog.open(DashboardRedemptionDetailComponent, {
			height: '90vh',
			width: '40vw'
		});
		dialogRef.afterClosed().subscribe(result => {
			this.feedbackFlag = true;
			if (result) this.feedbackMessageComponent.updateMessage(true, result, "alert-success");
			this.getMyChannelRedemptions(this.reqObjGetRedemptionOfChannels);
		});
	}

	/***********************************************  End  ******************************************************* */

	getBusinessEntity(reqObj) {
		this.dashboardService.getBusinessEntity(reqObj).subscribe(
			(responseObject) => {
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_SUCCESS:
						this.businessEntity = responseObject.result;
						this.engineerReqCount = responseObject.count;
						for (var x = 0; x < this.businessEntity.length; x++) {

							for (let user of this.businessEntity[x].businessEntityUsers) {
								if (user.userType === "owner") {
									this.businessEntity[x].ownerName = user.fullName;
									this.businessEntity[x].ownerNumber = user.mobileNumber;
								}
							}
						}
						console.log("this.businessEntity", this.businessEntity.length);
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ROLLBACK_ERROR:
					case responseCodes.RESP_SERVER_ERROR:
					case responseCodes.RESP_AUTH_FAIL:
					case responseCodes.RESP_ALREADY_EXIST:
					case responseCodes.RESP_KYC_AWAITING:
					case responseCodes.RESP_BLOCKED:


				}
			},
			err => {

			}
		);
	}

	showDetail(entity) {
		var entityDetails = entity;
		entityDetails.page = "dashboard";
		this.configService.setEntity(entityDetails);
		this.router.navigate(["/home/entity-details"]);
	}
	// api call to get client user info
	getClientUser() {

		this.dashboardService.getClientUser({}).subscribe(
			(responseObject) => {
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_SUCCESS:
						this.configService.setClientUserInfo(responseObject.result);
						console.log("client User", responseObject.result);
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ROLLBACK_ERROR:
					case responseCodes.RESP_SERVER_ERROR:
					case responseCodes.RESP_AUTH_FAIL:
					case responseCodes.RESP_ALREADY_EXIST:
					case responseCodes.RESP_KYC_AWAITING:
					case responseCodes.RESP_BLOCKED:

				}
			},
			err => {

			}
		);
	}

	getMySecondarySales(requestObject) {
		requestObject.creditPartyId = this.programUserInfo.programUserId;
		delete requestObject.debitPartyId;

		this.showLoader = true;
		this.dashboardService.getMySecondarySales(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.myClaimsInfo = responseObject.result;
							this.claimsFiltering(responseObject.result);
							console.log("climas", this.myClaimsInfo);
							// Phase 2 changes - Bug Fixing : For Digital and code based program please show the Sales ID as Invoice number wherever required
							for (let i = 0; i < this.myClaimsInfo.length; i++) {
								if (this.programInfo.claimEntryType == "invoice") {
									console.log("Inside Invoice");
									this.myClaimsInfo[i].invoiceNum = this.myClaimsInfo[i].invoiceNumber;
								} else if (this.programInfo.claimEntryType == "digital" || this.programInfo.claimEntryType == "code") {
									this.myClaimsInfo[i].invoiceNum = this.myClaimsInfo[i].secondarySalesId;
									console.log("Inside Digital or Code");
								}
							}
							// End of Phase 2 changes
							this.showClaims = true;

							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	getChildSecondarySales(requestObject) {
		requestObject.debitPartyId = this.programUserInfo.programUserId;
		delete requestObject.creditPartyId;
		this.showLoader = true;
		this.dashboardService.getChildSecondarySales(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.myClaimsInfo2 = responseObject.result;
							this.claimsFiltering(responseObject.result);
							console.log("climas", this.myClaimsInfo2);
							// Phase 2 changes - Bug Fixing : For Digital and code based program please show the Sales ID as Invoice number wherever required
							for (let i = 0; i < this.myClaimsInfo2.length; i++) {
								if (this.programInfo.claimEntryType == "invoice") {
									console.log("Inside Invoice");
									this.myClaimsInfo2[i].invoiceNum = this.myClaimsInfo2[i].invoiceNumber;
								} else if (this.programInfo.claimEntryType == "digital" || this.programInfo.claimEntryType == "code") {
									this.myClaimsInfo2[i].invoiceNum = this.myClaimsInfo2[i].secondarySalesId;
									console.log("Inside Digital or Code");
								}
							}
							// End of Phase 2 changes
							this.showClaims = true;

							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.showLoader = false;
							this.myClaimsInfo2 = null;


							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	/**************************************** get Channel Redemptios ***************************** */

	getMyChannelRedemptions(reqObj) {

		reqObj.sort = { "createdAt": -1 };
		this.showLoader = true;
		this.dashboardService.getAllOrders(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					console.log("claim responce", responseObject);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_SUCCESS:
							this.ordersList = [];
							if (responseObject.pendingRedeems !== undefined) this.pendingRedeems = responseObject.pendingRedeems;
							else this.pendingRedeems = 0;

							this.totalRedeems = responseObject.count;
							for (let i = 0; i < responseObject.result.length; i++) {
								console.log("inside for", i + ' ' + responseObject.result[i].orderId);
								this.ordersList.push(responseObject.result[i]);
								console.log("inside for", this.ordersList);
							}

							break;
						case responseCodes.RESP_FAIL:
							break;
						case responseCodes.RESP_AUTH_FAIL:
						case responseCodes.RESP_ROLLBACK_ERROR:
						case responseCodes.RESP_SERVER_ERROR:
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
				}
			);
	}

	getRegionsPerformance() {
		var reqObj: any = {};
		reqObj.programId = this.configService.getloggedInProgramInfo().programId;
		reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
		this.dashboardService.getRegionsPerformance(reqObj)
			.subscribe(
				(responseObject) => {

					let responseCodes = this.configService.getStatusTokens();

					console.log("claim responce", responseObject);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_SUCCESS:
							this.regionPerformanceArr = responseObject.result;
							console.log("region performance", this.regionPerformanceArr)

							for (let i = 0; i < this.regionPerformanceArr.length; i++) {
								this.totalNoOfInvoices = this.totalNoOfInvoices + this.regionPerformanceArr[i].noOfInvoices;
								this.allTotalPointsEarned = this.allTotalPointsEarned + this.regionPerformanceArr[i].totalPointsEarned;
							}
							break;

						case responseCodes.RESP_FAIL:
							break;
						case responseCodes.RESP_AUTH_FAIL:
						case responseCodes.RESP_ROLLBACK_ERROR:
						case responseCodes.RESP_SERVER_ERROR:
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
				}
			);
	}

	routing2() {
		var flag = {
			"isChannelGraph": false,
			"isMySalesGraph": true
		};
		this.configService.setChannelGraphFlag(flag);
		this.router.navigate(['../home/dashboard-performance-graphs']);

	}

	getAllProgramFromMaster() {
		this.showLoader = true;
		var requestObject = {
			programIdArray: this.programsMapped
		};
		this.dashboardService.getAllProgramFromMaster(requestObject)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.programArray = responseObject.result;
						console.log("getAllProgramFromMastergetAllProgramFromMaster", responseObject.result);
						this.configService.setBEProgramsMapped(this.programArray)
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	};
	// for showing channels performance graphs for demo: 29-Jan-2018
	routing3Temp() {
		var flag = {
			"isChannelGraph": true,
			"isMySalesGraph": false
		};
		this.configService.setChannelGraphFlag(flag);
		this.router.navigate(['../home/dashboard-performance-graphs']);
	}

	routeTo(page) {
		if (page === 'birthday') {
			this.router.navigate(['../home/notifications']);
			this.configService.setFlagToBirthdayNotification(true);
		} else if (page === 'currentMonthLeads') {
			this.configService.setLeadDashboardLink('currentMonthLeads');
			this.router.navigate(['../home/leads-list']);
		} else if (page === 'lastMonthLeads') {
			this.configService.setLeadDashboardLink('lastMonthLeads');
			this.router.navigate(['../home/leads-list']);
		}

	}

	/**
		 * To get birthday count
		 */
	getTodaysBirthdayCount(requestObject) {
		console.log("response");
		this.showLoader = true;
		this.dashboardService.getTodaysBirthdayCount(requestObject)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						var birthCount = responseObject.count;
						this.birthdayCount = birthCount;
						console.log("birthdayCount", responseObject);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}

	getLeaderboard() {
		console.log("trupti$2");
		console.log("start");
		this.showLoader = true;
		var date = new Date();
		var month = date.getMonth();
		console.log("date", date, "month", month);
		var requestObject = {
			programId: this.stringService.getStaticContents().jswprogramId,
			clientId: this.stringService.getStaticContents().jswClientId,
			serviceType: "programOperations",
			//stateName: this.configService.getLoggedInUserInfo().state,
			month: month
		};
		console.log("mid");
		this.leaderboardRecordNotfound = false;
		this.dashboardService.getLeaderboard(requestObject)
			.subscribe((responseObject) => {
				console.log("after response");
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.leaderboardRecordNotfound = true;
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.leaderboardRecordNotfound = true;
						break;
					case responseCodes.RESP_SUCCESS:
						this.leaderboardRecordNotfound = false;
						//this.regionData = responseObject.regionData;
						this.allOverData = responseObject.allOverData;
						this.leaderboardRegion = responseObject.region;
						console.log("this.allOverData", this.allOverData);
						var productsLength = responseObject.result.length;

						this.tableRegionData = [];
						// if (this.regionData.length > 0) {
						// 	for (var r = 0; r < this.regionData.length; r++) {
						// 		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole) {

						// 			if (this.regionData[r].product == "HRC") {
						// 				this.tableRegionData.push(this.regionData[r]);
						// 			} else {
						// 				this.leaderboardRecordNotfound = true;
						// 				this.panIndia = "";
						// 			}
						// 		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
						// 			if (this.regionData[r].product == "TMT") {
						// 				this.tableRegionData.push(this.regionData[r]);
						// 			} else {
						// 				this.leaderboardRecordNotfound = true;
						// 				this.panIndia = "";
						// 			}
						// 		}
						// 	}
						// } else {
						// 	this.leaderboardRecordNotfound = true;
						// 	this.panIndia = "";
						// }
						// this.allOverDealerTableData = [];
						this.allOverDistributorTableData = [];
						if (this.allOverData.length > 0) {
							for (var t = 0; t < this.allOverData.length; t++) {
								if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole || this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole
									|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
									|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
									|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
									if (this.allOverData[t].product == "HRC") {
										this.allOverDistributorTableData.push(this.allOverData[t]);
									} else {
										this.leaderboardRecordNotfound = true;
										this.panIndia = "";
									}
								} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole || this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole) {
									// if (this.allOverData[t].product == "TMT") {
									// 	this.allOverDealerTableData.push(this.allOverData[t]);
									// } else {
									// 	//this.leaderboardRecordNotfound = true;
									// 	this.panIndia = "";
									// }
								}
							}
						} else {
							this.leaderboardRecordNotfound = true;
							this.panIndia = "";
						}
						console.log("----------------this.allOverTableData", this.allOverDealerTableData);
						console.log("----------------this.allOverTableData", this.allOverDistributorTableData);

						console.log("-----------------this.leaderboardRecordNotfound", this.leaderboardRecordNotfound);


						// TO DO -- FOR DEALER, only NEO STEEL and COATED To be shown -- B4 Moving to production

						if (this.programUserInfo.roleName === 'Dealer') {
							productsLength = 2;
						}
						this.panIndia = "PAN India";
						for (var a = 0; a < productsLength; a++) {
							if (this.leaderboardInfo[a]) {
								var tempObj = {};
								tempObj = {
									categoryName: this.leaderboardInfo[a].categoryName,
									categoryId: this.leaderboardInfo[a].categoryId,
								}
								this.leaderboardHeaders.push(tempObj);
								console.log("this.leaderboardHeaders", this.leaderboardHeaders);
							}
						}
						this.tabChange(this.leaderboardInfo[0], 0);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.leaderboardRecordNotfound = true;
						break;
					case responseCodes.RESP_FAIL:
						this.leaderboardRecordNotfound = true;
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.leaderboardRecordNotfound = true;
						break;
					case responseCodes.RESP_BLOCKED:
						this.leaderboardRecordNotfound = true;
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	};


	tabChange(abcd, index) {
		console.log("abcd", abcd, "index", index);
		this.selectedTab = index;
		this.users = [];
		for (var b = 0; b < this.leaderboardInfo.length; b++) {
			console.log("in for");
			if (abcd.categoryId === this.leaderboardInfo[b].categoryId) {
				console.log("in if");
				this.users = this.leaderboardInfo[b].userSales;
				this.showDetails = true;
				break;
			}
		}
		console.log("this.users", this.users);
	};

	getLead() {

		var obj = {
			providedby: this.configService.getloggedInProgramUser().programUserId,
			programId: this.configService.getprogramInfo().programId,
			clientId: this.configService.getprogramInfo().clientId,
			frontendUserInfo: this.configService.getFrontEndUserInfo(),
			pending: true,
			sort: { createdAt: -1 }
		}

		// In case of engineer admin
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			delete obj.providedby;
		};

		this.showLoader = true;
		this.leadService.getLead(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.showLeadsTable = false;
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.showLeadsTable = false;
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("channel Info in home component", responseObject);
						this.leadslist = responseObject.result;
						this.showLeadsTable = true;
						console.log("this.leadslist", this.leadslist, "this.showLeadsTable", this.showLeadsTable);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.showLeadsTable = false;
						break;
					case responseCodes.RESP_FAIL:
						this.showLeadsTable = false;
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.showLeadsTable = false;
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	};

	generateLeadCsv() {
		this.printingArray = [];

		if (this.leadslist !== null || this.leadslist !== undefined) {
			this.headerArray = ["projectname",
				"ownername",
				"projecttype",
				"expectedvalue",
				"address",
				"city",
				"state",
				"pincode",
				"keycontacts",
				"providedby",
				"providedbyRole",
				"userName",
				"qualified",
				"status",
				"programId",
				"clientId",
				"leadId",
				"createdAt",
				"updatedAt"];
			for (var i = 0; i < this.leadslist.length; i++) {
				var temp = {
					"projectname": "",
					"ownername": "",
					"projecttype": "",
					"expectedvalue": "",
					"address": "",
					"city": "",
					"state": "",
					"pincode": 0,
					"keycontacts": [],
					"providedby": "",
					"providedbyRole": "",
					"userName": "",
					"qualified": "",
					"status": "",
					"programId": "",
					"clientId": "",
					"leadId": "",
					"createdAt": "",
					"updatedAt": ""
				}

				temp.projectname = this.leadslist[i].data.projectname;
				temp.ownername = this.leadslist[i].data.ownername;
				temp.projecttype = this.leadslist[i].data.projecttype;
				temp.expectedvalue = this.leadslist[i].data.expectedvalue;
				temp.address = this.leadslist[i].data.address;
				temp.city = this.leadslist[i].data.city;
				temp.state = this.leadslist[i].data.state;
				temp.pincode = this.leadslist[i].data.pincode;
				temp.keycontacts = this.leadslist[i].data.keycontacts;
				temp.providedby = this.leadslist[i].data.providedby;
				temp.providedbyRole = this.leadslist[i].data.providedbyRole;
				temp.userName = this.leadslist[i].userName[0];
				temp.qualified = this.leadslist[i].data.qualified;
				temp.status = this.leadslist[i].data.status;
				temp.programId = this.leadslist[i].data.programId;
				temp.clientId = this.leadslist[i].data.clientId;
				temp.leadId = this.leadslist[i].data.leadId;
				temp.createdAt = this.leadslist[i].data.createdAt.slice(0, 10);
				temp.updatedAt = this.leadslist[i].data.updatedAt.slice(0, 10);

				this.printingArray.push(temp);
			};
		}
		this._csvService.download(this.printingArray, "Lead_Report");

	}

	getSuperStarsOfTheMonth() {
		var obj = {
			providedby: this.configService.getloggedInProgramUser().programUserId,
			programId: this.configService.getprogramInfo().programId,
			clientId: this.configService.getprogramInfo().clientId
		}
		this.leadService.getSuperstarsOfTheMonth(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("channel Info in home component", responseObject);
						// for (var a = 0; a < responseObject.result.length; a++) {
						// 	for (var b = 0; b < responseObject.result[a].programUserInfo.length; b++) {
						// 		if (responseObject.result[a].programUserInfo[b].businessEntityUserDetails) {

						// 			for (var c = 0; c < responseObject.result[a].programUserInfo[b].businessEntityUserDetails.length; c++) {
						// 				if (responseObject.result[a].programUserInfo[b].businessEntityUserDetails[c].userType === "owner") {

						// 					var obj = {
						// 						userDetails: responseObject.result[a].programUserInfo[b].userDetails,
						// 						numberOfLeads: responseObject.result[a]._id.numberOfLeads,
						// 						businessEntityUserDetails: responseObject.result[a].programUserInfo[b].businessEntityUserDetails[c]
						// 					};
						// 					this.superStarsOfTheMonth.push(obj)
						// 				}
						// 			}
						// 		}
						// 	}
						// }
						for (let i = 0; i < responseObject.result.length; i++) {
							var obj = {
								userName: responseObject.result[i].userName,
								numberOfLeads: responseObject.result[i].noOfLeads,
								state: responseObject.result[i].state
							};
							this.superStarsOfTheMonth.push(obj);
						}
						console.log("this.superStarsOfTheMonth", this.superStarsOfTheMonth);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	// NEW CODE ADDED BY TRUPTI TO FETCH COUNT AS WELL AS TONNAGE
	countLeadForQualified() {
		console.log("start 123");
		this.showLoader = true;
		var endDate = new Date();
		endDate.setDate(endDate.getDate() + 1);
		var startDate = new Date();
		startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
		console.log("First Date of Month", startDate.toISOString().slice(0, 10));
		console.log("Todays Date", endDate.toISOString().slice(0, 10));


		var obj = {
			providedby: this.programUserInfo.programUserId,
			programId: this.programInfo.programId,
			clientId: this.programInfo.clientId,
			startDate: startDate.toISOString().slice(0, 10),
			endDate: endDate.toISOString().slice(0, 10),
			serviceType: "Lead"
		}
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			delete obj.providedby
		}
		console.log("countLeadForQualified",obj);
		this.dashboardService.countLead(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
					console.log("trupti");
						console.log("Lead Count Info", responseObject);
						// this.leadCount = responseObject.result;
						// var count = responseObject.count;
						// this.currentMonthLeadCount = this.currentMonthLeadCount + count;
						var result=responseObject.result;
						var qualifiedCount=0;
						var rejectedCount=0;
						var pendingCount=0;
						var qualifiedTonnage=0;
						var pendingTonnage=0;
						var rejectedTonnage=0;

						for(let i=0;i<result.length;i++){
							
							if(result[i]._id==="qualified"){         // for qualified
								console.log("qualified",result[i]);
								qualifiedCount=result[i].totalCount;
								qualifiedTonnage=result[i].expectedtonnage;
							} else if(result[i]._id==="pending"){      //for pending
								console.log("pending",result[i]);
								pendingCount=result[i].totalCount;
								pendingTonnage=result[i].expectedtonnage;
							} else if(result[i]._id==="rejected"){     //for rejected
								console.log("rejected",result[i]);
								rejectedCount=result[i].totalCount;
								rejectedTonnage=result[i].expectedtonnage;
							}
						}
						
						this.currentMonthLeadCount = qualifiedCount + pendingCount;
						this.qualifiedTonnage = qualifiedTonnage;
						this.pendingTonnage = pendingTonnage;
						this.rejectedTonnage = rejectedTonnage;

						console.log("this.currentMonthLeadCount",this.currentMonthLeadCount);
						console.log("this.qualifiedTonnage",this.qualifiedTonnage);
						console.log("this.pendingTonnage",this.pendingTonnage);
						console.log("this.rejectedTonnage",this.rejectedTonnage);

						console.log("Current Month Count", responseObject.count);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						console.log("get current lead RESP_FAIL");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	countLeadForPending() {
		console.log("start");
		this.showLoader = true;
		var endDate = new Date();
		endDate.setDate(endDate.getDate() + 1);
		var startDate = new Date();
		startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
		console.log("First Date of Month", startDate.toISOString().slice(0, 10));
		console.log("Todays Date", endDate.toISOString().slice(0, 10));


		var obj = {
			providedby: this.programUserInfo.programUserId,
			programId: this.programInfo.programId,
			clientId: this.programInfo.clientId,
			startDate: startDate.toISOString().slice(0, 10),
			endDate: endDate.toISOString().slice(0, 10),
			serviceType: "Lead"
		}
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			delete obj.providedby
		}
		console.log("mid");
		this.dashboardService.countLead(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("Lead Count Info", responseObject);
						var count1 = responseObject.count;
						this.currentMonthLeadCount = this.currentMonthLeadCount + count1;
						console.log("Current Month Count", responseObject.count);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	// NEW CODE ADDED BY TRUPTI TO FETCH COUNT AND TONNAGE
	lastMonthLeadCount() {
		console.log("start");
		this.showLoader = true;
		var now = new Date();

		var endDate = new Date();
		endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1)


		var startDate = new Date(endDate);
		startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))



		console.log("Previous Month Last Date", endDate);
		console.log("Previous Month First Date", startDate);
		var obj1 = {
			providedby: this.programUserInfo.programUserId,
			programId: this.programInfo.programId,
			clientId: this.programInfo.clientId,
			startDate: startDate.toISOString().slice(0, 10),
			endDate: endDate.toISOString().slice(0, 10),
			serviceType: "Lead"
		}
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			delete obj1.providedby
		}
		console.log("mid");
		this.dashboardService.countLead(obj1).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("trupti");
						console.log("Lead Count Info", responseObject);
						// this.leadCount = responseObject.result;
						// var count4 = responseObject.count;
						// this.lastMonthLead = this.lastMonthLead + count4;

						var result=responseObject.result;
						var qualifiedCountPrev=0;
						var rejectedCountPrev=0;
						var pendingCountPrev=0;
						var qualifiedTonnagePrev=0;
						var pendingTonnagePrev=0;
						var rejectedTonnagePrev=0;

						for(let i=0;i<result.length;i++){

							if(result[i]._id==="qualified"){         // for qualified
								console.log("qualified",result[i]);
								qualifiedCountPrev=result[i].totalCount;
								qualifiedTonnagePrev=result[i].expectedtonnage;
							} else if(result[i]._id==="pending"){      //for pending
								console.log("pending",result[i]);
								pendingCountPrev=result[i].totalCount;
								pendingTonnagePrev=result[i].expectedtonnage;
							} else if(result[i]._id==="rejected"){     //for rejected
								console.log("rejected",result[i]);
								rejectedCountPrev=result[i].totalCount;
								rejectedTonnagePrev=result[i].expectedtonnage;
							}
						}

						this.lastMonthLead = qualifiedCountPrev + pendingCountPrev;
						this.previousQualifiedTonnage = qualifiedTonnagePrev ;

						console.log("this.lastMonthLead ",this.lastMonthLead );
						console.log("this.previousQualifiedTonnage",this.previousQualifiedTonnage);
						console.log("Last Month Lead Count", responseObject.count);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	lastMonthLeadCountForPending() {
		console.log("start");
		this.showLoader = true;
		var now = new Date();

		var endDate = new Date();
		endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1)


		var startDate = new Date(endDate);
		startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))



		console.log("Previous Month Last Date", endDate);
		console.log("Previous Month First Date", startDate);
		var obj1 = {
			providedby: this.programUserInfo.programUserId,
			programId: this.programInfo.programId,
			clientId: this.programInfo.clientId,
			pending: true,
			startDate: startDate.toISOString().slice(0, 10),
			endDate: endDate.toISOString().slice(0, 10),
			serviceType: "Lead"
		}
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			delete obj1.providedby
		}
		console.log("mid");
		this.dashboardService.countLead(obj1).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("Last Month Lead Count", responseObject);
						// this.leadCount = responseObject.result;
						// var count3 = responseObject.count;
						// this.lastMonthLead = this.lastMonthLead + count3;
						console.log("Last Month Lead Count", responseObject.count);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	getEvents() {

		var obj = {
			programId: this.configService.getprogramInfo().programId,
			clientId: this.configService.getprogramInfo().clientId,
			serviceType: "programSetup",
			programRoleId: this.programUserInfo.programRole,
			sort: 1
		}
		//handling for client users
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
		  } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
		  }else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
		  } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
		  this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
		  this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
		}
		this.showLoader = true;
		this.dashboardService.getEvents(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("Event component", responseObject);

						var dateTodayZ = new Date();
						var dateTodayZstring = dateTodayZ.toISOString();
						var m = dateTodayZstring.split('T')
						var n = new Date(m[0]);
						this.eventslist = [];

						if (responseObject.result) {
							var eventsArr = responseObject.result;
							for (var a = 0; a < eventsArr.length; a++) {
								
							// $$commented to include banner images for all events$$
							  if (eventsArr[a].startDate && eventsArr[a].endDate) {
								var i = eventsArr[a].startDate.split('T');
			  
								var x = new Date(i[0]);
								console.log("Start Date", x);
			  
								var j = eventsArr[a].endDate.split('T');
								var y = new Date(j[0]);
								console.log("End Date", y);
								console.log("Todays Date", n);
								if (x < n && y < n) { 
								  console.log("Past Events");
								//   this.pastEvents.push(this.eventslist[a]);
									eventsArr[a].label = "pastEvent"
								} else if ((x >= n && y >= n) || (x <= n && y >= n)) {
								//   this.eventslist.push(eventsArr[a]);
									console.log("Upcoming Events");
									eventsArr[a].label = "upcomingEvent"
								}
							  }
							  	console.log("eventsArr[a].label",eventsArr[a].label);
								this.eventslist.push(eventsArr[a]);
							}
						}


						if (responseObject.result) {
							console.log("EventList Length", this.eventslist.length);
							for (var a = 0; a < this.eventslist.length; a++) {
								
								if(this.eventslist[a].showBanner != undefined && this.eventslist[a].showBanner === true ){
									if (this.eventslist[a].bannerImage) {
										var obj = {
											imagePath: "",
											label: "",
											text: ""
										};
										obj.imagePath = this.eventslist[a].bannerImage;
										obj.text = this.eventslist[a].eventDescription;
										obj.label = this.eventslist[a].label;
										console.log("obj",obj);
										if(obj.imagePath!==""){
											this.sliders1.push(obj);
										}
										console.log("slider array", this.sliders1);
										// break;
									}
								}
							}
						}

						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	/*
		METHOD         : getDistributorSalesDetails()
		DESCRIPTION    : To get Distributor Total Sales
		*/
	getDistributorSalesDetails() {


		this.showLoader = true;

		var reqObj = {
			"clientId": this.configService.getloggedInProgramInfo().clientId,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"userId": this.configService.getloggedInProgramUser().programUserId
		}

		console.log("getDistributorSalesDetails", reqObj);
		this.dashboardService.getDistributorSalesDetails(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					console.log(responseCodes);
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.userSalesClaimsDetails = responseObject.result;
							this.totalDistributorSales = this.userSalesClaimsDetails.salesDetails[0].totalSales;

							console.log("Distributor CLAIMS success", this.userSalesClaimsDetails);
							console.log("Distributor Total Sales", this.totalDistributorSales);

							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}


	/*
     For Todays Birthday Fetching
 */
	getTodaysBirthday() {
		console.log("response RRRRRRRRRRRRRRRRRRRRRRRRRRR");
		this.showLoader = true;
		var todaysDate = new Date();
		var reqObj1 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"kycStatus": "NA",
			"todaysDate": todaysDate.toISOString().slice(0, 10),
		}
		console.log("req RRRRRRRRR", reqObj1);
		this.homeService.getTodaysBirthday(reqObj1)
			.subscribe((responseObject) => {
				console.log("response obkect RRRRRRRRRRRRRRRRRR", responseObject.result);
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.todaysBirthday = responseObject.result;
						console.log(" birthday array", this.todaysBirthday);
						if (this.todaysBirthday) {
							for (var a = 0; a < this.todaysBirthday.length; a++) {

								console.log("Todays Birthday Business Id", this.todaysBirthday[a].businessId);
								console.log("Logged In User ProgramUserId", this.programUserInfo.programUserId);
								if (this.todaysBirthday[a].businessId === this.programUserInfo.programUserId) {
									console.log("ID MATCH");
									var obj = {
										imagePath: "",
										label: "",
										text: ""
									};
									obj.imagePath = "assets/logo.png";
									obj.text = "Happy Birthday";
									this.sliders1.push(obj);
									console.log("slider array", this.sliders1);
								}
								else {
									console.log("ID NOT MATCH");
								}
							}
						}
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}


	showProductTable(product) {

		console.log("Product  in showProductTable", product);
	
		if (product == "HRC") {
	
			this.showDistributorProduct1 = true;
			this.showDistributorProduct2 = false;
			this.showDistributorProduct3 = false;
			this.showDistributorProduct4 = false;
			this.showDistributorProduct5 = false;
			const dom1: any = document.getElementById('HRC').classList.add('active');
			const dom3: any = document.getElementById('LONGS').classList.remove('active');
			const dom4: any = document.getElementById('CRM').classList.remove('active');
			const dom5: any = document.getElementById('Coated').classList.remove('active');
			const dom6: any = document.getElementById('HRC').classList.remove('productNameDivActive');
			this.tableRegionData = [];
			// if (this.regionData.length > 0) {
			// 	for (var r = 0; r < this.regionData.length; r++) {
			// 		if (this.regionData[r].product == "HRC") {
			// 			this.tableRegionData.push(this.regionData[r]);
			// 		} else {
			// 			this.leaderboardRecordNotfound = true;
			// 		}
			// 	}
			// } else {
			// 	this.leaderboardRecordNotfound = true;
			// }
			console.log("this.tableRegionDatathis.tableRegionData", this.tableRegionData);
			if (this.allOverData.length > 0) {
				for (var t = 0; t < this.allOverData.length; t++) {
					if (this.allOverData[t].product == "HRC") {
						this.allOverDistributorTableData.push(this.allOverData[t]);
					} else {
						//this.leaderboardRecordNotfound = true;
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}
			console.log("this.allOverTableData.allOverTableData", this.allOverDistributorTableData);
		}
		else if (product == "CRM") {
			this.tableRegionData = [];
			// if (this.regionData.length > 0) {
			// 	for (var r = 0; r < this.regionData.length; r++) {
			// 		if (this.regionData[r].product == "CRM") {
			// 			this.tableRegionData.push(this.regionData[r]);
			// 		} else {
			// 			this.leaderboardRecordNotfound = true;
			// 		}
			// 	}
			// } else {
			// 	this.leaderboardRecordNotfound = true;
			// }
			console.log("this.tableRegionDatathis.tableRegionData", this.tableRegionData);
			this.allOverDistributorTableData = [];
			if (this.allOverData.length > 0) {
				for (var t = 0; t < this.allOverData.length; t++) {
					if (this.allOverData[t].product == "CRM") {
						this.allOverDistributorTableData.push(this.allOverData[t]);
					} else {
						//this.leaderboardRecordNotfound = true;
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}
			console.log("this.allOverTableData.allOverTableData", this.allOverDistributorTableData);
			this.showDistributorProduct1 = false;
			this.showDistributorProduct2 = true;
			this.showDistributorProduct3 = false;
			this.showDistributorProduct4 = false;
			this.showDistributorProduct5 = false;
			const dom1: any = document.getElementById('HRC').classList.remove('active');
			const dom2: any = document.getElementById('CRM').classList.add('active');
			const dom3: any = document.getElementById('LONGS').classList.remove('active');
			const dom5: any = document.getElementById('Coated').classList.remove('active');
			const dom6: any = document.getElementById('HRC').classList.remove('productNameDivActive');
		}
		else if (product == "LONGS") {
	
			this.showDistributorProduct1 = false;
			this.showDistributorProduct2 = false;
			this.showDistributorProduct3 = true;
			this.showDistributorProduct4 = false;
			this.showDistributorProduct5 = false;
			const dom1: any = document.getElementById('HRC').classList.remove('active');
			const dom3: any = document.getElementById('LONGS').classList.add('active');
			const dom4: any = document.getElementById('CRM').classList.remove('active');
			const dom5: any = document.getElementById('Coated').classList.remove('active');
			const dom6: any = document.getElementById('HRC').classList.remove('productNameDivActive');
			this.tableRegionData = [];
			// if (this.regionData.length > 0) {
			// 	for (var r = 0; r < this.regionData.length; r++) {
			// 		if (this.regionData[r].product == "LONGS") {
			// 			this.tableRegionData.push(this.regionData[r]);
			// 		} else {
			// 			this.leaderboardRecordNotfound = true;
			// 		}
			// 	}
			// } else {
			// 	this.leaderboardRecordNotfound = true;
			// }
	
			this.allOverDistributorTableData = [];
			if (this.allOverData.length > 0) {
				for (var t = 0; t < this.allOverData.length; t++) {
					if (this.allOverData[t].product == "LONGS") {
						this.allOverDistributorTableData.push(this.allOverData[t]);
					} else {
						//this.leaderboardRecordNotfound = true;
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}
			console.log("this.tableRegionDatathis.tableRegionData", this.tableRegionData);
			console.log("this.allOverTableData.allOverTableData", this.allOverDistributorTableData);
		}
		else if (product == "Coated") {
	
			this.showDistributorProduct1 = false;
			this.showDistributorProduct2 = false;
			this.showDistributorProduct3 = false;
			this.showDistributorProduct4 = false;
			this.showDistributorProduct5 = true;
			const dom1: any = document.getElementById('HRC').classList.remove('active');
			const dom3: any = document.getElementById('LONGS').classList.remove('active');
			const dom4: any = document.getElementById('CRM').classList.remove('active');
			const dom5: any = document.getElementById('Coated').classList.add('active');
			const dom6: any = document.getElementById('HRC').classList.remove('productNameDivActive');
			this.tableRegionData = [];
			// if (this.regionData.length > 0) {
			// 	for (var r = 0; r < this.regionData.length; r++) {
			// 		if (this.regionData[r].product == "Coated") {
			// 			this.tableRegionData.push(this.regionData[r]);
			// 		} else {
			// 			this.leaderboardRecordNotfound = true;
			// 		}
			// 	}
			// } else {
			// 	this.leaderboardRecordNotfound = true;
			// }
			this.allOverDistributorTableData = [];
			if (this.allOverData.length > 0) {
				for (var t = 0; t < this.allOverData.length; t++) {
					if (this.allOverData[t].product == "Coated") {
						this.allOverDistributorTableData.push(this.allOverData[t]);
					} else {
						//this.leaderboardRecordNotfound = true;
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}
			console.log("this.tableRegionDatathis.tableRegionData", this.tableRegionData);
			console.log("this.allOverTableData.allOverTableData", this.allOverDistributorTableData);
		}
	
	
	}

	showDealerProductTable(product) {
		console.log("Product in showDealerProductTable", product);
		if (product == "TMT") {
			this.showDealerProduct1 = true;
			this.showDealerProduct2 = false;
			const dom1: any = document.getElementById('dealerTMT').classList.add('active');
			const dom2: any = document.getElementById('dealerCoated').classList.remove('active');
			const dom3: any = document.getElementById('dealerTMT').classList.remove('productNameDiv1Active');
			this.allOverDealerTableData = [];
			if (this.topRetailers.length > 0) {
				for (var t = 0; t < this.topRetailers.length; t++) {
					var tmpOBject: any = {};
					tmpOBject.totalSales = 0;
					if (this.topRetailers[t].categoryName == "TMT") {
						// this.allOverDealerTableData.push(this.topRetailers[t]);
						tmpOBject.name = this.topRetailers[t].userName;
						tmpOBject.state = this.topRetailers[t].state;
						tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[t].totalSales;
						if (this.allOverData.length < 5) {
							this.allOverDealerTableData.push(tmpOBject);
						}
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}


			// REGION TABLE COMMENTED NOT REQUIRED
			// this.regionDataTable = [];
			// if (this.topRetailers.length > 0) {
			// 	for (var t = 0; t < this.topRetailers.length; t++) {
			// 		var tmpOBject: any = {};
			// 		tmpOBject.totalSales = 0;
			// 		if (this.topRetailers[t].categoryName == "TMT") {
			// 			// this.allOverDealerTableData.push(this.topRetailers[t]);
			// 			console.log("this.topRetailers[t].state", this.topRetailers[t].state);
			// 			for (var m = 0; m < this.userRegionInfo.states.length; m++) {
			// 				console.log("this.topRetailers[t].state", this.topRetailers[t].state);
			// 				if (this.topRetailers[t].state === this.userRegionInfo.states[m].stateName) {
			// 					console.log("this.topRetailers[t].state", this.topRetailers[t].state);
			// 					tmpOBject.name = this.topRetailers[t].userName;
			// 					tmpOBject.state = this.topRetailers[t].state;
			// 					tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[t].totalSales;
			// 				}
			// 			}
			// 			if (this.regionDataTable.length < 5) {
			// 				this.regionDataTable.push(tmpOBject);
			// 			}
			// 		}
			// 	}
			// }


		}
		else if (product == "Coated") {
			this.showDealerProduct2 = true;
			this.showDealerProduct1 = false;
			const dom1: any = document.getElementById('dealerTMT').classList.remove('active');
			const dom2: any = document.getElementById('dealerCoated').classList.add('active');
			const dom3: any = document.getElementById('dealerTMT').classList.remove('productNameDiv1Active');

			this.allOverDealerTableData = [];


			// TO DO : ADD User IDS from top retailers 
			console.log("retailersUserIdArray", this.retailersUserIdArray);
			if (this.retailersUserIdArray.length > 0) {

				for (var t = 0; t < this.retailersUserIdArray.length; t++) {
					console.log("1", this.retailersUserIdArray[t]);
					if (this.topRetailers.length > 0) {

						var tmpOBject: any = {};
						tmpOBject.totalSales = 0;
						for (var a = 0; a < this.topRetailers.length; a++) {
							console.log("2", this.topRetailers[a]);
							if (this.retailersUserIdArray[t] === this.topRetailers[a].userId) {
								console.log("matched");
								if (this.topRetailers[a].categoryName === "Coated") {
									console.log("brands matched");
									tmpOBject.name = this.topRetailers[a].userName;
									tmpOBject.state = this.topRetailers[a].state;
									tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[a].totalSales;
									console.log("tempOBJJJ", tmpOBject);
								}
							}
						}
						if (tmpOBject.name && tmpOBject.state && tmpOBject.totalSales !== 0) {
							if (this.allOverData.length < 5) {
								this.allOverDealerTableData.push(tmpOBject);
							}
						}
					}
				}
			} else {
				this.leaderboardRecordNotfound = true;
			}




			this.regionDataTable = [];



			console.log("Region this.retailersUserIdArray", this.retailersUserIdArray);

			// REGION TABLE COMMENTED NOT REQUIRED
			// if (this.retailersUserIdArray.length > 0) {

			// 	for (var t = 0; t < this.retailersUserIdArray.length; t++) {
			// 		console.log("Region User Id is", this.retailersUserIdArray[t]);
			// 		if (this.topRetailers.length > 0) {

			// 			var tmpOBject: any = {};
			// 			tmpOBject.totalSales = 0;
			// 			for (var a = 0; a < this.topRetailers.length; a++) {
			// 				//console.log("Region topRetailers 2",this.topRetailers[a]);
			// 				if (this.retailersUserIdArray[t] === this.topRetailers[a].userId) {
			// 					console.log("Region User Id found !!!", this.topRetailers[a]);
			// 					if (this.topRetailers[a].brandCode === "Colouron+" || this.topRetailers[a].brandCode === "GC Sheets") {
			// 						console.log("Region Brand matched !!!", this.topRetailers[a].brandCode);
			// 						for (var m = 0; m < this.userRegionInfo.states.length; m++) {
			// 							console.log(" Region this.topRetailers[t].state", this.userRegionInfo.states[m].stateName);
			// 							if (this.topRetailers[a].state === this.userRegionInfo.states[m].stateName) {
			// 								console.log("Region state matched");
			// 								tmpOBject.name = this.topRetailers[a].userName;
			// 								tmpOBject.state = this.topRetailers[a].state;
			// 								tmpOBject.totalSales = tmpOBject.totalSales + this.topRetailers[a].totalSales;
			// 							}
			// 						}
			// 					}
			// 				}
			// 			}
			// 			if (tmpOBject.name && tmpOBject.state && tmpOBject.totalSales !== 0) {
			// 				console.log("Pushing temp object", tmpOBject);
			// 				if (this.regionDataTable.length < 5) {
			// 					this.regionDataTable.push(tmpOBject);
			// 				}
			// 			}
			// 		}
			// 	}
			// } else {
			// 	this.leaderboardRecordNotfound = true;
			// }




		}

	}




	achieveTarget(a, b) {
		console.log("Inside Achieve Target");
		console.log("Total Dealer Sales", a);


		console.log("Annual Target", b);

		this.achieved = Math.round((a / b) * 100);
		console.log("Achievement", this.achieved);
		this.remainingTarget = 100 - this.achieved;
		console.log("Remaining Target", this.remainingTarget);
		console.log("Chart Function Inside", this.achieved, "Chart Function Inside", this.remainingTarget);

		this.dealerchart(this.achieved, this.remainingTarget);
	}
	dealerchart(achived, remaining) {


		// console.log("Chart Function Inside", achieved, "Chart Function Inside", remaining);

		this.chart = this.AmCharts.makeChart("chartdiv",
			{
				"type": "pie",
				"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				"innerRadius": "80%",
				"pullOutRadius": "0%",
				"labelText": "",
				"colors": [
					"#ae995a",
					"#d2f6ff"
				],
				"labelColorField": "#8E8B51",
				"outlineThickness": 0,
				"titleField": "category",
				"valueField": "column-1",
				"theme": "default",
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": false,
					"align": "center",
					"markerType": "circle"
				},
				"titles": [],
				"dataProvider": [
					{
						"category": "Achieved Target",
						"column-1": this.achieved,
					},
					{
						"category": "Remaining Target",
						"column-1": this.remainingTarget,
					}
				]
			}
		);

	};

	/**
    * METHOD   : getNonTransactionalBoosters
    * DESC     : get Contest list.
    *
    */
   	getNonTransactionalBoosters() {

		var obj = {
			programId: this.configService.getprogramInfo().programId,
			programRoleId: this.programUserInfo.programRole,
			clientId: this.configService.getprogramInfo().clientId,
			serviceType: "programSetup",
			boosterAction: "e",
		}

		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
		} else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
		this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
		this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
			obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
	  	}	
		console.log("ongoing contest obj.programRoleId ",obj.programRoleId);
		this.showLoader = true;
		console.log("getNonTransactionalBoosters reqObj",obj);
		this.specialOffersService.getNonTransactionalBoosters(obj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;

					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SUCCESS:
							let boosterArr = responseObject.result;
							console.log("this.boosterArr", boosterArr);
							let ongoingContests = [];
							var dateTodayZ = new Date();
							var dateTodayZstring = dateTodayZ.toISOString();
							let m = dateTodayZstring.split('T')
							let n = new Date(m[0]);
							console.log("NEW DATE",n);

							if (boosterArr !== undefined) {

								for (let i = 0; i < boosterArr.length; i++) {
									if (boosterArr[i].boosterAction === "e") {
										// $$commented to include banner images for all contests$$
										if (boosterArr[i].boosterStartDate && boosterArr[i].boosterEndDate) {
											var o = boosterArr[i].boosterStartDate.split('T');
											console.log("o", o);
											var x = new Date(o[0]);
											var j = boosterArr[i].boosterEndDate.split('T');
											var y = new Date(j[0]);
											console.log("xxxxx", x, "   n", n, "   yyyy", y);
											if (x < n && y < n) {
												console.log("past");
												boosterArr[i].label = "past";
											} else if ((x == n || y >= n) && (x <= n || y == n)) {
												console.log("ongoing ");
												boosterArr[i].label = "ongoing";
											} else if ((x > n && y > n)) {
                                                console.log("Inside else if");
                                                boosterArr[i].label = "upcoming";
											}
											console.log("boosterArr[i].label",boosterArr[i].label);
											ongoingContests.push(boosterArr[i]);
										}
									}
								}
							}

							if(ongoingContests!==undefined && ongoingContests.length>0){
								for(let i=0;i<ongoingContests.length;i++){	
									// show banner image logic
									if(ongoingContests[i].showBanner !== undefined && ongoingContests[i].showBanner === true){
										if (ongoingContests[i].bannerImages && ongoingContests[i].bannerImages.length>0) {
											var obj = {
												imagePath: "",
												label: "",
												text: ""
											};
											obj.imagePath = ongoingContests[i].bannerImages[0];
											obj.text = ongoingContests[i].boosterDescription;
											obj.label = ongoingContests[i].label;
											console.log("obj",obj);
											if(obj.imagePath!==""){
												this.sliders1.push(obj);
											}
											console.log("slider array", this.sliders1);
										}
									}
								}
							}

							break;
						case responseCodes.RESP_AUTH_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_FAIL:
							this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
					}
				},
				err => {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}

	/**
    * METHOD   : getCelebrations
    * DESC     : get celebrations list.
    *
    */
	getCelebrations() {
		var obj = {
		programId: this.configService.getprogramInfo().programId,
		clientId: this.configService.getprogramInfo().clientId,
		programRoleId: this.programUserInfo.programRole,
		serviceType: "programSetup"
		};

		if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswDealerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole;
		} else if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswDistributorAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole;
		} else if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswEngineerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		} else if (
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswASMAdminProgramRole ||
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswCMProgramRole ||
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswRCMProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		}
		console.log("obj", obj);
		this.showLoader = true;

		// In case of engineer admin
		if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswEngineerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		}

		this.celebrationService.getCelebrations(obj).subscribe(
		responseObject => {
			this.showLoader = false;

			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
			case responseCodes.RESP_ROLLBACK_ERROR:
				break;
			case responseCodes.RESP_SERVER_ERROR:
				break;
			case responseCodes.RESP_SUCCESS:
				console.log("Celebration ResposneObject", responseObject);
				let celebrationslist = responseObject.result;

				// to separate images and videos 
				for(let i=0;i<celebrationslist.length;i++){
					if(celebrationslist[i].showBanner != undefined && celebrationslist[i].showBanner === true){
						if(celebrationslist[i].bannerImage!==undefined){
				
							var obj = {
								imagePath: "",
								label: "",
								text: ""
							};
							obj.imagePath = celebrationslist[i].bannerImage;
							obj.text = celebrationslist[i].description;
							obj.label = "celebrations";
							console.log("obj",obj);
							if(obj.imagePath!==""){
								this.sliders1.push(obj);
							}
							console.log("slider array", this.sliders1);
							// break;
						}
					}
				}

				break;
			case responseCodes.RESP_AUTH_FAIL:
				break;
			case responseCodes.RESP_FAIL:
				break;
			case responseCodes.RESP_ALREADY_EXIST:
				break;
			}
		},
		err => {
			this.showLoader = false;
		}
		);
	}

	goToPage(sliderObj){
		console.log("sliderObj",sliderObj);
		if(sliderObj.label==="celebrations"){
			this.router.navigate(["../../home/celebrations"]);
		} else if (sliderObj.label==="ongoing"){
			this.router.navigate(["../../home/ongoingcontest"]);
		} else if (sliderObj.label==="upcomingEvent"){
			this.router.navigate(["../../home/events"]);
		} else if (sliderObj.label==="pastEvent"){
			this.router.navigate(["../../home/event-detail"]);
		} else if (sliderObj.label==="upcoming"){
			this.router.navigate(["../../home/upcomingcontest"]);
		} else if (sliderObj.label==="past"){
			this.router.navigate(["../../home/pastcontest"]);
		} 
		
	}
	

}

