/*
	Author			:	Pratik Gawand
	Description		: 	Component for channels user profile detail page
	Date Created	: 	25 May 2017
	Date Modified	: 	25 May 2017
*/

import { Component, OnInit, ViewChild, Input, NgZone } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfigService } from "../shared-services/config.service";
import { DashboardService } from "./dashboard.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { PlatformLocation } from '@angular/common';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { DashboardRedemptionDetailComponent } from "./dashboard-redemption-detail.component";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

@Component({
    templateUrl: './approvals-redemption-list.html',
    providers: []

})
export class DashBoardApprovalsRedemptionList implements OnInit {

    previousChannelsInfo: any;

    private ahowActionPopup: boolean = false;
    private addClaimRoute: string;
    private programUserArray: any;
    public math: any;
    public showClaimsTable: boolean = true;
    public showRRTable: boolean = false;
    public showReturnsTable: boolean = false;
    public resultCount: number = 0;
    private showClaimsUI: boolean = true;
    private showPointsUI: boolean = false;
    private showApprovalUI: boolean = false;
    private active: string = "claims";
    private channelRequestObject: any;
    private channelBackRequestObject: any;

    private channelsFound: boolean = false;
    private noChannelsFound: boolean = false;

    private parentUserId: string;

    // Claims Declaration 
    private profileInfo: any;
    private myClaimsInfo: any = null;
    private noOfClaims: number;
    private noOfRedeems: number;
    public filterMyClaimForm: FormGroup;
    public requestObj: any = {};
    public filterName: string;


    public showInvoice: boolean = false;
    public showIVP: boolean = false; // SHOW INVOICE VALUE PRODUCT
    public showIQP: boolean = false; // SHOW INVOICE Quantity PRODUCT
    public showIBP: boolean = false; // SHOW INVOICE Both PRODUCT
    public showCodeTable: boolean = false; //SHOW CODE 

    public programEndDate: string;
    public programStartDate: string;
    public currentStartDate: string;


    public showDVP: boolean = false; // SHOW Digital VALUE PRODUCT
    public showDQP: boolean = false; // SHOW Digital Quantity PRODUCT
    public showDBP: boolean = false; // SHOW Digital Both PRODUCT


    public showDigital: boolean = false;
    public showBoth: boolean = false;

    public date: Date = new Date();
    currentMonth = this.date.getMonth();
    public totalClaimsCount: number;
    private creditPartyId: string;
    private isReturn: boolean;
    public clientColor: any;
    private businessInfo: any = {};
    private channelsInfo: any;
    private canClaimForChild: boolean = false;
    private roleInfo: any;
    private requestObject: any = {
        programId: "",
        clientId: "",
        parentUserId: "",
        frontendUserInfo: {},
        skip: 0,
        limit: 10

    }

    public requestObjectRedeem: any = {};
    public showLoader: boolean;
    public redeemRequestsArr: any = [];
    public programUserInfo: any = {};
    private limit: number;
    private skip: number;
    private serialSkip: number;
    public disablePrev: boolean = true;
    public disableNext: boolean = false;
    public pageCount: number = 1; //to maintain pagecount in pagination
    public totalRedeemsCount: number;
    public noRecordMessage: boolean = false;
    public filterReedemRequestsForm: FormGroup;
    public statusFilterName: string;
    public statusFilterName2: string;
    private endDate: any;
    private sdate: any;
    private edate: any;
    private filterStartDate: string;
    private filterEndDate: string;
    private approvalStatus: string;
    private orderStatus: string;
    public timeline: string;
    private startDate: any;
    public fromTo: boolean = false;
    public redeemProgramStartDate: string;
    public redeemcurrentStartDate: string;
    public isStartDate: boolean = false;
    public isEndDate: boolean = false;
    public requestObjectSales: any = {};
    public programInfo: any = {};
    public tab: any = "";


    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute, private router: Router,
        private configService: ConfigService,
        private dashboardService: DashboardService,
        private stringService: StringService,
        public dialog: MdDialog,
        private formBuilder: FormBuilder,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    ) {

        this.tab = this.configService.getTab();
        this.limit = 10;
        this.skip = 0;
        this.serialSkip = 0;
        this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
        var start = this.date.getUTCFullYear() + "-" + ((this.date.getUTCMonth() + 1) - 1) + "-" + this.date.getUTCDate();

        this.sdate = new Date(start);
        this.startDate = new Date(start);

        var end = this.date.getUTCFullYear() + "-" + (this.date.getUTCMonth() + 1) + "-" + this.date.getUTCDate();

        this.approvalStatus = "";
        this.orderStatus = "";
        this.edate = new Date(end).toLocaleDateString();
        this.endDate = new Date(end).toLocaleDateString();
        this.filterName = " This Month";
        this.statusFilterName = "All";
        this.statusFilterName2 = "All";
    }

    ngOnInit() {
        this.redemptionForm();
        this.claimsForm();
        this.RedemptionFormvalueChanges();
        this.claimsFormvalueChanges();
        this.programUserInfo = this.configService.getloggedInProgramInfo();
        this.programInfo = this.configService.getloggedInProgramInfo();
        this.businessInfo = this.configService.getloggedInBEInfo();
        /*******************  redemption request obj  ***************** */
        this.requestObjectRedeem = {};
        this.requestObjectRedeem.sort = { createdAt: -1 };
        this.requestObjectRedeem.programId = this.programUserInfo.programId;
        this.requestObjectRedeem.clientId = this.programUserInfo.clientId;
        this.requestObjectRedeem.frontendUserInfo = {};
        this.requestObjectRedeem.skip = this.skip;
        this.requestObjectRedeem.limit = this.limit;
        this.requestObjectRedeem.customerIdArray = this.configService.getChildsIdArray();
        if (this.requestObjectRedeem.customerIdArray.length !== 0) this.getAllOrder(this.requestObjectRedeem);
        else this.noRecordMessage = true;
        /********************** end ******************************** */

        this.detectClaimsFormNGetSecondarySale();
    }

    /* ******************************* detect claims form ************************** */

    // INVOICE PRODUCT VALUE
    detectClaimsFormNGetSecondarySale() {



        if (this.businessInfo != null) {
            console.log('in be', this.businessInfo);
            console.log('cp', this.creditPartyId);
            this.requestObjectSales = {
                programId: this.programInfo.programId,
                clientId: this.programInfo.clientId,
                frontendUserInfo: {},
                skip: 0,
                debitPartyId: this.businessInfo.businessId,
                limit: this.limit,
                sort:
                    { createdAt: -1 }
            }

            this.requestObject = {
                programId: this.programUserInfo.programId,
                clientId: this.programUserInfo.clientId,
                frontendUserInfo: {},
                skip: 0,
                debitPartyId: this.businessInfo.businessId,
                limit: this.limit,
                startDate: this.startDate,
                endDate: this.endDate,
                approvalStatus: this.approvalStatus,
                parentUserId: this.parentUserId
            }

        }
        else {
            console.log('in ce');
            this.creditPartyId = this.configService.getloggedInProgramUser().programUserId;
            console.log("this.creditPartyId", this.creditPartyId);
            this.requestObjectSales = {
                programId: this.programInfo.programId,
                clientId: this.programInfo.clientId,
                frontendUserInfo: {},
                skip: 0,
                debitPartyId: this.creditPartyId,
                limit: this.limit,
                sort:
                    { createdAt: -1 }
            }
            this.requestObject = {
                programId: this.programUserInfo.programId,
                clientId: this.programUserInfo.clientId,
                frontendUserInfo: {},
                skip: 0,
                debitPartyId: this.creditPartyId,
                limit: this.limit,
                startDate: this.startDate,
                endDate: this.endDate,
                approvalStatus: this.approvalStatus,
                parentUserId: this.parentUserId
            }
        }


        this.getAllSecondarySales(this.requestObjectSales);

        if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
            console.log("THIS IS TRUE");
            this.showIVP = true;
        }
        // INVOICE BRAND VALUE
        else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
            this.showIVP = true;
            console.log("THIS IS TRUE");
        }
        // INVOICE PRODUCT BOTH  
        else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
            this.showIVP = true;
            console.log("THIS IS TRUE");
        }
        // INVOICE BRAND BOTH
        else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
            this.showIVP = true;
            console.log("THIS IS TRUE");
        }
        // INVOICE PRODUCT QUANTITY  
        else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
            this.showIQP = true;
            console.log("THIS IS TRUE");
        }
        // INVOICE BRAND QUANTITY
        else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
            this.showIQP = true;
            console.log("THIS IS TRUE");
        }

        // DIGITAL PRODUCT VALUE
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
            this.showDVP = true;
            console.log("THIS IS TRUE");
        }
        // DIGITAL BRAND VALUE
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
            this.showDVP = true;
            console.log("THIS IS TRUE");
        }

        // DIGITAL PRODUCT BOTH
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
            this.showDVP = true;
            console.log("THIS IS TRUE");
        }

        // DIGITAL BRAND BOTH
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
            this.showDVP = true;
            console.log("THIS IS TRUE");
        }

        // DIGITAL PRODUCT QUANTITY
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
            this.showDQP = true;
            console.log("THIS IS TRUE");
        }
        // DIGITAL BRAND QUANTITY
        else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
            this.showDQP = true;
            console.log("THIS IS TRUE");
        }
        else if (this.programUserInfo.claimEntryType === "code") {
            this.showCodeTable = true;
            console.log("THIS IS TRUE");
        }

    }

    /********************************************************************************* */
    // List Claims Methods
    getMonthName(dt: number): String {

        let monthArray: String[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return monthArray[dt];

    }
    redemptionForm() {
        this.filterReedemRequestsForm = this.formBuilder.group({
            "redeemShowResults": ["10"],
            "redeemNext": [""],
            "redeemPrevious": [""],
            "redeemStartDate": [""],
            "redeemEndDate": [""]
        });
    }

    claimsForm() {
        this.filterMyClaimForm = this.formBuilder.group({
            "showResults": ["10"],
            "next": [""],
            "previous": [""],
            "startDate": [""],
            "endDate": [""]
        });
    }

    /******************************** form value changes  ********************************* */

    showResults(limit) {
        if (this.approvalStatus == "") {
            this.statusFilterName = "All";
        } else {
            this.statusFilterName = this.approvalStatus.replace(/\b\w/g, function (l) { return l.toUpperCase() })
        }


        this.requestObject.limit = parseInt(this.filterMyClaimForm.value.showResults);
        this.requestObject.skip = 0;
        var requestObj = this.requestObject;



        this.getAllTheSecondarySales(this.requestObject);
    }

    claimsFormvalueChanges() {
        this.filterMyClaimForm.valueChanges.subscribe(data => {
            this.requestObj.programId = this.programUserInfo.programId;
            this.requestObj.clientId = this.programUserInfo.clientId;
            this.requestObj.debitPartyId = this.creditPartyId;
            this.requestObj.frontendUserInfo = {};
            this.requestObj.skip = this.requestObject.skip;
            this.requestObj.limit = this.requestObject.limit;
            this.requestObj.startDate = this.requestObject.startDate;
            this.requestObj.endDate = this.requestObject.endDate;
            this.requestObj.approvalStatus = this.requestObject.approvalStatus;
            this.requestObj.parentUserId = this.requestObject.parentUserId;

            if (data.showResults != null && data.showResults != "") {
                this.requestObj.limit = parseInt(data.showResults);
                this.limit = parseInt(data.showResults);
                this.requestObject.limit = parseInt(data.showResults);

                this.pageCount = 1;
                this.requestObj.startDate;
                this.requestObj.endDate;
            } else if (data.startDate != null && data.startDate != "") {

                this.requestObj.limit = parseInt(data.showResults);
                this.limit = parseInt(data.showResults);
                this.requestObject.limit = parseInt(data.showResults);
                this.requestObj.startDate = data.startDate;
                this.requestObj.endDate = data.endDate;
                this.startDate = data.startDate;
                this.startDate = new Date(this.startDate);
                this.requestObject.startDate = data.startDate;
                this.endDate = data.endDate;
                this.endDate = new Date(this.endDate);
                this.requestObject.endDate = data.endDate;
                this.pageCount = 1;
                this.isStartDate = true;


            }

            if (data.endDate != null && data.endDate != "") {

                this.requestObj.limit = parseInt(data.showResults);
                this.limit = parseInt(data.showResults);
                this.requestObject.limit = parseInt(data.showResults);
                this.requestObj.startDate = data.startDate;
                this.requestObj.endDate = data.endDate;
                this.startDate = data.startDate;
                this.requestObject.startDate = data.startDate;
                this.endDate = data.endDate;
                this.requestObject.endDate = data.endDate;
                this.pageCount = 1;
                this.isEndDate = true;
            }

            if (this.isStartDate && this.isEndDate) {

                var startDate = new Date(this.startDate);
                var endDate = new Date(this.endDate);
                this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
                    " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();

            }

            var startDate = new Date(this.startDate);
            var endDate = new Date(this.endDate);
            if (this.requestObj.startDate != undefined || this.requestObject.endDate != undefined) {
                this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
                    " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
            } else {
                this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
            }


            console.log("IN VALUE CHANGE", this.requestObj);


            this.getAllSecondarySales(this.requestObj);


        });
    }

    RedemptionFormvalueChanges() {
        this.filterReedemRequestsForm.valueChanges.subscribe(data => {
            this.requestObjectRedeem = {};
            this.requestObjectRedeem.sort = { createdAt: -1 };
            this.requestObjectRedeem.programId = this.programUserInfo.programId;
            this.requestObjectRedeem.clientId = this.programUserInfo.clientId;
            this.requestObjectRedeem.frontendUserInfo = {};
            this.requestObjectRedeem.skip = this.requestObjectRedeem.skip;
            this.requestObjectRedeem.limit = this.requestObjectRedeem.limit;
            this.requestObjectRedeem.sdate = this.requestObjectRedeem.sdate;
            this.requestObjectRedeem.edate = this.requestObjectRedeem.edate;
            this.requestObjectRedeem.orderStatus = this.requestObjectRedeem.orderStatus;

            if (data.redeemShowResults != null && data.redeemShowResults != "") {
                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
                this.limit = parseInt(data.redeemShowResults);
                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);

                this.pageCount = 1;
                this.requestObjectRedeem.sdate;
                this.requestObjectRedeem.edate;
            } else if (data.redeemStartDate != null && data.redeemStartDate != "") {

                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
                this.limit = parseInt(data.redeemShowResults);
                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
                this.requestObjectRedeem.sdate = data.redeemStartDate;
                this.requestObjectRedeem.edate = data.redeemEndDate;
                this.sdate = data.redeemStartDate;
                this.sdate = new Date(this.sdate);
                this.requestObjectRedeem.sdate = data.redeemStartDate;
                this.edate = data.edate;
                this.edate = new Date(this.edate);
                this.requestObjectRedeem.edate = data.redeemEndDate;
                this.pageCount = 1;
                this.isStartDate = true;


            }

            if (data.redeemEndDate != null && data.redeemEndDate != "") {

                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
                this.limit = parseInt(data.redeemShowResults);
                this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
                this.requestObjectRedeem.sdate = data.redeemStartDate;
                this.requestObjectRedeem.edate = data.redeemEndDate;
                this.sdate = data.redeemStartDate;
                this.requestObjectRedeem.startDate = data.redeemStartDate;
                this.edate = data.redeemEndDate;
                this.requestObjectRedeem.edate = data.redeemEndDate;
                this.pageCount = 1;
                this.isEndDate = true;
            }

            if (this.isStartDate && this.isEndDate) {

                var startDate = new Date(this.sdate);
                var endDate = new Date(this.edate);
                this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
                    " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();

            }

            var startDate = new Date(this.sdate);
            var endDate = new Date(this.edate);
            if (this.requestObjectRedeem.sdate != undefined || this.requestObjectRedeem.edate != undefined) {
                this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
                    " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
            } else {
                this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
            }


            console.log("IN VALUE CHANGE", this.requestObjectRedeem);


            this.getAllOrder(this.requestObjectRedeem);


        });
    }
    /*************************************************************************************** */

    /****************************************  Navigate to Order Detail Page  *********************************** */

    openRedemptionDetailPage(redeem) {
        this.configService.setMyRedemption(redeem);
        let dialogRef = this.dialog.open(DashboardRedemptionDetailComponent, {
            height: '90vh',
            width: '35vw'
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) this.feedbackMessageComponent.updateMessage(true, result, "alert-success");

            this.getAllOrder(this.requestObjectRedeem);
        });
    }

    /***********************************************  End  ******************************************************* */

    /************************************ Filters  *********************************** */

    redeemShowResults(limit) {
        if (this.orderStatus == "") {
            this.statusFilterName = "All";
        } else {
            this.statusFilterName = this.orderStatus.replace(/\b\w/g, function (l) { return l.toUpperCase() })
        }
        this.requestObjectRedeem.limit = parseInt(this.filterReedemRequestsForm.value.redeemShowResults);
        this.requestObjectRedeem.skip = 0;
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToPending() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showPendingRedeems");
        this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
        this.orderStatus = "Pending"
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);

    }



    redeemChangeToConfirmed() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showApprovedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemConfirmed;
        this.orderStatus = "Confirmed";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToProcessing() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemProcessing;
        this.orderStatus = "Processing";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToShipped() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemShipped;
        this.orderStatus = "Shipped";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToCompleted() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemCompleted;
        this.orderStatus = "Completed";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToCancelled() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemCancelled;
        this.orderStatus = "Cancelled";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToReturned() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
        this.statusFilterName = this.stringService.getStaticContents().redeemReturned;
        this.orderStatus = "Returned";
        this.requestObjectRedeem.orderStatus = this.orderStatus;
        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }


    redeemChangeToAll() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showAllRedeems");


        this.statusFilterName = this.stringService.getStaticContents().redeemAll;
        delete this.requestObjectRedeem.orderStatus;

        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }


    redeemChangeToLifeTime() {
        this.requestObjectRedeem.skip = 0;
        this.filterName = "All";
        this.timeline = "All Redemptions";
        this.statusFilterName = "All";
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showLifeTimeRedeems");
        delete this.requestObjectRedeem.sdate;
        delete this.requestObjectRedeem.edate;
        console.log("Lifetime", this.requestObjectRedeem);
        this.serialSkip = this.requestObjectRedeem.skip;
        console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }


    redeemChangeToWeek() {
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showWeekRedeems");
        this.fromTo = false;
        this.filterName = " This Week";
        var dt = new Date(this.sdate);
        var weekDate = new Date();
        var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
        this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
        this.requestObjectRedeem.sdate = weekStartDate;
        this.requestObjectRedeem.edate = weekDate;
        this.serialSkip = this.requestObjectRedeem.skip;
        console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }


    redeemChangeToQuarter() {
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showQuarterRedeems");
        this.fromTo = false;
        this.filterName = "This Quarter";
        var quarterMonth = new Date();
        var dt = new Date();
        var currentMonth = quarterMonth.getMonth();
        dt.setMonth(dt.getMonth() - 3);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
        this.requestObjectRedeem.sdate = new Date(date);
        this.requestObjectRedeem.edate = new Date();
        this.serialSkip = this.requestObjectRedeem.skip;
        console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChangeToMonth() {
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showMonthRedeems");
        this.fromTo = false;
        this.filterName = " This Month";
        var month = new Date();
        var dt = new Date();
        var currentMonth = month.getMonth();
        dt.setMonth(dt.getMonth() - 1);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
        var startDate = new Date(date);
        this.requestObjectRedeem.sdate = startDate;
        this.requestObjectRedeem.edate = new Date();
        console.log("Request Object : ", this.requestObjectRedeem);
        this.serialSkip = this.requestObjectRedeem.skip;
        console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemChooseStartToEnd() {
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showCustomRedeems");
        this.filterName = "Time Period";
        this.fromTo = true;
        this.serialSkip = this.requestObjectRedeem.skip;
    }


    redeemOnStartDate(startDate) {
        this.redeemProgramStartDate = startDate;
        this.redeemcurrentStartDate = startDate;
    }

    redeemChangeToAllApprovalStatus() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showAllRedeems");


        this.statusFilterName2 = this.stringService.getStaticContents().redeemAll;
        delete this.requestObjectRedeem.approvalStatus;

        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemApprovalStatusPending() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Pending Approval Status Redeem");

        this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusPending;
        this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemApprovalStatusApproved() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Approved Approval Status Redeem");

        this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusApproved;
        this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemApprovalStatusRejected() {
        this.skip = 0;
        this.requestObjectRedeem.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Rejected Approval Status Redeem");

        this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusRejected;
        this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

        console.log("REQ", this.requestObjectRedeem);
        this.getAllOrder(this.requestObjectRedeem);
    }
    /********************************************************************************** */
    redeemOnPrevClick() {
        console.log("prev");
        this.requestObjectRedeem.skip = this.requestObjectRedeem.skip - this.limit;
        this.requestObjectRedeem.limit = this.limit;
        this.pageCount--;
        this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "previousPageClaims", this.pageCount);
        this.serialSkip = this.requestObjectRedeem.skip;
        this.getAllOrder(this.requestObjectRedeem);
    }

    redeemOnNextClick() {
        console.log("next");
        this.requestObjectRedeem.skip = this.requestObjectRedeem.skip + this.limit;
        this.requestObjectRedeem.limit = this.limit;
        this.pageCount++;
        this.serialSkip = this.requestObjectRedeem.skip;
        this.getAllOrder(this.requestObjectRedeem);
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObjectRedeem.programId, "nextPageClaims", this.pageCount);

    }

    /*************************************** claims functionality ***************************** */





    claimsFiltering(claimsInfo) {
        for (let i = 0; i < claimsInfo.length; i++) {
            let invoiceDoc = claimsInfo[i].invoiceDocument;
            claimsInfo[i].invoiceDocument = invoiceDoc;
            claimsInfo[i].approvalStatus
            if (claimsInfo[i].approvalStatus == "pending") {
                claimsInfo[i].pointsEarned = "Pending";
            }
            else if (claimsInfo[i].approvalStatus == "rejected") {
                claimsInfo[i].pointsEarned = 0;
            }

        }
        let userInfo = this.configService.getloggedInProgramUser();
        console.log("USER INFO : ", claimsInfo);
        console.log("USER INFO : ", userInfo);

        if (claimsInfo) {

            for (let i = 0; i < claimsInfo.length; i++) {
                claimsInfo[i].isReturn = false;
                if (claimsInfo[i].debitPartyId == userInfo.programUserId) {
                    claimsInfo[i].isReturn = true;
                }
                console.log(claimsInfo[i].debitPartyId);

            }
            console.log("^^^^^  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", claimsInfo);
            console.log(userInfo.programUserId);
        }
        this.myClaimsInfo = claimsInfo;
        console.log("Claims Info : ", this.myClaimsInfo);

    }

    changeToPending() {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showPendingClaims");
        this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
        this.approvalStatus = "pending"
        this.requestObject.approvalStatus = this.approvalStatus;
        console.log("REQ", this.requestObject);
        this.getAllSecondarySales(this.requestObject);

    }

    changeToApproved() {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showApprovedClaims");
        this.statusFilterName = this.stringService.getStaticContents().myClaimsApproved;
        this.approvalStatus = "approved";
        this.requestObject.approvalStatus = this.approvalStatus;
        console.log("REQ", this.requestObject);
        this.getAllSecondarySales(this.requestObject);
    }
    changeToRejected() {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showRejectedClaims");
        this.statusFilterName = this.stringService.getStaticContents().myClaimsRejected;
        this.approvalStatus = "rejected";
        this.requestObject.approvalStatus = this.approvalStatus;
        console.log("REQ", this.requestObject);
        this.getAllSecondarySales(this.requestObject);
    }

    changeToAll() {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showAllClaims");


        this.statusFilterName = this.stringService.getStaticContents().redeemAll;
        delete this.requestObject.approvalStatus;
        delete this.requestObject.startDate;
        delete this.requestObject.endDate;
        console.log("REQ", this.requestObject);
        this.getAllSecondarySales(this.requestObject);
    }

    changeToLifeTime() {
        this.filterName = "All";
        this.timeline = "All Claims";
        this.statusFilterName = "All";
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showLifeTimeClaims");
        delete this.requestObject.startDate;
        delete this.requestObject.endDate;
        delete this.requestObject.approvalStatus;
        console.log("Lifetime", this.requestObject);
        this.getAllTheSecondarySales(this.requestObject);
    }

    changeToWeek() {
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showWeekClaims");
        this.fromTo = false;
        this.filterName = " This Week";
        var dt = new Date(this.startDate);
        var weekDate = new Date();
        var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
        this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
        this.requestObject.startDate = weekStartDate;
        this.requestObject.endDate = weekDate;
        this.getAllSecondarySales(this.requestObject);
    }

    changeToQuarter() {
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showQuarterClaims");
        this.fromTo = false;
        this.filterName = "This Quarter";
        var quarterMonth = new Date();
        var dt = new Date();
        var currentMonth = quarterMonth.getMonth();
        dt.setMonth(dt.getMonth() - 3);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
        this.requestObject.startDate = new Date(date);
        this.requestObject.endDate = new Date();
        this.getAllSecondarySales(this.requestObject);
    }

    changeToMonth() {
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showMonthClaims");
        this.fromTo = false;
        this.filterName = " This Month";
        var month = new Date();
        var dt = new Date();
        var currentMonth = month.getMonth();
        dt.setMonth(dt.getMonth() - 1);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
        var startDate = new Date(date);
        this.requestObject.startDate = startDate;
        this.requestObject.endDate = new Date();
        this.getAllSecondarySales(this.requestObject);
    }

    chooseStartToEnd() {
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showCustomClaims");
        this.filterName = "Time Period";
        this.fromTo = true;
    }

    changeLimit() {

        this.requestObject.limit = this.filterMyClaimForm.value.showResults;
        this.getAllSecondarySales(this.requestObject);
    }

    onPrevClick() {
        this.requestObject.skip = this.requestObject.skip - this.limit;
        this.requestObject.limit = this.limit;
        this.pageCount--;
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "previousPageClaims", this.pageCount);
        this.serialSkip = this.requestObject.skip;
        this.getAllSecondarySales(this.requestObject);
    }

    onNextClick() {
        this.requestObject.skip = this.requestObject.skip + this.limit;
        this.requestObject.limit = this.limit;
        this.pageCount++;
        this.serialSkip = this.requestObject.skip;
        this.getAllSecondarySales(this.requestObject);
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "nextPageClaims", this.pageCount);

    }


    onStartDate(startDate) {
        this.programStartDate = startDate;
        this.currentStartDate = startDate;
    }

    /******************************************************************************************* */

    /***************************************	API CALLS 	****************************************************/
    /*
        METHOD         : getAllSecondarySales()
        DESCRIPTION    : To get all claims
    */
    getAllSecondarySales(requestObject) {
        this.showLoader = true;
        this.noRecordMessage = false;


        if (requestObject.approvalStatus == "") {
            delete requestObject.approvalStatus;
        }

        this.dashboardService.getAllSecondarySales(requestObject)
            .subscribe(
                (responseObject) => {
                    // setTimeout(() => { this.showLoader = false }, 1000)
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:

                            this.myClaimsInfo = responseObject.result;
                            var resultCount = responseObject.result.count;
                            this.noOfClaims = responseObject.result.length;
                            this.totalClaimsCount = responseObject.count;

                            this.configService.setMyClaims(responseObject.result);
                            console.log("**********getAllSecondarySales*************");
                            console.log("Length", this.myClaimsInfo.length);
                            console.log("Limit", this.limit);
                            console.log("Count", resultCount);
                            console.log("this.totalClaimsCount", this.totalClaimsCount);
                            this.serialSkip = this.requestObject.skip;

                            if ((this.pageCount) * (this.limit) >= this.totalClaimsCount) {
                                this.disableNext = true;
                            } else {
                                this.disableNext = false;
                            }
                            console.log("Mul-->", this.pageCount * this.limit);

                            console.log("Result", this.disableNext);
                            if (this.pageCount == 1) {
                                this.disablePrev = true;
                            } else {
                                this.disablePrev = false;
                            }
                            console.log("SKIP", this.requestObject.skip);
                            this.claimsFiltering(responseObject.result);
                            console.log("SKIPPER", this.serialSkip);

                            break;
                        case responseCodes.RESP_AUTH_FAIL:

                            break;
                        case responseCodes.RESP_FAIL:
                            this.myClaimsInfo = null;
                            this.noRecordMessage = true;
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }
    functionForGettingSecondarySale() {

        var Obj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "programId": this.configService.getprogramInfo().programId,
            "clientId": this.configService.getprogramInfo().clientId,
            "debitPartyId": this.creditPartyId,
            "parentUserId": this.parentUserId,
            "skip": this.skip,
            "limit": this.limit
        };
        setTimeout(() => {
            console.log("timeOUTTT");
            this.getAllSecondarySales(Obj);
        }, 5000);

        console.log("afterdelay");
    }
    getAllTheSecondarySales(requestObject) {
        this.showLoader = true;
        this.noRecordMessage = false;

        delete requestObject.startDate;
        delete requestObject.endDate;

        if (requestObject.approvalStatus == "") {
            delete requestObject.approvalStatus;
        }

        this.dashboardService.getAllSecondarySales(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.claimsFiltering(responseObject.result);

                            this.configService.setMyClaims(responseObject.result);
                            this.totalClaimsCount = responseObject.count;
                            this.noOfClaims = this.totalClaimsCount;
                            console.log("**********getAllTheSecondarySales*************");
                            console.log("myClaimsInfo.length : ", this.myClaimsInfo.length);
                            console.log("totalClaimsCount : ", this.totalClaimsCount);
                            console.log("pageCount * limit : ", this.pageCount * this.limit);
                            console.log("Limit : ", this.limit);
                            console.log("this.pageCount : ", this.pageCount);

                            if ((this.pageCount) * (this.limit) >= this.totalClaimsCount) {
                                this.disableNext = true;
                            } else {
                                this.disableNext = false;
                            }

                            this.serialSkip = this.requestObject.skip;

                            if (this.pageCount == 1) {
                                this.disablePrev = true;
                            } else {
                                this.disablePrev = false;
                            }
                            console.log("SKIP", this.requestObject.skip);
                            console.log("SKIPPER", this.serialSkip);
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            this.showLoader = false;
                            this.myClaimsInfo = null;
                            this.noRecordMessage = true;
                            this.showBoth = false;
                            this.showDBP = false;
                            this.showDQP = false;
                            this.showDVP = false;
                            this.showIBP = false;
                            this.showIQP = false;

                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }
    // List Redeem Requests Method
    getAllOrder(requestObject) {
        this.showLoader = true;
        this.noRecordMessage = false;
        this.showLoader = true;
        this.redeemRequestsArr = [];
        let redeem: any[] = [];

        console.log("getAllOrders request Object : ", requestObject);
        this.dashboardService.getAllOrders(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.redeemRequestsArr = responseObject.result;
                            this.totalRedeemsCount = responseObject.count;
                            this.noRecordMessage = false;
                            console.log("ResponseObjectArray getAllOrder", this.redeemRequestsArr);

                            this.serialSkip = this.requestObjectRedeem.skip;

                            if ((this.pageCount) * (this.limit) >= this.totalRedeemsCount) {
                                this.disableNext = true;
                            } else {
                                this.disableNext = false;
                            }
                            console.log("Mul-->", this.pageCount * this.limit);

                            console.log("Result", this.disableNext);
                            if (this.pageCount == 1) {
                                this.disablePrev = true;
                            } else {
                                this.disablePrev = false;
                            }
                            console.log("SKIP", this.requestObjectRedeem.skip);
                            console.log("SKIPPER", this.serialSkip);
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            this.noRecordMessage = true;
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );

    }
}