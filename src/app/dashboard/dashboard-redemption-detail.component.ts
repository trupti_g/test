/*
	Author			:	Vaishali Sagvekar
	Description		: 	Component for Redemption Details 
	Date Created	: 	30-Aug-2017
	Date Modified	: 	
*/

import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { DashboardService } from "./dashboard.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({

    templateUrl: './dashboard-redemption-detail.component.html',
    providers: [Device]


})
export class DashboardRedemptionDetailComponent implements OnInit {

    public redeemForm: FormGroup;
    public order: any = {};
    public approvalStatus: string = "";
    private showLoader: boolean = false;

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    private programUserInfo: any;
    private comment: string = "";

    constructor(public dialogRef: MdDialogRef<DashboardRedemptionDetailComponent>,
        private configService: ConfigService,
        private dashboardService: DashboardService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {

        this.configService.setRewardGalleryFlag(false);
      

        // Make form builder for comment 
        this.redeemForm = this.formBuilder.group({
            "comment": [""]
        })


    }
    ngOnInit() {

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Redemption Details")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Redemption Details");
                    ga('send', 'pageview');
                }
            });
        }
        
        this.programUserInfo = this.configService.getloggedInProgramUser();
        console.log("this.programUserInfo", this.programUserInfo);
        if (this.configService.getMyRedemption()) {
            this.order = this.configService.getMyRedemption();
            console.log("this.order", this.order);
            if (this.order.comment !== undefined) {
                this.redeemForm.patchValue({
                    "comment": this.order.comment
                })
            }

        }

    }

    closeInvoice() {
        this.dialogRef.close();
    }

    // on Approve click
    onApprove(redeem) {
        let requestObj: any = {};
        requestObj.orderArray = [];
        let singleOrderArrayObject = {
            "orderId": "",
            "updateDetails": {}
        }
        singleOrderArrayObject.orderId = redeem.orderId;
        singleOrderArrayObject.updateDetails = {
            "approvalStatus": "Approved",
            "comment": this.redeemForm.value.comment
        }
        requestObj.orderArray.push(singleOrderArrayObject);
        requestObj.approvalStatus = "Approved";
        this.approvalStatus = "Approved";
        this.updateOrder(requestObj);
    }


    onCancel(redeem) {
        let requestObj: any = {};
        requestObj.orderArray = [];
        let singleOrderArrayObject = {
            "orderId": "",
            "updateDetails": {}
        }
        singleOrderArrayObject.orderId = redeem.orderId;
        singleOrderArrayObject.updateDetails = {
            "approvalStatus": "Rejected",
            "comment": this.redeemForm.value.comment
        }
        requestObj.orderArray.push(singleOrderArrayObject);
        requestObj.approvalStatus = "Rejected";
        this.approvalStatus = "Rejected";
        this.updateOrder(requestObj);
    }

    updateOrder(requestObject) {

        this.showLoader = true;
        this.dashboardService.updateOrder(requestObject).subscribe((responseObject) => {
            this.showLoader = false;
            console.log(responseObject);
            let responseCodes = this.configService.getStatusTokens();
            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;

                case responseCodes.RESP_SERVER_ERROR:
                    break;

                case responseCodes.RESP_SUCCESS:
                    this.order.approvalStatus = this.approvalStatus;
                    console.log(responseObject.result);
                    let msg = "Order " + this.order.orderId + " successfully updated !!";
                    this.dialogRef.close(msg);
                    break;

                case responseCodes.RESP_AUTH_FAIL:
                    break;

                case responseCodes.RESP_FAIL:
                    break;

                case responseCodes.RESP_ALREADY_EXIST:
                    break;
            }
        },
            err => {
                this.showLoader = false;
            }
        );
    }

}
