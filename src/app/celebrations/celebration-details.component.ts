	import { CelebrationModule } from "./celebrations.module";
	import {
	Component,
	OnInit,
	Input,
	OnDestroy,
	OnChanges,
	NgZone,
	ViewChild
	} from "@angular/core";
	import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
	import {
	FormGroup,
	FormBuilder,
	Validators, 
	FormControl
	} from "@angular/forms";
	import { ConfigService } from "../shared-services/config.service";
	import { Http, Headers, RequestOptions } from "@angular/http";
	import { AppFooterComponent } from "../footer/app-footer.component";
	import { ClientFooterComponent } from "../footer/client-footer.component";
	import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
	import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
	import { CelebrationService } from "./celebrations.service";
	import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
	import { SidebarComponent } from "../home/sidebar/sidebar.component";
	import { StringService } from "../shared-services/strings.service";
	import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


	@Component({
	templateUrl: "./celebration-details.component.html",
	styleUrls: ["../app.component.scss", "./celebration.component.scss"],
	providers: [Device]
	})
	export class CelebrationDetailsComponent implements OnInit {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	noRecords: boolean = false;
	programUserInfo: any;

	public imagesArray:any[] = [];
	public videosArray:any[] = [];

	public selectedCelebration:any;
	public isZoom:boolean = false;
	public imageURL:any;

	constructor(
		private router: Router,
		private configService: ConfigService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private route: ActivatedRoute,
		private http: Http,
		private celebrationService: CelebrationService,
		public sidebar: SidebarComponent,
		private stringService: StringService,
		private device: Device,
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
		this.displaypc = false;
		this.displaymobile = true;
		console.log("display mobile val", this.displaymobile);
		} else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
		this.displaypc = false;
		this.displaymobile = true;
		} else {
		this.displaypc = true;
		this.displaymobile = false;
		}


		this.programUserInfo = configService.getloggedInProgramUser();

	}

	ngOnInit() {
		// google analytics page view code
		if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Celebration-details")
        } else {
		}
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga("set", "page", "Celebration-details");
				ga("send", "pageview");
			}
		});

		this.sidebar.close();
		this.sidebar.addExpandClass("pages");
		this.sidebar.addExpandClass1("pages1");

		this.selectedCelebration = this.configService.getCelebration();
		console.log("this.selectedCelebration",this.selectedCelebration);
		console.log("this.selectedCelebration.videos",this.selectedCelebration.videos);
	}

	zoom(n) {
		this.googleAnalyticsEventsService.emitEvent("Celebrations", "viewPhoto", this.selectedCelebration.title);
		this.configService.setModalClose(false);
		if (this.isZoom === false) {
			this.isZoom = true;
			this.imageURL = n;
		}
	}

	closeDetails() {
		this.configService.setModalClose(true);
		if (this.isZoom === true) {
			this.isZoom = false;
		}
	}

	backToCelebrations(){
		this.googleAnalyticsEventsService.emitEvent("Celebrations", "back", this.selectedCelebration.title);
		console.log("Inside celebration Details");
        this.router.navigate(['../../home/celebrations']);
	}

	tabClick(tab){
		console.log("this.selectedCelebration.title",this.selectedCelebration.title);
		this.googleAnalyticsEventsService.emitEvent("Celebrations", tab, this.selectedCelebration.title);
	}

}
