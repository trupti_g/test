import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class CelebrationService {
    public subject = new Subject<any>();

    constructor(private http: Http,
        private configService: ConfigService) {

    }


    getCelebrations(obj) {
        let url = this.configService.getApiUrls().getCelebrations;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };
}