	import { CelebrationModule } from "./celebrations.module";
	import {
	Component,
	OnInit,
	Input,
	OnDestroy,
	OnChanges,
	NgZone,
	ViewChild
	} from "@angular/core";
	import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
	import {
	FormGroup,
	FormBuilder,
	Validators,
	FormControl
	} from "@angular/forms";
	import { ConfigService } from "../shared-services/config.service";
	import { Http, Headers, RequestOptions } from "@angular/http";
	import { AppFooterComponent } from "../footer/app-footer.component";
	import { ClientFooterComponent } from "../footer/client-footer.component";
	import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
	import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
	import { CelebrationService } from "./celebrations.service";
	import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
	import { SidebarComponent } from "../home/sidebar/sidebar.component";
	import { StringService } from "../shared-services/strings.service";
	import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

	@Component({
	templateUrl: "./celebration.component.html",
	styleUrls: ["../app.component.scss", "./celebration.component.scss"],
	providers: [Device]
	})
	export class CelebrationComponent implements OnInit {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	noRecords: boolean = false;
	programUserInfo: any;
	sliders1: { imagePath: string; label: string; text: string }[];
	celebrationslist: any;
	eventGallery: boolean;
	eventslist: any;
	showLoader: boolean;
	public alerts: Array<any> = [];
	public sliders: Array<any> = [];
	public eventImages: Array<any> = [];
	public image: Array<any> = [];

	public imagesArray:any[] = [];
	public videosArray:any[] = [];


	constructor(
		private router: Router,
		private configService: ConfigService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private route: ActivatedRoute,
		private http: Http,
		private celebrationService: CelebrationService,
		public sidebar: SidebarComponent,
		private stringService: StringService,
		private device: Device,
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
		this.displaypc = false;
		this.displaymobile = true;
		console.log("display mobile val", this.displaymobile);
		} else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
		this.displaypc = false;
		this.displaymobile = true;
		} else {
		this.displaypc = true;
		this.displaymobile = false;
		}

		// google analytics page view code
		if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Celebrations")
        } else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga("set", "page", "Celebrations");
					ga("send", "pageview");
				}
			});
		}
	

		this.programUserInfo = configService.getloggedInProgramUser();

		this.sliders1 = [
		// {
		//     imagePath: 'assets/1.JPG',
		//     label: 'First slide',
		//     text:
		//         'Marathon in Maleswaram - 2018'
		// },
		{
			imagePath: "assets/slider2.jpg",
			label: "First slide",
			text: "Cooking Contest 2018"
		},
		{
			imagePath: "assets/slider3.jpg",
			label: "Second slide",
			text: "Beauty Peagent Contest - 2018"
		}
		];
	}

	ngOnInit() {
		this.stringService.setImageFormats();
        this.stringService.setVideoFormats();
		this.getCelebrations();

		this.sidebar.close();
		this.sidebar.addExpandClass("pages");
		this.sidebar.addExpandClass1("pages1");
	}

	getCelebrations() {
		var obj = {
		programId: this.configService.getprogramInfo().programId,
		clientId: this.configService.getprogramInfo().clientId,
		programRoleId: this.programUserInfo.programRole,
		serviceType: "programSetup"
		};

		if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswDealerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole;
		} else if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswDistributorAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole;
		} else if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswEngineerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		} else if (
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswASMAdminProgramRole ||
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswCMProgramRole ||
		this.programUserInfo.programRole ===
			this.stringService.getStaticContents().jswRCMProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		}
		console.log("obj", obj);
		this.showLoader = true;

		// In case of engineer admin
		if (
		this.programUserInfo.programRole ===
		this.stringService.getStaticContents().jswEngineerAdminProgramRole
		) {
		obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;
		}

		this.celebrationService.getCelebrations(obj).subscribe(
		responseObject => {
			this.showLoader = false;

			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
			case responseCodes.RESP_ROLLBACK_ERROR:
				break;
			case responseCodes.RESP_SERVER_ERROR:
				break;
			case responseCodes.RESP_SUCCESS:
				console.log("Celebration ResposneObject", responseObject);
				this.celebrationslist = responseObject.result;
				for (var a = 0; a < this.celebrationslist.length; a++) {
				console.log("a",a);
				console.log("this.celebrationslist[a]",this.celebrationslist[a]);
				if (this.celebrationslist[a].imageArray) {
					for (
					var b = 0; 
					b < this.celebrationslist[a].imageArray.length;
					b++
					) {
					var obj = { 
						imagePath: "",
						text: ""
					};
					obj.imagePath = this.celebrationslist[a].imageArray[b];
					obj.text = this.celebrationslist[a].title;
					// this.eventImages.push(obj.imagePath);
					this.sliders.push(obj);
					this.eventGallery = true;
					}
				}
				console.log("this.sliders Celebrations", this.sliders);
				}


				// to separate images and videos 
				for(let i=0;i<this.celebrationslist.length;i++){

					this.celebrationslist[i].images = [];
					this.celebrationslist[i].videos = [];

					// to covert date into readable format
					var date = new Date(this.celebrationslist[i].celebrationDate);
					var startDate = date.toDateString();
					this.celebrationslist[i].celebrationDate = startDate;

					for(let j=0;j<this.celebrationslist[i].imageArray.length;j++){
						// to extract and store extension from the media
						var ext: string = this.celebrationslist[i].imageArray[j].slice(this.celebrationslist[i].imageArray[j].lastIndexOf('.'),this.celebrationslist[i].imageArray[j].length).toLowerCase();
						console.log("ext",ext);
						
						var imageFormats=this.stringService.getImageFormats();
						var videoFormats=this.stringService.getVideoFormats();
						var isFound=false; //set true if type as image is found

						// to check for image
						for (let x = 0; x < imageFormats.length; x++) {
							console.log("imageFormats[x]",imageFormats[x]);
							if (ext === imageFormats[x]) {
								console.log("MATCHED!!");
								this.celebrationslist[i].images.push(this.celebrationslist[i].imageArray[j]);
								isFound=true; 
								break;
							}
						}

						// to check for videos
						if(isFound === false){ // to avoid checking for video if already found as image

							for (let x = 0; x < videoFormats.length; x++) {
								console.log("videoFormats[x]",videoFormats[x]);
								if (ext === videoFormats[x]) {
									console.log("MATCHED!!");
									this.celebrationslist[i].videos.push(this.celebrationslist[i].imageArray[j]);
									break;
								}
							}
						}
					}

					console.log("final images array",this.celebrationslist[i].images);
					console.log("final videos array",this.celebrationslist[i].videos);
				}

				break;
			case responseCodes.RESP_AUTH_FAIL:
				break;
			case responseCodes.RESP_FAIL:
				this.noRecords = true;
				break;
			case responseCodes.RESP_ALREADY_EXIST:
				break;
			}
		},
		err => {
			this.showLoader = false;
		}
		);
	}

	viewGallery(celebration){
		this.googleAnalyticsEventsService.emitEvent("Celebrations", "viewGallery", celebration.title);
		console.log("celebration",celebration);
		this.configService.setCelebration(celebration);
		this.router.navigate(['./home/celebrations-details']);
	}
}
