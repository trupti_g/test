

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "@angular/material";
import { FooterModule } from "../footer/footer.module";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { SharedModule } from "../shared-components/shared.module";
import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
import { CelebrationComponent } from "./celebration.component";
import { CelebrationService } from "./celebrations.service";
import { CelebrationDetailsComponent } from "./celebration-details.component"



@NgModule({
    declarations: [CelebrationComponent, CelebrationDetailsComponent],

    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpModule,
        MaterialModule,
        FooterModule,
        ChartsModule,
        AmChartsModule,
        SharedModule,
        NgbCarouselModule.forRoot()
    ],
    exports: [],

    providers: [CelebrationService],
    entryComponents: []
})
export class CelebrationModule { }
