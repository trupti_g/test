/*
    Author: Akash Narkhede.
    Description: Service to set and get product details.
*/

import { Injectable, OnInit } from "@angular/core";

@Injectable()

export class ProductDetailsService {
    private productArr: any = [];
    private placeOrderArr: any = [];
    private productQuantity: any = [];
    private cartCount: number = 0;

    setProduct(productArr) {
        this.productArr = productArr;
        console.log("show productArr", this.productArr);
        this.cartCount= this.productArr.length;
        localStorage.setItem("productDetails", JSON.stringify(productArr));
        console.log("ProductArr in SetProduct Function Product-Details-Service", this.productArr, this.productArr.length);
    }

    getProduct() {
        console.log("ProductArr in GetProduct Function Product-Details-Service", this.productArr);
        if (JSON.parse(localStorage.getItem("productDetails")).length>0){
            return JSON.parse(localStorage.getItem("productDetails"));
        }
        return this.productArr;
    }

    setPlaceOrderProduct(placeOrderArr) {
        this.placeOrderArr = placeOrderArr;
        localStorage.setItem("placedProductDetails", JSON.stringify(placeOrderArr));
        console.log("PlaceOrderArr in setPlaceOrderProduct Function Product-Details-Service", this.placeOrderArr);
    }

    getPlaceOrderProduct() {
        return this.placeOrderArr;
    }

    setCartCount(cartCount) {
        this.cartCount = cartCount;
        console.log("Cart Count in setCartCount : ", this.cartCount);
    }

    getCartCount() {
        console.log("Cart Count in getCartCount : ", this.cartCount);
        return this.cartCount;
    }
}