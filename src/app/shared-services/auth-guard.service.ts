/*
    Author           :     Deepak Terse
    Description      :     Service to implement application access i.e. it should prevent user from accessing inner 
                           modules if he is not logged in
    Date Created     :     08 March 2016
    Date Modified    :     08 March 2016
*/

import { CanActivate, RouterStateSnapshot, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
        if (localStorage.getItem("loggedInUserInfo") !== null) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
