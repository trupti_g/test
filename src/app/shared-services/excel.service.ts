import { Injectable } from '@angular/core';



const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

interface Window {
    BlobBuilder: any;
    WebKitBlobBuilder: any;
    MozBlobBuilder: any;
    MSBlobBuilder: any;
    URL: any;
}

declare var window: Window;

@Injectable()

export class ExcelService {

    constructor() {
        // Blank Constructor for Demo Purpose
    }


    // Download CSV
    download(data: any, filename: string) {
        console.log("In dowload function dkjkdnkjndkndkvnkkddnvkdvkkvnk")

        var csvData = this.ConvertToCSV(data);
        var a: any = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        //var blob = new Blob([csvData], { type: 'text/csv' });


        // FOR MOBILE APP
        try {
            var blob = new Blob([csvData], { type: 'application/vnd.ms-excel' });
            //var jpeg = new Blob( [array], {type : "image/jpeg"});
        }
        catch (e) {
            // TypeError old chrome and FF
            console.log("Excepton", e);
            window.BlobBuilder = window.BlobBuilder ||
                window.WebKitBlobBuilder ||
                window.MozBlobBuilder ||
                window.MSBlobBuilder;
            if (e.name == 'TypeError' && window.BlobBuilder) {
                //     var bb = new BlobBuilder();
                //     var jpblobeg = bb.getBlob('text/csv');
                console.log("1st IFFFFFFFFFFFFFFFf");

            }
            else if (e.name == "InvalidStateError") {
                // InvalidStateError (tested on FF13 WinXP)
                var blob = new Blob([csvData], { type: 'application/vnd.ms-excel' });
            }
            else {
                // We're screwed, blob constructor unsupported entirely   
                console.log("We're screwed, blob constructor unsupported entirely");
            }
        }




        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.target = "_blank";
        var isIE = /*@cc_on!@*/false || !!(<any>document).documentMode;

        if (isIE) {
            var retVal = navigator.msSaveBlob(blob, filename + '.xlsx');
            console.log("isIE     adadadadadadadadadadadadad")
        }
        else {
            a.download = filename + '.xlsx';
            console.log("else     adadadadadadadadadadadadad")
        }
        // If you will any error in a.download then dont worry about this. 
        a.click();
    }


    // convert Json to CSV data
    ConvertToCSV(objArray: any) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = "";

        for (var index in objArray[0]) {
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += '"' + array[i][index] + '"';
            }

            str += line + '\r\n';
        }

        return str;
    }
}