import { Injectable, OnInit, EventEmitter, ViewChild } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { ConfigService } from "./config.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { RewardsGalleryService } from "../rewards-gallery/rewards-gallery.service";
import { FeedbackMessageComponent } from '../feedback-message.component';
import { GoogleAnalyticsEventsService } from "./google-analytics-events.service";

import { Observable, Subject } from "rxjs";

@Injectable()
export class CartService {

    public cartArray: any[] = [];

    private programUserInfo: any;
    public programInfo: any;
    public RGType: string = "";
    public stopLoader: any = new Subject();
    public feedback: any = new Subject();
    public cartItemsUpdated: any = new Subject();
    public feedbackComponent: any;
    private productName: any = null;


    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private http: Http,
        private configService: ConfigService,
        private globalRewardGalleryService: GlobalRewardGalleryService,
        private rewardsGalleryService: RewardsGalleryService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService
    ) {

        this.programInfo = this.configService.getloggedInProgramInfo();
        if (this.programInfo !== null) {

            let RGType = this.programInfo.useGlobalRewardGallery ? "GlobalRG" : "ProgramSpecificRG";
            this.setFields(RGType);
        }
    }

    setFields(RGType) {
        if (this.configService.getloggedInProgramInfo()) {
            this.RGType = RGType;

            this.programInfo = this.configService.getloggedInProgramInfo();

            if (this.configService.getloggedInProgramUser()) {
                this.programUserInfo = this.configService.getloggedInProgramUser();
                this.getCartFormReqObj(this.productName);
            }
        }
    }

    /*******************************    UPDATE TO CART      ******************************/
    clearCart() {
        let reqObj = {
            "findCriteria": {
                "userId": this.programUserInfo.programUserId,
                "programId": this.programInfo.programId,
                "type": this.RGType,
                "productStatus": "Pending"
            },
            "updateInfo": {
                "productStatus": "Placed",
            }
        }
        this.updateToCart(reqObj);
    }

    updateToCartFormReqObj(productId: string, productStatus: string, quantity?: number) {
        let reqObj = {
            "findCriteria": {
                "userId": this.programUserInfo.programUserId,
                "programId": this.programInfo.programId,
                "type": this.RGType,
                "productId": productId,
                "productStatus": productStatus
            },
            "updateInfo": {
                "quantity": quantity
            }
        }
        this.updateToCart(reqObj);
    }
    updateToRemoveCartFormReqObj(productId: string, productStatus: string, quantity?: number) {
        console.log("inside to remove");
        let reqObj = {
            "findCriteria": {
                "userId": this.programUserInfo.programUserId,
                "programId": this.programInfo.programId,
                "type": this.RGType,
                "productId": productId,

            },
            "updateInfo": {
                "productStatus": productStatus,
                "quantity": quantity
            }
        }
        console.log("inside to remove", reqObj);
        this.updateToCart(reqObj);
    }

    updateToCart(reqObj) {
        this.updateToCartAPI(reqObj)
            .subscribe((responseObject) => {
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.getCartFormReqObj(this.productName);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.cartArray = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }
    /***************************    END OF UPDATE TO CART   ******************************/



    /*******************************    ADD TO CART      ******************************/
    addToCartFormReqObj(productId, productName) {
        console.log(this.configService.getloggedInProgramInfo());
        let reqObj = {
            "addInfo": {
                "userId": this.programUserInfo.programUserId,
                "programId": this.programInfo.programId,
                "type": this.RGType,
                "productStatus": "Pending",
                "productId": productId,
                "productName": productName,
                "quantity": 1
            }
        }

        this.addToCart(reqObj);
    }

    addToCart(reqObj) {
        console.log("IN ADD TO CART API");

        this.addToCartAPI(reqObj)
            .subscribe((responseObject) => {
                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("ADDED TO CART--", reqObj.addInfo.productName);
                        this.googleAnalyticsEventsService.emitEvent("Cart", "Added to cart", reqObj.addInfo.productName);
                        this.feedback.next(reqObj.addInfo.productName);
                        var productName = reqObj.addInfo.productName;
                        this.getCartFormReqObj(productName);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }
    /***************************    END OF ADD TO CART   ******************************/




    /*******************************    CART REFRESH     *******************************/
    getCartFormReqObj(productName) {
        let reqObj = {
            "findCriteria": {
                "userId": this.programUserInfo.programUserId,
                "programId": this.programInfo.programId,
                "type": this.RGType,
                "productStatus": "Pending"
            }
        }
        this.getCart(reqObj, productName);
    }

    getCart(reqObj, productName) {
        console.log("---------------------inside get cart function");
        this.getCartAPI(reqObj)
            .subscribe((responseObject) => {
                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("reponse", responseObject.result);
                        this.getCartProductFormReqObj(responseObject.result, productName);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        //added here to get subtotal zero.
                        this.getCartProductFormReqObj(responseObject.result, productName);
                        console.log("********INSIDE GET CART FAILURE***********")
                        this.cartArray = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }

    getCartProductFormReqObj(cartArray, productName) {
        console.log("------------In getCartProductFormReqObj");
        let productIdArray: any[] = [];
        for (let i = 0; i < cartArray.length; i++) {
            productIdArray.push(cartArray[i].productId);
        }

        if (this.RGType === "GlobalRG") {
            let globalProdReqObj: any = {
                "clientId": this.programInfo.clientId,
                "serviceType": "programSetup",
                "frontendUserInfo": this.configService.getFrontEndUserInfo(),
                "productArray": productIdArray
            };
            this.getGlobalProducts(globalProdReqObj, cartArray);

        }
        else if (this.RGType === "ProgramSpecificRG") {
            let specificProdReqObj: any = {
                "programId": this.programInfo.programId,
                "clientId": this.programInfo.clientId,
                "serviceType": "programSetup",
                "productArray": productIdArray,
                "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            };
            this.getSpecificProducts(specificProdReqObj, cartArray, productName);

        }
    }

    private getSpecificProducts(specificProdReqObj, cartArray, productName) {
        console.log("-------------In getSpecificProducts");
        this.rewardsGalleryService.getSpecificRewardsGalleryProduct(specificProdReqObj).subscribe(
            (responseObject) => {
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        let productList: any[] = responseObject.result;
                        for (let i = 0; i < cartArray.length; i++) {
                            for (let j = 0; j < productList.length; j++) {
                                if (cartArray[i].productId === productList[j].productId) {
                                    cartArray[i].productInfo = productList[j];
                                    console.log("inside cartservice PRODUCT ID----", productList[j].productId);
                                }
                            }
                        }
                        console.log("REQUESTOBJ----", specificProdReqObj);
                        this.cartArray = cartArray;

                        this.stopLoader.next(productName);

                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.cartArray = [];
                        this.stopLoader.next(this.cartArray);
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_KYC_AWAITING:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.stopLoader.next();
            }
        );
    }

    private getGlobalProducts(globalProdReqObj, cartArray) {
        this.globalRewardGalleryService.getGlobalProductMaster(globalProdReqObj).subscribe(
            (responseObject) => {

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        let productList: any[] = responseObject.result;
                        for (let i = 0; i < cartArray.length; i++) {
                            for (let j = 0; j < productList.length; j++) {

                                if (cartArray[i].productId === productList[j].productId) {
                                    cartArray[i].productInfo = productList[j];
                                }
                            }
                        }
                        this.cartArray = cartArray;
                        console.log("inside get cart success");
                        console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", this.cartArray);
                        this.stopLoader.next(this.cartArray);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.cartArray = [];
                        this.stopLoader.next(this.cartArray);
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_KYC_AWAITING:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.stopLoader.next();
            }
        );
    }
    /****************************   END OF CART REFRESH  *******************************/


    /****************************   API CALLS    ************************************/
    addToCartAPI(reqObj) {
        let url = this.configService.getApiUrls().addToCart;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });
        console.log(url, reqObj, options);
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }

    getCartAPI(reqObj) {
        let url = this.configService.getApiUrls().getCart;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }

    updateToCartAPI(reqObj) {
        let url = this.configService.getApiUrls().updateToCart;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    /*************************  END OF API CALLS     *****************************/
}