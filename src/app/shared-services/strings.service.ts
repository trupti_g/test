/*
	Author			:	Deepak Terse
	Description		: 	Service to set and get all error messages, labels and strings required in the app
	Date Created	: 	08 March 2016
	Date Modified	: 	08 March 2016
*/

import { Injectable, OnInit } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { ConfigService } from "./config.service";

/*@Injectable :
    It lets Angular know that a class can be used with the dependency injector.
*/
@Injectable()
export class StringService implements OnInit {

	private regex: any;

	private responseMessages: any;
	private errorMessages: any;

	private fieldLabels: any;
	private buttonLabels: any;
	private staticContents: any = {};

	private imgFormats: any;
	private videoFormats: any;

	private categories: any = {};

	private month: any = [];
	private year: any = [];

	constructor(private configService: ConfigService) {
		this.setResponseMessage();
		this.setErrorMessage();
		this.setfieldLabels();
		this.setButtonLabels();
		this.setStaticContents();
		this.setRegex();
	}

	ngOnInit() {
		console.log("initialising the string service", this.configService.getTerminology(), this.configService.getTerminology());
		this.staticContents.point = this.configService.getTerminology().slice(0, this.configService.getTerminology().length - 2);
		console.log("terminology point or coupon", this.staticContents.point);
	}

	/**************************************		REGEX SETTER GETTER 	********************************/
	setRegex() {
		this.regex = {};
		this.regex.emailRegex = "/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/";

		// this.regex.alphaRegex = "^[a-zA-Z][a-zA-Z0-9\\s]+$";
		this.regex.alphaRegex = "^[a-zA-Z]+$";

		this.regex.alphaNumRegex = "^[a-zA-Z0-9][a-zA-Z0-9\\s]+$";
		this.regex.mobNumRegex = '[0-9]{10}';
		this.regex.contactNumRegex = "[0-9]{8,12}";
		this.regex.pincodeRegex = "[0-9]{6}";
	}

	getRegex() {
		return this.regex;
	}
	/*******************************		END OF REGEX SETTER GETTER 	********************************/

	/************************************** 	MESSAGES SETTER GETTER 		*************************************/
	setResponseMessage() {
		this.responseMessages = {};

		this.responseMessages.invalidUsernamePasswordMsg = "Invalid username/password !!!";
		this.responseMessages.userBlockedMsg = "User is Blocked!!!, Please contact Administrator.";
	}

	getResponseMessage() {
		return this.responseMessages
	}

	setErrorMessage() {
		this.errorMessages = {};
		this.errorMessages.userNameReq = "Please enter your User Name";

		//Common error messages
		this.errorMessages.streetAddressReq = "Street address is required";
		this.errorMessages.areaReq = "Area is required";
		this.errorMessages.landmarkReq = "Landmark is required";
		this.errorMessages.stateReq = "State is required";
		this.errorMessages.cityReq = "City is required";
		this.errorMessages.pincodeReq = "Pincode is required";

		this.errorMessages.invalidUserName = "Invalid User Name";
		this.errorMessages.invalidFirstName = "Invalid First Name";
		this.errorMessages.invalidLastName = "Invalid Last Name";
		this.errorMessages.invalidEmail = "Invalid Email Address";
		this.errorMessages.invalidPincode = "Invalid Pincode";
		this.errorMessages.invalidMobileNumber = "Invalid Mobile Number";

		this.errorMessages.emailPattern = "Please enter a valid email";

		//Login component
		this.errorMessages.usernameReq = "Please enter your username.";
		this.errorMessages.passwordReq = "Please enter your password.";

		// this.errorMessages.kycAwaiting = "Your KYC is not Verified yet.";
		this.errorMessages.kycAwaiting = "Your registration is pending for approval.";
		this.errorMessages.invalidCredentials = "Username or password is incorrect";
		this.errorMessages.blocked = "Your Account has being blocked";

		//My profile
		this.errorMessages.businessNameReq = "Please enter an outlet name";
		this.errorMessages.registrationNumberReq = "Please enter a registration number";

		this.errorMessages.firstNameReq = "Please enter your First name";
		this.errorMessages.firstNameMinLength = "First name should not be less than 2 characters";
		this.errorMessages.firstNameMaxLength = "First name should not be more than 30 characters";
		this.errorMessages.firstNameCannotBeANumber = "First name cannot contain a number";

		this.errorMessages.lastNameReq = "Please enter your Last name";
		this.errorMessages.lastNameMinLength = "Last name should not be less than 2 characters";
		this.errorMessages.lastNameMaxLength = "Last name should not be more than 30 characters";
		this.errorMessages.firstNameCannotBeANumber = "First name cannot contain a number";

		this.errorMessages.dateOfBirthReq = "Please enter your date of birth";
		this.errorMessages.genderReq = "Please select your gender";
		this.errorMessages.maritalStatusReq = "Please enter a marital status";
		this.errorMessages.marriageAnniversaryReq = "Please enter a marriage anniversary";

		this.errorMessages.mobileNumberReq = "Please enter your Mobile Number";
		this.errorMessages.mobileNumberPattern = "Please enter a valid Mobile Number";

		this.errorMessages.emailReq = "Please enter your email address";
		this.errorMessages.emailPattern = "Please enter a valid email address";

		this.errorMessages.streetAddressReq = "Please enter your street address";
		this.errorMessages.streetAddressMinLength = "Street address should be more than 5 characters";
		this.errorMessages.streetAddressMaxLength = "Street address cannot exceed 150 characters";

		this.errorMessages.areaReq = "Please enter your area";
		this.errorMessages.areaMinLength = "Area should be more than 5 characters";
		this.errorMessages.areaMaxLength = "Area cannot exceed 150 characters";

		this.errorMessages.landmarkReq = "Please enter your landmark";
		this.errorMessages.landmarkMinLength = "Landmark should be more than 5 characters";
		this.errorMessages.landmarkMaxLength = "Landmark cannot exceed 150 characters";



		//Sign Up Form
		this.errorMessages.signupOutletName = "Please enter your Outlet name";
		this.errorMessages.signupfirstNameReq = "Please enter your First name";
		this.errorMessages.signuplastNameReq = "Please enter your Last name";
		this.errorMessages.signupOutletNameMaxLength = "Outlet name can't exceed 80 characters";
		this.errorMessages.signupOutletNameMinLength = "Outlet name should be more than 4 characters";
		this.errorMessages.signupStreetAddressReq = "Street Address is required.";
		this.errorMessages.signupAreaReq = "Area is required.";
		this.errorMessages.signupLandmarkReq = "Landmark is required.";
		this.errorMessages.signupMaxLength = "Can't exceed 80 characters";
		this.errorMessages.signupMinLength = "Should Be more  than 3 characters";
		this.errorMessages.firstNameReq = "Please enter your First name";
		this.errorMessages.lastNameReq = "Please enter your Last name";
		this.errorMessages.mobileNumReq = "Please enter your Mobile Number";
		this.errorMessages.emailReq = "Please enter your Email Address";
		this.errorMessages.birthDateReq = "Please enter your Date of birth";
		this.errorMessages.genderReq = "Please select your gender";
		this.errorMessages.maritalStatusReq = "Please select your marital status";

		// SIGN UP PASSWORD FORM

		this.errorMessages.signupPasswordMaxLength = "Can't exceed 80 characters";
		this.errorMessages.signupPasswordMinLength = "Should be more than 4 characters";
		this.errorMessages.signupPasswordMismatch = "Passwords do not match";
		this.errorMessages.signupPasswordRequired = "Password is required";

		//Claims Form 
		this.errorMessages.invoiceNumberReq = "Invoice Number is required";
		this.errorMessages.transactionDateReq = "Transaction Date is required";
		this.errorMessages.invoiceAmountReq = "Invoice Amount is required";
		this.errorMessages.invoiceAmountExcludingTaxReq = "Invoice Amount Excluding GST is required";
		this.errorMessages.distributorReq = "Select or Search a Distributor";
		this.errorMessages.invoiceUploadReq = "Invoice Upload is required";
		this.errorMessages.vatPercentageReq = "GST Percentage is required";
		this.errorMessages.distributorMobileNumberReq = "Distributor Mobile Number is required";

		this.errorMessages.distributorMobileNumberInvalid = "Distributor Mobile Number is invalid";


		this.errorMessages.productNameReq = "Product Name is required";
		this.errorMessages.brandNameReq = "Brand Name is required";
		this.errorMessages.skuReq = "SKU is required";
		this.errorMessages.packagingUnitReq = "Packaging Unit is required";
		this.errorMessages.quantityReq = "Quantity is required";
		this.errorMessages.discountReq = "Discount is required";
		this.errorMessages.mrpReq = "MRP is required";

		this.errorMessages.mandatoryField = "This field is mandatory.";

		//LeadManagement 
		this.errorMessages.ProjectName = "Project Name is required";
		this.errorMessages.OwnerName = "Builder Name is required";
		this.errorMessages.ProjectType = "Project Type is required";
		this.errorMessages.Expectedvalue = "Expected Tonnage is required";
		this.errorMessages.Address = "Address is required";
		this.errorMessages.City = "City is required";
		this.errorMessages.District = "District is required";
		this.errorMessages.State = "State is required";
		this.errorMessages.PinCode = "PinCode is required";

		this.errorMessages.AcName = "Name is required";
		this.errorMessages.AcVenue = "Venue is required";
		this.errorMessages.AcBudget = "Budget is required";
		this.errorMessages.AcStartDate = "Start Date is required";
		this.errorMessages.AcStartTime = "Start Time is required";
		this.errorMessages.AcEndDate = "End Date is required";
		this.errorMessages.AcEndTime = "End Time is required";
		this.errorMessages.Role = "Role is required";
		this.errorMessages.Count = "Count is required";
		this.errorMessages.ReportDetails = "Report Details is required";
		this.errorMessages.Description = "Description is required";
		this.errorMessages.Cost = "Cost is required";
		this.errorMessages.Contact = "Contact is required";
		//leads Report 
		this.errorMessages.From = "From Date is required";
		this.errorMessages.ToDate = "To Date is required";
		this.errorMessages.leadCode = "Lead Code is required";
		this.errorMessages.projectOwner = "Project Owner is required";
		this.errorMessages.influencer = "Influencer is required";
		//project information
		this.errorMessages.contactname = "Contact Name is required";
		this.errorMessages.contactrole = "Contact Role is required";
		this.errorMessages.contactcontact = 'Contact is required';
		//
		this.errorMessages.vcname = "Provider Name is required";
		this.errorMessages.vccontact = "Provider Contact Number is required";
		this.errorMessages.vcdate = "Provided Date is required";
		this.errorMessages.createdby = "Created By is required";
		this.errorMessages.createdbycontact = "Created Contact is required";
		this.errorMessages.createdbyrole = "Created By Role is required";
		this.errorMessages.contactNumMinLength = "contact number should be atleast of 10 numbers";
		this.errorMessages.contactNumMaxLength = "contact number should not be more than 12 numbers";
		this.errorMessages.contactNumberPattern = "Please enter a valid contact number";

	}

	getErrorMessage() {
		return this.errorMessages
	}

	/*********************************** 	END OF MESSAGES SETTER GETTER 	*************************************/

	/***********************************		LABELS SETTER GETTER 		**************************************/
	setfieldLabels() {
		this.fieldLabels = {};


		//Login component
		this.fieldLabels.username = "Username:";
		this.fieldLabels.password = "Password:";

		//My Profile
		this.fieldLabels.firstName = "First Name";
		this.fieldLabels.lastName = "Last Name";
		this.fieldLabels.email = "Email";
		this.fieldLabels.mobileNo = "Mobile Number";
		this.fieldLabels.altMobileNum = "Alternate Mobile Number";
		this.fieldLabels.streetAddress = "Street Address";
		this.fieldLabels.area = "Area";
		this.fieldLabels.landmark = "Landmark";
		this.fieldLabels.streetAddress = "Street Address";
		this.fieldLabels.area = "Area";
		this.fieldLabels.pincode = "Pincode";
		this.fieldLabels.gender = "Gender";
		this.fieldLabels.maritalStatus = "Marital Status";
		this.fieldLabels.birthDate = "Birth Date";
		this.fieldLabels.employeeId = "Employee ID";
		this.fieldLabels.designation = "Designation";
		this.fieldLabels.division = "Division";
		this.fieldLabels.parentUser = "Parent User";

		// Sign Up Form
		this.fieldLabels.firstName = "*First Name:";
		this.fieldLabels.lastName = "*Last Name:";
		this.fieldLabels.email = "*Email:";
		this.fieldLabels.mobileNo = "*Mobile Number:";
		this.fieldLabels.streetAddress = "*Street Address:";
		this.fieldLabels.area = "Area:";
		this.fieldLabels.landmark = "Landmark:";
		this.fieldLabels.pincode = "*Pincode:";
		this.fieldLabels.outletName = "*Outlet Name:";
		this.fieldLabels.state = "*State:";
		this.fieldLabels.city = "*City:";
		this.fieldLabels.signUp = "SIGN UP";
		this.fieldLabels.close = "CLOSE";

		// SIGN UP  OTP FORM
		this.fieldLabels.otp = "OTP:";
		this.fieldLabels.resendOtp = "Resend OTP";
		this.fieldLabels.submitOtp = "SUBMIT";

		// Set Password Form
		this.fieldLabels.userName = "User Name:";
		this.fieldLabels.password = "Password:";
		this.fieldLabels.confirmPassword = "Confirm Password:";
		this.fieldLabels.submit = "SUBMIT";

		//Claims Form 
		this.fieldLabels.invoiceNumber = "Invoice Number";
		this.fieldLabels.transactionDate = "Transaction Date";
		this.fieldLabels.invoiceAmount = "Invoice Amount Including GST";
		this.fieldLabels.invoiceAmountExcludingTax = "Invoice Amount Excluding GST";
		this.fieldLabels.distributor = "Distributor";
		this.fieldLabels.invoiceUpload = "Invoice Upload";
		this.fieldLabels.vatPercentage = "GST Percentage";
		this.fieldLabels.productName = "Product Name";
		this.fieldLabels.brandName = "Brand Name";
		this.fieldLabels.sku = "SKU";
		this.fieldLabels.packagingUnit = "Packaging Unit";
		this.fieldLabels.quantity = "Quantity";
		this.fieldLabels.discount = "Discount(%)";
		this.fieldLabels.mrp = "MRP";
		this.fieldLabels.skuDetails = "SKU Details :";
		this.fieldLabels.pointsEarned = "Sales ";
		this.fieldLabels.boosterPoints = "Booster ";

		// MyCLaims		 Details/ Pop Up / Invoice
		this.fieldLabels.invoiceDate = "Invoice Date";
		this.fieldLabels.uploadDate = "Upload Date";
		this.fieldLabels.total = "Total";
		this.fieldLabels.totalVat = "Total Excluding GST";
		this.fieldLabels.status = "Status";
		this.fieldLabels.unitPrice = "Unit Price";
		this.fieldLabels.packagingUnit = "Packaging Unit";
		this.fieldLabels.discounts = "Discount %";
		this.fieldLabels.downloadHardCopy = "Download Soft Copy >>";
		this.fieldLabels.productName = "Product Name";
		this.fieldLabels.brandName = "Brand Name";
		this.fieldLabels.quantity = "Quantity";
		this.fieldLabels.productName = "Product Name";
		this.fieldLabels.productId = "Product Id";
		this.fieldLabels.packagingUnitId = "Packaging Unit Id";
		this.fieldLabels.packagingUnit = "Packaging Unit";


		// DIGITAL Details/Popup

		this.fieldLabels.claimId = "Claim ID";
		this.fieldLabels.claimDate = "Claim Date";
		this.fieldLabels.productName = "Product Name";
		this.fieldLabels.packagingUnit = "Packaging Unit";


		//Unicode CLaims
		this.fieldLabels.uniqueCode = "Enter Coupon Code";

		//Display Contest
		this.fieldLabels.contestName = "Contest Name:";
		this.fieldLabels.startDate = "Start Date:";
		this.fieldLabels.endDate = "End Date:";
		this.fieldLabels.contestDescription = "Contest Description:";
		this.fieldLabels.contestTermsAndConditions = "Terms And Conditions:";

		// Offer Details
		this.fieldLabels.offerName = "Offer Name:";
		this.fieldLabels.offerDescription = "Offer Description:";
	}

	getfieldLabels() {
		return this.fieldLabels;
	}
	setPoints(points) {
		this.fieldLabels.points = points;
		this.staticContents.points = points;
		this.staticContents.point = this.staticContents.points.substr(0, this.staticContents.points.length - 1);
		console.log("terminology point or coupon", this.staticContents.point);
		console.log("initialising field labels in String SErvice ))))))))))))))))++++++++++++++", this.staticContents.point, this.fieldLabels.points, this.staticContents.points, this.configService.getTerminology());
		this.setStaticContents();
	}

	setButtonLabels() {
		this.buttonLabels = {};

		//Login component
		this.buttonLabels.login = "LOGIN";
		this.buttonLabels.signup = "SIGNUP";

		//My Claims Detail/Pop Up 

		this.buttonLabels.close = "CLOSE";



	}

	getButtonLabels() {
		return this.buttonLabels;
	}

	setStaticContents() {
		//this.staticContents = {};
		this.staticContents.secondarySalesId = "Secondary Sales ID";
		this.staticContents.transactionDate = "Transaction Date";
		//App component
		this.staticContents.h1PoweredBy = "Powered by";
		this.staticContents.h1ProductCatalog = "Product Catalog";
		this.staticContents.h1About = "About the Program";
		this.staticContents.h1Testimonial = "Testimonial";
		this.staticContents.h1Welcome = "Welcome, ";
		this.staticContents.h1ProgramTiers = "Program Tiers";
		this.staticContents.h1Privilege = "Privileges";

		this.staticContents.f2Text1 = " +91 9686202046 / 9972334590 ";
		this.staticContents.f2Text1A = "(9:30am to 6:30pm, Mon-Fri)";
		this.staticContents.f2Text2 = "Rewards are subject to availability and supply restrictions. In case exact model" +
			" or merchandise mentioned in catalogue is not available at the time of redemption" +
			" program center will intimate the member of such a status an" +
			"d arrange/offer an alternate model of the merchandise.";
		this.staticContents.f2Terms = "Terms of Use";
		this.staticContents.f2Security = "Security";
		this.staticContents.f2Policy = "Privacy Policy";
		this.staticContents.f2Copyright = "annectoś Rewards & Retail Pvt Ltd – ";
		this.staticContents.f2Year = " 2017";


		//Login component
		this.staticContents.loginWelcome = "Welcome to our Rewards Program";
		this.staticContents.loginForgotPassword = "Forgot Password?";
		//this.staticContents.loginSignup = "Not a user? Sign up for our Rewards Program";
		this.staticContents.loginSignup = "Register";
		this.staticContents.loginSignUpButton = "SIGN UP";
		this.staticContents.loginButton = "LOGIN";
		this.staticContents.forgotPassword = "Forgot Password?"

		//Sign up component
		this.staticContents.h3header = "Annectos Rewards Logo";
		this.staticContents.signupTitle = "Sign up for our Rewards Programs";
		this.staticContents.signupInnerTitle = "Please provide the following details to help us register you in the program:";
		this.staticContents.loginSignUpButton = "SIGN UP";
		this.staticContents.loginButton = "LOGIN";
		this.staticContents.forgotPassword = "Forgot Password?"

		//Sign up OTP component
		this.staticContents.h3header = "Annectos Rewards Logo";
		this.staticContents.otpTitle = "Enter OTP";
		this.staticContents.otpBackTitle = "< Change your registration details";
		this.staticContents.otpInnerTitle1 = "Thank you for providing your details. An OTP has been sent to the phone number y" +
			"ou provided - ";
		this.staticContents.otpInnerTitle2 = ". Please add the OTP to proceed.";
		this.staticContents.resendOtp = "Resend OTP";

		this.staticContents.loginSignUpButton = "SIGN UP";
		this.staticContents.loginButton = "LOGIN";
		this.staticContents.forgotPassword = "Forgot Password?"

		//Sign up Set Password component
		this.staticContents.h3header = "Annectos Rewards Logo";
		this.staticContents.setPasswordTitle = "Registration Successful!";
		this.staticContents.signupInnerTitle = "Please provide the following details to help us register you in the program:";
		this.staticContents.loginSignUpButton = "SIGN UP";
		this.staticContents.loginButton = "LOGIN";
		this.staticContents.forgotPassword = "Forgot Password?"

		//Home component
		this.staticContents.h2Dashboard = "Dashboard";
		this.staticContents.h2Channels = "Channels";
		this.staticContents.h2Claims = "Claims";
		this.staticContents.h2MyClaims = "My Claims";
		this.staticContents.h2AddClaim = "Add Claim";
		this.staticContents.h2MyRedemptions = "My Redemptions";
		this.staticContents.h2RewardsGallery = "Rewards Gallery";
		this.staticContents.h2SpecialOffers = "Offers";
		this.staticContents.h2AllOffers = "All Offers";
		this.staticContents.h2Contest = "Contest";
		this.staticContents.h2LeadManagement = "Lead Management";
		this.staticContents.h2MyLeads = "My Leads";
		this.staticContents.h2AddLead = "Add Lead";

		this.staticContents.MyProfile = "My Profile";
		this.staticContents.Logout = "Logout";

		this.staticContents.h2Star = this.configService.getTerminology();

		this.staticContents.f1Text = "Some Dummy Text";

		this.staticContents.clientDsc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
			"to, sed eleifend dui venenatis mattis.";

		// Set Password
		// this.staticContents.successMsg = "You have successfully completed the registration process.";
		this.staticContents.h3otpVerified = "OTP Verification Successful!";
		this.staticContents.setLoginDetail = "Please set your login details to access the program:";

		// About Us SECTION ONE s1
		this.staticContents.s1Title = "The Racold Royal Dealer Program";
		this.staticContents.s1Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eget sodales le" +
			"o, ut pretium arcu. Nunc in vestibulum mi, fermentum mattis sem. Donec dolor tor" +
			"tor, accumsan sit amet arcu tempus, rhoncus fringilla lorem.";
		this.staticContents.s1DownloadBrochure = "Download Brochure";
		this.staticContents.s1TermCondition = "Terms and Conditions";
		this.staticContents.s1FAQs = "FAQs";
		this.staticContents.s1Points = this.configService.getTerminology();

		// SECTION TWO s2 ABOUT THE PROGRAM

		this.staticContents.s2Title = "About the Program";
		this.staticContents.s2Description = "Racold Royal Dealer Program: Racold India welcomes you to an exclusive loyalty p" +
			"rogram for System Integrators and their distributors PAN India called the SRacol" +
			"d Royal Dealer Program. In this program, you will earn benefits in the form of r" +
			"ewards for selling a. distributing more Digilink products in the market place.";
		this.staticContents.s2WhoShouldParticpate = "Who Should Participate";
		this.staticContents.s2WhoShouldParticpateDSC = "Select dealers of Racold";
		this.staticContents.s2BenefitOfParticpate = "Benefits of participating :";
		this.staticContents.s2BenefitOfParticpateDSC = "As an exclusive and registered member in the Racold Royal Dealer Program, the part" +
			"icipants will get the below mentioned privileges:";
		this.staticContents.s2BenefitOfParticpateUL1 = "You are a select dealer in this program which makes you special";
		this.staticContents.s2BenefitOfParticpateUL2 = "Rewards of loyalty";
		this.staticContents.s2BenefitOfParticpateUL3 = "Dedicated team 8, visits";
		this.staticContents.s2BenefitOfParticpateUL4 = "First update, offerings in the future";


		// SECTION THREE s3 TERMS AND Conditions
		this.staticContents.s3TnC = "Terms and Conditions";

		this.staticContents.s3TnC1 = "Racold royal dealers program is offered by Racold Thermo Private Limited for sel" +
			"ect dealers.";
		this.staticContents.s3TnC2 = "The reward" + this.configService.getTerminology() + " are valid only on purchase of Racold products and the accounti" +
			"ng of the same will be done by Racold post validated the source";
		this.staticContents.s3TnC3 = this.configService.getTerminology() + " are categorized in two categories:";
		this.staticContents.s3TnC3a = "a. Accrued " + this.configService.getTerminology() + ": Awarded for each sale uploaded by the dealers";
		this.staticContents.s3TnC3b = "b. Credited " + this.configService.getTerminology() + ": Accrued " + this.configService.getTerminology() + "will be convert. into credit. " + this.configService.getTerminology() + " only onc" +
			"e the program center has validated the purchase made by dealers";
		this.staticContents.s3TnC4 = "Program benefit is not transferable nor can be redeemed as cash or credit note";
		this.staticContents.s3TnC5 = "The images shown in the online reward catalogue may differ from the actual produ" +
			"ct";
		this.staticContents.s3TnC6 = "Good/materials return. will lead to rever.I of " + this.configService.getTerminology() + " as per the grid";
		this.staticContents.s3TnC7 = "Racold thermos pvt limited/ program center has the right to ask for furnishing t" +
			"he true copies of invoices for the product purchased by you.";
		this.staticContents.s3TnC8 = "Racold thermo private limited R.erves the right to change, modify or discontinue" +
			" the program at any given " + this.configService.getTerminology() + " of time without assigning any reason(s) for the " +
			"same";
		this.staticContents.s3TnC9 = "All products featured in the reward catalogue are subject to availability of the" +
			" goods and supplier warrantie,restrictions at the time of redemptions. Racolcl t" +
			"hermo pvt. Ltd, managing agency provides any warranty whether expressly or impli" +
			"ed) whatso ever with respect of the product/services acquired under the rewards " +
			"program";
		this.staticContents.s3TnC10 = "Racold thermo private limited or it managing agency reserves the right to cancel" +
			", change or substitute any reward any time without any prior notice.";
		this.staticContents.s3TnC11 = "Redemptions request once given cannot be cancel. or withdrawn or changed. On red" +
			"emptions, the reward " + this.configService.getTerminology() + " would automatically be subtracted from the accumulat." +
			" balance.";
		this.staticContents.s3TnC12 = "Computation of the reward mechanics shall be final, conclusive and binding on th" +
			"e participating dealers and will not be liable to be disputed or questioned.";
		this.staticContents.s3TnC13 = "The participating dealers have to ensured that they furnish the information in i" +
			"ts true form and any wrongly represented information will result in permanent di" +
			"squalification from the program";
		this.staticContents.s3TnC14 = "The redemptions of the " + this.configService.getTerminology() + " will start from October 30th 2016";
		this.staticContents.s3TnC15 = "There shall be base " + this.configService.getTerminology() + " based on the last year sales which will have to be ac" +
			"hieved for starting the redemption. " + this.configService.getTerminology() + " below the base " + this.configService.getTerminology() + " will be treated" +
			" at 80%. For example: If your base " + this.configService.getTerminology() + " are 50,000 and you achieve 40,000 " + this.configService.getTerminology() + "" +
			"s at the end of the program you will be eligible only for 80% of 40,000 " + this.configService.getTerminology() + " w" +
			"hich will be equal to 32,000 " + this.configService.getTerminology() + "";
		this.staticContents.s3TnC16 = "At the time of the redemption, if you are on an overdue status which will be val" +
			"idated by the concern. distributor/ Racold , the complete redemption will be sta" +
			"lled and the value of OD and the concerned " + this.staticContents.point + " will be removed from the accrued/" +
			"credited " + this.configService.getTerminology() + ".";
		this.staticContents.s3TnC17 = "Once the program ends in December 2016 you are required to complete the redempti" +
			"on ,uest latest by January 31st, 2017";
		this.staticContents.s3TnC18 = "Any " + this.configService.getTerminology() + " balance or not redemeed post January 31st, 2017 will become null & vo" +
			"id.";
		this.staticContents.s3TnC19 = "All disputes will be settled under the Pune jurisdiction only.";

		// SECTION 4 FAQS
		this.staticContents.s4Faq = "FAQs";

		this.staticContents.s4FaqQ1 = "1. What is Racold Royal";
		this.staticContents.s4FaqA1 = "Sir / Madam, Racold Royal is an rewards program being conduct. by Racold Thermo " +
			"Private Limit.";

		this.staticContents.s4FaqQ2 = "2. What is rewards Program mean";
		this.staticContents.s4FaqA2 = "Rewards Program mean, for every purchase of Racold Products that you do, you wou" +
			"ld be credited with " + this.configService.getTerminology() + ". You can red.m these " + this.configService.getTerminology() + " for exciting rewards.";

		this.staticContents.s4FaqQ3 = " 3. Where these " + this.configService.getTerminology() + " would be credited ?";
		this.staticContents.s4FaqA3 = "" + this.configService.getTerminology() + " would get credited in a specific micro site for which you would get a sec" +
			"ure access user id & password. You will also get a mobile app thru which you can" +
			" access your account.";

		this.staticContents.s4FaqQ4 = "4. How can I redeem my " + this.configService.getTerminology() + " for rewards? ";
		this.staticContents.s4FaqA4 = "Sir / Madam, the micro site would have rewards gallery and you can redeem your" + this.configService.getTerminology() + " for listed rewards.";

		this.staticContents.s4FaqQ5 = "5. Do I have any Target?";
		this.staticContents.s4FaqA5 = "Sir / Madam, this program does not have any Target and you can redeem your " + this.staticContents.point + "" +
			"s from the lst time your " + this.configService.getTerminology() + " are loaded. However, if you are redeeming before" +
			" reaching the base " + this.configService.getTerminology() + " of last year, redemption is permitted for 80% of the a" +
			"ccumulated " + this.configService.getTerminology() + " only.";

		this.staticContents.s4FaqQ6 = "6. Who will load the " + this.configService.getTerminology() + " in the portal?";
		this.staticContents.s4FaqA6 = "Sir you can self load your " + this.configService.getTerminology() + " yourselves along with the Invoices for the Rac" +
			"old Items Purchased between lst & 7th of every month. Racold Backend Team would " +
			"validate the same and credit the " + this.configService.getTerminology() + " by 15th of every month.";

		this.staticContents.s4FaqQ7 = "7. What is the program period?";
		this.staticContents.s4FaqA7 = "Sir you can self load your " + this.configService.getTerminology() + " yourselves along with the Invoices for the Rac" +
			"old Items Purchased between lst & 7th of every month. Racold Backend Team would " +
			"validate the same and credit the " + this.configService.getTerminology() + " by 15th of every month.";

		this.staticContents.s4FaqQ8 = "8. Does that mean I would get " + this.configService.getTerminology() + " for all purchases made from 1st April? ";
		this.staticContents.s4FaqA8 = "Yes";

		this.staticContents.s4FaqQ9 = "9. Is it applicable to all dealers";
		this.staticContents.s4FaqA9 = "Sir, this program is confined to a select set of dealers who are handpicked by R" +
			"acold basis multiple criteria and parameters";

		this.staticContents.s4FaqQ10 = "10. Ok. Does that mean the existing schemes would stop?.";
		this.staticContents.s4FaqA10 = "Sir, as communicated already, this is a special program for a select set of deal" +
			"ers only and this is over and above any trade schemes that we may operate that a" +
			"re applicable for all dealers. Therefore, this program is a bonus / special reco" +
			"gnition for these Select Special Dealers.";

		this.staticContents.s4FaqQ11 = "11. What are these different categories within this program and what is the diff" +
			"erence between those.";
		this.staticContents.s4FaqA11start = "Sir there are 3 category of Dealers within this Program that are,";
		this.staticContents.s4FaqA11a = "Premium Partner, (>1000 Units)";
		this.staticContents.s4FaqA11b = "Prestige Partner & (240 - 999 Units)";
		this.staticContents.s4FaqA11c = "Shining Star. (100 - 239 units";
		this.staticContents.s4FaqA1end = "This is basis the 2015 established volume and corresponding " + this.configService.getTerminology() + " simulation at " +
			"our end. While the reward gallery is common for all, there would be certain soft" +
			" benefits beyond the rewards to recognize the Premium & Prestige.";

		this.staticContents.s4FaqQ12 = "12. Can I commit volume for 2016 and get listed in Premium or Prestige?";
		this.staticContents.s4FaqA12 = "Sir, we fully appreciate and thank for the interest to upgrade. However, for thi" +
			"s year program your category is basis 2015 performance only. We will certainly u" +
			"pgrade you to Pre­mium or prestige in the following year program basis 2016 turn" +
			"over.";

		this.staticContents.s4FaqQ13 = "13. Can I request for CN in the place of these rewards?";
		this.staticContents.s4FaqA13 = "Sir, first of all " + this.configService.getTerminology() + " does not have any denominating value to be converted as" +
			" CN. Further, this is a reward program that's over and above the Periodic Trade " +
			"Schemes that we settle as CN or FOC. Hence we would appreciate if you participat" +
			"e and enjoy this program in its true shape and sense.";
		this.staticContents.s4FaqQ14 = "14. Are these articles that I redeem covered under warranty? And who would facil" +
			"itate in case of any defects.";
		this.staticContents.s4FaqA14 = "Sir, article listed are from leading brands in the category and it comes with ma" +
			"nufacturer's warranty controlled by their own terms and conditions. Therefore, t" +
			"he manufacturer under their own terms and condition shall be liable for any defe" +
			"cts or damages or warranty and you shall for all reason do direct transaction fo" +
			"r these rewards that you redeem with the manufacturers.";

		// SECTION 5 s5

		/////////////////////// RACOLD STARTS ///////////////////////////


		this.staticContents.s5Product = "Product";
		this.staticContents.s5Family = "Family";
		this.staticContents.s5SKU = "SKU";
		this.staticContents.s5Size = "Size";

		this.staticContents.s5Product1 = "Velis";
		this.staticContents.s5Family1 = "Velis";
		this.staticContents.s5SKU1 = "Velis-45L";
		this.staticContents.s5Size1 = "45L"
		this.staticContents.points1 = "10";

		this.staticContents.s5Product2 = "Velis";
		this.staticContents.s5Family2 = "Velis";
		this.staticContents.s5SKU2 = "Velis-25L";
		this.staticContents.s5Size2 = "25L"
		this.staticContents.points2 = "7";

		this.staticContents.s5Product3 = "Velis";
		this.staticContents.s5Family3 = "Velis";
		this.staticContents.s5SKU3 = "Velis-65L";
		this.staticContents.s5Size3 = "65L"
		this.staticContents.points3 = "10";

		this.staticContents.s5Product4 = "Pronto Style";
		this.staticContents.s5Family4 = "Pronto";
		this.staticContents.s5SKU4 = "Pronto Style-3L";
		this.staticContents.s5Size4 = "3L"
		this.staticContents.points4 = "3";

		this.staticContents.s5Product5 = "Pronto Style";
		this.staticContents.s5Family5 = "Pronto";
		this.staticContents.s5SKU5 = "Pronto Style-6L";
		this.staticContents.s5Size5 = "6L"
		this.staticContents.points5 = "4";

		this.staticContents.s5Product6 = "Pronto Style";
		this.staticContents.s5Family6 = "Pronto";
		this.staticContents.s5SKU6 = "Pronto Style-3L";
		this.staticContents.s5Size6 = "3L"
		this.staticContents.points6 = "3";

		this.staticContents.s5Product7 = "Pronto Style";
		this.staticContents.s5Family7 = "Pronto";
		this.staticContents.s5SKU7 = "Pronto Style-6L";
		this.staticContents.s5Size7 = "6L"
		this.staticContents.points7 = "4";

		this.staticContents.s5Product8 = "Pronto Neo";
		this.staticContents.s5Family8 = "Pronto";
		this.staticContents.s5SKU8 = "Pronto Neo-3L";
		this.staticContents.s5Size8 = "3L"
		this.staticContents.points8 = "2";



		/////////////////////// RACOLD ENDS ///////////////////////////

		// ************* My Claims ***************

		this.staticContents.myClaimsTitle = "My Claims"
		this.staticContents.myClaimsViewFor = "View Claims for : ";

		this.staticContents.myClaimsMonth = "This Month";
		this.staticContents.myClaimsWeek = "This Week";
		this.staticContents.myClaimsQuarter = "This Quarter";
		this.staticContents.myClaimsChoose = "Choose a Time Period";


		this.staticContents.myClaimsStatus1 = "Status : "
		this.staticContents.myClaimsAll = "All";
		this.staticContents.myClaimsPending = "Pending"
		this.staticContents.myClaimsInProcess = "In Process";
		this.staticContents.myClaimsApproved = "Qualified";
		this.staticContents.myClaimsRejected = "Rejected";
		this.staticContents.myClaimsHold = "Hold";

		this.staticContents.myClaimsStartDate = "Start Date :";
		this.staticContents.myClaimsEndDate = "End Date :";



		this.staticContents.myClaimsPerPage = "per page";
		this.staticContents.myClaimsShow = "Show :"
		this.staticContents.myClaimsClaims = "Claims";
		this.staticContents.myClaimsFive = "5";
		this.staticContents.myClaimsTen = "10";
		this.staticContents.myClaimsTwenty = "20";
		this.staticContents.myClaimsFifty = "50";
		this.staticContents.myClaimsHundred = "100";


		this.staticContents.myClaimsId = "Claim ID";
		this.staticContents.myClaimsSerial = "Sr.No.";

		this.staticContents.myClaimsInvoice = "In. No.";
		this.staticContents.myClaimsDate = "Uploaded Date";
		this.staticContents.myClaimsTotal = "Total";
		this.staticContents.myClaimsTotalVat = "Total Excluding GST";
		this.staticContents.myClaimsDistributor = "Distributor";
		this.staticContents.myClaimsPointEarned = "" + this.configService.getTerminology() + " Earned";

		this.staticContents.myClaimsStatus = "Status";
		this.staticContents.myClaimsAction = "Action";
		this.staticContents.myClaimsLoading = "Loading..";
		this.staticContents.myClaimsNoRecord = "No Record found!";

		// ************* Redeemption Requests***************
		this.staticContents.redeemSerial = "Sr.No.";
		this.staticContents.redeemId = "Order Id";
		this.staticContents.redeemCustomerName = "Customer Name";
		this.staticContents.redeemApprovalStatus = "Approval Status";
		this.staticContents.redeemOrderStatus = "Order Status";
		this.staticContents.redeemOrderDate = "Order Date";
		this.staticContents.redeemProgramName = "Program Name";
		this.staticContents.redeemProgramQuantity = "Product Quantity";
		this.staticContents.redeemTotalPoints = "Total" + this.configService.getTerminology();
		this.staticContents.redeemApprove = "Approve";
		this.staticContents.redeemCancel = "Cancel";
		this.staticContents.redeemViewFor = "Period:";

		this.staticContents.redeemAll = "All";
		this.staticContents.redeemMonth = "This Month";
		this.staticContents.redeemWeek = "This Week";
		this.staticContents.redeemQuarter = "This Quarter";
		this.staticContents.redeemChoose = "Choose a Time Period";

		this.staticContents.redeemStatus1 = "Order Status: "
		this.staticContents.redeemPending = "Pending"
		this.staticContents.redeemInProcess = "In Process";
		this.staticContents.redeemConfirmed = "Confirmed";
		this.staticContents.redeemProcessing = "Processing";
		this.staticContents.redeemShipped = "Shipped";
		this.staticContents.redeemCompleted = "Completed";
		this.staticContents.redeemCancelled = "Cancelled";
		this.staticContents.redeemReturned = "Returned";

		this.staticContents.redeemStartDate = "Start Date :";
		this.staticContents.redeemEndDate = "End Date :";

		this.staticContents.redeemPerPage = "per page";
		this.staticContents.redeemShow = "Show :"
		this.staticContents.redeem = "Redeems";
		this.staticContents.redeemTen = "10";
		this.staticContents.redeemTwenty = "20";
		this.staticContents.redeemFifty = "50";
		this.staticContents.redeemHundred = "100";
		this.staticContents.redeemApprovalStatus = "Approval Status";
		this.staticContents.redeemApprovalStatusAll = "All";
		this.staticContents.redeemApprovalStatusPending = "Pending";
		this.staticContents.redeemApprovalStatusApproved = "Approved";
		this.staticContents.redeemApprovalStatusRejected = "Rejected";
		this.staticContents.redeemApprovalStatusRejected = "Rejected";

		this.staticContents.orderId = "Order Id";
		this.staticContents.orderDate = "Order Date";
		this.staticContents.totalPoints = "Total Points"
		this.staticContents.approvalStatus = "Approval Status";


		//region performance
		this.staticContents.regionName = "Region Name";
		this.staticContents.noOfClaims = "No. of Claims";
		this.staticContents.points = "Points";


		// PRODUCTION
		this.staticContents.jswprogramId = "PR1518085378956";
		this.staticContents.jswClientId = "CL1518008748419";
		this.staticContents.jswEngineerProgramRole = "BR1519707374235";
		this.staticContents.jswDistributorProgramRole = "BR1519707341478";
		this.staticContents.jswDealerProgramRole = "BR1519707359517";
		this.staticContents.jswDistrubutorProfileid = "UP1525700014661";
		this.staticContents.jswEngineerAdminProgramRole = "CD1528226207319";
		this.staticContents.jswDistributorAdminProgramRole = "CD1528226257458";
		this.staticContents.jswDealerAdminProgramRole = "CD1521523719341";
		this.staticContents.jswASMAdminProgramRole = "CD1521523766222";
		this.staticContents.jswCMProgramRole = "CD1533800083722";
		this.staticContents.jswRCMProgramRole = "CD1519994067578";
		//for expert engineer
		this.staticContents.jswEngineerProfileId = "UP1531873691460";
	}

	getStaticContents() {
		return this.staticContents;
	}
	/***********************************	END OF LABELS SETTER GETTER 	*************************************/
	/***********************************		CATEGORIES SETTER GETTER 		**************************************/

	SetCategories() {
		this.categories = [{
			"categoryName": "About Program",
			"subCatgories": ["Other"]
		},
		{
			"categoryName": "Business Related queries",
			"subCatgories": ["Other"]
		},
		{
			"categoryName": "Technical queries",
			"subCatgories": ["Other"]
		},
		{
			"categoryName": "General Enquires",
			"subCatgories": ["Other"]
		}

		]
	}

	getCategories() {
		return this.categories;
	}
	/***********************************	END OF CATEGORIES SETTER GETTER 	*************************************/

	setImageFormats() {
		this.imgFormats = [".jpg", ".jpeg", ".bmp", ".tif", ".tiff", ".bmp", ".gif", ".png", ".eps", ".raw", ".cr2", ".nef", ".orf", ".sr2"];
	}

	getImageFormats() {
		return this.imgFormats;
	}

	setVideoFormats() {
		this.videoFormats = [".3g2", ".3gp", ".avi", ".flv", ".h264", ".m4v", ".mkv", ".mov", ".mp4", ".mpg", ".mpeg", ".rm", ".swf", ".vob", ".wmv"];
	}

	getVideoFormats() {
		return this.videoFormats;
	}

	setMonth(isCurrentYr: boolean) {

		this.month =
			[{ "val": 1, "mon": "Jan" }, { "val": 2, "mon": "Feb" },
			{ "val": 3, "mon": "Mar" }, { "val": 4, "mon": "Apr" },
			{ "val": 5, "mon": "May" }, { "val": 6, "mon": "Jun" },
			{ "val": 7, "mon": "Jul" }, { "val": 8, "mon": "Aug" },
			{ "val": 9, "mon": "Sep" }, { "val": 10, "mon": "Oct" },
			{ "val": 11, "mon": "Nov" }, { "val": 12, "mon": "Dec" }
			]
		if (isCurrentYr == true) {
			var date = (new Date());
			var mon = date.getMonth();
			this.month.splice(mon + 1, 12 - mon);
			console.log("AFTErSLICE---", this.month);
		}
	}

	getMonth() {
		return this.month;
	}

	setYear() {
		this.year = [];
		var date: string = String(new Date());
		var dateArr = date.split(" ");
		var currentYr = Number(dateArr[3]);
		console.log("currentYr---", currentYr);
		for (let i = 2015; i <= currentYr; i++) {
			console.log("inside lopp----", i);
			this.year.push({ "val": i, "yr": i });
		}
	}

	getYear() {
		console.log(" this.year", this.year);
		return this.year;
	}

}