/*
	Author			:	Deepak Terse
	Description		: 	Service to set and get all api related constants and urls
	Date Created	: 	28 March 2016
	Date Modified	: 	28 March 2016
*/


import { Injectable, OnInit, EventEmitter } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { Observable, BehaviorSubject } from "rxjs";


interface Window {
	AWS?: any;
}

/*@Injectable :
    It lets Angular know that a class can be used with the dependency injector.
*/
@Injectable()
export class S3UploadService implements OnInit {
	public loader: any;
	public params: any;

	private AWSService: any;
	private bucket: any;

	public fileUrl: any[];

	public fileUploaded = new BehaviorSubject([]);
	private countOfUploaded: number;
	constructor() {
		this.fileUrl = [];
		this.params = [];

		this.AWSService = window["AWS"];

		this.AWSService.config.accessKeyId = 'AKIAJNALBLLSQ7CIWLKQ';
		this.AWSService.config.secretAccessKey = '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';

		this.bucket = new this.AWSService.S3({
			signatureVersion: 'v4',
			region: 'ap-south-1'
		});
	}

	ngOnInit() {

	}

	formParams(fileInput: any, filePath: string) {
		this.params = [];
		console.log("FileInput", fileInput);
		let file;
		
		if(fileInput.target != undefined){
		let file: any = fileInput.target.files[0];
		

		var d = new Date();
		let ext: string = file.name.slice(file.name.lastIndexOf('.'), file.name.length);
		let filename: string = filePath + d.getTime() + ext;

		filename.replace("%", "");
		this.params.push({ Bucket: 'phoenix2-content-files', Key: filename, Body: file });
		}
		else{
		let file : any = fileInput;
		var d = new Date();
		let filename = filePath + d.getTime() + ".csv";
		filename.replace("%", "");
		this.params.push({ Bucket: 'phoenix2-content-files', Key: filename, Body: file });
		}


		
		// }
	}

	uploadFiles() {

		let fileUrl = [];
		let fileUploaded = this.fileUploaded;
		console.log("file uploaded in servce upload files",fileUploaded);
		let params: any[] = this.params;
		console.log("params", params);
		for (let i = 0; i < params.length; i++) {
			this.bucket.upload(params[i], function (err, data) {
				console.log("data  in servce upload files",data);
				console.log("data", data, "err", err);
				fileUrl.push(data);
				console.log("fileUrl", fileUrl);
				if (fileUrl.length === params.length)
					fileUploaded.next(fileUrl);
			})
				.on('httpUploadProgress', function (progress) {
					console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
				});
		}
	}




	/*Called from component containing input type file when file is selected
	to form params required for file upload
*/
	formParamsMultiple(fileInput: any, filePath: string, index?: number) {
		console.log("fileinput in service", fileInput, "File Path in Service", filePath, "index", index);
		if (fileInput !== undefined) {
			let file: any = fileInput;
			console.log("file", file.name);

			var d = new Date();
			if (file.name !== undefined) {

				console.log("form params called");
				let ext: string = file.name.slice(file.name.lastIndexOf('.'), file.name.length);


				let filename: string = filePath + d.getTime() + ext;

				filename.replace("%", "");

				this.params[index] = { Bucket: 'phoenix2-content-files', Key: filename, Body: file };
				console.log("this.params", this.params);
			}
		}
		else {
			console.log("No file");
		}

	}

	emptyAllParams() {
		this.params = [];
	}

	formEmptyParams(index) {
		this.params[index] = {};
	}

	/*Called on submission of form to upload files */
	uploadFilesMultiple() {
		this.countOfUploaded = 0;
		let fileUrl = [];
		let fileUploaded = this.fileUploaded;
		console.log("this.fileUploaded", this.fileUploaded);
		let params: any[] = this.params;
		console.log("this.params", this.params);
		let count = 0;
		let count1 = 0;
		for (let pIndex = 0; pIndex < this.params.length; pIndex++) {
			console.log("upload files called");
			if (this.params[pIndex].Body) {
				params[pIndex] = this.params[pIndex];
				this.countOfUploaded++;
			}
			else {
				params[pIndex] = {};
			}
		}

		count = this.countOfUploaded;
		console.log("@@@@@ Params @@@@@", params);
		for (let i = 0; i < params.length; i++) {
			if (params[i].Body) {
				console.log("params[i]", params[i]);
				this.bucket.upload(params[i], function (err, data) {
					fileUrl[i] = data;
					count1++;
					console.log("File URL length", fileUrl.length);
					console.log("Params URL length", params.length);
					console.log("Count of selected ", count);
					console.log("Count of uploaded ", count1);

					if (count1 === count) {//fileUrl.length //params.length
						console.log("@@@@@File URL @@@@@@@@", fileUrl);
						fileUploaded.next(fileUrl);
						this.params = [];
						count = 0;
						count1 = 0;

					}

				})
					.on('httpUploadProgress', function (progress) {
						this.loader = Math.round(progress.loaded / progress.total * 100);
						console.log("this.loader", this.loader);
						console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
					});
			}
			else {
				fileUrl[i] = {};
			}
		}
		return;
	};
}


