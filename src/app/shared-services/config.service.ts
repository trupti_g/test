/*
	Author			:	Deepak Terse
	Description		: 	Service to set and get all api related constants and urls
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/


import { Injectable, OnInit, EventEmitter } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { Observable } from "rxjs";

/*@Injectable :
    It lets Angular know that a class can be used with the dependency injector.
*/
@Injectable()
export class ConfigService implements OnInit {
	pastContestGallery: any;
	upcomingContestDetails: any;
	contestDetails: any;
	pastEventDetails: any;
	public isDashboard = false;
	routeName: any;
	entityDetails: any;
	public eventArray: any = [];
	public leadApproval: boolean = false;
	leadDetails: any;
	public birthdayNotificationFlag: boolean;
	public activityNotificationFlag: boolean;
	public wishNotificationFlag: boolean;
	channelGraphFlag: any;
	BEProgramsMapped: any;
	apiBasePath2: string;
	tempFrontendInfo: any;
	ProductCatalogCategorySubCategories: any;
	clientObj: any;

	//constants required for api consumptions
	private apiBasePath: string;
	private apiBasePath4: string;
	private apiBasePathWithHttp: string;
	private servicePorts: any;
	private apiUrls: any;
	private statusTokens: any;
	private approved: any;
	private rejected: any;
	private pending: any;
	public userInfo: any;
	private headers;
	private options;
	public userId: string = "";
	public tab: any;

	//image upload multiple
	public imageLimit: number;
	public compressLimit: number;
	public childsIdArray: any = [];
	public isDealer: boolean = false;
	public isDistributor: boolean = false;
	public isEngineer: boolean = false;

	// to activate scroll after modal
	public modalClose: boolean = true;


	loggedInUserInfo: any;
	loggedInBEInfo: any;
	loggedInProgramInfo: any;
	loggedInProgramUserInfo: any;
	loggedInClientProgramUserInfo: any;

	frontEndUserInfo: any;
	productInfo: any;
	dashBoardInfo: any;
	loginPageInfo: any;
	public isRewardGallery: boolean;
	public isLogin: boolean;
	public terminology: string = "";
	public themeColor: string;
	public isChannelsFound: boolean = false;
	public activityCount = 0;
	public wishCount = 0;
	public channelProfileInfo: any;
	public channelsChainRequestObject: any;
	public channelsBackRequestObject: any;
	public channelsChain: any = [];
	public selectedCreditPartyId: string = "";

	public celebration: any;
	public selectedStateFilter: any;
	public selectedStatusFilter: any;
	public isSetSelectedFilter: boolean = false;


	userInfoChangeEvent: EventEmitter<any> = new EventEmitter();

	constructor(private http: Http) {
		this.isLogin = false;

		this.isRewardGallery = false;
		this.setRewardGalleryFlag(this.isRewardGallery);

		//console.log("Terminology : ", this.terminology);

		this.compressLimit = 0.1;
		this.imageLimit = 1000000;
		this.setApiBasePath();
		this.setServicePorts();
		this.setApiUrls();
		this.setStatustokens();

		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.userInfo = this.getLoggedInUserInfo();

		if (this.getloggedInProgramUser())
			this.userId = this.getloggedInProgramUser().programUserId;

		this.ngOnInit();
	}

	ngOnInit() {


		this.getloggedInBEInfo();
	}



	/*********************************	LOGGEDIN USERINFO SETTER GETTER	***************************************/
	getLoggedInUserInfo() {

		this.loggedInUserInfo = JSON.parse(localStorage.getItem("loggedInUserInfo"));
		return this.loggedInUserInfo;
	}

	setLoggedInUserInfo(userObject) {
		localStorage.setItem("productDetails", JSON.stringify([]));

		localStorage.setItem("loggedInUserInfo", JSON.stringify(userObject));
		if (userObject != null) {
			if (userObject.token) {
				localStorage.setItem("userToken", JSON.stringify(userObject));
			}
		}

	}

	setClientUserInfo(clientUserInfo) {
		localStorage.setItem("clientUserInfo", JSON.stringify(clientUserInfo));
	}

	getClientUserInfo() {
		return JSON.parse(localStorage.getItem("clientUserInfo"));
	}

	getloggedInBEInfo() {
		this.loggedInBEInfo = JSON.parse(localStorage.getItem("loggedInBEInfo"));
		return this.loggedInBEInfo;
	}

	setloggedInBEInfo(userBEObject) {
		localStorage.setItem("loggedInBEInfo", JSON.stringify(userBEObject));
		this.getloggedInBEInfo();
	}

	getloggedInProgramInfo() {
		this.loggedInProgramInfo = {};
		this.loggedInProgramInfo = JSON.parse(localStorage.getItem("loggedInProgramInfo"));
		return this.loggedInProgramInfo;
	}

	setloggedInProgramInfo(userProgramObject) {
		this.loggedInProgramInfo = {};
		localStorage.setItem("loggedInProgramInfo", JSON.stringify(userProgramObject));
		this.getloggedInProgramInfo();
	}


	setloggedInProgramBannerInfo(userProgramObject) {
		localStorage.setItem("loggedInProgramInfo", JSON.stringify(userProgramObject));
		this.getloggedInProgramInfo();
	}

	setDashBoardInfo(dashBoardInfo) {
		localStorage.setItem("dashboardInfo", JSON.stringify(dashBoardInfo));
		this.dashBoardInfo = JSON.parse(localStorage.getItem("dashboardInfo"));
		return this.dashBoardInfo;

	}

	setIsChannelsFound(isChannelsFound) {
		this.isChannelsFound = isChannelsFound;
	}
	getIsChannelsFound() {
		return this.isChannelsFound;
	}

	getDashBoardInfo() {
		return this.dashBoardInfo;
	}

	setProgramUserInfo(ProgramUserInfo) {
		localStorage.setItem("ProgramUserInfo", JSON.stringify(ProgramUserInfo));
	}

	getProgramUserInfo() {
		let programUserInfo = JSON.parse(localStorage.getItem("ProgramUserInfo"));
		return JSON.parse(localStorage.getItem("ProgramUserInfo"));
	}

	setLoginPageInfo(loginPageInfo) {
		localStorage.setItem("loginPageInfo", JSON.stringify(loginPageInfo));
		this.loginPageInfo = JSON.parse(localStorage.getItem("loginPageInfo"));

	}
	getLoginPageInfo() {
		return this.loginPageInfo;
	}



	getloggedInProgramUser() {
		return JSON.parse(localStorage.getItem("loggedInProgramUserInfo"));
	}



	getmyClaims() {
		this.loggedInProgramInfo = JSON.parse(localStorage.getItem("myClaimsList"));
		return this.loggedInProgramInfo;
	}

	setMyClaims(myClaims) {
		localStorage.setItem("myClaimsList", JSON.stringify(myClaims));

	}

	setRedeems(redeems) {
		localStorage.setItem("redeemsList", JSON.stringify(redeems));

	}

	getRedeems() {
		this.loggedInProgramInfo = JSON.parse(localStorage.getItem("redeemsList"));
		return this.loggedInProgramInfo;
	}

	setloggedInProgramUser(userProgramUser) {

		localStorage.setItem("loggedInProgramUserInfo", JSON.stringify(userProgramUser));
	}

	setPoints(points) {
		localStorage.setItem("points", points);
	}

	getPoints() {
		return (localStorage.getItem("points"));
	}
	getAuthenticationToken(): string {
		if (JSON.parse(localStorage.getItem("userToken")))
			return JSON.parse(localStorage.getItem("userToken")).token;
	}
	getUserInfo(): string {
		return JSON.parse(localStorage.getItem("userToken"));
	}


	setFrontEndUserInfo(userInfo) {
		if (userInfo) {
			this.frontEndUserInfo = userInfo;
		}
		else
			this.frontEndUserInfo = {
				"userId": this.getloggedInProgramUser().userId
			}
	}
	getFrontEndUserInfo() {
		return this.frontEndUserInfo;
	}
	getFrontEndUserInfoPromise(): Promise<any> {
		return Promise.resolve(this.frontEndUserInfo);
	}


	// Reward Gallery

	setRewardGalleryProduct(product) {
		localStorage.setItem("productInfo", JSON.stringify(product));
	}

	getRewardGalleryProduct() {
		return JSON.parse(localStorage.getItem("productInfo"));

	}

	setProductCatalogObj(product) {
		localStorage.setItem("productCatalogObj", JSON.stringify(product));
	}

	getProductCatalogObj() {
		return JSON.parse(localStorage.getItem("productCatalogObj"));

	}

	getRejected() {
		return this.rejected;

	}
	getApproved() {
		return this.approved;
	}
	getPending() {
		return this.pending;
	}
	setRejected(rejected) {
		this.rejected = rejected;

	}
	setApproved(approved) {
		this.approved = approved;
	}
	setPending(pending) {
		this.pending = pending;
	}

	setMyRedemption(myRedemption) {
		localStorage.setItem("myRedemptionList", JSON.stringify(myRedemption));

	}


	getMyRedemption() {
		return JSON.parse(localStorage.getItem("myRedemptionList"));

	}

	setTab(tab) {
		this.tab = tab;
	}

	getTab() {
		return this.tab;
	}

	setChannelGraphFlag(flag) {
		this.channelGraphFlag = flag;
		console.log("flag", flag);
	}

	getChannelGraphFlag() {
		return this.channelGraphFlag;
	}
	//My channels 

	setMyChannelsProfile(profile) {
		// console.log("in setMyChannelsProfile in channelservice",profile);
		this.channelProfileInfo = profile;
		console.log("in setMyChannelsProfile in channelservice", this.channelProfileInfo);
	}

	getMyChannelsProfile(): any {
		console.log("in getMyChannelsProfile in channelservice", this.channelProfileInfo);
		return this.channelProfileInfo;
	}
	setMyChannelsChainRequestObject(channelsChain) {
		this.channelsChainRequestObject = channelsChain;
	}

	getMyChannelsChainRequestObject(): any {
		return this.channelsChainRequestObject;
	}

	setMyChannelsBackRequestObject(channelsChain) {
		console.log("in config setMyChannelsBackRequestObject", channelsChain);
		this.channelsBackRequestObject = channelsChain;
	}

	getMyChannelsBackRequestObject(): any {
		console.log("in config getMyChannelsBackRequestObject", this.channelsBackRequestObject);
		return this.channelsBackRequestObject;
	}
	setMyChannelsChain(channelsChain) {
		this.channelsChain = channelsChain;
	}

	getMyChannelsChain(): any {
		return this.channelsChain;
	}

	setSelectedChannelCreditPartyId(creditPartyId) {
		this.selectedCreditPartyId = creditPartyId;
	}

	getSelectedChannelCreditPartyId() {
		return this.selectedCreditPartyId;
	}
	/*********************************	END OF LOGGEDIN USERINFO SETTER GETTER	**********************************/





	/****************************************** 	API URLS SETTER GETTER	***************************************/
	setApiBasePath() {
		// this.apiBasePath = "http://testwinserver.annectos.net";
		// this.apiBasePath2 = "http://testwinserver.annectos.net";
		// this.apiBasePath4 ="http://testwinserver.annectos.net";
 
		// this.apiBasePath = "http://localhost";
		// this.apiBasePath2 = "http://localhost";
		// this.apiBasePath4 = "http://localhost";

		this.apiBasePath = "http://phoenix2backend.goldencircle.in";
		this.apiBasePath2 = "http://phoenix2backend2.goldencircle.in";
		this.apiBasePath4 = "http://phoenix2backend4.goldencircle.in";
	}

	getprogramInfo() { 
		return JSON.parse(localStorage.getItem("loggedInProgramInfo"));
	}

	setServicePorts() {
		this.servicePorts = {};

		this.servicePorts.configuration = 1331;
		this.servicePorts.logsService = 1332;
		this.servicePorts.authentication = 1337;
		this.servicePorts.internalRolesUsers = 1338;
		this.servicePorts.businessEntity = 1339;
		this.servicePorts.clientInit = 1340;
		this.servicePorts.programSetup = 1334;
		this.servicePorts.programOperations = 1334;
		this.servicePorts.clientSetup = 1334;
		this.servicePorts.rewardsGallery = 1341;
		this.servicePorts.registerMe = 1337;
		this.servicePorts.placeOrder = 1345;
		this.servicePorts.displayContest = 1353;
		this.servicePorts.communication = 1335;
		this.servicePorts.leadManagement = 1334;
		this.servicePorts.askAnExpert = 1382;
	}

	setApiUrls() {
		this.apiUrls = {};


		/**********************************		REWARD GALLERY APIS 	************************************* */
		//Global Reward Gallery 
		this.apiUrls.getGlobalProductMaster = this.apiBasePath + ":" + this.servicePorts.rewardsGallery + "/getGlobalProductMaster/v1";
		this.apiUrls.getAllCategoryAndSubCategory = this.apiBasePath + ":" + this.servicePorts.rewardsGallery + "/getAllCategoryAndSubCategory/v1";
		this.apiUrls.getBrandMaster = this.apiBasePath + ":" + this.servicePorts.rewardsGallery + "/getBrandMaster/v1";

		//Program Specific Reward Gallery
		this.apiUrls.getSpecificRewardsGalleryProduct = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getSpecificRewardsGalleryProduct/v1";
		this.apiUrls.getSpecificCategoriesAndSubCategories = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getSpecificCategoriesAndSubCategories/v1";
		this.apiUrls.getSpecificBrands = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getSpecificBrands/v1";
		this.apiUrls.getRewardsGallerySlabs = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getRewardsGallerySlabs/v1";
		/****************************** 	END OF REWARD GALLERY APIS 	************************************* */



		this.apiUrls.getAllProducts = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllProduct/v1";
		this.apiUrls.getAllBrands = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllClientBrands/v1";
		this.apiUrls.getAllClient = this.apiBasePath + ":" + this.servicePorts.clientInit + "/getAllClients/v1";
		this.apiUrls.getClientCategoryAndSubCategory = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getClientCategoryAndSubCategory/v1";
		this.apiUrls.getClientUser = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllClientUsers/v1";

		// Authentication Service APIs
		this.apiUrls.login = this.apiBasePath + ":" + this.servicePorts.authentication + "/userLogin/v1";
		this.apiUrls.forgotPassword = this.apiBasePath + ":" + this.servicePorts.authentication + "/forgotPassword/v1";

		this.apiUrls.getProgramUser = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getAllProgramUsers/v1";

		//Config Service APIs
		this.apiUrls.getStates = this.apiBasePath + ":" + this.servicePorts.configuration + "/getAllStates/v1";
		this.apiUrls.getCities = this.apiBasePath + ":" + this.servicePorts.configuration + "/getAllCities/v1";
		this.apiUrls.getPrograms = this.apiBasePath + ":" + this.servicePorts.clientInit + "/getAllProgramFromMaster/v1";

		// Sign Up APIs
		this.apiUrls.signup = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/addBusinessEntity/v1";

		this.apiUrls.signupTodo = this.apiBasePath + ":" + this.servicePorts.authentication + "/registerme/calling/todo";



		this.apiUrls.updateClientUser = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/updateClientUser/v1";
		this.apiUrls.updateEntity = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/updateBusinessEntity/v1";
		this.apiUrls.updateEntityUser = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/updateBusinessEntityUser/v1";

		//Claims
		this.apiUrls.getAllSecondarySales = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getAllSecondarySales/v1";
		this.apiUrls.returnSecondarySale = this.apiBasePath + ":" + this.servicePorts.programOperations + "/returnSecondarySale/v1";

		// Local Add Claims Starts
		this.apiUrls.addSecondarySale = this.apiBasePath + ":" + this.servicePorts.programOperations + "/addSecondarySale/v1";
		// Local Add Claims Starts

		this.apiUrls.getAllPackagingUnits = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllClientPackaging/v1";

		//Dashboard 
		this.apiUrls.getUserSalesClaimsDetails = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getUserSalesClaimsDetails/v1";
		this.apiUrls.getAllTopFives = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getTopBottomRecords/v1";
		this.apiUrls.getTopRetailersProductwise = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getTopRetailersProductwise/v1";
		// Reedemption
		this.apiUrls.getChannelOrders = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/getAllOrders/v1";

		// Channels 
		this.apiUrls.updateSecondarySale = this.apiBasePath + ":" + this.servicePorts.programOperations + "/updateSecondarySale/v1";
		this.apiUrls.getMyChannels = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getMyChannels/v1";
		this.apiUrls.getMyChannels1 = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getMyChannels/v1";
		this.apiUrls.updateOrder = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/updateOrder/v1";
		this.apiUrls.findIfChannelExist = this.apiBasePath + ":" + this.servicePorts.programOperations + "/findIfChannelExist/v1";
		// About The Program Page
		this.apiUrls.getAllScheme = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getProgramScheme/v1";
		this.apiUrls.getProgramScheme = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getProgramScheme/v1";

		//Unique Code
		this.apiUrls.getAllUniqueCodes = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getAllUniqueCodes/v1";

		// Home
		this.apiUrls.getAllOrders = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/addOrders/v1";
		this.apiUrls.addPointConversionRequest = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/addPointConversionRequest/v1";
		this.apiUrls.getPointConversionRequest = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/getPointConversionRequest/v1";
		this.apiUrls.getAllStarPointConfiguration = this.apiBasePath + ":" + this.servicePorts.rewardsGallery + "/getAllStarPointConfiguration/v1";

		// Reedemption
		this.apiUrls.addOrders = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/getAllOrders/v1";

		//SpecialOffers
		this.apiUrls.getNonTransactionalBoosters = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getNonTransactionalBoosters/v1";
		this.apiUrls.addDisplayContest = this.apiBasePath + ":" + this.servicePorts.programSetup + "/addDisplayContest/v1";
		this.apiUrls.getDisplayContest = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getDisplayContest/v1";
		this.apiUrls.updateDisplayContest = this.apiBasePath + ":" + this.servicePorts.programSetup + "/updateDisplayContest/v1";

		this.apiUrls.addToCart = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/addToCart/v1";
		this.apiUrls.updateToCart = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/updateToCart/v1";
		this.apiUrls.getCart = this.apiBasePath2 + ":" + this.servicePorts.placeOrder + "/getCart/v1";

		//product catalog
		this.apiUrls.getClientBrands = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllClientBrands/v1";
		this.apiUrls.getFilterClientBrands = this.apiBasePath + ":" + this.servicePorts.clientSetup + "/getAllClientBrands/v1";
		this.apiUrls.sendEmail = this.apiBasePath + ":" + this.servicePorts.communication + "/sendEmail/v1";

		// business entity info
		this.apiUrls.getBusinessEntity = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/getBusinessEntity/v1";

		//to add login log
		this.apiUrls.addLoginLog = this.apiBasePath + ":" + this.servicePorts.logsService + "/addUserLog/v1";

		//Get Region Performance
		this.apiUrls.getRegionsPerformance = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getRegionsPerformance/v1";
		this.apiUrls.getComparisionReport = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getComparisionReport/v1";


		//updateProgramusers
		this.apiUrls.updateProgramUser = this.apiBasePath + ":" + this.servicePorts.programSetup + "/updateProgramUserProfileQuestions/v1";
		this.apiUrls.getProfileQuestions = this.apiBasePath + ":" + this.servicePorts.clientInit + "/getProfileQuestions/v1";

		//leaderboard 
		this.apiUrls.getTop5DealersOrDistributors = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getTop5DealersOrDistributors/v1";
		this.apiUrls.getLeaderBoard = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getLeaderBoard/v1";

		this.apiUrls.getProgramParticipantsReport = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getProgramParticipantsReport/v1";
		this.apiUrls.getProgramLoginReport = this.apiBasePath + ":" + this.servicePorts.logsService + "/getUserLoginReport/v1";

		this.apiUrls.getAllRetailers = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/getAllRetailers/v1";

		//Get Todays Birthday Count
		this.apiUrls.getTodaysBirthdayCount = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/getTodaysBirthdayCount/v1";


		//Get Todays Birthday Data
		this.apiUrls.getTodaysBirthday = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/getTodaysBirthday/v1";
		//lead management
		this.apiUrls.addLead = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/addLead/v1";
		this.apiUrls.getLeads = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/getLeads/v1";
		this.apiUrls.getLeadReport = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/getLeadReport/v1";
		this.apiUrls.updateLead = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/updateLead/v1";
		this.apiUrls.getSuperstarsOfTheMonth = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/getStarOfMonth/v1";

		this.apiUrls.countLead = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/countLeads/v1";
		this.apiUrls.getEvents = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getEvent/v1";
		this.apiUrls.getCelebrations = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getCelebrations/v1";


		//For Distributor Total Sales
		this.apiUrls.getDistributorSalesDetails = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getDistributorSalesDetails/v1";
		this.apiUrls.getCoatedProducts = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getCoatedProducts/v1";

		//To Get User Notifications
		this.apiUrls.getUserNotifications = this.apiBasePath + ":" + this.servicePorts.programOperations + "/getUserNotifications/v1";

		//To Add User Notifications
		this.apiUrls.addUserNotifications = this.apiBasePath + ":" + this.servicePorts.programOperations + "/addUserNotifications/v1";


		//To Update User Notifications
		this.apiUrls.updateUserNotifications = this.apiBasePath + ":" + this.servicePorts.programOperations + "/updateUserNotifications/v1";

		this.apiUrls.findBusinessEntityUser = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/findBusinessEntityUser/v1";
		this.apiUrls.migrateBusinessEntity = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/updateBusinessEntity/v1";
		this.apiUrls.addProgramUser = this.apiBasePath + ":" + this.servicePorts.programSetup + "/addProgramUser/v1";


		this.apiUrls.uploadLeaderboardData = this.apiBasePath + ":" + this.servicePorts.programOperations + "/uploadLeaderboardData/v1";


		this.apiUrls.uploadHallOfFameData = this.apiBasePath + ":" + this.servicePorts.leadManagement + "/addStarOfMonth/v1";


		this.apiUrls.getBusinessEntityUsers = this.apiBasePath + ":" + this.servicePorts.businessEntity + "/getBusinessEntityUsers/v1";


		this.apiUrls.addEventRegistration = this.apiBasePath + ":" + this.servicePorts.programSetup + "/addEventRegistration/v1";

		this.apiUrls.getEventRegistration = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getEventRegistration/v1";


		// feedback
		this.apiUrls.giveFeedback = this.apiBasePath + ":" + this.servicePorts.authentication + "/giveFeedback/v1";
		this.apiUrls.getFeedback = this.apiBasePath + ":" + this.servicePorts.authentication + "/getFeedback/v1";

		this.apiUrls.getProgramRegions = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getProgramRegions/v1";


		// ASKANEXPERT
		this.apiUrls.addAnswer = this.apiBasePath4 + ":" + this.servicePorts.askAnExpert + "/addAnswer/v1";

		this.apiUrls.getForumQuestions = this.apiBasePath4 + ":" + this.servicePorts.askAnExpert + "/getForumQuestions/v1";

		this.apiUrls.getAnswer = this.apiBasePath4 + ":" + this.servicePorts.askAnExpert + "/getAnswer/v1";

		this.apiUrls.updateForumQuestion = this.apiBasePath4 + ":" + this.servicePorts.askAnExpert + "/updateForumQuestion/v1";

		this.apiUrls.updateForumQuestions = this.apiBasePath4 + ":" + this.servicePorts.askAnExpert + "/updateForumQuestion/v1";


		this.apiUrls.getAllProgramUsers = this.apiBasePath4 + ":" + this.servicePorts.programSetup + "/getAllProgramUsers/v1";

		// engineer request API
		this.apiUrls.getApprovedEngineerRequest = this.apiBasePath + ":" + this.servicePorts.programSetup + "/getApprovedEngineerRequest/v1";


	}



	getApiUrls() {

		return this.apiUrls;
	}

	/***************************************** 	END OF API URLS SETTER GETTER	************************************/



	/************************************ 	STATUS TOKEN SETTER GETTER 	*******************************************/
	setStatustokens() {
		this.statusTokens = {};

		this.statusTokens.RESP_ROLLBACK_ERROR = -2;
		this.statusTokens.RESP_SERVER_ERROR = -1;
		this.statusTokens.RESP_SUCCESS = 0;
		this.statusTokens.RESP_AUTH_FAIL = 1;
		this.statusTokens.RESP_FAIL = 2;
		this.statusTokens.RESP_ALREADY_EXIST = 3;
		this.statusTokens.RESP_BLOCKED = 4;
		this.statusTokens.RESP_KYC_AWAITING = 5;
	}

	getStatusTokens() {
		return this.statusTokens;
	}
	/*****************************	END OF STATUS TOKEN SETTER GETTER 	*****************************************/




	/********************************* 		API CALLS 		******************************************/
	getPrograms(requestObject) {
		let url = this.getApiUrls().getPrograms;
		let reqObj = requestObject;
		var frontendInfo = this.getFrontEndUserInfo()
		if (frontendInfo != undefined) {
			reqObj.frontendUserInfo = frontendInfo;
		} else {
			reqObj.frontendUserInfo = {
				"userId": this.userId
			};
		}


		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.getAuthenticationToken()
		});
		let options = new RequestOptions({ headers: headers });

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				//console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}

	getStates() {
		let url = this.getApiUrls().getStates;
		let reqObj = {
			searchString: "",
			sort: "stateName ASC"
		};
		let headers = new Headers({
			'Content-Type': 'application/json',
			// 'Authorization': 'Bearer ' + this.getAuthenticationToken()
		});
		let options = new RequestOptions({ headers: headers });

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				//console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}

	getCities(state) {
		let url = this.getApiUrls().getCities;
		let reqObj = {
			stateName: state,
			sort: "cityName ASC"
		};
		let headers = new Headers({
			'Content-Type': 'application/json',
			// 'Authorization': 'Bearer ' + this.getAuthenticationToken()
		});
		let options = new RequestOptions({ headers: headers });

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				//console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}
	/*****************************		END OF API CALLS	******************************************/
	getProgramUser(reqObj): any {
		// var frontEndInfo = this.frontendUserInfo;
		var frontEndInfo = this.getFrontEndUserInfo();
		this.userInfo = this.getLoggedInUserInfo();
		var userIds: any = {};
		if (this.userInfo.clientId) {
			userIds = this.userInfo.clientId;
		} else if (this.userInfo.userId) {
			userIds = this.userInfo.userId;
		}
		// //console.log("fR",frontEndInfo);       
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {
			frontEndInfo["userId"] = userIds;
		}
		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		reqObj.serviceType = "programSetup";
		reqObj.frontendUserInfo = frontEndInfo;
		//console.log(reqObj);
		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		let url = this.getApiUrls().getProgramUser;
		reqObj.serviceType = "programSetup";
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				//console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				//console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	//Get application Config 
	getApplicationConfig(): any {
		let url = this.apiBasePath + ":1331/getApplicationConfig/v1";
		// //console.log("application config ******************", this.appIdReqObj);

		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ',

		});

		let options2 = new RequestOptions({ headers: headers });
		let reqBody = { "isClient": true };
		//console.log(options2);
		//console.log("inside getapplication config service");
		return this.http.post(url, reqBody, options2)
			.map((res: Response) => {
				//console.log("res.............................", res);
				return res.json();

			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || "Server error");
			});
	}

	getBEInfoAPI(reqObj) {
		let url = this.getApiUrls().getBusinessEntity;
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ',

		});

		let options2 = new RequestOptions({ headers: headers });
		return this.http.post(url, reqObj, options2)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || "Server error");
			});
	}

	// Setter and Getter to check whether reward gallery page is selected
	setRewardGalleryFlag(isRewardGallery) {
		this.isRewardGallery = isRewardGallery;
	}

	getRewardGalleryFlag() {
		return this.isRewardGallery;
	}

	// Setter and Getter to check whether Logged In
	setLoginFlag(isLogin) {
		this.isLogin = isLogin;
	}

	getLoginFlag() {
		return this.isLogin;
	}

	// setter and getter for point or coupon
	setTerminology(terminology) {
		this.terminology = terminology;
	}

	getTerminology() {
		return this.terminology;
	}

	setClientObj(clientObj) {
		this.clientObj = clientObj;
	}

	getClientObj() {
		return this.clientObj;
	}

	setThemeColor(themeColor) {
		this.themeColor = themeColor;
	}

	getThemeColor() {
		return this.themeColor;
	}

	/********************************* 	SET RECORDS FOR DETAILS PAGE 	*********************** */
	public categorySubCategoriesArray: any[] = [];
	public slabsArray: any[] = [];

	setCategorySubCategories(categorySubCategoriesArray) {
		console.log("in set sub category EEEEEEEEE", categorySubCategoriesArray);
		this.categorySubCategoriesArray = categorySubCategoriesArray;
	}

	getCategorySubCategories() {
		return this.categorySubCategoriesArray;
	}

	setProductCatalogCategorySubCategories(ProductCatalogCategorySubCategories) {
		this.ProductCatalogCategorySubCategories = ProductCatalogCategorySubCategories;

	}

	getProductCatalogCategorySubCategories() {
		return this.ProductCatalogCategorySubCategories;
	}

	setSlabs(slabsArray) {
		this.slabsArray = slabsArray;

	}

	getSlabs() {
		return this.slabsArray
	}

	setChildsIdArray(childsArray) {
		this.childsIdArray = childsArray;
	}

	getChildsIdArray() {
		return this.childsIdArray;
	}

	setBEProgramsMapped(programsArray) {
		this.BEProgramsMapped = programsArray;
		console.log("this.BEProgramsMapped---", this.BEProgramsMapped);
	}
	getBEProgramsMapped() {
		return this.BEProgramsMapped;
	}

	setFlagToBirthdayNotification(flag) {
		this.birthdayNotificationFlag = flag;
		if (flag === true) {
			this.activityNotificationFlag = false;
			this.wishNotificationFlag = false;
		}
	}
	getFlagToBirthdayNotification() {
		return this.birthdayNotificationFlag;
	}

	setFlagToActivityNotification(flag) {
		this.activityNotificationFlag = flag;
		if (flag === true) {
			this.birthdayNotificationFlag = false;
			this.wishNotificationFlag = false;
		}
	}
	getFlagToActivityNotification() {
		return this.activityNotificationFlag;
	}


	setFlagToWishNotification(flag) {
		this.wishNotificationFlag = flag;
		if (flag === true) {
			this.birthdayNotificationFlag = false;
			this.activityNotificationFlag = false;
		}
	}
	getFlagToWishNotification() {
		return this.wishNotificationFlag;
	}




	setEntity(entity) {
		this.entityDetails = entity;
		console.log("Entity", this.entityDetails);
	}

	getEntity() {
		return this.entityDetails;
	}

	setLead(lead) {
		this.leadDetails = lead;
		console.log("Lead", this.leadDetails);
	}
	getLead() {
		return this.leadDetails;
	}

	setLeadApprovalFlag(flag) {
		this.leadApproval = flag;
	}
	getLeadApprovalFlag() {
		return this.leadApproval;
	}

	setEvent(event) {
		this.eventArray = event;
	}
	getEvent() {
		return this.eventArray;
	}


	setPastEventDetails(pastEvent) {
		this.pastEventDetails = pastEvent;
	}
	getPastEventDetails() {
		return this.pastEventDetails;
	}


	setContestDetails(contest) {
		this.contestDetails = contest;
	}
	getContestDetails() {
		return this.contestDetails;
	}



	setUpcomingContestDetails(contest) {
		this.upcomingContestDetails = contest;
	}
	getUpcomingContestDetails() {
		return this.upcomingContestDetails;
	}


	setPastContestGallery(contest) {
		this.pastContestGallery = contest;
	}
	getPastContestGallery() {
		return this.pastContestGallery;
	}

	setLeadDashboardLink(leadLink) {
		this.routeName = leadLink;
	}
	getLeadDashboardLink() {
		return this.routeName;
	}
	passbackiconflag() {

	}
	getbackiconflag(flag) {

	}
	/***************************** 	END OF SET RECORDS FOR DETAILS PAGE 	********************** */

	setModalClose(close) {
		this.modalClose = close;
	}

	getModalClose() {
		return this.modalClose;
	}

	setCelebration(celebration){
		this.celebration = celebration;
		console.log("in config service",this.celebration);
	}

	getCelebration(){
		return this.celebration;
	}

	setEngineerRequestFilter(statusFilter, stateFilter){
		this.selectedStateFilter = stateFilter;
		this.selectedStatusFilter = statusFilter;
		// this.isSetSelectedFilter = true;
	}

	getEngineerRequestFilter(){
		return {
			stateFilter: this.selectedStateFilter,
			statusFilter: this.selectedStatusFilter
		}
	}

	setSelectedFilter(value){
		this.isSetSelectedFilter = value;
	}

	getSelectedFilter(){
		return this.isSetSelectedFilter;
	}

}
