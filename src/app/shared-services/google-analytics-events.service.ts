import {Injectable} from "@angular/core";

@Injectable()
export class GoogleAnalyticsEventsService {

  public emitEvent(eventCategory: string,
                   eventAction: string,
                   eventLabel: string = null,
                   eventValue: number = null) {

  //  for web
    ga('send', 'event', {
      eventCategory: eventCategory,
      eventLabel: eventLabel,
      eventAction: eventAction,
      eventValue: eventValue
    });
    console.log("inside GoogleAnalyticsEventsService");

    // for mobile
    // (<any>window).ga.trackEvent(eventCategory, eventAction, eventLabel, eventValue)
  }
}