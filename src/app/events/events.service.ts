import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
// import { HeaderComponent } from '../home/header/header.component';



@Injectable()

export class EventsService {
    public subject = new Subject<any>();
    backicon:boolean= false;
    

    constructor(private http: Http,
        private configService: ConfigService
        // private headercomponent : HeaderComponent
     ) {

    }

    addLead(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().addLead;

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    getEvents(obj) {
        let url = this.configService.getApiUrls().getEvents;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;

            //To do remove later
            delete request.frontendUserInfo.appType;

        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };


    updateLead(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().updateLead;

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };




    addEventRegistration(obj) {
        let url = this.configService.getApiUrls().addEventRegistration;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;



        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };


   getbackiconflag(flag)
   { 
       console.log("inside getbackiconflag events service",flag);
       this.backicon = flag;
       
   }
   passbackiconflag()
   {
   
    console.log("inside passbackiconflag events service",this.backicon);
       
       return this.backicon;

   }

    getEventRegistration(obj) {
        let url = this.configService.getApiUrls().getEventRegistration;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;



        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };
    // sendMessage(message: string) {
    //     this.subject.next({ text: message });
    //     console.log("inside subject send message on eevnt service messageeeeee",message);
    // }

    // getMessage(): Observable<any> {
    //     console.log("inside subject send message on eevnt service",this.subject.asObservable());
    //     return this.subject.asObservable();
    //     // console.log("inside subject send message on eevnt service",this.subject.asObservable());
    // }

    // clearMessage() {
    //     this.subject.next();
    // }

    sendMessage(message: string) {
        console.log("inside send message fun in event service");
        this.subject.next({ text: message });
    }

    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        console.log("inside get message fun in event service");
        return this.subject.asObservable();
    }

}