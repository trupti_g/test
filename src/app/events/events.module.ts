/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "@angular/material";
import { FooterModule } from "../footer/footer.module";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { SharedModule } from "../shared-components/shared.module";
import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
import { EventComponent } from "./event.component";
import { EventDetailComponent } from "./event-detail.component";
import { EventsService } from "./events.service";

import { EventGalleryComponent } from "./eventgallery.component";

@NgModule({
  declarations: [EventComponent, EventDetailComponent, EventGalleryComponent],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    FooterModule,
    ChartsModule,
    AmChartsModule,
    SharedModule,
    NgbCarouselModule.forRoot()
  ],
  exports: [],

  providers: [EventsService],
  entryComponents: []
})
export class EventModule { }
