import { EventModule } from './events.module';
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { EventsService } from "./events.service";
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    selector: 'app-event',
    templateUrl: './event-detail.component.html',
    styleUrls: ['./event-detail.component.scss', "../app.component.scss"],
    providers: [Device]
})
export class EventDetailComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    isDealer: boolean = false;
    isDistributor: boolean = false;
    isEngineer: boolean = false;
    noRecords: boolean;
    programUserInfo: any;
    upcomingEvents: any = [];
    pastEvents: any = [];
    public eventArray: any = [];
    eventGallery: boolean;
    eventslist: any;
    showLoader: boolean;
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public eventImages: Array<any> = [];
    public image: Array<any> = [];
    n: Date;
    public m: any = [];



    constructor(private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private zone: NgZone,
        private activatedRoute: ActivatedRoute,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private route: ActivatedRoute,
        private http: Http,
        private eventsService: EventsService,
        public sidebar: SidebarComponent,
        private stringService: StringService,
        private device: Device,
    ) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }


        this.eventArray = [this.configService.getEvent()];
        console.log("this.eventArray", this.eventArray);
        this.programUserInfo = configService.getloggedInProgramUser();


        var dateTodayZ = new Date();
        var dateTodayZstring = dateTodayZ.toISOString();
        this.m = dateTodayZstring.split('T')
        this.n = new Date(this.m[0]);
        console.log("NEW DATE", this.n);

        this.sliders = [
            {
                imagePath: 'assets/slider1.jpg',
                label: 'First slide',
                text:
                    'Marathon in Maleswaram - 2018'
            },
            {
                imagePath: 'assets/slider2.jpg',
                label: 'Second slide',
                text: 'Cooking Contest 2018'
            },
            {
                imagePath: 'assets/slider3.jpg',
                label: 'Third slide',
                text:
                    'Beauty Peagent Contest - 2018'
            }
        ]

    }
    ngOnInit() {

        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Event Details")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Event Details");
                    ga('send', 'pageview');
                }
            });
        }

        this.sidebar.close();
        this.isEngineer = true;
        this.isDistributor = false;
        this.getEvents();

    }
    // function to select contest (ASM login)
    setEventprofile(profile) {
        console.log("profile is ", profile);
        if (profile == 0) {
            this.isDealer = true;
            this.isDistributor = false;
            this.isEngineer = false;
        } else if (profile == 1) {
            this.isDealer = false;
            this.isDistributor = true;
            this.isEngineer = false;
        } else if (profile == 2) {
            this.isDealer = false;
            this.isDistributor = false;
            this.isEngineer = true;
        }
        this.pastEvents = [];
        this.noRecords = false;
        this.getEvents();
    }

    getEvents() {

        var obj = {
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            programRoleId: this.programUserInfo.programRole,
            serviceType: "programSetup",
            sort: -1
        }

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            if (this.isDealer == true) {
                console.log("000000000000000000000000000");
                obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            } else if (this.isDistributor == true) {
                console.log("1111111111111111");
                obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            } else {
                console.log("222222222222222222222222");
                obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            }
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
        } if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        }else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
        }
        this.showLoader = true;
        this.eventsService.getEvents(obj).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("Event Result", responseObject);
                        this.eventslist = responseObject.result;
                        for (var a = 0; a < this.eventslist.length; a++) {

                            if (this.eventslist[a].startDate && this.eventslist[a].endDate) {
                                var i = this.eventslist[a].startDate.split('T');

                                var x = new Date(i[0]);
                                console.log("Start Date", x);

                                var j = this.eventslist[a].endDate.split('T');
                                var y = new Date(j[0]);
                                console.log("End Date", y);
                                console.log("Todays Date", this.n);
                                if (x < this.n && y < this.n) {
                                    console.log("Upcoming Events");
                                    this.pastEvents.push(this.eventslist[a]);
                                } else if ((x >= this.n && y >= this.n) || (x <= this.n && y >= this.n)) {
                                    this.upcomingEvents.push(this.eventslist[a]);
                                }
                            }
                        }

                        if (this.pastEvents.length === 0) {
                            this.noRecords = true;
                        }
                        else {
                            this.noRecords = false;
                        }


                        for (var a = 0; a < this.pastEvents.length; a++) {
                            console.log("startDate", this.pastEvents[a].startDate);
                            console.log("endDate", this.pastEvents[a].endDate);
                            var date = new Date(this.pastEvents[a].startDate);
                            var startDate = date.toDateString();
                            this.pastEvents[a].startDate = startDate;


                            var date1 = new Date(this.pastEvents[a].endDate);
                            var endDate = date1.toDateString();
                            this.pastEvents[a].endDate = endDate;



                        }


                        console.log("Upcoming Events", this.upcomingEvents);
                        console.log("Past Events", this.pastEvents);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.noRecords = true;
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    };


    showgallery(past) {

        this.googleAnalyticsEventsService.emitEvent("Past Events", "View", past.eventName);

        console.log("Inside Gallery Function");
        console.log("Past Event", past);

        var pasteventDeatils = past;
        this.configService.setPastEventDetails(pasteventDeatils);



        this.router.navigate(["/home/eventgallery"]);
    }



}

