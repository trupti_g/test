import { EventModule } from './events.module';
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { EventsService } from "./events.service";
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Location } from '@angular/common';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    selector: 'app-event',
    templateUrl: './eventgallery.component.html',
    styleUrls: ['./eventgallery.component.scss', "../app.component.scss"],
    providers: [Device]
})
export class EventGalleryComponent implements OnInit {

    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    endDate: string;
    startDate: string;
    eventlocation: any;
    eventVenue: any;
    contact: any;
    location: any;
    galleryImages: any = [];
    eventdescription: any;
    eventName: any;
    eventId: any;
    getPastEvent: any;
    upcomingEvents: any = [];
    pastEvents: any = [];
    public eventArray: any = [];
    eventGallery: boolean;
    eventslist: any;
    showLoader: boolean;
    backicon: boolean = true;
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public eventImages: Array<any> = [];
    public image: Array<any> = [];
    n: Date;
    public m: any = [];
    programUserInfo: any;
    noRecordMessage:boolean = false;

    public galleryVideoArr:any[] = [];
    public galleryImagesArr:any[] = [];
    public isZoom:boolean = false;
    public imageURL:any;

    constructor(private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private zone: NgZone,
        private activatedRoute: ActivatedRoute,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private route: ActivatedRoute,
        private http: Http,
        private eventsService: EventsService,
        public sidebar: SidebarComponent,
        private stringService: StringService,
        private _location: Location,
        private device: Device,
    ) {

        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

     

        this.programUserInfo = configService.getloggedInProgramUser();
        this.eventArray = [this.configService.getEvent()];
        console.log("this.eventArray", this.eventArray);


        var dateTodayZ = new Date();
        var dateTodayZstring = dateTodayZ.toISOString();
        this.m = dateTodayZstring.split('T')
        this.n = new Date(this.m[0]);
        console.log("NEW DATE", this.n);

        this.sliders = [
            {
                imagePath: '../assets/slider1.jpg',
                label: 'First slide',
                text:
                    'Marathon in Maleswaram - 2018'
            },
            {
                imagePath: '../assets/slider2.jpg',
                label: 'Second slide',
                text: 'Cooking Contest 2018'
            },
            {
                imagePath: '../assets/slider3.jpg',
                label: 'Third slide',
                text:
                    'Beauty Peagent Contest - 2018'
            }
        ]

    }

    backClicked() {
        this._location.back();
    }

    ngOnInit() {
        
         // google analytics page view code
         if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Event gallery")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Event gallery");
                    ga('send', 'pageview');
                }
            });
        }

        this.sidebar.close();
        this.stringService.setImageFormats();
        this.stringService.setVideoFormats();
        this.sidebar.addExpandClass('pages');
        this.getPastEvent = this.configService.getPastEventDetails();
        console.log("Past Event Details", this.getPastEvent);
        this.eventId = this.getPastEvent.eventId;
        this.getEvents();
        this.backicon == true;
        console.log("In Event Gallery on init method", this.backicon);
        this.passbackiconflag();
        this.configService.passbackiconflag();
        this.configService.getbackiconflag(this.backicon);
        this.sendMessage();
    }
    sendMessage(): void {
        // send message to subscribers via observable subject
        console.log("Inside send message function event gallery");
        this.eventsService.sendMessage('Message from Home Component to App Component!');
    }

    clearMessage(): void {
        // clear message
        this.eventsService.clearMessage();
    }

    passbackiconflag() {
        this.backicon = true;
        console.log("inside passbackiconflag eventgallery", this.backicon)
        this.eventsService.getbackiconflag(this.backicon);

    }

    getEvents() {

        var obj = {
            "eventId": this.eventId,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup"
        }
        this.showLoader = true;
        this.eventsService.getEvents(obj).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("Event Result", responseObject);
                        this.eventslist = responseObject.result;
                        for (var a = 0; a < this.eventslist.length; a++) {


                            this.eventName = this.eventslist[a].eventName;
                            this.eventdescription = this.eventslist[a].eventDescription;
                            this.eventVenue = this.eventslist[a].eventVenue;
                            this.eventlocation = this.eventslist[a].location;
                            this.contact = this.eventslist[a].contact;
                            this.galleryImages = this.eventslist[a].galleryImages;
                            var date = new Date(this.eventslist[a].startDate);
                            var startDate = date.toDateString();
                            this.startDate = startDate;

                            var date = new Date(this.eventslist[a].endDate);
                            var endDate = date.toDateString();
                            this.endDate = endDate;
                        }

                        if(this.galleryImages.length===0){
                            this.noRecordMessage=true;
                        }

                        // to separate images and videos 
                        this.galleryImagesArr = [];
                        this.galleryVideoArr = [];

                        for(let i=0;i<this.galleryImages.length;i++){
                            // to extract and store extension from the media
                            var ext: string = this.galleryImages[i].slice(this.galleryImages[i].lastIndexOf('.'),this.galleryImages[i].length).toLowerCase();
                            console.log("ext",ext);
                            
                            var imageFormats=this.stringService.getImageFormats();
                            var videoFormats=this.stringService.getVideoFormats();
                            var isFound=false; //set true if type as image is found

                            // to check for image
                            for (let x = 0; x < imageFormats.length; x++) {
                                console.log("imageFormats[x]",imageFormats[x]);
                                if (ext === imageFormats[x]) {
                                    console.log("MATCHED!!");
                                    this.galleryImagesArr.push(this.galleryImages[i]);
                                    isFound=true; 
                                    break;
                                }
                            }

                            // to check for videos
                            if(isFound === false){ // to avoid checking for video if already found as image

                                for (let x = 0; x < videoFormats.length; x++) {
                                    console.log("videoFormats[x]",videoFormats[x]);
                                    if (ext === videoFormats[x]) {
                                        console.log("MATCHED!!");
                                        this.galleryVideoArr.push(this.galleryImages[i]);
                                        break;
                                    }
                                }
                            }
                        }

                        console.log("final images array",this.galleryImagesArr);
                        console.log("final videos array",this.galleryVideoArr);
                        console.log("Gallery Images", this.galleryImages);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    };



    backtopastEvents() {
        this.googleAnalyticsEventsService.emitEvent("Past Events", "Back clicked", "Back to events");
        console.log("Inside  backtopastEvents Function");
        this.router.navigate(["/home/event-detail"]);
    }

    setmargin() {
        console.log("I am in margin function");
        if ((this.displaymobile === true) && (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole)) {
            document.getElementById("maindiv").style.marginTop = "6rem";
        }
    }
    // sendMessage(): void {
    //     // send message to subscribers via observable subject
    //     console.log("send message method in eventgallery page",this.backicon);
    //     this.eventsService.sendMessage(this.backicon);
    //     // console.log("send message method in eventgallery page",this.backicon);
    // }


    zoom(n) {
		this.configService.setModalClose(false);
		if (this.isZoom === false) {
			this.isZoom = true;
			this.imageURL = n;
		}
	}

	closeDetails() {
		this.configService.setModalClose(true);
		if (this.isZoom === true) {
			this.isZoom = false;
		}
	}





}

