import { EventModule } from './events.module';
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { EventsService } from "./events.service";
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { StringService } from "../shared-services/strings.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  providers: [Device]
})
export class EventComponent implements OnInit {
  carddiv: boolean = true;
  seedetailsdiv: boolean = false;
  mobilewidth: any;
  displaypc: boolean = true;
  displaymobile: boolean = false;
  noRecords: boolean = false;
  showRegisteredMessage: boolean = false;
  isDealer: boolean = false;
  isDistributor: boolean = false;
  isEngineer: boolean = false;
  eventDescrption: any;
  public registeredEventId: any = [];
  eventIdArray: any = [];
  programUserInfo: any;
  Eventname: any;
  eventId: any;
  bannerImage: any;
  contact: any;
  location: any;
  eventVenue: any;
  showData: boolean = false;
  eventName: any;
  upcomingEvents: any = [];
  seeDetailsArray: any = [];
  pastEvents: any = [];
  n: Date;
  eventGallery: boolean;
  eventslist: any;
  showLoader: boolean;
  public alerts: Array<any> = [];
  public sliders: Array<any> = [];
  public eventImages: Array<any> = [];
  public image: Array<any> = [];
  public m: any = [];


  private registrationForm: FormGroup;





  public selectedTab: string;
  @ViewChild(FeedbackMessageComponent)
  private feedbackMessageComponent: FeedbackMessageComponent;

  constructor(private router: Router,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private zone: NgZone,
    private activatedRoute: ActivatedRoute,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private route: ActivatedRoute,
    private http: Http,
    private eventsService: EventsService,
    public sidebar: SidebarComponent,
    private stringService: StringService,
    private device: Device,
  ) {
    this.mobilewidth = window.screen.width;
    console.log("Width:", this.mobilewidth);
    if (this.mobilewidth <= 576) {
      this.displaypc = false;
      this.displaymobile = true;
    }
    else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
      this.displaypc = false;
      this.displaymobile = true;

    }
    else {
      this.displaypc = true;
      this.displaymobile = false;
    }


    this.programUserInfo = configService.getloggedInProgramUser();
    this.registrationForm = this.formBuilder.group({

      userName: ["", [Validators.required]],
      mobileNumber: ["", [Validators.required, Validators.pattern('[0-9]{10}')]],
      address: ["", [Validators.required]]

    });

    var dateTodayZ = new Date();
    var dateTodayZstring = dateTodayZ.toISOString();
    this.m = dateTodayZstring.split('T')
    this.n = new Date(this.m[0]);
    console.log("NEW DATE", this.n);
    this.sliders = [
      {
        imagePath: 'assets/slider1.jpg',
        label: 'First slide',
        text:
          'Marathon in Maleswaram - 2018'
      },
      {
        imagePath: 'assets/slider2.jpg',
        label: 'Second slide',
        text: 'Cooking Contest 2018'
      },
      {
        imagePath: 'assets/slider3.jpg',
        label: 'Third slide',
        text:
          'Beauty Peagent Contest - 2018'
      }
    ]
  }

  ngOnInit() {
	// google analytics page view code
	if(this.device.device === "android" || this.device.device === "ios"){
		console.log("inside android and ios");
		(<any>window).ga.trackView("Events")
	} else {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
			ga('set', 'page', "Events");
			ga('send', 'pageview');
			}
		});
	}

    this.sidebar.close();
    this.isEngineer = true;
    this.isDistributor = false;
    this.sidebar.addExpandClass('pages');
    this.getEvents();
  }

  // function to select contest (ASM login)
  setEventprofile(profile) {
    console.log("profile is ", profile);
    if (profile == 0) {
      this.isDealer = true;
      this.isDistributor = false;
      this.isEngineer = false;
    } else if (profile == 1) {
      this.isDealer = false;
      this.isDistributor = true;
      this.isEngineer = false;
    } else if (profile == 2) {
      this.isDealer = false;
      this.isDistributor = false;
      this.isEngineer = true;
    }
    this.upcomingEvents = [];
    this.noRecords = false;
    this.getEvents();
  }

  getEvents() {

    var obj = {
      programId: this.configService.getprogramInfo().programId,
      clientId: this.configService.getprogramInfo().clientId,
      programRoleId: this.programUserInfo.programRole,
      serviceType: "programSetup",
      sort: 1
    }
    if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
      obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
    } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
      this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
      this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
      if (this.isDealer == true) {
        console.log("000000000000000000000000000");
        obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
      } else if (this.isDistributor == true) {
        console.log("1111111111111111");
        obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
      } else {
        console.log("222222222222222222222222");
        obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
      }
    } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
      obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
    }else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
      obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
    }
    this.showLoader = true;
    this.eventsService.getEvents(obj).subscribe(
      (responseObject) => {
        this.showLoader = false;

        let responseCodes = this.configService.getStatusTokens();
        console.log("Response Codes", responseCodes);
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:
            console.log("Event Result", responseObject);
            this.eventslist = responseObject.result;

            if (this.eventslist) {
              for (var a = 0; a < this.eventslist.length; a++) {

                if (this.eventslist[a].startDate && this.eventslist[a].endDate) {
                  var i = this.eventslist[a].startDate.split('T');

                  var x = new Date(i[0]);
                  console.log("Start Date", x);

                  var j = this.eventslist[a].endDate.split('T');
                  var y = new Date(j[0]);
                  console.log("End Date", y);
                  console.log("Todays Date", this.n);
                  if (x < this.n && y < this.n) {
                    console.log("Upcoming Events");
                    this.pastEvents.push(this.eventslist[a]);
                  } else if ((x >= this.n && y >= this.n) || (x <= this.n && y >= this.n)) {
                    this.upcomingEvents.push(this.eventslist[a]);
                    console.log("RRRRRRRRRRRRRRRRRRRRR", this.upcomingEvents);
                  }
                }
              }

              if (this.upcomingEvents.length === 0) {
                this.noRecords = true;
              }
              else {
                this.noRecords = false;
              }

              for (var a = 0; a < this.upcomingEvents.length; a++) {
                console.log("startDate", this.upcomingEvents[a].startDate);
                console.log("endDate", this.upcomingEvents[a].endDate);
                var date = new Date(this.upcomingEvents[a].startDate);
                var startDate = date.toDateString();
                this.upcomingEvents[a].startDate = startDate;


                var date1 = new Date(this.upcomingEvents[a].endDate);
                var endDate = date1.toDateString();
                this.upcomingEvents[a].endDate = endDate;

                this.eventIdArray.push(this.upcomingEvents[a].eventId);

              }

              console.log("Upcoming Events", this.upcomingEvents);
              console.log("Past Events", this.pastEvents);

              this.getEventRegistration();
            }

            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:

            this.noRecords = true;

            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => {
        this.showLoader = false;
      }
    );
  };




  getEventRegistration() {

    var obj = {

      frontendUserInfo: this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
      programId: this.configService.getprogramInfo().programId,
      clientId: this.configService.getprogramInfo().clientId,
      programUserId: this.programUserInfo.programUserId,
      eventIdArray: this.eventIdArray,
      serviceType: "programSetup"
    }

    console.log("getEventRegistration Object", obj);



    this.showLoader = true;
    this.eventsService.getEventRegistration(obj).subscribe(
      responseObject => {
        let responseCodes = this.configService.getStatusTokens();
        console.log("Response Codes", responseCodes);
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:

            console.log("getEventRegistration", responseObject.result);

            if (responseObject.result) {

              for (var a = 0; a < responseObject.result.length; a++) {
                this.registeredEventId.push(responseObject.result[a].eventId);
              }
              console.log("this.registerEventId", this.registeredEventId);

            }


            console.log("eventIdArray", this.eventIdArray);



            if (this.registeredEventId) {

              for (var a = 0; a < this.registeredEventId.length; a++) {
                const dom3: any = document.getElementById(this.registeredEventId[a]).style.display = "none";
                console.log(document.getElementById("reg" + this.registeredEventId[a]));
                const dom4: any = document.getElementById("reg" + this.registeredEventId[a]).style.display = "block";
                this.showLoader = false;
              }
            }
            //to display registerME for unregistered events
            var unregisteredEventID = [];
            console.log("UPCOMING EVENTS-------------------", this.upcomingEvents);
            for (var i = 0; i < this.upcomingEvents.length; i++) {
              var isExist = false;
              for (var j = 0; j < this.registeredEventId.length; j++) {
                if (this.upcomingEvents[i].eventId === this.registeredEventId[j]) {
                  isExist = true;
                  break;
                }
              }
              if (isExist == false) {
                unregisteredEventID.push(this.upcomingEvents[i].eventId);
              }

            }
            console.log("Unregistered events----", unregisteredEventID);
            if (unregisteredEventID) {

              for (var a = 0; a < unregisteredEventID.length; a++) {

                const dom3: any = document.getElementById(unregisteredEventID[a]).style.display = "initial";
              }
            }


            //this.showRegisteredMessage
            this.showLoader = false;
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            this.showLoader = false;
            console.log("inside RESP_FAIL")
              //to display registerME for unregistered events
              var unregisteredEventID = [];
              console.log("this.eventIdArray.length", this.upcomingEvents);
              for (var i = 0; i < this.eventIdArray.length; i++) {
                unregisteredEventID.push(this.eventIdArray[i]);
              }
              console.log("unregisteredEventID",unregisteredEventID);
              if (unregisteredEventID) {
  
                for (var a = 0; a < unregisteredEventID.length; a++) {
  
                  const dom3: any = document.getElementById(unregisteredEventID[a]).style.display = "initial";
                }
              }
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => {
        this.showLoader = false;
      }
    );

  }


  showEventDetail(event) {
    this.configService.setEvent(event);
    this.router.navigate(['../home/event-detail']);
  }





  seeDetails(upcoming) {
    this.googleAnalyticsEventsService.emitEvent("Ongoing Events", "View", upcoming.eventName);

    console.log("Upcoimng Event Deatil", upcoming);
    this.showData = true;

    console.log("this.showData", this.showData);
    this.seeDetailsArray = upcoming;
    this.eventName = upcoming.eventName;
    this.eventVenue = upcoming.eventVenue;
    this.location = upcoming.location;
    this.contact = upcoming.contact;
    this.bannerImage = upcoming.bannerImage;
    this.eventDescrption = upcoming.eventDescription;

    console.log("Upcoimng Event Deatil", this.seeDetailsArray);
    this.configService.setModalClose(false);




  }
  showdetailsdiv(upcoming) {
    this.googleAnalyticsEventsService.emitEvent("Ongoing Events", "View", this.eventName);

    this.carddiv = false;
    this.seedetailsdiv = true;
    console.log("Upcoimng Event Deatil", upcoming);
    this.showData = true;

    console.log("this.showData", this.showData);
    this.seeDetailsArray = upcoming;
    this.eventName = upcoming.eventName;
    this.eventVenue = upcoming.eventVenue;
    this.location = upcoming.location;
    this.contact = upcoming.contact;
    this.bannerImage = upcoming.bannerImage;
    this.eventDescrption = upcoming.eventDescription;

    console.log("Upcoimng Event Deatil", this.seeDetailsArray);
  }

  cancel() {
    console.log("Cancel Called");
    this.showData = false;
    this.configService.setModalClose(true);

    document.getElementById("body").style.overflow = "auto";

  }


  register(upcoming) {

    this.googleAnalyticsEventsService.emitEvent("Ongoing Events", "RegisterInitiated", upcoming.eventName);

    console.log("Inside register");

    this.eventId = upcoming.eventId;
    this.Eventname = upcoming.eventName;
    console.log("Event Id Inside register", this.eventId);
    console.log("this.registerEventId", this.registeredEventId);

    this.registrationForm = this.formBuilder.group({

      userName: ["", [Validators.required]],
      mobileNumber: ["", [Validators.required, Validators.pattern('[0-9]{10}')]],
      address: ["", [Validators.required]]

    });
  }

  Registration() {

    this.googleAnalyticsEventsService.emitEvent("Ongoing Events", "Register", this.eventName);

    console.log("Event Id", this.eventId);
    var obj = {

      "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
      "programId": this.configService.getprogramInfo().programId,
      "clientId": this.configService.getprogramInfo().clientId,
      "programUserId": this.programUserInfo.programUserId,
      "eventId": this.eventId,
      "eventName": this.Eventname,
      "participantName": this.registrationForm.value.userName,
      "mobileNumber": this.registrationForm.value.mobileNumber,
      "address": this.registrationForm.value.address,
      "serviceType": "programSetup"
    }

    console.log("Request object", obj);

    this.showLoader = true;
    this.eventsService.addEventRegistration(obj).subscribe(
      responseObject => {
        this.showLoader = false;

        let responseCodes = this.configService.getStatusTokens();
        console.log("Response Codes", responseCodes);
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:

            console.log(responseObject.result);
            const dom3: any = document.getElementById('closePopup').click();

            this.feedbackMessageComponent.updateMessage(true, "Successfully registered to the event", "alert-success");

            this.getEventRegistration();
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            //this.feedbackMessageComponent.updateMessage(true, "Your are already Registered", "alert-danger");
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => {
        this.showLoader = false;
      }
    );


  }
  showcarddiv() {
    this.googleAnalyticsEventsService.emitEvent("Ongoing Events", "Back");
    this.carddiv = true;
    this.seedetailsdiv = false;
    this.getEventRegistration();
  }



}

