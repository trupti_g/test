

import { Component, Input } from "@angular/core";

@Component({
    selector: 'feedback-message',
    template: `
        <div *ngIf="feedbackMessageObj.show" class="alert {{feedbackMessageObj.msgClass}} alert-dismissable feedback-message">
            <a class="close" data-dismiss="alert" aria-label="close" (click)="updateMessage(false, '', '','')">×</a>
            <div [innerHTML]="feedbackCustomMsg">
            </div>
       </div>
    `
})
export class FeedbackMessageComponent {

    private feedbackMessageObj: any;
    private feedbackSuccessMsg: string = "";
    private feedbackCustomMsg: string = "";
    private isViewMsg: boolean = false;
    private isShowMsg: boolean = false;
    constructor() {
        this.feedbackMessageObj = {
            "show": false,
            "msgText": "",
            "msgClass": ""
        };
    }

    /**
      * METHOD   : updateRoleFormMessageObject
      * DESC     : updates the bootstrap styled form message
      */
    updateMessage(showFlag: boolean, msgText: string, msgClass: string, customText?: any) {
        console.log("IN FEEDBACK");
        if (msgClass === "alert-danger") {
            this.isShowMsg = true;
            this.feedbackSuccessMsg = customText;
            this.feedbackCustomMsg = JSON.stringify(msgText, null, 2);
        } else {
            this.isShowMsg = false;
            this.feedbackSuccessMsg = msgText;
            this.feedbackCustomMsg = customText;
        }
        if (msgClass === "alert-success") {
            this.isShowMsg = true;
            this.feedbackSuccessMsg = customText;
            this.feedbackCustomMsg = msgText;
        } else {
        }
        this.feedbackMessageObj = {
            "show": showFlag,
            "msgText": msgText,
            "msgClass": msgClass
        };
    }
}