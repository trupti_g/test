import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FeedbackMessageComponent } from "./feedback-message.component";

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, NgbModule],
    declarations: [
        FeedbackMessageComponent
    ],
    exports: [
        FeedbackMessageComponent
    ],
})
export class SharedModule {

}