import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { HomeService } from "./home.service";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service"
import { RewardsGalleryService } from "../rewards-gallery/rewards-gallery.service"
import { CartService } from "../shared-services/cart.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: "./home-placeorder.component.html",
    styleUrls: ['./home.component2.css'],
    providers: [Device]
})

export class HomePlaceorderComponent implements OnInit {
    public programArr: any = [];
    public statesArray: any = [];
    public citiesArray: string[] = [];
    public subPoint: number = 0;
    public reedemPoints: number = 0;
    public shippingAddressForm: FormGroup;
    public currentShippingAddressForm: FormGroup;
    public radioSelectForm: FormGroup;
    public showLoader: Boolean = false;
    public requestObj: any = {};
    public userinfo: any;
    public userName: any;
    public programUserInfo: any;
    public userDetails: any;
    public isCurrentAddress: boolean;
    public isNewAddress: boolean;
    private isQuantityReadOnly: boolean = false;
    public cartArray: any = [];
    public math: Math;
    private isAllowedToChangeQuantity: boolean = true;
    public programUser: any = {};
    public userInfo: any = {};
    public programInfo: any = {};
    public isProgramPointsToRedeem: boolean;
    public clientColor: any;
    public hasPointsToRedeem: boolean = false;
    public isShippingInfoFilled = false;
    public isPlacedOrderClicked = false;
    private redemptionPeriodStartDate: any;
    private redemptionPeriodEndDate: any;
    mobilewidth: any;                        //added for screen width
    displaypc: boolean = true;                //added for screen width
    displaymobile: boolean = false;           //added for screen width
    private alternateMobileNo: any;
    isAlternate: boolean = false;

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        public cartService: CartService,
        private stringService: StringService,
        private homeService: HomeService,
        private productDetailsService: ProductDetailsService,
        private formBuilder: FormBuilder,
        private globalRewardGalleryService: GlobalRewardGalleryService,
        private rewardsGalleryService: RewardsGalleryService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {

        // Code for screen width
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

        this.programUser = this.configService.getloggedInProgramUser();
        this.userInfo = this.configService.getLoggedInUserInfo();
        this.showLoader = true;
        this.cartService.getCartFormReqObj(null);
        this.cartArray = cartService.cartArray;
        this.isCurrentAddress = true;
        this.isNewAddress = false;
        this.clientColor = this.configService.getThemeColor();
        this.programInfo = this.configService.getloggedInProgramInfo();

        console.log("********cartArray**********", this.cartService.cartArray);
        this.calPoints();
        for (let i = 0; i < this.cartService.cartArray.length; i++) {
            this.subPoint = this.subPoint + (this.cartService.cartArray[i].productInfo.pointsPerQuantity * this.cartService.cartArray[i].quantity);
        }

        this.radioSelectForm = this.formBuilder.group({
            "shippingAddress": ["", [Validators.required]],
        })

        this.shippingAddressForm = this.formBuilder.group({
            "streetAddress": ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
            "pincode": ["", [Validators.required, Validators.pattern('[0-9]{6}')]],
            "city": [{ value: "" }, [Validators.required]],
            "state": [{ value: "" }, [Validators.required]],
            "mobileNumber": ["", [Validators.required, Validators.pattern('[0-9]{10}')]]
        })

        this.currentShippingAddressForm = this.formBuilder.group({
            "currentStreetAddress": [""],
            "currentPincode": [""],
            "currentCity": [{ value: "" }],
            "currentState": [{ value: "" }],
            "currentMobileNumber": [""],
            "outletName": [""],
            "userName": [""],
            "alternateMobileNumber": [""],
            "landmark": [""]
        })

        this.userDetails = this.configService.getloggedInProgramUser().userDetails;
        if (this.configService.getLoggedInUserInfo().alternateMobileNumber) {
            this.isAlternate = true;
            this.alternateMobileNo = this.configService.getLoggedInUserInfo().alternateMobileNumber[0];
        }

        this.currentShippingAddressForm.patchValue({
            "currentStreetAddress": this.userDetails.streetAddress,
            "currentPincode": this.userDetails.pincode,
            "currentCity": this.userDetails.city,
            "currentState": this.userDetails.state,
            "currentMobileNumber": this.userDetails.mobileNumber,
            "outletName": this.configService.getloggedInBEInfo().businessName,
            "userName": this.userInfo.fullName,
            "alternateMobileNumber": this.alternateMobileNo,
            "landmark": this.configService.getLoggedInUserInfo().landmark
        });

        this.getStates();

        this.cartService.stopLoader.subscribe((value) => {
            this.showLoader = false;
            this.isQuantityReadOnly = false;

            this.subPoint = 0;
            for (let i = 0; i < this.cartService.cartArray.length; i++) {
                this.subPoint = this.subPoint + (this.cartService.cartArray[i].productInfo.pointsPerQuantity * this.cartService.cartArray[i].quantity);
            }
            this.calPoints();
        });
        this.redemptionPeriodStartDate = new Date(this.programInfo.redemptionPeriodStartDate);
        this.redemptionPeriodEndDate = new Date(this.programInfo.redemptionPeriodEndDate);
    }

    ngOnInit() {
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('PlaceOrder')
		}
		else{
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "PlaceOrder");
					ga('send', 'pageview');
				}
			});
		}
        this.onCurrentAddressClick();
        if (this.programInfo.allowStarPointsConversion === true && this.cartService.RGType === "GlobalRG") {
            this.isProgramPointsToRedeem = false;
        } else {
            this.isProgramPointsToRedeem = true;
        }
        if (this.isProgramPointsToRedeem === true) {
            let reqObj: any = {};
            reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
            reqObj.programId = this.configService.getloggedInProgramInfo().programId;
            reqObj.programUserId = this.configService.getloggedInProgramUser().programUserId;
            reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
            console.log("reqObjreqObj", reqObj);
            this.getProgramUser(reqObj);
        } else {
            if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") this.getBEInfoFormReqObj();
            else if (this.configService.getLoggedInUserInfo().userType === "ClientUser") this.getClientUser();
        }
    }
    calPoints() {
        this.subPoint = 0;
        for (let i = 0; i < this.cartService.cartArray.length; i++) {
            this.subPoint = this.subPoint + (this.cartService.cartArray[i].productInfo.pointsPerQuantity * this.cartService.cartArray[i].quantity);
        }

        let loggedInUserInfo: any = {};
        loggedInUserInfo = this.configService.getLoggedInUserInfo();

        console.log("this.programInfo.allowStarPointsConversion >>> ", this.programInfo.allowStarPointsConversion, "this.cartService.RGType >>> ", this.cartService.RGType)
        if (this.programInfo.allowStarPointsConversion === true && this.cartService.RGType === "GlobalRG") {
            console.log("both galleries v n now global gallery", this.programInfo.allowStarPointsConversion + ' ' + this.cartService.RGType);
            if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") this.reedemPoints = this.configService.getloggedInBEInfo().availableStarPoints;
            else if (this.configService.getloggedInProgramUser().userType === "ClientUser") this.reedemPoints = this.configService.getClientUserInfo().availableStarPoints;
            this.isProgramPointsToRedeem = false;
        }
        else {
            console.log("single galleries v n now program gallery", this.programInfo.allowStarPointsConversion + ' ' + this.cartService.RGType);
            this.reedemPoints = 0;
            console.log("this.configService.getloggedInProgramUser().totalPointsAvailableForRedeem", this.configService.getloggedInProgramUser().totalPointsAvailableForRedeem);
            this.reedemPoints = this.configService.getloggedInProgramUser().totalPointsAvailableForRedeem;
            this.isProgramPointsToRedeem = true;
        }

        console.log(">>>>>> Star Points : ", this.reedemPoints, "this.subPoint : ", this.subPoint);

        if (this.reedemPoints >= this.subPoint) {
            this.hasPointsToRedeem = true;
        } else {
            this.hasPointsToRedeem = false;
        }
    }
    getQuantityList(number) {
        let array: any = [];
        for (let i = 1; i <= number; i++) {
            array.push(number);
        }
        return array;

    }


    resetForms() {
        this.shippingAddressForm.reset();
        this.currentShippingAddressForm.reset();
        this.radioSelectForm.reset();
    }

    onCurrentAddressClick() {
        this.isCurrentAddress = true;
        this.isNewAddress = false;
    }

    onNewAddressClick() {
        this.isCurrentAddress = false;
        this.isNewAddress = true;
    }

    /**********************************     API CALLS    **********************************/
    /*
        METHOD         : getStates()
        DESCRIPTION    : To get an array of all states
    */
    getStates() {
        this.showLoader = true;

        let states: string[] = [];
        this.configService.getStates().subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        this.statesArray[i] = responseObject.result[i].stateName;
                    }
                }
                else {
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    }

    /*
        METHOD         : getStates()
        DESCRIPTION    : To get an array of all cities what belong to the selected state
    */
    getCities(state) {
        this.showLoader = true;
        this.configService.getCities(state).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        this.citiesArray[i] = responseObject.result[i].cityName;
                    }
                }
                else {
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    }

    getProgramUser(requestObject) {

        console.log("inside getProgramUser");
        this.showLoader = true;
        this.configService.getProgramUser(requestObject).subscribe((responseObject) => {
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();
            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;
                case responseCodes.RESP_SERVER_ERROR:
                    break;
                case responseCodes.RESP_SUCCESS:
                    this.configService.setloggedInProgramUser(responseObject.result[0]);
                    this.configService.setProgramUserInfo(responseObject.result[0]);
                    this.programUserInfo = responseObject.result[0];
                    break;
                case responseCodes.RESP_AUTH_FAIL:
                    break;
                case responseCodes.RESP_FAIL:
                    break;
                case responseCodes.RESP_ALREADY_EXIST:
                    break;
            }
        }, err => {
            this.showLoader = false;
        });
    }
    /********************************   END OF API CALLS     ******************************/


    /*********************************** 	CART FUNCTIONALITY 	 *****************************/

    onRemoveFromCart(productId, productName) {
        this.googleAnalyticsEventsService.emitEvent("Cart", "Remove from cart", productName);
        this.showLoader = true;
        this.calPoints();
        this.cartService.updateToRemoveCartFormReqObj(productId, "Removed");
    }



    onQuantityClick(product, quantity, index) {
        console.log(quantity, index);
        if (quantity <= 0) {
            this.cartService.cartArray[index].quantity = 1;
            quantity = 1;
        }
        if (quantity > product.productInfo.stock) {
            product.quantity = product.productInfo.stock;
            quantity = product.productInfo.stock;
        }
        this.showLoader = true;
        this.isQuantityReadOnly = true;
        this.calPoints();
        this.cartService.updateToCartFormReqObj(product.productId, "Pending", quantity);

    }
    /***************************** 	END OF CART FUNCTIONALITY 	 ******************************/


    onPlaceOrderClick() {
        this.showLoader = true;
        if (new Date() >= this.redemptionPeriodStartDate && new Date() <= this.redemptionPeriodEndDate) {
            this.isPlacedOrderClicked = true;
            console.log("hasPointsToRedeem isCurrentAddress", this.hasPointsToRedeem + ' ' + this.isCurrentAddress)
            this.requestObj = {};
            let shippingAdd: any = {};
            let productObj: any = {};
            let productDetails: any = [];
            let loggedInUserInfo: any = {};

            console.log("this.isPlacedOrderClicked", this.isPlacedOrderClicked);
            loggedInUserInfo = this.configService.getLoggedInUserInfo();

            if (this.programInfo.allowStarPointsConversion === true && this.cartService.RGType === "GlobalRG") {
                if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") this.reedemPoints = this.configService.getloggedInBEInfo().availableStarPoints;
                else if (this.configService.getloggedInProgramUser().userType === "ClientUser") this.reedemPoints = this.configService.getClientUserInfo().availableStarPoints;
                this.isProgramPointsToRedeem = false;
            }
            else {
                this.reedemPoints = this.configService.getloggedInProgramUser().totalPointsAvailableForRedeem;
                this.isProgramPointsToRedeem = true;
            }

            console.log(">>>>>  this.cartService.RGType : ", this.cartService.RGType, "this.programInfo.allowStarPointsConversion : ", this.programInfo.allowStarPointsConversion, "this.isProgramPointsToRedeem : ", this.isProgramPointsToRedeem);
            if (this.reedemPoints >= this.subPoint) {
            } else {
                this.feedbackMessageComponent.updateMessage(true, "points exceed than total reedem points ", "alert-failed");
            }
            for (let i = 0; i < this.cartService.cartArray.length; i++) {
                productObj.productId = this.cartService.cartArray[i].productId;
                productObj.productName = this.cartService.cartArray[i].productName;
                productObj.productQuantity = this.cartService.cartArray[i].quantity;
                productObj.pointsPerQuantity = this.cartService.cartArray[i].productInfo.pointsPerQuantity;
                this.googleAnalyticsEventsService.emitEvent("Reward Gallery", "Place order", productObj.productName);
                productDetails.push(productObj);
                productObj = {};
            }


            if (this.isNewAddress) {
                shippingAdd.state = this.shippingAddressForm.value.state;
                shippingAdd.city = this.shippingAddressForm.value.city;
                shippingAdd.streetAddress = this.shippingAddressForm.value.streetAddress;
                shippingAdd.pincode = this.shippingAddressForm.value.pincode;
                shippingAdd.mobileNumber = this.shippingAddressForm.value.mobileNumber;
            } else if (this.isCurrentAddress) {
                shippingAdd.state = this.currentShippingAddressForm.value.currentState;
                shippingAdd.city = this.currentShippingAddressForm.value.currentCity;
                shippingAdd.streetAddress = this.currentShippingAddressForm.value.currentStreetAddress;
                shippingAdd.pincode = this.currentShippingAddressForm.value.currentPincode;
                shippingAdd.mobileNumber = this.currentShippingAddressForm.value.currentMobileNumber;
            }

            this.requestObj.clientId = this.configService.getloggedInProgramInfo().clientId;
            this.requestObj.programId = this.configService.getloggedInProgramInfo().programId;
            this.requestObj.customerId = this.configService.getloggedInProgramUser().programUserId;
            this.requestObj.customerName = this.userDetails.userName;

            if (loggedInUserInfo.clientId) {
                this.requestObj.entityId = this.configService.getloggedInProgramInfo().clientId;
                this.requestObj.entityName = this.configService.getloggedInProgramInfo().clientName;
            } else if (loggedInUserInfo.userId) {
                this.requestObj.entityId = this.configService.getloggedInBEInfo().businesId;
                this.requestObj.entityName = this.configService.getloggedInBEInfo().businessName;
            }

            if (this.configService.getLoggedInUserInfo() !== null && this.configService.getLoggedInUserInfo() !== undefined) this.requestObj.userId = this.configService.getLoggedInUserInfo().userId;
            if (this.configService.getLoggedInUserInfo() !== null && this.configService.getLoggedInUserInfo() !== undefined) this.requestObj.userType = this.configService.getLoggedInUserInfo().userType;
            if (this.configService.getLoggedInUserInfo() !== null && this.configService.getLoggedInUserInfo() !== undefined) this.requestObj.userName = this.configService.getLoggedInUserInfo().fullName;
            this.requestObj.programWisePoints = 2505.2206;
            this.requestObj.orderStatus = "Pending";
            this.requestObj.totalPoints = this.subPoint;
            this.requestObj.shippingAddress = shippingAdd;
            this.requestObj.productDetails = productDetails;
            console.log("this.reedemPoints", this.reedemPoints, "this.subPoint", this.subPoint);
            if (!((this.isNewAddress && this.shippingAddressForm.valid) || this.isCurrentAddress)) {
                this.feedbackMessageComponent.updateMessage(true, "Please fill the shipping address correctly", "alert-danger");
            }
            else if (this.cartService.cartArray.length <= 0) {
                this.feedbackMessageComponent.updateMessage(true, "Please add product to place order", "alert-danger");
            }
            else if (this.reedemPoints < this.subPoint) {
                this.feedbackMessageComponent.updateMessage(true, "You don't have sufficient points to redeem", "alert-danger");
            }
            else {
                this.addOrder(this.requestObj);
            }
        }
        else {
            this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't place order outside of Redemption Period.", "alert-danger", "");
        }
    }

    addOrder(requestObject) {
        this.isPlacedOrderClicked = true;
        this.showLoader = true;
        requestObject.RGType = this.cartService.RGType;
        console.log("\n RGType " + this.cartService.RGType);
        console.log("Request Object in AddOrder : ", requestObject);
        this.homeService.addOrder(requestObject, this.isProgramPointsToRedeem)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.programArr = [];
                        this.programArr = responseObject.result;
                        let programUserInfo = this.configService.getloggedInProgramUser();
                        programUserInfo.totalPointsAvailableForRedeem = this.reedemPoints;
                        this.cartService.clearCart();

                        console.log("programUserInfo.totalPointsAvailableForRedeem : ", programUserInfo.totalPointsAvailableForRedeem);
                        this.configService.setloggedInProgramUser(programUserInfo);

                        let reqObj: any = {};
                        reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
                        reqObj.programId = this.configService.getloggedInProgramInfo().programId;
                        reqObj.programUserId = this.configService.getloggedInProgramUser().programUserId;
                        reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
                        console.log("requestObject", requestObject);
                        if (requestObject.isProgramPointsToRedeem === true) this.getProgramUser(reqObj);
                        else if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") this.getBEInfoFormReqObj();
                        else if (this.configService.getLoggedInUserInfo().userType === "ClientUser") this.getClientUser();
                        this.resetForms();
                        this.feedbackMessageComponent.updateMessage(true, "Order Placed Successfully", "alert-success");
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_BLOCKED:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                }
            },
                err => {
                    this.showLoader = false;

                }
            );

    }

    getBEInfoFormReqObj() {
        let getBEInfoReqObj = {
            "businessId": this.configService.getloggedInBEInfo().businessId,
            "programId": this.programInfo.programId,
            "kycStatus": true,
            "frontendUserInfo": this.configService.getFrontEndUserInfo()
        }
        this.getBEInfo(getBEInfoReqObj);
    }
    // for navigation mobile
    cancelNav() {
        console.log("filter");
        this.router.navigate(['../home/rewrdsgallerymob']);

    }

    getBEInfo(getBEInfoReqObj) {
        console.log("inside getBEInfo");
        this.configService.getBEInfoAPI(getBEInfoReqObj).subscribe(
            (responseObject) => {
                console.log("inside of brinfo responce");
                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_SUCCESS:
                        this.configService.setloggedInBEInfo(responseObject.result);

                        break;
                    case responseCodes.RESP_FAIL:
                        console.log("no BE found");
                        break;
                    case responseCodes.RESP_ROLLBACK_ERROR:
                    case responseCodes.RESP_SERVER_ERROR:
                    case responseCodes.RESP_AUTH_FAIL:
                    case responseCodes.RESP_ALREADY_EXIST:
                    case responseCodes.RESP_KYC_AWAITING:
                    case responseCodes.RESP_BLOCKED:
                }
            }, err => {
                console.log("error");
            });
    }

    // api call to get client user info
    getClientUser() {

        this.homeService.getClientUser({}).subscribe(
            (responseObject) => {
                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_SUCCESS:
                        this.configService.setClientUserInfo(responseObject.result);
                        console.log("client User", responseObject.result);
                        break;
                    case responseCodes.RESP_FAIL:

                        break;
                    case responseCodes.RESP_ROLLBACK_ERROR:
                    case responseCodes.RESP_SERVER_ERROR:
                    case responseCodes.RESP_AUTH_FAIL:
                    case responseCodes.RESP_ALREADY_EXIST:
                    case responseCodes.RESP_KYC_AWAITING:
                    case responseCodes.RESP_BLOCKED:

                }
            },
            err => {

            }
        );
    }

    nav() {
        this.router.navigate(['../home/rewardsgallery']);
    }

}