/*
	Author			:	Deepak Terse
	Description		: 	Setting home module for all the files required for home page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { CommonModule } from "@angular/common";
import { HomeRoutingModule } from "./home.routing.module";
import { UserModule } from "../user/user.module";
import { ClaimsModule } from "../claims/claims.module";
import { DashboardModule } from "../dashboard/dashboard.module";
import { RedemptionsModule } from "../redemptions/redemptions.module";
import { LeadManagementModule } from "../lead-management-system/lead-management.module";
import { EventModule } from "../events/events.module";
import { CelebrationModule } from "../celebrations/celebrations.module";
import { ChannelsModule } from "../channels/channels.module";


import { RewardsGalleryModule } from "../rewards-gallery/rewards-gallery.module";
import { GlobalRewardsGalleryModule } from "../global-rewards-gallery/rewards-gallery.module";
import { SpecialOffersModule } from "../special-offers/special-offers.module";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "@angular/material";
import { FooterModule } from "../footer/footer.module";
import { HomeService } from "./home.service";
import { SharedModule } from "../shared-components/shared.module";
import { SpecialOffersService } from "../special-offers/special-offers.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { HeaderComponent } from "./header/header.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";

import { PrivilegesComponent } from "./privileges.component";

import { LoyaltyComponent } from "./loyalty.component";


import { ReportComponent } from "./report.component";

import { NotificationsComponent } from "./notifications.component";

import { BannerComponent } from "./banner/banner.component";

import { LeaderboardUploadComponent } from "./leaderboardDataUpload.component";

import { HallOfFameUploadComponent } from "./hallOfFameDataUpload.component";
import { HomePlaceorderComponent } from "./home-placeorder.component";
// import { ProgramSelection } from "../login/programselection.component"

// import { FeedbackComponent } from "../feedback/feedback.component";
import { FeedbackModule } from "../feedback/feedback.module";
import { AskAnExpertmodule } from "../ask-an-expert/ask-an-expert.module";



@NgModule({
  declarations: [HomePlaceorderComponent, HomeComponent, HeaderComponent, SidebarComponent, PrivilegesComponent, ReportComponent,
    NotificationsComponent, BannerComponent, LeaderboardUploadComponent, HallOfFameUploadComponent, LoyaltyComponent],


  imports: [
    CommonModule,
    HomeRoutingModule,
    UserModule,
    MaterialModule,
    ClaimsModule,
    DashboardModule,
    RedemptionsModule,
    RewardsGalleryModule,
    GlobalRewardsGalleryModule,
    SpecialOffersModule,
    ReactiveFormsModule,
    FormsModule,
    FooterModule,
    SharedModule,
    LeadManagementModule,
    EventModule,
    CelebrationModule,
    ChannelsModule,
    NgbDropdownModule.forRoot(),
    FeedbackModule,
    AskAnExpertmodule
  ],
  exports: [],

  providers: [
    HomeService,
    GlobalRewardGalleryService,
    SidebarComponent,
    BannerComponent,
    HeaderComponent
  ],

  entryComponents: []
})
export class HomeModule { }
