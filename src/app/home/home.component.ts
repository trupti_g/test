/*
	Author			:	Deepak Terse
	Description		: 	UI  which will be available in all the pages except login and signup pages
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ConfigService } from "../shared-services/config.service";
import { CartService } from "../shared-services/cart.service";
import { AppService } from "../app.service";

import { StringService } from "../shared-services/strings.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service"
import { RewardsGalleryService } from "../rewards-gallery/rewards-gallery.service"
import { LoginService } from "../login/login.service";
import { HomeService } from "./home.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { CsvService } from "../shared-services/csv.service";
import { LeadManagementService } from "../lead-management-system/lead-management.service";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { ProgramSelection } from "../login/programselection.component"




@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit {
	leadslist: any;
	birthdayCount: any;
	isShowLeadManagementHover: boolean;
	rows: any;
	programloginArr: any[];
	tempArrayForParsing: any[];
	tempRolesSetArray: any[];
	tempRoleArray: any[];
	tempCityArray: any[];
	tempStateArray: any[];
	printingArray: any[];
	headerArray: string[];
	programparticipantsArr: any[];
	public showBirthdayPopUp: boolean = false;
	loginHover: boolean;
	dealerHover: boolean;
	distributorHover: boolean;
	leadHover: boolean;
	clientsArr: any[];
	private roverElementColor: string;
	private isChannelsFound: boolean = false;
	private userInfo: any;
	private showChannels: boolean = false;
	private programUserInfo: any;
	private loggedInProgramInfo: any;
	private clientBrandColor: any;
	private userPoints: string;
	private programRoleInfo: any;
	private showClaims: boolean = false;
	private showPoints: boolean = false;
	private showRedemption: boolean = false;
	private isCartClick: boolean = false;
	private isCartOutsideClick: boolean = false;
	private isButtonDisable: boolean;
	public parseInteger: any;
	isIn: boolean = false;
	public isReedemPoints: boolean = false;
	public productArr: any = [];
	private addClaimRoute: string;
	public programLogo: string;
	public cartCount: number = 0;
	public subPoint: number = 0;
	public reedemPoints: number = 0;

	public parseMethod: any;
	public localStorageVar: any;
	public pointsAvailableToRedeem: number;
	public isEligibleForRedemption: boolean;
	public math: any;
	public isEligibleForPointConversion: boolean;
	public programInfo: any;
	private cart: any;
	public currentlyDeleted: any;
	public rewardGalleryRoute: any;
	private globalProductList: any;
	private localProductList: any;
	private showLoader: boolean = false;
	private loginInfo: any;
	private businessInfo: any = {};
	private getmyChannelsReqObj: any = {};
	private clientId: any;
	private options;
	private headers;
	public cartArray: any = [];
	public clientColor: any;


	public isAddClaimHover: boolean;
	public isShowClaimHover: boolean;
	public isOfferHover: boolean;
	public isContestHover: boolean;


	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private http: Http,
		private route: ActivatedRoute,
		private router: Router,
		private loginService: LoginService,
		private configService: ConfigService,
		public cartService: CartService,
		public appService: AppService,
		private stringService: StringService,
		private productDetailsService: ProductDetailsService,
		private homeService: HomeService,
		private globalRewardGalleryService: GlobalRewardGalleryService,
		private rewardsGalleryService: RewardsGalleryService,
		private _csvService: CsvService,
		private leadService: LeadManagementService,
		private sidebarcomponent: SidebarComponent

	) {

		console.log("home subscribed called constructor");

		this.programInfo = this.configService.getloggedInProgramInfo();

		let RGType = this.programInfo.useGlobalRewardGallery ? "GlobalRG" : "ProgramSpecificRG";

		this.cartService.RGType = RGType;
		this.cartService.setFields(RGType);
		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
		});
		this.clientId = this.configService.getloggedInProgramInfo().clientId;
		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.configService
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		if (this.configService.getloggedInProgramInfo()) {
			let loggedInProgramInfo = this.configService.getloggedInProgramInfo();
			console.log("&&&&&&&&&&&&& TERMINOLOGY USED IN PROGRAM &&&&&&&&&&&&&&&&&", loggedInProgramInfo.terminologyToUseInProgram);
			if (loggedInProgramInfo.terminologyToUseInProgram == "point") {
				console.log("HOME COMPONENT POINTS SECTION");
				let s: string = "Points";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			if (loggedInProgramInfo.terminologyToUseInProgram == "coupon") {
				console.log("HOME COMPONENT COUPONS SECTION");
				let s: string = "Coupons";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			if (loggedInProgramInfo.terminologyToUseInProgram == "voucher") {
				let s: string = "Voucher";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			this.cartService.getCartFormReqObj(null)

		}




		this.configService.setRewardGalleryFlag(false);
		this.isButtonDisable = false;
		this.math = Math;
		this.parseInteger = parseInt;
		this.isEligibleForRedemption = false;
		this.isEligibleForPointConversion = false;

		this.productArr = JSON.parse(localStorage.getItem("productDetails"));
		console.log(this.productArr);
		this.parseMethod = JSON.parse;
		this.localStorageVar = localStorage;
		console.log("Product Array of local storage", this.productArr);
		this.cartCount = this.productDetailsService.getCartCount();
		this.userInfo = configService.getLoggedInUserInfo();

		for (let i = 0; i < this.productArr.length; i++) {
			this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
		}

		// if (this.userInfo.clientId) {
		// 	ga('set', 'userId', this.userInfo.email);
		// }


		// Set the user ID using signed-in user_id.
		this.programUserInfo = configService.getloggedInProgramUser();



		// [Comment - production]
		this.pointsAvailableToRedeem = this.programUserInfo.totalPointsAvailableForRedeem;
		// [End Comment - production]

		//[UnComment - production]
		// this.pointsAvailableToRedeem = this.programUserInfo.pointsAvialableToRedeem;
		//[End UnComment - production]


		let programUser = this.programUserInfo;
		if (this.programUserInfo) {


			this.programRoleInfo = programUser.programRoleInfo;
			console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
			if (this.programRoleInfo) {

				if (!this.programRoleInfo.eligibleForRedemption) {
					this.isEligibleForRedemption = true;
				}

				if (this.programRoleInfo.canMakeClaims == true) {
					this.showClaims = true;
				} else {
					this.showClaims = false;
				}

				if (this.programRoleInfo.canEarnPoints == true) {
					this.showPoints = true;
				} else {
					this.showPoints = false;
				}
				if (this.programRoleInfo.eligibleForRedemption == true) {
					this.showRedemption = true;
				} else {
					this.showRedemption = false;
				}


			}
		} else {
			this.showClaims = true;
			this.showPoints = true;
			this.showRedemption = true;
		}


		console.log('ROle Name', this.programUserInfo.roleName);
		if (this.programUserInfo.roleName === "Retailer") {

			this.showChannels = false;
		} else {
			this.showChannels = true;
		}


		this.programInfo = this.configService.getloggedInProgramInfo();
		this.programLogo = this.programInfo.programLogo[0];
		console.log("PROGRAM INFO CONSTRUCTOR [HOME COMPONENT] : ", this.programInfo);

		console.log("*********************************************", this.programUserInfo);


		// END OF CHECK

		this.cartService.stopLoader.subscribe((value) => {
			console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@", this.cartService.cartArray);
			console.log(value);
		})


	}


	ngOnInit() {
		console.log("showBirthdayPopUp", this.showBirthdayPopUp);
		setTimeout(() => {
			console.log("timeOUTTT");
			this.showBirthdayPopUp = true;
		}, 10000);
		console.log("showBirthdayPopUp", this.showBirthdayPopUp);
		console.log("home subscribed called oninit");
		// this.addLoginLog();
		console.log("**********SUBJECT CALLED********")
		this.cartService.cartItemsUpdated.subscribe((value) => {
			this.cartArray = value;
			console.log("**********SUBJECT CALLED********", value)

		})
		this.configService.setLoginFlag(true);
		this.setAddClaimRoutes();
		this.setPoints();
		console.log("user", this.configService.getFrontEndUserInfo());
		this.appService.frontEndInfo.subscribe((value) => {
			var reqObj = {
				"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
				"clientId": this.configService.getloggedInProgramInfo().clientId
			}
			console.log("calling getClient");
			this.getAllClients(reqObj);
		})
		console.log("calling getCart");
		this.syncCart();
		console.log("cart count", this.productDetailsService.getCartCount());
		this.cartCount = this.productDetailsService.getCartCount();
		let type = this.programInfo.useGlobalRewardGallery ? "GlobalRG" : "ProgramSpecificRG";
		this.loginInfo = this.configService.getLoggedInUserInfo();
		this.businessInfo = this.configService.getloggedInBEInfo();

		this.cartArray = this.cartService.cartArray;
		console.log("cartService.cartArray", this.cartService.cartArray);


		var abc = document.getElementById("sidebar");
		console.log("abcvcvasvcydgsvcygvsycvydgvhy", abc);

		if (this.programInfo.useGlobalRewardGallery) this.rewardGalleryRoute = "./globalrewardsgallery";
		else this.rewardGalleryRoute = "./rewardsgallery";

		// CHECK FOR ELIGIBILITY FOR POINT CONVERSION
		if (this.programInfo.useGlobalRewardGallery) {
			this.isEligibleForPointConversion = false;
		} else {
			if (this.programInfo.allowStarPointsConversion) {
				this.isEligibleForPointConversion = true;
			} else {
				this.isEligibleForPointConversion = false;
			}
		}
		console.log("useGlobalRewardGallery : ", this.programInfo.useGlobalRewardGallery);
		console.log("allowStarPointsConversion : ", this.programInfo.allowStarPointsConversion);
		console.log("isEligibleForPointConversion : ", this.isEligibleForPointConversion);



	}


	//****************************************  CART FUNCTIONALITY 	 ***************************************/



	syncCart() {
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		console.log("requestObject to cart", reqObj);
	}




	updateCartQuantity(productArray) {
		let cart: any = [];
		console.log("original cart", cart);
		console.log("product Array Length in cart Update", productArray.length);
		for (let i = 0; i < productArray.length; i++) {
			if (productArray[i].quantity == 0) {
				productArray[i].quantity = 1;
				continue;
			}
			let found: boolean = false;
			for (let j = 0; j < cart.length; j++) {
				console.log("for updatation check ", cart[j].productId,
					cart[j].productId, productArray[i].productId
					, productArray[i].type, cart[j].type,
					"isPlaced", (!cart[j].isPlaced || cart[j].isPlaced == false)
					, (cart[j].programId == this.programInfo.programId && cart[j].productId == productArray[i].productId && productArray[i].type == cart[j].type
						&& (!cart[j].isPlaced || cart[j].isPlaced == false)));
				if (cart[j].programId == this.programInfo.programId && cart[j].productId == productArray[i].productId &&
					productArray[i].type == cart[j].type && (!cart[j].isPlaced || cart[j].isPlaced == false)) {
					found = true;
					cart[j].quantity = productArray[i].quantity;
				}
			}
			if (!found) {
				let cartObject: any = {};
				cartObject.programId = this.programInfo.programId;
				cartObject.productId = productArray[i].productId;
				cartObject.quantity = productArray[i].quantity;
				if (productArray[i].type == "globalRewardGallery") {
					cartObject.type = "globalRewardGallery";
				} else if (productArray[i].type == "programSpecificRewardGallery") {
					cartObject.type = "programSpecificRewardGallery";
				}
				cartObject.isPlaced = false;
				console.log("ADDING NEW ELEMENT IN CART DATABSE ", cartObject);
				cart.push(cartObject);
			}
		}
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.updateInfo = {};
		reqObj.updateInfo.productList = cart;
		console.log("passing this object to upate the cart on quantity click", reqObj);
		this.updateCartProductList(reqObj);
	}
	updateCartOnClose(productArray) {
		let cart: any = [];
		console.log("product closed", this.currentlyDeleted[0]);
		console.log("product Array Length in cart Update", productArray.length);
		console.log("get cart", cart);
		for (let i = 0; i < cart.length; i++) {
			let found: boolean = false;

			if (!found) {
				if (this.currentlyDeleted[0]) {
					if (cart[i].programId == this.programInfo.programId &&
						cart[i].productId == this.currentlyDeleted[0].productId && cart[i].type == this.currentlyDeleted[0].type
						&& (!cart[i].isPlaced)) {
						console.log("removing product from cart", cart[i]);
						let popped = cart.pop(i);
						console.log("wanted to delete", JSON.stringify(this.currentlyDeleted))
						console.log("product popped on delete", JSON.stringify(popped));
					}
				}
			}
		}
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.updateInfo = {};
		reqObj.updateInfo.productList = cart;
		console.log("passing this object to upate the cart on quantity click", reqObj);
		this.updateCartProductList(reqObj);

	}
	/**
	 * 
	 * @param requestObject
	 * @author sumant.k@sankeysolutions.com
	 * this function is used to get user Cart. it takes parameter : userId  
	 * it sets localstorage cart to this.cart
	 */
	getCartProductList(requestObject) {
		console.log("in get cart Items");

		if (!requestObject.userId) {
			console.log("Passing userInfo to access Cart inside", this.userInfo);
			requestObject.userId = this.userInfo.clientId;
		}

	}
	/**
	* 
	* 
	* 
	* @param requestObject
	* @author sumant.k@sankeysolutions.com
	* this function is used to update or create user Cart. it takes parameter : updateinfo{
	* 		"userId":""
	* 		"productList":{
	* 
	* 		"productId","programId","quantity"
	* }
	* 
	* }  
	*/

	updateCartProductList(requestObject) {

	}

	/************************************* 	END OF CART FUNCTIONALITY 	*********************************** */
	// store state
	toggleState() { // click handler
		console.log('IN');
		let bool = this.isIn;
		this.isIn = bool === false ? true : false;
	}

	setAddClaimRoutes() {
		let programInfo: any = this.configService.getloggedInProgramInfo();

		if (programInfo.claimEntryType === "invoice")
			this.addClaimRoute = "./addclaims-invoice";
		else if (programInfo.claimEntryType === "digital") {
			if (programInfo.basedOn === "product")
				this.addClaimRoute = "./addclaims-digital-products";
			else if (programInfo.basedOn === "brand")
				this.addClaimRoute = "./addclaims-digital-brands";
		}
		else if (programInfo.claimEntryType === "code") {
			this.addClaimRoute = "./addclaims-code";
		}


	}


	setPoints() {
		if (this.programUserInfo) {
			this.userPoints = this.programUserInfo.totalPointsEarned;
		}

	}

	resetChain() {
	}

	ngOnChanges() {
		this.cartCount = this.productDetailsService.getCartCount();
	}

	//workaround for production environment
	onCartClick2() {

	}

	onCartClick() {
		if (this.cartService.cartArray.length != 0) {
			this.isCartClick = !this.isCartClick;
			this.isCartOutsideClick = false;
			if (this.isEligibleForRedemption) {
				this.isCartClick = false;
			}
			console.log("isCartClick = ", this.isCartClick);
			this.productArr = this.productDetailsService.getProduct();

			console.log("onclick fetch from config SERvice", this.productArr);
			this.cartCount = this.productArr.length;
			console.log("cart length onClick Cart", this.cartCount)
			if (this.productArr.length > 0) {
				this.isButtonDisable = false;
			} else {
				this.isButtonDisable = true;
			}
			this.subPoint = 0;
			for (let i = 0; i < this.productArr.length; i++) {
				this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
				this.productArr[i].isInStock = false;
			}

			this.reedemPoints = parseInt(localStorage.getItem("points"));
			console.log("Redeem Points : ", this.reedemPoints);
			console.log("Sub Point : ", this.subPoint);
			if (this.reedemPoints >= this.subPoint) {
				this.isReedemPoints = false;
			} else {
				this.isReedemPoints = true;
			}
		}

	}


	onCloseClick(index) {
		let product = this.productArr.splice(index, 1);
		this.currentlyDeleted = product;
		this.productDetailsService.setProduct(this.productArr);
		this.productDetailsService.setPlaceOrderProduct(this.productArr);
		console.log("product Array after deletion : ", this.productArr);
		this.cartCount = this.productArr.length;
		if (this.productArr.length) {
			this.isButtonDisable = false;
		} else {
			this.isButtonDisable = true;
		}
		this.subPoint = 0;
		for (let i = 0; i < this.productArr.length; i++) {
			this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
		}

		this.reedemPoints = parseInt(localStorage.getItem("points"));
		console.log("Redeem Points : ", this.reedemPoints);
		console.log("Sub Point : ", this.subPoint);
		if (this.reedemPoints >= this.subPoint) {
			this.isReedemPoints = false;
		} else {
			this.isReedemPoints = true;
		}
		this.updateCartOnClose(this.productArr);
	}




	getAllClients(requestObject) {
		requestObject.event = "login";
		requestObject.userId = this.userInfo.userId;
		requestObject.programId = this.programInfo.programId;
		console.log("response");
		this.homeService.getAllClient(requestObject)
			.subscribe((responseObject) => {
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.clientsArr = responseObject.result;
						this.configService.setClientObj(this.clientsArr[0]);
						console.log('IN COLOR', this.clientsArr[0].clientBrandColor);
						this.clientColor = this.clientsArr[0].clientBrandColor;
						this.configService.setThemeColor(this.clientsArr[0].clientBrandColor);
						console.log("response", responseObject.result);
						console.log("clientsArr", this.clientsArr);

						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}

	getMyChannels(requestObject) {
		this.showLoader = true;

		console.log('req', requestObject);
		if (requestObject.approvalStatus == "") {
			delete requestObject.approvalStatus;
		}

		this.getMyChannelsAPI(requestObject)

			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							console.log("channel Info in home component", responseObject);
							this.isChannelsFound = true;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.isChannelsFound = false;
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	/*********************************** 	CART FUNCTIONALITY 	 *****************************/

	onRemoveFromCart(productId) {
		this.showLoader = true;

		this.cartService.updateToRemoveCartFormReqObj(productId, "Removed");
	}

	onPlaceOrderClick() {
		this.showLoader = true;
		this.isCartClick = false;
		this.productDetailsService.setPlaceOrderProduct(this.productArr);
	}
	getColour(url1): any {

		let scolortToSet: any = null;
		let scolortToSet1: any = null;
		scolortToSet = {
			'background-color': this.configService.getThemeColor()
		}
		scolortToSet1 = {
		}
		let url: string = url1;
		let homeUrl = "/home";
		let ckeckUrl: string = url1;
		url = homeUrl + ckeckUrl.substr(1, ckeckUrl.length);
		if (url1 == 'rewardGallery') {
			url = homeUrl + this.rewardGalleryRoute.substr(1, this.rewardGalleryRoute.length);


			if (this.router.isActive(homeUrl + "/globalrewardsgallery/categories", true) || this.router.isActive(homeUrl + "/rewardsgallery/categories", false)) {
				return scolortToSet;
			} else if (this.router.url.includes('product-details') || this.router.url.includes('categories')) {
				return scolortToSet;
			}
			else {
				return scolortToSet1;
			}



		}
		if (url1 == 'channels') {
			url = homeUrl + this.rewardGalleryRoute.substr(1, this.rewardGalleryRoute.length);

			if (this.router.url.includes('sales-flow') || this.router.url.includes('profile-details')) {
				return scolortToSet;
			}
			else {
				return scolortToSet1;
			}




		}
		if (url1 == 'claim') {
			let claimUrl1: string;
			let claimUrl2: string;
			claimUrl1 = homeUrl + "/listclaims";
			claimUrl2 = homeUrl + "/addclaims-invoice";

			let setColor = this.router.isActive(claimUrl1, true) || this.router.isActive(claimUrl2, true);
			if (setColor) {
				return scolortToSet;
			} else {
				return scolortToSet1;
			}
		}
		if (url1 == 'booster') {
			let claimUrl1: string;
			let claimUrl2: string;
			claimUrl1 = homeUrl + "/booster";
			claimUrl2 = homeUrl + "/contest-details";
			let setColor = this.router.isActive(claimUrl1, true) || this.router.isActive(claimUrl2, true);
			if (setColor) {
				return scolortToSet;
			} else {
				return scolortToSet1;
			}
		}
		if (url1 == 'LeadManagement') {
			let leadUrl1: string;
			leadUrl1 = homeUrl + "/lead-management-home";
			let setColor = this.router.isActive(leadUrl1, true);
			if (setColor) {
				return scolortToSet;
			} else {
				return scolortToSet1;
			}
		}

		if (url1 != undefined) {
			let url: string;
			let homeUrl = "/home";
			let ckeckUrl: string = url1;
			url = homeUrl + ckeckUrl.substr(1, ckeckUrl.length);
			let setColor = this.router.isActive(url, true);
			if (setColor) {
				return scolortToSet;
			} else {
				return scolortToSet1;
			}
		}
	}


	mouseLeaveAddClaim() {
		console.log("on mouseOver LEAVE   ........................");
		this.isAddClaimHover = false;
		console.log('this.isAddClaimHover', this.isAddClaimHover);
	}
	mouseOverAddClaim() {
		this.isAddClaimHover = true;
		console.log('this.isAddClaimHover OVER', this.isAddClaimHover);
		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}

	mouseLeaveShowClaim() {
		console.log("on mouseOver LEAVE   ........................");
		this.isShowClaimHover = false;
		console.log('this.isAddClaimHover', this.isAddClaimHover);
	}
	mouseOverShowClaim() {
		this.isShowClaimHover = true;
		console.log('this.isAddClaimHover OVER', this.isAddClaimHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}
	mouseLeaveShowLeadManagement() {
		console.log("on mouseOver LEAVE   ........................");
		this.isShowClaimHover = false;
		console.log('this.isAddClaimHover', this.isAddClaimHover);
	}
	mouseOverShowLeadManagement() {
		this.isShowLeadManagementHover = true;
		console.log('this.isAddClaimHover OVER', this.isShowLeadManagementHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}
	mouseLeaveOffer() {
		console.log("on mouseOver LEAVE   ........................");
		this.isOfferHover = false;
		console.log('this.isAddClaimHover', this.isAddClaimHover);
	}
	mouseOverOffer() {
		this.isOfferHover = true;
		console.log('this.isAddClaimHover OVER', this.isAddClaimHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}

	mouseLeaveContest() {
		console.log("on mouseOver LEAVE   ........................");
		this.isContestHover = false;
		console.log('this.isAddClaimHover', this.isAddClaimHover);
	}
	mouseOverContest() {
		this.isContestHover = true;
		console.log('this.isAddClaimHover OVER', this.isAddClaimHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}
	//demo start JSW
	mouseLeaveLeadReport() {
		console.log("on mouseOver LEAVE   ........................");
		this.leadHover = false;
	}
	mouseOverLeadReport() {
		this.leadHover = true;
		console.log('this.isAddClaimHover OVER', this.isAddClaimHover);

		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}

	mouseLeaveDistributorReport() {
		console.log("on mouseOver LEAVE   ........................");
		this.distributorHover = false;
	}
	mouseOverDistributorReport() {
		this.distributorHover = true;

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}

	mouseLeaveDealerReport() {
		console.log("on mouseOver LEAVE   ........................");
		this.dealerHover = false;
	}
	mouseOverDealerReport() {
		this.dealerHover = true;

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}

	mouseLeaveLoginReport() {
		console.log("on mouseOver LEAVE   ........................");
		this.loginHover = false;
	}
	mouseOverLoginReport() {
		this.loginHover = true;

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		let scolortToSet: any = null;

	}
	//demo end JSW




	getBackgroundColor() {
		let scolortToSet: any = null;
		scolortToSet = {
			'background-color': this.roverElementColor
		}
		return scolortToSet;
	}
	mouseLeave() {
		console.log("on mouseLeave   ........................");
		return;
	}

	onQuantityClick(productId, quantity, index) {
		if (quantity <= 0) {
			console.log("*******************INDEX,QUANTITY**********", index + "***********" + quantity);
			this.cartService.cartArray[index].quantity = 1;
			quantity = 1;
		}
		this.cartService.updateToCartFormReqObj(productId, "Pending", quantity);


	}
	/***************************** 	END OF CART FUNCTIONALITY 	 ******************************/

	getMyChannelsAPI(requestObject): any {
		console.log("inside home service to get channels");
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		requestObject.serviceType = "programSetup";
		requestObject.clientId = this.clientId;

		requestObject.programId = this.configService.getloggedInProgramInfo().programId;
		var userIds = this.userInfo.userId;
		requestObject.userInfo = {};
		console.log("this.configService.getProgramUserInfo()", this.configService.getProgramUserInfo());
		requestObject.userInfo = {
			"programId": this.configService.getloggedInProgramInfo().programId,
			"programLogin": true,
			"userId": this.configService.getloggedInProgramUser().programUserId,
			"userType": ""
		}

		this.loginInfo = this.configService.getLoggedInUserInfo();
		console.log("loginInfo>>>>>>", this.loginInfo);
		if (this.loginInfo.clientId === undefined || this.loginInfo.clientId === null) {
			requestObject.userInfo.userType = "ChannelPartner"
		} else {
			requestObject.userInfo.userType = "ClientUser"
		}
		console.log("req<><><><><>", requestObject.userInfo.userType);
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}


		let reqObj = requestObject;
		reqObj.frontendUserInfo = frontEndInfo;
		let url = this.configService.getApiUrls().getMyChannels;
		console.log("getMyChannels rqObj", reqObj);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	closeDetails() {
		this.showBirthdayPopUp = false
	}




	downloadDistributorDealerReport() {
		this.getAllParticipantsReports();
	}
	getAllParticipantsReports() {
		var requestObj = {
			"programId": this.configService.getprogramInfo().programId,

			"clientId": this.configService.getprogramInfo().clientId,
			"reportType": "Detailed",
			"serviceType": "programSetup",
			"sort": {
				"createdAt": -1
			},
			"programRole": [JSON.parse(localStorage.getItem("loggedInProgramUserInfo")).programRole]
		};
		this.programparticipantsArr = [];
		this.showLoader = true;
		this.homeService.getProgramParticipantsReport(requestObj)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						this.programparticipantsArr = [];
						this.programparticipantsArr = responseObject.result;
						console.log("this.programparticipantsArr", this.programparticipantsArr);
						for (let i = 0; i < this.programparticipantsArr.length; i++) {
							if (this.programparticipantsArr[i].businessEntityUserDetails !== undefined) {
								for (let j = 0; j < this.programparticipantsArr[i].businessEntityUserDetails.length; j++) {
									if (this.programparticipantsArr[i].businessEntityUserDetails[j].userType === "owner") {
										this.programparticipantsArr[i].fullName = this.programparticipantsArr[i].businessEntityUserDetails[j].firstName + ' ' + this.programparticipantsArr[i].businessEntityUserDetails[j].lastName
									} else {
										this.programparticipantsArr[i].fullName = "-";
									}
								}
							}
						}
						this.generateParticipantsCsv();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.programparticipantsArr = [];
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	}
	generateParticipantsCsv() {
		this.printingArray = [];
		this.tempStateArray = [];
		this.tempCityArray = [];
		this.tempRoleArray = [];
		this.tempRolesSetArray = [];
		this.tempArrayForParsing = [];

		if (this.programparticipantsArr !== null || this.programparticipantsArr !== undefined) {
			this.headerArray = ["User Id", "User Name", "User Type", "Role", "Enroll Date", "Mobile No."];
			for (var i = 0; i < this.programparticipantsArr.length; i++) {
				var temp = {
					"User_Id": "",
					"User_Name": "",
					"User_Type": "",
					"Role_Name": "",
					"Enrolled_Date": "",
					"Mobile_Number": ""
				};
				temp.User_Id = this.programparticipantsArr[i].userId;
				temp.User_Name = this.programparticipantsArr[i].userName;
				temp.User_Type = this.programparticipantsArr[i].userType;
				temp.Role_Name = this.programparticipantsArr[i].roleName;
				temp.Enrolled_Date = this.programparticipantsArr[i].enrolledDate;
				if (this.programparticipantsArr[i].mobileNumber == "" || this.programparticipantsArr[i].mobileNumber == undefined) {
					temp.Mobile_Number = "";
				} else {
					temp.Mobile_Number = this.programparticipantsArr[i].mobileNumber;

				}
				this.printingArray.push(temp);
			}

			this._csvService.download(this.printingArray, 'Program_Participant_Report');
		}
	};

	downloadLoginReport() {

		this.getAllLoginReports();
	}
	getAllLoginReports() {
		var requestObj = {
			"programId": "PR1493824208902",
			"clientId": this.configService.getprogramInfo().clientId,
			"skip": 0,
			"limit": 1000,
			"reportType": "Detailed",
			"serviceType": "programSetup",
			"sort": {
				"createdAt": -1
			},
		};
		this.programloginArr = [];
		this.showLoader = true;
		this.homeService.getProgramLoginReport(requestObj)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						this.programloginArr = [];
						this.programloginArr = responseObject.result;
						this.generateLoginCsv();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.programloginArr = [];
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	};

	generateLoginCsv() {
		this.printingArray = [];
		if (this.programloginArr !== null || this.programloginArr !== undefined) {
			this.rows = [];
			this.headerArray = ["User Name", "Login Time", "State", "City", "Mobile Number Or Email ID", "App Type", "Browser", "IP"];
			for (var i = 0; i < this.programloginArr.length; i++) {
				var temp = {
					"UserName": "",
					"LoginTime": "",
					"State": "",
					"City": "",
					"MobileNumberOrEmailID": "",
					"AppType": "",
					"Browser": "",
					"IP": "",
				};
				if (this.programloginArr[i].data.userName == undefined || this.programloginArr[i].data.userName == "") {
					temp.UserName = "";

				} else {
					temp.UserName = this.programloginArr[i].data.userName;
				}
				if (this.programloginArr[i].data.time == undefined || this.programloginArr[i].data.time == "") {
					temp.LoginTime = "";

				} else {
					temp.LoginTime = this.programloginArr[i].data.time;
				}
				if (this.programloginArr[i].data.State == undefined || this.programloginArr[i].data.State == "") {
					temp.State = "";

				} else {
					temp.State = this.programloginArr[i].data.region_name;
				}
				if (this.programloginArr[i].data.City == undefined || this.programloginArr[i].data.City == "") {
					temp.City = "";

				} else {
					temp.City = this.programloginArr[i].data.City;
				}

				if (this.programloginArr[i].data.MobileNumberOrEmailID == undefined || this.programloginArr[i].data.MobileNumberOrEmailID == "") {
					temp.MobileNumberOrEmailID = "";

				} else {
					temp.MobileNumberOrEmailID = this.programloginArr[i].data.MobileNumberOrEmailID;
				}

				if (this.programloginArr[i].data.appType == undefined || this.programloginArr[i].data.appType == "") {
					temp.AppType = "";

				} else {
					temp.AppType = this.programloginArr[i].data.appType;
				}

				if (this.programloginArr[i].data.browser == undefined || this.programloginArr[i].data.browser == "") {
					temp.Browser = "";

				} else {
					temp.Browser = this.programloginArr[i].data.browser;
				}

				if (this.programloginArr[i].data.IP == undefined || this.programloginArr[i].data.IP == "") {
					temp.IP = "";

				} else {
					temp.IP = this.programloginArr[i].data.IP;
				}
				temp.City = this.programloginArr[i].data.city;
				temp.MobileNumberOrEmailID = this.programloginArr[i].data.mobileNoOrEmailId;
				temp.AppType = this.programloginArr[i].data.appType;
				temp.Browser = this.programloginArr[i].data.browser;
				temp.IP = this.programloginArr[i].data.ip;
				this.printingArray.push(temp);

				var tempArray = [];
				tempArray.push(temp.UserName);
				tempArray.push(temp.LoginTime);
				tempArray.push(temp.State);
				tempArray.push(temp.City);
				tempArray.push(temp.MobileNumberOrEmailID);
				tempArray.push(temp.AppType);
				tempArray.push(temp.Browser);
				tempArray.push(temp.IP);
				this.rows.push(tempArray);
			}

			this._csvService.download(this.printingArray, 'Program_Login_Report');
		}
	}

	downloadDistributorLeadReport() {

		var obj = {
			providedby: this.configService.getloggedInProgramUser().programUserId,
			programId: this.configService.getprogramInfo().programId,
			clientId: this.configService.getprogramInfo().clientId,
			frontendUserInfo: this.configService.getFrontEndUserInfo()
		}
		this.leadService.getLead(obj).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("channel Info in home component", responseObject);
						this.leadslist = responseObject.result;
						this.generateLeadCsv();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	};

	generateLeadCsv() {
		this.printingArray = [];

		if (this.leadslist !== null || this.leadslist !== undefined) {
			this.headerArray = ["projectname",
				"ownername",
				"projecttype",
				"expectedvalue",
				"address",
				"city",
				"state",
				"pincode",
				"keycontacts",
				"providedby",
				"providedbyRole",
				"userName",
				"qualified",
				"status",
				"programId",
				"clientId",
				"leadId",
				"createdAt",
				"updatedAt"];
			for (var i = 0; i < this.leadslist.length; i++) {
				var temp = {
					"projectname": "",
					"ownername": "",
					"projecttype": "",
					"expectedvalue": "",
					"address": "",
					"city": "",
					"state": "",
					"pincode": 0,
					"keycontacts": [],
					"providedby": "",
					"providedbyRole": "",
					"userName": "",
					"qualified": "",
					"status": "",
					"programId": "",
					"clientId": "",
					"leadId": "",
					"createdAt": "",
					"updatedAt": ""
				}

				temp.projectname = this.leadslist[i].data.projectname;
				temp.ownername = this.leadslist[i].data.ownername;
				temp.projecttype = this.leadslist[i].data.projecttype;
				temp.expectedvalue = this.leadslist[i].data.expectedvalue;
				temp.address = this.leadslist[i].data.address;
				temp.city = this.leadslist[i].data.city;
				temp.state = this.leadslist[i].data.state;
				temp.pincode = this.leadslist[i].data.pincode;
				temp.keycontacts = this.leadslist[i].data.keycontacts;
				temp.providedby = this.leadslist[i].data.providedby;
				temp.providedbyRole = this.leadslist[i].data.providedbyRole;
				temp.userName = this.leadslist[i].userName[0];
				temp.qualified = this.leadslist[i].data.qualified;
				temp.status = this.leadslist[i].data.status;
				temp.programId = this.leadslist[i].data.programId;
				temp.clientId = this.leadslist[i].data.clientId;
				temp.leadId = this.leadslist[i].data.leadId;
				temp.createdAt = this.leadslist[i].data.createdAt.slice(0, 10);
				temp.updatedAt = this.leadslist[i].data.updatedAt.slice(0, 10);

				this.printingArray.push(temp);
			};
		}
		this._csvService.download(this.printingArray, "Lead_Report");

	}

	// code to auto collapse sidebar on click background

	closeSidebar() {
		console.log("@@@@");
		// const dom2: any = document.getElementById("open").style.display = "block";
		// const dom3: any = document.getElementById("close").style.display = "none";
		document.getElementById("sidebar").style.width = "0";
		document.getElementById("sidebar").style.overflow = "hidden";
		document.getElementById("close").style.display = "none";
		document.getElementById("open").style.display = "block";
		document.getElementById("body").style.overflow = "auto";
		if (document.getElementById("modal")) {
			console.log("in if cndition: close sidebar");
			document.getElementById("body").style.overflow = "hidden";
		}

		if (this.configService.getModalClose()) {
			console.log("this.configService.getModalClose()", this.configService.getModalClose());
			console.log("in if cndition: open sidebar");
			document.getElementById("body").style.overflow = "auto";
		}
		if (!this.configService.getModalClose()) {
			if($(event.target).closest(".modal,.js-open-modal").length){
				console.log("outside click");
				document.getElementById("body").style.overflow = "auto";
				this.configService.setModalClose(true);
			}
			else{
				console.log("this.configService.getModalClose()", this.configService.getModalClose());
				console.log("in if cndition: open sidebar");
				document.getElementById("body").style.overflow = "hidden";
			}
		}


	}


}
