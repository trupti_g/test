/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from './home.service';
import { SidebarComponent } from './sidebar/sidebar.component'
import { HeaderComponent } from './header/header.component';
import { StringService } from "../shared-services/strings.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './notifications.component.html',
	styleUrls: ['../app.component.scss', './notifications.component.scss'],
	providers: [Device]

})
export class NotificationsComponent implements OnInit, OnDestroy {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	public noBirthdayRecords: boolean = false;
	public noRecords: boolean = false;
	public noRecordswish: boolean = false;
	public notificationIdArr: Array<any> = [];
	receiverProgramUserId: any;
	showData: boolean = false;
	RoleName: any;
	State: any;
	fullname: any;
	programUserInfo: any;
	public todaysBirthday: any = [];
	public activity: Array<any> = [];
	public wish: Array<any> = [];
	userService: any;
	showLoader: boolean;
	programUserId: any;
	private birthdaywish: FormGroup;


	public selectedTab: string;
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private productDetailsService: ProductDetailsService,
		private rewardsGalleryService: GlobalRewardGalleryService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private homeService: HomeService,
		public sidebar: SidebarComponent,
		public header: HeaderComponent,
		private stringService: StringService,
		private device: Device,
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}
		this.selectedTab = 'Activity';
		this.programUserInfo = configService.getloggedInProgramUser();
		this.birthdaywish = this.formBuilder.group({
			message: ["Happy Birthday!!"]
		});
	}

	ngOnInit() {

		// google analytics page view code
		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Notifications')
		} else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Notifications");
					ga('send', 'pageview');
				}
			});
		}
	

		this.sidebar.close();
		if (this.configService.getFlagToBirthdayNotification() === true) {
			this.selectedTab = 'Birthday';
			const dom: any = document.getElementById('birthday-tab').classList.add('active');
			const dom1: any = document.getElementById('birthday').classList.add('show');
			const dom2: any = document.getElementById('birthday').classList.add('active');
		} else if (this.configService.getFlagToActivityNotification() === true) {
			const dom: any = document.getElementById('activity-tab').classList.add('active');
			const dom1: any = document.getElementById('activity').classList.add('show');
			const dom2: any = document.getElementById('activity').classList.add('active');
		}
		else if (this.configService.getFlagToWishNotification() === true) {
			const dom: any = document.getElementById('wish-tab').classList.add('active');
			const dom1: any = document.getElementById('wish').classList.add('show');
			const dom2: any = document.getElementById('wish').classList.add('active');
		}


		var todaysDate = new Date();


		var reqObj1 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"kycStatus": "NA",
			"todaysDate": todaysDate.toISOString().slice(0, 10),
		}
		this.getTodaysBirthday(reqObj1);




		var reqObj2 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"programUserId": this.programUserInfo.programUserId,
			"serviceType": "programOperations",
			"isRead": false,
			"sort": { "createdAt": -1 }


		}
		this.getUserNotifications(reqObj2);

	}

	ngOnDestroy() {
		this.configService.setFlagToActivityNotification(true);

	}

	tabChange(tab) {
		this.selectedTab = tab;
	}
	tabClick(tab: string) {
		this.googleAnalyticsEventsService.emitEvent("Notifications", "Tab click", tab)
	}

	navigate(leadId) {
		this.router.navigate(['../home/activityDetails']);
		this.configService.setFlagToBirthdayNotification(false);
	}




	/*
     For Todays Birthday Fetching
 */
	getTodaysBirthday(requestObject) {
		console.log("response");
		this.showLoader = true;
		this.homeService.getTodaysBirthday(requestObject)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.noBirthdayRecords = true;
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.noBirthdayRecords = true;
						break;
					case responseCodes.RESP_SUCCESS:


						this.todaysBirthday = responseObject.result;
						if (!this.todaysBirthday) {
							if (this.todaysBirthday.length == 0) {
								this.noBirthdayRecords = true;
								console.log("noBirthdayRecords", this.noBirthdayRecords);

							}
						}
						console.log("Todays Birthday", this.todaysBirthday);
						if (this.todaysBirthday) {
							for (var a = 0; a < this.todaysBirthday.length; a++) {
								var fullName = this.todaysBirthday[a].firstName + " " + this.todaysBirthday[a].lastName;
								console.log("FullName", fullName);
								this.todaysBirthday[a].fullName = fullName;
							}
						}

						console.log("TodaysBirthday", responseObject.result);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.noBirthdayRecords = true;
						break;
					case responseCodes.RESP_FAIL:
						this.noBirthdayRecords = true;
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.noBirthdayRecords = true;
						break;
				}
			},
				err => {

				}
			);
	}


	/**
	 * 
	 * For User Notifications
	 */
	getUserNotifications(requestObject) {
		console.log("response");
		this.showLoader = true;
		this.homeService.getUserNotifications(requestObject)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						console.log("UserNotifications", responseObject);

						if (responseObject.result) {

							for (var a = 0; a < responseObject.result.length; a++) {

								if (responseObject.result[a].type === "Lead") {

									var obj = {
										activitymessage: "",
										createdAt: "",
										notificationId: ""
									}
									obj.notificationId = responseObject.result[a].notificationId;
									obj.activitymessage = responseObject.result[a].message;

									//IE 10 Issue
									var date = new Date(responseObject.result[a].createdAt);
									obj.createdAt = date.toDateString();
									console.log("obj.createdAt", obj.createdAt);


									this.activity.push(obj);

								}

								if (responseObject.result[a].type === "birthday") {

									var obj1 = {
										wishesmessage: "",
										createdAt: "",
										senderName: "",
										notificationId: ""
									}
									obj1.notificationId = responseObject.result[a].notificationId;
									obj1.wishesmessage = responseObject.result[a].message;
									obj1.createdAt = responseObject.result[a].createdAt;
									obj1.senderName = responseObject.result[a].senderName;


									this.wish.push(obj1);

								}

								console.log("Type", responseObject.result[a].type)
							}

						}
						console.log("Activities", this.activity);


						if (this.activity.length === 0) {
							this.noRecords = true;
							console.log("Flag1", this.noRecords);
						}
						console.log("Wish Array", this.wish.length);
						if (this.wish.length === 0) {
							this.noRecordswish = true;
							console.log("Flag2", this.noRecordswish);
						}
						console.log("Wishes", this.wish);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.noRecordswish = true;

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	}

	sendData(todays) {
		this.fullname = todays.fullName;
		this.State = todays.state;
		this.RoleName = todays.roleName;
		this.receiverProgramUserId = todays.businessId;

		var getLoggedInUserInfo = this.configService.getLoggedInUserInfo();

		this.googleAnalyticsEventsService.emitEvent("Notifications", "Send wishes", getLoggedInUserInfo.fullName);



		console.log("this.fullname", this.fullname);
		console.log("this.state", this.State);
		console.log("this.RoleName", this.RoleName);
		console.log("this.receiverProgramUserId", this.receiverProgramUserId);

		this.showData = true;

	}

	sendWish() {

		console.log("Send Wish Inside Function");
		var obj = {

			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"programId": this.configService.getprogramInfo().programId,
			"clientId": this.configService.getprogramInfo().clientId,
			"senderName": this.configService.getLoggedInUserInfo().fullName,
			"senderRoleName": this.configService.getloggedInProgramUser().roleName,
			"message": this.birthdaywish.value.message,
			"senderId": this.programUserInfo.programUserId,
			"receiverId": this.receiverProgramUserId,
			"serviceType": "programOperations",
			"type": "birthday"
		}

		console.log("Object for Add User Notifications", obj);
		this.showLoader = true;

		this.homeService.addUserNotifications(obj).subscribe(
			responseObject => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						const dom3: any = document.getElementById('closePopup').click();

						this.feedbackMessageComponent.updateMessage(true, "Birthday wish sent", "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, "Some error occured during sending birthday wish ", "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);




	}


	/**
	 * 
	 * Updating Activity User Notification as Mark as Read
	 */
	updateUserNotification(activity) {

		console.log("Inside Update User Notifications");

		console.log("Activity", activity);

		console.log("Notification Id", activity.notificationId);


		var obj = {
			"clientId": this.configService.getprogramInfo().clientId,
			"notificationId": activity.notificationId,
			updateInfo: {
				"isRead": true,
			},

			"serviceType": "programOperations"

		}

		console.log("Upadte Object", obj);
		this.showLoader = true;
		this.homeService.updateUserNotifications(obj).subscribe(
			responseObject => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						console.log(responseObject.result);

						if (responseObject.result < 0) {
							this.noRecords = true;
						}


						if (responseObject.result) {
							this.noRecords = false;
							for (var a = 0; a < this.activity.length; a++) {
								for (var b = 0; b < responseObject.result.length; b++) {

									if (this.activity[a].notificationId === responseObject.result[b].notificationId) {
										this.activity.splice(a, 1);

										if (b !== responseObject.result.length) {

											this.configService.activityCount--;
										}
									}


								}

							}

						}





						this.feedbackMessageComponent.updateMessage(true, "Notification was marked as read", "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, "Failed while Marked as Read", "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);



	}

	/**
	 * 
	 * Updating Birthday Wishes User Notification as Mark as Read
	 */

	userNotifications(wish) {



		console.log("Inside Birthday Wish Update User Notifications");

		console.log("wish", wish);

		console.log("Notification Id", wish.notificationId);


		var obj = {
			"clientId": this.configService.getprogramInfo().clientId,
			"notificationId": wish.notificationId,
			updateInfo: {
				"isRead": true,
			},

			"serviceType": "programOperations"

		}

		console.log("Upadte Object", obj);

		this.showLoader = true;
		this.homeService.updateUserNotifications(obj).subscribe(
			responseObject => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						console.log(responseObject.result);



						if (responseObject.result) {
							for (var a = 0; a < this.wish.length; a++) {
								for (var b = 0; b < responseObject.result.length; b++) {

									if (this.wish[a].notificationId === responseObject.result[b].notificationId) {
										this.wish.splice(a, 1);
										if (b !== responseObject.result.length) {

											this.configService.wishCount--;
										}
									}
								}
							}
						}

						this.feedbackMessageComponent.updateMessage(true, "Successfully  Marked as Read", "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, "Failed while Marked as Read ", "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);



	}
}
