
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from './home.service';
import { SidebarComponent } from './sidebar/sidebar.component'
import { HeaderComponent } from './header/header.component'
import { S3UploadService } from "../shared-services/s3-upload.service";
import { ISubscription } from "rxjs/Subscription";
import { StringService } from "../shared-services/strings.service";
import { CsvService } from "../shared-services/csv.service";
import { LeadManagementService } from "../lead-management-system/lead-management.service";
import { ReportComponent } from "./report.component";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable, BehaviorSubject } from 'rxjs';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
    templateUrl: './hallOfFameDataUpload.component.html',
    styleUrls: ['../app.component.scss', './hallOfFameDataUpload.component.scss'],
    providers: [Device]

})

export class HallOfFameUploadComponent implements OnInit, OnDestroy {
    isFileSelected: boolean;

    detailedMsg: string;
    showLoader: boolean;
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    public apiCounter: number;
    private subscription: ISubscription;
    private fileSelectCount: number;
    private isFileError: boolean;
    private uploadForm: FormGroup;
    private month: any = [];
    private year: any = [];
    private isYearSelected: boolean = false;
    printingArray = [];
    keyContactsArray = [];
    tempArray = [];
    tempArray1: any[];
    programUserInfo: any;
    public leadslist: any = [];
    headerArray: string[];

    public fileArr;

    subject1 = new BehaviorSubject("");

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private homeService: HomeService,
        public sidebar: SidebarComponent,
        public header: HeaderComponent,
        private s3UploadService: S3UploadService,
        private stringService: StringService,
        private _csvService: CsvService,
        private leadService: LeadManagementService,
        private device: Device,
    ) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
    }

    ngOnInit() {

        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('hallOfFame')
		} else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga("set", "page", "hallOfFame");
                    ga("send", "pageview");
                }
            });
        }


        //this.excelToJson();
        this.sidebar.close();
        this.uploadForm = this.formBuilder.group({
            "month": [{ value: "" , disabled: true}, [Validators.required]],
            "year": ["", [Validators.required]],
            "uploadFile": [""]
        });
        this.submitFile();

        // var convertExcel = require('excel-as-json').processFile;
        // console.log("convertExcel", convertExcel);
        this.stringService.setYear();
        this.year = this.stringService.getYear();
        console.log("this.year ", this.year);

        this.valueChanges();
        this.programUserInfo = this.configService.getloggedInProgramUser();

        this.subject1.subscribe((value) => {
            console.log("Subscription got", value);

        })
        // var node_xj = require("xls-to-json");

        // //converting excel data to json
        // node_xj({
        //     input: 'ChannelPartner.xlsx',  // input xls
        //     //input:'clientUserTemplate.xlsx',
        //     //input: 'excelsheet/' + excelFileName,
        //     output: "output.json" // output json
        // }, function (err, responseArray) {
        //     if (err) {
        //         console.log("AJ  err", err);
        //     } else {
        //         console.log("AJ  responseArray", responseArray);
        //     }
        // })

    }

    //To track changes in Feedback form
    valueChanges() {
        this.uploadForm.valueChanges.subscribe((year) => {
            console.log("DATA---------------------", year);

            if (year !== null && year !== "") {
            }
        });
    }

    onChange(year) {
        console.log("year", year);
        var date = new Date();
        var currentYr = date.getFullYear();
        if (year == currentYr) {
            this.stringService.setMonth(true);
        }
        else {
            this.stringService.setMonth(false);
        }
        this.month = this.stringService.getMonth();
        this.uploadForm.get('month').enable();
    }

    ngOnDestroy() {

    }



    bulkUpload() {
        this.apiCounter = 0;
        this.s3UploadService.uploadFiles();
    }


    resetClick() {
        this.uploadForm.reset({

        });
        this.isFileError = false;
        this.fileSelectCount = 0;
    }



    onFileChange(fileInput: any) {

        console.log("File", fileInput);


        this.fileSelectCount++;
        if ((this.fileSelectCount == 0) || ((this.fileSelectCount % 2) != 0)) {
            this.isFileError = true;
        } else {
            this.isFileError = false;
        }


        let filePath = "JSW_UploadContents" + "/" + "JSW_HallOfFameUpload_Data" + "/" + "HF";
        this.s3UploadService.formParams(fileInput, filePath);

    }




    /**
       * METHOD   : submitFile
       * DESC     : Called when file is successfully uploaded 
       */
    submitFile() {
        console.log("before subscribe");
        this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
            console.log("@@@@@@@ VAlue @@@@@@@@@", value);
            console.log("File uploaded");
            var Url;
            if (value.length > 0) {
                let reqObj: any = {};
                reqObj.template = "";
                let pathArray: string[] = []
                for (let i = 0; i < value.length; i++) {
                    pathArray.push(value[i].Key);
                    Url = value[i].Location;
                }
                console.log("URL", Url);

                // this.convertExceltoJson(Url)
                //     .map((res: Response) => res)
                //     .subscribe(res => function () {
                //         console.log("RESPONSE TTGGGGG", res);


                //     });

                if (this.apiCounter === 0) {
                    let reqObj = {
                        // "template": pathArray[0],
                        "template": pathArray[0],
                        "clientId": this.configService.getprogramInfo().clientId,
                        "month": this.uploadForm.value.month,
                        "year": this.uploadForm.value.year,
                        "frontendUserInfo": this.configService.getFrontEndUserInfo(),
                        "programId": this.configService.getprogramInfo().programId,
                        "serviceType": "Lead"
                    };
                    console.log('reqObj', reqObj);
                    this.uploadHallOfFameData(reqObj);
                    console.log("After Method");
                    pathArray[0] = "";
                    this.apiCounter = 1;
                }

            }
        });
    }


    /**
  * METHOD   : uploadHallOfFameData
  * DESC     : Bulk Upload the  Hall Of Fame data
  */
    uploadHallOfFameData(requestObj) {

        this.showLoader = true;
        this.homeService.uploadHallOfFameData(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;

                console.log("**********Add Data**********");
                console.log("Response Object", responseObject);
                let responseCodes = this.configService.getStatusTokens();
                this.detailedMsg = "";
                if (responseObject.errorResult) {
                    for (let i = 0; i < responseObject.errorResult.length; i++) {
                        this.detailedMsg = this.detailedMsg + '<div>' + '<span style="font-weight:bold !important; min-width:150px !important;">Record &nbsp;: &nbsp;' + responseObject.errorResult[i].SrNo + '</span> &nbsp; &nbsp; &nbsp; &nbsp;' + '<span style="font-weight:bold !important;">Reason &nbsp; : &nbsp;' + responseObject.errorResult[i].reason + '</span>' + '</div>';
                    }

                }
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:

                    // console.log("RRRRRRRRRRRRRRRrrr");
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:


                        console.log("************Success*************************");



                        this.feedbackMessageComponent.updateMessage(true, "Successfully Uploaded", "alert-success");
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                    // console.log("RRRRRRRRRRRRRRRrrr1");
                        break;
                    case responseCodes.RESP_FAIL:
                    console.log("RRRRRRRRRRRRRRRrrr2");
                    this.feedbackMessageComponent.updateMessage(true, "Star of month already uploaded for this month !!", "alert-danger");

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                    // console.log("RRRRRRRRRRRRRRRrrr3");
                        break;
                    case responseCodes.RESP_BLOCKED:
                    // console.log("RRRRRRRRRRRRRRRrrr4");
                        break;
                }
            },
                err => {
                    this.showLoader = false;
                }
            );
    }

    downloadLeadReport() {

        var startdate = new Date(this.uploadForm.value.year, this.uploadForm.value.month - 1, 1);
        var enddate = new Date(this.uploadForm.value.year, this.uploadForm.value.month, 0);
        console.log("RRRRRRR",startdate,enddate);
        var obj = {
            providedby: this.configService.getloggedInProgramUser().programUserId,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            frontendUserInfo: this.configService.getFrontEndUserInfo(),
            qualified: true,
            startDate: startdate,
            endDate: enddate
        }
        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            delete obj.providedby
        };
        this.showLoader = true;
        this.leadService.getLeadReport(obj).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        // console.log("RrrrrrRRRRRRRRRRRRRRRRRRR1");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                    // console.log("RrrrrrRRRRRRRRRRRRRRRRRRR2");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        console.log("channel Info in home component", responseObject);
                        this.leadslist = responseObject.result;
                        this.generateLeadCsv();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                    // console.log("RrrrrrRRRRRRRRRRRRRRRRRRR3");
                        break;
                    case responseCodes.RESP_FAIL:
                    console.log("responseObject.message",responseObject.message);
                    this.feedbackMessageComponent.updateMessage(true,"Qualified leads not found for selected month", "alert-danger");
                    // console.log("RrrrrrRRRRRRRRRRRRRRRRRRR4");
                        this.showLoader = false;
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                    // console.log("RrrrrrRRRRRRRRRRRRRRRRRRR5");
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    };

    generateLeadCsv() {
        this.printingArray = [];
        this.keyContactsArray = [];
        this.tempArray = [];

        if (this.leadslist !== null || this.leadslist !== undefined) {
            this.headerArray = [
                "Sr. No.",
                "projectname",
                "ownername",
                "projecttype",
                "expectedvalue",
                "address",
                "city",
                "state",
                "pincode",
                "keycontacts",
                "providedby",
                "expectedSalesPeriod",
                "providedbyRole",
                "userName",
                "Mobile No",
                "distributor_Name",
                "ASM_Name",
                "points_earned",
                "qualified",
                "status",
                "createdAt",
                "engineerName",
                "engineerId",
                "month",
                "year",
                "StarOfMonth"
            ];

            console.log("AJJJJJJJJJJJJJJJJJJJJJJJJJ-----------,this.leadslist.length", this.leadslist.length);
            for (var i = 0; i < this.leadslist.length; i++) {

                console.log("AJJJJJJJJJJJJJJJJJJJJJJJJJ-----------,i", i);
                if (this.leadslist[i].data.keycontacts) {
                    if (this.leadslist[i].data.keycontacts.length > 0) {
                        for (var j = 0; j < this.leadslist[i].data.keycontacts.length; j++) {
                            this.keyContactsArray = [];
                            this.tempArray = [];
                            this.tempArray1 = [];
                            var temp: any = {}
                            if (j === 0) {
                                temp["Sr. No."] = i + 1;
                                // temp["Engineer Name"] = "",
                                // temp["Star Of the Month"] ="",
                                // temp["Engineer Id"] ="",
                                temp["Project Name"] = this.leadslist[i].data.projectname;
                                temp["Owner Name"] = this.leadslist[i].data.ownername;
                                temp["Project Type"] = this.leadslist[i].data.projecttype;
                                if (this.leadslist[i].data.expectedtonnage !== undefined
                                    && this.leadslist[i].data.expectedtonnage !== ""
                                    && this.leadslist[i].data.expectedtonnage !== null) {
                                    temp["Expected Tonnage"] = this.leadslist[i].data.expectedtonnage;
                                } else {
                                    temp["Expected Tonnage"] = 0
                                }

                                temp["Address"] = this.leadslist[i].data.address;
                                temp["City"] = this.leadslist[i].data.city;
                                temp["State"] = this.leadslist[i].data.state;
                                temp["Pin Code"] = this.leadslist[i].data.pincode;
                                // temp.key_contacts = this.leadslist[i].data.keycontacts;
                                // temp.provided_by = this.leadslist[i].data.providedby;
                                // temp.expectedSalesPeriod = this.leadslist[i].data.expectedSalesPeriod;
                                if (this.leadslist[i].data.expectedSalesPeriod !== undefined
                                    && this.leadslist[i].data.expectedSalesPeriod !== ""
                                    && this.leadslist[i].data.expectedSalesPeriod !== null) {
                                    temp["Expected Sales Period"] = this.leadslist[i].data.expectedSalesPeriod;
                                } else {
                                    temp["Expected Sales Period"] = "N.A."
                                }
                                // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
                                temp["User Name"] = this.leadslist[i].userName;
                                temp["Mobile No"] = this.leadslist[i].userMobileNo;

                                // temp.distributor_Id = this.leadslist[i].distributorId;
                                temp["Distributor Name"] = this.leadslist[i].distributorName;
                                // temp.ASM_Id = this.leadslist[i].ASMId;
                                temp["ASM Name"] = this.leadslist[i].ASMName;
                                if (this.leadslist[i].data.pointsEarned !== undefined
                                    && this.leadslist[i].data.pointsEarned !== ""
                                    && this.leadslist[i].data.pointsEarned !== null) {
                                    temp["Points Earned"] = this.leadslist[i].data.pointsEarned;
                                } else {
                                    temp["Points Earned"] = 0
                                }
                                // temp.qualified = this.leadslist[i].data.qualified;

                                // temp.program_Id = this.leadslist[i].data.programId;
                                // temp.client_Id = this.leadslist[i].data.clientId;
                                // temp.lead_Id = this.leadslist[i].data.leadId;
                                temp["Lead Added Date"] = this.leadslist[i].data.createdAt.slice(0, 10);
                                // temp.lead_updated_Date = this.leadslist[i].data.updatedAt.slice(0, 10);
                                // temp.key_contacts = this.tempArray1;
                                if (this.leadslist[i].data.qualified === true) {
                                    temp["Status"] = "Qualified";
                                } else if (this.leadslist[i].data.rejected === true) {
                                    temp["Status"] = "Rejected";
                                } else if (this.leadslist[i].data.pending === true) {
                                    temp["Status"] = "Pending";
                                } else {
                                    temp["Status"] = "";
                                }

                                console.log("T ---this.leadslist[i].data.providedby", this.leadslist[i].data.providedby);
                                temp["engineerId"] = this.leadslist[i].data.providedby;
                                temp["month"] = this.uploadForm.value.month;
                                temp["year"] = this.uploadForm.value.year;
                                temp["StarOfMonth"] = "";
                            }
                            else {
                                temp["Sr. No."] = "";
                                temp["Project Name"] = ""
                                temp["Owner Name"] = "";
                                temp["Project Type"] = "";
                                temp["Expected Tonnage"] = "";
                                temp["Address"] = "";
                                temp["City"] = "";
                                temp["State"] = "";
                                temp["Pin Code"] = "";
                                // temp.key_contacts = this.leadslist[i].data.keycontacts;
                                // temp.provided_by = "";
                                temp["Expected Sales Period"] = "",
                                    // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
                                    temp["User Name"] = "";
                                temp["Mobile No"] = "";

                                // temp.distributor_Id = "";
                                temp["Distributor Name"] = "";
                                // temp.ASM_Id = "";
                                temp["ASM Name"] = "";
                                temp["Points Earned"] = "";
                                // temp.qualified = this.leadslist[i].data.qualified;

                                // temp.program_Id = "";
                                // temp.client_Id = "";
                                // temp.lead_Id = "";
                                temp["Lead Added Date"] = "";
                                // temp.lead_updated_Date = "";
                                // temp.key_contacts = this.tempArray1;
                                temp["Status"] = "";
                                temp["engineerId"] = "";
                                temp["month"] = "";
                                temp["year"] = "";
                                temp["StarOfMonth"] = "";
                            }
                            if (this.leadslist[i].data.keycontacts[j].keyPersonName) {
                                temp["Key Contact Name"] = this.leadslist[i].data.keycontacts[j].keyPersonName;
                            } else {
                                temp["Key Contact Name"] = "";
                            }

                            if (this.leadslist[i].data.keycontacts[j].keyPersonRole) {
                                temp["Key Contact Role"] = this.leadslist[i].data.keycontacts[j].keyPersonRole;
                            } else {
                                temp["Key Contact Role"] = "";
                            }

                            if (this.leadslist[i].data.keycontacts[j].keyPersonContact) {
                                temp["Key Contact Mobile No"] = this.leadslist[i].data.keycontacts[j].keyPersonContact;
                            } else {
                                temp["Key Contact Mobile No"] = "";
                            }

                            console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
                            this.printingArray.push(temp);
                            console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray", i);
                        }
                    }
                    else {
                        this.formLeadObject(i);
                    }
                } else {
                    this.formLeadObject(i);
                }

            };
        }
        console.log(" download");

        this._csvService.download(this.printingArray, "Lead_Report");

    }

    formLeadObject(i) {
        var temp: any = {};
        temp["Sr. No."] = i + 1;
        temp["Project Name"] = this.leadslist[i].data.projectname;
        temp["Owner Name"] = this.leadslist[i].data.ownername;
        temp["Project Type"] = this.leadslist[i].data.projecttype;
        // temp.expected_tonnage = this.leadslist[i].data.expectedtonnage;
        if (this.leadslist[i].data.expectedtonnage !== undefined
            && this.leadslist[i].data.expectedtonnage !== ""
            && this.leadslist[i].data.expectedtonnage !== null) {
            temp["Expected Tonnage"] = this.leadslist[i].data.expectedtonnage;
        } else {
            temp["Expected Tonnage"] = 0
        }
        temp["Address"] = this.leadslist[i].data.address;
        temp["City"] = this.leadslist[i].data.city;
        temp["State"] = this.leadslist[i].data.state;
        temp["Pin Code"] = this.leadslist[i].data.pincode;

        // temp.provided_by = this.leadslist[i].data.providedby;
        // temp.expectedSalesPeriod = this.leadslist[i].data.expectedSalesPeriod;
        if (this.leadslist[i].data.expectedSalesPeriod !== undefined
            && this.leadslist[i].data.expectedSalesPeriod !== ""
            && this.leadslist[i].data.expectedSalesPeriod !== null) {
            temp["Expected Sales Period"] = this.leadslist[i].data.expectedSalesPeriod;
        } else {
            temp["Expected Sales Period"] = "N.A."
        }
        // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
        temp["User Name"] = this.leadslist[i].userName;
        temp["Mobile No"] = this.leadslist[i].userMobileNo;
        // temp.distributor_Id = this.leadslist[i].distributorId;
        temp["Distributor Name"] = this.leadslist[i].distributorName;
        // temp.ASM_Id = this.leadslist[i].ASMId;
        temp["ASM Name"] = this.leadslist[i].ASMName;
        if (this.leadslist[i].data.pointsEarned !== undefined
            && this.leadslist[i].data.pointsEarned !== ""
            && this.leadslist[i].data.pointsEarned !== null) {
            temp["Points Earned"] = this.leadslist[i].data.pointsEarned;
        } else {
            temp["Points Earned"] = 0
        }
        // temp.qualified = this.leadslist[i].data.qualified;

        // temp.program_Id = this.leadslist[i].data.programId;
        // temp.client_Id = this.leadslist[i].data.clientId;
        // temp.lead_Id = this.leadslist[i].data.leadId;
        temp["Lead Added Date"] = this.leadslist[i].data.createdAt.slice(0, 10);
        // temp.lead_updated_Date = this.leadslist[i].data.updatedAt.slice(0, 10);
        // temp.key_contacts = this.tempArray1;
        if (this.leadslist[i].data.qualified === true) {
            temp["Status"] = "Qualified";
        }
        if (this.leadslist[i].data.rejected === true) {
            temp["Status"] = "Rejected";
        }
        if (this.leadslist[i].data.pending === true) {
            temp["Status"] = "Pending";
        }
        temp["engineerId"] = this.leadslist[i].data.providedby;
        temp["month"] = this.uploadForm.value.month;
        temp["year"] = this.uploadForm.value.year;
        temp["StarOfMonth"] = "";
        temp["Key Contact Name"] = "";
        temp["Key Contact Role"] = "";
        temp["Key Contact Mobile No"] = "";
        console.log("this.leadslist[i].data.providedby", this.leadslist[i].data.providedby);

        console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
        this.printingArray.push(temp);
        console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
    }




}
