import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SidebarComponent } from '../sidebar/sidebar.component'
import { ConfigService } from "../../shared-services/config.service";
import { StringService } from "../../shared-services/strings.service";
import { HomeService } from "../home.service";
// import { TranslateService } from '@ngx-translate/core';
import { CartService } from "../../shared-services/cart.service";
import { ProductDetailsService } from "../../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../../global-rewards-gallery/rewards-gallery.service";
import { RewardsGalleryService } from "../../rewards-gallery/rewards-gallery.service";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { EventsService } from "../../events/events.service";
import { Subscription } from 'rxjs/Subscription';
import { FILL_STYLE_FLAG } from '@angular/core/src/animation/animation_constants';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";


@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	privilegeDistributor: boolean;
	nonprivilegeDistributor: boolean;
	public activitycount: any = 0;
	public wishcount: any = 0;
	public birthdayCount = 0;
	public showpopup: boolean = false;
	programUserInfo: any;
	pushRightClass: string = 'push-right';
	userInfo: any;
	isLoggedIn: boolean = false;
	userName: string;
	message1: boolean = false;
	subscription: Subscription;

	private roverElementColor: string;
	private isChannelsFound: boolean = false;
	private showChannels: boolean = false;
	private loggedInProgramInfo: any;
	private clientBrandColor: any;
	private userPoints: string;
	private programRoleInfo: any;
	private showClaims: boolean = false;
	private showPoints: boolean = false;
	private showRedemption: boolean = false;
	private isCartClick: boolean = false;
	private isCartOutsideClick: boolean = false;
	private isButtonDisable: boolean;
	// public isInStock: boolean = false;
	public parseInteger: any;
	isIn: boolean = false;
	public isReedemPoints: boolean = false;
	public productArr: any = [];
	// public productQuantityArr: any = [];
	private addClaimRoute: string;
	public programLogo: string;
	public cartCount: number = 0;
	public subPoint: number = 0;
	public reedemPoints: number = 0;

	public parseMethod: any;
	public localStorageVar: any;
	public pointsAvailableToRedeem: number;
	public isEligibleForRedemption: boolean;
	public math: any;
	public isEligibleForPointConversion: boolean;
	public programInfo: any;
	private cart: any;
	public currentlyDeleted: any;
	public rewardGalleryRoute: any;
	private globalProductList: any;
	private localProductList: any;
	private showLoader: boolean = false;
	private loginInfo: any;
	private businessInfo: any = {};
	private getmyChannelsReqObj: any = {};
	private clientId: any;
	private options;
	private headers;
	public cartArray: any = [];
	public clientColor: any;
	public isShowcart: boolean = false;
	public isCartEmpty: boolean = false;
	public isCartFull: boolean = true;
	public backicon: boolean = false;




	constructor(public router: Router,
		public sidebar: SidebarComponent,
		private configService: ConfigService,
		private cartService: CartService,
		private productDetailsService: ProductDetailsService,
		private globalRewardGalleryService: GlobalRewardGalleryService,
		private rewardsGalleryService: RewardsGalleryService,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private stringService: StringService,
		private eventsService: EventsService,
		private _location: Location,
		private homeService: HomeService) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
			console.log("Display mobile", this.displaymobile);
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;
			console.log("Display mobile", this.displaymobile);
		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
			console.log("Display mobile", this.displaymobile);
		}
		this.parseInteger = parseInt;
		console.log("home subscribed called constructor");

		this.subscription = this.eventsService.subject.subscribe(message => {
			this.message1 = true;
			console.log("########################## this.message ", this.message1);
			//  this.message = message; 
			this.show();
		});

		// this.message1 = true;

		// this.subscription = this.eventsService.getMessage().subscribe(message => { this.message = message; });		console.log("header subscription",this.subscription);

		// console.log("header backicon",message);
		// console.log("header subscription",backicon);
	}

	show() {
		console.log("########################## in show this.message ", this.message1);
		this.message1 = true;
	}
	ngOnDestroy() {
		// unsubscribe to ensure no memory leaks
		this.subscription.unsubscribe();
	}
	backClicked() {
		this._location.back();
	}

	ngOnInit() {


		console.log("Inside Ng init before", this.cartService.cartArray);
		// this.getbackiconflagheader();
		// this.eventsService.passbackiconflag();

		this.programInfo = this.configService.getloggedInProgramInfo();
		console.log("PROGRAMINFO-----------------", this.programInfo)
		let RGType = this.programInfo.useGlobalRewardGallery ? "GlobalRG" : "ProgramSpecificRG";

		this.cartService.RGType = RGType;
		this.cartService.setFields(RGType);
		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
		});
		this.clientId = this.configService.getloggedInProgramInfo().clientId;
		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this
				.configService
				.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		if (this.configService.getloggedInProgramInfo()) {
			let loggedInProgramInfo = this.configService.getloggedInProgramInfo();
			console.log("&&&&&&&&&&&&& TERMINOLOGY USED IN PROGRAM &&&&&&&&&&&&&&&&&", loggedInProgramInfo.terminologyToUseInProgram);
			if (loggedInProgramInfo.terminologyToUseInProgram == "point") {
				console.log("HOME COMPONENT POINTS SECTION");
				let s: string = "Points";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			if (loggedInProgramInfo.terminologyToUseInProgram == "coupon") {
				console.log("HOME COMPONENT COUPONS SECTION");
				let s: string = "Coupons";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			if (loggedInProgramInfo.terminologyToUseInProgram == "voucher") {
				let s: string = "Voucher";
				this.configService.setTerminology(s);
				this.stringService.setPoints(s);
			}
			this.cartService.getCartFormReqObj(null)
		}




		this.configService.setRewardGalleryFlag(false);
		this.isButtonDisable = false;
		this.math = Math;
		this.parseInteger = parseInt;
		this.isEligibleForRedemption = false;
		this.isEligibleForPointConversion = false;

		this.productArr = JSON.parse(localStorage.getItem("productDetails"));
		console.log(this.productArr);
		this.parseMethod = JSON.parse;
		this.localStorageVar = localStorage;
		console.log("Product Array of local storage", this.productArr);
		this.cartCount = this.productDetailsService.getCartCount();
		this.userInfo = this.configService.getLoggedInUserInfo();
		console.log("USERINFO--------------", this.userInfo);




		for (let i = 0; i < this.productArr.length; i++) {
			this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
		}

		// if (this.userInfo.clientId) {
		// 	ga('set', 'userId', this.userInfo.email);
		// }


		// Set the user ID using signed-in user_id.
		this.programUserInfo = this.configService.getloggedInProgramUser();

		if (this.programUserInfo.programRole == this.stringService.getStaticContents().jswDealerProgramRole ||
			this.programUserInfo.programRole == this.stringService.getStaticContents().jswEngineerProgramRole) {
			this.isShowcart = true;
		}
		// if(this.isShowcart){
		// document.getElementById("bell").style.marginLeft = "7%";
		// }else{
		// document.getElementById("bell").style.marginLeft = "40%";
		// }

		// [Comment - production]
		this.pointsAvailableToRedeem = this.programUserInfo.totalPointsAvailableForRedeem;
		// [End Comment - production]

		//[UnComment - production]
		// this.pointsAvailableToRedeem = this.programUserInfo.pointsAvialableToRedeem;
		//[End UnComment - production]


		let programUser = this.programUserInfo;
		if (this.programUserInfo) {

			this.programRoleInfo = programUser.programRoleInfo;
			console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
			if (this.programRoleInfo) {

				if (!this.programRoleInfo.eligibleForRedemption) {
					this.isEligibleForRedemption = true;
				}

				if (this.programRoleInfo.canMakeClaims == true) {
					this.showClaims = true;
				} else {
					this.showClaims = false;
				}

				if (this.programRoleInfo.canEarnPoints == true) {
					this.showPoints = true;
				} else {
					this.showPoints = false;
				}
				if (this.programRoleInfo.eligibleForRedemption == true) {
					this.showRedemption = true;
				} else {
					this.showRedemption = false;
				}


			}
		} else {
			this.showClaims = true;
			this.showPoints = true;
			this.showRedemption = true;
		}


		console.log('ROle Name', this.programUserInfo.roleName);
		if (this.programUserInfo.roleName === "Retailer") {

			this.showChannels = false;
		} else {
			this.showChannels = true;
		}


		this.programInfo = this.configService.getloggedInProgramInfo();
		this.programLogo = this.programInfo.programLogo[0];
		console.log("PROGRAM INFO CONSTRUCTOR [HOME COMPONENT] : ", this.programInfo);

		console.log("*********************************************", this.programUserInfo);
		if (this.programInfo.useGlobalRewardGallery) this.rewardGalleryRoute = "./globalrewardsgallery";
		else this.rewardGalleryRoute = "./rewardsgallery";

		// CHECK FOR ELIGIBILITY FOR POINT CONVERSION
		if (this.programInfo.useGlobalRewardGallery) {
			this.isEligibleForPointConversion = false;
		} else {
			if (this.programInfo.allowStarPointsConversion) {
				this.isEligibleForPointConversion = true;
			} else {
				this.isEligibleForPointConversion = false;
			}
		}
		console.log("useGlobalRewardGallery : ", this.programInfo.useGlobalRewardGallery);
		console.log("allowStarPointsConversion : ", this.programInfo.allowStarPointsConversion);
		console.log("isEligibleForPointConversion : ", this.isEligibleForPointConversion);


		// END OF CHECK

		this.cartService.stopLoader.subscribe((value) => {
			console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@", this.cartService.cartArray);
			console.log(value);
		})





		this.programUserInfo = this.configService.getloggedInProgramUser();

		this.router.events.subscribe(val => {
			if (
				val instanceof NavigationEnd &&
				window.innerWidth <= 992 &&
				this.isToggled()
			) {
				this.toggleSidebar();
			}
		});

		var todaysDate = new Date();
		var reqObj: any = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},

			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"kycStatus": "NA",
			"todaysDate": todaysDate.toISOString().slice(0, 10)

		}
		this.getTodaysBirthdayCount(reqObj);

		var reqObj1 = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientId": this.configService.getprogramInfo().clientId,
			"programId": this.configService.getprogramInfo().programId,
			"programUserId": this.programUserInfo.programUserId,
			"serviceType": "programOperations",
			"isRead": false,


		}
		this.getUserNotifications(reqObj1);

		// cart
		this.cartService.cartItemsUpdated.subscribe((value) => {
			console.log("**********SUBJECT CALLED********", value)
			console.log("cartService.cartArray", this.cartService.cartArray);
		})

		console.log("calling getCart");
		this.syncCart();
		console.log("cart count", this.productDetailsService.getCartCount());
		this.cartCount = this.productDetailsService.getCartCount();
		this.cartArray = this.cartService.cartArray;
		if (this.cartService.cartArray.length == 0) {
			this.isCartEmpty = true;
			this.isCartFull = false;
		}
		console.log("cartService.cartArray", this.cartService.cartArray);


		//Customization for JSW Privileged /Non - Privileged Distributors

		if (this.programUserInfo.programRole == this.stringService.getStaticContents().jswDistributorProgramRole) {
			if (this.programUserInfo.profileId === this.stringService.getStaticContents().jswDistrubutorProfileid) {

				this.nonprivilegeDistributor = true;
				this.privilegeDistributor = false;
				console.log("this.privilegeDistributor", this.privilegeDistributor);
				console.log("privilegeDistributor  11");
			}
			else {
				this.nonprivilegeDistributor = false;
				this.privilegeDistributor = true;
				console.log(" privilegeDistributor 12");
			}
		}
		else {
			this.nonprivilegeDistributor = false;
			this.privilegeDistributor = true;
			console.log(" privilegeDistributor 2");
		}
		console.log("this.nonprivilegeDistributor  --  this.privilegeDistributor ",this.nonprivilegeDistributor, this.privilegeDistributor);
		console.log("Inside Ng init at end", this.cartService.cartArray);
	}



	syncCart() {
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		console.log("requestObject to cart", reqObj);
	}


	updateCartQuantity(productArray) {
		let cart: any = [];
		console.log("original cart", cart);
		console.log("product Array Length in cart Update", productArray.length);
		for (let i = 0; i < productArray.length; i++) {
			if (productArray[i].quantity == 0) {
				productArray[i].quantity = 1;
				continue;
			}
			let found: boolean = false;
			for (let j = 0; j < cart.length; j++) {
				console.log("for updatation check ", cart[j].productId,
					cart[j].productId, productArray[i].productId
					, productArray[i].type, cart[j].type,
					"isPlaced", (!cart[j].isPlaced || cart[j].isPlaced == false)
					, (cart[j].programId == this.programInfo.programId && cart[j].productId == productArray[i].productId && productArray[i].type == cart[j].type
						&& (!cart[j].isPlaced || cart[j].isPlaced == false)));
				if (cart[j].programId == this.programInfo.programId && cart[j].productId == productArray[i].productId &&
					productArray[i].type == cart[j].type && (!cart[j].isPlaced || cart[j].isPlaced == false)) {
					found = true;
					cart[j].quantity = productArray[i].quantity;
				}
			}
			if (!found) {
				let cartObject: any = {};
				cartObject.programId = this.programInfo.programId;
				cartObject.productId = productArray[i].productId;
				cartObject.quantity = productArray[i].quantity;
				if (productArray[i].type == "globalRewardGallery") {
					cartObject.type = "globalRewardGallery";
				} else if (productArray[i].type == "programSpecificRewardGallery") {
					cartObject.type = "programSpecificRewardGallery";
				}
				cartObject.isPlaced = false;
				console.log("ADDING NEW ELEMENT IN CART DATABSE ", cartObject);
				cart.push(cartObject);
			}
		}
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.updateInfo = {};
		reqObj.updateInfo.productList = cart;
		console.log("passing this object to upate the cart on quantity click", reqObj);
		this.updateCartProductList(reqObj);
	}
	updateCartOnClose(productArray) {
		let cart: any = [];
		console.log("product closed", this.currentlyDeleted[0]);
		console.log("product Array Length in cart Update", productArray.length);
		console.log("get cart", cart);
		for (let i = 0; i < cart.length; i++) {
			let found: boolean = false;
			if (!found) {
				if (this.currentlyDeleted[0]) {
					if (cart[i].programId == this.programInfo.programId &&
						cart[i].productId == this.currentlyDeleted[0].productId && cart[i].type == this.currentlyDeleted[0].type
						&& (!cart[i].isPlaced)) {
						console.log("removing product from cart", cart[i]);
						let popped = cart.pop(i);
						console.log("wanted to delete", JSON.stringify(this.currentlyDeleted))
						console.log("product popped on delete", JSON.stringify(popped));
					}
				}
			}
		}
		let reqObj: any = {};
		reqObj.frontendUserInfo = this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {};
		reqObj.userId = this.configService.getloggedInProgramUser().programUserId;
		reqObj.updateInfo = {};
		reqObj.updateInfo.productList = cart;
		console.log("passing this object to upate the cart on quantity click", reqObj);
		this.updateCartProductList(reqObj);

	}
	/**
	 * 
	 * @param requestObject
	 * @author sumant.k@sankeysolutions.com
	 * this function is used to get user Cart. it takes parameter : userId  
	 * it sets localstorage cart to this.cart
	 */
	getCartProductList(requestObject) {
		console.log("in get cart Items");

		if (!requestObject.userId) {
			console.log("Passing userInfo to access Cart inside", this.userInfo);
			requestObject.userId = this.userInfo.clientId;
		}

	}
	/**
	* 
	* 
	* 
	* @param requestObject
	* @author sumant.k@sankeysolutions.com
	* this function is used to update or create user Cart. it takes parameter : updateinfo{
	* 		"userId":""
	* 		"productList":{
	* 
	* 		"productId","programId","quantity"
	* }
	* 
	* }  
	*/

	updateCartProductList(requestObject) {

	}
	ngOnChanges() {
		this.cartCount = this.productDetailsService.getCartCount();
	}

	//workaround for production environment
	onCartClick2() {

	}

	onCartClick() {


		if (this.cartService.cartArray.length != 0) {
			this.isCartClick = !this.isCartClick;
			this.isCartOutsideClick = false;
			if (this.isEligibleForRedemption) {
				this.isCartClick = false;
			}
			console.log("isCartClick = ", this.isCartClick);
			this.productArr = this.productDetailsService.getProduct();
			console.log("onclick fetch from config SERvice", this.productArr);
			this.cartCount = this.productArr.length;
			console.log("cart length onClick Cart", this.cartCount);
			if (this.productArr.length > 0) {
				this.isButtonDisable = false;
			} else {
				this.isButtonDisable = true;
			}
			this.subPoint = 0;
			for (let i = 0; i < this.productArr.length; i++) {
				this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
				this.productArr[i].isInStock = false;
			}

			this.reedemPoints = parseInt(localStorage.getItem("points"));
			console.log("Redeem Points : ", this.reedemPoints);
			console.log("Sub Point : ", this.subPoint);
			if (this.reedemPoints >= this.subPoint) {
				this.isReedemPoints = false;
			} else {
				this.isReedemPoints = true;
			}
		}

	}


	onCloseClick(index) {
		let product = this.productArr.splice(index, 1);
		this.currentlyDeleted = product;
		this.productDetailsService.setProduct(this.productArr);
		this.productDetailsService.setPlaceOrderProduct(this.productArr);
		console.log("product Array after deletion : ", this.productArr);
		this.cartCount = this.productArr.length;
		if (this.productArr.length) {
			this.isButtonDisable = false;
		} else {
			this.isButtonDisable = true;
		}
		this.subPoint = 0;
		for (let i = 0; i < this.productArr.length; i++) {
			this.subPoint = this.subPoint + (this.productArr[i].pointsPerQuantity * this.productArr[i].quantity);
		}

		this.reedemPoints = parseInt(localStorage.getItem("points"));
		console.log("Redeem Points : ", this.reedemPoints);
		console.log("Sub Point : ", this.subPoint);
		if (this.reedemPoints >= this.subPoint) {
			this.isReedemPoints = false;
		} else {
			this.isReedemPoints = true;
		}
		this.updateCartOnClose(this.productArr);
	}

	onQuantityClick(productId, quantity, index) {
		if (quantity <= 0) {
			console.log("*******************INDEX,QUANTITY**********", index + "***********" + quantity);
			this.cartService.cartArray[index].quantity = 1;
			quantity = 1;
		}
		this.cartService.updateToCartFormReqObj(productId, "Pending", quantity);

	}

	onRemoveFromCart(productId, productName) {
		this.googleAnalyticsEventsService.emitEvent("Cart", "Remove from cart", productName);
		this.showLoader = true;
		this.cartService.updateToRemoveCartFormReqObj(productId, "Removed");
	}


    /**
    * To get birthday count
    */
	getTodaysBirthdayCount(requestObject) {
		console.log("response");
		this.homeService.getTodaysBirthdayCount(requestObject)
			.subscribe((responseObject) => {
				console.log("requestObject", requestObject);
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.birthdayCount = responseObject.count;
						console.log("birthdayCount", responseObject);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}

    /**
    * To get User Notification count
    */


	getUserNotifications(requestObject) {
		console.log("response");
		this.homeService.getUserNotifications(requestObject)
			.subscribe((responseObject) => {
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:

						console.log("UserNotifications", responseObject);

						if (responseObject.result) {
							this.configService.activityCount = 0;
							this.configService.wishCount = 0;
							for (var a = 0; a < responseObject.result.length; a++) {

								if (responseObject.result[a].type === "Lead") {
									this.configService.activityCount++;
									console.log("this.configService.activityCount", this.configService.activityCount);

								}
								if (responseObject.result[a].type === "birthday") {
									this.configService.wishCount++;
								}

								console.log("Type", responseObject.result[a].type)
							}

						}

						console.log("Activities Count", this.configService.activityCount);
						console.log("Wishes Count", this.configService.wishCount);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}

	onPlaceOrderClick() {
		this.isCartClick = false;
		this.productDetailsService.setPlaceOrderProduct(this.productArr);
	}

	isToggled(): boolean {
		const dom: Element = document.querySelector('body');
		return dom.classList.contains(this.pushRightClass);
	}

	toggleSidebar() {
		const dom: any = document.querySelector('body');
		dom.classList.toggle(this.pushRightClass);
	}

	rltAndLtr() {
		const dom: any = document.querySelector('body');
		dom.classList.toggle('rtl');
	}

	onLoggedout() {
		localStorage.removeItem('isLoggedin');
	}


	open() {
		this.sidebar.open();
		console.log("Stop scroll function");
		const dom3: any = document.getElementById('sidebar').style.display;
		console.log("Dom @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", dom3);
		if (dom3 == "block") {
			const dom3: any = document.getElementById('appBody').style.overflow = "hidden";
		}
		else {
			const dom3: any = document.getElementById('appBody').style.display = "auto";
		}




	}




	close() {
		this.sidebar.close();
		const dom3: any = document.getElementById('sidebar').style.display;
		console.log("Dom ############################", dom3);
		if (dom3 == "block") {
			const dom3: any = document.getElementById('appBody').style.overflow = "hidden";
		}
		else {
			const dom3: any = document.getElementById('appBody').style.display = "auto";
		}
		// window.onload = function()
		// {
		// 	document.getElementById("appBody").style.overflow = "auto";
		// }
	}
	dropdown() {

		const dom3: any = document.getElementById('loginpanel').style.display;
		console.log("Dom3", dom3);
		if (dom3 == "block") {
			const dom3: any = document.getElementById('loginpanel').style.display = "none";
		}
		else {
			const dom3: any = document.getElementById('loginpanel').style.display = "block";

		}
	}


	onMyProfileClick() {
		// document.getElementById("dropdowncontent").style.display = "hi";
		let beInfo = this.configService.getloggedInBEInfo();
		if (beInfo != null) {
			this.router.navigate(['/home/be-profile']);
		} else {
			this.router.navigate(['/home/client-profile']);
		}
		//this.profilecontenthideshow();

	}

	// profile content box close code
	profilecontenthideshow() {
		var x = document.getElementById("profilecontent");
		x.style.display = "none";
	}

	profilecontentshow() {
		var x = document.getElementById("profilecontent");
		x.style.display = "block";
	}

	onLogoutClick() {
		this.configService.activityCount = 0;
		this.configService.wishCount = 0;
		var userInfo = this.userInfo;
		this.isLoggedIn = false;
		this.userName = null;
		this.userInfo = null;
		this.configService.setLoggedInUserInfo(null);
		this.configService.setloggedInBEInfo(null);
		this.configService.setLoggedInUserInfo(null);
		this.configService.setloggedInProgramUser(null);
		this.configService.setloggedInProgramInfo(null);
		this.configService.setClientUserInfo(null);
		localStorage.clear();



		this.router.navigate(["login"]);

	}

	notificationpopup() {

		console.log("Notification popup");
		this.showpopup = true
		const dom3: any = document.getElementById("open").style.display = "block";


	}

	routeToNotifications() { // for demo 
		this.router.navigate(['../home/notifications']);
		this.configService.setFlagToBirthdayNotification(true);
	}

	routeToActivities() {
		this.router.navigate(['../home/notifications']);
		this.configService.setFlagToActivityNotification(true);
	}
	routeToWish() {
		this.router.navigate(['../home/notifications']);
		this.configService.setFlagToWishNotification(true);
	};


	testMethod(event) {
		console.log("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", event);
		event.stopPropagation();
	}

	// // For header back icon
	// getbackiconflagheader()
	// {

	//  this.configService.passbackiconflag();
	// 	console.log("in header get flag function",this.backicon);
	// 	// console.log("in header get flag function",this.backicon);

	// }
}
