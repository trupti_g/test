import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { ConfigService } from "../../shared-services/config.service";
import { StringService } from "../../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  showMenu3: string;
  showMenu2: string;
  showMenu1: string;
  privilegeDistributor: boolean;
  nonprivilegeDistributor: boolean;
  programRoleInfo: any;
  programUserInfo: any;
  isActive: boolean = false;
  showMenu: string = "";
  pushRightClass: string = "push-right";
  public elementRef;
  constructor(public router: Router,
    private configService: ConfigService,
    private stringService: StringService,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService) {

    this.programUserInfo = configService.getloggedInProgramUser();

    let programUser = this.programUserInfo;

    if (this.programUserInfo) {
      this.programRoleInfo = programUser.programRoleInfo;
      console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
    }

    this.router.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });


  }

  eventCalled() {
    this.isActive = !this.isActive;
  }

  ngOnInit() {

    if (this.programUserInfo.programRole == this.stringService.getStaticContents().jswDistributorProgramRole) {
      if (this.programUserInfo.profileId === this.stringService.getStaticContents().jswDistrubutorProfileid) {

        this.nonprivilegeDistributor = true;
        this.privilegeDistributor = false;
        console.log("this.privilegeDistributor", this.privilegeDistributor);
      }
      else {
        this.nonprivilegeDistributor = false;
        this.privilegeDistributor = true;
      }
    }
    else {
      this.nonprivilegeDistributor = false;
      this.privilegeDistributor = true;
    }
    var fixed = document.getElementById('appBody');

  }


  //menu 1
  addExpandClass(element: any) {
    this.showMenu1 = "0";
    this.showMenu2 = "0";
    this.showMenu3 = "0";
    console.log("elem", element);
    console.log("this.showMenu", this.showMenu);
    if (element === this.showMenu) {
      console.log("in if");

      this.showMenu = "0";
    }
    else {
      console.log("in else");
      this.showMenu = element;
    }
  }
  // stopScroll()
  // {
  //   console.log("Stop scroll function");
  //   var fixed = document.getElementById('appBody');

  //   fixed.addEventListener('touchmove', function(e) {

  //       e.preventDefault();

  //   }, false);
  // }


  //menu 1
  addExpandClass1(element: any) {
    this.showMenu = "0";
    this.showMenu2 = "0";
    this.showMenu3 = "0";
    console.log("elem", element);
    console.log("this.showMenu", this.showMenu1);
    if (element === this.showMenu1) {
      console.log("in if");
      this.showMenu1 = "0";
    }
    else {
      console.log("in else");
      this.showMenu1 = element;
    }
  }



  //menu 2
  addExpandClass2(element: any) {
    this.showMenu = "0";
    this.showMenu1 = "0";
    this.showMenu3 = "0";
    console.log("elem222", element);
    console.log("this.showMenu2", this.showMenu2);
    if (element === this.showMenu2) {
      console.log("in if");
      this.showMenu2 = "0";
    }
    else {
      console.log("in else");
      this.showMenu2 = element;
    }

  }

  //menu 3
  addExpandClass3(element: any) {
    this.showMenu = "0";
    this.showMenu1 = "0";
    this.showMenu2 = "0";
    console.log("elem", element);
    console.log("this.showMenu", this.showMenu3);
    if (element === this.showMenu3) {
      console.log("in if");
      this.showMenu3 = "0";
    }
    else {
      console.log("in else");
      this.showMenu3 = element;
    }

  }


  isToggled(): boolean {
    const dom: Element = document.querySelector("body");
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector("body");
    dom.classList.toggle(this.pushRightClass);
  }

  rltAndLtr() {
    const dom: any = document.querySelector("body");
    dom.classList.toggle("rtl");
  }


  onLoggedout() {
    localStorage.removeItem("isLoggedin");
  }

  open() {
    console.log("in opennnnnnnnnnnnnnnnnnnnnnnnn");
    const dom1: any = (document.getElementById("sidebar").style.width = "235px");
    const dom2: any = (document.getElementById("open").style.display = "none");
    const dom3: any = (document.getElementById("close").style.display = "block");

    console.log("Dom1", dom1);
    document.getElementById("sidebar").style.overflow = "auto";

    // Code to stop backgroung scrolling when side bar is open

    // var scroll =  document.getElementById('body');
    // console.log("RRRRRRRRRRRRRRRRRRRRRRRR",scroll);


    document.getElementById("body").style.overflow = "hidden";
    console.log("RRRRRRRRRRRRRRRRRRRRRRRR", scroll);
  }

  close() {
    console.log("in closeeeeeeeeeeeeeeeeeeeeeeee");


    const dom2: any = document.getElementById("open").style.display = "block";
    const dom3: any = document.getElementById("close").style.display = "none";
    document.getElementById("sidebar").style.width = "0";
    document.getElementById("sidebar").style.overflow = "hidden";

    // Code to active backgroung scrolling when side bar is close
    // var scroll =  document.getElementById('body');
    // console.log("TTTTTTTTTTTTTTTTTTTTTTTTT",scroll);


    document.getElementById("body").style.overflow = "auto";
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);


  }

  // closeSidebar()
  // {
  //   console.log("@@@@");
  //   // const dom2: any = document.getElementById("open").style.display = "block";
  //   // const dom3: any = document.getElementById("close").style.display = "none";
  //   document.getElementById("sidebar").style.width = "0";
  //   document.getElementById("sidebar").style.overflow = "hidden";
  // }



  notificationPage() {

    this.configService.setFlagToActivityNotification(true);

    console.log("Inside Notification Function");

    //To close submenu
    this.showMenu = "0";
    this.showMenu1 = "0";
    this.showMenu2 = "0";

  }


  submenuClose() {
    const dom3: any = (document.getElementById('leadmgt').style.display = "none");

  }


  productClose() {
    this.googleAnalyticsEventsService.emitEvent("Our products", "View", "JSW site redirection");


    const dom2: any = (document.getElementById("open").style.display = "block");
    const dom3: any = (document.getElementById("close").style.display = "none");
    document.getElementById("sidebar").style.width = "0";

    //To close submenu
    this.showMenu = "0";

    this.close();   //to activate scroll

  }

  closeSubMenus() {
    console.log("In closeSubMenus");
    this.showMenu = "0";
    this.showMenu1 = "0"
    this.showMenu2 = "0"
    this.showMenu3 = "0"
  }

  expertClose() {
    console.log("inside expertClose");
    this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "View", "askAnExpert site redirection");


    const dom2: any = (document.getElementById("open").style.display = "block");
    const dom3: any = (document.getElementById("close").style.display = "none");
    document.getElementById("sidebar").style.width = "0";

    //To close submenu
    this.showMenu = "0";

    this.configService.setModalClose(true);
    this.close();   //to activate scroll

  }



}
