
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from './home.service';
import { SidebarComponent } from './sidebar/sidebar.component'
import { HeaderComponent } from './header/header.component'
import { S3UploadService } from "../shared-services/s3-upload.service";
import { ISubscription } from "rxjs/Subscription";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
    templateUrl: './leaderboardDataUpload.component.html',
    styleUrls: ['../app.component.scss', './leaderboardDataUpload.component.scss'],
    providers: [Device]

})
export class LeaderboardUploadComponent implements OnInit, OnDestroy {
    isFileSelected: boolean;

    detailedMsg: string;
    showLoader: boolean;
    public apiCounter: number;
    private subscription: ISubscription;
    private fileSelectCount: number;
    private isFileError: boolean;
    private uploadForm: FormGroup;

    mobilewidth: any;                   //Added for width capture
    displaypc: boolean = true;        //Added for width capture
    displaymobile: boolean = false;   //Added for width capture


    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private homeService: HomeService,
        public sidebar: SidebarComponent,
        public header: HeaderComponent,
        private s3UploadService: S3UploadService,
        private device: Device,
    ) {
    //    Code for screen width capture
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
            // document.getElementById("desktop-div").style.visibility = "visible";
            // document.getElementById("mobile-div").style.visibility = "hidden";
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
            // document.getElementById("desktop-div").style.visibility = "visible";
            // document.getElementById("mobile-div").style.visibility = "hidden";
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

    }

    ngOnInit() {
        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('leaderboardDataUpload')
		} else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga("set", "page", "leaderboardDataUpload");
                    ga("send", "pageview");
                }
            });
        }
		

        this.sidebar.close();
        this.uploadForm = this.formBuilder.group({

            "uploadFile": ""
        });
        this.submitFile();

    }

    ngOnDestroy() {

    }



    bulkUpload() {
        this.apiCounter = 0;
        this.s3UploadService.uploadFiles();
    }


    resetClick() {
        this.uploadForm.reset({

        });
        this.isFileError = false;
        this.fileSelectCount = 0;
    }



    onFileChange(fileInput: any) {

        console.log("File", fileInput);


        this.fileSelectCount++;
        if ((this.fileSelectCount == 0) || ((this.fileSelectCount % 2) != 0)) {
            this.isFileError = true;
        } else {
            this.isFileError = false;
        }


        let filePath = "JSW_UploadContents" + "/" + "JSW_LeaderboardUpload_Data" + "/" + "LD";
        this.s3UploadService.formParams(fileInput, filePath);

    }




    /**
       * METHOD   : submitFile
       * DESC     : Called when file is successfully uploaded 
       */
    submitFile() {
        console.log("before subscribe");
        this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
            console.log("@@@@@@@ VAlue @@@@@@@@@", value);
            console.log("File uploaded");
            if (value.length > 0) {
                let reqObj: any = {};
                reqObj.template = "";
                let pathArray: any[] = []
                for (let i = 0; i < value.length; i++) {
                    pathArray.push(value[i].Key);
                }
                if (this.apiCounter === 0) {
                    let reqObj = {
                        "template": pathArray[0],
                        "clientId": this.configService.getprogramInfo().clientId,
                        "frontendUserInfo": this.configService.getFrontEndUserInfo(),
                        "programId": this.configService.getprogramInfo().programId,
                        "serviceType": "programOperations"
                    };
                    console.log('reqObj', reqObj);
                    this.uploadLeaderboardData(reqObj);
                    console.log("After Method");
                    pathArray[0] = "";
                    this.apiCounter = 1;
                }
            }
        });
    }




    /**
  * METHOD   : uploadLeaderboardData
  * DESC     : Bulk Upload the  Leaderboard data
  */
    uploadLeaderboardData(requestObj) {

        this.showLoader = true;
        this.homeService.uploadLeaderboardData(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;

                console.log("**********Add Data**********");
                console.log("Response Object", responseObject);
                let responseCodes = this.configService.getStatusTokens();
                this.detailedMsg = "";
                if (responseObject.errorResult) {
                    for (let i = 0; i < responseObject.errorResult.length; i++) {
                        this.detailedMsg = this.detailedMsg + '<div>' + '<span style="font-weight:bold !important; min-width:150px !important;">Record &nbsp;: &nbsp;' + responseObject.errorResult[i].SrNo + '</span> &nbsp; &nbsp; &nbsp; &nbsp;' + '<span style="font-weight:bold !important;">Reason &nbsp; : &nbsp;' + responseObject.errorResult[i].reason + '</span>' + '</div>';
                    }

                }
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:


                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:


                        console.log("************Success*************************");


                        this.feedbackMessageComponent.updateMessage(true, "Successfully Uploaded", "alert-success");
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
                err => {
                    this.showLoader = false;
                }
            );
    }


}
