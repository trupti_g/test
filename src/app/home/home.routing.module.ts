/*  Author         : DEEPAK TERSE
    Description    : Business Entity Routing module
                     Routes to all BE components are specified here breadcrumb
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { HomePlaceorderComponent } from "./home-placeorder.component";

// //Claims components
import { ListClaimsComponent } from "../claims/list-claims.component";
import { AddClaimsInvoiceComponent } from "../claims/add-claims-invoice.component";

//Dashboard components
import { DashboardHomeComponent } from "../dashboard/dashboard-home.component";
//About program 
import { manageraboutusComponent } from "../user/manageraboutus.component";
//Redemptions components
import { RedemptionsHomeComponent } from "../redemptions/redemptions-home.component";

//Rewards Gallery components
import { RewardsGalleryHomeComponent } from "../rewards-gallery/rewards-gallery-home.component";
import { RewardsGalleryHomeComponent2 } from "../global-rewards-gallery/rewards-gallery-home.component";

//Rewards  Gallery Product Details components
import { RewardsGalleryProductDetailsComponent } from "../rewards-gallery/rewards-gallery-product-details.component";
import { RewardsGalleryCategoriesComponent } from "../rewards-gallery/rewards-gallery-categories.component";

//Special Offers
import { ContestDetailsComponent } from "../special-offers/contest/special-offers-contest-details.component";
import { SpecialOffersBoostersComponent } from "../special-offers/boosters/special-offers-boosters.component";

//User components
import { BusinessEntityProfileComponent } from "../user/business-entity-profile.component";
import { ClientProfileComponent } from "../user/client-profile.component";
import { AboutUsComponent } from "../user/aboutus.component";
import { TestimonialComponent } from "../user/testimonial.component";
import { DashboardRedemptionDetailComponent } from "../dashboard/dashboard-redemption-detail.component";
import { DashBoardApprovalsRedemptionList } from "../dashboard/approvals-redemption-list";
import { PerformanceGraphsComponent } from "../dashboard/performance-graphs.component";
import { ProgramTiersComponent } from "../user/program-tiers.component";
import { NotificationsComponent } from "./notifications.component";
import { PrivilegesComponent } from "./privileges.component";
import { LeadManagementViewLeadsComponent } from "../lead-management-system/lead-management-view-leads.component";
import { LeadManagementAddLeadsComponent } from "../lead-management-system/lead-management-add-leads.component";
import { LeadManagementLeadDetailsComponent } from "../lead-management-system/lead-management-lead-details.component";



import { CelebrationComponent } from "../celebrations/celebration.component";
import { CelebrationDetailsComponent } from "../celebrations/celebration-details.component";

import { ReportComponent } from "./report.component";
import { EngineerHomeComponent } from '../user/engineer-home.component';

import { EngineerRequestComponent } from '../user/engineerRequest.component';
import { EngineerDetailPageComponent } from '../user/engineer-Request-DetailPage.component';

import { DealerAboutUsComponent } from "../user/dealer-aboutus.component";


import { RewardGalleryComponent } from "../user/reward-gallery.component";


import { ContactUsComponent } from "../user/contactus.component";

import { LeaderboardUploadComponent } from "./leaderboardDataUpload.component";

import { HallOfFameUploadComponent } from "./hallOfFameDataUpload.component";

//Events
import { EventGalleryComponent } from "../events/eventgallery.component";
import { EventComponent } from "../events/event.component";
import { EventDetailComponent } from "../events/event-detail.component";

//Contest
import { OnGoingContestComponent } from "../special-offers/contest/ongoingcontest.component";
import { ContestComponent } from "../special-offers/contest/contestdetails.component";
import { UpcomingContestComponent } from "../special-offers/contest/upcomingcontest.component";
import { UpcomingDetailComponent } from "../special-offers/contest/upcoming-contest-details.component";
import { PastContestComponent } from "../special-offers/contest/past-contest.component";
import { PastContestGalleryComponent } from "../special-offers/contest/past-gallery.component";

import { LoyaltyComponent } from "./loyalty.component";


//feedback
import { FeedbackComponent } from "../feedback/feedback.component";
import { ViewFeedbackComponent } from "../feedback/viewFeedback.component";  //added by ravi


//Ask An Expert
import { AskAnExpertComponent } from "../ask-an-expert/ask-an-expert.component";

import { ChannelsHierarchyComponent } from "./../channels/channels-hierarchy.component";
import { ChannelsDetailsComponent } from "./../channels/channels-details.component";









export const homeRoutes: Routes = [
    {
        path: '',
        // component: HomeComponent,
        children: [
            {
                path: 'placeorder',
                component: HomePlaceorderComponent,
            },
            //User components
            {//for demo
                path: 'programtiers',
                component: ProgramTiersComponent,
            },
            {
                path: 'notifications',
                component: NotificationsComponent,
            },
            {
                path: 'privileges',
                component: PrivilegesComponent,
            },

            {
                path: 'aboutus',
                component: AboutUsComponent,
            },
            {
                path: 'testimonial',
                component: TestimonialComponent,
            },
            {
                path: 'be-profile',
                component: BusinessEntityProfileComponent,
            },
            {
                path: 'client-profile',
                component: ClientProfileComponent,
            },
            {
                path: 'manageraboutus',
                component: manageraboutusComponent,
            },


            //Claims Routes
            {
                path: 'listclaims',
                component: ListClaimsComponent,
            },
            {
                path: 'addclaims-invoice',
                component: AddClaimsInvoiceComponent,
            },


            //Dashboard components
            {
                path: 'dashboard',
                component: DashboardHomeComponent,
            },

            //Redemptions components
            {
                path: 'redemptions',
                component: RedemptionsHomeComponent,
            },




            //Dashboard components
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },


            //Special Offers
            {
                path: 'contest-details',
                component: ContestDetailsComponent,
            },

            {
                path: 'booster',
                component: SpecialOffersBoostersComponent,
            },
            {
                path: 'dashboardRedemptionDetail',
                component: DashboardRedemptionDetailComponent,
            },
            {
                path: 'dashboard-approvals-redemption-list',
                component: DashBoardApprovalsRedemptionList,
            },
            {
                path: 'dashboard-performance-graphs',
                component: PerformanceGraphsComponent,
            },

            {
                path: 'leads-list',
                component: LeadManagementViewLeadsComponent,
            },
            {
                path: 'add-leads',
                component: LeadManagementAddLeadsComponent,
            },
            {
                path: 'lead-details',
                component: LeadManagementLeadDetailsComponent,
            },

            {
                path: 'events',
                component: EventComponent,
            },

            {
                path: 'reports',
                component: ReportComponent,
            },

            {
                path: 'celebrations',
                component: CelebrationComponent,
            },

            {
                path: 'celebrations-details',
                component: CelebrationDetailsComponent,
            },

            {
                path: 'event-detail',
                component: EventDetailComponent,
            },

            {
                path: 'home-page',
                component: EngineerHomeComponent,
            },


            {
                path: 'EngineersRequest',
                component: EngineerRequestComponent,
            },

            {
                path: 'entity-details',
                component: EngineerDetailPageComponent,
            },


            {
                path: 'dealeraboutus',
                component: DealerAboutUsComponent,
            },


            // Reward Gallery 

            {
                path: 'rewardsgallery', component: RewardsGalleryHomeComponent,
                loadChildren: 'app/rewards-gallery/rewards-gallery.module#RewardsGalleryModule',
            },

            {
                path: 'contactus',
                component: ContactUsComponent,
            },



            {
                path: 'leaderboardupload',
                component: LeaderboardUploadComponent,
            },



            {
                path: 'HallOfFameUpload',
                component: HallOfFameUploadComponent,
            },



            {
                path: 'eventgallery',
                component: EventGalleryComponent,
            },


            //Contests
            {
                path: 'ongoingcontest',
                component: OnGoingContestComponent,
            },


            {
                path: 'contestDetails',
                component: ContestComponent,
            },


            {
                path: 'upcomingcontest',
                component: UpcomingContestComponent,
            },


            {
                path: 'upcomingDetails',
                component: UpcomingDetailComponent,
            },


            {
                path: 'pastcontest',
                component: PastContestComponent,
            },

            {
                path: 'pastcontestgallery',
                component: PastContestGalleryComponent,
            },

            {
                path: 'loyalty',
                component: LoyaltyComponent,
            },

            //feedback

            {
                path: 'feedback',
                component: FeedbackComponent,
            },
            { //added by ravi
                path: 'viewFeedback',
                component: ViewFeedbackComponent,
            },
            // reawds gallery mob
            {
                path: 'rewrdsgallerymob',
                component: RewardsGalleryCategoriesComponent,
            },

            // Ask An Expert
            {
                path: 'askanexpert',
                component: AskAnExpertComponent,
            },
            //My Channel app
             {
                path: 'ChannelsComponent',
                component: ChannelsHierarchyComponent,
            },
            {
                path: 'channel-profile-details',
                component: ChannelsDetailsComponent,
            },




        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {

}
