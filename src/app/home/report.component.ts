import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { SidebarComponent } from './sidebar/sidebar.component';
import { ConfigService } from "../shared-services/config.service";
import { HomeComponent } from './home.component'
import { HomeService } from "./home.service";
import { CsvService } from "../shared-services/csv.service";
import { LeadManagementService } from "../lead-management-system/lead-management.service";
import { StringService } from "../shared-services/strings.service";
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { UV_UDP_REUSEADDR } from "constants";
import { S3UploadService } from "../shared-services/s3-upload.service";
import { ISubscription } from "rxjs/Subscription";
declare var FileTransfer:any;   //Added by Ravi for mobile app report download 
declare var cordova:any           //Added by Ravi for mobile app report download 
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
    templateUrl: "./report.component.html",
    styleUrls: ["../app.component.scss"],
    providers: [Device]
})
export class ReportComponent implements OnInit {
    role: any;
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    tempArray1: any[];
    tempArray: any[];
    keyContactsArray: any[];
    public leadslist: any = [];
    public programparticipantsArr: any = [];
    public tempArrayForParsing: any[];
    tempRoleArray: any[];
    tempRolesSetArray: any[];
    tempCityArray: any[];
    tempStateArray: any[];
    showLoader: boolean;
    headerArray: string[];
    rows: any[];
    programloginArr: any;
    printingArray: any[];
    programRoleInfo: any;
    programUserInfo: any;
    public csv: any;
    public report: any;
    public filePath:any;
    downloadPath : any;
    leadNotFound: boolean = false;
    showPopUp: boolean = false;
    private subscription: ISubscription;

    

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private router: Router,
        public sidebar: SidebarComponent,
        public home: HomeComponent,
        private configService: ConfigService,
        private homeService: HomeService,
        private _csvService: CsvService,
        private leadService: LeadManagementService,
        private stringService: StringService,
        private s3UploadService: S3UploadService,
        private elementRef:ElementRef,
        private device: Device,
        
    ) {
        console.log("platform++++++",window.navigator.platform,window.navigator.onLine);
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
        this.programUserInfo = configService.getloggedInProgramUser();

        let programUser = this.programUserInfo;

        if (this.programUserInfo) {
            this.programRoleInfo = programUser.programRoleInfo;
            console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ",this.displaymobile, this.programRoleInfo);
            // if(this.programRoleInfo.showLeadReport === true){
            //     this.downloadLeadReport();
            // }
            // window.navigator.platform == "Android" || window.navigator.platform == "iPhone" 
        if(this.displaymobile){
            console.log("inside display mobile codein constructor");
            if(this.programRoleInfo.showLeadReport === true){
                this.downloadLeadReport();
            }
            if(this.programRoleInfo.showDistributorReport === true){
                this.downloadDistributorDealerReport('distributor');
            }
            if(this.programRoleInfo.showDealerReport === true){
                this.downloadDistributorDealerReport('dealer');
            }
            if(this.programRoleInfo.showEngineerReport === true){
                this.downloadDistributorDealerReport('engineer');
            }
        }
        }
    }

    ngOnInit() {
         // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Reports')
		} else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga("set", "page", "Reports");
                    ga("send", "pageview");
                }
            });
        }
	
        this.sidebar.close();
        console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ",this.displaymobile,this.programRoleInfo);
    }

    ngAfterViewInit() {
        if(this.displaymobile){
          
            if(this.programRoleInfo.showLeadReport === true){
                document.getElementById("dwnld").addEventListener("click", this.onDeviceReady.bind(this) );
            }
            if(this.programRoleInfo.showDistributorReport === true){
                document.getElementById("dwnld1").addEventListener("click", this.onDeviceReady.bind(this) );
            }
            if(this.programRoleInfo.showDealerReport === true){
                document.getElementById("dwnld2").addEventListener("click", this.onDeviceReady.bind(this) );
            }
            if(this.programRoleInfo.showEngineerReport === true){
                document.getElementById("dwnld3").addEventListener("click", this.onDeviceReady.bind(this) );
            }
        }
        // this.showPopUp = true;
        // this.feedbackMessageComponent.updateMessage(true, "File Downloaded success fully on" + "Path", "alert-success");
    }
    

    downloadLoginReport() {

        this.getAllLoginReports();
    }
    getAllLoginReports() {
        console.log("UserId", this.configService.getLoggedInUserInfo().userId);
        var requestObj = {
            "programId": this.configService.getprogramInfo().programId,
            "clientId": this.stringService.getStaticContents().jswClientId,
            //"userId": "BU152030378429912",
            "userId": this.configService.getLoggedInUserInfo().userId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "skip": 0,
            "limit": 1000,
            "userType": "channelPartner",
            "reportType": "Detailed",
            "serviceType": "programSetup",
            "sort": {
                "createdAt": -1
            },
        };
        this.programloginArr = [];
        this.showLoader = true;
        this.homeService.getProgramLoginReport(requestObj)
            .subscribe((responseObject) => {

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        this.programloginArr = [];
                        this.programloginArr = responseObject.result;

                        console.log("ProgramloginArr", this.programloginArr);
                        this.generateLoginCsv();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.showLoader = false;
                        this.programloginArr = [];
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
                err => {
                    this.showLoader = false;
                }
            );
    };

    generateLoginCsv() {
        this.printingArray = [];
        if (this.programloginArr !== null || this.programloginArr !== undefined) {
            this.rows = [];
            this.headerArray = ["User Name", "Login Time", "State", "City", "Mobile Number Or Email ID", "App Type", "Browser", "IP"];
            for (var i = 0; i < this.programloginArr.length; i++) {
                var temp = {
                    "UserName": "",
                    "LoginTime": "",
                    "State": "",
                    "City": "",
                    "MobileNumberOrEmailID": "",
                    "AppType": "",
                    "Browser": "",
                    "IP": "",
                };
                if (this.programloginArr[i].data.userName == undefined || this.programloginArr[i].data.userName == "") {
                    temp.UserName = "";

                } else {
                    temp.UserName = this.programloginArr[i].data.userName;
                }
                if (this.programloginArr[i].data.time == undefined || this.programloginArr[i].data.time == "") {
                    temp.LoginTime = "";

                } else {
                    temp.LoginTime = this.programloginArr[i].data.time;
                }
                if (this.programloginArr[i].data.State == undefined || this.programloginArr[i].data.State == "") {
                    temp.State = "";

                } else {
                    temp.State = this.programloginArr[i].data.region_name;
                }
                if (this.programloginArr[i].data.City == undefined || this.programloginArr[i].data.City == "") {
                    temp.City = "";

                } else {
                    temp.City = this.programloginArr[i].data.City;
                }

                if (this.programloginArr[i].data.MobileNumberOrEmailID == undefined || this.programloginArr[i].data.MobileNumberOrEmailID == "") {
                    temp.MobileNumberOrEmailID = "";

                } else {
                    temp.MobileNumberOrEmailID = this.programloginArr[i].data.MobileNumberOrEmailID;
                }

                if (this.programloginArr[i].data.appType == undefined || this.programloginArr[i].data.appType == "") {
                    temp.AppType = "";

                } else {
                    temp.AppType = this.programloginArr[i].data.appType;
                }

                if (this.programloginArr[i].data.browser == undefined || this.programloginArr[i].data.browser == "") {
                    temp.Browser = "";

                } else {
                    temp.Browser = this.programloginArr[i].data.browser;
                }

                if (this.programloginArr[i].data.IP == undefined || this.programloginArr[i].data.IP == "") {
                    temp.IP = "";

                } else {
                    temp.IP = this.programloginArr[i].data.IP;
                }

                temp.LoginTime = this.programloginArr[i].data.time;
                temp.State = this.programloginArr[i].data.region_name;

                console.log("State", temp.State = this.programloginArr[i].data.region_name);
                temp.City = this.programloginArr[i].data.city;
                temp.MobileNumberOrEmailID = this.programloginArr[i].data.mobileNoOrEmailId;
                temp.AppType = this.programloginArr[i].data.appType;
                temp.Browser = this.programloginArr[i].data.browser;
                temp.IP = this.programloginArr[i].data.ip;
                this.printingArray.push(temp);

                var tempArray = [];
                tempArray.push(temp.UserName);
                tempArray.push(temp.LoginTime);
                tempArray.push(temp.State);
                tempArray.push(temp.City);
                tempArray.push(temp.MobileNumberOrEmailID);
                tempArray.push(temp.AppType);
                tempArray.push(temp.Browser);
                tempArray.push(temp.IP);
                this.rows.push(tempArray);
            }

            this._csvService.download(this.printingArray, 'Program_Login_Report');
        }
    };

    downloadDistributorDealerReport(role) {
        if (role === 'distributor') {
            this.role = this.stringService.getStaticContents().jswDistributorProgramRole;
        } else if (role === 'dealer') {
            this.role = this.stringService.getStaticContents().jswDealerProgramRole;
        } else if (role === 'engineer') {
            this.role = this.stringService.getStaticContents().jswEngineerProgramRole;
        }
        this.getAllParticipantsReports();
    }
    getAllParticipantsReports() {
        var requestObj = {
            "programId": this.configService.getprogramInfo().programId,
            "clientId": this.stringService.getStaticContents().jswClientId,
            "reportType": "Detailed",
            "serviceType": "programSetup",
            "sort": {
                "createdAt": -1
            },
            "programRole": [this.role]
        };
        this.programparticipantsArr = [];
        this.showLoader = true;
        this.homeService.getProgramParticipantsReport(requestObj)
            .subscribe((responseObject) => {

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        this.programparticipantsArr = [];
                        this.programparticipantsArr = responseObject.result;
                        console.log("this.programparticipantsArr", this.programparticipantsArr);
                        for (let i = 0; i < this.programparticipantsArr.length; i++) {
                            if (this.programparticipantsArr[i].businessEntityUserDetails !== undefined) {
                                for (let j = 0; j < this.programparticipantsArr[i].businessEntityUserDetails.length; j++) {
                                    if (this.programparticipantsArr[i].businessEntityUserDetails[j].userType === "owner") {
                                        this.programparticipantsArr[i].fullName = this.programparticipantsArr[i].businessEntityUserDetails[j].firstName + ' ' + this.programparticipantsArr[i].businessEntityUserDetails[j].lastName
                                    } else {
                                        this.programparticipantsArr[i].fullName = "-";
                                    }
                                }
                            }
                        }
                        this.generateParticipantsCsv();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.showLoader = false;
                        this.programparticipantsArr = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
                err => {
                    this.showLoader = false;
                }
            );
    }
    generateParticipantsCsv() {
        this.printingArray = [];
        this.tempStateArray = [];
        this.tempCityArray = [];
        this.tempRoleArray = [];
        this.tempRolesSetArray = [];
        this.tempArrayForParsing = [];

        if (this.programparticipantsArr !== null || this.programparticipantsArr !== undefined) {
            this.headerArray = ["User Id", "User Name", "User Type", "Role", "Enroll Date", "Mobile No."];
            for (var i = 0; i < this.programparticipantsArr.length; i++) {
                var temp = {
                    "User Id": "",
                    "User Name": "",
                    "User Type": "",
                    "Role Name": "",
                    "Enrolled Date": "",
                    "Mobile Number": "",
                    "State": "",
                    "City": ""
                };
                temp["User Id"] = this.programparticipantsArr[i].userId;
                temp["User Name"] = this.programparticipantsArr[i].userName;
                temp["User Type"] = this.programparticipantsArr[i].userType;
                temp["Role Name"] = this.programparticipantsArr[i].roleName;
                temp["Enrolled Date"] = this.programparticipantsArr[i].enrolledDate;
                if (this.programparticipantsArr[i].mobileNumber == "" || this.programparticipantsArr[i].mobileNumber == undefined) {
                    temp["Mobile Number"] = "";
                } else {
                    temp["Mobile Number"] = this.programparticipantsArr[i].mobileNumber;

                }
                temp["State"] = this.programparticipantsArr[i].city;
                temp["City"] = this.programparticipantsArr[i].state;
                this.printingArray.push(temp);
            }
            var fileName: string;
            if (this.role == this.stringService.getStaticContents().jswDistributorProgramRole) {
                fileName = "Distributor Report";
            }
            else if (this.role == this.stringService.getStaticContents().jswEngineerProgramRole) {
                fileName = "Engineer Report";
            }
            else if (this.role == this.stringService.getStaticContents().jswDealerProgramRole) {
                fileName = "Dealer Report";
            }


            if(this.displaypc){
                this._csvService.download(this.printingArray, fileName);
            }else{
    
                console.log("Array",this.printingArray);
                // this._csvService.download(this.printingArray, "Lead_Report");
                this.csv = this._csvService.ConvertToCSV(this.printingArray);
                console.log("csv file RRR",this.csv);
                this.filePath = "Programs-Data" + "/" + "Lead" + "/" + "lead";
                console.log("filepath Rrr",this.csv);
                this.s3UploadService.formParams(this.csv,this.filePath);
                console.log("here");
                this.s3UploadService.uploadFiles();
                console.log("after s3 upload");
                this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
                    if(value.length > 0){
                        this.showLoader = false;
                        console.log("in subscribe",value[0].Location);
                        this.report = value[0].Location;
                        console.log("report =",this.report);
                    }
                });
            }
        }
    };

    downloadLeadReport() {

        var obj = {
            providedby: this.configService.getloggedInProgramUser().programUserId,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            frontendUserInfo: this.configService.getFrontEndUserInfo()
        }
        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            delete obj.providedby
        };
        this.showLoader = true;
        this.leadService.getLeadReport(obj).subscribe(
            (responseObject) => {
                console.log("response of getLead",responseObject.result);
                if(responseObject.result == null || responseObject.result.length == 0){
                    if(this.displaypc){
                        this.feedbackMessageComponent.updateMessage(true, "Sorry!! No leads found", "alert-danger");    
                    }else{
                        this.leadNotFound = true;
                    }
                }
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        console.log("channel Info in home component", responseObject);
                        this.leadslist = responseObject.result;
                        this.generateLeadCsv();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.showLoader = false;

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
        );
    };

    generateLeadCsv() {
        this.printingArray = [];
        this.keyContactsArray = [];
        this.tempArray = [];
        this.showLoader = true;
        if (this.leadslist !== null || this.leadslist !== undefined) {
            this.headerArray = [
                "Sr. No.",
                "projectname",
                "ownername",
                "projecttype",
                "expectedvalue",
                "address",
                "city",
                "state",
                "pincode",
                "keycontacts",
                "providedby",
                "expectedSalesPeriod",
                "providedbyRole",
                "userName",
                "distributor_Name",
                "ASM_Name",
                "points_earned",
                "qualified",
                "status",
                "createdAt",
            ];

            console.log("AJJJJJJJJJJJJJJJJJJJJJJJJJ-----------,this.leadslist.length", this.leadslist.length);
            for (var i = 0; i < this.leadslist.length; i++) {

                console.log("AJJJJJJJJJJJJJJJJJJJJJJJJJ-----------,i", i);
                if (this.leadslist[i].data.keycontacts) {
                    if (this.leadslist[i].data.keycontacts.length > 0) {
                        for (var j = 0; j < this.leadslist[i].data.keycontacts.length; j++) {
                            this.keyContactsArray = [];
                            this.tempArray = [];
                            this.tempArray1 = [];
                            var temp: any = {}
                            if (j === 0) {
                                temp["Sr. No."] = i + 1;
                                temp["Project Name"] = this.leadslist[i].data.projectname;
                                temp["Owner Name"] = this.leadslist[i].data.ownername;
                                temp["Project Type"] = this.leadslist[i].data.projecttype;
                                if (this.leadslist[i].data.expectedtonnage !== undefined
                                    && this.leadslist[i].data.expectedtonnage !== ""
                                    && this.leadslist[i].data.expectedtonnage !== null) {
                                    temp["Expected Tonnage"] = this.leadslist[i].data.expectedtonnage;
                                } else {
                                    temp["Expected Tonnage"] = 0
                                }

                                temp["Address"] = this.leadslist[i].data.address;
                                temp["City"] = this.leadslist[i].data.city;
                                temp["State"] = this.leadslist[i].data.state;
                                temp["Pin Code"] = this.leadslist[i].data.pincode;
                                // temp.key_contacts = this.leadslist[i].data.keycontacts;
                                // temp.provided_by = this.leadslist[i].data.providedby;
                                // temp.expectedSalesPeriod = this.leadslist[i].data.expectedSalesPeriod;
                                if (this.leadslist[i].data.expectedSalesPeriod !== undefined
                                    && this.leadslist[i].data.expectedSalesPeriod !== ""
                                    && this.leadslist[i].data.expectedSalesPeriod !== null) {
                                    temp["Expected Sales Period"] = this.leadslist[i].data.expectedSalesPeriod;
                                } else {
                                    temp["Expected Sales Period"] = "N.A."
                                }
                                // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
                                temp["User Name"] = this.leadslist[i].userName;
                                // temp.distributor_Id = this.leadslist[i].distributorId;
                                temp["Distributor Name"] = this.leadslist[i].distributorName;
                                // temp.ASM_Id = this.leadslist[i].ASMId;
                                temp["ASM Name"] = this.leadslist[i].ASMName;
                                if (this.leadslist[i].data.pointsEarned !== undefined
                                    && this.leadslist[i].data.pointsEarned !== ""
                                    && this.leadslist[i].data.pointsEarned !== null) {
                                    temp["Points Earned"] = this.leadslist[i].data.pointsEarned;
                                } else {
                                    temp["Points Earned"] = 0
                                }
                                // temp.qualified = this.leadslist[i].data.qualified;

                                // temp.program_Id = this.leadslist[i].data.programId;
                                // temp.client_Id = this.leadslist[i].data.clientId;
                                // temp.lead_Id = this.leadslist[i].data.leadId;
                                temp["Lead Added Date"] = this.leadslist[i].data.createdAt.slice(0, 10);
                                // temp.lead_updated_Date = this.leadslist[i].data.updatedAt.slice(0, 10);
                                // temp.key_contacts = this.tempArray1;
                                if (this.leadslist[i].data.qualified === true) {
                                    temp["Status"] = "Qualified";
                                } else if (this.leadslist[i].data.rejected === true) {
                                    temp["Status"] = "Rejected";
                                } else if (this.leadslist[i].data.pending === true) {
                                    temp["Status"] = "Pending";
                                } else {
                                    temp["Status"] = "";
                                }
                            } else {
                                temp["Sr. No."] = "";
                                temp["Project Name"] = ""
                                temp["Owner Name"] = "";
                                temp["Project Type"] = "";
                                temp["Expected Tonnage"] = "";
                                temp["Address"] = "";
                                temp["City"] = "";
                                temp["State"] = "";
                                temp["Pin Code"] = "";
                                // temp.key_contacts = this.leadslist[i].data.keycontacts;
                                // temp.provided_by = "";
                                temp["Expected Sales Period"] = "",
                                    // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
                                    temp["User Name"] = "";
                                // temp.distributor_Id = "";
                                temp["Distributor Name"] = "";
                                // temp.ASM_Id = "";
                                temp["ASM Name"] = "";
                                temp["Points Earned"] = "";
                                // temp.qualified = this.leadslist[i].data.qualified;

                                // temp.program_Id = "";
                                // temp.client_Id = "";
                                // temp.lead_Id = "";
                                temp["Lead Added Date"] = "";
                                // temp.lead_updated_Date = "";
                                // temp.key_contacts = this.tempArray1;
                                temp["Status"] = "";
                            }
                            if (this.leadslist[i].data.keycontacts[j].keyPersonName) {
                                temp["Key Contact Name"] = this.leadslist[i].data.keycontacts[j].keyPersonName;
                            } else {
                                temp["Key Contact Name"] = "";
                            }

                            if (this.leadslist[i].data.keycontacts[j].keyPersonRole) {
                                temp["Key Contact Role"] = this.leadslist[i].data.keycontacts[j].keyPersonRole;
                            } else {
                                temp["Key Contact Role"] = "";
                            }

                            if (this.leadslist[i].data.keycontacts[j].keyPersonContact) {
                                temp["Key Contact Mobile No"] = this.leadslist[i].data.keycontacts[j].keyPersonContact;
                            } else {
                                temp["Key Contact Mobile No"] = "";
                            }
                            console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
                            this.printingArray.push(temp);
                            console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
                        }
                    }
                    else {
                        this.formLeadObject(i);
                    }
                } else {
                    this.formLeadObject(i);
                }


            };
        }
        if(this.displaypc){
            this._csvService.download(this.printingArray, "Lead_Report");
            this.showLoader = false;
        }else{

            console.log("Array",this.printingArray);
            // this._csvService.download(this.printingArray, "Lead_Report");
            this.csv = this._csvService.ConvertToCSV(this.printingArray);
            console.log("csv file RRR",this.csv);
            this.filePath = "Programs-Data" + "/" + "Lead" + "/" + "lead";
            console.log("filepath Rrr",this.csv);
            this.s3UploadService.formParams(this.csv,this.filePath);
            console.log("here");
            this.s3UploadService.uploadFiles();
            console.log("after s3 upload");
            this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
                if(value.length > 0){
                    this.showLoader = false;
                    console.log("in subscribe",value[0].Location);
                    this.report = value[0].Location;
                    console.log("report =",this.report);
                }
            });
        }

    }

    onDeviceReady() {
        // Now safe to use device APIs
        // this.showLoader = true;
        if(this.leadNotFound){
            this.feedbackMessageComponent.updateMessage(true, "Sorry!! No leads found", "alert-danger");    
        }
        console.log("report =",this.report);
        console.log("______________________------------------------------------------");
        var uri = encodeURI(this.report);
        //var fileURL =  "///storage/emulated/0/DCIM/myFile";
        var filepath;
        var options = {};
        var trustHosts = true;
        var pqr = false;
        var abc;
        // this.showPopUp = true;

        var fileURL= cordova.file.externalApplicationStorageDirectory + "LeadReport.csv";  // for Android
        // var fileURL= cordova.file.syncedDataDirectory + "LeadReport.csv";   // for IOS
        // var fileURL= "///storage/emulated/0/DCIM/test.csv";
        // var fileURL = "file:///android_asset/www/assets/test.csv";
        console.log("his.filename",fileURL);
        var ft;
        
        var permissions =  cordova.plugins.permissions;

        // function onDeviceReady() {
            console.log(FileTransfer);
            ft = new FileTransfer();
            permissions.hasPermission(permissions.WRITE_USER_DICTIONARY, function(res){
                if(res.hasPermission){
                    console.log("got write permission");
                    pqr = true;
                    ft.download(
                        uri,
                        fileURL,
                        function(entry) { 
                            
                            // this.showLoader = false;
                            // this.downloadPath = fileURL;
                            // this.showPopUp = true;
                            console.log("in entry function");
                            // pqr = true;
                            console.log("download complete: " + entry.toURL());
                            abc =  entry.toURL();
                            cordova.plugins.fileOpener2.open(
                                abc, // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
                               'text/csv', 
                               { 
                                   error : function(e) { 
                                       console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
                                   },
                                   success : function () {
                                       console.log('file opened successfully'); 				
                                   }
                               }
                            );
                            // this.showLoader = false;
                            // this.downloadPath = fileURL;
                            // this.showPopUp = true;
                        },
                        function(error) {
                            console.log("in error function");
                            
                            console.log("download error source " + error.source);
                            console.log("download error target " + error.target);
                            console.log("download error code" + error.code);
                            console.log("download error code" + error.body);
                        },
                        true,
                      
                    );
                }else{
                    console.log("no permission to download");
                    permissions.requestPermission(permissions.WRITE_USER_DICTIONARY, success,failure);

                }
                function success(){ console.log("permission received");
                pqr = true;
                console.log("got write permission next time");
                ft.download(
                    uri,
                    fileURL,
                    function(entry) { 
                        // this.downloadPath = fileURL;
                        // this.showPopUp = true;
                        console.log("in entry function");
                        // pqr = true;
                        console.log("download complete: " + entry.toURL());
                        abc =  entry.toURL();
                        cordova.plugins.fileOpener2.open(
                            abc, // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
                           'text/csv', 
                           { 
                               error : function(e) { 
                                   console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
                               },
                               success : function () {
                                   console.log('file opened successfully'); 				
                               }
                           }
                        );
                    },
                    function(error) {
                        console.log("in error function");
                        console.log("download error source " + error.source);
                        console.log("download error target " + error.target);
                        console.log("download error code" + error.code);
                        console.log("download error code" + error.body);
                    },
                    true,
                    // {
                    //     headers: {
                    //         "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                    //     }
                    // });
                );};
                function failure(){console.log("fail to get permssions")};
            });
            console.log("in final after download");
            // this.showPopUp = true;
            // this.openFileMobile();
            // this.feedbackMessageComponent.updateMessage(true, "File Downloaded success fully on" + fileURL + "Path", "alert-success");

    }


    // ++++++++++++++++++++++++++++++
    
    // openFileMobile() {
    //     this.showPopUp = false;
    //     console.log("in file open method");
    //     cordova.plugins.fileOpener2.open(
    //         'file:///data/user/0/in.jswprivilegeclub.mobileApp/files/LeadReport.csv', // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
    //         'application/csv', 
    //         { 
    //             error : function(e) { 
    //                 console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
    //             },
    //             success : function () {
    //                 console.log('file opened successfully'); 				
    //             }
    //         }
    //     );
    // }

    // closePopUp(){
    //     this.showPopUp = false;
    // }
 
    
    // ++++++++++++++++++++++++++++++


    formLeadObject(i) {
        var temp: any = {};
        temp["Sr. No."] = i + 1;
        temp["Project Name"] = this.leadslist[i].data.projectname;
        temp["Owner Name"] = this.leadslist[i].data.ownername;
        temp["Project Type"] = this.leadslist[i].data.projecttype;
        // temp.expected_tonnage = this.leadslist[i].data.expectedtonnage;
        if (this.leadslist[i].data.expectedtonnage !== undefined
            && this.leadslist[i].data.expectedtonnage !== ""
            && this.leadslist[i].data.expectedtonnage !== null) {
            temp["Expected Tonnage"] = this.leadslist[i].data.expectedtonnage;
        } else {
            temp["Expected Tonnage"] = 0
        }
        temp["Address"] = this.leadslist[i].data.address;
        temp["City"] = this.leadslist[i].data.city;
        temp["State"] = this.leadslist[i].data.state;
        temp["Pin Code"] = this.leadslist[i].data.pincode;

        // temp.provided_by = this.leadslist[i].data.providedby;
        // temp.expectedSalesPeriod = this.leadslist[i].data.expectedSalesPeriod;
        if (this.leadslist[i].data.expectedSalesPeriod !== undefined
            && this.leadslist[i].data.expectedSalesPeriod !== ""
            && this.leadslist[i].data.expectedSalesPeriod !== null) {
            temp["Expected Sales Period"] = this.leadslist[i].data.expectedSalesPeriod;
        } else {
            temp["Expected Sales Period"] = "N.A."
        }
        // temp.provided_by_Role_Name = this.leadslist[i].data.providedbyRole;
        temp["User Name"] = this.leadslist[i].userName;
        // temp.distributor_Id = this.leadslist[i].distributorId;
        temp["Distributor Name"] = this.leadslist[i].distributorName;
        // temp.ASM_Id = this.leadslist[i].ASMId;
        temp["ASM Name"] = this.leadslist[i].ASMName;
        if (this.leadslist[i].data.pointsEarned !== undefined
            && this.leadslist[i].data.pointsEarned !== ""
            && this.leadslist[i].data.pointsEarned !== null) {
            temp["Points Earned"] = this.leadslist[i].data.pointsEarned;
        } else {
            temp["Points Earned"] = 0
        }
        // temp.qualified = this.leadslist[i].data.qualified;

        // temp.program_Id = this.leadslist[i].data.programId;
        // temp.client_Id = this.leadslist[i].data.clientId;
        // temp.lead_Id = this.leadslist[i].data.leadId;
        temp["Lead Added Date"] = this.leadslist[i].data.createdAt.slice(0, 10);
        // temp.lead_updated_Date = this.leadslist[i].data.updatedAt.slice(0, 10);
        // temp.key_contacts = this.tempArray1;
        if (this.leadslist[i].data.qualified === true) {
            temp["Status"] = "Qualified";
        }
        if (this.leadslist[i].data.rejected === true) {
            temp["Status"] = "Rejected";
        }
        if (this.leadslist[i].data.pending === true) {
            temp["Status"] = "Pending";
        }
        temp["Key Contact Name"] = "";
        temp["Key Contact Role"] = "";
        temp["Key Contact Mobile No"] = "";
        console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
        this.printingArray.push(temp);
        console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ this.printingArray11", i);
    }

    TestMeth() {
        console.log("INSIDE REPORT COMPONENT");
    }
    
}
