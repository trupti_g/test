/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from './home.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";

@Component({
	templateUrl: './privileges.component.html',
	styleUrls: ['./privileges.component.scss', '../app.component.scss']
})
export class PrivilegesComponent implements OnInit, OnDestroy {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	programRoleInfo: any;
	programUserInfo: any;
	showDivPopUp6: boolean;
	showDivPopUp5: boolean;
	showDivPopUp3: boolean;
	showDivPopUp4: boolean;
	showDivPopUp2: boolean;
	showDivPopUp1: boolean;


	public selectedTab: string;
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private productDetailsService: ProductDetailsService,
		private rewardsGalleryService: GlobalRewardGalleryService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private homeService: HomeService,
		public sidebar: SidebarComponent,
		private stringService: StringService
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}
		this.selectedTab = 'Activity';
		this.programUserInfo = configService.getloggedInProgramUser();

		let programUser = this.programUserInfo;

		if (this.programUserInfo) {
			this.programRoleInfo = programUser.programRoleInfo;
			console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
		}

	}

	showPopup1() {
		this.showDivPopUp1 = true;
	}
	showPopup2() {
		this.showDivPopUp2 = true;
	}
	showPopup3() {
		this.showDivPopUp3 = true;
	}
	showPopup4() {
		this.showDivPopUp4 = true;
	}
	showPopup5() {
		this.showDivPopUp5 = true;
	}
	showPopup6() {
		this.showDivPopUp6 = true;
	}




	closeDetails() {
		if (this.showDivPopUp1 === true) {
			this.showDivPopUp1 = false;
		}
		if (this.showDivPopUp2 === true) {
			this.showDivPopUp2 = false;
		}
		if (this.showDivPopUp3 === true) {
			this.showDivPopUp3 = false;
		}
		if (this.showDivPopUp4 === true) {
			this.showDivPopUp4 = false;
		}
		if (this.showDivPopUp5 === true) {
			this.showDivPopUp5 = false;
		}
		if (this.showDivPopUp6 === true) {
			this.showDivPopUp6 = false;
		}
	}

	ngOnInit() {
		window.scrollTo(0, 0);
		this.sidebar.close();
	}

	ngOnDestroy() {

	}


}
