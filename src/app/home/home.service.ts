import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { StringService } from "../shared-services/strings.service";

@Injectable()

export class HomeService {
    public subject = new Subject<any>();
    private clientId: any;
    private programId: any;
    private userInfo;
    private options;
    private headers;
    constructor(private http: Http,
        private configService: ConfigService,
        private stringService: StringService, ) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        this.options = new RequestOptions({ headers: this.headers });


        console.log("this.programId", this.configService.getloggedInProgramInfo());
        this.userInfo = this.configService.getLoggedInUserInfo();
    }


    addOrder(requestObject, isProgramPointsToRedeem): any {
        let recordInfo: any = {};
        let frontendUserInfo: any = {};
        console.log("this.configService.getloggedInProgramUser()", this.configService.getloggedInProgramUser());
        if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") requestObject.userType = "BE";
        else if (this.configService.getloggedInProgramUser().userType === "ClientUser") requestObject.userType = "Client";

        recordInfo = requestObject;
        let requestO: any = {};
        requestO.frontendUserInfo = this.configService.getFrontEndUserInfo();
        requestO.recordInfo = recordInfo;
        requestO.isProgramPointsToRedeem = isProgramPointsToRedeem;
        delete requestO.frontendUserInfo.appType;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestO;


        let url = this.configService.getApiUrls().getAllOrders;
        console.log("reqObj", reqObj);
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });


    }

    addPointConversionRequest(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("RequestObject in Add Order Service", requestObject);
        let reqObj = requestObject;

        if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") reqObj.userType = "BE";
        else if (this.configService.getloggedInProgramUser().userType === "ClientUser") reqObj.userType = "Client";


        let url = this.configService.getApiUrls().addPointConversionRequest;
        console.log("reqObj", reqObj);
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getPointConversionRequest(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("RequestObject in Add Order Service", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getPointConversionRequest;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getAllStarPointConfiguration(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("REQUEST OBJECT: ", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getAllStarPointConfiguration;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("INSIDE MAP : ", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("INSIDE CATCH");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getAllProgramFromMaster(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("REQUEST OBJECT: ", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getPrograms;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("INSIDE MAP : ", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("INSIDE CATCH");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    getAllClient(requestObject): any {

        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        if (requestObject.userId) { requestObject.frontendUserInfo.userId = requestObject.userId; }
        if (requestObject.programId) { requestObject.frontendUserInfo.programId = requestObject.programId; }

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("RequestObject in Add Order Service", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getAllClient;
        console.log("URLLLL", url);
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("error", error);
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getBusinessEntity(requestObject: any): any {
        let url = this.configService.getApiUrls().getBusinessEntity;

        //get frontenduserinfo
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            requestObject.frontendUserInfo = frontEndInfo;
        } else {
            requestObject.frontendUserInfo = {};
        }

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url, requestObject, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("error", error);
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    getClientUser(reqobj): any {
        reqobj.serviceType = "client";
        reqobj.appType = "webApp";
        reqobj.clientId = this.configService.getprogramInfo().clientId;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            reqobj.frontendUserInfo = frontEndInfo;
        } else {
            reqobj.frontendUserInfo = {};
        }
        console.log("this.configService.getloggedInProgramUser()", this.configService.getloggedInProgramUser());
        reqobj.clientUserId = this.configService.getloggedInProgramUser().programUserId;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getClientUser;

        return this.http.post(url, reqobj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }








    getTodaysBirthdayCount(reqobj): any {

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getTodaysBirthdayCount;


        return this.http.post(url, reqobj, options)
            .map((res: Response) => {
                console.log("birthday count RRRRRRRR",res.json())
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }


    getTodaysBirthday(reqobj): any {
        console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR home service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getTodaysBirthday;

        console.log("url rrrrrrrrrrrrrrrrrrrrrrrrrrrr", url);

        return this.http.post(url, reqobj, options)
            .map((res: Response) => {
                console.log("response in home service RRRRRR",res.json());
                return res.json();
               
            
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    };



    getProgramParticipantsReport(requestObject): any {
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            requestObject.frontendUserInfo = frontEndInfo;
        } else {
            requestObject.frontendUserInfo = {};
        }
        requestObject.frontendUserInfo = frontEndInfo;
        requestObject.serviceType = "programSetup";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getProgramParticipantsReport;

        return this
            .http
            .post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    };

    /***************************************** START OF LOGIN REPORT ******************************************/
    getProgramLoginReport(requestObject): any {
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        requestObject.serviceType = "programSetup";
        requestObject.clientId = this.stringService.getStaticContents().jswClientId;
        let url = this.configService.getApiUrls().getProgramLoginReport;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }
    /***************************************** END OF LOGIN REPORT ******************************************/



    getUserNotifications(reqobj): any {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getUserNotifications;

        console.log("get User Notfications URL", url);



        return this.http.post(url, reqobj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    };


    addUserNotifications(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().addUserNotifications;

        console.log("Add User Notification URL", url);

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };


    updateUserNotifications(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().updateUserNotifications;

        console.log("Update User Notification URL", url);

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };


    uploadLeaderboardData(requestObject): any {

        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().uploadLeaderboardData;

        console.log("url", url);

        return this.http.post(url, reqObj, options).timeout(9000000)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || "Server error");
            });
    }




    uploadHallOfFameData(requestObject): any {

        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().uploadHallOfFameData;

        console.log("url", url);

        return this.http.post(url, reqObj, options).timeout(9000000)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || "Server error");
            });
    }


}