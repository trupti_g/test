/*
	Author			:	Trupti Ghude
	Description		: 	Ask An Expert
	Date Created	: 	05 july 2018
	Date Modified	: 	05 july 2018
*/ 

import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";
import { AskAnExpertService } from "./ask-an-expert.service";
import { identifierModuleUrl } from "@angular/compiler";
import { Observable } from "rxjs"; 
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({

    templateUrl: './ask-an-expert.component.html',
    styleUrls: ['./ask-an-expert.component.scss'],
    providers: [Device]
})
export class AskAnExpertComponent  {

    mobilewidth: any;                     //to capture screen width
    displaypc: boolean = true;            //to capture screen width
    displaymobile: boolean = false;       //to capture screen width

    private answerForm: FormGroup;
    public allQuestions: any[] = [];
    public allQuestionsId: any[] = [];
    public allAnswers: any[] = [];
    public allAnswerQuestion: any[] = [];
    public QuestionIndex: any;
    public questionAsked: any[] = [];
    public loggedInUserName: String;
    public questionAskedBy: any[] = [];
    public questionUpdatedAtDateTime: any[] = [];
    public answeredAtDateTime: any[] = [];
    public questionCityState: any[] = [];
    public answerQuestionAskedBy: any[] = [];
    public answerQuestionCityState: any[] = [];
    public answerQuestionAtDateTime: any[] = [];


    public questionIdArray: any[] = [];
    public allQuesAndAns: any[] = [];
    public questionsRepliedArray : any[] = [];

    public tempQuesArr=[];
    public tempAnsArr=[];

    public programUserInfo;
    public loggedInUserId;

    public isEngineer: boolean = false;
    public userNameSearching: boolean;
    public searchingParent: boolean;
    public searchParentFailed: boolean;
    public engineerObj: any = {};
    public usersMapForm: FormGroup;

    public usersArr: any[];
    public showForm= [];

    public isReadOnly: boolean = false;
    public unanswered: boolean = false;

    showLoader: boolean;
    noRecords: boolean;
    noRecordsAns: boolean;

    public selectedQuestion: any;

    public questionsArray: any[] = [];
    public answeresArray: any[] = [];

    public startRecord: number;
    public endRecord: number;
    public skip: number;
    public limit: number=5;
    public pageCount: number=1;
    public serialSkip: number;
    public isPrevious: boolean;
    public isNext: boolean;
    public totalRecords: number;

    public startRecordAns: number;
    public endRecordAns: number;
    public skipAns: number;
    public limitAns: number=5;
    public pageCountAns: number=1;
    public serialSkipAns: number;
    public isPreviousAns: boolean;
    public isNextAns: boolean;
    public totalRecordsAns: number;
    

    
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private zone: NgZone,
        private activatedRoute: ActivatedRoute,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private route: ActivatedRoute,
        private http: Http,
        public sidebar: SidebarComponent,
        private stringService: StringService,
        private askanexpertService: AskAnExpertService,
        private device: Device,
    ) {
        
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
            console.log("display mobile val", this.displaymobile);
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
    }

    
	ngOnInit() { 

        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("AskAnExpert")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "AskAnExpert");
                    ga('send', 'pageview');
                }
            });
        }
      
        
        this.sidebar.close();
        this.programUserInfo = this.configService.getloggedInProgramUser();
        this.loggedInUserName = this.configService.getLoggedInUserInfo().firstName +
        " " +  this.configService.getLoggedInUserInfo().lastName;
        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole){
            this.isEngineer = true;
            this.loggedInUserId = this.configService.getLoggedInUserInfo().businessId[0];
        }else{
            this.loggedInUserId = this.configService.getLoggedInUserInfo().clientUserId;
        }

       
        this.initanswerForm();
        this.initUsersForm() ;
        this.getForumQuestions();
        // this.getAnswer();

     

        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole 
        || this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole 
        || this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole){
            this.isReadOnly=true;
        }
        console.log("in oninit loggedInUserId",this.loggedInUserId);
        // this.valueChanges();
    }

    initanswerForm() {
        this.answerForm = this.formBuilder.group({
            answer: ["", [Validators.required]],
    });
    }

    initUsersForm() {
        this.usersMapForm = this.formBuilder.group({
            parentUserId: ["", [Validators.required]],
            engineers: ["", [Validators.required]],
    });
    }

     //To track changes in UsersMap form
     valueChanges() {
        this.usersMapForm.valueChanges.subscribe(data => {
            console.log("DATA---------------------", data);

            if (data.engineerId !== null && data.engineerId !== "") {
                console.log("this.selectedQuestion",this.selectedQuestion);
                if(this.usersArr.indexOf(data.engineerId)!=-1){    
                    this.usersArr.push(data.engineerId)
                    this.updateForumQuestions(this.selectedQuestion);
               }else{
                    console.log("Engineer already exists");
               }
            }
        });
    }

    assignAnswerForm(){
        this.configService.setModalClose(true);
        this.answerForm.patchValue({
            "answer": ""
        });
        console.log("this.QuestionIndex",this.QuestionIndex);
        var questionId=0;
        if(this.questionsArray!== undefined && this.QuestionIndex!== undefined){
            if(this.questionsArray[this.QuestionIndex] !== undefined && this.questionsArray[this.QuestionIndex].questionId !== undefined){
                console.log("before this.questionsArray[this.QuestionIndex].questionId",this.questionsArray[this.QuestionIndex].questionId);
                questionId= Number((this.questionsArray[this.QuestionIndex].questionId).substring(4));
                console.log("after questionId",questionId);
            }
        }
        
        console.log("questionId",questionId);

        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "closeAnswerClick", this.loggedInUserId);
    }
    
            
    getForumQuestions(){
        console.log("getForumQuestions");
        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole){
            var id = this.configService.getLoggedInUserInfo().businessId;
            console.log("ID: " + id);
            var obj = {
                engineerId: id.toString(),
                skip: 0
            };identifierModuleUrl
        }else{
            var obj = {
                engineerId: this.programUserInfo.programUserId,
                skip: 0
            };
            delete obj.engineerId;
        }
        this.showLoader = true;
        this.askanexpertService.getForumQuestions(obj).subscribe(responseObject => {
            console.log("DATA---------------------", responseObject);

            // if (responseObject.result !== null && responseObject.result !== "") {
            //     for (var i = 0; i < responseObject.result.length; i++) {      
            //             this.allQuestions.push(responseObject.result[i].question.replace(/(<([^>]+)>)/ig, ""));
            //             this.allQuestionsId.push(responseObject.result[i].questionId);
            //             this.questionAskedBy.push(responseObject.result[i].frontendUserInfo.name);
            //             this.questionUpdatedAtDateTime.push(responseObject.result[i].createdAt.split("T")[0]  + ", " + responseObject.result[i].createdAt.slice(11, -5).split('.'));
            //             this.questionCityState.push(responseObject.result[i].city + "," + " " + responseObject.result[i].state);
            //     } 
            //     this.allQuestions.reverse();
            //     this.allQuestionsId.reverse();
            //     this.questionAskedBy.reverse();
            //     this.questionUpdatedAtDateTime.reverse(); 
            //     this.questionCityState.reverse();  
            // }  
     
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;
                case responseCodes.RESP_SERVER_ERROR:
                    break;
                case responseCodes.RESP_SUCCESS:
                    console.log("form questions arr");
                    for (var i = 0; i < responseObject.result.length; i++) { 
                        this.questionIdArray.push(responseObject.result[i].questionId);
                        responseObject.result[i].answers=[];
                        this.tempQuesArr.push(responseObject.result[i]);
                        console.log("responseObject.result[i]",responseObject.result[i]);
                    }
                
                    console.log("this.tempQuesArr",this.tempQuesArr);
                    console.log("this.questionIdArray",this.questionIdArray);
                    this.getAnswer();
                    break;
                case responseCodes.RESP_AUTH_FAIL:
                    break;
                case responseCodes.RESP_FAIL:
                    this.noRecords=true;
                    break;
                case responseCodes.RESP_ALREADY_EXIST:
                    break;
                case responseCodes.RESP_KYC_AWAITING:
                    break;
                case responseCodes.RESP_BLOCKED:
                    break;
            }
        },
        err => {
            this.showLoader = false;
        }
        ); 
            
    }
    
    
    addAnswer(){
        var questionId=0;
        if(this.questionsArray && this.QuestionIndex){
            if(this.questionsArray[this.QuestionIndex] !== undefined && this.questionsArray[this.QuestionIndex].questionId !== undefined){
                console.log("before this.questionsArray[this.QuestionIndex].questionId",this.questionsArray[this.QuestionIndex].questionId);
                questionId= Number((this.questionsArray[this.QuestionIndex].questionId).substring(4));
                console.log("after questionId",questionId);
            }
        }
        
        console.log("questionId",questionId);

        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "postAnswer", this.loggedInUserId);
        let formBodyAnswer: string = this.answerForm.value.answer as string;
        var obj = {
            answer: formBodyAnswer,
            userId: this.loggedInUserId,
            clientId: this.configService.getprogramInfo().clientId,
            questionId: this.questionsArray[this.QuestionIndex].questionId,
            programId: this.configService.getprogramInfo().programId,
            createdBy: this.loggedInUserId
        };
        console.log("this.answeresArray[this.QuestionIndex].questionId",this.questionsArray[this.QuestionIndex].questionId);
        this.showLoader = true;
        this.askanexpertService.addAnswer(obj).subscribe(responseObject => {
            console.log("DATA---------------------", responseObject);
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;
                case responseCodes.RESP_SERVER_ERROR:
                    break;
                case responseCodes.RESP_SUCCESS:
                    this.feedbackMessageComponent.updateMessage(true, "Answer added Successfully", "alert-success");
                    window.location.reload();
                    // this.getForumQuestions();
                    break;
                case responseCodes.RESP_AUTH_FAIL:
                    break;
                case responseCodes.RESP_FAIL:
                this.noRecords=true;
                    break;
                case responseCodes.RESP_ALREADY_EXIST:
                    break;
                case responseCodes.RESP_KYC_AWAITING:
                    break;
                case responseCodes.RESP_BLOCKED:
                    break;
            }
        },
        err => {
            this.showLoader = false;
        }
        ); 
    }

    answerClick(index)
    {  
        this.configService.setModalClose(false);
        this.QuestionIndex = index;
        var questionId=0;
        if(this.questionsArray && this.QuestionIndex){
            if(this.questionsArray[this.QuestionIndex] !== undefined && this.questionsArray[this.QuestionIndex].questionId !== undefined){
                console.log("before this.questionsArray[this.QuestionIndex].questionId",this.questionsArray[this.QuestionIndex].questionId);
                questionId= Number((this.questionsArray[this.QuestionIndex].questionId).substring(4));
                console.log("after questionId",questionId);
            }
        }
        
        console.log("questionId",questionId);

        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "addAnswerClick", this.loggedInUserId);
    }

    getAnswer(){
        // var obj = {
        //     engineerId: this.configService.getLoggedInUserInfo().businessId,
        //     skip: 0,
        //     limit: 20
        // }; 

        var obj = {
            questionIdArray: this.questionIdArray,
            skip: 0
        }; 
        this.showLoader = true;
        this.askanexpertService.getAnswer(obj).subscribe(responseObject => {
            console.log("DATA---------------------", responseObject);

            // if (responseObject.result !== null && responseObject.result !== "") {
            //     for (var i = 0; i < responseObject.result.length; i++) {                
            //         this.allAnswers[i]=responseObject.result[i].answer.replace(/(<([^>]+)>)/ig, "");
            //         this.allAnswerQuestion[i]=responseObject.result[i].questionInfo.question.replace(/(<([^>]+)>)/ig, "");
            //         this.answeredAtDateTime.push(responseObject.result[i].createdAt.split("T")[0] + ", " +responseObject.result[i].createdAt.slice(11, -5).split('.'));
            //         this.answerQuestionAskedBy.push(responseObject.result[i].questionInfo.frontendUserInfo.name);
            //         this.answerQuestionCityState.push(responseObject.result[i].questionInfo.city + "," + " " + responseObject.result[i].questionInfo.state);
            //         this.answerQuestionAtDateTime.push(responseObject.result[i].questionInfo.createdAt.split("T")[0] + ", " + responseObject.result[i].questionInfo.createdAt.slice(11, -5).split('.'));

            //     }
            //     this.allAnswers.reverse();
            //     this.allAnswerQuestion.reverse();
            //     this.answeredAtDateTime.reverse();
            //     this.answerQuestionAskedBy.reverse();
            //     this.answerQuestionCityState.reverse();
            //     this.answerQuestionAtDateTime.reverse();
            // }
         

            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;
                case responseCodes.RESP_SERVER_ERROR:
                    break;
                case responseCodes.RESP_SUCCESS:

                   this.allQuesAndAns=responseObject.result;
    
                    console.log("to form ALL QUESTIONS ASKED array");
                    console.log("this.tempQuesArr",this.tempQuesArr);
                    console.log("this.allQuesAndAns",this.allQuesAndAns);
                    // to form ALL QUESTIONS ASKED array
                    for(let i=this.tempQuesArr.length-1;i>=0;i--){   //questions array
                        console.log("trupti");
                        console.log("inside this.tempQuesArr[i]",this.tempQuesArr[i]);
                        for(let j=0;j<this.allQuesAndAns.length;j++){    //answers array
                            console.log("this.tempQuesArr[i].questionId",this.tempQuesArr[i].questionId);
                            console.log("this.allQuesAndAns[j].questionId",this.allQuesAndAns[j].questionId);
                            // if question id matches add that answer to temp all questions ASKED array
                            if(this.tempQuesArr[i].questionId===this.allQuesAndAns[j].questionId){ 
                                // if answer belongs to loggedIn user, remove that question from questions asked tab
                                if(this.allQuesAndAns[j].userId === this.loggedInUserId){
                                    console.log("removing question");
                                    var removed=this.tempQuesArr.splice(i, 1);
                                    console.log("removed",removed);
                                    break;
                                }else{
                                    // else add the answer
                                    console.log("answer found");
                                    this.tempQuesArr[i].answers.push(this.allQuesAndAns[j]);
                                }
                            }
                        }
                    }
                    console.log("to form ALL QUESTION REPLIED array");
                    console.log("this.allQuesAndAns",this.allQuesAndAns);
                    // to form ALL QUESTION REPLIED array
                    for(let i=0;i<this.allQuesAndAns.length;i++){    //questions array
                        console.log("this.allQuesAndAns[i].userId",this.allQuesAndAns[i].userId);
                        console.log("this.loggedInUserId",this.loggedInUserId);
                        // if user id matches add that answer to temp all questions REPLIED array
                        if(this.allQuesAndAns[i].userId===this.loggedInUserId){ 
                            this.questionsRepliedArray.push(this.allQuesAndAns[i].questionId);
                            this.allQuesAndAns[i].answers=[];
                            this.allQuesAndAns[i].answers.push(this.allQuesAndAns[i]);
                            this.tempAnsArr.push(this.allQuesAndAns[i]);
                            console.log("this.tempQuesArr[i]",this.allQuesAndAns[i]);
                        }
                    }
                    
                    console.log("this.tempAnsArr",this.tempAnsArr);
                    console.log("this.questionsRepliedArray",this.questionsRepliedArray);
                    console.log("this.allQuesAndAns",this.allQuesAndAns);
                    console.log("to add multiple answrs");
                    // to add answers answered by other users to temp all questions REPLIED array
                    for(let i=0;i<this.allQuesAndAns.length;i++){
                        for(let j=0;j<this.tempAnsArr.length;j++){
                            if(this.allQuesAndAns[i].questionId===this.tempAnsArr[j].questionId && this.allQuesAndAns[i].userId != this.loggedInUserId){
                                this.tempAnsArr[j].answers.push(this.allQuesAndAns[i]);
                            }
                            console.log("this.tempAnsArr[j]",this.tempAnsArr[j]);
                        }
                    }
    
                    console.log("this.tempAnsArr",this.tempAnsArr);
                    console.log("this.tempQuesArr",this.tempQuesArr);
                    console.log("this.tempQuesArr.length",this.tempQuesArr.length);
                    for (var i = 0; i < this.tempQuesArr.length; i++) {
                        console.log("unanswered logic");
                        // logic to flag question if not answered for more than a week
                        var askedDate=new Date(this.tempQuesArr[i].createdAt);
                        var currentDate=new Date();
                        
                        console.log("askedDate",askedDate);
                        console.log("currentDate",currentDate);
                        var isUnanswered=false;
    
                        // check if question is not answred 
                        if(this.tempQuesArr[i].answers){    
                            // if it is not answered check if question asked date is before a week
                        if(this.tempQuesArr[i].answers.length === 0){ 
                            
    
                                var timeDiff = Math.abs(askedDate.getTime() - currentDate.getTime());
                                console.log("timeDiff",timeDiff);
                                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                console.log("diffDays",diffDays); 
    
                                if(diffDays>7){
                                    isUnanswered=true;
                                }
                            }
                        }
    
                        console.log("isUnanswered",isUnanswered);
    
                        console.log("this.tempQuesArr[i].answers",this.tempQuesArr[i].answers); 
                        this.allQuestions[i]={
                            "answers": [],
                            "question" : this.tempQuesArr[i].question.replace(/(<([^>]+)>)/ig, ""),
                            "isUnanswered": isUnanswered,
                            "engineers": this.tempQuesArr[i].engineers,
                            "questionId" : this.tempQuesArr[i].questionId,
                            "userInfo" : this.tempQuesArr[i].userInfo
                        }
    
                        for(let j=0;j<this.tempQuesArr[i].answers.length;j++){
                            var date = new Date(this.tempQuesArr[i].answers[j].createdAt);
                            date.toString();
                            // var dateArr=String(date).split(" ");
                            // var newDate=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];

                            var newDate2 = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

                            var offset = date.getTimezoneOffset() / 60;
                            var hours = date.getHours();

                            newDate2.setHours(hours - offset);
                            var dateArr=String(newDate2).split(" ");
                            var newDate1=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];
                            var tempQues ={
                                "answer": this.tempQuesArr[i].answers[j].answer,
                                "answeredAtDateTime": newDate1,
                                "createdBy":  this.tempQuesArr[i].answers[j].userName
                            }
                            this.allQuestions[i].answers.push(tempQues);
                        } 
                        var askedDate = new Date(this.tempQuesArr[i].createdAt);
                        askedDate.toString();
                        var newDate3 = new Date(askedDate.getTime()+askedDate.getTimezoneOffset()*60*1000);

                        var offset = askedDate.getTimezoneOffset() / 60;
                        var hours = askedDate.getHours();

                        newDate3.setHours(hours - offset);
                        var dateArr1=String(newDate3).split(" ");
                        var newDate4=dateArr1[2] + " " + dateArr1[1] + " " + dateArr1[3] + " " + dateArr1[4];
                        
                        this.allQuestions[i].question=this.tempQuesArr[i].question.replace(/(<([^>]+)>)/ig, ""); 
                        this.allQuestions[i].questionUpdatedAtDateTime = newDate4;
                        this.allQuestions[i].questionCityState = this.tempQuesArr[i].city + "," + " " + this.tempQuesArr[i].state;
                        if(this.tempQuesArr[i].frontendUserInfo){
                            this.allQuestions[i].questionAskedBy = this.tempQuesArr[i].frontendUserInfo.name;
                        }
                        
                        this.allQuestions[i].showForm=false;  
                        this.allQuestionsId.push(this.tempQuesArr[i].questionId);
                       
                      
                        // this.questionUpdatedAtDateTime.push(newDate1);
                        // this.questionCityState.push(this.tempQuesArr[i].city + "," + " " + this.tempQuesArr[i].state);
                    } 
                    console.log("this.allQuestions",this.allQuestions);
                    this.allQuestions.reverse();
                    this.allQuestionsId.reverse();
                    this.questionAskedBy.reverse();
                    this.questionUpdatedAtDateTime.reverse(); 
                    this.questionCityState.reverse();  
                    
                    for (var i = 0; i < this.tempAnsArr.length; i++) { 
                        console.log("this.tempAnsArr[i].answers",this.tempAnsArr[i].answers);  
                        this.allAnswers[i]={
                            "answers": []
                        }
                        for(let j=0;j<this.tempAnsArr[i].answers.length;j++){
                            var date = new Date(this.tempAnsArr[i].answers[j].createdAt);
                            console.log("date",date);
                            date.toString();
                            var newDateANS = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

                            var offset = date.getTimezoneOffset() / 60;
                            var hours = date.getHours();
                        
                            newDateANS.setHours(hours - offset);
                        
                            var dateArr=String(newDateANS).split(" ");
                            console.log("dateArr",dateArr);
                            var newDateANS1=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];
                            console.log("newDate",newDateANS1);
                            var tempAns ={
                                "answer": this.tempAnsArr[i].answers[j].answer,
                                "answeredAtDateTime": newDateANS1 ,
                                "createdBy":  this.tempAnsArr[i].answers[j].userName
                            }
                            console.log("tempAns",tempAns);
                            this.allAnswers[i].answers.push(tempAns);
                        }
                        var date = new Date(this.tempAnsArr[i].questionInfo.createdAt);
                        console.log("date",date);
                        date.toString();

                        var newDateAns = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

                        var offset = date.getTimezoneOffset() / 60;
                        var hours = date.getHours();

                        newDateAns.setHours(hours - offset);

                        var dateArr=String(newDateAns).split(" ");
                        console.log("dateArr",dateArr);
                        var newDateAns1=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];
                    
                        // this.allAnswers[i] = this.tempAnsArr[i].answers;           
                        // this.allAnswers[i]=this.tempAnsArr[i].answer.replace(/(<([^>]+)>)/ig, "");
                        this.allAnswers[i].question=this.tempAnsArr[i].questionInfo.question.replace(/(<([^>]+)>)/ig, "");
                        this.allAnswers[i].answeredAtDateTime=this.tempAnsArr[i].createdAt.split("T")[0] + ", " +this.tempAnsArr[i].createdAt.slice(11, -5).split('.');
                        if(this.tempAnsArr[i].frontendUserInfo){
                            this.allAnswers[i].answerQuestionAskedBy=this.tempAnsArr[i].questionInfo.frontendUserInfo.name;
                        }
                        this.allAnswers[i].answerQuestionCityState=this.tempAnsArr[i].questionInfo.city + "," + " " + this.tempAnsArr[i].questionInfo.state;
                        this.allAnswers[i].answerQuestionAtDateTime=newDateAns1;
    
                    }
                    this.allAnswers.reverse();
                    this.allAnswerQuestion.reverse();
                    this.answeredAtDateTime.reverse();
                    this.answerQuestionAskedBy.reverse();
                    this.answerQuestionCityState.reverse();
                    this.answerQuestionAtDateTime.reverse();

                    // pagination logic
                    // FOR QUESTIONS
                    this.totalRecords = this.allQuestions.length;
                    console.log(" this.totalRecords", this.totalRecords);
                    this.startRecord = 1;
                    if(this.totalRecords >= this.limit){
                        this.endRecord = this.limit
                    }else{
                        this.endRecord = this.totalRecords;
                    }
            
                    if(this.totalRecords>this.limit){
                        this.isNext = true;
                    }

                    // FOR ANSWERS
                    this.totalRecordsAns = this.allAnswers.length;
                    console.log(" this.totalRecords", this.totalRecords);
                    this.startRecordAns = 1;
                    if(this.totalRecordsAns >= this.limitAns){
                        this.endRecordAns = this.limitAns
                    }else{
                        this.endRecordAns = this.totalRecordsAns;
                    }
            
                    if(this.totalRecordsAns>this.limitAns){
                        this.isNextAns = true;
                    }


                    if(this.allQuestions.length===0){
                        this.noRecords=true;
                    }
                    
                    if(this.allAnswers.length===0){
                        this.noRecordsAns=true;
                    }

                    this.formQuestionArray();
                    this.formAnswerArray();
    
                    break;
                case responseCodes.RESP_AUTH_FAIL:
                    break;
                case responseCodes.RESP_FAIL:
                    this.noRecordsAns=true;
                    for (var i = 0; i < this.tempQuesArr.length; i++) {
                        console.log("unanswered logic");
                        // logic to flag question if not answered for more than a week
                        var askedDate=new Date(this.tempQuesArr[i].createdAt);
                        var currentDate=new Date();
                        
                        console.log("askedDate",askedDate);
                        console.log("currentDate",currentDate);
                        var isUnanswered=false;
    
                        // check if question is not answred 
                        if(this.tempQuesArr[i].answers){    
                            // if it is not answered check if question asked date is before a week
                        if(this.tempQuesArr[i].answers.length === 0){ 
                            
    
                                var timeDiff = Math.abs(askedDate.getTime() - currentDate.getTime());
                                console.log("timeDiff",timeDiff);
                                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                console.log("diffDays",diffDays); 
    
                                if(diffDays>7){
                                    isUnanswered=true;
                                }
                            }
                        }
    
                        console.log("isUnanswered",isUnanswered);
    
                        console.log("this.tempQuesArr[i].answers",this.tempQuesArr[i].answers); 
                        this.allQuestions[i]={
                            "answers": [],
                            "question" : this.tempQuesArr[i].question.replace(/(<([^>]+)>)/ig, ""),
                            "isUnanswered": isUnanswered,
                            "engineers": this.tempQuesArr[i].engineers,
                            "questionId" : this.tempQuesArr[i].questionId,
                            "userInfo" : this.tempQuesArr[i].userInfo
                        }
    
                        for(let j=0;j<this.tempQuesArr[i].answers.length;j++){
                            var date = new Date(this.tempQuesArr[i].answers[j].createdAt);
                            date.toString();
                            // var dateArr=String(date).split(" ");
                            // var newDate=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];

                            var newDate2 = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

                            var offset = date.getTimezoneOffset() / 60;
                            var hours = date.getHours();

                            newDate2.setHours(hours - offset);
                            var dateArr=String(newDate2).split(" ");
                            var newDate1=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];
                            var tempQues ={
                                "answer": this.tempQuesArr[i].answers[j].answer,
                                "answeredAtDateTime": newDate1,
                                "createdBy":  this.tempQuesArr[i].answers[j].userName
                            }
                            this.allQuestions[i].answers.push(tempQues);
                        } 
                        var askedDate = new Date(this.tempQuesArr[i].createdAt);
                        askedDate.toString();
                        var newDate3 = new Date(askedDate.getTime()+askedDate.getTimezoneOffset()*60*1000);

                        var offset = askedDate.getTimezoneOffset() / 60;
                        var hours = askedDate.getHours();

                        newDate3.setHours(hours - offset);
                        var dateArr1=String(newDate3).split(" ");
                        var newDate4=dateArr1[2] + " " + dateArr1[1] + " " + dateArr1[3] + " " + dateArr1[4];
                        
                        this.allQuestions[i].question=this.tempQuesArr[i].question.replace(/(<([^>]+)>)/ig, ""); 
                        this.allQuestions[i].questionUpdatedAtDateTime = newDate4;
                        this.allQuestions[i].questionCityState = this.tempQuesArr[i].city + "," + " " + this.tempQuesArr[i].state;
                        if(this.tempQuesArr[i].frontendUserInfo){
                            this.allQuestions[i].questionAskedBy = this.tempQuesArr[i].frontendUserInfo.name;
                        }
                        
                        this.allQuestions[i].showForm=false;  
                        this.allQuestionsId.push(this.tempQuesArr[i].questionId);
                       
                      
                        // this.questionUpdatedAtDateTime.push(newDate1);
                        // this.questionCityState.push(this.tempQuesArr[i].city + "," + " " + this.tempQuesArr[i].state);
                    } 
                    console.log("this.allQuestions",this.allQuestions);
                    this.allQuestions.reverse();
                    this.allQuestionsId.reverse();
                    this.questionAskedBy.reverse();
                    this.questionUpdatedAtDateTime.reverse(); 
                    this.questionCityState.reverse();  
                    // pagination logic
                    // FOR QUESTIONS
                    this.totalRecords = this.allQuestions.length;
                    console.log(" this.totalRecords", this.totalRecords);
                    this.startRecord = 1;
                    if(this.totalRecords >= this.limit){
                        this.endRecord = this.limit
                    }else{
                        this.endRecord = this.totalRecords;
                    }
            
                    if(this.totalRecords>this.limit){
                        this.isNext = true;
                    }
                    
                    this.formQuestionArray();
                    break;
                case responseCodes.RESP_ALREADY_EXIST:
                    break;
                case responseCodes.RESP_KYC_AWAITING:
                    break;
                case responseCodes.RESP_BLOCKED:
                    break;
            }
        },
        err => {
            this.showLoader = false;
        }
        ); 

    }

    formQuestionArray(){
        this.questionsArray=[];
        console.log("this.startRecord",this.startRecord);
        console.log("this.endRecord",this.endRecord);
        console.log("this.pageCount",this.pageCount);

        for(let i=this.startRecord-1;i<this.endRecord;i++){
            console.log("i",i);
            this.allQuestions[i].showForm=false;
            this.questionsArray.push(this.allQuestions[i]);
        }

        // if(this.startRecord===this.endRecord){
        //     for(let i=this.startRecord-1;i<=this.endRecord;i++){
        //         this.questionsArray.push(this.allQuestions[i]);
        //     } 
        // }

        if(this.pageCount===1){
            this.isPrevious=false;
        }

        console.log("this.questionsArray",this.questionsArray);
        this.showLoader=false;
        
    }

    formAnswerArray(){
        this.answeresArray=[];
        console.log("this.startRecord",this.startRecordAns);
        console.log("this.endRecord",this.endRecordAns);
        console.log("this.pageCount",this.pageCountAns);

        for(let i=this.startRecordAns-1;i<this.endRecordAns;i++){
            console.log("i",i);
            this.answeresArray.push(this.allAnswers[i]);
        }


        if(this.pageCountAns===1){
            this.isPreviousAns=false;
        }

        console.log("this.questionsArray",this.questionsArray);
        this.showLoader=false;
        
    }

    onPrevClickAns(){
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "previous", "answers");
        
        this.showLoader=true;
        var tempstarRecord=this.startRecordAns;
        this.endRecordAns = tempstarRecord - 1;
        this.startRecordAns = tempstarRecord - this.limit ;
        
        console.log("onPrevClick");
        console.log("this.endRecordAns ",this.endRecordAns );
        this.isNextAns=true;
        this.pageCountAns--;
       
        this.formAnswerArray();
    }

    onNextClickAns(){
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "next", "answers");
        this.showLoader=true;
        this.startRecordAns = this.endRecordAns +1;
        console.log("this.totalRecordsAns",this.totalRecordsAns);
        var remaining = this.totalRecordsAns - this.pageCountAns * this.limitAns;
        console.log("remaining",remaining);
        if(remaining>this.limitAns){
            this.endRecordAns = this.startRecordAns + this.limitAns - 1;
        }else{
            this.endRecordAns = this.totalRecordsAns
            this.isNextAns = false;
        }
        this.isPreviousAns=true;
        this.pageCountAns++;

        this.formAnswerArray();
    }


    onPrevClick(){
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "previous", "questions");
        this.showLoader=true;
        var tempstarRecord=this.startRecord;
        this.endRecord = tempstarRecord - 1;
        this.startRecord = tempstarRecord - this.limit ;
        
        console.log("onPrevClick");
        console.log("this.endRecord ",this.endRecord );
        this.isNext=true;
        this.pageCount--;
       
        this.formQuestionArray();
    }

    onNextClick(){
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "next", "questions");
        this.showLoader=true;
        this.startRecord = this.endRecord +1;
        console.log("this.totalRecords",this.totalRecords);
        var remaining = this.totalRecords - this.pageCount * this.limit;
        console.log("remaining",remaining);
        if(remaining>this.limit){
            this.endRecord = this.startRecord + this.limit - 1;
        }else{
            this.endRecord = this.totalRecords
            this.isNext = false;
        }
        this.isPrevious=true;
        this.pageCount++;

        this.formQuestionArray();
    }



    /**
     * METHOD   : searchParentUser
     * DESC     : methods to search the string input for the Parent User in Program Users
     *
     */
    searchParentUser = (text$: Observable<string>) =>

        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .do(() => { this.userNameSearching = true })
            .switchMap((term) =>

                this.askanexpertService.searchProgramUsers1(term)
                    .do(() => {
                        this.searchParentFailed = false;
                    })
                    .catch(() => {
                        this.searchParentFailed = true;
                        return Observable.of([]);
                    })
            )
            .do(() => this.searchingParent = false);

    parentNameResultFormatter = (z: { userDetails: any, userName: string }) => {
        return z.userDetails.userName || '';
    }
    parentNameInputFormatter = (z: { userDetails: any, userName: string, programUserId: string }) => {
        if (z.userDetails) {
            console.log("z",z);

            var isDuplicate = false;

            for(let i=0;i<this.usersArr.length;i++){
                console.log("z.programUserId",z.programUserId);
                console.log("this.usersArr[i].userId",this.usersArr[i].userId);
                if(z.programUserId === this.usersArr[i].userId){
                    isDuplicate = true;
                    break;
                }
            }

            if(isDuplicate === false){
                this.engineerObj = z;

                var tempName = z.userDetails.userName;

                if(z.userDetails.userName.length>28){
                    tempName = z.userDetails.userName.substring(0, 25);
                    tempName = tempName + "..."
                }

                z.userDetails.userName=tempName;

                var tempObj = {
                    "userId" : z.programUserId,
                    "userDetails" : z.userDetails
                }
                this.usersArr.push(tempObj)
                console.log("bcjb",this.usersArr)
                this.updateForumQuestions(this.selectedQuestion);
                this.usersMapForm.patchValue({
                    engineerId: z.programUserId,
                });

                this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "addEngineer", z.programUserId);
            }
   
            return z.userDetails.userName || '';
        }
        else {
            console.log("Do Nothing!");
        }
    }

    reallocateUsers(question,index){

        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "ReallocateButtonClick", this.loggedInUserId);

        console.log("question",question);
        this.selectedQuestion=question;
        for(let i=0;i<this.questionsArray.length;i++){
            if(i !== index){
                this.questionsArray[i].showForm = false;
            }
            else{
                this.questionsArray[i].showForm = true;
            }
        }

        this.usersArr = [] ;

        for(let i=0;i<question.engineers.length;i++){
            for(let j=0;j<question.userInfo.length;j++){
                console.log("question.engineers[i]",question.engineers[i]);
                console.log("question.userInfo[j].programUserId",question.userInfo[j].programUserId);
                if(question.engineers[i]===question.userInfo[j].programUserId){
                    var tempName = question.userInfo[j].userDetails.userName;

                    if(question.userInfo[j].userDetails.userName.length>28){
                        tempName = question.userInfo[j].userDetails.userName.substring(0, 25);
                        tempName = tempName + "..."
                    }

                    question.userInfo[j].userDetails.userName=tempName;

                    var tempObj={
                        "userId" : question.engineers[i],
                        "userDetails" :  question.userInfo[j].userDetails
                    }
                    console.log("tempObj",tempObj);
                    this.usersArr.push(tempObj);
                }
            }
        }

       console.log("final this.usersArr",this.usersArr);

        this.usersMapForm.reset();
    }

      /**
     * METHOD   : deleteUser
     * DESC     : DELETE A USER FROM THE  TABLE.
     *
     */
    deleteUser(index) {
        var userId = "";
        if(this.usersArr[index] !== undefined){
            userId = this.usersArr[index].userId;
        }
       
        console.log("userId",userId);
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", "deleteEngineer", userId);

        this.usersArr.splice(index, 1);
        console.log("this.selectedQuestion",this.selectedQuestion);
        this.updateForumQuestions(this.selectedQuestion);
     }

    updateForumQuestions(Question){
        console.log("Question",Question);
        var requestobject ={
            questionId : Question.questionId,
            updateInfo:{}
        };

        var engineersArr = [];

        for(let i=0;i<this.usersArr.length;i++){
            engineersArr.push(this.usersArr[i].userId);
        }

        requestobject.updateInfo={
            engineers: engineersArr
        }
        console.log("requestobject",requestobject);
        this.showLoader = true;
        this.askanexpertService.updateForumQuestions(requestobject).subscribe(
            responseObject => {
              this.showLoader = false;
      
              let responseCodes = this.configService.getStatusTokens();
              switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                  break;
                case responseCodes.RESP_SERVER_ERROR:
                  break;
                case responseCodes.RESP_SUCCESS:
                    console.log("updateForumQuestions responseObject", responseObject);
                    this.usersMapForm.reset();
                    var records = responseObject.result[0];
                    for(let i=0;i<this.allQuestions.length;i++){
                        if(this.allQuestions[i].questionId ===  records.questionId){
                            this.allQuestions[i].engineers = records.engineers;
                            this.allQuestions[i].userInfo = records.userInfo;
                        }
                    }

                    for(let j=0;j<this.questionsArray.length;j++){
                        if(this.questionsArray[j].questionId ===  records.questionId){
                            this.questionsArray[j].engineers = records.engineers;
                            this.questionsArray[j].userInfo = records.userInfo;
                        }
                    }
                  
                  break;
                case responseCodes.RESP_AUTH_FAIL:
                  break;
                case responseCodes.RESP_FAIL:
                  this.noRecords = true;
                  break;
                case responseCodes.RESP_ALREADY_EXIST:
                  break;
              }
            },
            err => {
              this.showLoader = false;
            }
        );
    }

    tabClick(tab){
        console.log("tab",tab);
        console.log("this.loggedInUserId",this.loggedInUserId);
        this.googleAnalyticsEventsService.emitEvent("AskAnExpert", tab, this.loggedInUserId);
    }

}