import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class AskAnExpertService {
    public subject = new Subject<any>();

    constructor(private http: Http,
        private configService: ConfigService) {

    } 

    getForumQuestions(obj){
        let url = this.configService.getApiUrls().getForumQuestions;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {
            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                console.log(res.json);
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    addAnswer(obj) {
        let url = this.configService.getApiUrls().addAnswer;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {
            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                console.log(res.json);
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    getAnswer(obj){
        let url = this.configService.getApiUrls().getAnswer;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {
            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                console.log(res.json);
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    searchProgramUsers1(searchString: string = '', userName?: string, parentNameList?: string[]): Observable<any> {
        console.log("userName:::::", userName);
        console.log("parentnamesList:::::", parentNameList);
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj: any = {
            "clientId" : this.configService.getloggedInProgramInfo().clientId,
            "programId" : this.configService.getloggedInProgramInfo().programId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "serviceType": "programSetup",
            "profileId": "UP1531873691460",
            "programRole": "BR1519707374235"
        };
        reqObj.searchUser = searchString;
        if(this.configService.getFrontEndUserInfo() === undefined){
            reqObj.frontendUserInfo = {};
        }

        let url = this.configService.getApiUrls().getAllProgramUsers;;
        console.log("url",url);
        console.log("reqObj",reqObj);
        console.log("options",options);

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                let responseArr = res.json().result;
                if (userName) {
                    if (responseArr) {
                        for (let i = 0; i < responseArr.length; i++) {
                            if (userName === responseArr[i].userName) {
                                responseArr.splice(i, 1);
                            }
                        }
                    }
                }
                if (parentNameList) {
                    console.log("In elseif program setup service!!!");
                    for (let parentIndex = 0; parentIndex < parentNameList.length; parentIndex++) {
                        for (let resIndex = 0; resIndex < responseArr.length; resIndex++) {
                            if (parentNameList[parentIndex] === responseArr[resIndex].userName) {
                                responseArr.splice(resIndex, 1);
                            }
                        }
                    }
                }

                return responseArr;
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }

    updateForumQuestions(obj){
        let url = this.configService.getApiUrls().updateForumQuestions;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {
            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        console.log("obj",obj);
        console.log("options",options);
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                console.log(res.json);
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

}