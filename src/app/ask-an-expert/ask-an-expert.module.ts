/*
	Author			:	Ravi Thokal
	Description		: 	Ask An Expert
	Date Created	: 	05 july 2018
	Date Modified	: 	05 july 2018
*/
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "@angular/material";
import { FooterModule } from "../footer/footer.module";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { SharedModule } from "../shared-components/shared.module";
import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
import { AskAnExpertComponent } from "./ask-an-expert.component";
import { AskAnExpertService } from "./ask-an-expert.service";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    declarations: [AskAnExpertComponent],

    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpModule, 
        MaterialModule,
        FooterModule,
        ChartsModule,
        AmChartsModule,
        SharedModule,
        NgbCarouselModule.forRoot(),
        NgbModule
    ],
    exports: [],

    providers: [AskAnExpertService],
    entryComponents: []
})
export class AskAnExpertmodule {  
}