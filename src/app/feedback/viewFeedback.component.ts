/*
	Author			:	Ravi Thokal
	Description		: 	View Feedback: to display feedback message
	Date Created	: 	18 Sep 2018
*/

import { Component, ViewChild, OnInit, Input } from "@angular/core";
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from "../user/user.service";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FeedbackMessageComponent } from './feedback-message.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: 'viewFeedback.component.html',
    providers: [UserService,Device],
    styleUrls: ["./viewFeedback.component.scss"],
})

export class ViewFeedbackComponent implements OnInit {

    public feedbackArr:any[] = [];
    public requestObject = {
        programId : "",
        limit: 10,
        skip: 0
    };
    public categoryIdArray = [];
    public categoryArray = [];
    public selectedFeedback={};

    private limit: number=10;
    private skip: number;
    private serialSkip: number;
    public disablePrev: boolean = true;
    public disableNext: boolean = false;
    isCountCanBeShown: boolean;
    isPrevious: boolean;
    isNext: boolean;
    endRecord: number;
    startRecord: number;
    totalRecords: number;
    public pageCount: number = 1; //to maintain pagecount in pagination
    public isNoRecords:boolean ;
    
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    feedbackForm: FormGroup;
    private userInfo;


    private showLoader: boolean = false;
    constructor(

        private formBuilder: FormBuilder,
        private userService: UserService,
        private configService: ConfigService,
        private stringService: StringService,
        public sidebar: SidebarComponent,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private router: Router,
        private device: Device,
    ) {

         // for width code start  
         this.mobilewidth = window.screen.width;
         console.log("Width:", this.mobilewidth);
         if (this.mobilewidth <= 576) {
             this.displaypc = false;
             this.displaymobile = true;
         }
         else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
             this.displaypc = false;
             this.displaymobile = true;
 
         }
         else {
             this.displaypc = true;
             this.displaymobile = false;
         }
         // for width code end
         this.feedbackForm = this.formBuilder.group({
            category: [""],
        });
    }

    ngOnInit() {
        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("viewFeedback");
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "viewFeedback");
                    ga('send', 'pageview');
                }
            });
        }
       

        this.sidebar.close();
        this.stringService.SetCategories();
        this.categoryArray = this.stringService.getCategories();
        console.log("this.categoryArray ",this.categoryArray );
        for (let i = 0; i < this.categoryArray.length; i++) {
			this.categoryArray[i].isSelected = false;
        }
        this.limit = 10;
        this.skip = 0;
        this.serialSkip = 0;
        this.getFeedback(this.requestObject);
        this.userInfo = this.configService.getLoggedInUserInfo();
        console.log("this.userInfo",this.userInfo);
    }

    setCategoryMob(index) {
        console.log("this.categoryArray[index].categoryName",this.categoryArray[index].categoryName);
        this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "viewCategory", this.categoryArray[index].categoryName);
		console.log("set Category mob",index);
        
        // this.categoryArray[index].isSelected = !this.categoryArray[index].isSelected;
        this.categoryIdArray = [];
        // for (let i = 0; i < this.categoryArray.length; i++) {
            console.log("this.categoryArray[i]",this.categoryArray[index]);
            this.categoryIdArray.push(this.categoryArray[index].categoryName);
        
		// this.isBrandsToRefresh = false;
        // this.getProductFormParams();
        console.log("this.categoryIdArray",this.categoryIdArray);
        this.skip=0;
        this.requestObject.skip=0;
        this.pageCount = 1;
        this.totalRecords = 0;
        this.startRecord = 0;
        this.endRecord = 0;
        this.limit = 10;
        this.skip = 0;
        this.getFeedback(this.requestObject);
	}

    setCategory(index) {
		console.log("set Category",index);
        
        this.categoryArray[index].isSelected = !this.categoryArray[index].isSelected;
        this.categoryIdArray = [];
        for (let i = 0; i < this.categoryArray.length; i++) {
            console.log("this.categoryArray[i]",this.categoryArray[i]);
            if (this.categoryArray[i].isSelected) {
                console.log("this.categoryArray[i].categoryName",this.categoryArray[i].categoryName);
                this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "viewCategory", this.categoryArray[i].categoryName);
                this.categoryIdArray.push(this.categoryArray[i].categoryName);
            }
        }
		// this.isBrandsToRefresh = false;
        // this.getProductFormParams();
        console.log("this.categoryIdArray",this.categoryIdArray);
        this.skip=0;
        this.requestObject.skip=0;
        this.pageCount = 1;
        this.totalRecords = 0;
        this.startRecord = 0;
        this.endRecord = 0;
        this.limit = 10;
        this.skip = 0;
        this.getFeedback(this.requestObject);
    }

    //method to apply filter according to sected category  for mobile -- adde by Rvai
    valueChanges() {
        console.log("in value change",this.feedbackForm.value.category);
        this.feedbackForm.valueChanges.subscribe(data => {
            this.categoryIdArray= [];
            console.log("DATA---------------------", data);
               
            if (data.category !== null && data.category !== "" && data.category !== "All"){
                for (var i = 0; i < this.categoryArray.length; i++) {
                    if (this.categoryArray[i].categoryName == data.category) {
                        this.categoryIdArray.push(this.categoryArray[i].categoryName);
                    }
                }

            }
            console.log("this.categoryIdArray",this.categoryIdArray);
            this.skip=0;
            this.requestObject.skip=0;
            this.pageCount = 1;
            this.totalRecords = 0;
            this.startRecord = 0;
            this.endRecord = 0;
            this.limit = 10;
            this.skip = 0;
            this.getFeedback(this.requestObject);
        });
    }


    getFeedback(reqObj) {
        console.log("In get Feedback----------------------");

        if(this.categoryIdArray.length!==0){
            reqObj.categoryArr=this.categoryIdArray
        }else{
            delete reqObj.categoryArr;
        }
        this.showLoader = true;
        this.userService.getFeedback(reqObj)
            .subscribe(
                (responseObject) => {

                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            console.log("feedback msg component-------------------" + responseObject.message);
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
                            this.feedbackArr=responseObject.result;
                            for(let i=0;i<this.feedbackArr.length;i++){
                                this.feedbackArr[i].showMore=false;
                                var date = new Date(this.feedbackArr[i].createdAt);
                                date.toString();
                                // var dateArr=String(date).split(" ");
                                // var newDate=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];

                                var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

                                var offset = date.getTimezoneOffset() / 60;
                                var hours = date.getHours();

                                newDate.setHours(hours - offset);

                                var dateArr=String(newDate).split(" ");
                                var newDate1=dateArr[2] + " " + dateArr[1] + " " + dateArr[3] + " " + dateArr[4];


                                this.feedbackArr[i].createdAt=newDate1;

                                if(this.feedbackArr[i].feedback.length>25){
                                    this.feedbackArr[i].showMore=true;
                                    this.feedbackArr[i].originalFeedback=this.feedbackArr[i].feedback;
                                    console.log("before this.feedbackArr[i].feedback",this.feedbackArr[i].feedback);
                                    var tempFeedback = this.feedbackArr[i].feedback.substring(0, 25);
                                    console.log("tempFeedback",tempFeedback);
                                    console.log("after this.feedbackArr[i].feedback",this.feedbackArr[i].feedback);
                                    this.feedbackArr[i].feedback = tempFeedback + "  "
                                }
                            }

                            if (responseObject.result.length < this.limit) {
                                this.isNext = false;
                            } else {
                                this.isNext = true;
                            }
                
                            if (this.pageCount == 1) {
                                this.isPrevious = false;
                            } else {
                                this.isPrevious = true;
                            }
                            if (this.requestObject.skip == 0) {
                                this.totalRecords = responseObject.count;
                            }
            
                            // code to show no. of records on navigation page
                            if (responseObject.result.length > 0) {
                                console.log("this.pageCount",this.pageCount);
                                console.log("this.limit",this.limit);
                                console.log("this.pageCount",this.pageCount);
                                console.log("responseObject.result.length",responseObject.result.length);
                                this.isCountCanBeShown = true;
                                if (this.pageCount > 1) {
                                    this.startRecord =
                                    this.pageCount * this.limit - (this.limit - 1);
                                    this.endRecord =
                                    this.startRecord + (responseObject.result.length - 1);
                                } else {
                                    this.startRecord = 1;
                                    this.endRecord =
                                    this.startRecord + (responseObject.result.length - 1);
                                }
                            } else {
                                this.isCountCanBeShown = false;
                            }
            
                            //Disable next button if total records are equal to pagination limit
                            if (this.totalRecords === this.pageCount * this.limit) {
                                this.isNext = false;
                            }

                            console.log("this.isNext",this.isNext);
                            console.log("this.isNoRecords",this.isNoRecords)
                            console.log("this.isPrevious",this.isPrevious);
                           
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.isNoRecords=true;
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }

                },
                err => {
                    this.showLoader = false;
                    // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }

    showMore(feedback){
        if(this.userInfo!==undefined){
            var tempUserId = this.userInfo.userId!==undefined? this.userInfo.userId : this.userInfo.clientUserId ; //to check if not client user
            console.log("tempUserId",tempUserId);
            this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "seeMoreclick", tempUserId);
        }
        
        console.log("on click showMore",feedback);
        this.selectedFeedback = feedback;
        this.configService.setModalClose(false);
    }

    close(){
        if(this.userInfo!==undefined){
            var tempUserId = this.userInfo.userId!==undefined? this.userInfo.userId : this.userInfo.clientUserId ; //to check if not client user
            console.log("tempUserId",tempUserId);
            this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "closeMoreClick", tempUserId);
        }
        this.configService.setModalClose(true);
    }

    onPrevClick() {
        this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "previous");
        this.requestObject.skip = this.requestObject.skip - this.limit;
        this.requestObject.limit = this.limit;
        this.pageCount--;
        this.serialSkip = this.requestObject.skip;
        this.getFeedback(this.requestObject);
    }
    
    
    onNextClick() {
        this.googleAnalyticsEventsService.emitEvent("ViewFeedback", "next");
        this.requestObject.skip = this.requestObject.skip + this.limit;
        this.requestObject.limit = this.limit;
        this.pageCount++;
        this.serialSkip = this.requestObject.skip;
        this.getFeedback(this.requestObject);
    }


}