// File Name: feedback.component.ts
// Created By: Trupti Ghude
// Created date: 6/8/18
// Description: To allow user to submit feedback



import { Component, ViewChild, OnInit, Input } from "@angular/core";
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from "../user/user.service";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FeedbackMessageComponent } from './feedback-message.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: 'feedback.component.html',
    providers: [UserService,Device],
    styleUrls: ["./feedback.component.scss", "../app.component.scss"],
})

export class FeedbackComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;

    private showLoader: boolean = false;
    feedbackForm: FormGroup;
    public subcategories: any;
    public showSubcategories: boolean = false;
    programUserInfo: any;
    public feedbackReqObj = {
        "userId": null,
        "feedback": null,
        "category": null,
        "subcategory": null,
        "programId": null
    }

    public feedbackArr=[];

    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(

        private formBuilder: FormBuilder,
        private userService: UserService,
        private configService: ConfigService,
        private stringService: StringService,
        public sidebar: SidebarComponent,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private router: Router,
        private device: Device,
    ) {
        // for width code start  
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
        // for width code end
        this.programUserInfo = configService.getloggedInProgramUser();
        this.feedbackForm = this.formBuilder.group({
            category: ["", [Validators.required]],
            // subcategory: ["", [Validators.required]],
            feedback: ["", [Validators.required]]
        });

     

    }


    ngOnInit() {
        // google analytics page view code 
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("feedback")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "feedback");
                    ga('send', 'pageview');
                }
            });
        }
        this.sidebar.close();
        this.stringService.SetCategories();
        this.valueChanges();
    }

    //To track changes in Feedback form
    valueChanges() {
        this.feedbackForm.valueChanges.subscribe(data => {
            console.log("DATA---------------------", data);

            if (data.category !== null && data.category !== "") {
                var categories = this.stringService.getCategories();
                for (var i = 0; i < categories.length; i++) {
                    if (categories[i].categoryName == data.category) {
                        this.subcategories = categories[i].subCatgories;
                        this.showSubcategories = true;
                    }
                }

            }
        });
    }

    //to create request object when user submits the form
    onSubmitClick() {

        this.googleAnalyticsEventsService.emitEvent("Submit Feedback", "Submit", this.feedbackForm.value.category);
        console.log("In OnSubmit----------------");
        console.log("get categories----------", this.stringService.getCategories());

        if (this.feedbackForm.value.feedback !== "") {
            console.log("feedback------" + this.feedbackForm.value.feedback)
            this.feedbackReqObj.category = this.feedbackForm.value.category;
            // this.feedbackReqObj.subcategory = this.feedbackForm.value.subcategory;
            this.feedbackReqObj.feedback = this.feedbackForm.value.feedback;
            this.feedbackReqObj.userId = this.programUserInfo.programUserId;
            this.feedbackReqObj.programId = this.programUserInfo.programId;
            console.log("feedback request object-------", this.feedbackReqObj);
            this.giveFeedback(this.feedbackReqObj);
        }
        else {
            // this.feedbackMessageComponent.updateMessage(true, "Please give some feedback !!", "alert-danger");
        }
    }

    //to send feedback request object and process the response
    giveFeedback(reqObj) {
        console.log("In GiveFeedBack----------------------");
        this.showLoader = true;
        this.userService.giveFeedback(reqObj)
            .subscribe(
                (responseObject) => {

                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            console.log("feedback msg component-------------------" + responseObject.message);
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
                            this.feedbackArr=responseObject.result;
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }

                },
                err => {
                    this.showLoader = false;
                    // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }

} 