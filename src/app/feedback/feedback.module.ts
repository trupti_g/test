import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";

import { FeedbackComponent } from './feedback.component';
import { FeedbackMessageComponent } from "./feedback-message.component";
import { ViewFeedbackComponent } from "./viewFeedback.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  imports: [FormsModule, ReactiveFormsModule, CommonModule,],
  declarations: [FeedbackComponent, FeedbackMessageComponent,ViewFeedbackComponent],
  entryComponents: [],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [FeedbackMessageComponent],

})
export class FeedbackModule { } 