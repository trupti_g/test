import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class LeadManagementService {
    public subject = new Subject<any>();

    constructor(private http: Http,
        private configService: ConfigService) {

    }

    addOrder(requestObject): any {
        let recordInfo: any = {};
        let frontendUserInfo: any = {};
        recordInfo = requestObject;
        console.log("RequestObject in Add Order Service", requestObject);
        let requestO: any = {};
        requestO.frontendUserInfo = this.configService.getFrontEndUserInfo();
        requestO.recordInfo = recordInfo;

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestO;

        let url = this.configService.getApiUrls().getAllOrders;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    addPointConversionRequest(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("RequestObject in Add Order Service", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().addPointConversionRequest;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getPointConversionRequest(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("RequestObject in Add Order Service", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getPointConversionRequest;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getAllStarPointConfiguration(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("REQUEST OBJECT: ", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getAllStarPointConfiguration;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("INSIDE MAP : ", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("INSIDE CATCH");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getAllProgramFromMaster(requestObject): any {
        let frontendUserInfo: any = {};
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("REQUEST OBJECT: ", requestObject);
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getPrograms;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("INSIDE MAP : ", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("INSIDE CATCH");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getStatesV1() {
        let url = this.configService.getApiUrls().getStates;

        let reqObj = {
            searchString: ""
        };

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                // console.log("inside map", res.json());
                return res.json();
                // return cities;
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    getStatesV2() {
        let url = this.configService.getApiUrls().getStates;
        var reqObj = {};
        reqObj = {
            searchString: ""
        };
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    getCitiesV1(state) {

        let url = this.configService.getApiUrls().getCities;
        let reqObj = {
            "stateName": state,
            "sort": { "cityName": 1 }
        };

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    getAllClients() {
        let url = 'http://localhost:1340/getAllClients/v1'
        let reqObj = {
            "frontendUserInfo": {
                "userId": "sdSAD"
            }
        };

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ'
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    sendSms() {
        let url = 'http://localhost:1335/sendSms/v1'
        let reqObj = {
            "frontendUserInfo": {
                "userId": "sdSAD"
            },
            "mobileNumber": "9902730379",
            "message": "hi",
        };

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ'
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    sendEmail() {
        let url = 'http://localhost:1335/sendEmail/v1'
        let reqObj = {
            "frontendUserInfo": {
                "userId": "sdSAD"
            },
            "from": "info@annectos.in",
            "to": "shalini@annectos.net",
            "subject": "Password",
            "text": "You password is 123456",
            "templatePlan": "Email Verification",
            "communicationLogInfo": [
                {
                    "communicationType": "EMAIL",
                    "sendTo": "shalini@annectos.net",
                    "message": "Test email ",
                    "userId": "AU1485160341816"
                }
            ],
            "Details": [
                {
                    // "details": this.BasicDetailsForm.values
                }
            ]
        };

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ'
        });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
                // return cities;
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    addLead(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().addLead;

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });
        request.serviceType = "Lead";
        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
                // return cities;
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    getLead(obj) {
        let url = this.configService.getApiUrls().getLeads;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        obj.serviceType = "Lead";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    getLeadReport(obj) {
        let url = this.configService.getApiUrls().getLeadReport;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        obj.serviceType = "Lead";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                console.log("inside mapppppp");
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };
    updateLead(requestObject) {
        let request = requestObject;
        let url = this.configService.getApiUrls().updateLead;

        request.frontendUserInfo = this.configService.getFrontEndUserInfo();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });
        request.serviceType = "Lead";
        return this.http.post(url, request, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
                // return cities;
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };

    getSuperstarsOfTheMonth(obj) {
        let url = this.configService.getApiUrls().getSuperstarsOfTheMonth;
        var request = obj;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        obj.serviceType = "Lead";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };
}