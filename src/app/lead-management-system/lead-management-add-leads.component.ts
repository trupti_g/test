/**
 * Created by Hrishikesh
 */
import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  OnChanges,
  NgZone,
  ViewChild
} from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import * as myGlobals from "./globals";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { StringService } from "../shared-services/strings.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { LeadManagementService } from "./lead-management.service";
import { ConfigService } from "../shared-services/config.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  templateUrl: "./lead-management-add-leads.component.html",
  // styleUrls: ['./lead-management.component.css', '../claims/claims.component.css'],
  styleUrls: ["../app.component.scss", "./lead-management.component.scss"],
  providers: [Device]
})
export class LeadManagementAddLeadsComponent implements OnInit {
  mobilewidth: any;
  displaypc: boolean = true;
  displaymobile: boolean = false;
  programUserInfo: any;
  marked = false;
  showKeyContactsTable: boolean;
  showLoader: boolean;
  public keyContactsArray: any = [];
  private leadIndex: string = "";
  private lead: any;
  private serviceUsersList: any[];
  // private salesUsersList: any[];
  private inputMediumList: any[];
  private statusList: any[];
  private cityArray: string[] = [];
  private citiesForOffice: string[] = [];
  private projectInfo: FormGroup;
  private PersonDetailsForm: FormGroup;
  private ProvidedForm: FormGroup;
  private serviceEngineerForm: FormGroup;
  private serviceUserNamesList: any[];
  private bnewlead: boolean;
  public qualified: boolean = false;
  private loggedInUser: any;
  private statesArray: string[] = [];
  //save old keycontacts for comparison
  private lkeycontacts: any = { keycontacts: [] };
  private leadDetails: any = {};
  private ProvidedInfo: any = {};
  private createdBy: any = {};
  private visits: any = {};
  public isQualified: boolean = false;


  @ViewChild(FeedbackMessageComponent)
  private feedbackMessageComponent: FeedbackMessageComponent;
  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private http: Http,
    private formBuilder: FormBuilder,
    private stringService: StringService,
    private leadService: LeadManagementService,
    public sidebar: SidebarComponent,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private device: Device,
  ) {
    this.mobilewidth = window.screen.width;
    console.log("Width:", this.mobilewidth);
    if (this.mobilewidth <= 576) {
      this.displaypc = false;
      this.displaymobile = true;
      // document.getElementById("desktop-div").style.visibility = "visible";
      // document.getElementById("mobile-div").style.visibility = "hidden";
    }
    else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
      this.displaypc = false;
      this.displaymobile = true;
      // document.getElementById("desktop-div").style.visibility = "visible";
      // document.getElementById("mobile-div").style.visibility = "hidden";

    }
    else {
      this.displaypc = true;
      this.displaymobile = false;
    }

  

    this.programUserInfo = configService.getloggedInProgramUser();

    this.PersonDetailsForm = this.formBuilder.group({
      contactname: [""],
      contactrole: [""],
      contactcontact: ["", [Validators.minLength(10), Validators.maxLength(12), Validators.pattern('[0-9]{10,12}$')]],
      remarks: [""]
    });

    this.projectInfo = this.formBuilder.group({
      projectname: ["", [Validators.required]],
      ownername: ["", [Validators.required]],
      projecttype: [""],
      expectedvalue: [0, [Validators.required]],
      // vcqualified: ["", Validators.required],
      address: ["", [Validators.required]],
      city: ["", [Validators.required]],
      state: ["", [Validators.required]],
      pincode: ["", [Validators.required]],
      expectedSalesPeriod: ["", [Validators.required]]
    });

    this.projectInfo.valueChanges.subscribe(data => {
      if (
        data.state !== null &&
        data.state !== undefined &&
        data.state !== ""
      ) {
        this.getCities(data.state);
      }
    });

  }

  ngOnInit() {

    // google analytics page view code
    if(this.device.device === "android" || this.device.device === "ios"){
      console.log("inside android and ios");
      (<any>window).ga.trackView('Add Leads')
    } else {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', "Add Leads");
          ga('send', 'pageview');
        }
      });
    }
    window.scrollTo(0, 0);
    this.sidebar.close();
    this.sidebar.addExpandClass1('pages1');
    this.getStates();
    var obj = {
      name: "Suresh",
      role: "serviceengineer",
      contact: "9876757647",
      pincode: "560002",
      id: "5968602291095b2de0d2ee41"
    };

    myGlobals.setLoggedInUser(obj);
    // this.loggedInUser = myGlobals.loggedInUser;
    this.loggedInUser = obj;
    this.leadIndex = myGlobals.leadindex;
    this.bnewlead = myGlobals.bnewlead;
    this.lead = myGlobals.glead;

    if (!this.bnewlead) {
      this.getLead();
    } else {
      this.createDummyLead();
    }

    this.inputMediumList = [
      { id: "email", name: "Email" },
      { id: "mail", name: "Mail" },
      { id: "call", name: "Call" }
    ];
    this.statusList = [
      { id: "created", name: "Created" },
      { id: "assigned", name: "Assigned" },
      { id: "visitscheduled", name: "Visit Scheduled" },
      { id: "intrevisit", name: "Interested Revist" },
      { id: "revisit", name: "Revisit" },
      { id: "readytopurchase", name: "Ready to Purchase" },
      { id: "dcclosed", name: "Deal Closed" },
      { id: "llunqualified", name: "Lead Lost - UnQualified" },
      { id: "lldelay", name: "Lead Lost - Delay in Approach" },
      { id: "llprice", name: "Lead Lost - Prices Not Competitive" },
      { id: "llspec", name: "Lead Lost - Technical Specs Not Met" },
      { id: "llrelevance", name: "Lead Lost - Irrelevant Lead" }
    ];

    console.log(" this.loggedInUser " + JSON.stringify(this.loggedInUser));

  }

  initserviceEngineerForm() {
    this.serviceEngineerForm = this.formBuilder.group({
      keyPersonName: ["", [Validators.required]],
      keyPersonRole: ["", [Validators.required]],
      keyPersonContact: ["", [Validators.required]],
      vcserviceuserid: ["", [Validators.required]],
      salescontact: ["", [Validators.required]],
      status: ["", [Validators.required]]
    });
  }

  getStates(): string[] {
    let states: string[] = [];
    var req = {};
    this.leadService.getStatesV1().subscribe(
      responseObject => {
        console.log("states response", responseObject);
        let responseCodes = this.configService.getStatusTokens();
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_SERVER_ERROR:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_SUCCESS:
            if (responseObject.result) {
              for (let i = 0; i < responseObject.result.length; i++) {
                this.statesArray[i] = responseObject.result[i].stateName;
              }
            }

            break;

          case responseCodes.RESP_AUTH_FAIL:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_FAIL:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_ALREADY_EXIST:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;
        }
      },
      err => {
        // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger", this.errorMessageService.getErrorMessages().connectionErr);
      }
    );
    return states;
  }

  getCities(state): string[] {
    let cities: string[] = [];
    this.leadService.getCitiesV1(state).subscribe(
      responseObject => {
        let responseCodes = this.configService.getStatusTokens();
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_SERVER_ERROR:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_SUCCESS:
            if (responseObject.result) {
              this.cityArray = [];
              for (let i = 0; i < responseObject.result.length; i++) {
                cities[i] = responseObject.result[i].cityName;
                this.cityArray.push(cities[i]);
                console.log(" this.cityArray" + JSON.stringify(this.cityArray));
              }
            }
            break;

          case responseCodes.RESP_AUTH_FAIL:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_FAIL:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;

          case responseCodes.RESP_ALREADY_EXIST:
            this.feedbackMessageComponent.updateMessage(
              true,
              responseObject.message,
              "alert-danger"
            );
            break;
        }
      },
      err => {
        // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger", this.errorMessageService.getErrorMessages().connectionErr);
      }
    );
    return cities;
  }

  providedInit() {
    this.ProvidedForm = this.formBuilder.group({
      vcname: ["", [Validators.required]],
      vccontact: ["", [Validators.required]],
      vcdate: ["", [Validators.required]],
      createdby: ["", [Validators.required]],
      createdbycontact: ["", [Validators.required]],
      createdbyrole: ["", [Validators.required]],
      infoMedium: ["", [Validators.required]]
    });
  }

  getLead() {
    this.http
      .get("http://localhost:1380/lead" + "?id=" + this.leadIndex)
      .map(res => res.json())
      .subscribe(
        data => {
          this.lead = data;
          this.leadDetails = data;
          this.ProvidedInfo = data.providedby;
          this.createdBy = data.createdby;
          this.visits = data.visits[0];

          this.initLeadValue();
          myGlobals.setLead(data);
          this.lkeycontacts = {
            keycontacts: JSON.parse(JSON.stringify(data.keycontacts))
          };
        },
        err => {
          console.log("Error retrieving Lead information");
        }
      );
  }
  initLeadValue() {
    this.projectInfo = this.formBuilder.group({
      projectname: [this.leadDetails.projectname],
      ownername: [this.leadDetails.ownername],
      projecttype: [this.leadDetails.projecttype],
      expectedvalue: [this.leadDetails.expectedtonnage],
      //OR
      vcqualified: [this.leadDetails.projectname],
      address: [this.leadDetails.address],
      city: [this.leadDetails.city],
      district: [this.leadDetails.district],
      state: [this.leadDetails.state],
      pincode: [this.leadDetails.pincode],
      expectedSalesPeriod: [this.leadDetails.expectedSalesPeriod]
    });
    this.PersonDetailsForm = this.formBuilder.group({
      contactname: ["", [Validators.required]],
      contactrole: ["", [Validators.required]],
      contactcontact: ["", [Validators.required]],
      vcname: ["", [Validators.required]],
      vccontact: ["", [Validators.required]],
      vcdate: ["", [Validators.required]],
      remarks: ["", [Validators.required]]
    });
    this.ProvidedForm = this.formBuilder.group({
      vcname: [this.ProvidedInfo.name],
      vccontact: [this.ProvidedInfo.contact],
      vcdate: [this.ProvidedInfo.vcdate],
      createdby: [this.createdBy.createdby],
      createdbycontact: [this.createdBy.createdbycontact],
      createdbyrole: [this.createdBy.createdbyrole]
    });
  }

  //need basic structure for lead information
  createDummyLead() {
    this.lead = {
      projectname: "",
      ownername: "",
      projectype: "",
      expectedvalue: 0,
      pincode: 0,
      state: "",
      district: "",
      city: "",
      address: "",
      keycontacts: [],
      providedby: { id: "", name: "", contact: "", role: "" },
      createdby: { name: "", contact: "" },
      createdate: "",
      inputmedium: "",
      assignedto: { assignid: 0, name: "" },
      salescontact: { assignid: 0, name: "" },
      qualified: "false",
      status: "new",
      estimation: [],
      influencers: [],
      visits: [],
      sales: [],
      variance: { reason: "", remark: "", date: "" },
      history: []
    };
  }



  isSESelected(index: number) {
    if (this.lead && this.lead.assignedto && this.lead.assignedto.name) {
      return index === this.lead.assignedto.name;
    }
    return false;
  }

  isSalesContactSelected(index: number) {
    if (this.lead && this.lead.salescontact && this.lead.salescontact.name) {
      return index === this.lead.salescontact.name;
    }
    return false;
  }

  isInputMediumSelected(index: number) {
    if (this.lead && this.lead.inputmedium) {
      return index === this.lead.inputmedium;
    }
    return false;
  }

  isStatusSelected(index: number) {
    if (this.lead && this.lead.status) {
      return index === this.lead.status;
    }
    return false;
  }

  isCall() {
    return this.lead.inputmedium === "call";
  }
  isSnailMail() {
    return this.lead.inputmedium === "mail";
  }
  isEmail() {
    return this.lead.inputmedium === "email";
  }
  addContact(kname: string, krole: string, kcontact: string) {
    if (
      kname === null ||
      kname === "" ||
      krole === null ||
      krole === "" ||
      kcontact === null ||
      kcontact === ""
    ) {
      alert("Please input valid name, role and contact details!!");
    } else {
      var kc = { name: kname, role: krole, contact: kcontact };
      this.lead.keycontacts.push(kc);
      kname = "";
    }
  }
  editContact(index: number) {
    alert("edit contact");
  }

  deleteContact(index: number) {
    this.lead.keycontacts.splice(index, 1);
  }



  //helper Methods
  getLMSUserForId(lmsuserid: string) {
    var index = 0;
    for (let i = 0; i < myGlobals.loginUsers.length; i++) {
      if (myGlobals.loginUsers[i].id === lmsuserid) {
        return myGlobals.loginUsers[i];
      }
    }
    return null;
  }

  getSEforInfluencer(influencerid: string) {
    for (let i = 0; i < myGlobals.masterSEInfluencer.length; i++) {
      if (myGlobals.masterSEInfluencer[i].iid === influencerid) {
        return this.getLMSUserForId(
          myGlobals.masterSEInfluencer[i].ses[0].seid
        );
      }
    }
    return null;
  }

  getSEforPin(pincode: number) {
    for (let i = 0; i < myGlobals.masterPinSE.length; i++) {
      if (myGlobals.masterPinSE[i].pincode === pincode) {
        return this.getLMSUserForId(myGlobals.masterPinSE[i].ses[0].seid);
      }
    }
    return null;
  }


  //Methods calling Rest APIs
  addLead() {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Add lead", this.projectInfo.value.projectname);
    console.log("remarks", this.PersonDetailsForm.value.remarks);
    var obj = {
      projectname: this.projectInfo.value.projectname,
      ownername: this.projectInfo.value.ownername,
      projecttype: this.projectInfo.value.projecttype,
      expectedtonnage: this.projectInfo.value.expectedvalue,
      address: this.projectInfo.value.address,
      city: this.projectInfo.value.city,
      state: this.projectInfo.value.state,
      pincode: this.projectInfo.value.pincode,
      expectedSalesPeriod: this.projectInfo.value.expectedSalesPeriod,
      keycontacts: this.keyContactsArray,
      providedby: this.configService.getloggedInProgramUser().programUserId,
      providedbyRole: this.configService.getloggedInProgramUser().userType,
      createdate: new Date(),

      qualified: false,
      pending: true,
      rejected: false,
      status: "created",

      programId: this.configService.getprogramInfo().programId,
      clientId: this.configService.getprogramInfo().clientId,
      remarks: this.PersonDetailsForm.value.remarks
    };

    console.log("objjjjj", obj);
    this.showLoader = true;
    console.log("req", obj);
    this.leadService.addLead(obj).subscribe(
      responseObject => {
        this.showLoader = false;

        let responseCodes = this.configService.getStatusTokens();
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:
            console.log("channel Info in home component", responseObject);
            this.PersonDetailsForm.reset();
            this.projectInfo.reset();
            this.keyContactsArray = [];
            this.feedbackMessageComponent.updateMessage(true, "Lead Added Successfully", "alert-success");
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            this.feedbackMessageComponent.updateMessage(true, "Lead Adding Failed", "alert-danger");
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
        window.scrollTo(0, 0);
      },
      err => {
        this.showLoader = false;
      }
    );
  }

  addKeyContact() {

    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Add key person", this.PersonDetailsForm.value.contactcontact);
    var obj = {
      keyPersonName: "",
      keyPersonRole: "",
      keyPersonContact: ""
    };
    obj.keyPersonName = this.PersonDetailsForm.value.contactname;
    obj.keyPersonRole = this.PersonDetailsForm.value.contactrole;
    obj.keyPersonContact = this.PersonDetailsForm.value.contactcontact;
    if ((obj.keyPersonName === "" && obj.keyPersonRole === "" && obj.keyPersonContact === "") ||
      (obj.keyPersonName === null && obj.keyPersonRole === null && obj.keyPersonContact === null) ||
      (obj.keyPersonName === undefined && obj.keyPersonRole === undefined && obj.keyPersonContact === undefined)
    ) {
    } else {
      this.keyContactsArray.push(obj);
      this.showKeyContactsTable = true;
      // this.PersonDetailsForm.reset();
      this.PersonDetailsForm.patchValue({
        "contactname": "",
        "contactcontact": "",
        "contactrole": ""
      })
    }
    console.log("key person", obj, this.keyContactsArray);
  }

  deleteKeyPerson(is) {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Delete", this.keyContactsArray[is].keyPersonContact);
    // for(var a = 0 ; a < this.keyContactsArray.length; a++){
    var index = is;
    console.log("index", is);
    this.keyContactsArray.splice(index, 1);
    // }
  }

  updateProjectInfo() {
    //TODO: need to link with formdata from ui
    var vcreatedate = this.ProvidedForm.value.vcdate;
    var vcassignedto = this.serviceEngineerForm.value.vcserviceuserid;
    var vcsalescontact = this.serviceEngineerForm.value.salescontact;
    var vcstatus = this.serviceEngineerForm.value.status;

    //convert boolean into string based for consistency
    var vcqualified = "false";
    if (this.qualified === true) {
      vcqualified = "true";
    } else {
      vcqualified = "false";
    }

    var vchistory = this.lead.history;
    if (!vchistory) {
      vchistory = [];
    }
    //construct json to be inserted into db
    var obj = {
      projectname: this.projectInfo.value.projectname,
      ownername: this.projectInfo.value.ownername,
      projecttype: this.projectInfo.value.projecttype,
      expectedvalue: this.projectInfo.value.expectedvalue,
      address: this.projectInfo.value.address,
      city: this.projectInfo.value.city,
      district: this.projectInfo.value.district,
      state: this.projectInfo.value.district,
      pincode: this.projectInfo.value.pincode,
      keycontacts: this.PersonDetailsForm.value.contactcontact,
      providedby: {
        name: this.ProvidedForm.value.vcname,
        contact: this.ProvidedForm.value.vccontact
      },
      createdby: {
        id: this.lead.createdby.id,
        name: this.lead.createdby.name,
        contact: this.lead.createdby.contact,
        role: this.lead.createdby.role
      },
      createdate: this.ProvidedForm.value.vcdate,
      inputmedium: this.ProvidedForm.value.infoMedium,
      assignedto: { assignid: vcassignedto.id, name: vcassignedto.name },
      salescontact: { assignid: vcsalescontact.id, name: vcsalescontact.name },
      qualified: vcqualified,
      status: this.serviceEngineerForm.value.status,
      history: vchistory
    };
    //add to history
    var vcmoddate = new Date();
    var vcmodifiedcontent = this.compare(myGlobals.glead, obj);
    if (vcmodifiedcontent.length > 0) {
      var validStatusChange = this.validateStatusChange(myGlobals.glead, obj);
      if (validStatusChange === "true") {
        obj.history.push({
          modifiedby: this.loggedInUser.name,
          status: this.serviceEngineerForm.value.status,
          modifieddate: vcmoddate,
          modifiedcontent: JSON.stringify(vcmodifiedcontent)
        });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        this.http
          .put("http://localhost:1380/lead/" + this.lead.id, obj, options)
          .map(res => res.json())
          .subscribe(
            data => {
              console.log(data);
            },
            err => {
              console.log("Error updating Lead information");
            }
          );
      } else {
        alert(validStatusChange);
      }
    } else {
      alert("Nothing to modify");
    }
  }

  //comparitor of lead projection info change set
  compare(oldlead: any, changedlead: any) {
    var changeset = [];
    if (!(oldlead.projectname === changedlead.projectname)) {
      changeset.push({ projectname: changedlead.projectname });
    }
    if (!(oldlead.ownername === changedlead.ownername)) {
      changeset.push({ ownername: changedlead.ownername });
    }
    if (!(oldlead.projecttype === changedlead.projecttype)) {
      changeset.push({ projecttype: changedlead.projecttype });
    }
    if (!(oldlead.expectedvalue === changedlead.expectedvalue)) {
      changeset.push({ expectedvalue: changedlead.expectedvalue });
    }
    if (!(oldlead.address === changedlead.address)) {
      changeset.push({ address: changedlead.address });
    }
    if (!(oldlead.city === changedlead.city)) {
      changeset.push({ city: changedlead.city });
    }
    if (!(oldlead.district === changedlead.district)) {
      changeset.push({ district: changedlead.district });
    }
    if (!(oldlead.state === changedlead.state)) {
      changeset.push({ state: changedlead.state });
    }
    if (!(oldlead.pincode === changedlead.pincode)) {
      changeset.push({ pincode: changedlead.pincode });
    }
    //alert(JSON.stringify(this.lkeycontacts.keycontacts) + ":::" + JSON.stringify(changedlead.keycontacts));
    if (
      !(
        JSON.stringify(this.lkeycontacts.keycontacts) ===
        JSON.stringify(changedlead.keycontacts)
      )
    ) {
      changeset.push({ keycontacts: changedlead.keycontacts });
    }
    if (
      !(
        JSON.stringify(oldlead.createdby) ===
        JSON.stringify(changedlead.createdby)
      )
    ) {
      changeset.push({ createdby: changedlead.createdby });
    }
    if (!(oldlead.createdate === changedlead.createdate)) {
      changeset.push({ createdate: changedlead.createdate });
    }
    if (!(oldlead.inputmedium === changedlead.inputmedium)) {
      changeset.push({ inputmedium: changedlead.inputmedium });
    }
    if (
      !(
        JSON.stringify(oldlead.assignedto) ===
        JSON.stringify(changedlead.assignedto)
      )
    ) {
      changeset.push({ assignedto: changedlead.assignedto });
    }
    if (
      !(
        JSON.stringify(oldlead.salescontact) ===
        JSON.stringify(changedlead.salescontact)
      )
    ) {
      changeset.push({ salescontact: changedlead.salescontact });
    }

    if (!(oldlead.qualified === changedlead.qualified)) {
      changeset.push({ qualified: changedlead.qualified });
    }
    if (!(oldlead.status === changedlead.status)) {
      changeset.push({ status: changedlead.status });
    }
    return changeset;
  }

  validateStatusChange(curLead: any, newLead: any) {
    var validStatusChange = "true";
    var curStatusId = curLead.status;
    var newStatusId = newLead.status;
    var userRole = this.loggedInUser.role;
    if (newStatusId === "created") {
      if (!(userRole === "manager")) {
        validStatusChange = "Cannot move status back to Created";
      }
    } else if (newStatusId === "assigned") {
      if (!(curStatusId === "created" || curStatusId === "assigned")) {
        if (!(userRole === "manager")) {
          validStatusChange = "Cannot move status back to Assigned";
        }
      }
    } else if (
      (curStatusId === "intrevisit" || curStatusId === "revisit") &&
      (newStatusId === "visitscheduled" ||
        newStatusId === "assigned" ||
        newStatusId === "created")
    ) {
      if (!(userRole === "manager")) {
        validStatusChange =
          "Cannot move status from " + curStatusId + " to " + newStatusId;
      }
    } else if (newStatusId === "readytopurchase") {
      if (!(userRole === "sales" || userRole === "serviceengineer")) {
        validStatusChange =
          "Not Authorized to move status to Ready to Purchase";
      }
    } else if (newStatusId === "dcclosed") {
      if (!(curStatusId === "readytopurchase")) {
        validStatusChange = "Cannot move status to Deal Closed";
      }
      if (!(userRole === "sales")) {
        validStatusChange = "Not Authorized to move status to Deal Closed";
      }
    }
    return validStatusChange;
  }

  //Menu navigation functions
  projectInformation() {
    this.router.navigate(["lmprojectinfo"]);
  }
  mapInfluencer() {
    this.router.navigate(["lminfluencer"]);
  }
  visitInformation() {
    this.router.navigate(["lmvisits"]);
  }
  estimation() {
    this.router.navigate(["lmestimation"]);
  }
  salesInformation() {
    this.router.navigate(["lmsales"]);
  }
  variance() {
    this.router.navigate(["lmvariance"]);
  }
  history() {
    this.router.navigate(["lmhistory"]);
  }
  viewLeads() {
    this.router.navigate(["../home/leads-list"]);
  }
  planActivity() {
    this.router.navigate(["leadmanagementplanactivity"]);
  }

  reports() {
    this.router.navigate(["lmleadsreport"]);
  }

  goBack() {
    this.router.navigate(["login"]);
  }
  navigate(pageName) {
    this.router.navigate([pageName]);
  }

  reset() {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Reset");
    this.PersonDetailsForm.reset();
    this.projectInfo.reset();
    this.keyContactsArray = [];
    this.showKeyContactsTable = false;
  }
  cancelNav() {
    this.router.navigate(['../home/leads-list']);
  }
}
