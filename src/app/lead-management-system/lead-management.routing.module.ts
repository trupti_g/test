/*  Author         : Pratik Gawand
    Description    : Reward Gallery Routing module
                    
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

export const leadmanagementRoutes: Routes = [
    {
        path: '',
        children: [

        ]
    }
];



@NgModule({
    imports: [RouterModule.forChild(leadmanagementRoutes)],
    exports: [RouterModule],

})
export class LeadManagementRoutingModule {

}
