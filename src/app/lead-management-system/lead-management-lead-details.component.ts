/**
 * Created by annectos on 2017-04-14.
 */
import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Http, Headers, RequestOptions } from '@angular/http';
import * as myGlobals from './globals';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { StringService } from "../shared-services/strings.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { LeadManagementService } from "./lead-management.service";
import { ConfigService } from "../shared-services/config.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './lead-management-lead-details.component.html',
    // styleUrls: ['./lead-management.component.css', '../claims/claims.component.css'],
    styleUrls: ["../app.component.scss", "./lead-management.component.scss"],
    providers: [Device]
})
export class LeadManagementLeadDetailsComponent implements OnInit, AfterViewChecked {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    public deleteLead: boolean = false;
    public isEngineer: boolean = false;
    public programUserInfo: any;
    isPending: boolean;
    isRejected: boolean = false;
    leadInfo: any;
    leadApproval: boolean;
    public engineerForm: boolean = false;
    public isQualified: boolean = false;
    showKeyContactsTable: boolean = false;
    leadToPatch: any;
    showLoader: boolean;
    public keyContactsArray: any = [];
    private leadIndex: string = "";
    private lead: any;
    private serviceUsersList: any[];
    private salesUsersList: any[];
    private inputMediumList: any[];
    private statusList: any[];
    private cityArray: string[] = [];
    private citiesForOffice: string[] = [];
    private projectInfo: FormGroup;
    private PersonDetailsForm: FormGroup;
    private ProvidedForm: FormGroup;
    private serviceEngineerForm: FormGroup;
    private serviceUserNamesList: any[];
    private bnewlead: boolean;
    qualified: boolean = false;
    private loggedInUser: any;
    private statesArray: string[] = [];
    //save old keycontacts for comparison
    private lkeycontacts: any = { keycontacts: [] };
    private leadDetails: any = {};
    private ProvidedInfo: any = {};
    private createdBy: any = {};
    private visits: any = {};

    private showRejctionsField: boolean = false;
    private isUpdated: boolean = false;

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;
    constructor(
        private configService: ConfigService,
        private route: ActivatedRoute,
        private router: Router,
        private http: Http,
        private formBuilder: FormBuilder,
        private stringService: StringService,
        private leadService: LeadManagementService,
        public sidebar: SidebarComponent,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {


    }


    ngAfterViewChecked() {
        // window.scrollTo(0, 0);
    }

    ngOnInit() {

        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

        // google anyalytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Lead Details')
		} else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Lead Details");
                    ga('send', 'pageview');
                }
            });
        }
      


        this.programUserInfo = this.configService.getloggedInProgramUser();
        console.log("..a.a.a.aa.a.a.aa", this.programUserInfo);


        // });
        console.log("roleName", this.configService.getloggedInProgramUser().roleName);
        console.log("userType", this.configService.getloggedInProgramUser().userType);
        console.log(" this.engineerForm", this.engineerForm);
        if (this.configService.getloggedInProgramUser().roleName === "Distributor" || this.configService.getloggedInProgramUser().userType === "ClientUser") {
            this.engineerForm = true
        }
        console.log(" this.engineerForm", this.engineerForm);


        this.leadToPatch = this.configService.getLead();

        console.log("AJJJJJJJJJJJJJJJJJJJJJJ   this.leadToPatch", this.leadToPatch);

        window.scrollTo(0, 0);
        this.sidebar.close();
        this.sidebar.addExpandClass1('pages1');
        var obj = {
            name: 'Suresh',
            role: 'serviceengineer',
            contact: '9876757647',
            pincode: '560002',
            id: '5968602291095b2de0d2ee41'
        };

        myGlobals.setLoggedInUser(obj);
        this.loggedInUser = obj;
        this.leadIndex = myGlobals.leadindex;
        this.bnewlead = myGlobals.bnewlead;
        this.lead = myGlobals.glead;

        this.inputMediumList = [{ "id": "email", "name": "Email" }, { "id": "mail", "name": "Mail" }, { "id": "call", "name": "Call" }];
        this.statusList = [
            { "id": "created", "name": "Created" }, { "id": "assigned", "name": "Assigned" },
            { "id": "visitscheduled", "name": "Visit Scheduled" }, { "id": "intrevisit", "name": "Interested Revist" },
            { "id": "revisit", "name": "Revisit" }, { "id": "readytopurchase", "name": "Ready to Purchase" },
            { "id": "dcclosed", "name": "Deal Closed" },
            { "id": "llunqualified", "name": "Lead Lost - UnQualified" },
            { "id": "lldelay", "name": "Lead Lost - Delay in Approach" },
            { "id": "llprice", "name": "Lead Lost - Prices Not Competitive" },
            { "id": "llspec", "name": "Lead Lost - Technical Specs Not Met" },
            { "id": "llrelevance", "name": "Lead Lost - Irrelevant Lead" }
        ];

        console.log(" this.loggedInUser " + JSON.stringify(this.loggedInUser));
        this.patchValues();
        this.valueChanges();

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole) {
            this.isEngineer = true;
        }
        else {
            this.isEngineer = false;
        }
    }

    valueChanges() {
        this.serviceEngineerForm.valueChanges.subscribe(data => {
            console.log("DATA------", data);
            if (data.leadStatus !== null && data.leadStatus !== "") {
                if (data.leadStatus == "rejected") {
                    this.showRejctionsField = true;
                }
                else if (data.leadStatus == "qualified") {
                    this.showRejctionsField = false;
                }
            }

        });
    }





    deleteKeyPerson(is) {
        var index = is;
        console.log("index", is);
        this.keyContactsArray.splice(index, 1);
    }
    addKeyContact() {

        var obj = {
            keyPersonName: "",
            keyPersonRole: "",
            keyPersonContact: ""
        };
        obj.keyPersonName = this.PersonDetailsForm.value.contactname;
        obj.keyPersonRole = this.PersonDetailsForm.value.contactrole;
        obj.keyPersonContact = this.PersonDetailsForm.value.contactcontact;
        this.keyContactsArray.push(obj);
        this.showKeyContactsTable = true;
        this.PersonDetailsForm.patchValue({
            "contactname": "",
            "contactcontact": "",
            "contactrole": ""
        })
        console.log("key person", obj, this.keyContactsArray);
    }


    patchValues() {

        console.log("this.leadToPatch.data.keycontacts", this.leadToPatch.data.keycontacts);
        if (this.leadToPatch.data.keycontacts.length !== 0) {
            this.keyContactsArray = this.leadToPatch.data.keycontacts;
            this.showKeyContactsTable = true
        } else {
            this.showKeyContactsTable = false
        }
        console.log("this.leadToPatch", this.leadToPatch);
        console.log("this.leadToPatch.data.state", this.leadToPatch.data.state);

        console.log("this.programUserInfo.programRole", this.programUserInfo.programRole, "this.stringService.getStaticContents().jswEngineerProgramRole", this.stringService.getStaticContents().jswEngineerProgramRole);
        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole) {
            this.projectInfo = this.formBuilder.group({
                projectname: [this.leadToPatch.data.projectname],
                ownername: [this.leadToPatch.data.ownername],
                projecttype: [this.leadToPatch.data.projecttype],
                expectedvalue: [this.leadToPatch.data.expectedtonnage],
                address: [this.leadToPatch.data.address],
                state: [this.leadToPatch.data.state],
                city: [this.leadToPatch.data.city],
                pincode: [this.leadToPatch.data.pincode],
                expectedSalesPeriod: [this.leadToPatch.data.expectedSalesPeriod]

            });
            console.log("state form value", this.projectInfo.value.state);
            this.PersonDetailsForm = this.formBuilder.group({

                contactname: [, [Validators.required]],
                contactrole: ["", [Validators.required]],
                contactcontact: ["", [Validators.required]],
                remarks: [this.leadToPatch.data.remarks]

            });
            this.serviceEngineerForm = this.formBuilder.group({
                leadStatus: [""]
            })
        } else {
            this.projectInfo = this.formBuilder.group({
                projectname: { value: this.leadToPatch.data.projectname, disabled: true },
                ownername: { value: this.leadToPatch.data.ownername, disabled: true },
                projecttype: { value: this.leadToPatch.data.projecttype, disabled: true },
                expectedvalue: { value: this.leadToPatch.data.expectedtonnage, disabled: true },
                address: { value: this.leadToPatch.data.address, disabled: true },
                state: { value: this.leadToPatch.data.state, disabled: true },
                city: { value: this.leadToPatch.data.city, disabled: true },
                pincode: { value: this.leadToPatch.data.pincode, disabled: true },
                expectedSalesPeriod: { value: this.leadToPatch.data.expectedSalesPeriod, disabled: true }

            });
            console.log("state form value", this.projectInfo.value.state);
            this.PersonDetailsForm = this.formBuilder.group({
                contactname: [""],
                contactrole: [""],
                contactcontact: [""],
                remarks: { value: this.leadToPatch.data.remarks, disabled: true }

            });
            this.serviceEngineerForm = this.formBuilder.group({
                leadStatus: [""],
                reasonForRejection: [""]
            })
        }
        this.getStates();
    }
    getStates(): string[] {
        let states: string[] = [];
        var state = {}
        this.showLoader = true;
        this.leadService.getStatesV2().subscribe(
            (responseObject) => {
                console.log("states response", responseObject);
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_SERVER_ERROR:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        if (responseObject.result) {
                            for (let i = 0; i < responseObject.result.length; i++) {
                                this.statesArray[i] = responseObject.result[i].stateName;
                            }
                        }
                        console.log("this.leadToPatch", this.leadToPatch);
                        for (let j = 0; j < this.statesArray.length; j++) {
                            if (this.leadToPatch.data.state === this.statesArray[j]) {
                                console.log("in if ..............")
                            }
                        }
                        console.log("this.projectInfo.value.state", this.projectInfo.value.state);
                        this.getCities(this.projectInfo.value.state);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_FAIL:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_ALREADY_EXIST:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;
                }
            },
            err => {
                // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger", this.errorMessageService.getErrorMessages().connectionErr);
            }
        );
        return states;
    }

    getCities(state): string[] {
        let cities: string[] = [];
        this.showLoader = true;
        this.leadService.getCitiesV1(state).subscribe(
            (responseObject) => {
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_SERVER_ERROR:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_SUCCESS:
                        this.showLoader = false;
                        console.log("responseObject.result city", responseObject.result);
                        if (responseObject.result) {
                            this.cityArray = [];
                            for (let i = 0; i < responseObject.result.length; i++) {
                                this.cityArray.push(responseObject.result[i].cityName);
                                console.log(" this.cityArray", this.cityArray);

                            }
                        }
                        break;

                    case responseCodes.RESP_AUTH_FAIL:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_FAIL:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;

                    case responseCodes.RESP_ALREADY_EXIST:
                        this.showLoader = false;
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", );
                        break;
                }
            },
            err => {
                // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger", this.errorMessageService.getErrorMessages().connectionErr);
            }
        );
        return cities;
    }

    //Note: This method will also set the global variable(myGlobals) used by other pages
    //so it is essential to load this page before selecting any other page in project
    getLead() {
        this.http.get('http://localhost:1380/lead'
            + '?id=' + this.leadIndex
        ).map(res => res.json()).subscribe(
            data => {
                ;
                this.lead = data;
                this.leadDetails = data;
                this.ProvidedInfo = data.providedby;
                this.createdBy = data.createdby;
                this.visits = data.visits[0];


                // this.initLeadValue();
                myGlobals.setLead(data);
                this.lkeycontacts = { "keycontacts": JSON.parse(JSON.stringify(data.keycontacts)) };
            },
            err => { console.log("Error retrieving Lead information") }
        );
    }

    createDummyLead() {
        this.lead = {
            "projectname": "",
            "ownername": "",
            "projectype": "",
            "expectedvalue": 0,
            "pincode": 0,
            "state": "",
            "district": "",
            "city": "",
            "address": "",
            "keycontacts": [],
            "providedby": { "id": "", "name": "", "contact": "", "role": "" },
            "createdby": { "name": "", "contact": "" },
            "createdate": "",
            "inputmedium": "",
            "assignedto": { "assignid": 0, "name": "" },
            "salescontact": { "assignid": 0, "name": "" },
            "qualified": "false",
            "status": "new",
            "estimation": [],
            "influencers": [],
            "visits": [],
            "sales": [],
            "variance": { "reason": "", "remark": "", "date": "" },
            "history": []
        };
    }

    isSESelected(index: number) {
        ;
        if (this.lead && this.lead.assignedto && this.lead.assignedto.name) {
            return (index === this.lead.assignedto.name);
        }
        return false;
    }

    isSalesContactSelected(index: number) {
        ;
        if (this.lead && this.lead.salescontact && this.lead.salescontact.name) {
            return (index === this.lead.salescontact.name);
        }
        return false;
    }

    isInputMediumSelected(index: number) {
        if (this.lead && this.lead.inputmedium) {
            return (index === this.lead.inputmedium);
        }
        return false;
    }

    isStatusSelected(index: number) {
        ;
        if (this.lead && this.lead.status) {
            return (index === this.lead.status);
        }
        return false;
    }

    isCall() {
        return (this.lead.inputmedium === 'call');
    }
    isSnailMail() {
        return (this.lead.inputmedium === 'mail');
    }
    isEmail() {
        return (this.lead.inputmedium === 'email');
    }
    addContact(kname: string, krole: string, kcontact: string) {
        if (kname === null || kname === "" ||
            krole === null || krole === "" ||
            kcontact === null || kcontact === "") {
            alert("Please input valid name, role and contact details!!");
        } else {
            var kc = { name: kname, role: krole, contact: kcontact };
            this.lead.keycontacts.push(kc);
            kname = "";
        }
    }
    editContact(index: number) {
        alert("edit contact");
    }

    deleteContact(index: number) {
        this.lead.keycontacts.splice(index, 1);
    }

    getServiceUser(serviceuserid: string) {
        var index = 0;
        for (let i = 0; i < this.serviceUsersList.length; i++) {
            if (this.serviceUsersList[i].id === serviceuserid) {
                index = i;

            }
        }
        return this.serviceUsersList[index];
    }

    getSalesUser(salesuserid: string) {
        var index = 0;
        for (let i = 0; i < this.salesUsersList.length; i++) {
            if (this.salesUsersList[i].id === salesuserid) {
                index = i;
            }
        }
        return this.salesUsersList[index];
    }

    //helper Methods
    getLMSUserForId(lmsuserid: string) {
        var index = 0;
        for (let i = 0; i < myGlobals.loginUsers.length; i++) {
            if (myGlobals.loginUsers[i].id === lmsuserid) {
                return myGlobals.loginUsers[i];
            }
        }
        return null;
    }

    getSEforInfluencer(influencerid: string) {
        for (let i = 0; i < myGlobals.masterSEInfluencer.length; i++) {
            if (myGlobals.masterSEInfluencer[i].iid === influencerid) {
                return this.getLMSUserForId(myGlobals.masterSEInfluencer[i].ses[0].seid);
            }
        }
        return null;
    }

    getSEforPin(pincode: number) {
        for (let i = 0; i < myGlobals.masterPinSE.length; i++) {
            if (myGlobals.masterPinSE[i].pincode === pincode) {
                return this.getLMSUserForId(myGlobals.masterPinSE[i].ses[0].seid);
            }
        }
        return null;
    }

    toggleQualified($event) {
        console.log("Qualified", $event);
        // console.log("Qualified", $event.target.checked);
        if ($event.target) {
            if ($event.target.checked == true) {
                this.isQualified = true;
                this.isRejected = false;
                this.isPending = false;
                this.togglePending("");
                this.toggleRejected("");
                const dom1: any = document.getElementById('rejected');
                dom1.checked = false;
                const dom2: any = document.getElementById('pending');
                dom2.checked = false;

            } else {
                this.isQualified = false;
            }
        }
        else {
            this.isQualified = false;
        }
        console.log("isQualified", this.isQualified);
        console.log("isRejected", this.isRejected);
        console.log("isPending", this.isPending);
    }
    toggleRejected($event) {
        console.log("Qualified", $event);
        if ($event.target) {
            if ($event.target.checked == true) {
                this.isQualified = false;
                this.isRejected = true;
                this.isPending = false;
                this.togglePending("");
                this.toggleQualified("");
                const dom1: any = document.getElementById('qualified');
                dom1.checked = false;
                const dom2: any = document.getElementById('pending');
                dom2.checked = false;
            } else {
                this.isRejected = false;
            }
        }
        else {
            this.isRejected = false;
        }
        console.log("isQualified", this.isQualified);
        console.log("isRejected", this.isRejected);
        console.log("isPending", this.isPending);
    }
    togglePending($event) {
        console.log("Qualified", $event);;
        if ($event.target) {
            if ($event.target.checked == true) {
                this.isQualified = false;
                this.isRejected = false;
                this.isPending = true;
                this.toggleRejected("");
                this.toggleQualified("");
                const dom1: any = document.getElementById('qualified');
                dom1.checked = false;
                const dom2: any = document.getElementById('rejected');
                dom2.checked = false;
            } else {
                this.isPending = false;
            }
        }
        else {
            this.isPending = false;
        }
        console.log("isQualified", this.isQualified);
        console.log("isRejected", this.isRejected);
        console.log("isPending", this.isPending);
    }
    //Methods calling Rest APIs
    updateLead() {
        this.googleAnalyticsEventsService.emitEvent("Lead Management", "UpdateInitiated", this.projectInfo.value.projectname);
        var obj = {
            projectname: this.projectInfo.value.projectname,
            ownername: this.projectInfo.value.ownername,
            projecttype: this.projectInfo.value.projecttype,
            expectedtonnage: this.projectInfo.value.expectedvalue,
            address: this.projectInfo.value.address,
            city: this.projectInfo.value.city,
            state: this.projectInfo.value.state,
            pincode: this.projectInfo.value.pincode,
            expectedSalesPeriod: this.projectInfo.value.expectedSalesPeriod,
            keycontacts: this.keyContactsArray,
            providedby: this.leadToPatch.data.providedby,
            providedByName: this.leadToPatch.data.providedByName,
            providedByRole: this.leadToPatch.data.providedByRole,
            createdate: new Date(),
            qualified: this.isQualified,
            rejected: this.isRejected,
            pending: this.isPending,
            status: this.serviceEngineerForm.value.leadStatus,
            reasonforRejection: this.serviceEngineerForm.value.reasonForRejection,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            remarks: this.PersonDetailsForm.value.remarks,

            senderName: "",
            senderRoleName: "",
            senderId: "",
            isDeleted: false,
            notification: true

        };


        if (this.isEngineer === false) {
            obj.senderName = this.configService.getLoggedInUserInfo().firstName + " " + this.configService.getLoggedInUserInfo().lastName;
            obj.senderRoleName = this.programUserInfo.roleName;
            obj.senderId = this.programUserInfo.programUserId;
            console.log('this.serviceEngineerForm.value.leadStatus', this.serviceEngineerForm.value.leadStatus);
            if (this.serviceEngineerForm.value.leadStatus === "qualified") {
                obj.qualified = true;
                obj.rejected = false;
                obj.pending = false;
            }
            if (this.serviceEngineerForm.value.leadStatus === "rejected") {
                obj.qualified = false;
                obj.rejected = true;
                obj.pending = false;
            }
        }

        if (this.deleteLead === true) {
            obj.isDeleted = true;
            obj.status = "Deleted"
        }

        var requestObj = {
            updateInfo: obj,
            providedby: this.configService.getloggedInProgramUser().programUserId,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            leadId: this.leadToPatch.data.leadId
        };
        this.showLoader = true;
        console.log('req', requestObj);
        this.leadService.updateLead(requestObj).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.googleAnalyticsEventsService.emitEvent("Lead Management", "Update", this.projectInfo.value.projectname);

                        console.log("channel Info in home component", responseObject);
                        this.feedbackMessageComponent.updateMessage(true, "Lead Updated Successfully", "alert-success");
                        this.serviceEngineerForm.patchValue({
                            "leadStatus": responseObject.result[0].status
                        })
                        this.isUpdated = true;
                        this.serviceEngineerForm.get('leadStatus').disable();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, "Lead Updation Failed", "alert-success");

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
                window.scrollTo(0, 0);
            },
            err => {
                this.showLoader = false;
            }
        );


    }
    //comparitor of lead projection info change set
    compare(oldlead: any, changedlead: any) {
        var changeset = [];
        if (!(oldlead.projectname === changedlead.projectname)) {
            changeset.push({ projectname: changedlead.projectname });
        }
        if (!(oldlead.ownername === changedlead.ownername)) {
            changeset.push({ ownername: changedlead.ownername });
        }
        if (!(oldlead.projecttype === changedlead.projecttype)) {
            changeset.push({ projecttype: changedlead.projecttype });
        }
        if (!(oldlead.expectedvalue === changedlead.expectedvalue)) {
            changeset.push({ expectedvalue: changedlead.expectedvalue });
        }
        if (!(oldlead.address === changedlead.address)) {
            changeset.push({ address: changedlead.address });
        }
        if (!(oldlead.city === changedlead.city)) {
            changeset.push({ city: changedlead.city });
        }
        if (!(oldlead.district === changedlead.district)) {
            changeset.push({ district: changedlead.district });
        }
        if (!(oldlead.state === changedlead.state)) {
            changeset.push({ state: changedlead.state });
        }
        if (!(oldlead.pincode === changedlead.pincode)) {
            changeset.push({ pincode: changedlead.pincode });
        }
        //alert(JSON.stringify(this.lkeycontacts.keycontacts) + ":::" + JSON.stringify(changedlead.keycontacts));
        if (!(JSON.stringify(this.lkeycontacts.keycontacts) === JSON.stringify(changedlead.keycontacts))) {
            changeset.push({ keycontacts: changedlead.keycontacts });
        }
        if (!(JSON.stringify(oldlead.createdby) === JSON.stringify(changedlead.createdby))) {
            changeset.push({ createdby: changedlead.createdby });
        }
        if (!(oldlead.createdate === changedlead.createdate)) {
            changeset.push({ createdate: changedlead.createdate });
        }
        if (!(oldlead.inputmedium === changedlead.inputmedium)) {
            changeset.push({ inputmedium: changedlead.inputmedium });
        }
        if (!(JSON.stringify(oldlead.assignedto) === JSON.stringify(changedlead.assignedto))) {
            changeset.push({ assignedto: changedlead.assignedto });
        }
        if (!(JSON.stringify(oldlead.salescontact) === JSON.stringify(changedlead.salescontact))) {
            changeset.push({ salescontact: changedlead.salescontact });
        }

        if (!(oldlead.qualified === changedlead.qualified)) {

            changeset.push({ qualified: changedlead.qualified });
        }
        if (!(oldlead.status === changedlead.status)) {
            changeset.push({ status: changedlead.status });
        }
        return changeset;
    }

    validateStatusChange(curLead: any, newLead: any) {
        var validStatusChange = 'true';
        var curStatusId = curLead.status;
        var newStatusId = newLead.status;
        var userRole = this.loggedInUser.role;
        if (newStatusId === "created") {
            if (!(userRole === 'manager')) {
                validStatusChange = "Cannot move status back to Created";
            }
        } else if (newStatusId === "assigned") {
            if (!((curStatusId === "created") || (curStatusId === "assigned"))) {
                if (!(userRole === 'manager')) {
                    validStatusChange = "Cannot move status back to Assigned";
                }
            }
        } else if ((curStatusId === "intrevisit" || curStatusId === "revisit") &&
            (newStatusId === "visitscheduled" || newStatusId === "assigned" || newStatusId === "created")) {
            if (!(userRole === 'manager')) {
                validStatusChange = "Cannot move status from " + curStatusId + " to " + newStatusId;
            }
        } else if (newStatusId === 'readytopurchase') {
            if (!(userRole === 'sales' || userRole === 'serviceengineer')) {
                validStatusChange = "Not Authorized to move status to Ready to Purchase";
            }
        } else if (newStatusId === "dcclosed") {
            if (!(curStatusId === "readytopurchase")) {
                validStatusChange = "Cannot move status to Deal Closed";
            }
            if (!(userRole === 'sales')) {
                validStatusChange = "Not Authorized to move status to Deal Closed";
            }
        }
        return validStatusChange;
    }

    //Menu navigation functions
    projectInformation() {
        this.router.navigate(['lmprojectinfo']);
    }
    mapInfluencer() {
        this.router.navigate(['lminfluencer']);
    }
    visitInformation() {
        this.router.navigate(['lmvisits']);
    }
    estimation() {
        this.router.navigate(['lmestimation']);
    }
    salesInformation() {
        this.router.navigate(['lmsales']);
    }
    variance() {
        this.router.navigate(['lmvariance']);
    }
    history() {
        this.router.navigate(['lmhistory']);
    }
    viewLeads() {
        this.router.navigate(['../home/leads-list']);
    }
    planActivity() {
        this.router.navigate(['leadmanagementplanactivity']);
    }

    reports() {
        this.router.navigate(['lmleadsreport']);
    }

    goBack() {
        this.router.navigate(['login']);
    }
    navigate(pageName) {
        this.router.navigate([pageName]);
    }

    navigateBack() {
        this.router.navigate(['../home/leads-list']);
    }
    cancelNav() {
        if (this.configService.isDashboard === true) {
            this.router.navigate(['../home/dashboard']);
            this.configService.isDashboard = false
        } else {
            this.router.navigate(['../home/leads-list']);
            this.configService.isDashboard = false
        }

    }

    delete() {
        this.deleteLead = true;
        this.updateLead();
    }
}