//
// ===== File globals.ts    
//
'use strict';

export var sep='/';
export var version: string="1.0.0";

//Users and Partners
export let bLoggedIn:boolean = false;
export let userToken:any = null;
export let upType:string = "none";

export let leadindex:string = "";
export let glead:any;
export let bnewlead:boolean;

export let loggedInUser:any = null;

//master data
export let loginUsers:any[] = [];
export let masterSMSE:any[] = [];
export let masterSEInfluencer:any[] = [];
export let masterPinSE:any[] = [];

export function setLeadIndex(newValue: string) {
    leadindex = newValue;
}
export function setLead(lead:any) {
    glead = lead;
}
export function isNewLead(newValue:boolean) {
    bnewlead = newValue;
}
export function setLoggedInUser(newValue:any) {
    loggedInUser = newValue;
}

//master
export function setLoginUsers(newValue:any) {
    loginUsers = newValue;
}
export function setMasterSMSE(newValue:any) {
    masterSMSE = newValue;
}
export function setMasterSEInfluencer(newValue:any) {
    masterSEInfluencer = newValue;
}
export function setMasterPinSE(newValue:any) {
    masterPinSE = newValue;
}