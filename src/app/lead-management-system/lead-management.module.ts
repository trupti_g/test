/**
 * Created by Srikanth.
 */
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { FormControlDirective, FormGroupDirective } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { CommonModule } from "@angular/common";
import { LeadManagementRoutingModule } from "./lead-management.routing.module";
import { FormsModule } from '@angular/forms';
import { LeadManagementService } from "./lead-management.service";
import { CsvService } from "angular2-json2csv";
import { MaterialModule } from '@angular/material';
import { LeadManagementViewLeadsComponent } from "./lead-management-view-leads.component";
import { LeadManagementAddLeadsComponent } from "./lead-management-add-leads.component";
import { LeadManagementLeadDetailsComponent } from "./lead-management-lead-details.component";
import { SharedModule } from '../shared-components/shared.module';
import { FooterModule } from '../footer/footer.module';

@NgModule({
    declarations: [
        LeadManagementViewLeadsComponent, LeadManagementAddLeadsComponent, LeadManagementLeadDetailsComponent
    ],
    imports: [CommonModule, Ng2AutoCompleteModule, LeadManagementRoutingModule, ReactiveFormsModule, FormsModule, MaterialModule, SharedModule, FooterModule],
    exports: [],

    providers: [FormControlDirective, FormGroupDirective, LeadManagementService, CsvService]
})
export class LeadManagementModule {
}
