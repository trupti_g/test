import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  OnChanges,
  NgZone,
  ViewChild
} from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { Http, Headers, RequestOptions } from "@angular/http";
import * as myGlobals from "./globals";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { LeadManagementService } from "./lead-management.service";
import { CsvService } from "../shared-services/csv.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { noConflict } from "q";
import { RequreNumericError } from "survey-angular";
import { StringService } from "../shared-services/strings.service";
// import { JswLoaderComponent } from "../shared-components/jsw.loader";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  templateUrl: "./lead-management-view-leads.component.html",
  styleUrls: ["../app.component.scss"],
  providers: [Device]
  // styleUrls: ['../../css/animation.css','../../css/main.css'],
})
export class LeadManagementViewLeadsComponent implements OnInit {
  fromToDateInvalid: boolean;
  mobilewidth: any;
  displaypc: boolean = true;
  displaymobile: boolean = false;
  programUserInfo: any;
  programInfo: any;
  isTimePeriod: boolean = false;
  leadDuration: any;
  isNoRecords: boolean;
  generateCsv: boolean;
  headerArray: string[];
  printingArray: any[];
  showLoader: boolean;
  public requestObj: any = {};
  private summarylist: any[];
  private lead: any;
  private loggedInUser: any;
  private limit: number;
  private isPrevious: boolean = false;
  private isNext: boolean = false;
  private isCountCanBeShown: boolean;
  private pageCount: number;
  private totalRecords: number;
  private startRecord: number;
  private endRecord: number;
  private skip: number;
  public myVar: boolean = false;
  public FilterForm: FormGroup;
  @ViewChild(FeedbackMessageComponent)
  private feedbackMessageComponent: FeedbackMessageComponent;

  constructor(
    private router: Router,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private zone: NgZone,
    private activatedRoute: ActivatedRoute,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private route: ActivatedRoute,
    private http: Http,
    private leadService: LeadManagementService,
    private _csvService: CsvService,
    public sidebar: SidebarComponent,
    private stringService: StringService,
    private device: Device,
  ) {
    this.mobilewidth = window.screen.width;
    console.log("Width:", this.mobilewidth);
    if (this.mobilewidth <= 576) {
      this.displaypc = false;
      this.displaymobile = true;
    }
    else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
      this.displaypc = false;
      this.displaymobile = true;
    }
    else {
      this.displaypc = true;
      this.displaymobile = false;
    }


    this.leadDuration = this.configService.getLeadDashboardLink();
    this.FilterForm = this.formBuilder.group({
      timeFilter: [""],
      startDate: [""],
      endDate: [""],
      statusFilter: [""]
    });

    this.programUserInfo = configService.getloggedInProgramUser();

    this.FilterForm.valueChanges.subscribe(data => {
      if (data.timeFilter === "thisMonth") {
        this.isTimePeriod = false;
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
        this.requestObj.startDate = startDate.toISOString();
        this.requestObj.endDate = endDate.toISOString();
        delete this.requestObj.isPending;
        delete this.requestObj.isQualified;
        this.leadDuration = null;
      }
      if (data.timeFilter === "lastMonth") {
        this.isTimePeriod = false;
        var endDate = new Date();
        endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1);
        var startDate = new Date(endDate);
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
        this.requestObj.startDate = startDate.toISOString();
        this.requestObj.endDate = endDate.toISOString();
        delete this.requestObj.isPending;
        delete this.requestObj.isQualified;
        this.leadDuration = null;
      }
      if (data.timeFilter === "selectTimePeriod") {
        this.isTimePeriod = true;
        this.leadDuration = null;
        this.thisSelectTimePeriod();
        delete this.requestObj.isPending;
        delete this.requestObj.isQualified;
      }
      if (data.timeFilter === "all" || data.timeFilter === "") {
        this.isTimePeriod = false;
        delete this.requestObj.startDate;
        delete this.requestObj.endDate;
        delete this.requestObj.isPending;
        delete this.requestObj.isQualified;
        this.leadDuration = null;
      }
      if (data.statusFilter !== "" && data.statusFilter !== null && data.statusFilter !== undefined && data.statusFilter !== "all") {
        if (data.statusFilter === "qualified") {
          this.requestObj.qualified = true;
          this.requestObj.rejected = false;
          this.requestObj.pending = false;
        }
        if (data.statusFilter === "pending") {
          this.requestObj.qualified = false;
          this.requestObj.rejected = false;
          this.requestObj.pending = true;
        }
        if (data.statusFilter === "rejected") {
          this.requestObj.qualified = false;
          this.requestObj.rejected = true;
          this.requestObj.pending = false;
        }
      } else {
        delete this.requestObj.qualified;
        delete this.requestObj.rejected;
        delete this.requestObj.pending;
      }

      if (this.isTimePeriod === true) {
        if (!this.FilterForm.value.startDate && !this.FilterForm.value.endDate) {
          this.fromToDateInvalid = true;
        } else {
          this.fromToDateInvalid = false;
        }
      } else {
        this.fromToDateInvalid = false;
      }
      // this.getLead();
    });
  }
  thisSelectTimePeriod() {
    this.requestObj.startDate = this.FilterForm.value.startDate;
    this.requestObj.endDate = this.FilterForm.value.endDate;
    console.log("this.requestObj.startDate", this.requestObj.startDate, "this.requestObj.endDate", this.requestObj.endDate);
  }
  ngOnInit() {
    // google anyalytics page view code
    if(this.device.device === "android" || this.device.device === "ios"){
      console.log("inside android and ios");
      (<any>window).ga.trackView('View Leads')
    } else {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', "View Leads");
          ga('send', 'pageview');
        }
      });
    }
    window.scrollTo(0, 0);
    this.sidebar.close();
    this.sidebar.addExpandClass1('pages1');
    this.pageCount = 1;
    this.totalRecords = 0;
    this.startRecord = 0;
    this.endRecord = 0;
    this.limit = 10;
    this.skip = 0;
    this.summarylist = null;
    this.lead = null;
    myGlobals.isNewLead(false);
    myGlobals.setLead(null);
    myGlobals.setLeadIndex(null);
    this.getLead();
    this.loggedInUser = myGlobals.loggedInUser;
  }

  generateCSV() {
    this.generateCsv = true;
    this.getLead();
  }
  getLead() {
    this.showLoader = true;
    this.requestObj.providedby = this.configService.getloggedInProgramUser().programUserId;
    this.requestObj.programId = this.configService.getprogramInfo().programId;
    this.requestObj.clientId = this.configService.getprogramInfo().clientId;
    this.requestObj.skip = this.skip;
    this.requestObj.limit = this.limit;
    this.requestObj.sort = { createdAt: -1 };
    console.log("this.requestObjthis.requestObj", this.requestObj);

    // In case of engineer admin
    if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole || this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
      delete this.requestObj.providedby;
    };

    if (this.leadDuration === 'currentMonthLeads') {
      var endDate = new Date();
      endDate.setDate(endDate.getDate() + 1);
      var startDate = new Date();
      startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
      this.requestObj.startDate = startDate.toISOString().slice(0, 10);
      this.requestObj.endDate = endDate.toISOString().slice(0, 10);
      this.requestObj.isPending = true;
      this.requestObj.isQualified = true;
    } else if (this.leadDuration === 'lastMonthLeads') {
      var endDate = new Date();
      endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1);
      var startDate = new Date(endDate);
      startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
      this.requestObj.startDate = startDate.toISOString().slice(0, 10);
      this.requestObj.endDate = endDate.toISOString().slice(0, 10);
      this.requestObj.isPending = true;
      this.requestObj.isQualified = true;
    }

    if (this.generateCsv === true) {
      delete this.requestObj.limit;
      delete this.requestObj.skip;
    }
    if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
      delete this.requestObj.providedby
    };
    console.log("this.requestObjthis.requestObj", this.requestObj);
    this.leadService.getLead(this.requestObj).subscribe(
      responseObject => {

        this.myVar = true
        let responseCodes = this.configService.getStatusTokens();
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            this.showLoader = false;
            break;
          case responseCodes.RESP_SERVER_ERROR:
            this.showLoader = false;
            break;
          case responseCodes.RESP_SUCCESS:
            this.showLoader = false;
            this.isNoRecords = false
            console.log("channel Info in home component", responseObject);



            //For IE10
            for (var a = 0; a < responseObject.result.length; a++) {
              console.log("CreatedAt", responseObject.result[a].data.createdAt);
              var date = new Date(responseObject.result[a].data.createdAt);
              var createdAt = date.toDateString();
              responseObject.result[a].data.createdAt = createdAt;
            }

            this.summarylist = responseObject.result;
            console.log("this.summarylist", this.summarylist);
            this.configService.setLeadDashboardLink(null);
            if (this.generateCsv === true) {
              delete this.requestObj.limit;
              delete this.requestObj.skip;
              this.generateLeadCsv();
            }
            if (this.summarylist.length < this.limit) {
              this.isNext = true;
            } else {
              this.isNext = false;
            }

            if (this.pageCount == 1) {
              this.isPrevious = true;
            } else {
              this.isPrevious = false;
            }
            if (this.requestObj.skip == 0) {
              this.totalRecords = responseObject.count;
            }

            // code to show no. of records on navigation page
            if (this.summarylist.length > 0) {
              this.isCountCanBeShown = true;
              if (this.pageCount > 1) {
                this.startRecord =
                  this.pageCount * this.limit - (this.limit - 1);
                this.endRecord =
                  this.startRecord + (responseObject.result.length - 1);
              } else {
                this.startRecord = 1;
                this.endRecord =
                  this.startRecord + (responseObject.result.length - 1);
              }
            } else {
              this.isCountCanBeShown = false;
            }

            //Disable next button if total records are equal to pagination limit
            if (this.totalRecords === this.pageCount * this.limit) {
              // //console.log("counts true");
              this.isNext = true;
            }
            break;
          case responseCodes.RESP_AUTH_FAIL:
            this.summarylist = [];
            this.showLoader = false;
            break;
          case responseCodes.RESP_FAIL:
            this.summarylist = [];
            this.showLoader = false;
            this.isNoRecords = true
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => {
        this.showLoader = false;
      }
    );
  }
  generateLeadCsv() {
    console.log("this.summarylist", this.summarylist);
    this.printingArray = [];

    if (this.summarylist !== null || this.summarylist !== undefined) {
      this.headerArray = [
        "projectname",
        "ownername",
        "projecttype",
        "expectedvalue",
        "address",
        "city",
        "state",
        "pincode",
        "keycontacts",
        "providedby",
        "providedbyRole",
        "userName",
        "qualified",
        "status",
        "programId",
        "clientId",
        "leadId",
        "createdAt",
        "updatedAt"
      ];
      console.log("this.summarylist", this.summarylist);
      for (var i = 0; i < this.summarylist.length; i++) {
        var temp = {
          projectname: "",
          ownername: "",
          projecttype: "",
          expectedvalue: "",
          address: "",
          city: "",
          state: "",
          pincode: 0,
          keycontacts: [],
          providedby: "",
          providedbyRole: "",
          userName: "",
          qualified: "",
          status: "",
          programId: "",
          clientId: "",
          leadId: "",
          createdAt: "",
          updatedAt: ""
        };
        console.log("this.summarylist[i]", this.summarylist[i]);
        temp.projectname = this.summarylist[i].data.projectname;
        temp.ownername = this.summarylist[i].data.ownername;
        temp.projecttype = this.summarylist[i].data.projecttype;
        temp.expectedvalue = this.summarylist[i].data.expectedvalue;
        temp.address = this.summarylist[i].data.address;
        temp.city = this.summarylist[i].data.city;
        temp.state = this.summarylist[i].data.state;
        temp.pincode = this.summarylist[i].data.pincode;
        temp.providedby = this.summarylist[i].data.providedby;
        temp.providedbyRole = this.summarylist[i].data.providedbyRole;
        temp.userName = this.summarylist[i].userName[0];
        temp.qualified = this.summarylist[i].data.qualified;
        temp.status = this.summarylist[i].data.status;
        temp.programId = this.summarylist[i].data.programId;
        temp.clientId = this.summarylist[i].data.clientId;
        temp.leadId = this.summarylist[i].data.leadId;
        temp.createdAt = this.summarylist[i].data.createdAt.slice(0, 10);
        temp.updatedAt = this.summarylist[i].data.updatedAt.slice(0, 10);

        this.printingArray.push(temp);
      }
    }

    this._csvService.download(this.printingArray, "Lead_Report");
  }
  deleteLead(index: number) {
    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    this.http
      .delete(
        "http://localhost:1380/lead/" + this.summarylist[index].id,
        options
      )
      .map(res => res.json())
      .subscribe(
        data => {
          this.summarylist.splice(index, 1);
        },
        err => {
          console.log("Error deleting Lead information");
        }
      );
  }

  viewLeads() {
    //this.getLeads();
    this.router.navigate(["leadmanagementviewleads"]);
  }

  mapInfluencer() {
    this.router.navigate(["leadmanagementinfluencer"]);
  }

  showLead(lead) {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "View", lead.data.projectname);

    var leadDetails = lead;
    this.configService.setLead(leadDetails);
    this.router.navigate(["/home/lead-details"]);
  }

  planActivity() {
    this.router.navigate(["/home/lms/leadmanagementplanactivity"]);
  }

  reports() {
    this.router.navigate(["/home/lms/lmleadsreport"]);
  }

  showAddNewLead() {
    myGlobals.isNewLead(true);
    this.router.navigate(["../lead-management-system/lmprojectinfo"]);
  }

  assignModule() {
    this.router.navigate(["leadmanagement"]);
  }

  reportModule() {
    this.router.navigate(["leadmanagementreport"]);
  }

  goBack() {
    this.router.navigate(["login"]);
  }

  navigate(pageName) {
    this.router.navigate([pageName]);
  }

  prev() {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Previous click", "Previous");
    this.requestObj.skip = this.requestObj.skip - this.limit;
    this.skip = this.requestObj.skip;
    this.pageCount--;
    this.getLead();
  }

  /**
   * METHOD   : next
   * DESC     : to go to the next screen of records
   */
  next() {
    this.googleAnalyticsEventsService.emitEvent("Lead Management", "Next click", "Next");
    this.requestObj.skip = this.requestObj.skip + this.limit;
    this.skip = this.requestObj.skip;
    this.pageCount++;
    this.getLead();
  }

  filter() {
    console.log('this.requestObj', this.requestObj, "page", this.leadDuration);
    this.pageCount = 1;
    this.totalRecords = 0;
    this.startRecord = 0;
    this.endRecord = 0;
    this.limit = 10;
    this.skip = 0;
    this.getLead();
  }
}
