import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
// import { KSSwiperContainer, KSSwiperSlide } from 'angular2-swiper';
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../feedback-message.component";

@Component({
    templateUrl: "./special-offers-boosters.component.html",
    styleUrls: ["../special-offers.scss"],
    // providers: [KSSwiperContainer, KSSwiperSlide]
})

export class SpecialOffersBoostersComponent {
    private showLoader: boolean = false;
    public boosterArr: any = [{}];
    public isNext: boolean;
    public isPrev: boolean;
    public isOffer: boolean;

    // @ViewChild(KSSwiperContainer) swiperContainer: KSSwiperContainer;
    // @ViewChild(KSSwiperContainer) swiper: KSSwiperSlide;
    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService
    ) {
        this.configService.setRewardGalleryFlag(false);
        this.isOffer = false;
    }

    config: Object = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 50,
        autoplay: 3000,
        autoplayDisableOnInteraction: false
    };

    cancelClick() {
        this.router.navigate(['../dashboard'], { relativeTo: this.route });
    }

    // moveNext() {
    //     console.log("asa", this.swiperContainer.swiper.realIndex);
    //     this.isNext = true;
    //     this.swiperContainer.swiper.slideNext();
    // }

    // movePrev() {
    //     console.log("asa", this.swiperContainer.swiper.realIndex);
    //     this.isPrev = true;
    //     this.swiperContainer.swiper.slidePrev();
    // }
}