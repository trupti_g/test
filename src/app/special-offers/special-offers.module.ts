/*
	Author			:	Prasad Pingle
	Description		: 	Contains the screens for boosters and display contest
	Date Created	: 	10/07/2017
	Date Modified	: 	10/07/2017
*/

import { NgModule } from "@angular/core";

import { FooterModule } from '../footer/footer.module';



import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
// import { SwiperModule } from 'angular2-useful-swiper'; //or for angular-cli the path will be ../../node_modules/angular2-useful-swiper 
// import { KSSwiperModule } from 'angular2-swiper';
import { SharedModule } from "../shared-components/shared.module";
//to compress image 
import { Ng2ImgToolsModule } from 'ng2-img-tools';

//Special Offers
import { ContestDetailsComponent } from "./contest/special-offers-contest-details.component";
import { SpecialOffersBoostersComponent } from "./boosters/special-offers-boosters.component";
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

import { OnGoingContestComponent } from "./contest/ongoingcontest.component";
import { ContestComponent } from "./contest/contestdetails.component";
import { UpcomingContestComponent } from "./contest/upcomingcontest.component";
import { UpcomingDetailComponent } from "./contest/upcoming-contest-details.component";
import { PastContestComponent } from "./contest/past-contest.component";
import { PastContestGalleryComponent } from "./contest/past-gallery.component";



@NgModule({
	declarations: [ContestDetailsComponent, SpecialOffersBoostersComponent, OnGoingContestComponent, ContestComponent,
		UpcomingContestComponent, UpcomingDetailComponent, PastContestComponent, PastContestGalleryComponent],

	imports: [CommonModule,
		ReactiveFormsModule,
		HttpModule,
		MaterialModule.forRoot(),
		// KSSwiperModule,
		// SwiperModule,
		FooterModule,
		FormsModule,
		SharedModule,
		Ng2ImgToolsModule,
		NgbCarouselModule.forRoot()],
	exports: [],

	providers: [],
	entryComponents: []
})
export class SpecialOffersModule {
}