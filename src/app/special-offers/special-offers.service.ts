/*
	Author			:	Prasad Pingle
	Description		: 	Contains the services for boosters and display contest
	Date Created	: 	10/07/2017
	Date Modified	: 	10/07/2017
*/


import { Injectable, OnInit } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class SpecialOffersService {
    private headers: any;
    private options: any;
    private frontendUserInfo: any;
    public userInfo;
    private requestObj: any;
    public displayContestObjectBeingProcessed: any = {};


    constructor(private http: Http, private configService: ConfigService) {
        console.log("******8service started*********")
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this
                .configService
                .getAuthenticationToken()
        });
        this.options = new RequestOptions({ headers: this.headers });

        this.frontendUserInfo = this.configService.getFrontEndUserInfo();


        this.userInfo = this.configService.getLoggedInUserInfo();

        this.requestObj = {
            "programId": this
                .configService
                .getloggedInProgramInfo()
                .programId,
            "clientId": this
                .configService
                .getloggedInProgramInfo()
                .clientId
        }

    }




    /******************************************************  SETTER GETTER FUNCTIONS  ***********************************************/
    setDisplayContestObject(displayContestObject) {
        this.displayContestObjectBeingProcessed = displayContestObject;
        // console.log("*********Inservice*******", this.displayContestObjectBeingProcessed);
    }

    getDisplayContestObject() {
        // console.log("before return obj:", this.displayContestObjectBeingProcessed)
        return this.displayContestObjectBeingProcessed;
    }


    /*************************************************  END OF SETTER GETTER FUNCTIONS  *********************************************/


    /*******************************	API CALLS 	***********************************/
    /*
    *    METHOD         : getNonTransactionalBoosters()
    *    DESCRIPTION    :Get non transactional booster list
    */
    getNonTransactionalBoosters(obj): any {
       

        let url = this.configService.getApiUrls().getNonTransactionalBoosters;
        var request = obj;
        // request.frontendUserInfo = this.configService.getFrontEndUserInfo();    
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {
            request.frontendUserInfo = frontEndInfo;
        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                // console.log("inside map", res.json());
                return res.json();
                // return cities;
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }



    getEventRegistration(obj) {
        let url = this.configService.getApiUrls().getEventRegistration;
        var request = obj; 
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo != null || frontEndInfo != undefined) {

            request.frontendUserInfo = frontEndInfo;



        } else {
            request.frontendUserInfo = {};
        }
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        console.log("url" + JSON.stringify(url));
        return this.http.post(url, obj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    };




    /*
    *    METHOD         : addDisplayContest()
    *   DESCRIPTION    :add display contest
    */
    addDisplayContest(requestObj): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;


        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        requestObj.frontendUserInfo = frontEndInfo;
        requestObj.serviceType = "programOperations";
        requestObj.uploadedFrom = "channelApp";
        requestObj.programId = this.configService.getloggedInProgramInfo().programId;
        requestObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        console.log(requestObj);
        let url = this.configService.getApiUrls().addDisplayContest;
        return this.http.post(url, requestObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
   *    METHOD         : getDisplayContest()
   *   DESCRIPTION    :get display contest
   */
    getDisplayContest(boosterId): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;
        let requestObj: any = {};

        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        requestObj.frontendUserInfo = frontEndInfo;
        requestObj.serviceType = "programOperations";
        requestObj.programId = this.configService.getloggedInProgramInfo().programId;
        requestObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        requestObj.boosterId = boosterId;
        requestObj.userId = this.configService.getloggedInProgramUser().programUserId;
        console.log("get DISPLAY CONTEST---------", requestObj.userId);
        console.log(requestObj);
        let url = this.configService.getApiUrls().getDisplayContest;
        return this.http.post(url, requestObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
   *    METHOD         : getDisplayContest()
   *   DESCRIPTION    :get display contest with filter
   */

    getDisplayContests(requestObject): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        requestObject.frontendUserInfo = frontEndInfo;
        requestObject.serviceType = "programOperations";
        requestObject.programId = this.configService.getloggedInProgramInfo().programId;
        requestObject.clientId = this.configService.getloggedInProgramInfo().clientId;
        console.log(requestObject);
        let url = this.configService.getApiUrls().getDisplayContest;
        return this.http.post(url, requestObject, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
   *    METHOD         : updateDisplayContest()
   *   DESCRIPTION    :update display contest
   */
    updateDisplayContest(requestObj): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;


        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        requestObj.frontendUserInfo = frontEndInfo;
        requestObj.serviceType = "programOperations";
        requestObj.uploadedFrom = "channelApp";
        requestObj.programId = this.configService.getloggedInProgramInfo().programId;
        requestObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        console.log(requestObj);
        let url = this.configService.getApiUrls().updateDisplayContest;
        return this.http.post(url, requestObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }



    /******************************* 	END OF API CALLS 	**********************************/
}
