import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../shared-components/feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './past-contest.component.html',
    styleUrls: ['./past-contest.component.scss', "../../app.component.scss"],
    providers: [Device]
})

export class PastContestComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    noRecords1: boolean = false;
    imageArr: any[] = [];
    videoArr: any[] = [];
    recordId: any;
    displayContestArr: any;
    numberOfUsersLimit: any;
    boosterDescription: any;
    boosterId: any;
    programUserInfo: any;
    public noRecords: boolean = false;
    isDealer: boolean = false;
    isDistributor: boolean = false;
    isEngineer: boolean = false;
    public pastContests: any = [];
    startDate: any;
    contestName: any;
    public boosterArr: any = [];
    public isValidUser: boolean;
    private showLoader: boolean = false;
    n: Date;
    m: string[];




    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

      

        this.programUserInfo = configService.getloggedInProgramUser();
        this.isValidUser = false;
        var dateTodayZ = new Date();
        var dateTodayZstring = dateTodayZ.toISOString();
        this.m = dateTodayZstring.split('T')
        this.n = new Date(this.m[0]);
        console.log("NEW DATE", this.n);

    }

    ngOnInit() {
        this.sidebar.close();

        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Past Contests")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Past Contests");
                    ga('send', 'pageview');
                }
            });
        }

        console.log("in oninit", this.configService.isEngineer);
        console.log("in oninit", this.configService.isDistributor);
        console.log("in oninit", this.configService.isDealer);
        this.configService.isEngineer = true;
        this.configService.isDealer = false;
        this.configService.isDistributor = false;
        this.getNonTransactionalBoosters();
        if (this.configService.getloggedInProgramUser() !== undefined && this.configService.getloggedInProgramUser() !== null) {
            if (this.configService.getloggedInProgramUser().userType === "ClientUser") {
                this.isValidUser = false;
            }
            else if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") {
                this.isValidUser = true;
            }
        }

        this.stringService.setImageFormats();
        this.stringService.setVideoFormats();

       
    }

    // function to select contest (ASM login)
    setContestprofile(profile) {
        console.log("profile is ", profile);
        if (profile == 0) {
            this.configService.isDealer = false;
            this.configService.isDistributor = false;
            this.configService.isEngineer = true;
        } else if (profile == 1) {
            this.configService.isDealer = true;
            this.configService.isDistributor = false;
            this.configService.isEngineer = false;
        } else if (profile == 2) {
            this.configService.isDealer = false;
            this.configService.isDistributor = true;
            this.configService.isEngineer = false;

        }
        console.log("l------select contest dealer ", this.configService.isDealer);
        console.log("i-----select contest distr ", this.configService.isDistributor);
        console.log("i-------select contest engg ", this.configService.isEngineer);
        this.pastContests = [];
        this.noRecords = false;
        this.getNonTransactionalBoosters();
    }

    /**
    * METHOD   : getNonTransactionalBoosters
    * DESC     : get Contest list.
    *
    */
    getNonTransactionalBoosters() {

        var obj = {
            programRoleId: this.programUserInfo.programRole,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup",
            boosterAction: "e",
        }

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole;
            delete this.configService.userId;
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            delete this.configService.userId;
            if (this.configService.isDealer == true) {
                console.log("000000000000000000000000000");
                obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            } else if (this.configService.isDistributor == true) {
                console.log("1111111111111111");
                obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            } else {
                console.log("222222222222222222222222");
                obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            }
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole;

        }
        console.log("past contents roleId", obj.programRoleId);
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArr", this.boosterArr);




                            if (this.boosterArr !== undefined) {

                                for (let i = 0; i < this.boosterArr.length; i++) {
                                    if (this.boosterArr[i].boosterAction === "e") {

                                        if (this.boosterArr[i].boosterStartDate && this.boosterArr[i].boosterEndDate) {
                                            var o = this.boosterArr[i].boosterStartDate.split('T');
                                            console.log("o", o);
                                            var x = new Date(o[0]);
                                            var j = this.boosterArr[i].boosterEndDate.split('T');
                                            var y = new Date(j[0]);
                                            console.log("xxxxx", x, "this.n", this.n, "yyyy", y);
                                            if (x < this.n && y < this.n) {
                                                console.log("found---------------");
                                                this.pastContests.push(this.boosterArr[i]);
                                                console.log("this.pastContests", this.pastContests);
                                            } else if ((x > this.n && y > this.n)) {
                                            }
                                        }
                                    }
                                }

                                if (this.pastContests.length === 0) {
                                    this.noRecords = true;
                                }
                                console.log("Past", this.pastContests);


                                for (var a = 0; a < this.pastContests.length; a++) {
                                    var date = new Date(this.pastContests[a].boosterStartDate);
                                    var boosterStartDate = date.toDateString();
                                    this.pastContests[a].boosterStartDate = boosterStartDate;


                                    var date1 = new Date(this.pastContests[a].boosterEndDate);
                                    var boosterEndDate = date1.toDateString();
                                    this.pastContests[a].boosterEndDate = boosterEndDate;
                                }

                            }
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.noRecords = true;
                            this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }


    Details(past) {
        this.googleAnalyticsEventsService.emitEvent("Past Contest", "ViewDetails", past.boosterName);
        console.log("Inside Detail Function");
        console.log("contest Details", past);
        this.boosterId = past.boosterId;
        console.log("Booster Id", this.boosterId);

        // Code to stop backgroung scrolling
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

        this.configService.setModalClose(false);

        document.getElementById("body").style.overflow = "hidden";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);


        var obj = {
            boosterId: this.boosterId,
            programRoleId: this.programUserInfo.programRole,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup",
            boosterAction: "e",
        }
        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
                delete obj.programRoleId;
            // if (this.configService.isDealer == true) {
            //     console.log("2 000000000000000000000000000");
            //     obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            // } else if (this.configService.isDistributor == true) {
            //     console.log("2 1111111111111111");
            //     obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            // } else {
            //     console.log("2 222222222222222222222222");
            //     obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            // }
        }
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArr", this.boosterArr);




                            if (this.boosterArr !== undefined) {

                                for (let i = 0; i < this.boosterArr.length; i++) {
                                    if (this.boosterArr[i].boosterAction === "e") {

                                        this.boosterDescription = this.boosterArr[i].description;

                                    }
                                }
                                console.log("Booster Description", this.boosterDescription);
                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            // this.noRecords = true;
                            this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );

    }



    viewGallery(past) {
        this.googleAnalyticsEventsService.emitEvent("Past Contest", "ViewGallery", past.boosterName);
        console.log("Inside Gallery Function");
        console.log("Past", past);
        this.configService.setPastContestGallery(past);
        this.router.navigate(['../../home/pastcontestgallery']);

        console.log("past event gallery dealer", this.configService.isDealer);

        console.log("past event gallery distr", this.configService.isDistributor);

        console.log("past event gallery engg", this.configService.isEngineer);
    }


    close() {
        this.boosterDescription = "";

        // Code to active backgroung scrolling 
        // var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

        this.configService.setModalClose(true);

        // document.getElementById("body").style.overflow = "auto";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    }

    Winners(past) {
        this.googleAnalyticsEventsService.emitEvent("Past Events", "ViewWinners", past.boosterName);
        console.log("inside Winners Function", this.displayContestArr);
        console.log("past", past);
        this.numberOfUsersLimit = past.numberOfUsersLimit;
        this.boosterId = past.boosterId;
        console.log("this.boosterId", this.boosterId);
        console.log("this.numberOfUserLimit", this.numberOfUsersLimit);
        this.getDisplayContest();

        // Code to stop backgroung scrolling 
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

        this.configService.setModalClose(false);

        document.getElementById("body").style.overflow = "hidden";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

    }


    winnerClose() {
        console.log("Inside Close Function");
        this.noRecords1 = false;
        this.displayContestArr = [];

        // Code to active backgroung scrolling 
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

        this.configService.setModalClose(true);

        document.getElementById("body").style.overflow = "auto";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    }

    getDisplayContest() {
        // to clear previous response
        this.displayContestArr = [];
        this.noRecords1 = false;

        let requestObject: any = {};
        this.showLoader = true;
        requestObject.status = "approved";
        requestObject.limit = this.numberOfUsersLimit;
        requestObject.sort = { "score": -1 }
        // requestObject.programRoleId = this.programUserInfo.programRole;
        requestObject.boosterId = this.boosterId;

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            console.log("display gallery requestObject.userId", requestObject.userId);
            if (this.configService.isDealer == true) {
                console.log("000000000000000000000000000");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            } else if (this.configService.isDistributor == true) {
                console.log("1111111111111111");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            } else {
                console.log("222222222222222222222222");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            }
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole;
            delete requestObject.userId;
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole;
            delete requestObject.userId;
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            delete requestObject.userId;
        }


        console.log("Request Object for getDisplayContest", requestObject);
        this.specialOffersService.getDisplayContests(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();
                    console.log("Response codes", responseCodes);

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            if (responseObject.result)
                                this.displayContestArr = responseObject.result;
                                console.log("getDisplayContest : ", this.displayContestArr);
                             
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            this.noRecords1 = true;
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }




    viewEntry(contest) {
        this.googleAnalyticsEventsService.emitEvent("Past Events", "ViewEntry", contest.boosterId);
        console.log("Past Contest", contest);
        this.recordId = contest.recordId;
        console.log("recordId", this.recordId);
        console.log("this.displayContestArr", this.displayContestArr);

        // intially empty
        this.imageArr = []; 
        this.videoArr = [];
        var flag = false;
        if (this.displayContestArr) {
            for (var i = 0; i < this.displayContestArr.length; i++) {

                if (this.recordId === this.displayContestArr[i].recordId) {
                    console.log("Inside if condition");
                    // this.imageArr = this.displayContestArr[i].image

                    if(this.displayContestArr[i].image){
                        for(let j=0;j<this.displayContestArr[i].image.length;j++){
                            var ext: string = this.displayContestArr[i].image[j].slice(this.displayContestArr[i].image[j].lastIndexOf('.'),this.displayContestArr[i].image[j].length).toLowerCase();
                            
                            var imageFormats=this.stringService.getImageFormats();
                            var videoFormats=this.stringService.getVideoFormats();
                            var isFound=false; //set true if type as image is found
                            console.log("ext",ext);
                            // to check for image
                            for (let x = 0; x < imageFormats.length; x++) {
                                console.log("imageFormats[x]",imageFormats[x]);
                                if (ext === imageFormats[x]) {
                                    console.log("MATCHED!!");
                                    this.imageArr.push(this.displayContestArr[i].image[j]);
                                    isFound=true; 
                                    break;
                                }
                            }

                            // to check for videos
                            if(isFound === false){ // to avoid checking for video if already found as image

                                for (let x = 0; x < videoFormats.length; x++) {
                                    console.log("videoFormats[x]",videoFormats[x]);
                                    if (ext === videoFormats[x]) {
                                        console.log("MATCHED!!");
                                        this.videoArr.push(this.displayContestArr[i].image[j]);
                                        break;
                                    }
                                }
                            }
                        }

                        console.log("final image array", this.imageArr);
                        console.log("final video array",this.videoArr);
                    }
                    flag = true;
                    break;
                }
            }
            if (flag === false) {
                console.log("this.imageArr", this.imageArr);
                this.noRecords1 = true;
            }
            console.log("Image Array", this.imageArr);
            const dom3: any = document.getElementById('closePopup').click();

        }
        console.log("this.videoArr.length",this.videoArr.length);
        // if(this.videoArr.length>0){
        //     for(let i=0;i<this.videoArr.length;i++){
        //         var id = String(i)
        //         var secondvideo = document.getElementById(id);
        //         var statelabel = document.getElementById('videoState');
        //         console.log("secondvideo",secondvideo);
        //         secondvideo.addEventListener('play', function(e) { // Repeat this for other events
        //             // The video is playing
        //             console.log("video is playing");
        //             document.getElementById("body").style.overflow = "hidden";
        //             var scroll = document.getElementById('body');
        //             console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
            
        //         });
        //     }
        // }


        // Code to stop backgroung scrolling 
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

        // this.configService.setModalClose(false);

        // document.getElementById("body").style.overflow = "hidden";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    }
            
  
    entryImageClose() {

        console.log("Inside Image Clsoe Div");
        const dom3: any = document.getElementById('openPopUp').click();

        // this.configService.setModalClose(true);

        // document.getElementById("winner").style.overflow = "auto";

    }


}