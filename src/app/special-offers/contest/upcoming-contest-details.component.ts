import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../shared-components/feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './upcoming-contest-details.component.html',
    styleUrls: ['./upcoming-contest-details.component.scss', "../../app.component.scss"],
    providers: [Device]
})

export class UpcomingDetailComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    noRecords: boolean;
    programUserInfo: any;
    boosterArr: any;
    boosterId: any;
    getUpcomingContest: any;
    private showLoader: boolean = false;





    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
        this.programUserInfo = configService.getloggedInProgramUser();

    }

    ngOnInit() {
        // google anyalytics page view code 
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Upcoming Contests Details")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Upcoming Contests Details");
                    ga('send', 'pageview');
                }
            });
        }
        this.sidebar.close();
        console.log("details onInit");
        console.log("this.configService.isDealer", this.configService.isDealer);
        console.log("this.configService.isDistributor", this.configService.isDistributor);
        console.log("this.configService.isEngineer", this.configService.isEngineer);
        this.getUpcomingContest = this.configService.getUpcomingContestDetails();
        console.log("Upcoming Contest Details", this.getUpcomingContest);
        this.boosterId = this.getUpcomingContest.boosterId;
        console.log("Booster id", this.boosterId);
        this.getNonTransactionalBoosters();
        console.log("ngOninit called");


    }


    backtoupcoingContests() {
        this.router.navigate(['../../home/upcomingcontest']);
    }




    /**
  * METHOD   : getNonTransactionalBoosters
  * DESC     : get Contest list.
  *
  */
    getNonTransactionalBoosters() {
        console.log("Inside get non transction");


        var obj = {
            boosterId: this.boosterId,
            programId: this.configService.getprogramInfo().programId,
            programRoleId: this.programUserInfo.programRole,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup",
            boosterAction: "e",
        }

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole || this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
            this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            console.log("this.configService.isDealer", this.configService.isDealer);
            console.log("this.configService.isDistributor", this.configService.isDistributor);
            console.log("this.configService.isEngineer", this.configService.isEngineer);
            if (this.configService.isDealer == true) {
                console.log("000000000000000000000000000");
                obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            } else if (this.configService.isDistributor == true) {
                console.log("1111111111111111");
                obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            } else {
                console.log("222222222222222222222222");
                obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            }
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
        }
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArr", this.boosterArr);


                            if (this.boosterArr) {

                                for (var a = 0; a < this.boosterArr.length; a++) {
                                    var date = new Date(this.boosterArr[a].boosterStartDate);
                                    var boosterStartDate = date.toDateString();
                                    this.boosterArr[a].boosterStartDate = boosterStartDate;


                                    var date1 = new Date(this.boosterArr[a].boosterEndDate);
                                    var boosterEndDate = date1.toDateString();
                                    this.boosterArr[a].boosterEndDate = boosterEndDate;
                                }
                                console.log("this.boosterArr", this.boosterArr);
                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }














}