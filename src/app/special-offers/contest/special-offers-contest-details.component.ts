import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
@Component({
    templateUrl: './special-offers-contest-details.component.html',
    styleUrls: ['../special-offers.scss'],
    // styleUrls: ['../../app.component.scss'],
    providers: []
})

export class ContestDetailsComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    show1: boolean;
    contestImagePopUpSrc1: any;
    pastContestsIndex1: any;
    show: boolean;
    pastContestsIndex: any;
    upcomingContests: any = [];
    pastContests: any = [];
    n: Date;
    m: string[];
    private forgotPasswordForm: FormGroup;
    private showLoader: boolean = false;
    private showSuccess: boolean = false;
    private invalidUserName: boolean = false;
    private isDisplayContest: boolean = false;
    private message: string;
    public boosterArr: any = [{}];
    public contestObject: any = {};
    public displayContestArr: any = [];
    public personalDisplayContest: any = [];
    public isWinner: boolean;
    public isBeWinner: boolean;

    //Contest Details Variables
    public contestName: string;
    public startDate: string;
    public endDate: string;
    public contestDescription: string;
    public contestTermsAndConditions: string;
    public contestImageSrc: string;
    public contestImagePopUpSrc: string;
    public currentDate: any;
    public resultDate: any;
    public userId: any;
    public programId: any;
    public isValidUser: boolean;
    public clientColor: any;

    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

        this.configService.setRewardGalleryFlag(false);
        this.contestName = "";
        this.startDate = "";
        this.endDate = "";
        this.contestDescription = "";
        this.contestTermsAndConditions = "";
        this.contestImageSrc = "";
        this.isWinner = false;
        this.isBeWinner = false;
        this.isDisplayContest = false;
        this.isValidUser = false;
        this.clientColor = this.configService.getThemeColor();
        this.userId = this.configService.getloggedInProgramUser().programUserId;
        this.programId = this.configService.getloggedInProgramUser().programId;
        console.log("userId : ", this.userId);
        console.log("programId : ", this.programId);
        // Check for winner table view
        this.currentDate = new Date();
        console.log("Current Date : ", this.currentDate);
        this.getDisplayContest();

        var dateTodayZ = new Date();
        var dateTodayZstring = dateTodayZ.toISOString();
        this.m = dateTodayZstring.split('T')
        this.n = new Date(this.m[0]);
        console.log("NEW DATE", this.n);
    }

    ngOnInit() {
        // google anyalytics page view code
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                ga('set', 'page', "Special Offers Contest Details");
                ga('send', 'pageview');
            }
        });
        this.sidebar.close();
        this.getNonTransactionalBoosters();
        if (this.configService.getloggedInProgramUser() !== undefined && this.configService.getloggedInProgramUser() !== null) {
            if (this.configService.getloggedInProgramUser().userType === "ClientUser") {
                this.isValidUser = false;
            }
            else if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") {
                this.isValidUser = true;
            }
        }
    }



    /**
    * METHOD   : getNonTransactionalBoosters
    * DESC     : get Contest list.
    *
    */
    getNonTransactionalBoosters() {


        var obj = {
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup"

        }
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArrthis.boosterArr", this.boosterArr);
                            if (this.boosterArr !== undefined) {
                                for (let i = 0; i < this.boosterArr.length; i++) {
                                    if (this.boosterArr[i].boosterAction === "e") {
                                        this.contestObject = this.boosterArr[i];
                                        this.contestName = this.contestObject.boosterName;
                                        this.startDate = this.contestObject.boosterStartDate;
                                        this.endDate = this.contestObject.boosterEndDate;
                                        this.contestDescription = this.contestObject.description;
                                        this.contestTermsAndConditions = this.contestObject.termsAndConditions;
                                        console.log("contestObject.bannerImages[0] : ", this.contestObject.bannerImages[0]);
                                        this.contestImageSrc = this.contestObject.bannerImages[0];
                                        this.isDisplayContest = true;
                                        this.resultDate = new Date(this.contestObject.resultDate);
                                        console.log("this.currentDate > this.contestObject.resultDate : ", this.currentDate > this.resultDate);
                                        if (this.currentDate > this.resultDate) {
                                            this.isWinner = true;
                                        } else {
                                            this.isWinner = false;
                                        }
                                        console.log("isWinner : ", this.isWinner);



                                        break;
                                    }
                                }
                                for (let i = 0; i < this.boosterArr.length; i++) {
                                    if (this.boosterArr[i].boosterAction === "e") {
                                        if (this.boosterArr[i].boosterStartDate && this.boosterArr[i].boosterEndDate) {
                                            var o = this.boosterArr[i].boosterStartDate.split('T');
                                            var x = new Date(o[0]);
                                            var j = this.boosterArr[i].boosterEndDate.split('T');
                                            var y = new Date(j[0]);
                                            console.log("xxxxx", x, "this.n", this.n, "yyyy", y);
                                            if (x < this.n && y < this.n) {
                                                console.log("found---------------");
                                                this.pastContests.push(this.boosterArr[i]);
                                                console.log("this.pastContests", this.pastContests);
                                            } else if ((x > this.n && y > this.n) || (x < this.n && y > this.n)) {
                                                this.upcomingContests.push(this.boosterArr[i]);
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("Contest object:", this.contestObject);
                            this.specialOffersService.setDisplayContestObject(this.contestObject);

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.isDisplayContest = false;
                            this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }

    /**
    * METHOD   : getDisplayContest
    * DESC     : get Contest list.
    *
    */
    getDisplayContest() {
        let requestObject: any = {};
        this.showLoader = true;
        requestObject.status = "approved";
        this.specialOffersService.getDisplayContests(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            if (responseObject.result)
                                this.displayContestArr = responseObject.result;
                            console.log("getDisplayContest : ", this.displayContestArr);


                            if (this.displayContestArr) {
                                for (let i = 0; i < this.displayContestArr.length; i++) {
                                    if (this.userId == this.displayContestArr[i].userId && this.programId == this.displayContestArr[i].programId) {
                                        console.log("this.userId ", this.userId, "this.displayContestArr[i].userId", this.displayContestArr[i].userId);
                                        console.log("this.programId ", this.programId, "this.displayContestArr[i].programId", this.displayContestArr[i].programId);
                                        this.personalDisplayContest = this.displayContestArr[i];
                                        this.personalDisplayContest.userName = this.displayContestArr[i].businessUserInfo.userDetails.userName;
                                        this.personalDisplayContest.index = (i + 1);
                                    }
                                }
                                if (this.personalDisplayContest.index == 1) {
                                    this.isBeWinner = true;
                                }
                            }

                            console.log("personalDisplayContest : ", this.personalDisplayContest);
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }


    imageUploadClick() {
        this.router.navigate(['../contest-image'], { relativeTo: this.route });
    }


    cancelClick() {
        this.router.navigate(['../dashboard'], { relativeTo: this.route });
    }

    setIndex(index) {
        console.log("indexindexindex", index);
        this.pastContestsIndex = index;
        console.log("this.pastContestsIndex", this.pastContestsIndex);
        this.contestImagePopUpSrc = this.pastContests[index].bannerImages[0];
        this.show = true;
        console.log("this.contestImagePopUpSrc", this.contestImagePopUpSrc);
    }
    setIndex1(index) {
        console.log("indexindexindex", index);
        this.pastContestsIndex1 = index;
        console.log("this.pastContestsIndex", this.pastContestsIndex1);
        this.contestImagePopUpSrc1 = this.upcomingContests[index].bannerImages[0];
        this.show1 = true;
        console.log("this.contestImagePopUpSrc", this.contestImagePopUpSrc1);
    }
}