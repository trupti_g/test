import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../shared-components/feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './past-gallery.component.html',
    styleUrls: ['./past-gallery.component.scss', "../../app.component.scss"],
    providers: [Device]
})

export class PastContestGalleryComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    numberOfUsersLimit: any;
    boosterName: any;
    displayContestArr: any;
    getPastContestGallery: any;
    boosterDescription: any;
    boosterId: any;
    programUserInfo: any;
    public noRecords: boolean = false;
    public pastContests: any = [];
    startDate: any;
    contestName: any;
    public boosterArr: any = [];
    public isValidUser: boolean;
    private showLoader: boolean = false;
    n: Date;
    m: string[];

    public imageGallery: any = [];
    public videoGallery: any = [];
    public isZoom:boolean = false;
    public imageURL:any;


    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

      

        this.programUserInfo = configService.getloggedInProgramUser();


    }

    ngOnInit() {
        this.sidebar.close();

        // google anyalytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Past Contest Gallery")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Past Contest Gallery");
                    ga('send', 'pageview');
                }
            });
        }

        this.stringService.setImageFormats();
        this.stringService.setVideoFormats();
        this.getPastContestGallery = this.configService.getPastContestGallery();
        console.log("this.getPastContestGallery", this.getPastContestGallery);
        this.boosterId = this.getPastContestGallery.boosterId;
        this.boosterName = this.getPastContestGallery.boosterName;
        console.log("this.boosterId", this.boosterId);
        this.numberOfUsersLimit = this.getPastContestGallery.numberOfUsersLimit;
        console.log("this.numberOfUserLimit", this.numberOfUsersLimit);
        this.getDisplayContest();
        console.log("select contest dealer ",this.configService.isDealer);
        console.log("select contest distr ",this.configService.isDistributor);
        console.log("select contest engg ",this.configService.isEngineer);
    }

    // function to select contest (ASM login)
    setContestprofile(profile){
        console.log("profile is ",profile);
        if(profile == 0){
            this.configService.isDealer = true;
            this.configService.isDistributor = false;
            this.configService.isEngineer = false;
        }else if(profile == 1){
            this.configService.isDealer = false;
            this.configService.isDistributor = true;
            this.configService.isEngineer = false;
        }else if(profile == 2){
            this.configService.isDealer = false;
            this.configService.isDistributor = false;
            this.configService.isEngineer = true;
        }
       
        this.imageGallery = [];
        this.noRecords = false;
        this.getDisplayContest();
    }


    backtopastContests() {
        this.router.navigate(['../../home/pastcontest']);
    }


    /**
   * METHOD   : getDisplayContest
   * DESC     : get Contest list for showing image gallery
   *
   */
    getDisplayContest() {
        let requestObject: any = {};
        this.showLoader = true;
        requestObject.status = "approved";
        // requestObject.programRoleId = this.programUserInfo.programRole;
        requestObject.boosterId = this.boosterId;
        requestObject.userId = this.programUserInfo.programUserId;

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole;
            delete  requestObject.userId;
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole;
            delete  requestObject.userId;

        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            delete  requestObject.userId;
            console.log("display gallery requestObject.userId",requestObject.userId);
            if(this.configService.isDealer == true){
                console.log("000000000000000000000000000");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            }else if(this.configService.isDistributor == true){
                console.log("1111111111111111");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            }else{
                console.log("222222222222222222222222");
                // requestObject.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            } 
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            // requestObject.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            delete  requestObject.userId;
        }

        console.log("Request Object for getDisplayContest", requestObject);
        this.specialOffersService.getDisplayContests(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();
                    console.log("Response codes", responseCodes);

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            if (responseObject.result)
                                this.displayContestArr = responseObject.result;
                            console.log("getDisplayContest : ", this.displayContestArr);

                            if (this.displayContestArr) {
                                for (var a = 0; a < this.displayContestArr.length; a++) {
                                    if (this.displayContestArr[a].image) {
                                        for (var b = 0; b < this.displayContestArr[a].image.length; b++) {

                                            // new code added by trupti to separate images and videos

                                            // to extract and store extension from the media
                                            var ext: string = this.displayContestArr[a].image[b].slice(this.displayContestArr[a].image[b].lastIndexOf('.'),this.displayContestArr[a].image[b].length).toLowerCase();
                                            console.log("ext",ext);
                                            
                                            var imageFormats=this.stringService.getImageFormats();
                                            var videoFormats=this.stringService.getVideoFormats();
                                            var isFound=false; //set true if type as image is found

                                            // to check for image
                                            for (let x = 0; x < imageFormats.length; x++) {
                                                console.log("imageFormats[x]",imageFormats[x]);
                                                if (ext === imageFormats[x]) {
                                                    console.log("MATCHED!!");
                                                    this.imageGallery.push(this.displayContestArr[a].image[b]);
                                                    isFound=true; 
                                                    break;
                                                }
                                            }

                                            // to check for videos
                                            if(isFound === false){ // to avoid checking for video if already found as image

                                                for (let x = 0; x < videoFormats.length; x++) {
                                                    console.log("videoFormats[x]",videoFormats[x]);
                                                    if (ext === videoFormats[x]) {
                                                        console.log("MATCHED!!");
                                                        this.videoGallery.push(this.displayContestArr[a].image[b]);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("this.imageGallery", this.imageGallery);
                            console.log("this.videoGallery", this.videoGallery);
                                
                            if (this.displayContestArr.length === 0) {
                                this.noRecords = true;
                            }


                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            this.noRecords = true;
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }


    zoom(n) {
		this.configService.setModalClose(false);
		if (this.isZoom === false) {
			this.isZoom = true;
			this.imageURL = n;
		}
    }
    
    closeDetails() {
		this.configService.setModalClose(true);
		if (this.isZoom === true) {
			this.isZoom = false;
		}
	}

}