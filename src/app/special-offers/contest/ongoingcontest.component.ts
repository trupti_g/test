import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../shared-components/feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './ongoingcontest.component.html',
    styleUrls: ['./ongoingcontest.component.scss', "../../app.component.scss"],
    providers: [Device]
})

export class OnGoingContestComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    noRecords: boolean = false;
    // isDealer: boolean = false;
    // isDistributor: boolean = false;
    // isEngineer: boolean = false;
    programUserInfo: any;
    public ongoingContests: any = [];
    startDate: any;
    contestName: any;
    public boosterArr: any = [];
    public isValidUser: boolean;
    private showLoader: boolean = false;
    n: Date;
    m: string[];


    public bannerImage: string =  "";


    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
            console.log("display mobile val", this.displaymobile);
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

        this.programUserInfo = configService.getloggedInProgramUser();

        this.isValidUser = false;
        var dateTodayZ = new Date();
        var dateTodayZstring = dateTodayZ.toISOString();
        this.m = dateTodayZstring.split('T')
        this.n = new Date(this.m[0]);
        console.log("NEW DATE", this.n);

    }

    ngOnInit() {
        this.sidebar.close();

        // google analytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Ongoing Contest")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Ongoing Contest");
                    ga('send', 'pageview');
                }
            });
        }
        
        this.configService.isDealer = true;
        this.sidebar.addExpandClass('pages');
        this.sidebar.addExpandClass1('pages1');
        this.configService.isEngineer = true;
        this.configService.isDealer = false;
        this.configService.isDistributor = false;
        this.getNonTransactionalBoosters();
        if (this.configService.getloggedInProgramUser() !== undefined && this.configService.getloggedInProgramUser() !== null) {
            if (this.configService.getloggedInProgramUser().userType === "ClientUser") {
                this.isValidUser = false;
            }
            else if (this.configService.getloggedInProgramUser().userType === "ChannelPartner") {
                this.isValidUser = true;
            }
        }

        // for desktop
        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole && this.displaypc){ //for engineer
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/comp.jpg";
        } else if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole && this.displaypc){ //for distributor
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/comp.jpg";
        } else if((this.programUserInfo.programRole !== this.stringService.getStaticContents().jswDistributorProgramRole
        && this.programUserInfo.programRole !== this.stringService.getStaticContents().jswEngineerProgramRole) && this.displaypc){ //for dealer and other users
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/comp.jpg";
        }

        //for mobile
        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole && this.displaymobile){ //for engineer
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/mobile/contest.png";
        } else if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole && this.displaymobile){
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/mobile/contest.png";
        } else if((this.programUserInfo.programRole !== this.stringService.getStaticContents().jswDistributorProgramRole
        && this.programUserInfo.programRole !== this.stringService.getStaticContents().jswEngineerProgramRole) && this.displaymobile){
            this.bannerImage = "https://s3.ap-south-1.amazonaws.com/phoenix2-content-files/jsw-assets/assets/images/mobile/contest.png";
        }

    }
    
    // function to select contest (ASM login)
    setContestprofile(profile){
        console.log("profile is ",profile);
        if(profile == 0){
            this.configService.isDealer = true;
            this.configService.isDistributor = false;
            this.configService.isEngineer = false;
        }else if(profile == 1){
            this.configService.isDealer = false;
            this.configService.isDistributor = true;
            this.configService.isEngineer = false;
        }else if(profile == 2){
            this.configService.isDealer = false;
            this.configService.isDistributor = false;
            this.configService.isEngineer = true;
        }
        this.ongoingContests = [];
        this.noRecords = false;
        this.getNonTransactionalBoosters();
    }

    contestDetails(contest) {
        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "View", contest.boosterId);
        console.log("Inside contest Details");
        console.log("Contest", contest);

        this.configService.setContestDetails(contest);
        this.router.navigate(['../../home/contestDetails']);
    }



    /**
    * METHOD   : getNonTransactionalBoosters
    * DESC     : get Contest list.
    *
    */
    getNonTransactionalBoosters() {

        var obj = {
            programId: this.configService.getprogramInfo().programId,
            programRoleId: this.programUserInfo.programRole,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup",
            boosterAction: "e",
        }

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            if(this.configService.isDealer == true){
                console.log("000000000000000000000000000");
                obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            }else if(this.configService.isDistributor == true){
                console.log("1111111111111111");
                obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            }else{
                console.log("222222222222222222222222");
                obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            }    
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
        }
        console.log("ongoing contest obj.programRoleId ",obj.programRoleId);
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArr", this.boosterArr);


                            if (this.boosterArr !== undefined) {

                                for (let i = 0; i < this.boosterArr.length; i++) {
                                    if (this.boosterArr[i].boosterAction === "e") {
                                        if (this.boosterArr[i].boosterStartDate && this.boosterArr[i].boosterEndDate) {
                                           
                                            var o = this.boosterArr[i].boosterStartDate.split('T');
                                            console.log("o", o);
                                            var x = new Date(o[0]);
                                            var j = this.boosterArr[i].boosterEndDate.split('T');
                                            var y = new Date(j[0]);
                                            console.log("xxxxx", x, "this.n", this.n, "yyyy", y);
                                            if (x < this.n && y < this.n) {
                                                console.log("found---------------");
                                            } else if ((x == this.n || y >= this.n) && (x <= this.n || y == this.n)) {
                                                console.log("Inside else if");
                                                this.ongoingContests.push(this.boosterArr[i]);

                                                // to change banner dynamically
                                                if(this.boosterArr[i].showBanner != undefined && this.boosterArr[i].showBanner === true){
                                                    if(this.boosterArr[i].bannerImages && this.boosterArr[i].bannerImages.length>0){
                                                        this.bannerImage = this.boosterArr[i].bannerImages[0];
                                                    }
                                                   
                                                }
                                            }
                                        }
                                    }
                                }


                                if (this.ongoingContests.length === 0) {
                                    this.noRecords = true;
                                }

                                console.log("Ongoing", this.ongoingContests);


                                for (var a = 0; a < this.ongoingContests.length; a++) {

                                    var date = new Date(this.ongoingContests[a].boosterStartDate);
                                    var boosterStartDate = date.toDateString();
                                    this.ongoingContests[a].boosterStartDate = boosterStartDate;


                                    var date1 = new Date(this.ongoingContests[a].boosterEndDate);
                                    var boosterEndDate = date1.toDateString();
                                    this.ongoingContests[a].boosterEndDate = boosterEndDate;
                                }

                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.noRecords = true;
                            this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }



}