import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from "@angular/forms";
import { ConfigService } from "../../shared-services/config.service";
import { SpecialOffersService } from "../special-offers.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../../shared-components/feedback-message.component";
import { SidebarComponent } from '../../home/sidebar/sidebar.component';
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { Ng2ImgToolsService } from 'ng2-img-tools';
import { S3UploadService } from "../../shared-services/s3-upload.service";
import { ISubscription } from "rxjs/Subscription";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './contestdetails.component.html',
    styleUrls: ['./contestdetails.component.scss', "../../app.component.scss"],
    providers: [Device]
})

export class ContestComponent implements OnInit {
    public meter: number = 0;
    previewVideosSrcPast: any = [];
    imageFormArrayLength: number = 0;
    previewVideoSrc: any[] = [];
    selectedVideoIndex: number = 0;
    previewImagesSrcPast: any = [];
    boosterObject: any;
    displayContestArr: any = [];
    displayContestCount: any;
    isUpdateImages: boolean = false;
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    programUserInfo: any;
    public ImageCount: number = 0;
    atleastone: boolean = false;
    index: number;
    showimage: boolean = false;
    displayImg: boolean = false;
    y: any;
    countNumber: any = [];
    termsAndConditions: any;
    description: any;
    boosterDescription: any;
    boosterId: any;
    boosterArr: any = [];
    contestDetails: any;
    private showLoader: boolean = false;
    private contestregistrationForm: FormGroup;
    private contestregistrationVideoForm: FormGroup;
    private contestregistrationImageForm: FormGroup;
    files: any = [];
    public apiCounter: number;

    public src = new BehaviorSubject("");
    public videoSrc = new BehaviorSubject("");

    public previewImagesSrc: any[] = [];
    public showPreviewImage: boolean[] = [];
    public isShowImage: boolean = false;
    public selectedImageIndex: number = 0;
    private subscription: ISubscription;
    public pathArray: string[] = [];
    public displayContestObject: any = {};
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public canParticipate:boolean = false;
    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private specialOffersService: SpecialOffersService,
        private formBuilder: FormBuilder,
        private formBuilder1: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private s3UploadService: S3UploadService,
        private ng2ImgToolsService: Ng2ImgToolsService,
        private device: Device,) {

        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }


        this.programUserInfo = configService.getloggedInProgramUser();

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole 
        || this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole 
        || this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole
        ) {
			this.canParticipate = true;
		}

        this.boosterObject = this.configService.getContestDetails();
        console.log("this.boosterObject", this.boosterObject);
        console.log("this.displayContestObject", this.displayContestObject);
        if (this.displayContestObject) {
            if (this.displayContestObject.image !== undefined && this.displayContestObject.image !== null) {
                this.isUpdateImages = true;
            }
        }
        this.contestregistrationForm = this.formBuilder.group({

            nameofMember: ["", [Validators.required]],
            nameofParticipant: ["", [Validators.required]],
            age: ["", [Validators.required, Validators.maxLength(3)]],
            studyingIn: [""],
            relationship: ["", [Validators.required]],
            gender: ["", [Validators.required]]


        });

        this.contestregistrationImageForm = this.formBuilder.group({
            upload: [],
            imageDetails: this.formBuilder.array([this.initImageFormArr()])
        })
        this.contestregistrationVideoForm = this.formBuilder1.group({
            videoUpload: [],
            videoDetails: this.formBuilder1.array([this.initVideoFormArr()])
        })

        this.contestregistrationForm.valueChanges.subscribe(data => {
            if (data.fileUpload === 'image') {
                this.contestregistrationVideoForm.reset();
            } else if (data.fileUpload === 'video') {
                this.contestregistrationImageForm.reset();
            }
        });


    }


    fileOverBase(e: any): void {
        console.log("eee1", e);
        this.hasBaseDropZoneOver = e;
    }

    fileOverAnother(e: any): void {
        console.log("eee2", e);
        this.hasAnotherDropZoneOver = e;
    }
    ngOnInit() {
        this.sidebar.close();

        // google anyalytics page view code
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Contest Details")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Contest Details");
                    ga('send', 'pageview');
                }
            });
        }

        this.isShowImage = false;
        
        // this.showPreviewImage[] = true;
        this.sidebar.addExpandClass('pages');
        this.sidebar.addExpandClass1('pages1');
        this.contestDetails = this.configService.getContestDetails();
        this.stringService.setImageFormats();
        this.stringService.setVideoFormats();
        console.log("Contest Details", this.contestDetails);
        this.boosterId = this.contestDetails.boosterId;
        this.displayImg = false;
        if (this.boosterId) {
            this.getNonTransactionalBoosters();
        }


        this.contestregistrationForm.patchValue({
            "nameofMember": this.configService.getLoggedInUserInfo().fullName

        });

        console.log("FullName", this.configService.getLoggedInUserInfo().fullName);


        this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
            console.log("value", value);
            console.log("value.length",value.length);
            if (value.length > 0) {
                let reqObj: any = {};
                let pathArray: string[] = [];

                for (let i = 0; i < value.length; i++) {
                    console.log("i",i);
                    console.log("value[i]",value[i]);
                    if(value[i] !== undefined){
                        pathArray.push(value[i].Location);
                    }
                   
                }
                reqObj.image = pathArray;
                reqObj.frontendUserInfo = {};
                reqObj.userId = this.programUserInfo.programUserId;
                reqObj.state = this.programUserInfo.userDetails.state;
                reqObj.city = this.programUserInfo.userDetails.city;
                reqObj.boosterId = this.boosterObject.boosterId;
                reqObj.nameOfMember = this.contestregistrationForm.value.nameofMember;
                reqObj.nameOfParticipant = this.contestregistrationForm.value.nameofParticipant;
                reqObj.ageOfParticipant = this.contestregistrationForm.value.age;
                reqObj.relationshipWithMember = this.contestregistrationForm.value.relationship;
                if (this.contestregistrationForm.value.gender !== null || this.contestregistrationForm.value.gender !== undefined || this.contestregistrationForm.value.gender !== "") {
                    reqObj.gender = this.contestregistrationForm.value.gender;
                };
                reqObj.studyingIn = this.contestregistrationForm.value.studyingIn;
                reqObj.status = "pending";
                reqObj.score = 0;
                console.log("Path array:", pathArray);
                console.log("reqobject:", reqObj);
                console.log("pathArray.length", pathArray.length);
                console.log("this.contestDetails", this.contestDetails);
                for (var i = 0; i < pathArray.length; i++) {
                    console.log("pathArray[i]", pathArray[i]);
                    var uploadFileArr = pathArray[i].split(".");
                    console.log("uploadFileArr", uploadFileArr);
                    var ext = "." + uploadFileArr[uploadFileArr.length - 1];
                    console.log("ext", ext);
                    var imageformats = this.stringService.getImageFormats();
                    console.log("imageformats", imageformats);
                    console.log("imageformats.indexOf(ext)", imageformats.indexOf(ext));
                    if (imageformats.indexOf(ext) == -1) {
                        console.log("Upload video");
                        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Upload video", this.contestDetails.boosterName);
                    }
                    else {
                        console.log("Upload image");
                        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Upload image", this.contestDetails.boosterName);
                    }

                }
                // if (uploadFileArr)
                //     this.addDisplayContest(reqObj);

                if (this.apiCounter === 0) {
                    console.log("in If", this.isUpdateImages);
                    if (this.isUpdateImages === false) {
                        this.addDisplayContest(reqObj);
                    }
                    else {
                        console.log("Here--------------------------------");
                        reqObj = {};
                        reqObj.updateInfo = {};
                        reqObj.updateInfo.image = [];
                        reqObj.frontendUserInfo = {};
                        reqObj.recordId = this.displayContestObject.recordId;
                        reqObj.nameOfMember = this.contestregistrationForm.value.nameofMember;
                        reqObj.nameOfParticipant = this.contestregistrationForm.value.nameofParticipant;
                        reqObj.ageOfParticipant = this.contestregistrationForm.value.age;
                        reqObj.relationshipWithMember = this.contestregistrationForm.value.relationship;
                        if (this.contestregistrationForm.value.gender !== null || this.contestregistrationForm.value.gender !== undefined || this.contestregistrationForm.value.gender !== "") {
                            reqObj.gender = this.contestregistrationForm.value.gender;
                        };
                        reqObj.studyingIn = this.contestregistrationForm.value.studyingIn;
                        reqObj.updateInfo.image = pathArray;
                    }
                    this.apiCounter = 1;
                }
                else {
                    console.log("in else")
                }
            }
        });


        this.src.subscribe((value) => {
            this.showPreviewImage[this.selectedImageIndex] = true;
            this.previewImagesSrc[this.selectedImageIndex] = value;
        });
        this.videoSrc.subscribe((value) => {
            this.previewVideoSrc[this.selectedVideoIndex] = value;
        });
        // this.getDisplayContest();
    }


    initImageFormArr() {
        return this.formBuilder.group({
            upload: [""]

        });
    }
    addFormClick(i) {
        this.imageFormArrayLength = i;
        const control = <FormArray>this.contestregistrationImageForm.controls['imageDetails'];
        console.log("control length", control.length);
        control.push(this.initImageFormArr());
    }

    initVideoFormArr() {
        return this.formBuilder1.group({
            videoUpload: [""]
        });
    }

    addFormClickForVideo() {
        const control = <FormArray>this.contestregistrationVideoForm.controls['videoDetails'];
        control.push(this.initVideoFormArr());
    }

    /**
     * METHOD   : removeFormClick
     * DESC     : It listens remove form click & removes form.
     */
    removeFormClick(j: number) {
        console.log("Inside Remove Form video Click", j);
        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Remove video");
        const control1 = <FormArray>this.contestregistrationVideoForm.controls['videoDetails'];
        console.log(control1);
        control1.removeAt(j);
    }

    removeFormImageClick(j: number, src) {
        this.isShowImage = false;
        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Remove image");
        console.log("Inside Remove Form video Click", j);
        console.log("srccccc", src);
        const control1 = <FormArray>this.contestregistrationImageForm.controls['imageDetails'];
        console.log(control1);
        control1.removeAt(j);
        console.log("this.s3UploadService.params.length",this.s3UploadService.params.length);
        this.s3UploadService.params.splice(j, 1);
        this.showPreviewImage.splice(j, 1);
        this.previewImagesSrc.splice(j, 1);
    }


    goingContest() {
        console.log("Inside contest Details");
        this.router.navigate(['../../home/ongoingcontest']);
    }



    registerVideo() {
        console.log("Inside Register Function");

        this.apiCounter = 0;
        this.s3UploadService.uploadFilesMultiple();
    }

    register() {
        this.registerImage();
        this.registerVideo();
    }
    registerImage() {
        this.showLoader = true;
        console.log("Inside Register Function");

        this.isUpdateImages = false;

        this.apiCounter = 0;
        this.s3UploadService.uploadFilesMultiple();

    }




    uploadImage(fileInput: any, index) {

        console.log("Index", index);
        console.log("Called onfileInput change", fileInput);
        console.log("size", fileInput.target.files[0]);
        console.log("")
        // this.showPreviewImage[0] = false;
        this.displayImg = true;

        var temp = fileInput.target.files[0].type.split("/");
        var fileType = temp[0];
        if (fileType === "image") {
            if (fileInput.target.files[0].size < this.configService.imageLimit) {
                console.log("fileInput.target.files[0]", fileInput.target.files[0].size);
                this.ng2ImgToolsService.compress([fileInput.target.files[0]], this.configService.compressLimit, true).subscribe(
                    (result) => {
                        //to read the file and generate preview
                        this.getFile(result);
                        this.selectedImageIndex = index;
                        console.log("this.selectedImageIndex", this.selectedImageIndex);
                        this.isShowImage = true;
                        let filePath = "";
                        this.ImageCount++;
                        filePath = "JSW_UploadContents" + "/" + "Contest_Upload" + "/" + "Im";
                        //to form params required for file upload
                        this.s3UploadService.formParamsMultiple(result, filePath, index);

                    }, error => {
                        console.log("in error", error);
                    });
            }
            else {
                console.log("fileInput.target.files[0].size",fileInput.target.files[0].size);
                this.ng2ImgToolsService.compress([fileInput.target.files[0]], this.configService.compressLimit, true).subscribe(
                    (result) => {
                        //to read the file and generate preview
                        this.selectedImageIndex = index;
                        console.log("result",result);
                        this.getFile(result);
                        // this.selectedImageIndex = index;
                        // console.log("this.selectedImageIndex", this.selectedImageIndex);
                        // this.isShowImage = true;
                        // let filePath = "";
                        // this.ImageCount++;
                        // filePath = "JSW_UploadContents" + "/" + "Contest_Upload" + "/" + "Im";
                        // //to form params required for file upload
                        // this.s3UploadService.formParamsMultiple(result, filePath, index);

                    }, error => {
                        console.log("in error", error);
                    });

                    this.feedbackMessageComponent.updateMessage(true, "Please upload image upto size 1MB.", "alert-success");
                    window.scroll(0, 0);
                }
        } else if (fileType === "video") {
            let filePath = "JSW_UploadContents" + "/" + "Contest_Upload" + "/" + "VI";
            this.previewImagesSrc[index] = null;
            this.s3UploadService.formParamsMultiple(fileInput.target.files[0], filePath, index);
        } else {
            var msg: string = "Please select image or video file";
            this.feedbackMessageComponent.updateMessage(true, msg.substring(0, 33), "alert-danger");
            window.scroll(0, 0);
        }




    }

    getFile(fileInput) {

        console.log("inside getFile");
        var file = fileInput		//get selected file
        var reader = new FileReader();				//create reader object

		/*assign (class variable) src to (local variable) src for use in the onload method as class varibale cannot be access 
		  inside the onload method
		*/
        let src = this.src;



        console.log("src", src);

        //called when file is complete reading and 
        reader.onload = function (fileInput) {
            src.next(reader.result);	//Behavior subject to generate an event
        }

        //call the readAsDataURL() method if file is selected
        if (file) reader.readAsDataURL(file);		//read the selected image and generate an event when reading is completed
    }

    uploadVideo(fileInput: any, index) {
        console.log("Called onfileInput change", fileInput);

        console.log("File", fileInput);
        let filePath = "JSW_UploadContents" + "/" + "Contest_Upload" + "/" + "VI";
        this.s3UploadService.formParamsMultiple(fileInput.target.files[0], filePath, index);

    }
    /**
        * METHOD   : getNonTransactionalBoosters
        * DESC     : get Contest list.
        *
        */
    getNonTransactionalBoosters() {

        var obj = {
            programRoleId: this.programUserInfo.programRole,
            boosterId: this.boosterId,
            programId: this.configService.getprogramInfo().programId,
            clientId: this.configService.getprogramInfo().clientId,
            serviceType: "programSetup"

        }

        if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole ||
        this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
            if(this.configService.isDealer == true){
                console.log("000000000000000000000000000");
                obj.programRoleId = this.stringService.getStaticContents().jswDealerProgramRole
            }else if(this.configService.isDistributor == true){
                console.log("1111111111111111");
                obj.programRoleId = this.stringService.getStaticContents().jswDistributorProgramRole
            }else{
                console.log("222222222222222222222222");
                obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
            } 
        } else if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
            obj.programRoleId = this.stringService.getStaticContents().jswEngineerProgramRole
        }
        this.showLoader = true;
        this.specialOffersService.getNonTransactionalBoosters(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.boosterArr = responseObject.result;
                            console.log("this.boosterArr", this.boosterArr);


                            if (this.boosterArr) {
                                for (var a = 0; a < this.boosterArr.length; a++) {
                                    this.boosterDescription = this.boosterArr[a].description
                                    var res;
                                    res = this.boosterArr[a].termsAndConditions;
                                    this.termsAndConditions = res.split("\n");
                                    console.log("RESULT--------------", this.termsAndConditions);

                                }
                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:

                            this.feedbackMessageComponent.updateMessage(true, "No Contest", "alert-danger", "No Contest");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }



    /**
  * METHOD   : addDisplayContest
  * DESC     : add Contest image.
  *
  */
    addDisplayContest(obj) {
        this.showLoader = true;
        this.specialOffersService.addDisplayContest(obj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_SUCCESS:

                            this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Submit", this.contestDetails.boosterName);

                            console.log("Record SuccessFully Added");
                            this.clearImageFormArray();
                            window.scroll(0, 0);
                            this.feedbackMessageComponent.updateMessage(true, "Your entry was succesfully submitted", "alert-success");
                            this.OnReset();
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_FAIL:
                            window.scroll(0, 0);
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }


    /**
   * METHOD   : updateDisplayContest
   * DESC     : update Contest image.
   *
   */
    updateDisplayContest(reqObj) {
        this.showLoader = true;
        this.specialOffersService.updateDisplayContest(reqObj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            console.log("this.feedbackMessageComponent", this.feedbackMessageComponent);
                            this.clearImageFormArray();
                            window.scroll(0, 0);
                            this.feedbackMessageComponent.updateMessage(true, "Your entry is submitted", "alert-success");
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, "Error while registration.", "alert-success");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", "Contest Record Already Exist!");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }

    /**
       * METHOD   : getDisplayContest
       * DESC     : get Contest list.
       *
       */
    getDisplayContest() {
        this.showLoader = true;

        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "View", "Past Uploads");

        this.specialOffersService.getDisplayContest(this.boosterId)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.getNonTransactionalBoosters();



                            this.displayContestCount = responseObject.count;
                            var tempArray = responseObject.result;
                            this.showPreviewImage = [];
                            // this.showPreviewImage[0] = true;
                            console.log("Response of getDisplayContest : ", tempArray);
                            if (tempArray !== undefined && tempArray !== null) {
                                for (let i = 0; i < tempArray.length; i++) {
                                    if (tempArray[i].userId === this.configService.getloggedInBEInfo().businessId) {
                                        console.log("contest match");
                                        this.displayContestArr.push(tempArray[i]);
                                    }
                                }
                            };
                            console.log("this.displayContestArr", this.displayContestArr);
                            this.showPreviewImage = [];
                            // this.showPreviewImage[0] = true;
                            this.previewImagesSrcPast = [];

                            if (this.displayContestArr) {
                                for (var w = 0; w < this.displayContestArr.length; w++) {
                                    if (this.displayContestArr[w].image !== undefined && this.displayContestArr[w].image !== null) {
                                        console.log("this.displayContestObject.image", this.displayContestArr[w]);
                                        this.isUpdateImages = true;

                                        for (let j = 0; j < this.displayContestArr[w].image.length; j++) {
                                            if (this.displayContestArr[w].image[j] !== null) {
                                                let ext: string = this.displayContestArr[w].image[j].slice(this.displayContestArr[w].image[j].lastIndexOf('.'), this.displayContestArr[w].image[j].length).toLowerCase();
                                                console.log("ext/////", ext);

                                                for (let i = 0; i < this.stringService.getImageFormats().length; i++) {
                                                    console.log("ext", ext);
                                                    console.log("this.stringService.getImageFormats()[i]", this.stringService.getImageFormats()[i]);
                                                    if (ext === this.stringService.getImageFormats()[i]) {
                                                        console.log("this.previewImagesSrcPast", this.previewImagesSrcPast);
                                                        if (this.previewImagesSrcPast.includes(this.displayContestArr[w].image[j]) == false) {
                                                            this.previewImagesSrcPast.push(this.displayContestArr[w].image[j]);
                                                            console.log("MATCHED!");
                                                            break;
                                                        }
                                                    }
                                                }

                                                for (let i = 0; i < this.stringService.getVideoFormats().length; i++) {
                                                    console.log("ext", ext);
                                                    console.log("this.stringService.getImageFormats()[i]", this.stringService.getVideoFormats()[i]);
                                                    if (ext == this.stringService.getVideoFormats()[i]) {
                                                        if (this.previewVideosSrcPast.includes(this.displayContestArr[w].image[j]) == false) {
                                                            this.previewVideosSrcPast.push(this.displayContestArr[w].image[j]);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            console.log("showPreviewImage:", this.showPreviewImage);
                            console.log("previewImagesSrcPast:", this.previewImagesSrcPast);
                            console.log("Contest object:", this.displayContestObject);

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", "No records found");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                    this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }

    removeImage(index) {
        this.previewImagesSrcPast.splice(index, 1);
    };
    removeVideo(index) {
        this.previewVideosSrcPast.splice(index, 1);
    };
    updateContest() {
        var reqObj: any = {};
        reqObj.updateInfo = {};
        reqObj.updateInfo.image = [];
        reqObj.frontendUserInfo = {};
        reqObj.recordId = this.displayContestObject.recordId;
        reqObj.updateInfo.image = [];
        if (this.previewImagesSrcPast) {
            if (this.previewImagesSrcPast.length > 0) {
                for (var b = 0; b < this.previewImagesSrcPast.length; b++) {
                    reqObj.updateInfo.image.push(this.previewImagesSrcPast[b]);
                }
            }
        };
        if (this.previewVideosSrcPast) {
            if (this.previewVideosSrcPast.length > 0) {
                for (var c = 0; c < this.previewVideosSrcPast.length; c++) {
                    reqObj.updateInfo.image.push(this.previewVideosSrcPast[c]);
                }
            }
        };
        reqObj.updateInfo.image = this.previewImagesSrcPast;
        this.updateDisplayContest(reqObj)
    }

    clearImageFormArray() {
        const control1 = <FormArray>this.contestregistrationImageForm.controls['imageDetails'];
        console.log("this.imageFormArrayLength", this.imageFormArrayLength);

        for (var a = 0; a < control1.length; a++) {
            console.log("this.imageFormArrayLength inside", this.imageFormArrayLength);
            control1.removeAt(1);
        }
        this.previewImagesSrc = [];
        this.contestregistrationImageForm.reset();
        this.s3UploadService.emptyAllParams();
    };

    clearVideoFormArray() {
        const control2 = <FormArray>this.contestregistrationVideoForm.controls['imageDetails'];
        // console.log(control2);

        for (var a = 0; a < control2.length; a++) {
            console.log("this.imageFormArrayLength inside", this.imageFormArrayLength);
            control2.removeAt(1);

        }
    }

    tabEvent(tabName) {
        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "View", tabName);
    }

    OnReset() {
        this.contestregistrationForm.reset({});
        this.contestregistrationForm.patchValue({
            "nameofMember": this.configService.getLoggedInUserInfo().fullName

        });
    }

    removeFormFirstImage(j: number, src) {
        this.isShowImage = false;
        this.googleAnalyticsEventsService.emitEvent("Ongoing Contests", "Remove image");
        console.log("Inside Remove Form video Click", j);
        console.log("srccccc", src);
        // const control1 = <FormArray>this.contestregistrationImageForm.controls['imageDetails'];
        // console.log(control1);
        // control1.removeAt(j);
        this.s3UploadService.params.splice(j, 1);
        this.showPreviewImage.splice(j, 1);
        this.previewImagesSrc.splice(j, 1);
        // this.contestregistrationImageForm.controls.imageDetails
        // this.formBuilder.array([this.initImageFormArr()])
        this.contestregistrationImageForm.reset();
    }


}