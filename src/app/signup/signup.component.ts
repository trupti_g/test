
/*
	Author			:	Pratik Gawand
	Description		: 	Component for SignUp page
	Date Created	: 	09 March 2016
	Date Modified	: 	09 March 2016
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";
import { LoginService } from "../login/login.service";
import { matchingPasswords } from '../login/validators';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ProgramSelection } from '../login/programselection.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { FeedbackMessageComponent } from '../feedback-message.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './signup.component.html',
    // styleUrls: ['./signup.component.scss'],
    styleUrls: ["../app.component.scss", './signup.component.scss'],
    providers: [LoginService, Device]
})

export class SignUpComponent implements OnInit {
    mobileerror1: boolean = false;
    mobileNumberToFind: any;
    failureToFind: boolean;
    mobileerror: boolean = false;
    programidArray = [];
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    citiesArrayForUser: string[];
    statesArrayForUser: string[];
    showMarraigeAnniversaryField: boolean;
    public JSWProgramName: any;

    private signUpForm: FormGroup;
    private otpForm: FormGroup;
    private passwordForm: FormGroup;

    public clientName: string = "";
    public businessId: string = "";
    private showLoader: boolean = false;

    userInfo: any;

    private programSelection: any = undefined;
    private statesArray: string[] = [];
    private citiesArray: string[] = [];
    private programsArray: Array<any> = [];

    private showSignUp: boolean = true;
    private showSetPassword: boolean = false;
    private showOTP: boolean = false;

    private showProgramError: boolean = false;

    private showFeedback: boolean = false;
    private showOtpFeedBack: boolean = false;

    private otpfeedBackMessage: string;

    private otpAttempts: number = 2;


    private mobileNumber: number;

    private feedBackMessage: string;


    showStyle: false;
    public selectedIndex: number;
    public noProgramFound: boolean;
    public selectProgram: boolean = false;
    public requestObj: any = {};
    public findCount = 0;

    @ViewChild(ProgramSelection)
    private programSelectionComponent: ProgramSelection;

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
        private stringService: StringService,
        public dialog: MdDialog,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {
        this.requestObj = {};
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
    
        this.initSignUpForm();
        this.initOtpForm();
        this.initSetPasswordForm();



    }

    initOtpForm() {
        this.otpForm = this.formBuilder.group({
            "otp": ["", [Validators.required]],
        });
    }

    initSetPasswordForm() {
        this.passwordForm = this.formBuilder.group({
            "password": ["", [Validators.required, Validators.minLength(4), Validators.maxLength(80)]],
            "confirmPassword": ["", [Validators.required, Validators.minLength(4), Validators.maxLength(80)]],
        }, {
                validator: matchingPasswords('password', 'confirmPassword')
            });

    }



    initSignUpForm() {
        this.signUpForm = this.formBuilder.group({
            "firstName": ["", [Validators.required, Validators.pattern(this.stringService.getRegex().alphaRegex)]],
            "lastName": ["", [Validators.required, Validators.pattern(this.stringService.getRegex().alphaRegex)]],
            "outletName": ["", [Validators.required, Validators.minLength(3), Validators.maxLength(80)]],
            "email": ["", [Validators.required, Validators.pattern(/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/)]],
            "streetAddress": ["", [Validators.required, Validators.minLength(4), Validators.maxLength(80)]],
            "area": ["",],
            "landMark": [""],
            "state": ["", [Validators.required]],
            "city": ["", [Validators.required]],
            "pinCode": ["", [Validators.required, Validators.pattern('[0-9]{6}')]],
            "mobileNumber": ["", [Validators.required, Validators.pattern('[0-9]{10}')]],
            "dateOfBirth": [""],
            "gender": [""],
            "maritalStatus": [""],
            "marriageAnniversary": [""],
            "sameasAbove": ["", []],
            "userStreetAddress": ["", [Validators.required, Validators.maxLength(150)]],
            "userArea": ["", [Validators.maxLength(150)]],
            "userLandmark": ["", [Validators.maxLength(150)]],
            "userPincode": ["", [Validators.required, Validators.pattern('[0-9]{6}')]],
            "userCity": ["", [Validators.required]],
            "userState": ["", [Validators.required]],
        });

        this.signUpForm.valueChanges.subscribe(data => {


            console.log("Data is", data);
        })
    }

    checkMobNo() {
        console.log("changed", this.signUpForm.value.mobileNumber);
        if (this.signUpForm.value.mobileNumber.length == 10) {
            console.log("changed");
            this.onChangeMobileNumber(this.signUpForm.value.mobileNumber);
        }

    }


    onChangeAddress() {
        let sameasAbove = this.signUpForm.value.sameasAbove;
        console.log("sameasAbove", sameasAbove);
        if (sameasAbove === true) {
            console.log("Inside if");
            console.log("City", this.signUpForm.value.city);
            this.signUpForm.patchValue({
                "userStreetAddress": this.signUpForm.value.streetAddress,
                "userArea": this.signUpForm.value.area,
                "userLandmark": this.signUpForm.value.landMark,
                "userPincode": this.signUpForm.value.pinCode,
                "userState": this.signUpForm.value.state,
                "userCity": this.signUpForm.value.city,
            });
            this.getCitiesForUser(this.signUpForm.value.state);
            this.signUpForm.patchValue({
                "userCity": this.signUpForm.value.city
            });

        }
        else {
            this.signUpForm.patchValue({
                "userStreetAddress": "",
                "userArea": "",
                "userLandmark": "",
                "userPincode": "",
                "userState": "",
                "userCity": ""
            });

        }
    }
    onChangeMaritalStatus() {
        let maritalStatus = this.signUpForm.value.maritalStatus;
        if (maritalStatus == "married") this.showMarraigeAnniversaryField = true;
        else if (maritalStatus == "single") this.showMarraigeAnniversaryField = false;
    }

    ngOnInit() {
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView("Registration")
		} else {
            this.router.events.subscribe(event => {
                console.log("Event", event);
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Registration");
                    ga('send', 'pageview');
                }
            });
        }
        this.getStates();
        this.getPrograms();

    }
    showErrorMessage(message) {
        this.feedBackMessage = message;
        this.showFeedback = true;
    }


    goToSignup() {
        this.googleAnalyticsEventsService.emitEvent("Registration", "backToSignUp", this.signUpForm.value.mobileNumber);
        this.showSignUp = true;
        this.showOTP = false;
        this.showSetPassword = false;
    }
    verifyMobileNumber() {

        this.verifyOtp();
    }


    goToSetPassword() {
        this.showSignUp = false;
        this.showOTP = false;
        this.showSetPassword = true;

    }
    goForRegistration() {

        var fullName = this.signUpForm.value.firstName + " " + this.signUpForm.value.lastName;
        console.log("fullName", fullName);
        let requestObject = {

            "kycStatus": false,


            "businessEntityDetails": {
                "businessName": this.signUpForm.value.outletName,
                "state": this.signUpForm.value.state,
                "city": this.signUpForm.value.city,
                "streetAddress": this.signUpForm.value.streetAddress,
                "pincode": this.signUpForm.value.pinCode,
                "programsInterested": this.stringService.getStaticContents().jswprogramId,
                "landmark": this.signUpForm.value.landMark,
                "area": this.signUpForm.value.area,
                "createdBy": "self",
                "dataSource": "SelfRegister"
            },
            "businessEntityUsers": [
                {
                    "firstName": this.signUpForm.value.firstName,
                    "lastName": this.signUpForm.value.lastName,
                    "fullName": fullName,
                    "mobileNumber": this.signUpForm.value.mobileNumber,
                    "email": this.signUpForm.value.email,
                    "password": this.passwordForm.value.password,
                    "userType": "owner",
                    "createdBy": "self",
                    "dataSource": "SelfRegister",
                    "dateOfBirth": this.signUpForm.value.dateOfBirth,
                    "gender": this.signUpForm.value.gender,
                    "maritalStatus": this.signUpForm.value.maritalStatus,
                    "marriageAnniversary": this.signUpForm.value.marriageAnniversary,
                    "streetAddress": this.signUpForm.value.userStreetAddress,
                    "area": this.signUpForm.value.userArea,
                    "landmark": this.signUpForm.value.userLandmark,
                    "pincode": this.signUpForm.value.userPincode,
                    "city": this.signUpForm.value.userCity,
                    "state": this.signUpForm.value.userState
                }
            ],
            "existingBusinessEntityUsers": []

        }
        console.log(requestObject);
        this.signup(requestObject);

    }

    saveToTodo() {
        var programInfo = this.programSelection;
        console.log("this.programSelection", this.programSelection);
        console.log("this.programInfo", programInfo);
        var fullName = this.signUpForm.value.firstName + " " + this.signUpForm.value.lastName;
        let requestObject = {
            "outletName": this.signUpForm.value.outletName,
            "entityName": fullName,
            "entityType": "owner",
            "registeredMobile": this.signUpForm.value.mobileNumber,
            "city": this.signUpForm.value.city,
            "state": this.signUpForm.value.state,
            "programName": this.JSWProgramName,
            "programClient": this.clientName,
            "businessId": this.businessId
        }
        this.signupTodo(requestObject);
    }




    setProgram(program, index) {
        console.log("Index", index);
        this.selectProgram = true;
        this.selectedIndex = index;
        this.showProgramError = false;
        console.log("Program selected", program);
        this.programSelection = {
            programId: program.programId,
            programName: program.programName
        };

        this.clientName = program.clientName;


        console.log("Program selected", this.programSelection);
    }

    ngAfterViewInit() {
        this.showLoader = true;
    }

    checkInputs(): void {

        this.showSignUp = false;
        this.showOTP = true;
        this.sendOtp();
    }

    goToLogin() {
        console.log("origin", window.location.origin);

        this.router.navigate(['login']);

    }

    sendOtp() {
        this.googleAnalyticsEventsService.emitEvent("Registration", "otpSent", this.signUpForm.value.mobileNumber);
        var mobileNumber = this.signUpForm.value.mobileNumber;
        var requestObject = {
            "mobile": mobileNumber
        };
        console.log("OTP", requestObject);
        this.sendOtpToMobile(requestObject);
    }

    resendOtp() {

        this.googleAnalyticsEventsService.emitEvent("Registration", this.signUpForm.value.mobileNumber, "resendOtp");
        this.mobileNumber = this.signUpForm.value.mobileNumber;
        var requestObject = {
            "mobile": this.mobileNumber
        };
        console.log("OTP", requestObject);
        this.sendOtpToMobile(requestObject);

        this.otpfeedBackMessage = "OTP was Resent";
        this.showOtpFeedBack = true;
    }

    verifyOtp() {
        this.googleAnalyticsEventsService.emitEvent("Registration", "verifyOtp", this.signUpForm.value.mobileNumber);

        var requestObject = {
            "mobile": this.signUpForm.value.mobileNumber,
            "otp": this.otpForm.value.otp
        };
        console.log("OTP", requestObject);
        this.showOtpFeedBack = false;
        this.verifyOtpOfMobile(requestObject);



    }

    reSignUp() {
        this.signUpForm.reset();
        this.otpForm.reset();
        this.showSignUp = true;
        this.showOTP = false;
        this.showSetPassword = false;
        this.router.navigate['signup'];
    }




    /*
        METHOD         : signup()
        DESCRIPTION    : For signup of BE into the application.
    */
    signup(requestObject) {
        this.showLoader = true;

        this.loginService.signup(requestObject) 
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    console.log(responseObject);
                    console.log("Response code", responseCodes);
                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.businessId = responseObject.result.businessId;
                            this.saveToTodo();
                            this.googleAnalyticsEventsService.emitEvent("Registration", "Success", this.signUpForm.value.mobileNumber);
                            //this.router.navigate(["login"]);
                            // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_FAIL:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.showErrorMessage(responseObject.message);
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }



    /*
    METHOD         : signupTodo()
    DESCRIPTION    : For making entry of BE User so as to make the TODO TaskList of Telecalling
*/
    signupTodo(requestObject) {


        this.loginService.signupTodo(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    console.log(responseObject);
                },
                err => {

                    // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
                }
            );
    }


    /***************************	MODIFY FORM CONTROLS 	***********************************/
    getStates() {
        this.statesArray = this.getStatesV1();
        this.statesArrayForUser = this.getStatesV2();
    }

    getPrograms() {

        let requestObject = {
            "skip": 0,
            "isActive": false,
            "programId": "PR1493824208902"
        };

        console.log("origin", window.location.origin);
        if (window.location.origin == "http://racoldroyals.goldencircle.in") {
        } else {
            delete requestObject.programId;

        }
        this.programsArray = this.getProgramsV1(requestObject);
        console.log("Program", this.programsArray);


        console.log("Inside");
        console.log("Length", this.programsArray.length);
        for (var a = 0; a < this.programsArray.length; a++) {
            console.log("ProgramName", this.programsArray[a].programName);
            console.log("ProgramID", this.programsArray[a].programId);
        }





    }

    getCities(state) {
        this.citiesArray = this.getCitiesV1(state);
    }

    getCitiesForUser(state) {
        console.log("State change");
        this.citiesArrayForUser = this.getCitiesV2(state);
    }
    //To get program  array
    getProgramsV1(requestObject): string[] {
        let programs: Array<any> = [];
        this.showLoader = true;
        this.configService.getPrograms(requestObject).subscribe(
            (responseObject) => {
                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {

                    for (let i = 0; i < responseObject.result.length; i++) {
                        programs[i] = responseObject.result[i];
                        if (programs[i].programId === this.stringService.getStaticContents().jswprogramId) {
                            console.log("ProgramName", programs[i].programName);
                            this.JSWProgramName = programs[i].programName;

                        }

                    }

                    this.showLoader = false;
                    return programs;
                }



                else if (responseObject.statusCode === responseCodes.RESP_FAIL) {
                    this.showLoader = false;
                    this.noProgramFound = true;
                    this.selectProgram = true;

                }
            },
            err => {
                this.showLoader = false;

            }
        );
        return programs;


    }

    //To get states array
    getStatesV1(): string[] {
        let states: string[] = [];

        this.showLoader = true;

        this.configService.getStates().subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        states[i] = responseObject.result[i].stateName;
                    }
                    return states;
                }
                else {

                    return states;
                }
            },
            err => {
                this.showLoader = false;

            }
        );
        return states;
    }
    //To get states array
    getStatesV2(): string[] {
        let userStates: string[] = [];

        this.showLoader = true;

        this.configService.getStates().subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        userStates[i] = responseObject.result[i].stateName;
                    }
                    return userStates;
                }
                else {

                    return userStates;
                }
            },
            err => {
                this.showLoader = false;

            }
        );
        return userStates;
    }
    //To get cities array for repective states
    getCitiesV1(state): string[] {
        let cities: string[] = [];
        this.showLoader = true;

        this.configService.getCities(state).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        cities[i] = responseObject.result[i].cityName;
                    }
                    return cities;
                }
                else {
                    // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                    return cities;
                }
            },
            err => {
                this.showLoader = false;

            }
        );
        return cities;
    }

    //To get cities array for repective states
    getCitiesV2(state): string[] {
        let userCities: string[] = [];
        this.showLoader = true;

        this.configService.getCities(state).subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        userCities[i] = responseObject.result[i].cityName;
                    }
                    return userCities;
                }
                else {
                    // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                    return userCities;
                }
            },
            err => {
                this.showLoader = false;

            }
        );
        return userCities;
    }
    sendOtpToMobile(requestObject) {
        this.showLoader = true;
        console.log("In", requestObject);
        this.loginService.sendOTP(requestObject).subscribe((responseObject) => {
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            console.log(responseObject); 


        }, err => {
            this.showLoader = false;


        });
    }

    verifyOtpOfMobile(requestObject) {
        console.log("this.signUpForm.value.gender",this.signUpForm.value.gender);
        console.log("this.signUpForm.value.maritalStatus",this.signUpForm.value.maritalStatus)
        this.showLoader = true;
        console.log("In", requestObject);
        this.loginService.verifyOTP(requestObject).subscribe((responseObject) => {
            this.showLoader = false;
            console.log("IN COMPONENT", responseObject._body);
            var result = responseObject._body;

            console.log(result);
            result = result.trim();
            console.log(result);
            if (result === "OTP VERIFIED") {
                console.log("OTP  has being OTP VERIFIED");
                this.goToSetPassword();

            } else if (result === "OTP FAILED") {
                console.log("FAILED");
                this.showOtpFeedBack = true;
                if (this.otpAttempts > 0) {
                    this.otpfeedBackMessage = "OTP Verification Failed " + this.otpAttempts + " Attempts Left!";
                    this.otpAttempts--;
                    console.log(this.otpAttempts);
                } else {
                    this.showOtpFeedBack = false;
                    this.selectProgram = false;
                    this.goToLogin();

                }
            }


        }, err => {
            this.showLoader = false;

            // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
        });
    }


    Ok() {
        this.router.navigate(["login"]);
    }



    onChangeMobileNumber(mobileNumber) {

        if (mobileNumber.length == 0) {
            this.mobileerror = false;
            this.mobileerror1 = false;
        }

        console.log("Mobile Number", mobileNumber.length);
        this.showLoader = true;


        this.checkIfBusinessEntityExistsInMaster(mobileNumber);
        this.checkIfBusinessEntityExistsInStaging(mobileNumber);


    }

    checkIfBusinessEntityExistsInStaging(mobileNumber) {
        this.requestObj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "sort": { createdAt: -1 },
            "mobileNumberArray": [mobileNumber],
            "kycStatus": false
        }

        this.showLoader = true;
        this.loginService.getBusinessEntityUsers(this.requestObj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    console.log(responseObject);
                    console.log("Response code", responseCodes);
                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SUCCESS:
                            console.log("Flag1", this.mobileerror);

                            if (responseObject.statusCode === 0) {
                                console.log("Flag2", this.mobileerror);
                                console.log("Result", responseObject.result);
                                if (responseObject.result) {

                                    for (var a = 0; a < responseObject.result.length; a++) {
                                        console.log("businessEntityInfo Length", responseObject.result[a].businessEntityInfo.length);
                                        if (responseObject.result[a].businessEntityInfo) {

                                            for (var b = 0; b < responseObject.result[a].businessEntityInfo.length; b++) {
                                                console.log("ProgramID", responseObject.result[a].businessEntityInfo[b].programsInterested);
                                                if (responseObject.result[a].businessEntityInfo[b].programsInterested) {

                                                    for (var c = 0; c < responseObject.result[a].businessEntityInfo[b].programsInterested.length; c++) {

                                                        if (responseObject.result[a].businessEntityInfo[b].programsInterested[c] == this.stringService.getStaticContents().jswprogramId) {
                                                            console.log("ID MATCH");
                                                            this.mobileerror = true;
                                                            break;
                                                        }
                                                        else {
                                                            console.log("Inside Else Condition");
                                                            this.mobileerror = false;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // break;
                                    }
                                }

                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_FAIL:
                            this.mobileerror = false;
                            console.log("Function 2 Called");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.showErrorMessage(responseObject.message);
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }

    checkIfBusinessEntityExistsInMaster(mobileNumber) {
        this.requestObj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "sort": { createdAt: -1 },
            "mobileNumberArray": [mobileNumber],
            "kycStatus": true
        }

        this.showLoader = true;
        this.loginService.getBusinessEntityUsers(this.requestObj)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;

                    let responseCodes = this.configService.getStatusTokens();

                    console.log(responseObject);
                    console.log("Response code11", responseCodes);
                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_SUCCESS:
                            console.log("Flag111", this.mobileerror);

                            if (responseObject.statusCode === 0) {
                                console.log("Flag211", this.mobileerror);
                                console.log("Result1", responseObject.result);
                                if (responseObject.result) {

                                    for (var a = 0; a < responseObject.result.length; a++) {
                                        console.log("businessEntityInfo Length1", responseObject.result[a].businessEntityInfo.length);
                                        if (responseObject.result[a].businessEntityInfo) {

                                            for (var b = 0; b < responseObject.result[a].businessEntityInfo.length; b++) {
                                                console.log("ProgramID1", responseObject.result[a].businessEntityInfo[b].programsMapped);
                                                if (responseObject.result[a].businessEntityInfo[b].programsMapped) {

                                                    for (var c = 0; c < responseObject.result[a].businessEntityInfo[b].programsMapped.length; c++) {

                                                        if (responseObject.result[a].businessEntityInfo[b].programsMapped[c].programId == this.stringService.getStaticContents().jswprogramId) {
                                                            console.log("ID MATCH1");
                                                            this.mobileerror1 = true;
                                                            break;
                                                        }
                                                        else {
                                                            console.log("Inside Else Condition1");
                                                            this.mobileerror1 = false;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.showErrorMessage(responseObject.message);
                            break;
                        case responseCodes.RESP_FAIL:
                            this.mobileerror1 = false;

                            console.log("Function 2 Called");
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.showErrorMessage(responseObject.message);
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }

    getToday(): string {
        console.log("getTODAY----------------", new Date().toISOString().split('T')[0]);
        return new Date().toISOString().split('T')[0];
    }




}








