
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "./shared-services/config.service";
import { Observable, Subject } from "rxjs";

@Injectable()
export class AppService {
	public frontEndInfo: any = new Subject();
	private headers;
	private options;
	private frontEndUserInfo;
	private userInfo = {};

	constructor(private http: Http,
		private configService: ConfigService) {

		this.headers = new Headers({
			'Content-Type': 'application/json',
		});
		this.options = new RequestOptions({ headers: this.headers });
	}

	setUserInfo(userInfo) {
		this.userInfo = userInfo;
	}
	getUserInfo(): any {
		return this.userInfo;
	}

	setFrontEndUserInfo(userInfo) {
		this.frontEndUserInfo = userInfo;
	}
	getFrontEndUserInfo() {
		return this.frontEndUserInfo;
	}

	getIpAdress(): any {

		// let url = "http://ipinfo.io"; // 1000 
		let url = 'https://api.ipify.org/?format=json';

		return this.http.get(url)
			.map((res: Response) => {
				console.log("inside map");
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getIpLocation(ipAddress): any {

		// let url = "http://ipinfo.io"; // 1000 daily request
		let url = 'http://ipinfo.io/' + ipAddress + '?token=9cf7c7a4ba953d';

		// this.headers = new Headers({
		// 	'Accept': 'application/json',
		// 	'Authorization': 'Bearer' + "9cf7c7a4ba953d"
		// });
		this.options = new RequestOptions({ headers: this.headers });

		return this.http.get(url)
			.map((res: Response) => {
				console.log("getIpLocation res",res);
				return res.json();
			})
			.catch((error: any) => {
				console.log("getIpLocation inside catch");

				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


}
