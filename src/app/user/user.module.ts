/*
	Author			:	Deepak Terse
	Description		: 	Setting User module for all the user related page like profile, about us
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { ClientProfileComponent } from "./client-profile.component";
import { BusinessEntityProfileComponent } from "./business-entity-profile.component";

import { ProgramTiersComponent } from "./program-tiers.component";

import { AboutUsComponent } from "./aboutus.component";
import { TestimonialComponent } from "./testimonial.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
// import { SwiperModule } from 'angular2-useful-swiper'; //or for angular-cli the path will be ../../node_modules/angular2-useful-swiper

import { FeedbackMessageComponent } from "./feedback-message.component";
import { FooterModule } from '../footer/footer.module';
import { UserService } from "./user.service";
import { SurveyComponent } from './survey.component';
import { EngineerHomeComponent } from './engineer-home.component';
import { EngineerRequestComponent } from './engineerRequest.component';
import { EngineerDetailPageComponent } from './engineer-Request-DetailPage.component';
import { DealerAboutUsComponent } from "./dealer-aboutus.component";
import { RewardGalleryComponent } from "./reward-gallery.component";
import { manageraboutusComponent} from "./manageraboutus.component";
import { ContactUsComponent } from "./contactus.component";




@NgModule({
	declarations: [ClientProfileComponent,
		BusinessEntityProfileComponent,
		AboutUsComponent,
		TestimonialComponent,
		FeedbackMessageComponent,
		SurveyComponent,
		ProgramTiersComponent,
		EngineerHomeComponent,
		EngineerRequestComponent,
		EngineerDetailPageComponent,
		DealerAboutUsComponent,
		RewardGalleryComponent,
		manageraboutusComponent,
		ContactUsComponent],

	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule,  FooterModule],
	exports: [SurveyComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [FeedbackMessageComponent, SurveyComponent, UserService, BusinessEntityProfileComponent]

})
export class UserModule {
}
