/*
	Author			:	Deepak Terse
	Description		: 	Component for business entity profile page
	Date Created	: 	08 March 2016
	Date Modified	: 	18 March 2016
*/

import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { FeedbackMessageComponent } from "./feedback-message.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';

import { SurveyComponent } from './survey.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './business-entity-profile.component.html',
	styleUrls: ["../app.component.scss", './business-entity-profile.component.scss'],
	providers: [UserService, Device],

})
export class BusinessEntityProfileComponent implements OnInit, AfterViewInit {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	programUserInfo: any;
	user: any;
	isMale: boolean;
	isSingle: boolean;
	public loggedInUser: any;
	public isMarried: boolean;

	private businessEntityForm: FormGroup;
	private businessEntityUserForm: FormGroup;
	private passwordForm: FormGroup;

	private showPasswordField: boolean;

	private userInfo: any;
	private bEInfo: any;

	public showLoader: boolean;

	private statesArray: string[] = [];
	private citiesArray1: string[] = [];
	private citiesArray2: string[] = [];
	private editBEForm: boolean = true;
	private editFormBEBtn: boolean = true;
	private editBEUserForm: boolean = true;
	private editFormBEUserBtn: boolean = true;
	private editForm: boolean = true;
	public clientColor: any;
	public allowEdit: boolean = false;


	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private userService: UserService,
		private stringService: StringService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		public SurveyComponent: SurveyComponent,
		public sidebar: SidebarComponent,
		private device: Device,
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}

		this.programUserInfo = configService.getloggedInProgramUser();
		this.configService.setRewardGalleryFlag(false);
		this.userInfo = configService.getUserInfo();
		this.loggedInUser = configService.getUserInfo();
		this.showLoader = false;
		this.clientColor = this.configService.getThemeColor();
		this.initBusinessEntityForm();
		this.initBusinessEntityUserForm();

		this.businessEntityUserForm.valueChanges.subscribe(data => {

			if (data.maritalStatus === "married" || data.maritalStatus === "Married") {
				this.isMarried = true;
			}
			else {
				this.isMarried = false
			}
		});
	}

	ngOnInit() {
		this.sidebar.close();

		if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Business Entity Profile")
        } else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Business Entity Profile");
					ga('send', 'pageview');
				}
			});
		}
		

		this.getBusinessEntity();
		this.setUserInfo();
		this.initPasswordForm();

		this.getStates();
	}

	ngAfterViewInit() {
	}

	editSurvey() {
		if (!this.allowEdit) {
			this.allowEdit = true;
			this.SurveyComponent.removeReadOnly();
			// Change text of button to Update Information
			const dom1: any = document.getElementById('edit1').innerHTML = "UPDATE";
		}
		else {
			console.log("calling compoete");
			this.SurveyComponent.dom1[0].click();
			this.updateProgramUser();

		}


	}
	fillUser() {
		console.log("single", this.isSingle);
		if (this.userInfo.dateOfBirth != undefined) {
			this.userInfo.dateOfBirth = this.userInfo.dateOfBirth.slice(0, 10);
		}
		if (this.userInfo.marriageAnniversary != undefined) {
			this.userInfo.marriageAnniversary = this.userInfo.marriageAnniversary.slice(0, 10);
		}
		if (this.userInfo.maritalStatus == "single" || this.userInfo.maritalStatus == "Single") {
			console.log("single inside", this.isSingle);
			this.isSingle = true;
			this.isMarried = false;
		}
		else {
			this.isMarried = true;
			this.isSingle = false;
		}

		if (this.userInfo.gender == "male" || this.userInfo.gender == "Male") {
			this.isMale = true;
		}
		else {
			this.isMale = false;
		}
		console.log("single", this.isSingle);
		this.assignBusinessEntityUserForm();
		// }
	}
	/*
		METHOD         : initProfileForm()
		DESCRIPTION    : Called from ngOnInit when the component is loaded
						 Sets userInfo and bEInfo
	*/
	setUserInfo() {
		this.userInfo = this.configService.getLoggedInUserInfo();
		this.bEInfo = this.configService.getloggedInBEInfo();
	}

	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initBusinessEntityForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates BE form group and assign initial values and validations to its controls
    */
	initBusinessEntityForm() {
		this.businessEntityForm = this.formBuilder.group({
			businessName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(80)]],
			registrationNumber: ["", [Validators.required]],
			streetAddress: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			area: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			landmark: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			pincode: ["", [Validators.required, Validators.pattern('[0-9]{6}')]],
			city: [{ value: "", disabled: true }, [Validators.required]],
			state: [{ value: "", disabled: true }, [Validators.required]]
		});
	}

	/*
        METHOD         : initBusinessEntityUserForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates BE User form group and assign inital values and validations to its controls
    */
	initBusinessEntityUserForm() {
		this.businessEntityUserForm = this.formBuilder.group({
			firstName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
			lastName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
			dateOfBirth: [{ value: "", disabled: true }],
			gender: [""],
			maritalStatus: [""],
			marriageAnniversary: [{ value: "", disabled: true }, , []],
			mobileNumber: [{ value: "", disabled: true }, [Validators.required, Validators.pattern('[0-9]{10}')]],
			email: ["", [Validators.required, Validators.pattern(/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/)]],
			streetAddress: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			area: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			landmark: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
			pincode: ["", [Validators.required, Validators.pattern('[0-9]{6}'), Validators.maxLength(6)]],
			city: [{ value: "", disabled: true }, [Validators.required]],
			state: [{ value: "", disabled: true }, [Validators.required]],
		});
	}

	/*
        METHOD         : initPasswordForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates password form group and assign inital values and validations to its controls
    */
	initPasswordForm() {
		this.passwordForm = this.formBuilder.group({
			password: ["", [Validators.required]]
		});
	}

	changeBEForm() {

		this.editBEForm = false;
		this.editFormBEBtn = false;
		this.businessEntityForm.controls['city'].enable();
		this.businessEntityForm.controls['state'].enable();

	}
	changeBEUserForm() {
		this.editBEUserForm = false;
		this.editFormBEUserBtn = false;
		this.businessEntityUserForm.controls['city'].enable();
		this.businessEntityUserForm.controls['state'].enable();
		this.businessEntityUserForm.controls['dateOfBirth'].enable();
		this.businessEntityUserForm.controls['marriageAnniversary'].enable();

	}


	/*
		METHOD         : assignBusinessEntityForm()
		DESCRIPTION    : Called from ngOnInit when the component is loaded and after form initalisation
						 Assign initial values to form controls of BE form group
	*/
	assignBusinessEntityForm() {
		this.getCitiesForEntity(this.bEInfo.state);

		this.businessEntityForm.patchValue({
			"businessName": this.bEInfo.businessName,
			"registrationNumber": this.bEInfo.registrationNumber,
			"streetAddress": this.bEInfo.streetAddress,
			"area": this.bEInfo.area,
			"landmark": this.bEInfo.landmark,
			"pincode": this.bEInfo.pincode,
			"city": this.bEInfo.city,
			"state": this.bEInfo.state
		});
	}

	/*
		METHOD         : assignBusinessEntityUserForm()
		DESCRIPTION    : Called from ngOnInit when the component is loaded and after form initalisation
						 Assign initial values to form controls of BE User form group
	*/
	assignBusinessEntityUserForm() {
		this.getCitiesForUser(this.userInfo.state);

		this.businessEntityUserForm.patchValue({
			firstName: this.userInfo.firstName,
			lastName: this.userInfo.lastName,
			dateOfBirth: this.userInfo.dateOfBirth,
			gender: this.userInfo.gender,
			maritalStatus: this.userInfo.maritalStatus,
			marriageAnniversary: this.userInfo.marriageAnniversary,
			mobileNumber: this.userInfo.mobileNumber,
			email: this.userInfo.email,
			streetAddress: this.userInfo.streetAddress,
			area: this.userInfo.area,
			landmark: this.userInfo.landmark,
			pincode: this.userInfo.pincode,
			state: this.userInfo.state,
			city: this.userInfo.city
		});
	}

	getCitiesForEntity(state) {
		this.citiesArray1 = [];
		this.citiesArray1 = this.getCities(state);
	}
	getCitiesForUser(state) {
		this.citiesArray2 = [];
		this.citiesArray2 = this.getCities(state);
	}

	/******************************************* 	ONCLICK METHODS 	**********************************************/
	/*
        METHOD         : onResetBEFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls assignBusinessEntityForm() to reset form with the values that were before editing
    */
	onResetBEFormClick() {
		this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "resetBusinessInformation", this.bEInfo.businessId);
		this.assignBusinessEntityForm();
	}

	/*
        METHOD         : onResetBEUserFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls assignBusinessEntityUserForm() to reset form with the values that were before editing
    */
	onResetBEUserFormClick() {
		this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "resetUserInformation", this.userInfo.userId);
		this.assignBusinessEntityUserForm();
	}

	/*
        METHOD         : onUpdateBEFormClick()
        DESCRIPTION    : Called when user clicks on "UPDATE INFORMATION" button in BE Form
						 Detects changed values in forms, forms request object and calls updateBE() to
						 call updateEntity api
    */
	onUpdateBEFormClick() {
		this.router
		{
			window.scrollTo(0, 0);
		}
		let inputObject = this.businessEntityForm.value;
		this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateBusinessInformation", this.bEInfo.businessId);
		let updatedValues: any = {};
		updatedValues.businessId = this.bEInfo.businessId;

		if (inputObject.businessName != this.bEInfo.businessName)
			updatedValues.businessName = inputObject.businessName;
		if (inputObject.registrationNumber != this.bEInfo.registrationNumber)
			updatedValues.registrationNumber = inputObject.registrationNumber;
		if (inputObject.streetAddress != this.bEInfo.streetAddress)
			updatedValues.streetAddress = inputObject.streetAddress;
		if (inputObject.area != this.bEInfo.area)
			updatedValues.area = inputObject.area;
		if (inputObject.landmark != this.bEInfo.landmark)
			updatedValues.landmark = inputObject.landmark;
		if (inputObject.pincode != this.bEInfo.pincode)
			updatedValues.pincode = inputObject.pincode;
		if (inputObject.city != this.bEInfo.city)
			updatedValues.city = inputObject.city;
		if (inputObject.state != this.bEInfo.state)
			updatedValues.state = inputObject.state;

		let reqObj: any = {
			"kycStatus": true,
			"isStaging": false,
			"businessEntityDetails": updatedValues
		}

		this.updateBE(reqObj);
	}

	/*
        METHOD         : onUpdateFormClick()
        DESCRIPTION    : Called when user clicks on "UPDATE INFORMATION" button in BE User Form
						 Detects changed values in forms, forms request object and calls updateClientUser() to
						 update BE User Info
    */
	onUpdateBEUserFormClick() {
		this.router
		{
			window.scrollTo(0, 0);
		}
		document.documentElement.scrollTop = 0;
		this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateUserInformation", this.userInfo.userId);
		let inputObject = this.businessEntityUserForm.value;

		let updatedValues: any = {};
		updatedValues.userId = this.userInfo.userId;



		if (inputObject.firstName != this.userInfo.firstName) {
			updatedValues.firstName = inputObject.firstName;
			updatedValues.fullName = inputObject.firstName + " " + inputObject.lastName;
		}
		if (inputObject.lastName != this.userInfo.lastName) {
			updatedValues.lastName = inputObject.lastName;
			updatedValues.fullName = inputObject.firstName + " " + inputObject.lastName;
		}
		if (inputObject.dateOfBirth != this.userInfo.dateOfBirth)
			updatedValues.dateOfBirth = inputObject.dateOfBirth;
		if (inputObject.gender != this.userInfo.gender)
			updatedValues.gender = inputObject.gender;
		if (inputObject.maritalStatus != this.userInfo.maritalStatus)
			updatedValues.maritalStatus = inputObject.maritalStatus;
		if (inputObject.maritalStatus === "married") {

			if (inputObject.marriageAnniversary != this.userInfo.marriageAnniversary) {
				updatedValues.marriageAnniversary = inputObject.marriageAnniversary;
			}
		}
		else {
			if (updatedValues.marriageAnniversary) {
				delete updatedValues.marriageAnniversary
			}
		}
		if (inputObject.mobileNumber != this.userInfo.mobileNumber)
			updatedValues.mobileNumber = inputObject.mobileNumber;
		if (inputObject.email != this.userInfo.email)
			updatedValues.email = inputObject.email;
		if (inputObject.streetAddress != this.userInfo.streetAddress)
			updatedValues.streetAddress = inputObject.streetAddress;
		if (inputObject.area != this.userInfo.area)
			updatedValues.area = inputObject.area;
		if (inputObject.landmark != this.userInfo.landmark)
			updatedValues.landmark = inputObject.landmark;
		if (inputObject.pincode != this.userInfo.pincode)
			updatedValues.pincode = inputObject.pincode;
		if (inputObject.city != this.userInfo.city)
			updatedValues.city = inputObject.city;
		if (inputObject.state != this.userInfo.state)
			updatedValues.state = inputObject.state;

		let reqObj: any = {
			"kycStatus": true,
			"myUserId": this.userInfo.userId,
			"userInfo": updatedValues
		}
		console.log("User Obj", reqObj);
		this.updateBEUser(reqObj);
	}

	/*
        METHOD         : onUpdatePasswordClick()
        DESCRIPTION    : Called when user clicks on "UPDATE PASSWORD" button
				      	 Shows password form
    */
	onUpdatePasswordClick() {
		this.showPasswordField = true;
	}

	/*
        METHOD         : onCancelPasswordClick()
        DESCRIPTION    : Called when user clicks on "CANCEL" button
				      	 Hides password form
    */
	onCancelPasswordClick() {
		console.log("Value", this.showPasswordField);
		this.showPasswordField = false;
	}

	/*
        METHOD         : onSubmitPasswordClick()
        DESCRIPTION    : Called when user clicks on "SUBMIT" button
				      	 Forms request object and calls updateBEUser to update user password
    */
	onSubmitPasswordClick() {
		this.router
		{
			window.scrollTo(0, 0);
		}
		this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updatePassword", this.bEInfo.businessId);
		let inputObject = this.passwordForm.value;

		let reqObj: any = {
			"kycStatus": true,
			"myUserId": this.userInfo.userId,
			"userInfo": {
				"password": inputObject.password,
				"userId": this.userInfo.userId
			}
		}

		this.updateBEUser(reqObj);
	}


	/************************************ 	END OF ONCLICK METHODS 	************************************************/


	/************************************** 	API CALLS 	****************************************/
	/*
        METHOD         : updateBE()
        DESCRIPTION    : To update BE info
    */
	updateBE(requestObject) {
		this.showLoader = true;
		this.userService.updateEntity(requestObject)
			.subscribe(
				(responseObject) => {
					let responseCodes = this.configService.getStatusTokens();
					this.showLoader = false;

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SUCCESS:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
							this.configService.setloggedInBEInfo(responseObject.result);
							this.setUserInfo();
							this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateBusinessInformationSucceed", this.bEInfo.businessId);
							this.assignBusinessEntityUserForm();
							break;
						case responseCodes.RESP_AUTH_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateBusinessInformationFailed", this.bEInfo.businessId);
							break;
						case responseCodes.RESP_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
					}
				},
				err => {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}

	/*
        METHOD         : updateBEUser()
        DESCRIPTION    : To update BE user info
    */
	updateBEUser(requestObject) {
		this.showLoader = true;

		this.userService.updateEntityUser(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;

					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SUCCESS:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
							this.configService.setLoggedInUserInfo(responseObject.result);
							this.setUserInfo();
							this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateUserInformationSucceed", this.userInfo.userId);
							this.assignBusinessEntityForm();
							break;
						case responseCodes.RESP_AUTH_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							this.googleAnalyticsEventsService.emitEvent("Business Entity Profile", "updateUserInformationFailed", this.userInfo.userId);
							break;
						case responseCodes.RESP_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
					}
				},
				err => {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}

	/*
        METHOD         : getStates()
        DESCRIPTION    : To get an array of all states
    */
	getStates() {
		this.showLoader = true;

		let states: string[] = [];
		this.configService.getStates().subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
					for (let i = 0; i < responseObject.result.length; i++) {
						this.statesArray[i] = responseObject.result[i].stateName;
					}
				}
				else {
					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
		);
	}

	/*
		METHOD         : getStates()
		DESCRIPTION    : To get an array of all cities what belong to the selected state
	*/
	getCities(state): string[] {
		this.showLoader = true;

		let cities: string[] = [];
		this.configService.getCities(state).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
					for (let i = 0; i < responseObject.result.length; i++) {
						cities[i] = responseObject.result[i].cityName;
					}
					return cities;
				}
				else {
					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
					return cities;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
		);
		return cities;
	}

	getBusinessEntity() {
		var requestObject = {
			"businessId": this.configService.getloggedInBEInfo().businessId,
			"kycStatus": this.configService.getloggedInBEInfo().kycStatus
		}
		this.userService.getBusinessEntity(requestObject).subscribe(
			(responseObject) => {
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_SUCCESS:
						var businessEntity = responseObject.result;
						this.bEInfo = businessEntity;

						for (var i = 0; i < businessEntity.businessEntityUsers.length; i++) {
							if (this.loggedInUser.userId === businessEntity.businessEntityUsers[i].userId)

								this.userInfo = businessEntity.businessEntityUsers[i];
							console.log("this.userInfo", this.userInfo);
						}
						this.assignBusinessEntityForm();
						this.fillUser()
						console.log("business entity", businessEntity);
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ROLLBACK_ERROR:
					case responseCodes.RESP_SERVER_ERROR:
					case responseCodes.RESP_AUTH_FAIL:
					case responseCodes.RESP_ALREADY_EXIST:
					case responseCodes.RESP_KYC_AWAITING:
					case responseCodes.RESP_BLOCKED:


				}
			},
			err => {

			}
		);
	}





	public updateProgramUser() {
		console.log("this.answers", this.SurveyComponent.answers);
		this.showLoader = true;
		var updateObject = {
			progUserInfo: {
				answers: "",
			},
			serviceType: "programSetup",
			programUserId: this.configService.getloggedInProgramUser().programUserId,
			//this.programUserId = this.configService.getloggedInProgramUser().programUserId;
			// isApproved: true
		};

		updateObject.progUserInfo.answers = this.SurveyComponent.answers;
		console.log("updateObject", updateObject);
		this.userService.updateProgramUsers(updateObject)
			.subscribe((responseObject) => {
				console.log("responseObject", responseObject);
				let responseCodes = this.configService.getStatusTokens();
				this.showLoader = false;

				this.SurveyComponent.edit = false;
				var reqObj = {
					"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
					"clientId": this.configService.getprogramInfo().clientId,
					"programId": this.configService.getprogramInfo().programId,
				}
				//this.getProfileQuestions(reqObj);

				//this.SurveyComponent.makeReadOnly();
				// this.SurveyComponent.renderSurvey();
				this.SurveyComponent.getProfileQuestions(reqObj);
				this.allowEdit = false;


				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						this.user = responseObject.result;

						console.log("User", this.user)
						break;
					case responseCodes.RESP_AUTH_FAIL:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						//this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
				err => {

					this.showLoader = false;
					//this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}

	/*********************************** 	END OF API CALLS 	**********************************/
}
