
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl
} from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: "./contactus.component.html",
    // styleUrls: ['./user.component.css'],
    styleUrls: ["./contactus.component.scss", "../app.component.scss"],
    providers: [Device]

})
export class ContactUsComponent implements OnInit {
    programUserInfo: any;
    public aboutProgram: string;
    public termsAndCondition: string;
    public faqs: string;
    public programDescription: string;
    public isProduct: boolean;
    public bannerImage: string[] = [];
    public brochure: string;
    public programPoints: any = [];
    public pageCount: number = 1; //to maintain pagecount in pagination
    public isPrevious: boolean = true;
    public isNext: boolean = false;
    public loginProgramInfo: any = {};
    private limit: number = 10;
    private totalRecords: number = 0;
    private programName: string = "";
    public requestObject: any = {
        clientId: "",
        programId: "",
        limit: this.limit,
        skip: 0
    };
    public clientId: string;
    public programId: string;
    public programInfo: any;
    public showPoints: boolean = false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private stringService: StringService,
        private userService: UserService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,
    ) {
        this.programUserInfo = configService.getloggedInProgramUser();
        this.loginProgramInfo = this.configService.getloggedInProgramInfo();
        console.log(" this.loginProgramInfo", this.loginProgramInfo);
        this.configService.setRewardGalleryFlag(false);
        this.programInfo = configService.getloggedInProgramUser();

        console.log("this.programInfo", this.programInfo);

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Contact Us")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga("set", "page", "Contact Us");
                    ga("send", "pageview");
                }
            });
        }
      
        console.log("programInfo", this.programInfo);
        this.clientId = this.programInfo.clientId;
        this.programId = this.programInfo.programId;
        if (this.loginProgramInfo.basedOn === "product") {
            this.isProduct = true;
        } else {
            this.isProduct = false;
        }
        this.requestObject.clientId = this.clientId;
        this.requestObject.programId = this.programId;

        this.isPrevious = true;
        this.isNext = false;
    }




    ngOnInit() {

        this.sidebar.close();

    }



}
