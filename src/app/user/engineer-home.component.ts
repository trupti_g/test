/*
Author			:	Pratik Gawand
Description		: 	Component for About Us page
Date Created	: 	15 March 2016
Date Modified	: 	15 March 2016
*/

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl
} from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: "./engineer-home.component.html",
    styleUrls: ["./engineer-home.component.scss", "../app.component.scss"],
    providers: [Device]
})
export class EngineerHomeComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    programUserInfo: any;
    public aboutProgram: string;
    public termsAndCondition: string;
    public faqs: string;
    public programDescription: string;
    public isProduct: boolean;
    public bannerImage: string[] = [];
    public brochure: string;
    public programPoints: any = [];
    public pageCount: number = 1; //to maintain pagecount in pagination
    public isPrevious: boolean = true;
    public isNext: boolean = false;
    public loginProgramInfo: any = {};
    private limit: number = 10;
    private totalRecords: number = 0;
    private programName: string = "";
    public requestObject: any = {
        clientId: "",
        programId: "",
        limit: this.limit,
        skip: 0
    };
    public clientId: string;
    public programId: string;
    public programInfo: any;
    public showPoints: boolean = false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private stringService: StringService,
        private userService: UserService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,
    ) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
        this.programUserInfo = configService.getloggedInProgramUser();
        this.loginProgramInfo = this.configService.getloggedInProgramInfo();
        console.log(" this.loginProgramInfo", this.loginProgramInfo);
        this.configService.setRewardGalleryFlag(false);
        this.programInfo = configService.getloggedInProgramUser();
    }

    stopScroll() {
        // Code to active backgroung scrolling when side bar is close
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);


        document.getElementById("body").style.overflow = "hidden";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    }

    scrollActivate() {
        var scroll = document.getElementById('body');
        console.log("RRRRRRRRRRRrr", scroll);


        document.getElementById("body").style.overflow = "auto";
        console.log("RRRRRRRRRRRRRRRRRRrr", scroll);

    }

    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.sidebar.close();
        let programInfo = this.configService.getloggedInProgramInfo();
        this.programName = programInfo.programName;
        this.aboutProgram = programInfo.aboutProgram;
        this.termsAndCondition = programInfo.termsAndCondition;
        this.faqs = programInfo.faqs;
        this.programDescription = programInfo.programDescription;
        for (let i = 0; i < programInfo.programBannerImages.length; i++) {
            if (programInfo.programBannerImages[i] != null) {
                this.bannerImage.push(programInfo.programBannerImages[i]);
            }
        }

        if (programInfo.brochure) {
            this.brochure = programInfo.brochure;
        }

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Engineer home page")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Engineer home page");
                    ga('send', 'pageview');
                }
            });
        }
      

    }
    /************************************ 	END OF API CALLS 	************************************************/
}
