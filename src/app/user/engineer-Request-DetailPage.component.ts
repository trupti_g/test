

import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { FeedbackMessageComponent } from "./feedback-message.component";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: "./engineer-Request-DetailPage.component.html",
    styleUrls: ["../app.component.scss", "./engineer-Request-DetailPage.component.scss"],
    providers: [Device]

})
export class EngineerDetailPageComponent implements OnInit {
    password: any;
    email: any;
    fullName: any;
    userMarriageAnniversary: any;
    userMaritalStatus: any;
    userGender: any;
    userDOB: any;
    userLandmark: any;
    userArea: any;
    userStreetAddress: any;
    userPincode: any;
    userCity: any;
    userState: any;
    isSubmit: boolean;
    kycStatus: string;
    showSubmitButtton: boolean;
    isConfirmed: boolean;
    showFamilyCode: boolean;
    foundUsers: any;
    userId: any;
    createdBy: any;
    dataSource: any;
    businessId: any;
    mobileNumber: any;
    emailId: any;
    userType: any;
    lastName: any;
    firstName: any;
    yearOfEstablishment: any;
    approxShopSize: any;
    monthlyTurnOver: any;
    registrationNumber: any;
    landmark: any;
    area: any;
    streetAddress: any;
    pincode: any;
    city: any;
    state: any;
    businessName: any;
    public showLoader: boolean = false;
    public entitiesList: Array<any> = [];
    businessEnityArray: any;
    public cityArray: Array<any> = [];
    public stateArray: Array<any> = [];
    programUserInfo: any;
    private addEntityForm: FormGroup;
    mobilewidth: any;                   //Added for width capture
    displaypc: boolean = true;        //Added for width capture
    displaymobile: boolean = false;   //Added for width capture

    private approvalForm: FormGroup;
    private showRejctionsField: boolean = false;
    private isUpdated: boolean = false;
    private canUpdate: boolean = false;
    private userInfo: any;
    private approvalStatus: string;

    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private userService: UserService,
        private stringService: StringService,
        private formBuilder: FormBuilder,
        public sidebar: SidebarComponent,
        private device: Device,
    ) {
        console.log(" this.feedbackMessageComponent", this.feedbackMessageComponent);

        // code for width capture

        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

    }
    ngOnInit() {
        window.scroll(0, 0);
        this.sidebar.close();

        this.programUserInfo = this.configService.getloggedInProgramUser();

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Engineer request details")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Engineer request details");
                    ga('send', 'pageview');
                }
            });
    
        }
        console.log("")
        this.userInfo = this.configService.getProgramUserInfo();
        this.configService.getEntity();
        console.log("enityyyyyyyyy", this.configService.getEntity());
        this.approvalStatus = this.configService.getEntity().approvalStatus;
        if (this.approvalStatus != "approved"){
            this.businessId = this.configService.getEntity().businessId;
            this.businessName = this.configService.getEntity().businessName;
            this.state = this.configService.getEntity().state;
            this.city = this.configService.getEntity().city;
            this.pincode = this.configService.getEntity().pincode;
            this.streetAddress = this.configService.getEntity().streetAddress;
            this.area = this.configService.getEntity().area;
            this.landmark = this.configService.getEntity().landmark;
            this.registrationNumber = this.configService.getEntity().registrationNumber;
            this.monthlyTurnOver = this.configService.getEntity().monthlyTurnOver;
            this.approxShopSize = this.configService.getEntity().approxShopSize;
            this.yearOfEstablishment = this.configService.getEntity().yearOfEstablishment;
    
    
            this.firstName = this.configService.getEntity().businessEntityUsers[0].firstName;
            this.lastName = this.configService.getEntity().businessEntityUsers[0].lastName;
            this.fullName = this.configService.getEntity().businessEntityUsers[0].fullName;
            this.password = this.configService.getEntity().businessEntityUsers[0].password;
            this.userType = this.configService.getEntity().businessEntityUsers[0].userType;
            this.email = this.configService.getEntity().businessEntityUsers[0].email;
            this.mobileNumber = this.configService.getEntity().businessEntityUsers[0].mobileNumber;
            this.dataSource = this.configService.getEntity().businessEntityUsers[0].dataSource;
            this.createdBy = this.configService.getEntity().businessEntityUsers[0].createdBy;
            this.userId = this.configService.getEntity().businessEntityUsers[0].userId;
            this.userState = this.configService.getEntity().businessEntityUsers[0].state;
            this.userCity = this.configService.getEntity().businessEntityUsers[0].city;
            this.userPincode = this.configService.getEntity().businessEntityUsers[0].pincode;
            this.userStreetAddress = this.configService.getEntity().businessEntityUsers[0].streetAddress;
            this.userArea = this.configService.getEntity().businessEntityUsers[0].area;
            this.userLandmark = this.configService.getEntity().businessEntityUsers[0].landmark;
            this.userDOB = this.configService.getEntity().businessEntityUsers[0].dateOfBirth;
            this.userGender = this.configService.getEntity().businessEntityUsers[0].gender;
            this.userMaritalStatus = this.configService.getEntity().businessEntityUsers[0].maritalStatus;
            this.userMarriageAnniversary = this.configService.getEntity().businessEntityUsers[0].marriageAnniversary;
        }else{
            this.businessId = this.configService.getEntity().businessId;
            this.businessName = this.configService.getEntity().businessName;
            this.state = this.configService.getEntity().state;
            this.city = this.configService.getEntity().city;
            this.pincode = this.configService.getEntity().pincode;
            this.streetAddress = this.configService.getEntity().streetAddress;
            this.area = this.configService.getEntity().area;
            this.landmark = this.configService.getEntity().landmark;
            this.registrationNumber = this.configService.getEntity().registrationNumber;
            this.monthlyTurnOver = this.configService.getEntity().monthlyTurnOver;
            this.approxShopSize = this.configService.getEntity().approxShopSize;
            this.yearOfEstablishment = this.configService.getEntity().yearOfEstablishment;


            this.firstName = this.configService.getEntity().businessEntityUserDetails.firstName;
            this.lastName = this.configService.getEntity().businessEntityUserDetails.lastName;
            this.fullName = this.configService.getEntity().businessEntityUserDetails.fullName;
            this.password = this.configService.getEntity().businessEntityUserDetails.password;
            this.userType = this.configService.getEntity().businessEntityUserDetails.userType;
            this.email = this.configService.getEntity().businessEntityUserDetails.email;
            console.log("this.configService.getEntity().businessEntityUserDetails.email",this.configService.getEntity().businessEntityUserDetails.email);
            this.mobileNumber = this.configService.getEntity().businessEntityUserDetails.mobileNumber;
            this.dataSource = this.configService.getEntity().businessEntityUserDetails.dataSource;
            this.createdBy = this.configService.getEntity().businessEntityUserDetails.createdBy;
            this.userId = this.configService.getEntity().businessEntityUserDetails.userId;
            this.userState = this.configService.getEntity().businessEntityUserDetails.state;
            this.userCity = this.configService.getEntity().businessEntityUserDetails.city;
            this.userPincode = this.configService.getEntity().businessEntityUserDetails.pincode;
            this.userStreetAddress = this.configService.getEntity().businessEntityUserDetails.streetAddress;
            this.userArea = this.configService.getEntity().businessEntityUserDetails.area;
            this.userLandmark = this.configService.getEntity().businessEntityUserDetails.landmark;
            this.userDOB = this.configService.getEntity().businessEntityUserDetails.dateOfBirth;
            this.userGender = this.configService.getEntity().businessEntityUserDetails.gender;
            this.userMaritalStatus = this.configService.getEntity().businessEntityUserDetails.maritalStatus;
            this.userMarriageAnniversary = this.configService.getEntity().businessEntityUserDetails.marriageAnniversary;
        }
        
        console.log("this.firstName", this.firstName);
        console.log("this.configService.getEntity().businessEntityUsers[0]", this.configService.getEntity());

        this.initBusinessEntityForm();

        this.approvalForm = this.formBuilder.group({
            status: [""],
            reasonForRejection: [""]
        })

        this.valueChanges();
    }

    //console.log("this.businessName");

    /********************** 	INITIALIZATION OF FORM 	************************************/
    initBusinessEntityForm() {
        this.addEntityForm = this.formBuilder.group({
            businessId: ["", [Validators.required]],
            businessName: [this.businessName, [Validators.required, Validators.maxLength(80)]],
            registrationNumber: [this.registrationNumber, [Validators.required]],

            streetAddress: [this.streetAddress, [Validators.required, Validators.maxLength(150)]],
            area: [this.area, [Validators.required, Validators.maxLength(150)]],
            landmark: [this.landmark, [Validators.required, Validators.maxLength(150)]],
            pincode: [this.pincode, [Validators.required, Validators.pattern('[0-9]{6}')]],
            city: [this.city, [Validators.required]],
            state: [this.state, [Validators.required]],

            monthlyTurnOver: [this.monthlyTurnOver, []],
            approxShopSize: [this.approxShopSize, []],
            yearOfEstablishment: [this.yearOfEstablishment, [Validators.required]],
            firstName: [this.firstName, [Validators.required, Validators.maxLength(30)]],
            lastName: [this.lastName, [Validators.required, Validators.maxLength(30)]],
            userType: [this.userType, [Validators.required]],
            email: [this.email, [Validators.required, Validators.pattern(/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/)]],
            mobileNumber: [this.mobileNumber, [Validators.required, Validators.pattern('[0-9]{10}')]],


        });
    }

    valueChanges() {
        this.approvalForm.valueChanges.subscribe(data => {
            console.log("DATA------", data);
            if (data.status !== null && data.status !== "") {
                if (data.status == "rejected") {
                    this.showRejctionsField = true;
                    console.log("this.approvalForm.value.reasonForRejection",this.approvalForm.value.reasonForRejection);
                    if(this.approvalForm.value.reasonForRejection === null || this.approvalForm.value.reasonForRejection === ""){
                        this.canUpdate = false;
                    }
                }
                else if (data.status == "approved") {
                    this.showRejctionsField = false;
                    this.canUpdate = true;
                }
            }

            if (data.reasonForRejection !== null && data.reasonForRejection !== "") {
                this.canUpdate = true;
            }

        });
    }


    cancelClick() {
        if (this.configService.getEntity().page === 'dashboard') {
            this.router.navigate(['../dashboard'], { relativeTo: this.route });
        } else {
            this.configService.setSelectedFilter(true);
            this.router.navigate(['../EngineersRequest'], { relativeTo: this.route });
        }
    }
    approveClick() {
        // window.scrollTo(0, 0);
        this.kycStatus = "Confirm";
        let mobileNumberArray: string[] = [];

        // for (let x = 0; x < this.addEntityForm.value.businessEntityUsers.length; x++)
        mobileNumberArray.push(this.addEntityForm.value.mobileNumber);

        let requestObject = {
            "kycStatus": true,
            "mobileNumberArray": mobileNumberArray
        };
        this.verifyBEUser(requestObject);
        this.addEntityForm.patchValue({
            "kycStatus": "Done"
        });
        console.log("clicked", requestObject);
    }

    /*
       METHOD         : verifyBEUser()
       DESCRIPTION    : It is called when kyc status is confirmed to check for existing BE users with the same mobile number
                        If a user is found with the same mobile number then family code should be entered to map the Business Entities
                        If user is not found then the BE gets added in the master table on clicking Submit button
   */
    verifyBEUser(requestObject) {
        this.showLoader = true;
        var businessObj: any = {};
        businessObj.businessId = this.businessId;
        businessObj.businessName = this.businessName;
        businessObj.state = this.state;
        businessObj.city = this.city;
        businessObj.pincode = this.pincode;
        businessObj.streetAddress = this.streetAddress;
        businessObj.area = this.area;
        businessObj.landmark = this.landmark;
        businessObj.registrationNumber = this.registrationNumber;
        businessObj.monthlyTurnOver = this.monthlyTurnOver;
        businessObj.approxShopSize = this.approxShopSize;
        businessObj.yearOfEstablishment = this.yearOfEstablishment;
        businessObj.createdBy = "AU1493129678561";
        businessObj.dataSource = "JSWChannelWebUI";
        var businessUserObj: any = {};
        businessUserObj.firstName = this.firstName;
        businessUserObj.lastName = this.lastName;
        businessUserObj.fullName = this.fullName;
        businessUserObj.password = this.password;
        businessUserObj.userType = this.userType;
        businessUserObj.email = this.email;
        businessUserObj.mobileNumber = this.mobileNumber;
        businessUserObj.dataSource = this.dataSource;
        businessUserObj.createdBy = this.createdBy;
        businessUserObj.userId = this.userId;
        businessUserObj.state = this.userState;
        businessUserObj.city = this.userCity;
        businessUserObj.pincode = this.userPincode;
        businessUserObj.streetAddress = this.userStreetAddress;
        businessUserObj.area = this.userArea;
        businessUserObj.landmark = this.userLandmark;
        businessUserObj.dateOfBirth = this.userDOB;
        businessUserObj.gender = this.userGender;
        businessUserObj.maritalStatus = this.userMaritalStatus;
        businessUserObj.marriageAnniversary = this.userMarriageAnniversary;
        businessUserObj.businessId = [this.businessId];
        console.log("businessUserObj", businessUserObj);
        this.userService.findBusinessEntityUser(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:

                            break;
                        case responseCodes.RESP_SERVER_ERROR:

                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.foundUsers = responseObject.result;
                            this.showFamilyCode = true;
                            this.isConfirmed = true;
                            console.log("this.foundUsers", this.foundUsers);

                            businessObj.familyCode = this.foundUsers[0].businessId[0];
                            var obj = {
                                "kycStatus": true,
                                "isStaging": true,
                                "businessEntityDetails": businessObj,
                                "businessEntityUsers": [
                                    businessUserObj
                                ],
                                "existingBusinessEntityUsers": this.foundUsers,
                                "frontendUserInfo": {},
                            }
                            this.addBEToMaster(obj);
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            this.showSubmitButtton = true;
                            this.isConfirmed = true;
                            var obj1 = {
                                "kycStatus": true,
                                "isStaging": true,
                                "businessEntityDetails": businessObj,
                                "businessEntityUsers": [
                                    businessUserObj
                                ],
                                "frontendUserInfo": {},
                            }
                            this.addBEToMaster(obj1);
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    };

    addBEToMaster(requestObject) {
        this.showLoader = true;
        this.userService.migrateBusinessEntity(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.isSubmit = true;

                            this.programUserInfo = this.configService.getloggedInProgramUser();
                            console.log("this.programUserInfo",this.programUserInfo);
                            if(this.programUserInfo){
                                var userId = this.programUserInfo.programUserId;
                            }
                            

                            var obj2 = {
                                "clientId": this.stringService.getStaticContents().jswClientId,
                                "programId": this.stringService.getStaticContents().jswprogramId,
                                "programUserId": this.businessId,
                                "userType": "ChannelPartner",
                                "programRole": this.stringService.getStaticContents().jswEngineerProgramRole,
                                "uniqueCustomerCode": "",
                                "frontendUserInfo": {},
                                "serviceType": "programSetup",
                                "isApproved": true,
                                "approvedBy": this.programUserInfo.programUserId
                            };
                            console.log("obj2",obj2);
                            this.addProgramUser(obj2);

                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            break;
                        case responseCodes.RESP_FAIL:
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }
    /**
        * METHOD   : addProgramUsersMapping
        * DESC     : calls 'addProgramUsersMappingAPI' from ProgramSetupService, and handles the response
        *
        */
    addProgramUser(requestObj) {
        this.showLoader = true;
        this.userService.addProgramUser(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, "Engineer Request Not Approved", "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, "Engineer Request Not Approved", "alert-danger");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        ;
                        window.scrollTo(0, 0);
                        console.log(" this.feedbackMessageComponent", this.feedbackMessageComponent);
                        this.feedbackMessageComponent.updateMessage(true, "Engineer registration request was approved !!", "alert-success");
                        this.formReset();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer request not approved !!", "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        this.feedbackMessageComponent.updateMessage(true, "Engineer Request Not Approved", "alert-danger");
                        break;
                    case responseCodes.RESP_BLOCKED: this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer request not approved !!", "alert-danger");
                        break;
                }
            },
                err => {
                    // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger", this.errorMessageService.getErrorMessages().connectionErr);
                }
            );
    }

    updateEntity(){
        console.log("update");
        console.log("this.approvalForm.value.status",this.approvalForm.value.status);
        if(this.approvalForm.value.status === "approved"){
            this.approveClick();
        }else if(this.approvalForm.value.status === "rejected"){
            this.rejectEntity();
        }
    }

    
    rejectEntity() {
        var userName;
        if(this.programUserInfo.userDetails){
            userName =  this.programUserInfo.userDetails.userName;
        }

        var today:any = new Date();
        var dd:any = today.getDate();
        var mm:any = today.getMonth()+1; //January is 0!
        var yyyy:any = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 
        var date = mm + '/' + dd + '/' + yyyy;
        console.log("date",date);
        var requestObject1 = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "kycStatus": false,
            "isStaging": true,
            "businessEntityDetails": {
                "businessId" : this.businessId,
                "approvalStatus" : "rejected",
                "rejectedById": this.programUserInfo.programUserId,
                "rejectedByName": userName,
                "rejectedDate": date,
                "reasonForRejection": this.approvalForm.value.reasonForRejection
            }
        }
        console.log("this.userInfo",this.userInfo);
        console.log("this.programUserInfo",this.programUserInfo);
        console.log("requestObject1",requestObject1);
        this.showLoader = true;
        this.userService.migrateBusinessEntity(requestObject1)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer Request Not Rejected", "alert-success");
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer Request Not Rejected", "alert-success");
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.feedbackMessageComponent.updateMessage(true, "Engineer Request Rejected!!", "alert-success");
                            window.scroll(0, 0);
                            this.formReset();
                            break;
                        case responseCodes.RESP_AUTH_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer Request Not Rejected", "alert-success");
                            break;
                        case responseCodes.RESP_FAIL:
                            this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer Request Not Rejected", "alert-success");
                            window.scroll(0, 0);
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.feedbackMessageComponent.updateMessage(true, "Some error occured: Engineer Request Not Rejected", "alert-success");
                            break;
                    }
                },
                err => {
                    this.showLoader = false;
                }
            );
    }

    formReset(){
        this.approvalForm.reset();
        this.canUpdate = false;
    }
}


