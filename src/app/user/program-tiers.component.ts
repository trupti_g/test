// }
/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery
	Date Created	: 	03 April 2017
	Date Modified	: 	19 April 2017
*/

import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { GlobalRewardGalleryService } from "../global-rewards-gallery/rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from '../home/home.service';
import { UserService } from "./user.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './program-tiers.component.html',
	styleUrls: ['./program-tiers.component.scss'],
	providers: [Device]

})
export class ProgramTiersComponent implements OnInit {
	programUserInfo: any;
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;

	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private productDetailsService: ProductDetailsService,
		private rewardsGalleryService: GlobalRewardGalleryService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private homeService: HomeService,
		public sidebar: SidebarComponent,
		private stringService: StringService,
		private device: Device,
		//private userService: UserService
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}
		this.programUserInfo = configService.getloggedInProgramUser();
	}

	ngOnInit() {
		this.sidebar.close();

		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView("Program tiers")
		} else {
			this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga("set", "page", "Program tiers");
				ga("send", "pageview");
			}
			});
		}
	}
}