/*
	Author			:	Pratik Gawand
	Description		: 	Component for About Us page
	Date Created	: 	15 March 2016
	Date Modified	: 	15 March 2016

  */

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  templateUrl: "./manageraboutus.component.html",
  // styleUrls: ['./user.component.css'],
  styleUrls: ['../app.component.scss', './manageraboutus.component.scss', './aboutus.component.scss'],
  providers: [UserService, Device]
})
export class manageraboutusComponent implements OnInit {
  mobilewidth: any;
  displaypc: boolean = true;
  displaymobile: boolean = false;
  public aboutProgram: string;
  public termsAndCondition: string;
  public faqs: string;
  public programDescription: string;
  public isProduct: boolean;
  public bannerImage: string[] = [];
  public brochure: string;
  public programPoints: any = [];
  public pageCount: number = 1; //to maintain pagecount in pagination
  public isPrevious: boolean = true;
  public isNext: boolean = false;
  public loginProgramInfo: any = {};
  private limit: number = 10;
  private totalRecords: number = 0;
  private programName: string = "";

  public requestObject: any = {
    clientId: "",
    programId: "",
    limit: this.limit,
    skip: 0
  };
  public clientId: string;
  public programId: string;
  public programInfo: any;
  public showPoints: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private stringService: StringService,
    private userService: UserService,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
	public sidebar: SidebarComponent,
	private device: Device,
  ) {
    this.mobilewidth = window.screen.width;
    console.log("Width:", this.mobilewidth);
    if (this.mobilewidth <= 576) {
      this.displaypc = false;
      this.displaymobile = true;
    }
    else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
      this.displaypc = false;
      this.displaymobile = true;

    }
    else {
      this.displaypc = true;
      this.displaymobile = false;
    }
    this.loginProgramInfo = this.configService.getloggedInProgramInfo();
    console.log(" this.loginProgramInfo", this.loginProgramInfo);
    this.configService.setRewardGalleryFlag(false);
    this.programInfo = configService.getloggedInProgramUser();

    console.log("this.programInfo", this.programInfo);

    if(this.device.device === "android" || this.device.device === "ios"){
		console.log("inside android and ios");
		(<any>window).ga.trackView("About The Program")
	} else {
		this.router.events.subscribe(event => {
		if (event instanceof NavigationEnd) {
			ga("set", "page", "About The Program");
			ga("send", "pageview");
		}
		});
	}
  
    console.log("programInfo", this.programInfo);
    this.clientId = this.programInfo.clientId;
    this.programId = this.programInfo.programId;
    if (this.loginProgramInfo.basedOn === "product") {
      this.isProduct = true;
    } else {
      this.isProduct = false;
    }
    this.requestObject.clientId = this.clientId;
    this.requestObject.programId = this.programId;
    this.getProgramPoints(this.requestObject);
    this.isPrevious = true;
    this.isNext = false;
  }

  downloadBrochure() {
    this.googleAnalyticsEventsService.emitEvent(
      "About The Program",
      "",
      "downloadBrochure"
    );
  }

  goToLink(location: string): void {
    window.location.hash = location;
  }

  config: Object = {
    pagination: ".swiper-pagination",
    paginationClickable: true,
    nextButton: ".next",
    prevButton: ".prev",
    spaceBetween: 50,
    autoplay: 2000
  };

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.sidebar.close();
    let programInfo = this.configService.getloggedInProgramInfo();
    this.programName = programInfo.programName;
    this.aboutProgram = programInfo.aboutProgram;
    this.termsAndCondition = programInfo.termsAndCondition;
    this.faqs = programInfo.faqs;
    this.programDescription = programInfo.programDescription;
    // this.bannerImage = programInfo.programBannerImages;
    for (let i = 0; i < programInfo.programBannerImages.length; i++) {
      if (programInfo.programBannerImages[i] != null) {
        this.bannerImage.push(programInfo.programBannerImages[i]);
      }
    }

    if (programInfo.brochure) {
      this.brochure = programInfo.brochure;
    }

  }

  getAboutProgramPoints() {
    console.log("Request oBject", this.requestObject);
    this.getProgramPoints(this.requestObject);
  }

  onPrevClick() {
    this.requestObject.skip = this.requestObject.skip - this.limit;
    this.pageCount--;
    this.getAboutProgramPoints();
  }

  onNextClick() {
    this.requestObject.skip = this.requestObject.skip + this.limit;
    this.pageCount++;
    this.getAboutProgramPoints();
  }

  goToPage(page) {
    if (page === "PT") {
      this.router.navigate(['/home/programtiers']);
    }
    if (page === "PR") {
      this.router.navigate(['/home/privileges']);
    }
  }
  /***************************************	API CALLS 	****************************************************/
  /*
        METHOD         : updateClientUser()
        DESCRIPTION    : To update client user info
    */
  getProgramPoints(requestObject) {
    this.userService.getProgramPoints(requestObject).subscribe(
      responseObject => {
        let responseCodes = this.configService.getStatusTokens();

        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:
            console.log("Result", responseObject.result);
            this.programPoints = responseObject.result;
            this.showPoints = true;

            if (this.programPoints.length < 10) {
              this.isNext = true;
            } else {
              this.isNext = false;
            }

            if (this.pageCount == 1) {
              this.isPrevious = true;
            } else {
              this.isPrevious = false;
            }
            if (this.requestObject.skip == 0) {
              this.totalRecords = responseObject.count;
            }

            if (this.totalRecords == this.pageCount * this.limit) {
              this.isNext = true;
            }

            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => { }
    );
  }
  /************************************ 	END OF API CALLS 	************************************************/
  scrollActivate() {
    console.log("calling setModalClose");
    this.configService.setModalClose(true);
    var scroll = document.getElementById('body');
    console.log("RRRRRRRRRRRrr", scroll);


    document.getElementById("body").style.overflow = "auto";
    console.log("RRRRRRRRRRRRRRRRRRrr", scroll);

  }
  stopScroll() {
    // Code to active backgroung scrolling when side bar is close
    var scroll = document.getElementById('body');
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    this.configService.setModalClose(false);

    document.getElementById("body").style.overflow = "hidden";
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
  }
}
