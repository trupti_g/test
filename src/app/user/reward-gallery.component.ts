/*
	Author			:	Hrishikesh
	Description		: 	Component for About Us page
	Date Created	: 	29 sept 2017
	Date Modified	: 	29 sept 2017
*/

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component'


@Component({
    templateUrl: './reward-gallery.component.html',
    styleUrls: ['./reward-gallery.component.scss'],
    providers: [UserService]

})

export class RewardGalleryComponent implements OnInit {
    public loginProgramInfo: any;
    public testimonial: any;

    public programInfo: any;
    public showPoints: boolean = false;
    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private stringService: StringService,
        private userService: UserService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,

    ) {
        this.loginProgramInfo = this.configService.getloggedInProgramInfo();
        console.log("loginProgramInfo", this.loginProgramInfo);
        this.testimonial = this.loginProgramInfo.testimonial;
        console.log("testiii", this.testimonial);
    }

    ngOnInit() {
        this.sidebar.close();

    }
}