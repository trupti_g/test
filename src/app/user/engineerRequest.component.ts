

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from "../footer/app-footer.component";
import { ClientFooterComponent } from "../footer/client-footer.component";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { CsvService } from "../shared-services/csv.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: "./engineerRequest.component.html",
    styleUrls: ["../app.component.scss", "./engineerRequest.component.scss"],
    providers: [Device]

})
export class EngineerRequestComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    isNoRecords: boolean = false;
    public showLoader: boolean = false;
    public entitiesList: Array<any> = [];
    businessEnityArray: any;
    public cityArray: Array<any> = [];
    public stateArray: Array<any> = [];
    programUserInfo: any;
    private limit: number;
    private isPrevious: boolean = false;
    private isNext: boolean = false;
    private isCountCanBeShown: boolean;
    private pageCount: number;
    private totalRecords: number;
    private startRecord: number;
    private endRecord: number;
    public statesArray: string[] = [];
    private skip: number;
    private requestObj: any = {};
    // download report
    private printingArray: any[];
    private stateForm: FormGroup;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private userService: UserService,
        private stringService: StringService,
        private formBuilder: FormBuilder,
        public sidebar: SidebarComponent,
        private _csvService: CsvService,
        private device: Device,
  ) {
        this.programUserInfo = configService.getloggedInProgramUser();
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }

    }
    ngOnInit() {

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Engineer request")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Engineer request");
                    ga('send', 'pageview');
                }
            });
        }
        this.sidebar.close();
        this.pageCount = 1;
        this.totalRecords = 0;
        this.startRecord = 0;
        this.endRecord = 0;
        this.limit = 10;
        this.skip = 0;
        this.requestObj = {};
        console.log("this.displaypc",this.displaypc);
        console.log("this.displaymobile",this.displaymobile);


        this.requestObj = {

            "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
            "programId": this.configService.getprogramInfo().programId,
            "skip": this.skip,
            "limit": this.limit,
            "kycStatus": false,
            "sort": {
                "createdAt": -1
            },
            "userId": this.programUserInfo.programUserId,


        }
        if (this.configService.getLoggedInUserInfo().statesMapped && this.configService.getLoggedInUserInfo().statesMapped.length > 0) {
            this.stateArray = this.configService.getLoggedInUserInfo().statesMapped;
            this.requestObj.stateArray = this.stateArray;
        }
        if (this.configService.getLoggedInUserInfo().citiesMapped && this.configService.getLoggedInUserInfo().citiesMapped.length > 0) {
            this.cityArray = this.configService.getLoggedInUserInfo().citiesMapped;
            this.requestObj.cityArray = this.cityArray;
        }
        console.log("this.configService.getSelectedFilter()",this.configService.getSelectedFilter());
        console.log("this.configService.getEngineerRequestFilter()",this.configService.getEngineerRequestFilter());
        if(this.configService.getSelectedFilter() && this.configService.getEngineerRequestFilter().stateFilter!==undefined && this.configService.getEngineerRequestFilter().stateFilter!==null 
        && this.configService.getEngineerRequestFilter().statusFilter!==undefined && this.configService.getEngineerRequestFilter().statusFilter!==null){
            this.requestObj.approvalStatus = this.configService.getEngineerRequestFilter().statusFilter;
            if(this.configService.getEngineerRequestFilter().stateFilter != "All"){
                this.requestObj.stateArray = [this.configService.getEngineerRequestFilter().stateFilter];
            }
            
            this.configService.setSelectedFilter(false);
        }else{
            this.requestObj.approvalStatus = "pending";
        }

        console.log("this.configService.getLoggedInUserInfo().citiesMapped", this.configService.getLoggedInUserInfo().citiesMapped)
        console.log("this.configService.getLoggedInUserInfo().statesMapped", this.configService.getLoggedInUserInfo().statesMapped);
        console.log("Business Enirity Object", this.requestObj);
        if(this.requestObj.approvalStatus === "approved"){
            this.getApprovedEngineerRequest(this.requestObj);
        }else{
            this.getBusinessEntity(this.requestObj);
        }
        // this.getBusinessEntity(this.requestObj);
        this.getStates();
        this.stateForm = this.formBuilder.group({
            state: [],
            approvalStatus: []
        });
        var statePatchValue;
        if(this.requestObj.stateArray && this.requestObj.stateArray[0]!=undefined && this.requestObj.stateArray[0]!==null){
            statePatchValue = this.requestObj.stateArray[0];
        }else{
            statePatchValue = "All"
        }
        this.stateForm.patchValue({ 
            "state": statePatchValue,
            "approvalStatus": this.requestObj.approvalStatus
        })
        this.valueChanges();
    }

    valueChanges() {
        this.stateForm.valueChanges.subscribe(data => {
            console.log("data", data);
            this.requestObj.skip = 0;
            if (data.state != null && data.state != "") {
                var tempStateArr = [];
                tempStateArr.push(data.state);
                this.requestObj.stateArray = tempStateArr;
                
                if (data.state === "All") {
                    console.log("inside All", data.state);
                    delete this.requestObj.stateArray;
                    // in case user is mapped to certain states or cities
                    if (this.configService.getLoggedInUserInfo().statesMapped && this.configService.getLoggedInUserInfo().statesMapped.length > 0) {
                        this.stateArray = this.configService.getLoggedInUserInfo().statesMapped;
                        this.requestObj.stateArray = this.stateArray;
                    }
                    if (this.configService.getLoggedInUserInfo().citiesMapped && this.configService.getLoggedInUserInfo().citiesMapped.length > 0) {
                        this.cityArray = this.configService.getLoggedInUserInfo().citiesMapped;
                        this.requestObj.cityArray = this.cityArray;
                    }
                }
                console.log("this.requestObj", this.requestObj);
                
            }
            if(data.approvalStatus != null && data.approvalStatus != ""){
                this.requestObj.approvalStatus = data.approvalStatus;
            }
            if(data.approvalStatus === "approved"){
                delete this.requestObj.rejectedById;
                this.getApprovedEngineerRequest(this.requestObj);
            }else if(data.approvalStatus === "rejected"){
                delete this.requestObj.approvedBy;
                this.requestObj.rejectedById = this.programUserInfo.programUserId;
                this.getBusinessEntity(this.requestObj);
            }else if(data.approvalStatus === "pending"){
                delete this.requestObj.approvedBy;
                delete this.requestObj.rejectedById;
                this.getBusinessEntity(this.requestObj);
            }
            
        });
    }

    reset() {
        this.stateForm.reset();
        this.stateForm.patchValue({
            "state": "All",
            "approvalStatus": "pending"
        })
        this.requestObj.skip = 0;
        this.requestObj.approvalStatus = "pending";
        delete this.requestObj.stateArray;
        this.showLoader = true;
        this.getBusinessEntity(this.requestObj);
    }

    getStates() {
        this.showLoader = true;

        let states: string[] = [];
        this.configService.getStates().subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                console.log("eng request -- responseObject", responseObject);
                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
                    for (let i = 0; i < responseObject.result.length; i++) {
                        this.stateArray[i] = responseObject.result[i].stateName;

                    }
                }
                else {
                    //	this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                }
            },
            err => {
                this.showLoader = false;
                //	this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
            }
        );
        console.log("stateArray --", this.stateArray);

    }

    getBusinessEntity(requestObject) {
        console.log("response");
        this.showLoader = true;
        this.entitiesList = [];
        this.userService.getBusinessEntity(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("getBusinessEntity", responseObject);

                        if (responseObject.result) {
                            this.isNoRecords = false;
                            this.entitiesList = responseObject.result;
                            console.log("List ArrayehfuwiuhvkEVKNRUVKUERNVJKRNI", this.entitiesList, this.totalRecords);
                            for (var x = 0; x < this.entitiesList.length; x++) {

                                for (let user of this.entitiesList[x].businessEntityUsers) {
                                    if (user.userType === "owner") {
                                        this.entitiesList[x].ownerName = user.fullName;
                                        this.entitiesList[x].ownerNumber = user.mobileNumber;
                                    }
                                }
                            }
                        }
                        if (this.entitiesList.length < this.limit) {
                            this.isNext = true;
                        } else {
                            this.isNext = false;
                        }
  
                        if (this.pageCount == 1) {
                            this.isPrevious = true;
                        } else {
                            this.isPrevious = false;
                        }
                        console.log("this.requestObj.skip", this.requestObj.skip);
                        if (this.requestObj.skip == 0) {
                            this.totalRecords = responseObject.count;
                        }
                        console.log("responseObject.count", responseObject.count, this.totalRecords);
                        // code to show no. of records on navigation page
                        if (this.entitiesList.length > 0) {
                            this.isCountCanBeShown = true;
                            if (this.pageCount > 1) {
                                this.startRecord = this.pageCount * this.limit - (this.limit - 1);
                                this.endRecord = this.startRecord + (responseObject.result.length - 1);
                            } else {
                                this.startRecord = 1;
                                this.endRecord = this.startRecord + (responseObject.result.length - 1);
                            }
                        } else {
                            this.isCountCanBeShown = false;
                        }
                        console.log("gukHVKCHRVHKWHKCHIK", this.totalRecords);
                        //Disable next button if total records are equal to pagination limit
                        if (this.totalRecords === this.pageCount * this.limit) {
                            // //console.log("counts true");
                            this.isNext = true;
                        }
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_FAIL:
                        this.isNoRecords = true;
                        this.entitiesList = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }


    // to get approved engineer requests
    getApprovedEngineerRequest(requestObject) {
        console.log("this.programUserInfo.programRole",this.programUserInfo.programRole);
        // if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
        // || this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole){
            requestObject.approvedBy = this.programUserInfo.programUserId;
        // }

        console.log("response");
        this.showLoader = true;
        this.entitiesList = [];
        this.userService.getApprovedEngineerRequest(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("getApprovedEngineerRequest", responseObject);

                        if (responseObject.result) {
                            this.isNoRecords = false;
                            this.entitiesList = responseObject.result;
                            console.log("List ArrayehfuwiuhvkEVKNRUVKUERNVJKRNI", this.entitiesList, this.totalRecords);
                        }

                        if (this.entitiesList.length < this.limit) {
                            this.isNext = true;
                        } else {
                            this.isNext = false;
                        }
  
                        if (this.pageCount == 1) {
                            this.isPrevious = true;
                        } else {
                            this.isPrevious = false;
                        }
                        console.log("this.requestObj.skip", this.requestObj.skip);
                        if (this.requestObj.skip == 0) {
                            this.totalRecords = responseObject.count;
                        }
                        console.log("responseObject.count", responseObject.count, this.totalRecords);
                        // code to show no. of records on navigation page
                        if (this.entitiesList.length > 0) {
                            this.isCountCanBeShown = true;
                            if (this.pageCount > 1) {
                                this.startRecord = this.pageCount * this.limit - (this.limit - 1);
                                this.endRecord = this.startRecord + (responseObject.result.length - 1);
                            } else {
                                this.startRecord = 1;
                                this.endRecord = this.startRecord + (responseObject.result.length - 1);
                            }
                        } else {
                            this.isCountCanBeShown = false;
                        }
                        console.log("gukHVKCHRVHKWHKCHIK", this.totalRecords);
                        //Disable next button if total records are equal to pagination limit
                        if (this.totalRecords === this.pageCount * this.limit) {
                            // //console.log("counts true");
                            this.isNext = true;
                        }
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.isNoRecords = true;
                        break;
                    case responseCodes.RESP_FAIL:
                        this.isNoRecords = true;
                        this.entitiesList = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }



    showDetail(entity) {
        var entityDetails = entity;
        if(this.stateForm.value.approvalStatus){
            entityDetails.approvalStatus = this.stateForm.value.approvalStatus;
            this.configService.setEntity(entityDetails);
        }
        
        this.configService.setEngineerRequestFilter(this.stateForm.value.approvalStatus,this.stateForm.value.state);
        this.router.navigate(["/home/entity-details"]);
    }

    prev() {
        this.requestObj.skip = this.requestObj.skip - this.limit;
        this.skip = this.requestObj.skip;
        this.pageCount--;
        if(this.stateForm.value.approvalStatus != "approved"){
            this.getBusinessEntity(this.requestObj);
        }else{
            this.getApprovedEngineerRequest(this.requestObj);
        }
        // this.getBusinessEntity(this.requestObj);
    }

    /**
     * METHOD   : next
     * DESC     : to go to the next screen of records
     */
    next() {
        this.requestObj.skip = this.requestObj.skip + this.limit;
        this.skip = this.requestObj.skip;
        this.pageCount++;

        if(this.stateForm.value.approvalStatus != "approved"){
            this.getBusinessEntity(this.requestObj);
        }else{
            this.getApprovedEngineerRequest(this.requestObj);
        }
        // this.getBusinessEntity(this.requestObj);
    }


    /**
     * METHOD   : getAllEngineerRequestsReport
     * DESC     : to fetch engineer request's report
     */
    getAllEngineerRequestsReport() {
        var reqObj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
            "programId": this.configService.getprogramInfo().programId,
            "skip": 0,
            "kycStatus": false,
            "sort": {
                "createdAt": -1
            },
            "userId": this.programUserInfo.programUserId,
            "stateArray": [],
            "cityArray": [],
            "approvalStatus": "pending"
        };
        console.log("this.stateForm.value.state", this.stateForm.value.state);
        console.log("this.stateForm.value.state !== null", this.stateForm.value.state !== null);
        if (this.stateForm.value.state !== null && this.stateForm.value.state !== "") {
            var tempArr = [];
            tempArr.push(this.stateForm.value.state);
            reqObj.stateArray = tempArr;
        }
        else {
            delete reqObj.stateArray;
        }
        delete reqObj.cityArray;
        // incase filter is selected as all and user is mapped to certain states and cities
        if (this.stateForm.value.state === "All" || this.stateForm.value.state === null) {
            if (this.configService.getLoggedInUserInfo().statesMapped && this.configService.getLoggedInUserInfo().statesMapped.length > 0) {
                this.stateArray = this.configService.getLoggedInUserInfo().statesMapped;
                reqObj.stateArray = this.stateArray;
            }
            else {
                delete reqObj.stateArray;
            }
            if (this.configService.getLoggedInUserInfo().citiesMapped && this.configService.getLoggedInUserInfo().citiesMapped.length > 0) {
                this.cityArray = this.configService.getLoggedInUserInfo().citiesMapped;
                reqObj.cityArray = this.cityArray;
            }
        }

        if (this.stateForm.value.approvalStatus != "" && this.stateForm.value.approvalStatus != null
         && this.stateForm.value.approvalStatus != undefined) {
            reqObj.approvalStatus = this.stateForm.value.approvalStatus
        }

        if(this.stateForm.value.approvalStatus != "approved"){
            this.getBusinessEntityReport(reqObj);
        }else{
            this.getProgramUserReport(reqObj);
        }
    }

    getBusinessEntityReport(reqObj){
        console.log("reqObj", reqObj);
        this.showLoader = true;
        this.userService.getBusinessEntity(reqObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("getBusinessEntity", responseObject);

                        if (responseObject.result && responseObject.result.length > 0) {
                            var enitiesArr = responseObject.result;
                            console.log("enitiesArr", enitiesArr);

                            for (var i = 0; i < enitiesArr.length; i++) {
                                for (let user of enitiesArr[i].businessEntityUsers) {
                                    if (user.userType === "owner") {
                                        enitiesArr[i].ownerName = user.fullName;
                                        enitiesArr[i].ownerNumber = user.mobileNumber;
                                        console.log("enitiesArr[i].ownerName", enitiesArr[i].ownerName);
                                        console.log(" enitiesArr[i].ownerNumber", enitiesArr[i].ownerNumber);
                                    }
                                }
                            }
                        }
                        console.log("final enitiesArr", enitiesArr);
                        console.log("reqObj.stateArray", reqObj.stateArray);
                        var state = (reqObj.stateArray != undefined) ? reqObj.stateArray[0] : "";
                        console.log("state", state);
                        this.generateEngineerRequestReport(enitiesArr, state);

                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }

    getProgramUserReport(reqObj){
        console.log("inside getProgramUserReport",reqObj);
        if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
        || this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole){
            reqObj.approvedBy = this.programUserInfo.programUserId;
        }

        console.log("response");
        this.showLoader = true;
        this.userService.getApprovedEngineerRequest(reqObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("getProgramUserReport", responseObject);

                        if (responseObject.result && responseObject.result.length > 0) {
                            var enitiesArr = responseObject.result;
                            console.log("enitiesArr", enitiesArr);

                           
                        }
                        console.log("final enitiesArr", enitiesArr);
                        console.log("reqObj.stateArray", reqObj.stateArray);
                        var state = (reqObj.stateArray != undefined) ? reqObj.stateArray[0] : "";
                        console.log("state", state);
                        this.generateApprovedEngineerRequestReport(enitiesArr, state);

                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
                err => {

                }
            );
    }

    generateApprovedEngineerRequestReport(entities,stateTitle){
        this.printingArray = [];
        console.log("inside generateEngineerRequestReport", entities);

        for (var i = 0; i < entities.length; i++) {
            var temp = {};
            temp["BE Name"] = entities[i].businessName;
            temp["Code"] = entities[i].businessId;
            temp["State"] = entities[i].state;
            temp["City"] = entities[i].city;
            temp["Address"] = entities[i].streetAddress;
            temp["Pincode"] = entities[i].pincode;
            temp["Owner Name"] = entities[i].ownerName;
            temp["Owner Number"] = entities[i].ownerNumber;
            temp["Status"] = "approved";

            temp["Approved by"] = entities[i].approvedBy;
            temp["Approved by Code"] = entities[i].approvedById;
            temp["Date of approval"] = entities[i].approvedByDate;
            
            this.printingArray.push(temp)
        }
        console.log("stateTitle", stateTitle);
        var fileTitle = (stateTitle != "") ? stateTitle + "_Engineers_Request_Report" : 'Engineers_Request_Report';
        console.log("fileTitle", fileTitle);

        this._csvService.download(this.printingArray, fileTitle);
    }

    /**
     * METHOD   : generateEngineerRequestReport
     * DESC     : to generate engineer request's report
     */
    generateEngineerRequestReport(entities, stateTitle) {
        this.printingArray = [];
        console.log("inside generateEngineerRequestReport", entities);

        for (var i = 0; i < entities.length; i++) {
            var temp = {};
            temp["BE Name"] = entities[i].businessName;
            temp["Code"] = entities[i].businessId;
            temp["State"] = entities[i].state;
            temp["City"] = entities[i].city;
            temp["Address"] = entities[i].streetAddress;
            temp["Pincode"] = entities[i].pincode;
            temp["Owner Name"] = entities[i].ownerName;
            temp["Owner Number"] = entities[i].ownerNumber;
            temp["Status"] = entities[i].approvalStatus;

            if(entities[i].approvalStatus === "rejected"){
                temp["Rejected by"] = entities[i].rejectedByName;
                temp["Rejected by Code"] = entities[i].rejectedById;
                temp["Reason For rejection"] = entities[i].reasonForRejection;
                temp["Date of rejection"] = entities[i].rejectedDate;
            }

            this.printingArray.push(temp)
        }
        console.log("stateTitle", stateTitle);
        var fileTitle = (stateTitle != "") ? stateTitle + "_Engineers_Request_Report" : 'Engineers_Request_Report';
        console.log("fileTitle", fileTitle);

        this._csvService.download(this.printingArray, fileTitle);

    }

}
