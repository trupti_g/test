/*
	Author			:	Deepak Terse
	Description		: 	Component for client profile page
	Date Created	: 	07 March 2016
	Date Modified	: 	12 March 2016
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { FeedbackMessageComponent } from "./feedback-message.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
	templateUrl: './client-profile.component.html',
	// styleUrls: ['./user.component.css'],
	styleUrls: ["../app.component.scss",'./client-profile.component.scss'],
	providers: [UserService, Device]

})
export class ClientProfileComponent implements OnInit {
	clientsArr: any;
    mobilewidth: any;                   //Added for width capture
    displaypc: boolean = true;        //Added for width capture
    displaymobile: boolean = false;   //Added for width capture
	private profileForm: FormGroup;
	private passwordForm: FormGroup;

	private showLoader: boolean;

	private showPasswordField: boolean;
	private userInfo: any;
	private editCEForm: boolean = true;
	private editFormCEBtn: boolean = true;
	public clientColor: any;

	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private userService: UserService,
		private stringService: StringService,
		private formBuilder: FormBuilder,
		public sidebar: SidebarComponent,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private device: Device,) {
	
		this.configService.setRewardGalleryFlag(false);
		this.showPasswordField = false;
		this.showLoader = false;
		this.clientColor = this.configService.getThemeColor();

		// code for width capture

		this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;
        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
	}


	ngOnInit() {
		this.setUserInfo();
		this.initProfileForm();
		this.initPasswordForm();
		this.sidebar.close();

		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView("Client Profile")
		} else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Client Profile");
					ga('send', 'pageview');
				}
			});
		}

		// this.assignProfileForm();
		var reqObj = {
			"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
			"clientUserId": this.userInfo.clientUserId,
			"clientId": this.userInfo.clientId,
		}
		this.getClientUser(reqObj);
	}

	getClientUser(requestObject) {
		console.log("response");
		this.showLoader = true;
		this.userService.getClientUser(requestObject)
			.subscribe((responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.clientsArr = responseObject.result;
						this.userInfo = responseObject.result;
						this.assignProfileForm();
						console.log("response", responseObject.result);
						console.log("clientsArr", this.clientsArr);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
			err => {

			}
			);
	}
	/*
		METHOD         : initProfileForm()
		DESCRIPTION    : Called from ngOnInit when the component is loaded
						 Sets userInfo and bEInfo
	*/
	setUserInfo() {
		this.userInfo = this.configService.getLoggedInUserInfo();
	}




	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initProfileForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates profile form group and assign initial values and validations to its controls
    */
	initProfileForm() {
		this.profileForm = this.formBuilder.group({
			firstName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this.stringService.getRegex().alphaRegex)]],
			lastName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this.stringService.getRegex().alphaRegex)]],
			emailId: ["", [Validators.required, Validators.pattern(/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/)]],
			mobileNumber: ["", [Validators.required, Validators.pattern('[0-9]{10}')]],
			empId: ["", []],
			designationName: ["", []],
			divisionName: ["", []],
			parentUserName: ["", []]
		});
	}

	/*
        METHOD         : initPasswordForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates password form group and assign inital values and validations to its controls
    */
	initPasswordForm() {
		this.passwordForm = this.formBuilder.group({
			password: ["", [Validators.required]]
		});
	}

	/*
		METHOD         : assignProfileForm()
		DESCRIPTION    : Called from ngOnInit when the component is loaded and after form initalisation
						 Assign initial values to form controls of profile form group
	*/
	assignProfileForm() {
		this.profileForm.patchValue({
			"firstName": this.userInfo.firstName,
			"lastName": this.userInfo.lastName,
			"emailId": this.userInfo.emailId,
			"mobileNumber": this.userInfo.mobileNumber,
			"empId": this.userInfo.empId,
			"designationName": this.userInfo.designationName,
			"divisionName": this.userInfo.divisionName,
			"parentUserName": this.userInfo.parentUserName
		});
	}
	/***************************** 		END OF FORM METHODS 	********************************/

	changeCEForm() {
		this.editCEForm = false;
		this.editFormCEBtn = false;
	}


	/******************************************* 	ONCLICK METHODS 	**********************************************/
	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls assignProfileForm() to reset form with the values that were before editing
    */
	onResetFormClick() {
		this.googleAnalyticsEventsService.emitEvent("Client Profile", this.userInfo.clientUserId, "resetClientInformation");
		this.assignProfileForm();
	}

	/*
        METHOD         : onUpdateFormClick()
        DESCRIPTION    : Called when user clicks on "UPDATE INFORMATION" button
						 Detects changed values in forms, forms request object and calls updateClientUser() to
						 call updateclientuser api
    */
	onUpdateFormClick() {

		let inputObject = this.profileForm.value;
		this.googleAnalyticsEventsService.emitEvent("Client Profile", this.userInfo.clientUserId, "updateClientInformation");
		let updatedValues: any = {};

		if (inputObject.firstName != this.userInfo.firstName)
			updatedValues.firstName = inputObject.firstName;
		if (inputObject.lastName != this.userInfo.lastName)
			updatedValues.lastName = inputObject.lastName;
		if (inputObject.lastName != this.userInfo.lastName || inputObject.firstName != this.userInfo.firstName)
			updatedValues.fullName = inputObject.firstName + " " + inputObject.lastName;
		if (inputObject.mobileNumber != this.userInfo.mobileNumber)
			updatedValues.mobileNumber = inputObject.mobileNumber;
		if (inputObject.emailId != this.userInfo.emailId)
			updatedValues.emailId = inputObject.emailId;

		let reqObj: any = {
			"clientUserId": this.userInfo.clientUserId,
			"clientId": this.userInfo.clientId,
			"userInfo": updatedValues
		}

		this.updateClientUser(reqObj);
		this.router
		{
		   window.scrollTo(0, 0);
		 }
	}

	/*
        METHOD         : onUpdatePasswordClick()
        DESCRIPTION    : Called when user clicks on "UPDATE PASSWORD" button
				      	 Shows password form
    */
	onUpdatePasswordClick() {
		this.showPasswordField = true;
	}

	/*
        METHOD         : onCancelPasswordClick()
        DESCRIPTION    : Called when user clicks on "CANCEL" button
				      	 Hides password form
    */
	onCancelPasswordClick() {
		console.log("Inside Password Cancel");
		this.showPasswordField = false;
	}

	/*
        METHOD         : onSubmitPasswordClick()
        DESCRIPTION    : Called when user clicks on "SUBMIT" button
				      	 Forms request object and calls updateClientUser to update user password
    */
	onSubmitPasswordClick() {
		this.router
		{
		   window.scrollTo(0, 0);
		}
		let inputObject = this.passwordForm.value;
		this.googleAnalyticsEventsService.emitEvent("Client Profile", this.userInfo.clientUserId, "updatePassword");
		let reqObj: any = {
			"clientUserId": this.userInfo.clientUserId,
			"clientId": this.userInfo.clientId,
			"userInfo": {
				"password": inputObject.password
			}
		}

		this.updateClientUser(reqObj);
	}


	/************************************ 	END OF ONCLICK METHODS 	********************************************/




	/***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : updateClientUser()
        DESCRIPTION    : To update client user info
    */
	updateClientUser(requestObject) {
		this.showLoader = true;

		this.userService.updateClientUser(requestObject)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						this.feedbackMessageComponent.updateMessage(true, "User Information Updated Successfully", "alert-success");

						this.configService.setLoggedInUserInfo(responseObject.result);
						this.setUserInfo();
						this.assignProfileForm();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.googleAnalyticsEventsService.emitEvent("Client Profile", this.userInfo.clientUserId, "updateClientInformationFailed");
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;

				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}
	/************************************ 	END OF API CALLS 	************************************************/
}
