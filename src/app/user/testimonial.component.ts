/*
	Author			:	Hrishikesh
	Description		: 	Component for About Us page
	Date Created	: 	29 sept 2017
	Date Modified	: 	29 sept 2017
*/

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './testimonial.component.html',
    styleUrls: ['../app.component.scss', "./testimonial.component.scss"],
    providers: [UserService, Device]

})

export class TestimonialComponent implements OnInit {
    mobilewidth: any;
    displaypc: boolean = true;
    displaymobile: boolean = false;
    public loginProgramInfo: any;
    public testimonial: any;
    programUserInfo: any;
    public programInfo: any;
    public showPoints: boolean = false;
    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private stringService: StringService,
        private userService: UserService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        public sidebar: SidebarComponent,
        private device: Device,
    ) {
        this.mobilewidth = window.screen.width;
        console.log("Width:", this.mobilewidth);
        if (this.mobilewidth <= 576) {
            this.displaypc = false;
            this.displaymobile = true;
            console.log("display mobile val", this.displaymobile);
        }
        else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
            this.displaypc = false;
            this.displaymobile = true;

        }
        else {
            this.displaypc = true;
            this.displaymobile = false;
        }
        this.programUserInfo = configService.getloggedInProgramUser();
        this.loginProgramInfo = this.configService.getloggedInProgramInfo();
        console.log("loginProgramInfo", this.loginProgramInfo);
        this.testimonial = this.loginProgramInfo.testimonial;
        console.log("testiii", this.testimonial);
    }

    ngOnInit() {
        this.sidebar.close();

        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Testimonial")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Testimonial");
                    ga('send', 'pageview');
                }
            });
        }

    }
}