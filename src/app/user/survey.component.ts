

import * as Survey from 'survey-angular';
import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { ConfigService } from "../shared-services/config.service";
import { UserService } from "./user.service";


import { FeedbackMessageComponent } from "./feedback-message.component";


import { BusinessEntityProfileComponent } from "./business-entity-profile.component";

Survey.Survey.cssType = "bootstrap";

@Component({
    selector: 'survey1',
    template: `<div id='surveyElement'>
                </div>`,
    // providers: [UserService]
})
export class SurveyComponent implements OnInit {
    public surveyCompleted: boolean = false;
    edit: boolean;
    public programUserId: any;
    public programUserInfo: any;
    public answers: any;
    public user: any;
    public surveyJSON: any = {};
    public showLoader: boolean;
    public survey;
    public profileQuestions;
    public profileAnswersObj;
    public profileQuestionsLength;
    public allowEdit: boolean = false;
    public dom1: any;




    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    constructor(
        private configService: ConfigService,
        private userService: UserService,
    ) {
        this.surveyJSON.pages = [{ name: "page1", questions: [] }];

    };

    ngOnInit() {

        var reqObj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
            "clientId": this.configService.getprogramInfo().clientId,
            "programId": this.configService.getprogramInfo().programId,
        }
        this.getProfileQuestions(reqObj);
    };

    getProfileQuestions(requestObject) {
        this.programUserId = this.configService.getloggedInProgramUser().programUserId;
        console.log("response");
        this.showLoader = true;
        this.userService.getProfileQuestions(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        if (responseObject.result) {

                            for (var a = 0; a < responseObject.result.length; a++) {
                                if (responseObject.result[a].programsMapped) {
                                    delete responseObject.result[a].programsMapped;
                                };
                                if (responseObject.result[a].id) {
                                    delete responseObject.result[a].id;
                                }
                            }
                        }
                        console.log("response", responseObject.result);
                        if (this.surveyJSON.pages) {

                            this.surveyJSON.pages[0].questions = responseObject.result;
                        }
                        this.profileQuestions = responseObject.result;
                        console.log("surveyJSON", this.surveyJSON);
                        this.renderSurvey();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:

                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {

            }
            );
    }

    makeReadOnly() {
        this.survey.mode = 'display';
    }

    removeReadOnly() {
        var reqObj = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
            "clientId": this.configService.getprogramInfo().clientId,
            "programId": this.configService.getprogramInfo().programId,
        }
        this.getProfileQuestions(reqObj);

        this.edit = true;
    }

    renderSurvey() {
        this.survey = new Survey.Model(this.surveyJSON);
        Survey.SurveyNG.render("surveyElement", { model: this.survey });
        this.surveyCompleted = false;

        this.survey.completedHtml = "Thanks for updating the information !!";


        // Hide complete button
        this.dom1 = document.getElementsByClassName('sv_complete_btn');
        this.dom1[0].style.display = "none";
        this.onComplete();

        console.log(" this.surveyCompleted", this.surveyCompleted);

        var reqObj1 = {
            "frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
            "clientId": this.configService.getprogramInfo().clientId,
            "programId": this.configService.getprogramInfo().programId,
            "programUserId": this.programUserId,
        }

        this.getProgramUser(reqObj1);
    };

    onComplete() {

        this.survey.onComplete.add(function () {

            console.log("survey completed");

            var resultAsString = JSON.stringify(this.survey.data);
            this.answers = resultAsString;

            this.surveyCompleted = true;
            return this.answers;

        }.bind(this));
    }

    getProgramUser(reqObj) {
        this.showLoader = true;
        this.userService.getProgramUser(reqObj)
            .subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log("Answerrs", responseObject.result);
                        this.programUserInfo = responseObject.result[0];

                        this.profileAnswersObj = this.programUserInfo.answers;
                        this.profileQuestionsLength = this.profileQuestions.length;

                        for (var key in this.profileAnswersObj) {
                            console.log("answers", key, this.profileAnswersObj[key]);
                            for (var a = 0; a < this.profileQuestionsLength; a++) {
                                console.log("questions", this.profileQuestions[a])
                                if (key === this.profileQuestions[a].name) {
                                    this.survey.setValue(key, this.profileAnswersObj[key]);
                                }
                            }
                        }
                        console.log("this.survey.mode11", this.survey);
                        this.makeReadOnly();
                        if (this.edit === true) {
                            this.survey.mode = 'edit'
                        }
                        console.log("answer", this.surveyJSON);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
            );
    }

}


