
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class UserService {
	frontEndUserInfo: any;
	private headers;
	private options;
	private frontendUserInfo;
	private userInfo;

	constructor(private http: Http,
		private configService: ConfigService) {

		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.frontendUserInfo = this.configService.getFrontEndUserInfo();
		this.userInfo = this.configService.getLoggedInUserInfo();



	}

	updateClientUser(reqobj): any {
		let frontEndInfo = this.frontendUserInfo;

		console.log("this.userInfo", this.userInfo);
		frontEndInfo.userId = this.userInfo.clientUserId;
		reqobj.frontendUserInfo = frontEndInfo;
		reqobj.serviceType = "client";
		let url = this.configService.getApiUrls().updateClientUser;

		return this.http.post(url, reqobj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());	
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}



	/**************************    UPDATE EXISTING ENTITY     ****************************************/
	updateEntity(reqobj): any {

		let frontEndInfo = this.frontendUserInfo;
		frontEndInfo.userId = this.userInfo.userId;
		reqobj.frontendUserInfo = frontEndInfo

		let url = this.configService.getApiUrls().updateEntity;

		return this.http.post(url, reqobj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}
	/*******************************    END OF UPDATE EXISTING ENTITY     *****************************/



	/******************************* 	UPDATE EXISTING ENTITY USER 	******************************/
	updateEntityUser(reqobj): any {
		console.log("update entity user click");
		let frontEndInfo = this.frontendUserInfo;
		frontEndInfo.userId = this.userInfo.userId;
		console.log('User is ', this.userInfo);
		reqobj.frontendUserInfo = frontEndInfo

		console.log(reqobj);

		let url = this.configService.getApiUrls().updateEntityUser;

		return this.http.post(url, reqobj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				// return res.json();
				return Observable.throw(error.json().error || "Server error");
			});
	}
	/**************************     END OF UPDATE EXISTING ENTITY USER   *****************************/

	/******************************* 	Get Program Points	******************************/
	getProgramPoints(reqobj): any {

		var frontEndInfo = this.frontendUserInfo;

		if (frontEndInfo != null || frontEndInfo != undefined) {
			frontEndInfo.userId = this.userInfo.userId;
			reqobj.frontendUserInfo = frontEndInfo;

		} else {
			frontEndInfo = {};
			frontEndInfo['userId'] = this.userInfo.userId;
		}
		console.log("HERE IN ", frontEndInfo);
		reqobj.frontendUserInfo = frontEndInfo;



		reqobj.serviceType = "programSetup";
		let url = this.configService.getApiUrls().getAllScheme;

		return this.http.post(url, reqobj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || "Server error");
			});
	}


	getClientBrands(): any {
		let url = this.configService.getApiUrls().getClientBrands;

		var reqObj: any = {};

		reqObj.programId = this.configService.getprogramInfo().programId;
		reqObj.clientId = this.configService.getprogramInfo().clientId;
		reqObj.serviceType = "client";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userInfo.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getAllProduct(requestObj): any {
		requestObj.serviceType = "client";
		requestObj.appType = "adminApp";
		if (this.frontendUserInfo == undefined || this.frontendUserInfo == null) {
			requestObj.frontendUserInfo = {};
		}
		else {
			requestObj.frontendUserInfo = this.frontendUserInfo;
		}
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let url = this.configService.getApiUrls().getAllProducts;

		console.log(requestObj);

		return this.http.post(url, requestObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getClientCategoryAndSubCategory(): any {
		let url = this.configService.getApiUrls().getClientCategoryAndSubCategory;
		console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", url);
		var reqObj: any = {};
		reqObj.programId = this.configService.getprogramInfo().programId;
		reqObj.clientId = this.configService.getprogramInfo().clientId;
		reqObj.serviceType = "client";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo.appType) delete frontEndInfo.appType;
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userInfo.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}
	/**************************     END OF Get Program Points   *****************************/


	sendEmail(requestObject): any {

		requestObject.frontendUserInfo = this.frontendUserInfo;
		requestObject.clientId = this.configService.getprogramInfo().clientId;


		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().sendEmail;

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getFilterClientBrands(reqObj: any): any {
		let url = this.configService.getApiUrls().getFilterClientBrands;
		reqObj.programId = this.configService.getprogramInfo().programId;
		reqObj.clientId = this.configService.getprogramInfo().clientId;
		reqObj.serviceType = "client";

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) reqObj.frontendUserInfo = frontEndInfo;
		else reqObj.frontendUserInfo = { "userId": this.userInfo.userId };

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getBusinessEntity(requestObject: any): any {
		let url = this.configService.getApiUrls().getBusinessEntity;

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			requestObject.frontendUserInfo = frontEndInfo;
		} else {
			requestObject.frontendUserInfo = {};
		}

		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		return this.http.post(url, requestObject, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("error", error);
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	getClientUser(requestObject): any {

		let frontendUserInfo: any = {};
		requestObject.frontendUserInfo = {};
		requestObject.serviceType = "client";
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		console.log("RequestObject in Add Order Service", requestObject);
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().getClientUser;
		console.log("URLLLL", url);
		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("error", error);
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	//ProfileQuestions
	getProfileQuestions(requestObject: any): any {
		let url = this.configService.getApiUrls().getProfileQuestions;
		console.log("getProfileQuestions url", url);

		//get frontenduserinfo
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {

			requestObject.frontendUserInfo = frontEndInfo;
		} else {
			requestObject.frontendUserInfo = {};
		}

		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		return this.http.post(url, requestObject, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("error", error);
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}



	//UpdateProgramUsers
	updateProgramUsers(requestObject): any {
		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		requestObject.clientId = this.configService.getprogramInfo().clientId;
		requestObject.programId = this.configService.getprogramInfo().programId;
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().updateProgramUser;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}




	getProgramUser(reqObj): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;   
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}
		reqObj.serviceType = "programSetup";
		reqObj.frontendUserInfo = frontEndInfo;
		console.log(reqObj);
		let url = this.configService.getApiUrls().getProgramUser;
		reqObj.serviceType = "programSetup";
		return this
			.http
			.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	findBusinessEntityUser(requestObject): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;

		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}
		requestObject.frontendUserInfo = frontEndInfo;
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().findBusinessEntityUser;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || "Server error");
			});
	};

	migrateBusinessEntity(requestObject): any {

		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;     
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}
		requestObject.frontendUserInfo = frontEndInfo;
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().migrateBusinessEntity;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || "Server error");
			});
	};

	addProgramUser(requestObject): any {
		var frontEndInfo = this.configService.getFrontEndUserInfo();
		var userIds = this.userInfo.userId;     
		if (frontEndInfo == undefined) {
			frontEndInfo = {
				"userId": userIds
			}
		} else {

			frontEndInfo["userId"] = userIds;
		}
		requestObject.frontendUserInfo = frontEndInfo;
		requestObject.clientId = this.configService.getprogramInfo().clientId;
		requestObject.serviceType = "programSetup";
		requestObject.frontendUserInfo.appType = "adminApp";
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().addProgramUser;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	};

	giveFeedback(reqObj): any{

		let url=this.configService.getApiUrls().giveFeedback;
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				// return res.json();
				return Observable.throw(error.json().error || "Server error");
			});

	}

	getFeedback(reqObj): any{

		reqObj.programId = this.configService.getprogramInfo().programId;

		let url=this.configService.getApiUrls().getFeedback;
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map get feedbck", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				// return res.json();
				return Observable.throw(error.json().error || "Server error");
			});

	}

	getApprovedEngineerRequest(reqObj): any{

		reqObj.programId = this.configService.getprogramInfo().programId;
		reqObj.serviceType = "programSetup";
		reqObj.clientId = this.configService.getprogramInfo().clientId;

		let url=this.configService.getApiUrls().getApprovedEngineerRequest;
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("inside map get feedbck", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				// return res.json();
				return Observable.throw(error.json().error || "Server error");
			});

	}
}
