/*
	Author			:	Deepak Terse
	Description		: 	UI  which will be available in all the pages
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/
// declare var GoogleAnalytics:any;

import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";
import { ConfigService } from "./shared-services/config.service";
import { WindowRef } from './shared-components/WindowRef';
import { StringService } from "./shared-services/strings.service";
import { AppService } from "./app.service";
import { GoogleAnalyticsEventsService } from "./shared-services/google-analytics-events.service";
import { Observable, Subject } from "rxjs";

// import { Window } from 'selenium-webdriver';
//import {GoogleAnalytics} from "cordova-plugin-google-analytics/www'";
//import GoogleAnalytics from "../../node_modules/cordova-plugin-google-analytics";
//import {UniversalAnalyticsPlugin} from "../analytics.js";


// var GoogleAnalytics: any = require('GoogleAnalytics');

//declare var GoogleAnalytics: any;
declare var cordova:any;

// interface Window {
//     ga: any;
// }


declare var GoogleAnalytics:any;
declare var cordova:any;

declare var ga: any  ;
// eval("ga = (<any>window).ga")



@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	providers: [Device]
})
export class AppComponent implements OnInit {

	programRoleInfo: any;
	isRetailer: boolean = false;//for demo
	isLoggedIn: boolean = false;
	userName: string;
	userInfo: any; 
	deviceInfo: any;
	userLocationInfo: any;
	frontEndUserInfo: any;
	title: string = "Golden  Circle";
	public isProfileHover: boolean;
	public isLogoutHover: boolean;
	public roverElementColor: any;
	public clientColor: any;
	public isProgramTiers: boolean = false;
	pushRightClass: string = 'push-right';
	
	

	constructor(private router: Router,
		private configService: ConfigService,
		private stringService: StringService,
		private device: Device,
		private winRef: WindowRef,
		private appService: AppService,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		//private ga:GoogleAnalytics
	) {
		console.log("in APP COMPONENT!!!!");
		this.isProgramTiers = false;
	
		if (window.location.origin == "http://racoldroyals.goldencircle.in")
			router.navigate(['racold-login']);
		else router.navigate(["login"]);

		this.getIP();
		this.deviceInfo = this.device;


		if (configService.getLoggedInUserInfo() == null) {
			this.isLoggedIn = false;
		} else {
			this.isLoggedIn = true;
			this.userInfo = configService.getLoggedInUserInfo();
			if (this.userInfo.businessRole === "Retailer") {//for demo

				this.isRetailer = true
			} else {
				this.isRetailer = false
			}//for demo
			this.userName = this.userInfo.firstName;

		}

		this.clientColor = this.configService.getThemeColor();




	}

	ngOnInit() {

		ga = (<any>window).ga;
		var trackingId = "UA-120639172-2";
		//Called after the constructor, initializing input properties, and the first call to ngOnChanges.
		//Add 'implements OnInit' to the class.
		console.log("device",this.device);
		var deviceInfo = this.device
		document.addEventListener("deviceready", onDeviceReady, false);

		function onDeviceReady() {
			// Now safe to use device APIs
			console.log("onDeviceReady",onDeviceReady);
			//cordova.plugins.GoogleAnalytics.startTrackerWithId(trackingId , 30);  
			console.log("device",deviceInfo);
			if(deviceInfo.os === "android" || deviceInfo.os === "ios"){
				ga.startTrackerWithId(trackingId , 30);
				ga.trackView('Ajeet Yo');
				ga.trackEvent("Login", "logInSuccessssS", "swaraj");
			}
		}; 	
			//window.ga.	
	}

	showHideProgramTiers() {
		this.isProgramTiers = true;

	}

	mouseLeaveProfile() {
		console.log("on mouseOver LEAVE   ........................");
		this.isProfileHover = false;
		console.log('this.isAddClaimHover', this.isProfileHover);
	}
	mouseOverProfile() {
		this.isProfileHover = true;
		console.log('this.isProfileHover OVER ', this.isProfileHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		console.log('roverElementColor APPP', this.roverElementColor)
		let scolortToSet: any = null;

	}
	mouseLeaveLogout() {
		console.log("on mouseOver LEAVE   ........................");
		this.isLogoutHover = false;
		console.log('this.isAddClaimHover', this.isLogoutHover);
	}
	mouseOverLogout() {
		this.isLogoutHover = true;
		console.log('this.isLogoutHover OVER', this.isLogoutHover);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
		console.log('roverElementColor', this.roverElementColor)
		let scolortToSet: any = null;

	}


	onMyProfileClick() {
		let beInfo = this.configService.getloggedInBEInfo();
		if (beInfo != null) {
			this.router.navigate(['/home/be-profile']);
		} else {
			this.router.navigate(['/home/client-profile']);
		}



	}

	onLogoutClick() {
		var userInfo = this.userInfo;
		this.isLoggedIn = false;
		this.userName = null;
		this.userInfo = null;
		this.configService.setLoggedInUserInfo(null);
		this.configService.setloggedInBEInfo(null);
		this.configService.setLoggedInUserInfo(null);
		this.configService.setloggedInProgramUser(null);
		this.configService.setloggedInProgramInfo(null);
		this.configService.setClientUserInfo(null);
		localStorage.clear();

		if (window.location.origin == "http://racoldroyals.goldencircle.in") {

			this.router.navigate(['racold-login']);
		}
		else {
			this.router.navigate(["login"]);
		}

	}

	getLocation(ip): any {

		this.appService.getIpLocation(ip).subscribe(
			(responseObject) => {
				this.userLocationInfo = responseObject;
				this.frontEndUserInfo = {
					"ip": this.userLocationInfo.ip || " ",
					"browser": this.deviceInfo.browser || " ",
					"os": this.deviceInfo.os || " ",
					"os_version": this.deviceInfo.os_version || " ",
					"device": this.deviceInfo.device || " ",
					"city": this.userLocationInfo.city || " ",
					"country": this.userLocationInfo.country_name || " ",
					"country_code": this.userLocationInfo.country_code || " ",
					"latitude": this.userLocationInfo.latitude || " ",
					"longitude": this.userLocationInfo.longitude || " ",
					"region_code": this.userLocationInfo.region_code || " ",
					"region_name": this.userLocationInfo.region_name || " ",
					"time_zone": this.userLocationInfo.time_zone || " ",
					"pin_code": this.userLocationInfo.zip_code || " ",
					"appType": "web"
				}

				console.log("frontEndUserInfo", this.frontEndUserInfo);
				this.configService.setFrontEndUserInfo(this.frontEndUserInfo);
				this.appService.setFrontEndUserInfo(this.frontEndUserInfo);
				this.appService.frontEndInfo.next();

			},
			err => {
				this.frontEndUserInfo = this.userLocationInfo;
				this.frontEndUserInfo = {
					"ip": this.userLocationInfo.ip || " ",
					"browser": this.deviceInfo.browser || " ",
					"os": this.deviceInfo.os || " ",
					"os_version": this.deviceInfo.os_version || " ",
					"device": this.deviceInfo.device || " ",
					"appType": "web"
				}
				this.appService.setFrontEndUserInfo(this.frontEndUserInfo);
				this.configService.setFrontEndUserInfo(this.frontEndUserInfo);

			}
		);
	}

	getIP() {


		this.appService.getIpAdress().subscribe(
			(responseObject) => {
				this.userLocationInfo = responseObject;
				this.getLocation(responseObject.ip);
			},
			err => {
				this.frontEndUserInfo = {};
				this.appService.setFrontEndUserInfo(this.frontEndUserInfo);
				this.configService.setFrontEndUserInfo(this.frontEndUserInfo);
			}
		);



	}

	


}
