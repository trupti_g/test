/**
 * Created by Chetan on 2017-01-08.
 */
import { CanActivate, RouterStateSnapshot, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {ToastModule} from 'ng2-toastr';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
        return true;
    }
}
