/*
	Author			:	Pratik Gawand
	Description		: 	Setting Footer module
	Date Created	: 	29 April 2017
	Date Modified	: 	29 April 2017
*/

import { NgModule } from "@angular/core";
import {AppFooterComponent} from "./app-footer.component";
import { ClientFooterComponent } from "./client-footer.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {MaterialModule} from '@angular/material';


@NgModule({
	
	declarations: [AppFooterComponent,ClientFooterComponent],
	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule,MaterialModule.forRoot() ],
	exports : [AppFooterComponent,ClientFooterComponent],

	providers: []
	
})
export class FooterModule {

    
}