/*
	Author			:	Pratik Gawand
	Description		: 	Component for App Footer
	Date Created	: 	29 April 2017
	Date Modified	: 	29 April 2017
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";

@Component({
	selector: 'app-footer',
	templateUrl: './app-footer.component.html',
	styleUrls: ['./app-footer.component.css'],
	providers: []
})

export class AppFooterComponent implements OnInit {
	public applicationConfigArr: any;
	public footerMsg: any;
	public isRewardGallery: boolean = false;
	public isLogin: boolean = false;
	public programHelplineNo: any;
	public programInfo: any;
	public clientColor: any;

	constructor(
		private configService: ConfigService,
		private stringService: StringService,
	) {
		this.isLogin = this.configService.getLoginFlag();
		console.log("isLogin in FOOTER : ", this.isLogin);

		this.isRewardGallery = this.configService.getRewardGalleryFlag();
		console.log("isRewardGallery in FOOTER : ", this.isRewardGallery);

		this.programInfo = this.configService.getloggedInProgramInfo();
		if (this.programInfo !== undefined && this.programInfo !== null) {
			this.programHelplineNo = this.configService.getloggedInProgramInfo().programHelplineNo;
			console.log("programHelplineNo : ", this.programHelplineNo);
		}

		this.getApplicationConfig();
		this.clientColor = this.configService.getThemeColor();
	}

	ngOnInit() {

	}

	getApplicationConfig() {
		console.log("Get application config");
		this.configService.getApplicationConfig().subscribe(
			(responseObject) => {
				let responseCodes = this.configService.getStatusTokens();
				console.log("Get application config *******************************************", responseObject);

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_SUCCESS:
						this.applicationConfigArr = responseObject.result;
						console.log("Application config Array in response : ", this.applicationConfigArr);

						if (this.applicationConfigArr !== undefined && this.applicationConfigArr !== null) {
							this.footerMsg = this.applicationConfigArr.footerMessage;
						}

						console.log("Footer Message : ", this.footerMsg);
						break;
				}
			},
			err => {
				console.log("error");
			}
		);
	}

}