/*
	Author			:	Pratik Gawand
	Description		: 	Component for App Footer
	Date Created	: 	29 April 2017
	Date Modified	: 	29 April 2017
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { ConfigService } from "../shared-services/config.service";
import { StringService } from "../shared-services/strings.service";
import { HomeService } from "../home/home.service";
import { AppService } from "../app.service";

@Component({
	selector: 'client-footer',
	templateUrl: './client-footer.component.html',
	styleUrls: ['./client-footer.component.css'],
	providers: []
})

export class ClientFooterComponent implements OnInit {
	clientsArr: any;
	clientImageURL: any;
	public client: any = {};


	public programLogo: string = "";
	constructor(
		private configService: ConfigService,
		private stringService: StringService,
		private homeService: HomeService,
		private appService: AppService
	) {
		let programInfo = this.configService.getloggedInProgramInfo();
		this.programLogo = programInfo.programLogo[0];

	}

	ngOnInit() {
		this.appService.frontEndInfo.subscribe((value) => {
			var reqObj = {
				"frontendUserInfo": this.configService.getFrontEndUserInfo() !== undefined ? this.configService.getFrontEndUserInfo() : {},
				"clientId": this.configService.getloggedInProgramInfo().clientId
			}
			this.getAllClients(reqObj);
		})
	}
	getAllClients(requestObject) {
		console.log("response");
		this.homeService.getAllClient(requestObject)
			.subscribe((responseObject) => {
				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.clientsArr = responseObject.result;

						console.log("response", responseObject.result);
						console.log("clientsArr", this.clientsArr);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:

						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
				}
			},
				err => {

				}
			);
	}
}