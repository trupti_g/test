import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { StringService } from "../shared-services/strings.service";

@Injectable()
export class RewardsGalleryService {
  private headers;
  private options;
  private frontendUserInfo;
  public searchQuery: string;
  public searchCategory: string;
  private index: number = 0;
  programUserInfo: any;
  public allProducts = new Subject<any>();
  private product = new Subject<any>();
  public programInfo: any;
  public programId: string;
  public clientId: string;
  public loggedInUser: any;
  public userInfo: any;
  public brandIdArray: any[] = [];

  public userId: string = "";

  // private allProducts:any;

  constructor(
    private http: Http,
    private configService: ConfigService,
    private stringService: StringService
  ) {
    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });

    this.options = new RequestOptions({ headers: this.headers });
    if (this.configService.getloggedInProgramInfo() !== null) {
      this.programInfo = this.configService.getloggedInProgramInfo();
      this.clientId = this.configService.getloggedInProgramInfo().clientId;
      this.programId = this.configService.getloggedInProgramInfo().programId;
    }
    if (this.configService.getloggedInProgramUser() !== null) {
      this.userId = configService.getloggedInProgramUser().programUserId;
      this.loggedInUser = this.configService.getloggedInProgramUser();
    }

    this.frontendUserInfo = configService.getFrontEndUserInfo();
  }

  setSpecificProduct(product) {
    this.product = product;
  }
  getSpecificProduct(): Observable<any> {
    return this.product.asObservable();
  }

  setAllProductToService(products) {
    // this.allProducts.next({ "products" : products });
    this.allProducts.next(products);
    this.allProducts.subscribe(allProduct => {
      // this.allProducts = allProduct;
      console.log("all Product parent", allProduct);
    });
  }

  getAllProductsFromService(): Observable<any> {
    return this.allProducts.asObservable();
  }

  setProduct(pro) {
    this.product = pro;
  }

  getAllProducts(): any {
    console.log("calling get all products")
    var reqObj: any = {};
    let url = this.configService.getApiUrls().getAllRewardGalleryProducts;
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined) {
      reqObj.frontendUserInfo = frontEndInfo;
    } else {
      reqObj.frontendUserInfo = {
        userId: this.userId
      };
    }
    reqObj.clientId = this.clientId;
    reqObj.programId = this.programInfo.programId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "client";
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else  {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }
  
    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        console.log("inside map", res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log("inside catch");
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  getSpecificAllProducts(): any {
    var reqObj: any = {};
    let url = this.configService.getApiUrls()
      .getSpecificAllRewardGalleryProducts;
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined) {
      reqObj.frontendUserInfo = frontEndInfo;
    } else {
      reqObj.frontendUserInfo = {
        userId: this.userId
      };
    }
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "programSetup";
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    } {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    } 

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        console.log("inside map", res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log("inside catch");
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  getAllFilteredProducts(reqObj): any {
    let url = this.configService.getApiUrls().getAllRewardGalleryProducts;
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "client";

    if (frontEndInfo != null || frontEndInfo != undefined) {
      reqObj.frontendUserInfo = frontEndInfo;
    } else {
      reqObj.frontendUserInfo = {
        userId: this.userId
      };
    }
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else{
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
  
  return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => {
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  getSpecificAllFilterSlabs(reqObj): any {
    let url = this.configService.getApiUrls()
      .getSpecificAllRewardGalleryProducts;
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    console.log("info", frontEndInfo);
    if (frontEndInfo != null || frontEndInfo != undefined) {
      reqObj.frontendUserInfo = frontEndInfo;
    } else {
      reqObj.frontendUserInfo = {
        userId: this.userId
      };
    }
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "programSetup";
    console.log("getSpecificAllFilterSlabs request object : ", reqObj);
   
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        console.log("inside map", res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log("inside catch");
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  /************************* 	PROGRAM SPECIFIC REWARD GALLERY API CALLS 	************************** */
  //GET ALL PRODUCTS
  getSpecificRewardsGalleryProduct(reqObj): any {
    console.log("calling getSpecificRewardsGalleryProduct");
    let url = this.configService.getApiUrls().getSpecificRewardsGalleryProduct;

    // var reqObj: any = {},;
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;

    this.loggedInUser = this.configService.getloggedInProgramUser();

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });

    console.log(
      "this.configService.getAuthenticationToken() -------AJ",
      this.configService.getAuthenticationToken()
    );

    if (!this.configService.getAuthenticationToken()) {
      var token;
      if (JSON.parse(localStorage.getItem("userToken")))
        token = JSON.parse(localStorage.getItem("userToken")).token;

      this.headers = new Headers({
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      });
    }

    console.log(
      "getSpecificRewardsGalleryProduct---------------------AJ61",
      this.headers
    );

    if (this.loggedInUser.programRoleInfo !== null) {
      reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    }
    
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }
    reqObj.serviceType = "programSetup";

    //get frontenduserinfo
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined)
      reqObj.frontendUserInfo = frontEndInfo;
    else reqObj.frontendUserInfo = { userId: this.userId };

    this.options = new RequestOptions({ headers: this.headers });
    
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => {
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  //GET ALL CATEGORIES AND SUBCATEGORIES
  getSpecificCategoriesAndSubCategories(): any {
    let url = this.configService.getApiUrls()
      .getSpecificCategoriesAndSubCategories;

    var reqObj: any = {};
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    console.log("logged user Id ", this.loggedInUser.programRoleInfo.roleId);
    reqObj.serviceType = "programSetup";
    //permitted role
   
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else  {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }
    //get frontenduserinfo
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined)
      reqObj.frontendUserInfo = frontEndInfo;
    else reqObj.frontendUserInfo = { userId: this.userId };

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        console.log("inside map", res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log("inside catch");
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  //GET ALL SLABS
  getRewardsGallerySlabs(): any {
    let url = this.configService.getApiUrls().getRewardsGallerySlabs;

    var reqObj: any = {};
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "programSetup";

    //get frontenduserinfo
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined)
      reqObj.frontendUserInfo = frontEndInfo;
    else reqObj.frontendUserInfo = { userId: this.userId };
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else  {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        console.log("inside map", res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log("inside catch");
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  //GET ALL BRANDS
  getSpecificBrands(): any {
    let url = this.configService.getApiUrls().getSpecificBrands;

    var reqObj: any = {};
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "programSetup";

    //get frontenduserinfo
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined)
      reqObj.frontendUserInfo = frontEndInfo;
    else reqObj.frontendUserInfo = { userId: this.userId };
    
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else  {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => {
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  //GET ALL BRANDS
  getFilterBrandMaster(reqObj: any): any {
    let url = this.configService.getApiUrls().getBrandMaster;
    reqObj.programId = this.programId;
    reqObj.clientId = this.clientId;
    reqObj.permittedRoles = this.loggedInUser.programRoleInfo.roleId;
    reqObj.serviceType = "client";

    //get frontenduserinfo
    var frontEndInfo = this.configService.getFrontEndUserInfo();
    if (frontEndInfo != null || frontEndInfo != undefined)
      reqObj.frontendUserInfo = frontEndInfo;
    else reqObj.frontendUserInfo = { userId: this.userId };
   
    if(this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerProgramRole || this.loggedInUser.programRoleInfo.roleId === this.stringService.getStaticContents().jswEngineerAdminProgramRole){
      reqObj.permittedRoles = this.stringService.getStaticContents().jswEngineerProgramRole;
    }
    else {
      reqObj.permittedRoles = this.stringService.getStaticContents().jswDealerProgramRole;
  
    }

    this.headers = new Headers({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.configService.getAuthenticationToken()
    });
    this.options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(url, reqObj, this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => {
        return Observable.throw(
          error.json().error ||
            JSON.parse(error._body).message ||
            error.statusText ||
            "Some Error Occurred"
        );
      });
  }

  /******************** 	END OF PROGRAM SPECIFIC REWARD GALLERY API CALLS  **************************/

  /********************************* 	SET RECORDS FOR DETAILS PAGE 	*********************** */
  public categorySubCategoriesArray: any[] = [];
  public slabsArray: any[] = [];

  setCategorySubCategories(categorySubCategoriesArray) {
    this.categorySubCategoriesArray = categorySubCategoriesArray;
    console.log(
      "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&",
      this.categorySubCategoriesArray
    );
  }

  getCategorySubCategories() {
    return this.categorySubCategoriesArray;
  }

  setSlabs(slabsArray) {
    this.slabsArray = slabsArray;
    console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", this.slabsArray);
  }

  getSlabs() {
    return this.slabsArray;
  }
  /***************************** 	END OF SET RECORDS FOR DETAILS PAGE 	********************** */

  setBrandsArrayIndex(data) {
    this.brandIdArray = data;
    console.log("Data in set method on service page", this.brandIdArray);
  }
  getBrandsArrayMob(): any {
    console.log("data in get method on service page", this.brandIdArray);
    return this.brandIdArray;
  }
}
