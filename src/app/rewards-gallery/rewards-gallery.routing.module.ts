/*  Author         : Pratik Gawand
    Description    : Reward Gallery Routing module
                    
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RewardsGalleryHomeComponent } from "./rewards-gallery-home.component";

import { RewardsGalleryCategoriesComponent } from "./rewards-gallery-categories.component";

import { RewardsGalleryProductDetailsComponent } from "./rewards-gallery-product-details.component";


export const rewardGalleryRoutes: Routes = [
    {
        path: '',
        children: [


            //Rewards Gallery components
            {
                path: 'categories',
                component: RewardsGalleryCategoriesComponent
            },
            {
                path: 'categories:catId',
                component: RewardsGalleryCategoriesComponent
            },
            {
                path: 'product-details',
                component: RewardsGalleryProductDetailsComponent
            },


            //RewardGalleryHome components
            {
                path: '',
                redirectTo: 'categories',
                pathMatch: 'full'
            },
            {
                path: 'slabs',
                redirectTo: 'categories',
                pathMatch: 'full'
            }
        ]
    }
];



@NgModule({
    imports: [RouterModule.forChild(rewardGalleryRoutes)],
    exports: [RouterModule],

})
export class RewardsGalleryRoutingModule {

}
