/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RewardsGalleryHomeComponent } from "./rewards-gallery-home.component";
import { RewardsGalleryProductDetailsComponent } from "./rewards-gallery-product-details.component";
import { RewardsGalleryCategoriesComponent } from "./rewards-gallery-categories.component";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FooterModule } from '../footer/footer.module';

import { RewardsGalleryRoutingModule } from "./rewards-gallery.routing.module";
import { RewardsGalleryService } from "./rewards-gallery.service";
import { MaterialModule } from '@angular/material';
// import { KSSwiperModule } from 'angular2-swiper';
import { SharedModule } from "../shared-components/shared.module";
// import { SwiperModule } from 'angular2-useful-swiper'; //or for angular-cli the path will be ../../node_modules/angular2-useful-swiper 
// import { KSSwiperContainer, KSSwiperSlide, } from 'angular2-swiper';


@NgModule({
	declarations: [RewardsGalleryHomeComponent, RewardsGalleryCategoriesComponent, RewardsGalleryProductDetailsComponent],

	imports: [CommonModule,
		ReactiveFormsModule,
		RewardsGalleryRoutingModule,
		HttpModule,
		MaterialModule.forRoot(),
		// KSSwiperModule,
		// SwiperModule,
		FooterModule,
		FormsModule,
		SharedModule,
		// KSSwiperModule
	],

	exports: [],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA],

	providers: [RewardsGalleryService, RewardsGalleryCategoriesComponent]
})
export class RewardsGalleryModule {
}