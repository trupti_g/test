/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery
	Date Created	: 	03 April 2017
	Date Modified	: 	19 April 2017
*/

import { Component, OnInit, Input, OnDestroy, OnChanges, NgZone, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
import { RewardsGalleryService } from "./rewards-gallery.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Subscription } from 'rxjs/Subscription';
import { Pipe, PipeTransform } from '@angular/core';
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { StringService } from "../shared-services/strings.service";
import { HomeService } from "../home/home.service";
// import { FeedbackMessageComponent1 } from "./feedback-message1.component";


import { CartService } from "../shared-services/cart.service";

import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	selector: 'product-category',
	templateUrl: './rewards-gallery-categories.component.html',
	styleUrls: ['./rewards-gallery.component2.scss'],
	providers: [Device]
})

export class RewardsGalleryCategoriesComponent implements OnInit {
	public catcount: number = 0;
	private filterForm: FormGroup; //Filter  form group
	public showBrand: boolean = true;
	public isFilter: boolean = false;
	public isRewardGallery: boolean;
	public isMore: boolean;
	public noProductFound = false;
	public uncheckAll = false;
	private index: number = 0;
	public cartCount: number = 0;
	public category: string;
	public subscription: Subscription;
	public categoryName: string;
	public searchProduct: string;
	public brandArr: any = [];
	public productArr: any = [];
	private productQuantity: any = [];
	public isProductAdded: boolean[] = [];
	public checkGlobalRewardGallery: boolean;
	public isOptionClicked: boolean = true;
	public programInfo: any;
	public programId: string;
	public allProduct: any = [];
	public canAddToCart: boolean = true;
	public pointsSlabs: any = [];
	public requestObj: any = {

	};
	public allBrands: any;
	public allCategoryAndProduct: any;
	private showLoader: boolean = false;   //show loader
	public isSlab: boolean;
	public isCategory: boolean;
	public isSpecific: boolean;
	public isGlobalRewardGallery: boolean;
	public programUserInfo: any;
	public programRoleInfo: any;
	public isEligibleForRedemption: boolean;

	public statusMessage: string = "";


	public categorySubcategoryArray: any[] = [];
	public categorySubcategoryArray1: any[] = [];
	public categorySubcategoryArray2: any[] = [];
	public brandsArray: any[] = [];
	public slabsArray: any[] = [];
	public productsArray: any[] = [];


	public productCount: number;
	public filterText: string = "";
	public isSlabsRequired: boolean = false;
	public allowStarPointsConversion: boolean = false;

	/************ 	KEYS FO FILTERS 	********************************* */
	public brandIdArray: any[] = [];
	public categoryId: string = undefined;
	public slabId: string = undefined;
	public subCategoryId: string = undefined;
	public search: string = undefined;
	public slabValue: string = undefined;
	public sort: string = "";
	public programUser: any = {};
	public isBrandsToRefresh: boolean = true;
	public isdeleteBrand: boolean = false;
	public isCategoryHover: boolean;
	public roverElementColor: any;
	public clientColor: any;
	public selectedIndex: any;
	public selectedSubIndex: any;
	public selectedBrandIndex: any;
	public showBrandColor: boolean;



	/******************** 	END OF KEYS FOR FILTERS 	***************************** */

	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private productDetailsService: ProductDetailsService,
		private rewardsGalleryService: RewardsGalleryService,
		private formBuilder: FormBuilder,
		private zone: NgZone,
		private activatedRoute: ActivatedRoute,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private homeService: HomeService,
		public cartService: CartService,
		public sidebar: SidebarComponent,
		private stringService: StringService,
		private device: Device,
	) { }
	/**********************************		CODE STARTS 	*********************************************** */
	ngOnInit() {
		this.sidebar.close();
		// document.getElementById("open").style.display = "block";
		// document.getElementById("close").style.display = "none";
		this.isOptionClicked = true;

		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
			console.log("VALUE-----", value);
			if (value !== null && value !== undefined && value.length !== 0) {
				var productName = value;
				this.feedbackMessageComponent.updateMessage(true, productName + " added Successfully in cart", "alert-success");
			}
		});


		this.cartService.setFields("ProgramSpecificRG");

		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Reward Gallery Categories')
		} else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Reward Gallery Categories");
					ga('send', 'pageview');
				}
			});
		}
	
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.programUser = this.configService.getloggedInProgramUser();
		console.log("program info ---- ", this.programUser);
		// // eligibleForRedeemption Check
		this.programUserInfo = this.configService.getloggedInProgramUser();
		console.log("this.stringService.getStaticContents().jswASMAdminProgramRole", this.stringService.getStaticContents().jswASMAdminProgramRole);
		console.log(this.programUserInfo.roleId);
		if (this.programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole || this.programUserInfo.programRole
			=== this.stringService.getStaticContents().jswCMProgramRole || this.programUserInfo.programRole
			=== this.stringService.getStaticContents().jswRCMProgramRole || this.programUserInfo.programRole
			=== this.stringService.getStaticContents().jswDistributorProgramRole || this.programUserInfo.programRole
			=== this.stringService.getStaticContents().jswDistributorAdminProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswDealerAdminProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswEngineerAdminProgramRole ) {
			this.canAddToCart = false;
		}
		console.log(" categories - this.canAddToCart", this.canAddToCart);
		console.log("program user info ---- ", this.programUserInfo);
		// // check for useGlobalRewardGallery
		if (this.programInfo.useGlobalRewardGallery == false) {
			if (this.programInfo.allowStarPointsConversion) {
				this.isGlobalRewardGallery = true;
			} else {
				this.isGlobalRewardGallery = false;
			}
		}
		console.log("this.programUserInfo", this.programUserInfo);
		if (this.programUserInfo.programRoleInfo.eligibleForRedemption) this.isEligibleForRedemption = false;
		else this.isEligibleForRedemption = true;
		// // End of check

		this.clientColor = this.configService.getThemeColor();


		console.log('SPECFIC IS CALLED');
		this.statusMessage = "Fetching products...";
		// Close the Side Bar

		this.filterForm = this.formBuilder.group({
			"view": [""],
			"search": [],
			"category": ["All Categories"],
			"slabs": ["All Slabs"]
		});
		console.log("forming parameteres");
		this.activatedRoute.queryParams
			.subscribe(params => {
				var catId = params['catId'];
				var catName = params['catName'];
				var subCatId = params['subCatId'];
				var subCatName = params['subCatName'];
				var product = params['searchProduct'];
				var slab = params['slabId'];
				var slabValue = params['slabValue'];
				console.log("PARAMS catId", catId);
				console.log("PARAMS catName", catName);
				console.log("PARAMS subCatId", subCatId);
				console.log("PARAMS subCatName", subCatName);
				console.log("PARAMS product", product);
				console.log("PARAMS slab", slab);
				if (catId != undefined) {
					console.log("setting the categoryId", catId);
					this.categoryId = catId;
				}
				if (catName != undefined && subCatName == undefined) {
					this.filterText = catName;
				}
				if (subCatName != undefined) {
					this.filterText = subCatName;
				}
				if (subCatId != undefined) {
					this.subCategoryId = subCatId;
					console.log("setting the categoryId", this.subCategoryId);
				}
				if (slab !== undefined && slab !== "All Slabs") {
					this.slabValue = slab;
					this.filterText = slabValue;
					console.log("slabvalue is set to", this.slabValue);
				}
				console.log("product is to search was", product);
				if (product !== undefined) {
					this.search = product;
					this.filterForm.value.search = product;
					console.log("product is to search set to", this.search);
				}
			});

		this.getProductFormParams();
		console.log("exiting the getparam method");
		this.getSpecificBrands();
		this.isRewardGallery = true;
		this.configService.setRewardGalleryFlag(this.isRewardGallery);

		this.programInfo = this.configService.getloggedInProgramInfo();
		if (this.programInfo.isSlabsRequired) {
			this.getRewardsGallerySlabs();
			this.isSlabsRequired = true;
		}
		else {
			this.getSpecificCategoriesAndSubCategories();
			this.isSlabsRequired = false;
		}

		if (this.programInfo.allowStarPointsConversion) this.allowStarPointsConversion = true;
		else this.allowStarPointsConversion = false;
		this.productArr = this.productDetailsService.getProduct();
		console.log("exiting the initialization of form")
	}
	// ngAfterContentChecked()	
	// {
	// 	console.log("in after init fun rewards product page");
	//     this.sidebar.close();
	// }
	// ngAfterViewInit()	
	// {
	// 	this.sidebar.close();
	// }



	goToGlobalRG() {
		this.router.navigate(['./home/globalrewardsgallery/categories']);
	}
	/************************************* 	CODE ENDS 	******************************************** */

	addProductDetail(product, index) {
		this.feedbackMessageComponent.updateMessage(true, product.productName + " added Successfully in cart", "alert-success");
		for (let i = 0; i < this.productArr.length; i++) {
			this.isProductAdded[i] = false;
		}
		if (this.isGlobalRewardGallery) {
			product.type = "globalRewardGallery";
		} else {
			product.type = "programSpecificRewardGallery";
		}
		this.isProductAdded[index] = true;
		this.productArr = this.productDetailsService.getProduct();

		for (let i = 0; i < this.productArr.length; i++) {
			if (this.productArr[i].productId == product.productId) {
				this.productArr[i].quantity++;
				return;
			}
		}

		this.productArr.push(product);
		this.productArr[this.productArr.length - 1].quantity = 1;
		this.cartCount = this.productArr.length;

		this.productDetailsService.setProduct(this.productArr);
		this.productDetailsService.setCartCount(this.cartCount);
		this.cartCount = this.productDetailsService.getCartCount();
	}

	mouseLeaveCategory() {
		this.isCategoryHover = false;
	}
	mouseOverCategory() {
		this.isCategoryHover = true;
		this.roverElementColor = this.configService.getThemeColor();
	}

	mouseLeaveCategoryIndex() {
		this.selectedIndex = null;
	}
	mouseOverCategoryIndex(index) {
		this.selectedIndex = index;
		this.roverElementColor = this.configService.getThemeColor();
	}


	mouseLeaveSubCategoryIndex() {
		this.selectedSubIndex = null;
	}
	mouseOverSubCategoryIndex(index) {

		console.log('index', index);
		this.selectedSubIndex = index;
		console.log('index', this.selectedIndex);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}
	mouseLeaveBrandIndex() {
		this.selectedBrandIndex = null;
		this.showBrandColor = true;
	}
	mouseOverBrandIndex(index) {
		this.showBrandColor = false;
		console.log('index', index);
		this.selectedBrandIndex = index;
		console.log('index', this.selectedBrandIndex);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}

	toggleBrand() {
		this.showBrand = !this.showBrand;
	}

	goToProductDetail(product, element) {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery", "View details", product.productName);
		console.log("this----------------", element);
		this.configService.setRewardGalleryProduct(product);
		this.router.navigate(['./home/rewardsgallery/product-details']);
	}



	setProduct(products) {
		console.log(products);
		this.allProduct = products;
		console.log("Setted Product", products);
	}

	getProduct() {
		return this.allProduct;
	}

	getSpecificCategory(category, categoryName) {

		this.clearBrandFilter();

		let reqObj = {
			"categoryId": category
		}
		this.categoryName = categoryName
	}
	getSpecificCategoryAndSubCategory(category, subCategory, subCategoryName) {
		this.clearBrandFilter();
		let reqObj = {
			"categoryId": category,
			"subCategoryId": subCategory
		}
		this.categoryName = subCategoryName;
	}
	getAllProduct() {
		this.clearBrandFilter();
		this.categoryName = "All Categories";
	}

	getSearchProduct(searchProduct, catId) {
		this.clearBrandFilter();
		let reqObj = {
			"searchProduct": searchProduct,
			"categoryId": catId,
		}
		if (catId == "") {
			delete reqObj.categoryId;
		}

	}

	clearAll() {
		this.brandArr = [];
		var brand = this.allBrands;
		for (var i = 0; i < brand.length; i++) {
			brand[i].isSelected = false;
		}
	}
	//  For mobile Filter
	filterNav() {
		this.isFilter = true;
	}
	applyClick() {
		this.isFilter = false;
	}
	cancelNav() {
		console.log("filter");
		this.isFilter = false;
	}

	clearBrandFilter() {
		this.brandArr = [];

		var brand = this.allBrands;
		if (this.allBrands != undefined) {

			for (var i = 0; i < brand.length; i++) {
				brand[i].isSelected = false;
			}
		}
	}


	/*************************** 	GET PRODUCT FORM PARAMS 	******************************* */
	setSlabs(slabValue, slabText) {
		console.log("slabValue", slabValue, slabText);
		this.search = undefined;
		if (slabText == undefined) {
			slabText = "";
			this.filterText = "";
		} else {
		}
		this.filterText = slabText;
		this.slabValue = slabValue;
		this.isBrandsToRefresh = true;
		this.isdeleteBrand = true;
		this.getProductFormParams();
	}

	clearAllBrands() {
		this.brandIdArray = [];
		for (let i = 0; i < this.brandsArray.length; i++) {
			this.brandsArray[i].isSelected = false;
		}
		this.getProductFormParams();
	}
	optionClick() {
		this.isOptionClicked = false;
		document.getElementById("collapsibleNavbar").style.display = "block";
	}
	closeClick() {
		this.isOptionClicked = true;
		document.getElementById("collapsibleNavbar").style.display = "none";
	}

	setCategory(categoryId, categoryName) {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery", "viewCategory", categoryName);
		this.isOptionClicked = true;
		// document.getElementById("collapsibleNavbar").style.display = "none";
		console.log(categoryId);
		this.search = undefined;
		this.filterText = categoryName;
		console.log("setting the category search Name", this.search);
		if (categoryId !== "All Categories") this.categoryId = categoryId;
		this.subCategoryId = undefined;
		this.isBrandsToRefresh = true;
		if (categoryId === undefined) {

			this.categoryId = "All Categories"
			this.filterForm.patchValue({
				"category": "All Categories"
			})
		}
		else {
			// this.isdeleteBrand = false;
			this.filterForm.patchValue({
				"category": categoryId
			})
		}
		this.isdeleteBrand = true;
		this.getProductFormParams();
	}

	setSubCategory(categoryId, subCategoryId, subCategoryName) {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery", "viewSubCategory", subCategoryName);
		this.isOptionClicked = true;
		// document.getElementById("collapsibleNavbar").style.display = "none";
		this.search = undefined;
		this.filterText = subCategoryName;
		console.log("setting the category search Name", this.search, subCategoryName);
		this.filterText = subCategoryName;
		this.categoryId = categoryId;
		this.isBrandsToRefresh = true;
		this.isdeleteBrand = true;
		this.subCategoryId = subCategoryId;
		this.getProductFormParams();
	}
	func1(event: any)     //For mob divpropagation
	{
		console.log("event function gfhfjgkjjk")
		event.stopPropagation();
	}

	setBrandsArray(index) {
		console.log("set brands Array method");
		if (index == 'ALL') {
			this.brandIdArray = this.brandsArray;
		}
		else {
			this.brandsArray[index].isSelected = !this.brandsArray[index].isSelected;
			this.brandIdArray = [];
			for (let i = 0; i < this.brandsArray.length; i++) {
				if (this.brandsArray[i].isSelected) this.brandIdArray.push(this.brandsArray[i].brandId);
			}
		}
		this.isBrandsToRefresh = false;
		this.getProductFormParams();
		console.log("BRANDSSSSSSSSSSSSSSSSSSSSSSSSSSSSS---out of for loop", this.brandIdArray);
	}

	setSearch() {
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery", "viewCategory", this.filterForm.value.category);
		this.filterText = "";
		if (this.categoryId != "") this.categoryId = undefined;
		if (this.subCategoryId != "") this.subCategoryId = undefined;
		if (this.slabId != "") this.slabId = undefined;
		if (this.filterForm.value.category != "") this.categoryId = this.filterForm.value.category;
		if (this.filterForm.value.slabs != "") this.slabValue = this.filterForm.value.slabs;
		this.search = this.filterForm.value.search;
		this.isdeleteBrand = true;
		this.isBrandsToRefresh = true;
		//}
		this.getProductFormParams();

	}

	setSort() {
		if (this.sort === "name")
			this.sort = "points";
		else
			this.sort = "name"
		this.getProductFormParams();
	}

	getProductFormParams() {

		console.log("Inside getProductFormParams", this.brandIdArray);
		let reqObj: any = {};

		//CATEGORY FILTER
		if (this.categoryId && this.categoryId !== "All Categories") {
			console.log("inside all categories");
			reqObj.categoryId = this.categoryId;
			delete reqObj.brandIdArray;
			console.log("reqObj", reqObj);
			console.log("")
		}
		//SUBCATEGORY FILTER
		if (this.subCategoryId) reqObj.subCategoryId = this.subCategoryId;

		//BRAND FILTER
		if (this.brandIdArray.length > 0) reqObj.brandIdArray = this.brandIdArray;
		console.log("inside brand filter req object", this.brandIdArray)
		//SEARCH
		if (this.search) reqObj.searchProduct = this.search;

		//SLAB
		if (this.slabValue !== undefined && this.slabValue !== "All Slabs") reqObj.slabId = this.slabValue;
		console.log(reqObj, this.brandIdArray.length);
		console.log("reqObj", reqObj);

		if (this.isdeleteBrand) delete reqObj.brandIdArray;
		console.log("inside conditio req brandid array", reqObj.brandIdArray);
		console.log("Request object", reqObj);
		this.getSpecificRewardsGalleryProduct(reqObj);
	}
	/************************	END OF GET PRODUCT FORM PARAMS 	******************************* */
	keysrt(key) {
		return function (a, b) {
			if (a[key] > b[key]) return 1;
			if (a[key] < b[key]) return -1;
			return 0;
		}
	}



	/************************************** 	API CALLS 	 ************************************/
	//GET ALL PRODUCTS
	getSpecificRewardsGalleryProduct(reqObj) {
		console.log("INside getSpecificRewardsGalleryProduct", reqObj);
		this.statusMessage = "Fetching products...";
		this.showLoader = true;

		this.rewardsGalleryService.getSpecificRewardsGalleryProduct(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.noProductFound = false;
							if (this.sort == "name")
								this.productsArray = responseObject.result.sort(this.keysrt('pointsPerQuantity'));
							else
								this.productsArray = responseObject.result.sort(this.keysrt('productName'));
							;
							this.productCount = responseObject.count;
							console.log("this.productsArray : ", this.productsArray);
							/**
							 * added to access the product List of reward gallery
							 * 
							 * this.rewardsGalleryService.setAllProductToService(this.productsArray);
							 */
							console.log("this.isBrandsToRefresh this.isAllCategories", this.isBrandsToRefresh);
							if (this.isBrandsToRefresh === true) {
								console.log("this.isBrandsToRefresh = true");
								var brandIdsArr = [];
								if (this.productsArray !== undefined) {
									for (let i = 0; i < this.productsArray.length; i++) {
										brandIdsArr.push(this.productsArray[i].brandId);
									}
								}
								brandIdsArr.sort();

								var reqObj: any = {};
								reqObj.brandArray = brandIdsArr;
								this.getFilterBrandMaster(reqObj);
							}
							this.isBrandsToRefresh = false;
							this.isdeleteBrand = false;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.productsArray = [];
							this.noProductFound = true;
							this.statusMessage = "No product found";
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}


	//GET ALL CATEGORIES AND SUBCATEGORIES
	getSpecificCategoriesAndSubCategories() {
		this.showLoader = true;
		this.statusMessage = "Fetching products...";
		this.rewardsGalleryService.getSpecificCategoriesAndSubCategories().subscribe(
			(responseObject) => {
				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.categorySubcategoryArray = responseObject.result;

						console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$", this.categorySubcategoryArray[0].SubCategories.length)


						console.log("####################", this.categorySubcategoryArray)
						this.configService.setCategorySubCategories(this.categorySubcategoryArray);
						/**	for(let i=0;i< this.categorySubcategoryArray.length ;i++)
							{
								if(this.categorySubcategoryArray[i].SubCategories.length === 0
									|| this.categorySubcategoryArray[i].SubCategories.length === undefined)
									{
										this.categorySubcategoryArray.splice(i, 1);
									}
							}  */
						// this.configService.setCategorySubCategories(this.categorySubcategoryArray1);
						if (this.categorySubcategoryArray.length > 4) {
							this.isMore = true;
							for (let j = 0; j < 5; j++) {
								this.categorySubcategoryArray1.push(this.categorySubcategoryArray[j]);
							}
							for (let i = 5; i < this.categorySubcategoryArray.length; i++) {
								this.categorySubcategoryArray2.push(this.categorySubcategoryArray[i]);
							}
						} else {
							this.isMore = false;
							for (let i = 0; i < this.categorySubcategoryArray.length; i++) {
								this.categorySubcategoryArray1.push(this.categorySubcategoryArray[i]);
							}
						}


						console.log("this.categorySubcategoryArray1", this.categorySubcategoryArray1);

						console.log("this.categorySubcategoryArray : ", this.categorySubcategoryArray);
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.categorySubcategoryArray = [];
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_KYC_AWAITING:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
			err => {
				this.showLoader = false;
			}
		);
	}

	//GET ALL SLABS
	getRewardsGallerySlabs() {
		this.showLoader = true;
		this.statusMessage = "Fetching products...";
		this.rewardsGalleryService.getRewardsGallerySlabs()
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							let slabsArray: any[] = responseObject.result.pointsSlabs;
							this.slabsArray = slabsArray.sort(this.keysrt('slabValue'));
							this.configService.setSlabs(this.slabsArray);
							console.log("this.slabsArray : ", this.slabsArray);

							// this.slabsArray = responseObject.result.pointsSlabs;
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.slabsArray = [];
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	//GET ALL BRANDS
	getSpecificBrands() {
		this.showLoader = true;
		this.rewardsGalleryService.getSpecificBrands()
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.brandsArray = responseObject.result;
							console.log("this.brandsArray : ", this.brandsArray);
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.brandsArray = [];
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}


	getFilterBrandMaster(reqObj) {
		this.showLoader = true;
		this.statusMessage = "Fetching products...";
		this.rewardsGalleryService.getFilterBrandMaster(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							break;
						case responseCodes.RESP_SERVER_ERROR:
							break;
						case responseCodes.RESP_SUCCESS:
							this.brandsArray = responseObject.result;
							console.log("this.brandsArray : ", this.brandsArray);
							break;
						case responseCodes.RESP_AUTH_FAIL:
							break;
						case responseCodes.RESP_FAIL:
							this.brandsArray = [];
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							break;
						case responseCodes.RESP_KYC_AWAITING:
							break;
						case responseCodes.RESP_BLOCKED:
							break;
					}
				},
				err => {
					this.showLoader = false;
				}
			);
	}

	/********************************* 	END OF API CALLS 	 ************************************/


	/********************************* 	CART FUNCTIONALITY 	 ************************************/
	addToCart(product) {
		this.router
		{
			window.scrollTo(0, 0);
		}


		console.log("In addToCart Method", product);
		let cartArray: any[] = this.cartService.cartArray;

		console.log("In addToCart Method", cartArray);

		console.log("cartArray", cartArray);
		if (cartArray.length > 0) {
			for (let i = 0; i < cartArray.length; i++) {
				if (cartArray[i].productId == product.productId) {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, "Product " + product.productName + "already added to Cart", "alert-danger");
					break;
				}
				if (i == cartArray.length - 1) {
					this.googleAnalyticsEventsService.emitEvent("Cart", "Add to cart", product.productName);
					this.showLoader = true;
					this.cartService.addToCartFormReqObj(product.productId, product.productName);
				}
			}
		}
		else {
			this.showLoader = true;
			this.cartService.addToCartFormReqObj(product.productId, product.productName);
		}

	}
	/***************************** 	END OF CART FUNCTIONALITY 	 ********************************/
	// enter Key Search
	eventHandler(keyCode) {
		if (keyCode == 13) {
			console.log("KeyCode----------------------" + keyCode);
			this.setSearch();
		}
	}
}