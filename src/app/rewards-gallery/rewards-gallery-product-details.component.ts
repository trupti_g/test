/*
	Author			:	Pratik Gawand
	Description		: 	Component for Reward-Gallery Product Detail
	Date Created	: 	04 April 2017
	Date Modified	: 	04 April 2017
*/

import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { RewardsGalleryService } from "./rewards-gallery.service";
import { ProductDetailsService } from "../shared-services/product-details.service";
// import { KSSwiperContainer, KSSwiperSlide } from 'angular2-swiper';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { HomeService } from "../home/home.service";
import { CartService } from "../shared-services/cart.service";
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { StringService } from "../shared-services/strings.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
	templateUrl: './rewards-gallery-product-details.component.html',
	styleUrls: ['./rewards-gallery.component2.scss'],
	// providers: [KSSwiperContainer, KSSwiperSlide, Device]

})


export class RewardsGalleryProductDetailsComponent implements OnInit {
	imageURL: any;
	public isZoom: boolean = false;

	/**********************************	GET RECORD ARRAYS 	******************************* */
	public categorySubcategoriesArray: any[] = [];
	public categorySubcategoryArray1: any[] = [];
	public slabsArray: any[] = [];
	public slabValue: any;
	private filterText: any;
	public isOptionClicked: boolean = true;
	public isMore: boolean = false;


	slideTo: number;
	private showLoader: boolean = false;
	public isRewardGallery: boolean = false;
	private imageArr: string[] = ["/assets/images/reward-gallery-image-lg.png",
		"/assets/images/reward-gallery-image-lg.png",
		"/assets/images/reward-gallery-image-lg.png",
		"/assets/images/aboutus-racold.png"
	];

	public allProduct: any = [];
	public product: any;
	private productImages: any = [];

	brandIdArray: any = [];
	public allBrands: any;
	public allCategoryAndProduct: any;
	public parseInteger: any;

	private filterForm: FormGroup; //Filter  form group
	private highlight: string = "rg-hightlight";
	private intialIndex: number = 3;
	public programUserInfo: any;
	public programRoleInfo: any;
	public isEligibleForRedemption: boolean;

	config: Object = {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.next',
		prevButton: '.prev',
		spaceBetween: 50,
		intialSlide: 2
	};
	public isCategoryHover: boolean;
	public roverElementColor: any;
	public clientColor: any;
	public selectedIndex: any;
	public selectedSubIndex: any;
	public selectedBrandIndex: any;
	public showBrandColor: boolean;


	// @ViewChild(KSSwiperContainer) swiperContainer: KSSwiperContainer;
	// @ViewChild(KSSwiperContainer) swiper: KSSwiperSlide;
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;
	@ViewChild('carousel') private carousel: ElementRef;


	public programInfo: any = {};
	public programUser: any = {};
	public bEInfo: any = {};
	public isSlabsRequired: boolean = false;
	public canAddToCart: boolean = true;
	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		public cartService: CartService,

		private rewardsGalleryService: RewardsGalleryService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private productDetailsService: ProductDetailsService,
		private homeService: HomeService,
		public sidebar: SidebarComponent,
		private stringService: StringService,
		private device: Device,
	) {

	}

	ngOnInit() {
		this.sidebar.close();
		this.cartService.stopLoader.subscribe((value) => {
			this.showLoader = false;
			console.log("VALUE-----", value);
			if (value !== null && value.length !== 0) {
				var productName = value;
				this.feedbackMessageComponent.updateMessage(true, productName + " added Successfully in cart", "alert-success");
			}
		});

		this.programInfo = this.configService.getloggedInProgramInfo();
		this.programUser = this.configService.getloggedInProgramUser();
		this.bEInfo = this.configService.getloggedInBEInfo();
		if (this.programUser.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswCMProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswRCMProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswDistributorProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswDistributorAdminProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswDealerAdminProgramRole || this.programUser.programRole
			=== this.stringService.getStaticContents().jswEngineerAdminProgramRole) {
			this.canAddToCart = false;
		}

		if (this.programInfo.isSlabsRequired) {
			this.slabsArray = this.configService.getSlabs();
			this.isSlabsRequired = true;
		}
		else {
			this.categorySubcategoriesArray = this.configService.getCategorySubCategories();
			if (this.categorySubcategoriesArray.length > 4) {
				this.isMore = true;
				this.categorySubcategoryArray1 = this.categorySubcategoriesArray.splice(0, 5);
			} else {
				this.categorySubcategoryArray1 = this.categorySubcategoriesArray;
				this.isMore = false;
			}
			this.isSlabsRequired = false;
		}
		console.log(this.programInfo.isSlabsRequired, "******************************************************************************");
		console.log(this.categorySubcategoriesArray);
		console.log(this.slabsArray);

		this.isRewardGallery = true;
		this.configService.setRewardGalleryFlag(this.isRewardGallery);

		this.isEligibleForRedemption = false;
		// eligibleForRedeemption Check
		this.programUserInfo = this.configService.getloggedInProgramUser();
		console.log("ProgramUserInfo", this.programUserInfo);

		let programUser = this.programUserInfo.programRoleInfo;
		if (this.programUserInfo) {
			this.programRoleInfo = programUser;
			console.log("PROGRAM ROLE INFO : ", this.programRoleInfo);
			if (!this.programRoleInfo.eligibleForRedemption) {
				this.isEligibleForRedemption = true;
			}
		}
		// End 

		this.product = this.configService.getRewardGalleryProduct();
		this.product.shippingInfo = this.product.shippingInfo !== undefined ? this.product.shippingInfo : "Shipped in 40 - 45 business days";
		this.product.quantity = 1;
		this.parseInteger = parseInt;
		console.log("PRODUCT", this.product.image);
		var images = this.product.image;

		for (var i = 0; i < images.length; i++) {
			console.log("IMAGE", images[i]);
			if (images[i] != null) {
				this.productImages.push(images[i]);
			}
		}


		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView("Reward Gallery Product Details")
		} else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Reward Gallery Product Details");
					ga('send', 'pageview');
				}
			});
		}
	
		this.filterForm = this.formBuilder.group({
			"view": [""],
			"search": [""],
			"category": ["All Categories"],
			"slabs": ["All Slabs"]
		});


		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", "viewSpecificProduct", this.product.productName);
		this.getAllCategoryAndSubCategory();
		this.clientColor = this.configService.getThemeColor();

		// document.getElementById("open1").style.display = "block";
		// document.getElementById("close1").style.display = "none";
		this.isOptionClicked = true;
		// this.sidebar.close();

	}
	// ngAfterContentChecked()	
	// {
	//  	console.log("in after init fun rewards product page");
	// 	this.sidebar.close();
	// }
	// ngAfterViewInit()	
	// {
	// 	console.log("in view @@ init fun rewards product page");
	// 	this.sidebar.close();
	// }
	mouseLeaveCategory() {
		this.isCategoryHover = false;
	}
	mouseOverCategory() {
		this.isCategoryHover = true;
		this.roverElementColor = this.configService.getThemeColor();
	}

	mouseLeaveCategoryIndex() {
		this.selectedIndex = null;
		console.log('CALLLED');
	}
	mouseOverCategoryIndex(index) {

		console.log('index', index);
		this.selectedIndex = index;
		console.log('index', this.selectedIndex);
		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}

	optionClick() {
		this.isOptionClicked = false;
		document.getElementById("collapsibleNavbar").style.display = "block";
	}
	closeClick() {
		this.isOptionClicked = true;
		document.getElementById("collapsibleNavbar").style.display = "none";
	}

	mouseLeaveSubCategoryIndex() {
		this.selectedSubIndex = null;
	}
	mouseOverSubCategoryIndex(index) {

		console.log('index', index);
		this.selectedSubIndex = index;
		console.log('index', this.selectedSubIndex);

		console.log("on mouseOver   ........................");
		this.roverElementColor = this.configService.getThemeColor();
	}


	search() {
		if (this.isSlabsRequired == false) {
			console.log("Go To Category", this.filterForm.value.search, this.filterForm.value.category);

			if (this.filterForm.value.category != "") {
				this.router.navigate(['./home/rewardsgallery/categories'],
					{ queryParams: { searchProduct: this.filterForm.value.search, catId: this.filterForm.value.category } });
			} else {
				this.router.navigate(['./home/rewardsgallery/categories'],
					{ queryParams: { searchProduct: this.filterForm.value.search } });
			}
		} else if (this.isSlabsRequired == true) {
			console.log("Go To Slabs", this.filterForm.value.search, this.filterForm.value.slabs);
			this.router.navigate(['./home/rewardsgallery/categories'],
				{ queryParams: { searchProduct: this.filterForm.value.search, slabId: this.filterForm.value.slabs } });
		}
	}

	clearAllBrands() {

	}
	// Navigation of back button on mobile details page
	cancelNav() {
		console.log("filter");
		this.router.navigate(['../home/rewrdsgallerymob']);
	}

	// moveNext() {
	// 	console.log("asa", this.swiperContainer.swiper.realIndex);
	// 	if (this.swiperContainer.swiper.realIndex == this.intialIndex) {

	// 	}
	// 	this.swiperContainer.swiper.slideNext();
	// }

	// movePrev() {
	// 	console.log("asa", this.swiperContainer.swiper.realIndex);
	// 	this.swiperContainer.swiper.slidePrev();
	// }

	showImage(index) {
		this.slideTo = index;
		console.log("Index", index);
		// this.swiperContainer.swiper.slideTo(index);
		// (<any>$("#carouselExampleControls")).carousel(index);

	}

	goToSlabs(slabValue) {
		if (slabValue == undefined) {
			this.router.navigate(['./home/rewardsgallery/slabs'],
				{ queryParams: { slabId: this.product.slabId, slabValue: this.product.pointsPerQuantity } });
		} else {
			this.router.navigate(['./home/rewardsgallery/slabs'],
				{ queryParams: { slabId: this.product.slabId, slabValue: slabValue } });
		}

	}
	goBackToCategory() {
		this.router.navigate(['./home/rewardsgallery/categories'],
			{ queryParams: { catId: this.product.category.categoryId } });

	}
	setSlabfilter(slabId, slabValue) {
		if (slabId != undefined) {
			this.router.navigate(['./home/rewardsgallery/slabs'],
				{ queryParams: { slabId: slabId, slabValue: slabValue } });
		} else {
			this.router.navigate(['./home/rewardsgallery/slabs']);
		}

	}



	getSpecificCategory(category, catName) {
		this.isOptionClicked = false;
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", "viewCategory", catName);
		console.log("category", category);
		this.router.navigate(['./home/rewardsgallery/categories'],
			{ queryParams: { catId: category, catName: catName } });

	}
	getSpecificCategoryAndSubCategory(category, subCategory, subCatName) {
		this.isOptionClicked = false;
		this.googleAnalyticsEventsService.emitEvent("Reward Gallery Product Details", "viewSubCategory", subCatName);
		console.log("category", category);
		console.log("subCategory", subCategory);
		console.log("subNAME", subCatName);
		this.router.navigate(['./home/rewardsgallery/categories'],
			{ queryParams: { catId: category, subCatId: subCategory, subCatName: subCatName } });

	}

	/*
	   METHOD         : getAllCategoryAndSubCategory()
	   DESCRIPTION    : For Fetching All the Category and Sub Category in the Reward Gallery.
   */
	getAllCategoryAndSubCategory() {

	}

	//workaround for production
	addProductDetail2() {

	}

	addProductDetail() {
		this.feedbackMessageComponent.updateMessage(true, this.product.productName + " added Successfully in cart", "alert-success");
		setTimeout(this.feedbackMessageClose, 3000);
		this.product.type = "programSpecificRewardGallery";
		this.product.isPlaced = false;
		console.log("Product Details : ", this.product);
		let productArr: any = [];
		productArr = this.productDetailsService.getProduct();
		let found = false;
		for (let i = 0; i < productArr.length; i++) {
			if (productArr[i].productId == this.product.productId) {
				found = true;
				productArr[i].quantity = this.product.quantity;
			}
		}
		if (!found) {
			productArr.push(this.product);
			productArr[productArr.length - 1].quantity = this.product.quantity;
		}
		console.log("ProductArr in add product details", productArr);
		this.productDetailsService.setProduct(productArr);
	}

	feedbackMessageClose() {
		this.feedbackMessageComponent.updateMessage(false, '', '', '');
	}

	zoom(n) {

		if (this.isZoom === false) {
			this.isZoom = true;
			this.imageURL = this.productImages[n];
		}
	}

	closeDetails() {
		if (this.isZoom === true) {
			this.isZoom = false;
		}
	}

	/********************************* 	CART FUNCTIONALITY 	 ************************************/

	addToCart(product) {
		console.log("called", product);
		let cartArray: any[] = this.cartService.cartArray;

		this.googleAnalyticsEventsService.emitEvent("Cart", "Add to cart", product.productName);

		if (cartArray.length > 0) {


			for (let i = 0; i < cartArray.length; i++) {
				if (cartArray[i].productId == product.productId) {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, "Product " + product.productName + "already added to Cart", "alert-danger");
					break;
				}
				if (i == cartArray.length - 1) {
					this.showLoader = true;
					this.cartService.addToCartFormReqObj(product.productId, product.productName);
				}
			}
		}
		else {
			this.showLoader = true;
			this.cartService.addToCartFormReqObj(product.productId, product.productName);
		}
	}
	updateToCart() {

	}

	onQuantityClick(productId, quantity) {
		this.showLoader = true;

		this.cartService.updateToCartFormReqObj(productId, "Pending", quantity);


	}
	/***************************** 	END OF CART FUNCTIONALITY 	 ********************************/
	nav() {
		this.router.navigate(['../home/rewardsgallery']);
	}

}
