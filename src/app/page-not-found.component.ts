/**
 * Created by Chetan on 2017-01-08.
 */
import {Component} from "@angular/core";

@Component({
    template: '<h2>Page not found</h2>'
})
export class PageNotFoundComponent {}
