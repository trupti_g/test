/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { LoginService } from "./login.service";
import { StringService } from "../shared-services/strings.service";
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { ProgramSelection } from './programselection.component';
import { ClientProgramSelection } from './client-programselection.component';
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { ForgotPasswordComponent } from "../forgotPassword/forgot-password.component";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ErrorMessageComponent } from "./error-message.component";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss', '../app.component.scss'],
	providers: [Device]
})

export class LoginComponent implements OnInit {
	programUserInfo: any;

	mobilewidth: any;


	displaypc: boolean = true;
	displaymobile: boolean = false;
	forgotPassForm: FormGroup;
	showForgotPass: boolean;
	private loginForm: FormGroup; //Login  form group

	private showLoader: boolean = false;

	// Field for Username and Password
	private userName: string;
	private password: string;

	private feedBackMessage: string;

	private programsIdArr: string[] = [];
	private getProgramInfo: any;
	userInfo: any;
	programInfoArr: any;

	selectedOption: string;

	private singleProg: any;
	popUpInfo: any;
	beInfo: any;
	// Flag for checking the states of username and password fields
	private invalidUserName: boolean = false;
	private invalidPassword: boolean = false;
	private showFeedback: boolean = false;
	public loggedInUserInfo: any;

	public bEIndex: number;
	public programsIdsArray: any[];
	public showBusiness:boolean;
	public showProgram:boolean;
	public businessIndex:number;
	public businessEntityArr:any[];
	public noProgramMapped:boolean = false;


	public programUserId: string = "";
	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private loginService: LoginService,
		private formBuilder: FormBuilder,
		private stringService: StringService,
		public dialog: MdDialog,
		private device: Device,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService
	) {

		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}

		this.configService.setLoginFlag(false);
		this.configService.setRewardGalleryFlag(false);
		console.log("platform",this.device);
		console.log("this.device.device",this.device.device);
	
		this.userInfo = configService.getloggedInProgramUser();
		if (this.userInfo != null || this.userInfo != undefined) {
			this.router.navigate(['home']);
		} else {
			localStorage.clear();
			this.router.navigate(['login']);
		}

		this.loginForm = formBuilder.group({
			"userName": ["", Validators.required],
			"password": ["", Validators.required]
		});

		this.forgotPassForm = formBuilder.group({
			"forgotPass": [""]
		});
	}



	// Method for opening the dialog/ modal for Program Selection
	// openDialog() {

	// 	let config: MdDialogConfig = {
	// 		width: '500px',
	// 		height: '550px',


	// 	}


	// 	let dialogRef = this.dialog.open(ProgramSelection, config);
	// 	dialogRef.afterClosed().subscribe(result => {
	// 		if (result == undefined) {
	// 			this.router.navigate(['login']);
	// 			this.configService.setLoggedInUserInfo(null);
	// 			this.configService.setloggedInBEInfo(null);
	// 			this.programsIdArr = [];
	// 			localStorage.clear();
	// 		}

	// 	});
	// }
	openErroDialog() {
		let config: MdDialogConfig = {
			width: '40%',
			height: '70%',
		}

		let dialogRef = this.dialog.open(ErrorMessageComponent, config);
		dialogRef.afterClosed().subscribe(result => {
			if (result == undefined) {
				this.router.navigate(['login']);
				this.configService.setLoggedInUserInfo(null);
				this.configService.setloggedInBEInfo(null);
				this.programsIdArr = [];
				localStorage.clear();
			}

		});
	}

	scroll() {
		console.log("Inside scroll")
		this.router
		{
			window.scrollTo(0, 700);
		}
	}
	// Method for opening the dialog/ modal for Program Selection
	openClientDialog() {
		let config: MdDialogConfig = {
			width: '40%',
			height: '70%',
		}

		let dialogRef = this.dialog.open(ClientProgramSelection, config);
		dialogRef.afterClosed().subscribe(result => {
			if (result == undefined) {
				this.router.navigate(['login']);
				this.configService.setLoggedInUserInfo(null);
				this.configService.setloggedInBEInfo(null);
				localStorage.clear();
			}

		});
	}

	openForgotPasswordDialog() {
		console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
		this.googleAnalyticsEventsService.emitEvent("Login", "forgotPasswordClicked", this.loginForm.value.userId);
		let config: MdDialogConfig = {
			width: '550px',
			height: '250px',
		}

		let dialogRef = this.dialog.open(ForgotPasswordComponent, config);
		dialogRef.afterClosed().subscribe(result => {

		});
	}




	invalidCredential() {

		this.feedBackMessage = this.stringService.getErrorMessage().invalidCredentials;
		this.showFeedback = true;

	}

	userBlocked() {

		this.feedBackMessage = this.stringService.getErrorMessage().blocked;
		this.showFeedback = true;

	}

	kycAwaiting() {
		this.feedBackMessage = this.stringService.getErrorMessage().kycAwaiting;
		this.showFeedback = true;

	}

	showErrorMessage(message) {
		this.feedBackMessage = message;
		this.showFeedback = true;
	}




	// Method for the checking the input parameter and if both feilds are entered then calling the API for LOGIN
	checkInputs(): void {

		console.log("Inside check");
		this.showFeedback = false;
		if ((this.loginForm.value.userName != null && this.loginForm.value.userName != "") && (this.loginForm.value.password != null && this.loginForm.value.password != "")) {
			this.invalidPassword = false;
			this.invalidUserName = false;
			var userName = this.loginForm.value.userName;
			var password = this.loginForm.value.password;
			let requestObject = {
				userName: userName.trim(),
				password: password,
				appType: "channelApp"
			}
			this.login(requestObject);

		} else {
			if (!(this.loginForm.value.userName != null && this.loginForm.value.userName != "")) {
				this.invalidUserName = true;
			}
			if (!(this.loginForm.value.password != null && this.loginForm.value.password != "")) {
				this.invalidPassword = true;
			}

		}
	}


	// Method for unsetting the invalid / required message  on Username
	onChangeUserName() {
		this.invalidUserName = false;
	}


	// Method for unsetting the required message  on Password
	onChangeUserPassword() {
		this.invalidPassword = false;
	}


	// Method for navigating to Sign Up Page
	signUp() {
		console.log("this.stringService.getStaticContents().jswprogramId", this.stringService.getStaticContents().jswprogramId);
		this.googleAnalyticsEventsService.emitEvent("Registration", "registrationStarted", this.stringService.getStaticContents().jswprogramId);
		this.router.navigate(['../signup']);
	}


	getPrograms(programsIdsArray) {
		console.log("Program ID ARRAY In getPrograms", programsIdsArray);
		let requestObject = {
			"programIdArray": programsIdsArray,
			"isActive": true
		}
		this.getProgramsV1(requestObject);
	}

	getProgramsBE(programsIdsArray) {
		console.log("Program ID ARRAY in getProgramsBE", programsIdsArray);
		let requestObject = {
			"programIdArray": programsIdsArray,
			"isActive": true
		}

		this.popUpInfo = this.getProgramsBEV1(requestObject);


	}


	goToDashBoard() {
		var programInfo = this.configService.getloggedInProgramInfo();

		if (programInfo.length === 1) {

			this.configService.setloggedInProgramInfo(programInfo[0]);
			console.log("programInfo[0].dualAuthentication", programInfo[0].dualAuthentication);
			console.log("programInfo[0]", programInfo[0]);
			if (programInfo[0].dualAuthentication == true) {
				console.log("dualAuthentication : ", programInfo[0].dualAuthentication);
				this.openClientDialog();
			} else {
				console.log("dualAuthentication : ", programInfo[0].dualAuthentication);
				var programId = programInfo[0].programId;
				var clientId = programInfo[0].clientId;
				this.getProgramsUsers(programId, clientId);
			}
		} else {
			for (let i = 0; i < programInfo.length; i++) {
				console.log(programInfo[i]);
			}
			this.configService.setloggedInProgramInfo(programInfo);
			this.loginService.noProgramsUserMapped = false;
			this.openClientDialog();
		}



	}

	getProgramsUsers(programId, clientId) {
		let requestObject = {
			"programId": programId,
			"clientId": clientId,
			"programUserId": this.programUserId
		}
		console.log("inside getprograusers : ", requestObject);
		this.getProgramUserV1(requestObject, programId);
	}

	getProgramsBEUsers(programId, programUserId, clientId) {
		let requestObject = {
			"programId": programId,
			"clientId": clientId,
			"programUserId": programUserId
		}
		this.getProgramUserBEV1(requestObject, programId);
	}




	ngOnInit() {
		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Login')
		} else{
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Login");
					// ga('send', 'pageview');
				}
			});
		}
	}

	/***********************************    API CALLS   ******************************/
	/*
	   METHOD         : login()
	   DESCRIPTION    : For authentication of User Name and Password and make user log into the application.
   */
	login(requestObject) {
		this.showLoader = true;

		this.loginService.login(requestObject)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					let responseCodes = this.configService.getStatusTokens();
					switch (responseObject.statusCode){
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.showErrorMessage(responseObject.message);
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.showErrorMessage(responseObject.message);
							break;
						case responseCodes.RESP_SUCCESS:

							console.log("RESPONSEOBJRESULT--------------------", responseObject.result);
							console.log("MOBILENUMBER--------------------", responseObject.result.mobileNumber);
							if(this.device.device === "android" || this.device.device === "ios"){
								(<any>window).ga.setUserId(responseObject.result.mobileNumber)
							} else{
								ga('set', 'userId', responseObject.result.mobileNumber);
							}
							console.log("responseObject.result ",responseObject.result);
							if (responseObject.result.userId) {
								this.programUserId = responseObject.result.userId;
							}
							else {
								this.programUserId = responseObject.result.clientUserId;
							}
							console.log("responseObject.result.userId000000", responseObject.result.userId)
							this.configService.setLoggedInUserInfo(responseObject.result);
							console.log("Login Successful");
							// this.openDialog();
							this.googleAnalyticsEventsService.emitEvent("Login", "Success", this.loginForm.value.userName);
							console.log("responseObject.result.businessEntityInfo", responseObject.result.businessEntityInfo);
							console.log("responseObject.result.clientId", this.loggedInUserInfo);
						
							if (responseObject.result.businessEntityInfo) {
								console.log('in if  :',responseObject.result.businessEntityInfo.length);
								if (responseObject.result.businessEntityInfo.length === 1) {
									this.configService.setloggedInBEInfo(responseObject.result.businessEntityInfo[0]);
									this.beInfo = responseObject.result.businessEntityInfo[0];
									 console.log("responseObject.result.programRole   ",responseObject.result );
									// trupti
									// changes
										console.log("Mapped   ", this.beInfo.programsMapped.length);
										if (this.beInfo.programsMapped.length === 0) {
											console.log("Zeroo");
											this.feedBackMessage = "You are not enrolled into JSW Privilege Club."
											this.showFeedback = true
										// this.openDialog()
										} else {
											if (this.beInfo.programsMapped.length == 1) {
												console.log("Only One", this.beInfo.programsMapped[0],this.beInfo.programsMapped[0].programId, this.stringService.getStaticContents().jswprogramId);
												if(this.beInfo.programsMapped[0].programId != this.stringService.getStaticContents().jswprogramId){

													console.log("this.beInfo.programsMapped[0].programId   ",this.beInfo.programsMapped[0].programId);
													this.feedBackMessage = "You are not enrolled into JSW Privilege Club.";
													this.showFeedback = true;
												}
												else{
												this.programsIdArr.push(this.beInfo.programsMapped[0].programId);
												this.getProgramsBE(this.programsIdArr);
												}
											}
											else {
												console.log("More than One");
												console.log("Mapped   ", this.beInfo.programsMapped);
												let isJsw = false;
												let jswprogramId;
												for (let i = 0; i < this.beInfo.programsMapped.length; i++) {
													console.log("Program ID", this.beInfo.programsMapped[i]);
													console.log("String ProgramID", this.stringService.getStaticContents().jswprogramId);
														if (this.beInfo.programsMapped[i].programId === this.stringService.getStaticContents().jswprogramId) {
														isJsw = true;
														jswprogramId = this.beInfo.programsMapped[i].programId;
														console.log("JSW Flag1 ", isJsw);
													}
												// else {
												// 	this.programsIdArr.push(this.beInfo.programsMapped[i].programId);
												// 	console.log("JSW Flag2 ", isJsw);
												// }
												} //END FOR
											console.log("JSW Flag3", isJsw);
											if (isJsw) {
												this.programsIdArr = [];
												this.programsIdArr.push(jswprogramId);
												this.getProgramsBE(this.programsIdArr);
												console.log("In JSW", jswprogramId);

											} else {
												// this.openDialog();
												this.feedBackMessage = "You are not enrolled into JSW Privilege Club."
												this.showFeedback = true
												console.log("In Else");
											}
										}


									}

								
								} else {
									// this.openDialog();
									this.showBusiness = true;
									this.businessEntityArr = responseObject.result.businessEntityInfo;
									console.log("this.businessEntityArr",this.businessEntityArr);
								}



							}
							else if (responseObject.result.clientId) {

								var clientInfo = responseObject.result;
								if (clientInfo.programsMapped) {
									if (clientInfo.programsMapped.length == 0) {
										this.openClientDialog();

									} else if (clientInfo.programsMapped.length == 1) {
										console.log("Only One", clientInfo.programsMapped[0]);
										this.programsIdArr.push(clientInfo.programsMapped[0].programId);
										this.getPrograms(this.programsIdArr);

									}
									else {
										let isJsw = false;
										let jswprogramId;
										for (let i = 0; i < clientInfo.programsMapped.length; i++) {
											console.log("clientInfo.programsMapped[i]", clientInfo.programsMapped[i]);
											console.log("String ProgramID", this.stringService.getStaticContents().jswprogramId);
											if (clientInfo.programsMapped[i].programId === this.stringService.getStaticContents().jswprogramId) {
												isJsw = true;
												jswprogramId = clientInfo.programsMapped[i].programId;

												console.log("JSW Flag1 ", isJsw);
											} else {
											
												this.programsIdArr.push(clientInfo.programsMapped[i].programId);
												console.log("JSW Flag2 ", isJsw);

											}
										}

										console.log("JSW Flag3", isJsw);
										if (isJsw) {
											this.programsIdArr = [];
											this.programsIdArr.push(jswprogramId);
											this.getPrograms(this.programsIdArr);
											console.log("In JSW", jswprogramId);

										} else {
											console.log("In Else");
											this.feedBackMessage = "You are not enrolled into JSW Privilege Club.";
											this.showFeedback = true;
										}
									}

								}
								else {
									console.log("clientInfo.programsMapped : ", clientInfo.programsMapped);
									this.loginService.noProgramsUserMapped = true;
									this.openClientDialog();
								}
							}
							break;
						case responseCodes.RESP_AUTH_FAIL:
							console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
							this.googleAnalyticsEventsService.emitEvent("Login", "Failed", this.loginForm.value.userName);
							this.showErrorMessage(responseObject.message);

							break;
						case responseCodes.RESP_FAIL:
							console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
							this.googleAnalyticsEventsService.emitEvent("Login", "Failed", this.loginForm.value.userName);
							this.invalidCredential();
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
							this.googleAnalyticsEventsService.emitEvent("Login", "Failed", this.loginForm.value.userName);
							this.showErrorMessage(responseObject.message);
							break;
						case responseCodes.RESP_KYC_AWAITING:
							console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
							this.googleAnalyticsEventsService.emitEvent("Login", "Failed KYC awaiting", this.loginForm.value.userName);
							this.kycAwaiting();
							break;
						case responseCodes.RESP_BLOCKED:
							console.log(" this.loginForm.value.userId", this.loginForm.value.userId)
							this.googleAnalyticsEventsService.emitEvent("Login", "Blocked", this.loginForm.value.userName);
							this.userBlocked();
							break;
					}
				},
				err => {
					this.showLoader = false;

				}
			);
	}


	//To get program  array
	getProgramsV1(requestObject): any {

		console.log("Inside getProgramsV1");
		this.showLoader = true;

		let programs: string[] = [];
		this.configService.getPrograms(requestObject).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				if (responseObject.statusCode === responseCodes.RESP_FAIL) {

					console.log("Fails", responseCodes.RESP_FAIL);
				}

				if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {

					for (let i = 0; i < responseObject.result.length; i++) {
						programs[i] = responseObject.result[i];
					}
					this.configService.setloggedInProgramInfo(programs);
					this.goToDashBoard();
					return programs;
				}

			},
			err => {
				this.showLoader = false;

			}
		);


		return programs;

	}
	//To get program  array
	getProgramsBEV1(requestObject): any {
		this.showLoader = true;

		let programs: string[] = [];
		this.configService.getPrograms(requestObject).subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				if (responseObject.statusCode === responseCodes.RESP_FAIL) {
					this.openErroDialog();
					console.log("Fails", responseCodes.RESP_FAIL);
				}

				if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {

					for (let i = 0; i < responseObject.result.length; i++) {
						programs[i] = responseObject.result[i];
					}

					if (programs.length == 1) {
						console.log("posasa", programs[0]);

						this.singleProg = programs[0];
						console.log(this.singleProg.dualAuthentication);

						if (this.singleProg.dualAuthentication == false) {
							var reqClientId = this.singleProg.clientId;
							var reqProgramId = this.singleProg.programId;
							var reqProgramUserId = this.beInfo.businessId;


							this.getProgramsBEUsers(reqProgramId, reqProgramUserId, reqClientId);

						} else {
							console.log("in nner dual");
							// this.openDialog();
						}


						// if (programs ==)
					} else {
						console.log("in outer dual");
						// this.openDialog();
					}

				}

			},
			err => {
				this.showLoader = false;

			}
		);


		// return programs; 

	}

	/***********************************    API CALLS   ******************************/
	/*
		METHOD         : getProgramUser()
		DESCRIPTION    : For fetching the user mapped to the program
	*/
	getProgramUserV1(requestObject, programId) {
		this.showLoader = true;
		this.loggedInUserInfo = this.configService.getLoggedInUserInfo();
		this.loginService.getProgramUser(requestObject, programId, this.programUserId).subscribe((responseObject) => {
			this.showLoader = false;
			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
				case responseCodes.RESP_ROLLBACK_ERROR:
					break;
				case responseCodes.RESP_SERVER_ERROR:
					break;
				case responseCodes.RESP_SUCCESS:
					let responseObj = responseObject.result;
					console.log("Response Object getProgramUser : ", responseObj);
					console.log("loggedInUserInfo : ", this.loggedInUserInfo);
					if (this.loggedInUserInfo.clientId) {
						for (let i = 0; i < responseObj.length; i++) {
							if (this.loggedInUserInfo.clientUserId == responseObj[i].programUserId) {
								console.log("LOGGED IN PROGRAM USER INFO : ", responseObject.result[i]);
								this.configService.setloggedInProgramUser(responseObject.result[i]);
							}
						}
					}
					if (this.loggedInUserInfo.userId) {
						this.configService.setloggedInProgramUser(responseObject.result[0]);
					}
					this.router.navigate(['/home']);
					break;
				case responseCodes.RESP_AUTH_FAIL:
					break;
				case responseCodes.RESP_FAIL:
					this.loginService.noProgramsUserMapped = true;
					this.openClientDialog();
					break;
				case responseCodes.RESP_ALREADY_EXIST:
					break;
			}
		}, err => {
			this.showLoader = false;
		});
	}




	/***********************************    API CALLS   ******************************/
	/*
		METHOD         : getProgramUser()
		DESCRIPTION    : For fetching the user mapped to the program
	*/
	getProgramUserBEV1(requestObject, programId) {
		this.showLoader = true;
		this.loggedInUserInfo = this.configService.getLoggedInUserInfo();
		this.loginService.getProgramUser(requestObject, programId, this.programUserId).subscribe((responseObject) => {
			this.showLoader = false;
			let responseCodes = this.configService.getStatusTokens();
			switch (responseObject.statusCode) {
				case responseCodes.RESP_ROLLBACK_ERROR:
					break;
				case responseCodes.RESP_SERVER_ERROR:
					break;
				case responseCodes.RESP_SUCCESS:
					let responseObj = responseObject.result;
					console.log("Response Object getProgramUser : ", responseObj);
					console.log("loggedInUserInfo : ", this.loggedInUserInfo);
					this.configService.setloggedInProgramInfo(this.singleProg);
					if (this.loggedInUserInfo.clientId) {
						for (let i = 0; i < responseObj.length; i++) {
							if (this.loggedInUserInfo.clientUserId == responseObj[i].programUserId) {
								console.log("LOGGED IN PROGRAM USER INFO : ", responseObject.result[i]);
								this.configService.setloggedInProgramUser(responseObject.result[i]);
							}
						}
					}
					if (this.loggedInUserInfo.userId) {
						this.configService.setloggedInProgramUser(responseObject.result[0]);
						console.log("getProgramUserInfo :::: ", this.configService.getProgramUserInfo());
						console.log("responseObject.result[0]",responseObject.result[0]);
					//	let getProgramInfo = this.configService.getProgramUserInfo();
						console.log("getProgramInfo  :: ", responseObject.result[0].programRole, responseObject.result[0].profileId, this.stringService.getStaticContents().jswDistributorProgramRole, this.stringService.getStaticContents().jswDistrubutorProfileid );
					}
					console.log("responseObject.result");
					if( responseObject.result[0].programRole ===  this.stringService.getStaticContents().jswDistributorProgramRole &&
					responseObject.result[0].profileId === this.stringService.getStaticContents().jswDistrubutorProfileid){
						console.log("feedback message : no login allowed");
						this.feedBackMessage = "You are not enrolled into JSW Privilege Club";
						this.showFeedback = true;
					}
					else{
						this.router.navigate(['/home']);
					}
					console.log("getProgramUserInfo", this.configService.getProgramUserInfo());
					if (this.loggedInUserInfo.userId) {
						var dimensionValue = responseObject.result[0].roleName;
						console.log("dimensionValue", dimensionValue);
						if(this.device.device === "android" || this.device.device === "ios"){
							(<any>window).ga.addCustomDimension(1, dimensionValue,function(){
								console.log("success");
							},function(){
								console.log("error");
							});//1 instead of 'dimension1' for mobile
							(<any>window).ga.trackView('loggedIn')
						}else{
							ga('set', 'dimension1', dimensionValue);
							ga('send', 'pageview', 'loggedIn');
						}
					
					}
					if (this.loggedInUserInfo.userId) {
						var dimensionValue2 = this.loggedInUserInfo.userId;
						console.log("dimensionValue2", dimensionValue2);
						if(this.device.device === "android" || this.device.device === "ios"){
							(<any>window).ga.addCustomDimension(2, dimensionValue2) //2 instead of 'dimension2' for mobile
						}else{
							ga('set', 'dimension2', dimensionValue2);
						}
						
					}
					if (this.loggedInUserInfo.businessEntityInfo && this.loggedInUserInfo.businessEntityInfo[0].businessEntityUsers) {
						var dimensionValue3 = this.loggedInUserInfo.businessEntityInfo[0].businessEntityUsers[0].city;
						console.log("dimensionValue3", dimensionValue3);
						if(this.device.device === "android" || this.device.device === "ios"){
							(<any>window).ga.addCustomDimension(3 , dimensionValue3) //3 instead of 'dimension3' for mobile
						}else{
							ga('set', 'dimension3', dimensionValue3);
						}
					}
					if (responseObject.result[0].profileId !== null && responseObject.result[0].profileId !== undefined) {
						var dimensionValue4 = responseObject.result[0].profileId;
						console.log("dimensionValue4", dimensionValue4);
						if(this.device.device === "android" || this.device.device === "ios"){
							(<any>window).ga.addCustomDimension(4, dimensionValue4) //4 instead of 'dimension4' for mobile
						}else{
							ga('set', 'dimension4', dimensionValue4);
						}
					}
					if (this.loggedInUserInfo.businessEntityInfo && this.loggedInUserInfo.businessEntityInfo[0].businessEntityUsers) {
						var dimensionValue5 = this.loggedInUserInfo.businessEntityInfo[0].businessEntityUsers[0].state;
						console.log("dimensionValue5", dimensionValue5);
						if(this.device.device === "android" || this.device.device === "ios"){
							(<any>window).ga.addCustomDimension(5 , dimensionValue5) //5 instead of 'dimension5' for mobile
						}else{
							ga('set', 'dimension5', dimensionValue5);
						}
					}

					break;
				case responseCodes.RESP_AUTH_FAIL:
					break;
				case responseCodes.RESP_FAIL:
					this.loginService.noProgramsUserMapped = true;
					this.openClientDialog();
					break;
				case responseCodes.RESP_ALREADY_EXIST:
					break;
			}
		}, err => {
			this.showLoader = false;
		});
	}



	forgotPasswordClick() {
		this.showForgotPass = true
	}


	forgotPassword() {
		this.router.navigate(['../forgotPassword']);
	}

	close(){
		this.showBusiness=false;
		this.noProgramMapped=false;
	}

	// for handling multiple business entities
	openProgram(index) {
		console.log("inside openProgram",index);
        this.googleAnalyticsEventsService.emitEvent("Business Entity Selection", "selectBusinessEntity");
        this.bEIndex = index;

        this.configService.setloggedInBEInfo(this.businessEntityArr[index]);
		this.beInfo = this.businessEntityArr[index];
		console.log("this.beInfo",this.beInfo);

		console.log("Mapped   ", this.beInfo.programsMapped.length);
		// if not mapped to any program
		if (this.beInfo.programsMapped.length === 0) {
			console.log("Zeroo");
			this.feedBackMessage = "You are not enrolled into JSW Privilege Club."
			// this.showFeedback = true;
			this.noProgramMapped = true;
			this.showBusiness = false;
		}else{ 
			if (this.beInfo.programsMapped.length == 1) {
				console.log("Only One", this.beInfo.programsMapped[0],this.beInfo.programsMapped[0].programId, this.stringService.getStaticContents().jswprogramId);
				if(this.beInfo.programsMapped[0].programId != this.stringService.getStaticContents().jswprogramId){

					console.log("this.beInfo.programsMapped[0].programId   ",this.beInfo.programsMapped[0].programId);
					this.feedBackMessage = "You are not enrolled into JSW Privilege Club.";
					// this.showFeedback = true;
					this.noProgramMapped = true;
					this.showBusiness = false;
				}
				else{
					this.programsIdArr.push(this.beInfo.programsMapped[0].programId);
					this.getProgramsBE(this.programsIdArr);
				}
			}else {
				console.log("More than One");
				console.log("Mapped", this.beInfo.programsMapped);
				let isJsw = false;
				let jswprogramId;
				for (let i = 0; i < this.beInfo.programsMapped.length; i++) {
					console.log("Program ID", this.beInfo.programsMapped[i]);
					console.log("String ProgramID", this.stringService.getStaticContents().jswprogramId);
					if (this.beInfo.programsMapped[i].programId === this.stringService.getStaticContents().jswprogramId) {
						isJsw = true;
						jswprogramId = this.beInfo.programsMapped[i].programId;
						console.log("JSW Flag1 ", isJsw);
					}
				} //END FOR
				console.log("JSW Flag3", isJsw);
				if (isJsw) { //mapped to jsw
					this.programsIdArr = [];
					this.programsIdArr.push(jswprogramId);
					this.getProgramsBE(this.programsIdArr);
					console.log("In JSW", jswprogramId);

				} else { //not mapped to jsw
					this.feedBackMessage = "You are not enrolled into JSW Privilege Club."
					// this.showFeedback = true;
					this.noProgramMapped = true;
					this.showBusiness = false;
				}
			}
		}

    }


}
	/***********************************  END OF   API CALLS   ******************************/






