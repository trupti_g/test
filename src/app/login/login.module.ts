/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { LoginComponent } from "./login.component";

import { SignUpComponent } from "../signup/signup.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { LoginService } from './login.service';
import { ProgramSelection } from "./programselection.component";
import { ClientProgramSelection } from "./client-programselection.component";
import { ForgotPasswordComponent } from "../forgotPassword/forgot-password.component";
import { ErrorMessageComponent } from "./error-message.component";

import { AppFooterComponent } from '../footer/app-footer.component';

import { InfoMessageComponent } from "./info-message.component";

import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
import { FooterModule } from '../footer/footer.module';
import { JsonpModule } from '@angular/http';
// import { SwiperModule } from 'angular2-useful-swiper'; //or for angular-cli the path will be ../../node_modules/angular2-useful-swiper

@NgModule({

	declarations: [LoginComponent,
		SignUpComponent, ProgramSelection, ClientProgramSelection,
		ForgotPasswordComponent,
		ErrorMessageComponent,
		InfoMessageComponent

	],
	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule,  MaterialModule.forRoot(), FooterModule, JsonpModule],
	exports: [],

	providers: [LoginService, FeedbackMessageComponent, AppFooterComponent],
	entryComponents: [ProgramSelection, ClientProgramSelection, ErrorMessageComponent, InfoMessageComponent]
})
export class LoginModule {
}