/*
	Author			:	Pratik Gawand
	Description		: 	Component for Program Selection page
	Date Created	: 	14 March 2016
	Date Modified	: 	14 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { LoginService } from "./login.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";

@Component({
    templateUrl: './client-programselection.component.html',
    //selector: 'program-selection', styleUrls: ['./login.component.css'],
    providers: []
})



export class ClientProgramSelection implements OnInit {

    private otpForm: FormGroup;
    private noProgramsMapped: boolean = true;
    private noProgramsUserMapped: boolean = false;
    private programUserId: string = "";

    private showOtp: boolean = false;
    private mutipleProgram: boolean = false;
    private clientInfo: any = {};
    private userInfo: any = {};
    private showOtpFeedBack: boolean = false;
    private otpFeedback: string;
    private otpAttempts: number = 2;
    public otpSent: boolean = false;
    public otpSentFailed: boolean = false;
    public programUserArr: any;

    private reqClientId: string;
    private reqProgramId: string;
    public loggedInUserInfo: any;

    private showLoader = false;

    constructor(public dialogRef: MdDialogRef<ClientProgramSelection>,
        private configService: ConfigService,
        private loginService: LoginService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService
    ) {


        if (this.loginService.noProgramsUserMapped) {
            this.noProgramsMapped = false;
            this.noProgramsUserMapped = true;
        }
        else {

            this.userInfo = configService.getLoggedInUserInfo();
            if (configService.getloggedInProgramInfo()) {
                this.clientInfo = configService.getloggedInProgramInfo();
                if (this.clientInfo.length == undefined) {
                    this.noProgramsMapped = false;
                    this.showOtp = true;
                    this.sendOtp();
                }
                else {
                    this.noProgramsMapped = false;
                    this.mutipleProgram = true;
                }
            }

            this.initOtpForm();

        }



    }

    ngOnInit() {
        this.router.events.subscribe(event => {

            if (event instanceof NavigationEnd) {
                ga('set', 'page', "Login");
                ga('send', 'pageview');
            }
        });
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.

    }
    initOtpForm() {
        this.otpForm = this.formBuilder.group({
            "otp": ["", [Validators.required]]
        });
    }

    sendOtp() {

        var requestObject = {
            "mobile": this.userInfo.mobileNumber
        };
        this.sendOtpToMobile(requestObject);
    }
    resendOtp() {
        this.googleAnalyticsEventsService.emitEvent("Login", this.clientInfo.mobileNumber, "resendOTPCLient");
        var requestObject = {
            "mobile": this.userInfo.mobileNumber
        };
        this.sendOtpToMobile(requestObject);
        this.otpFeedback = "OTP was Resent";
        this.showOtpFeedBack = true;
    }

    verifyOtp() {
        this.googleAnalyticsEventsService.emitEvent("Login", this.clientInfo.mobileNumber, "verifyOTPCLient");
        var requestObject = {
            "mobile": this.userInfo.mobileNumber,
            "otp": this.otpForm.value.otp
        };
        this.showOtpFeedBack = false;
        this.verifyOtpOfMobile(requestObject);

    }
    goToLogin() {
        this.configService.setloggedInBEInfo(null);
        this.configService.setloggedInProgramUser(null);
        this.configService.setloggedInProgramInfo(null);
        this.configService.setLoggedInUserInfo(null);
        this.configService.setMyClaims(null);
        this.router.navigate(['/login']);
        this.dialogRef.close(ClientProgramSelection);
    }
    goToHomePage() {
        this.otpFeedback = "OTP Verifed Sucessfully";
        var clientInfo = this.configService.getloggedInProgramInfo()
        this.reqClientId = clientInfo.clientId;
        this.reqProgramId = clientInfo.programId;
        this.programUserId = this.configService.getLoggedInUserInfo().clientUserId;
        this.getProgramsUsers(this.reqProgramId, this.programUserId, this.reqClientId);

    }
    getProgramsUsers(programId, programUserId, clientId) {
        let requestObject = {
            "programId": programId,
            "clientId": clientId,
            "programUserId": programUserId,
            "userId": programUserId

        }
        this.getProgramUserV1(requestObject, programId);
    }

    closeDialog() {
        this.dialogRef.close(ClientProgramSelection);
        localStorage.clear();
    }

    openOTP(index) {
        var program: any = this.clientInfo[index];
        console.log("index", index);
        console.log("this.clientInfo[index]", this.clientInfo[index]);
        this.reqClientId = program.clientId;
        this.reqProgramId = program.programId;

        this.googleAnalyticsEventsService.emitEvent("Login", program.programId, "selectProgramClient");
        if (program.dualAuthentication == true) {
            this.sendOtp();
            this.showOtp = true;
            this.mutipleProgram = false;


        } else {
            this.configService.setloggedInProgramInfo(program);
            this.goToHomePage();

        }


    }


    /***********************************    API CALLS   ******************************/
    /*
        METHOD         : getProgramUser()
        DESCRIPTION    : For fetching the user mapped to the program
    */
    getProgramUserV1(requestObject, programId) {
        this.showLoader = true;
        this.loggedInUserInfo = this.configService.getLoggedInUserInfo();
        console.log('loggedInUserInfo------->', this.loggedInUserInfo);
        this.loginService.getProgramUser(requestObject, programId, this.programUserId).subscribe((responseObject) => {
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:

                    break;
                case responseCodes.RESP_SERVER_ERROR:

                    break;
                case responseCodes.RESP_SUCCESS:
                    this.programUserArr = responseObject.result;
                    if (this.loggedInUserInfo.clientId) {
                        for (let i = 0; i < this.programUserArr.length; i++) {
                            console.log("program user Id:", this.programUserArr[i].programUserId);
                            console.log("client id", this.loggedInUserInfo.clientUserId);
                            if (this.loggedInUserInfo.clientUserId == this.programUserArr[i].programUserId) {
                                console.log("LOGGED IN PROGRAM USER INFO : ", responseObject.result[i]);
                                this.configService.setloggedInProgramUser(responseObject.result[i]);
                            }
                        }
                    }
                    if (this.loggedInUserInfo.userId) {
                        this.configService.setloggedInProgramUser(responseObject.result[0]);
                    }
                    this.router.navigate(['/home']);
                    this.dialogRef.close(ClientProgramSelection);

                    break;
                case responseCodes.RESP_AUTH_FAIL:


                    break;
                case responseCodes.RESP_FAIL:
                    this.showOtp = false;
                    this.noProgramsUserMapped = true;

                    break;
                case responseCodes.RESP_ALREADY_EXIST:

                    break;
                case responseCodes.RESP_KYC_AWAITING:

                    break;
                case responseCodes.RESP_BLOCKED:

                    break;

            }
        }, err => {
            this.showLoader = false;

        });
    }

    sendOtpToMobile(requestObject) {
        this.showLoader = true;


        this.loginService.sendOTP(requestObject).subscribe((responseObject) => {
            this.showLoader = false;
            var res = responseObject._body;

            if (res.includes("ERR_MOBILE")) {
                this.otpSentFailed = true;
                this.otpSent = false;
            } else {
                this.otpSent = true;
                this.otpSentFailed = false;
            }

        }, err => {
            this.showLoader = false;

        });
    }

    verifyOtpOfMobile(requestObject) {
        this.showLoader = true;
        this
            .loginService
            .verifyOTP(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this
                    .configService
                    .getStatusTokens();
                var result = responseObject._body;

                result = result.trim();
                if (result === "OTP VERIFIED") {
                    this.goToHomePage();

                } else if (result === "OTP FAILED") {
                    this.showOtpFeedBack = true;
                    if (this.otpAttempts > 0) {
                        this.otpFeedback = "OTP Verification Failed " + this.otpAttempts + " Attempts Left!";
                        this.otpAttempts--;
                    } else {
                        this.goToLogin();

                    }

                }

            }, err => {
                this.showLoader = false;

            });
    }

}