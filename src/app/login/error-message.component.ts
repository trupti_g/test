/*
	Author			:	Pratik Gawand
	Description		: 	Component for Forgot Password Selection page
	Date Created	: 	19 April 2017
	Date Modified	: 	19 April 2017
*/

import {Component, OnInit, Input} from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {StringService} from "../shared-services/strings.service";
import {Router, ActivatedRoute, NavigationEnd} from "@angular/router";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {ConfigService} from "../shared-services/config.service";
import {LoginService} from "./login.service";
import {GoogleAnalyticsEventsService} from "../shared-services/google-analytics-events.service";

@Component({templateUrl: './error-message.component.html',  styleUrls: ['./login.component.css'], providers: []})

export class ErrorMessageComponent implements OnInit {

    private forgotPasswordForm : FormGroup;
    private showLoader : boolean = false;
    private showSuccess : boolean = false;
    private invalidUserName : boolean = false;
    private message : string;

    constructor(public dialogRef : MdDialogRef < ErrorMessageComponent >, private configService : ConfigService, private loginService : LoginService, private formBuilder : FormBuilder, private route : ActivatedRoute, private router : Router, private stringService : StringService, public googleAnalyticsEventsService : GoogleAnalyticsEventsService) {

     
        
    }

    ngOnInit() {

    }
    

   

    closeDialog() {
        this
            .dialogRef
            .close(ErrorMessageComponent);
        localStorage.clear();
    }
 

}