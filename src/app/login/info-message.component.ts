/*
	Author			:	Pratik Gawand
	Description		: 	Component for Showing Important Information 
	Date Created	: 	3rd August 2017
	Date Modified	: 	3rd August 2017
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { LoginService } from "./login.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";

@Component({
    templateUrl: './info-message.component.html',
    //styleUrls: ['./login.component.css'],
    providers: []
})



export class InfoMessageComponent implements OnInit {

    private forgotPasswordForm: FormGroup;
    private showLoader: boolean = false;
    private showSuccess: boolean = false;
    private invalidUserName: boolean = false;
    private message: string;

    constructor(public dialogRef: MdDialogRef<InfoMessageComponent>,
        private configService: ConfigService,
        private loginService: LoginService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService
    ) {



    }

    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.

    }




    closeDialog() {
        this.dialogRef.close(InfoMessageComponent);
    }






}