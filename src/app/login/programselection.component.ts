/*
	Author			:	Pratik Gawand
	Description		: 	Component for Program Selection page
	Date Created	: 	14 March 2016
	Date Modified	: 	14 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { LoginService } from "./login.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './programselection.component.html',
    selector: 'program-selection',
    //styleUrls: ['./login.component.css'],
    providers: [Device]
})

/*
	Class  for Dialog/Modal  Program Selection
	*/
export class ProgramSelection implements OnInit {

    private otpForm: FormGroup;

    private userInfo: any = {};
    private beInfo: any = {};
    private programInfo: any = {};

    public programUserId: any = "";

    private showBusiness: boolean = true;
    private showProgram: boolean = false;
    private showOtp: boolean = false;
    private showOnlyProgram: boolean = true;


    private showProgramNotFound: boolean = false;
    private noProgramMapped: boolean = false;


    private showOtpFeedBack: boolean = false;
    private otpFeedback: string;
    private otpAttempts: number = 2;
    public otpSent: boolean = false;
    public otpSentFailed: boolean = false;

    private reqClientId: string;
    private reqProgramId: string;
    private reqProgramUserId: string;

    private bEIndex: number = 0;
    private businessIndex: number;

    private programsArray: string[] = [];
    private programsIdsArray: string[] = [];

    private showLoader = false;
    public loggedInUserInfo: any;

    constructor(public dialogRef: MdDialogRef<ProgramSelection>,
        private configService: ConfigService,
        private loginService: LoginService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {

     

        if (configService.getloggedInProgramInfo()) {
            configService.setloggedInProgramInfo(null);
            configService.setLoggedInUserInfo(null);
            configService.setloggedInBEInfo(null);
            localStorage.clear();
            this.showBusiness = true;
            this.showProgram = false;
            this.showOtp = false;
            this.showOnlyProgram = true;

        } else {

            if (configService.getloggedInBEInfo()) {
                this.beInfo = configService.getloggedInBEInfo();

                if (this.beInfo.programsMapped.length != 0) {
                    for (var i = 0; i < this.beInfo.programsMapped.length; i++) {
                        this.programsIdsArray.push(this.beInfo.programsMapped[i].programId);
                    }

                    this.getPrograms(this.programsIdsArray);
                }
            }

            if (configService.getloggedInBEInfo()) {
                console.log("this.beInfo.programsMapped", this.beInfo.programsMapped);
                if (this.beInfo.programsMapped.length === 0) {
                    console.log('HERE in no program Mapped');
                    this.noProgramMapped = true;
                }

            }
            this.userInfo = configService.getLoggedInUserInfo();
            console.log("User info in BE", this.userInfo);

            this.initOtpForm();

        }

    }

    initOtpForm() {
        this.otpForm = this
            .formBuilder
            .group({
                "otp": [
                    "",
                    [Validators.required]
                ]
            });
    }

    getPrograms(programsIdsArray) {

        let requestObject = {

            "programIdArray": programsIdsArray,
            "isActive": true
        }

        this.programsArray = this.getProgramsV1(requestObject);

    }

    getProgramsUsers(programId, programUserId, clientId) {
        this.programUserId = programUserId;
        let requestObject = {
            "programId": programId,
            "programUserId": programUserId,
            "clientId": clientId,
            // "event": "login",
            "userId": programUserId
        }

        this.getProgramUserV1(requestObject, programId);
    }

    openProgram(index) {
        this.googleAnalyticsEventsService.emitEvent("Business Entity Selection", "selectBusinessEntity");
        this.bEIndex = index;

        this
            .configService
            .setloggedInBEInfo(this.userInfo.businessEntityInfo[index]);
        this.beInfo = this.userInfo.businessEntityInfo[index];

        this.googleAnalyticsEventsService.emitEvent("Business Entity Selection", this.userInfo.businessEntityInfo[index].businessName, "selectBusinessEntity");
        this.programsIdsArray = [];
        for (var i = 0; i < this.beInfo.programsMapped.length; i++) {
            this.programsIdsArray.push(this.beInfo.programsMapped[i].programId);
        }
        console.log("THE PROGRAM ARRAY", this.programsIdsArray);
        if (this.programsIdsArray.length != 0) {

            this.getPrograms(this.programsIdsArray);
        }


        this.showBusiness = false;
        this.showProgram = true;
        this.businessIndex = index;

    }
    openOTP(index) {


        this.googleAnalyticsEventsService.emitEvent("Business Entity Selection", this.userInfo.businessEntityInfo[this.bEIndex].businessName, "selectProgram");

        var program: any = this.programsArray[index];
        this.programInfo = this.programsArray[index];
        var beInfo: any = this
            .configService
            .getloggedInBEInfo();

        // AFTER OTP SUCESS
        this.reqClientId = program.clientId;
        this.reqProgramId = program.programId;
        this.reqProgramUserId = beInfo.businessId;

        if (program.dualAuthentication == true) {

            this.sendOtp();
            this.showOtp = true;
            this.showBusiness = false;
            this.showProgram = false;
            this.showOnlyProgram = false;

        } else {

            this.configService.setloggedInProgramInfo(this.programInfo);
            this.googleAnalyticsEventsService.emitEvent("Program Selection", this.programInfo.programName, "selectProgram");

            this.showBusiness = false;
            this.showProgram = false;
            this.showOnlyProgram = true;
            this.showLoader = true;
            this.goToHomePage();

        }
    }

    goToHomePage() {
        this.otpFeedback = "OTP Verifed Sucessfully"
        this.configService.setloggedInProgramInfo(this.programInfo);
        this.getProgramsUsers(this.reqProgramId, this.reqProgramUserId, this.reqClientId);
    }
    goToLogin() {
        this.configService.setloggedInBEInfo(null);
        this.configService.setloggedInProgramUser(null);
        this.configService.setloggedInProgramInfo(null);
        this.configService.setLoggedInUserInfo(null);
        this.configService.setMyClaims(null);
        this.router.navigate(['/login']);
        this.dialogRef.close(ProgramSelection);
    }
    closeDialog() {
        this.dialogRef.close(ProgramSelection);
        localStorage.clear();
    }

    verifyOtp() {
        this
            .googleAnalyticsEventsService
            .emitEvent("Program Selection", this.userInfo.mobileNumber, "dualAuthenticationResendOTP");
        var requestObject = {
            "mobile": this.userInfo.mobileNumber,
            "otp": this.otpForm.value.otp
        };
        this.showOtpFeedBack = false;
        this.verifyOtpOfMobile(requestObject);

    }
    sendOtp() {
        this.googleAnalyticsEventsService.emitEvent("Program Selection", this.userInfo.mobileNumber, "dualAuthentication");
        var requestObject = {
            "mobile": this.userInfo.mobileNumber
        };
        this.sendOtpToMobile(requestObject);
    }

    resendOtp() {
        this.googleAnalyticsEventsService.emitEvent("Program Selection", this.userInfo.mobileNumber, "dualAuthenticationVerified");
        var requestObject = {
            "mobile": this.userInfo.mobileNumber
        };
        this.sendOtpToMobile(requestObject);
        this.otpFeedback = "OTP was Resent";
        this.showOtpFeedBack = true;
    }

    ngOnInit() {
        if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Program Selection')
		} else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Program Selection");
                    ga('send', 'pageview');
    
                }
            });
        }
    }



    /***********************************    API CALLS   ******************************/
    /*
        METHOD         : getProgramUser()
        DESCRIPTION    : For fetching the user mapped to the program
    */
    getProgramUserV1(requestObject, programId) {
        this.showLoader = true;
        this.loggedInUserInfo = this.configService.getLoggedInUserInfo();
        console.log('loggedInUserInfo', this.loggedInUserInfo);

        this.loginService.getProgramUser(requestObject, programId, this.programUserId).subscribe((responseObject) => {
            this.showLoader = false;
            let responseCodes = this.configService.getStatusTokens();

            switch (responseObject.statusCode) {
                case responseCodes.RESP_ROLLBACK_ERROR:
                    break;
                case responseCodes.RESP_SERVER_ERROR:
                    break;
                case responseCodes.RESP_SUCCESS:
                    let responseObj = responseObject.result;
                    this.dialogRef.close(ProgramSelection);
                    // this.configService.setloggedInProgramUser(responseObject.result[0]);
                    if (this.loggedInUserInfo.clientId) {
                        for (let i = 0; i < responseObj.length; i++) {
                            if (this.loggedInUserInfo.clientUserId == responseObj[i].programUserId) {
                                console.log("LOGGED IN PROGRAM USER INFO : ", responseObject.result[i]);
                                this.configService.setloggedInProgramUser(responseObject.result[i]);
                                this.configService.setPoints(responseObject.result[i].totalPointsEarned);
                            }
                        }
                    }
                    if (this.loggedInUserInfo.userId) {

                        this.configService.setloggedInProgramUser(responseObject.result[0]);
                    }
                    this.router.navigate(['/home']);
                    break;
                case responseCodes.RESP_AUTH_FAIL:
                    break;
                case responseCodes.RESP_FAIL:
                    this.showOtp = false;
                    this.showProgramNotFound = true;
                    console.log('IN program user not found');
                    localStorage.clear();
                    break;
                case responseCodes.RESP_ALREADY_EXIST:
                    break;
                case responseCodes.RESP_KYC_AWAITING:
                    break;
                case responseCodes.RESP_BLOCKED:
                    break;

            }
        }, err => {
            this.showLoader = false;
        });
    }

    //To get program  array
    getProgramsV1(requestObject): string[] {
        this.showLoader = true;

        let programs: string[] = [];
        this
            .configService
            .getPrograms(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;

                let responseCodes = this
                    .configService
                    .getStatusTokens();

                if (responseObject.statusCode === responseCodes.RESP_FAIL) {

                    this.showProgramNotFound = true;
                }

                if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {

                    for (let i = 0; i < responseObject.result.length; i++) {
                        programs[i] = responseObject.result[i];
                    }
                    console.log("Programs", programs);
                    return programs;
                }
            }, err => {
                this.showLoader = false;
            });
        console.log("Programs", programs);
        return programs;

    }

    sendOtpToMobile(requestObject) {
        this.showLoader = true;
        this.otpSent = true;
        this
            .loginService
            .sendOTP(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                console.log(responseObject);
                var res = responseObject._body;
                console.log(typeof res);
                if (res.includes("ERR_MOBILE")) {
                    console.log("Failure");
                    this.otpSentFailed = true;
                    this.otpSent = false;
                } else {
                    this.otpSent = true;
                    this.otpSentFailed = false;
                }


            }, err => {
                this.showLoader = false;

            });
    }

    verifyOtpOfMobile(requestObject) {
        this.showLoader = true;
        this
            .loginService
            .verifyOTP(requestObject)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this
                    .configService
                    .getStatusTokens();
                var result = responseObject._body;
                result = result.trim();
                if (result === "OTP VERIFIED") {
                    this.goToHomePage();
                } else if (result === "OTP FAILED") {
                    this.showOtpFeedBack = true;
                    if (this.otpAttempts > 0) {
                        this.otpFeedback = "OTP Verification Failed " + this.otpAttempts + " Attempts Left!";
                        this.otpAttempts--;

                    } else {
                        this.goToLogin();

                    }

                }

            }, err => {
                this.showLoader = false;
            });
    }


}
