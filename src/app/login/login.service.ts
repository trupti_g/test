
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions, Jsonp } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class LoginService {
	private headers;
	private options;
	private frontendUserInfo;
	public noProgramsUserMapped;
	public noProgramsMapped;
	private userInfo = {};
	private oneProgramOtp: boolean;

	constructor(private http: Http,
		private configService: ConfigService,
		private jsonp: Jsonp) {

		console.log("this.configService.getAuthenticationToken()...", this.configService.getAuthenticationToken());
		this.headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.getAuthenticationToken(),
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.frontendUserInfo = configService.getFrontEndUserInfo();
	}


	setOneProgramOtp() {
		this.oneProgramOtp = true;
	}
	getOneProgramOtp() {
		return this.oneProgramOtp;
	}


	setUserInfo(userInfo) {
		this.userInfo = userInfo;
	}
	getUserInfo(): any {
		return this.userInfo;
	}

	setNoProgramUserMapped() {
		this.noProgramsUserMapped = true;
	}

	getNoProgramsUserMapped() {
		return this.noProgramsUserMapped;
	}


	login(reqObj): any {

		let url = this.configService.getApiUrls().login;
		let frontEndInfo = this.configService.getFrontEndUserInfo();
		console.log("userinfo -- ",reqObj);
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {};
		}
		console.log("login user info -- ",reqObj);
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {
				console.log("res.json() :  ",res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	forgotPassword(reqObj): any {

		let url = this.configService.getApiUrls().forgotPassword;
		let frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {};
		}
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	signup(reqObj): any {

		let url = this.configService.getApiUrls().signup;
		let frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {};
		}

		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

	signupTodo(reqObj): any {

		let url = this.configService.getApiUrls().signupTodo;
		return this.http.post(url, reqObj, this.options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}


	getProgramUser(reqObj, programId, userId): any {

		let frontEndInfo = this.configService.getFrontEndUserInfo();
		if (frontEndInfo != null || frontEndInfo != undefined) {
			reqObj.frontendUserInfo = frontEndInfo;
		} else {
			reqObj.frontendUserInfo = {};
		}

		reqObj.frontendUserInfo.programId = programId;
		reqObj.frontendUserInfo.programLogin = true;
		reqObj.frontendUserInfo.userId = userId;
		console.log(reqObj.frontendUserInfo);
		reqObj.serviceType = "programSetup";

		console.log(reqObj);
		var headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.getAuthenticationToken(),
		});
		var options = new RequestOptions({ headers: headers });

		let url = this.configService.getApiUrls().getProgramUser;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {

				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}
	sendOTP(reqObj): any {

		var headers = new Headers({
			'Content-Type': 'application/json',

		});

		var options = new RequestOptions({ headers: headers });




		let url = "http://www.webpostservice.com/sendsms/otp_req.php?username=Annectos&password=OkTXpm&type=TEXT&sender=ANCTOS&mobile=" + reqObj.mobile;

		return this.http.get(url)
			.map((res: Response) => {

				return res;
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}
	verifyOTP(reqObj): any {

		var headers = new Headers({
			'Content-Type': 'application/json',

		});
		var options = new RequestOptions({ headers: headers });
		headers.append('Access-Control-Allow-Methods', 'GET');
		headers.append('Access-Control-Allow-Origin', '*');

		let url = "http://www.webpostservice.com/sendsms/otp_res.php?username=Annectos&password=OkTXpm&type=TEXT&sender=ANCTOS&mobile="
			+ reqObj.mobile + "&otp=" + reqObj.otp;



		return this.http.get(url)
			.map((res: Response) => {
				console.log(res);

				return res;
			})
			.catch((error: any) => {
				console.log("inside catch", error);
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});

	}





	getBusinessEntityUsers(reqobj): any {
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ',

		});
		let options = new RequestOptions({ headers: headers });
		let url = this.configService.getApiUrls().getBusinessEntityUsers;

		console.log("getBusinessEntityUsersURL", url);



		return this.http.post(url, reqobj, options)
			.map((res: Response) => {
				return res.json();
			})
			.catch((error: any) => {
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	};






}
