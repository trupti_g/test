/*
	Author			:	Pratik Gawand
	Description		: 	Component for Forgot Password Selection page
	Date Created	: 	19 April 2017
	Date Modified	: 	19 April 2017
*/

import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { LoginService } from "../login/login.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './forgot-password.component.html',
    styleUrls: ["../app.component.scss", './forgot-password.component.scss'],
    providers: [LoginService,Device]
})



export class ForgotPasswordComponent implements OnInit {

    private forgotPasswordForm: FormGroup;
    private showLoader: boolean = false;
    private showSuccess: boolean = false;
    private invalidUserName: boolean = false;
    private message: string;
    private tryAgainBtn: boolean = false;
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;
    constructor(
        private configService: ConfigService,
        private loginService: LoginService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,
    ) {

        this.initForgotPasswordForm();

    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            console.log("Event", event);
            if (event instanceof NavigationEnd) {
                if(this.device.device === "android" || this.device.device === "ios"){
                    console.log("inside android and ios");
                    (<any>window).ga.trackView(event.urlAfterRedirects)
                } else {
                    ga('set', 'page', event.urlAfterRedirects);
                    ga('send', 'pageview');
                }
            }
        });
    }
    initForgotPasswordForm() {
        this.forgotPasswordForm = this.formBuilder.group({
            "userName": ["", [Validators.required]]
        });
    }



    sendPassword() {
        this.showSuccess = false;
        this.invalidUserName = false;
        var userName = this.forgotPasswordForm.value.userName;

        if (userName.includes("@")) {
            var isEmail = /([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/.test(userName);

            if (isEmail) {
                let requestObject = {
                    "userName": userName
                }

                this.forgotPassword(requestObject);

            } else {
                this.invalidUserName = true;
            }

        } else {
            var isMobile = /[0-9]{10}/.test(userName);
            if (isMobile && (userName.length == 10)) {
                let requestObject = {

                    "userName": userName
                }

                this.forgotPassword(requestObject);

            } else {
                this.invalidUserName = true;
            }
        }





    }

    closeDialog() {
    }
    showSuccessMessage() {
        this.showSuccess = true;
        this.message = "Password has been sent successfully to your registered mobileNumber !!";
    }



    showError(message) {
        this.showSuccess = true;
        this.message = message;
        this.tryAgainBtn = true;
    }

    tryAgain() {
        this.showSuccess = false;
        this.tryAgainBtn = false;
    }


    /***********************************    API CALLS   ******************************/


    /*
       METHOD         : forgotPassword()
       DESCRIPTION    : For providing the password to registered mobile number.
   */
    forgotPassword(requestObject) {
        this.showLoader = true;

        this.loginService.forgotPassword(requestObject)
            .subscribe(
                (responseObject) => {
                    this.showLoader = false;
                    let responseCodes = this.configService.getStatusTokens();

                    console.log(responseObject);
                    console.log("Response code", responseCodes);
                    switch (responseObject.statusCode) {
                        case responseCodes.RESP_ROLLBACK_ERROR:
                            this.showError(responseObject.message);
                            break;
                        case responseCodes.RESP_SERVER_ERROR:
                            this.showError(responseObject.message);
                            break;
                        case responseCodes.RESP_SUCCESS:
                            this.showSuccessMessage();
                            break;
                        case responseCodes.RESP_AUTH_FAIL:

                            var message = "Email ID / Mobile Number Not Found"
                            this.showError(message);

                            break;
                        case responseCodes.RESP_FAIL:
                            this.showError(responseObject.message);
                            break;
                        case responseCodes.RESP_ALREADY_EXIST:
                            this.showError(responseObject.message);
                            break;
                        case responseCodes.RESP_KYC_AWAITING:
                            this.showError(responseObject.message);
                            break;
                        case responseCodes.RESP_BLOCKED:
                            this.showError(responseObject.message);
                            break;


                    }
                },
                err => {
                    this.showLoader = false;

                }
            );
    }



    back() {
        this.router.navigate(["login"]);
    }
}


