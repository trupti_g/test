/*
	Author			:	Ajeet Yadav
	Description		: 	Redemptions Module
	Date Created	: 	07 June 2018
	Date Modified	: 	07 June 2018
*/

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';

import {FooterModule} from '../footer/footer.module';
import { RedemptionsHomeComponent } from "./redemptions-home.component";
import { RedemptionDetailComponent } from './redemption-detail.component';

@NgModule({
	declarations: [RedemptionsHomeComponent,RedemptionDetailComponent],
	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule,FooterModule],
	exports: [],
	providers: [],
	entryComponents: [RedemptionDetailComponent]
})
export class RedemptionsModule {
}