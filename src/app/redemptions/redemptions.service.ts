
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class RedemptionsService {
	private headers;
	private options;

	constructor(private http: Http,
		private configService: ConfigService) {
		this.headers = new Headers({
			'Content-Type': 'application/json'
		});
		this.options = new RequestOptions({ headers: this.headers });
	}

	// HTTP Network Call to get list of users orders made
	getAllOrders(requestObject): any {
		console.log("Service : Request Object In Get All Orders: ", requestObject);

		requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
		});
		let options = new RequestOptions({ headers: headers });
		let reqObj = requestObject;

		let url = this.configService.getApiUrls().addOrders;

		return this.http.post(url, reqObj, options)
			.map((res: Response) => {
				console.log("inside map", res.json());
				return res.json();
			})
			.catch((error: any) => {
				console.log("inside catch");
				return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
			});
	}

}
