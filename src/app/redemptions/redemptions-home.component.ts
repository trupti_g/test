/*
	Author			:	Deepak Terse
	Description		: 	Component for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { RedemptionsService } from "./redemptions.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { MdDialog, MdDialogRef } from '@angular/material';
import { RedemptionDetailComponent } from './redemption-detail.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
	templateUrl: './redemtion-home-component1.html',
	styleUrls: ["../app.component.scss", "./redemptions.component1.scss"],
	providers: [RedemptionsService,Device]

})
export class RedemptionsHomeComponent implements OnInit {
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;
	noRecordMessage: boolean = false;
	fromToDateInvalid: boolean;
	isTimePeriod: boolean;
	showDetail: boolean;
	order: any;
	endRecord: number;
	startRecord: number;
	isCountCanBeShown: boolean;
	public orderArr: any = [];
	public requestObj: any = {};
	private requestObjectRedeem: any = {
		customerId: "",
		programId: "",
		frontendUserInfo: {},
		skip: 0,
		limit: 10
	}
	public showLoader: boolean = false;
	public isRecord: boolean;
	public filterReedemRequestsForm: FormGroup;
	private filterName: string = "";
	private timeline: string = "";
	private statusFilterName: string = "";
	private orderStatus: string;
	private noOfRedeems: number;
	private serialSkip: number;
	private limit: number;
	private skip: number;
	private fromTo: boolean = false;
	public isStartDate: boolean = false;
	public isEndDate: boolean = false;
	public date: Date = new Date();
	currentMonth = this.date.getMonth();
	public redeemProgramStartDate: string;
	public redeemcurrentStartDate: string;
	private sdate: any;
	private edate: any;
	private programUserInfo: any;
	private pageCount: number = 1;
	public totalRecords: number = 0;
	public isPrevious: boolean = true;
	public isNext: boolean = false;
	public clientColor: any;
	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private redemptionsService: RedemptionsService,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		private stringService: StringService,
		private formBuilder: FormBuilder,
		public sidebar: SidebarComponent,
		public dialog: MdDialog,
		private device: Device,
	 ) {
		this.clientColor = this.configService.getThemeColor();
		this.router.events.subscribe(event => {

			// For width
			this.mobilewidth = window.screen.width;
			console.log("Width:", this.mobilewidth);
			if (this.mobilewidth <= 576) {
				this.displaypc = false;
				this.displaymobile = true;
				// document.getElementById("desktop-div").style.visibility = "visible";
				// document.getElementById("mobile-div").style.visibility = "hidden";
			}
			else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
				this.displaypc = false;
				this.displaymobile = true;
				// document.getElementById("desktop-div").style.visibility = "visible";
				// document.getElementById("mobile-div").style.visibility = "hidden";

			}
			else {
				this.displaypc = true;
				this.displaymobile = false;
			}
			// for width code end
		});
			
		this.configService.setRewardGalleryFlag(false);
		this.isRecord = false;
		console.log("Is Record Found : ", this.isRecord);

		var start = this.date.getUTCFullYear() + "-" + ((this.date.getUTCMonth() + 1) - 1) + "-" + this.date.getUTCDate();
		this.sdate = new Date(start);

		var end = this.date.getUTCFullYear() + "-" + (this.date.getUTCMonth() + 1) + "-" + this.date.getUTCDate();
		this.edate = new Date(end).toLocaleDateString();

		this.totalRecords = 0;
		this.isPrevious = true;
		this.isNext = false;
		this.limit = 10;
		this.skip = 0;
		this.serialSkip = 0;
		this.pageCount = 1;
		this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
		this.orderStatus = "";
		this.filterName = " This Month";
		this.statusFilterName = "All";

		this.programUserInfo = configService.getloggedInProgramInfo();
		this.filterReedemRequestsForm = this.formBuilder.group({
			// "redeemShowResults": ["10"],
			"redeemStartDate": [""],
			"redeemEndDate": [""],
			"periodFilter": [""],
			"orderStatusFilter": [""],
			"approvalStatusFilter": [""],

		});
	}

	ngOnInit() {

		// Close the submenu
		this.sidebar.close();

		if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Redemption')
		} else {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Redemption");
				ga('send', 'pageview');
			}
		}
		
		this.requestObjectRedeem = {
			frontendUserInfo: {},
			skip: 0,
			limit: this.limit,
			sdate: this.sdate,
			edate: this.edate,
		}
		console.log("this.requestObjectRedeem.programId", this.requestObjectRedeem.programId);
		console.log("construct this.requestObjectRedeem.programId", this.requestObjectRedeem.programId);
		console.log("construct this.programUserInfo", this.programUserInfo);
		this.requestObjectRedeem.programId = this.programUserInfo.programId;
		this.requestObjectRedeem.programUserId = this.programUserInfo.programUserId;
		console.log("construct this.requestObjectRedeem.programUserId", this.requestObjectRedeem.programUserId);
		console.log("construct this.requestObjectRedeem.programId", this.requestObjectRedeem.programId);
		this.getAllOrders(this.requestObjectRedeem);

		this.filterReedemRequestsForm.valueChanges.subscribe(data => {
			// this.requestObjectRedeem = {};
			console.log("In value changes", data);
			this.requestObjectRedeem.frontendUserInfo = {};
			this.requestObjectRedeem.skip = this.requestObjectRedeem.skip;
			this.requestObjectRedeem.limit = this.requestObjectRedeem.limit;
			this.requestObjectRedeem.sdate = this.requestObjectRedeem.sdate;
			this.requestObjectRedeem.edate = this.requestObjectRedeem.edate;
			if (this.requestObjectRedeem.orderStatus) {
				this.requestObjectRedeem.orderStatus = this.requestObjectRedeem.orderStatus;
			}
			// if (data.redeemShowResults != null && data.redeemShowResults != "") {
			// 	this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
			// 	this.limit = parseInt(data.redeemShowResults);
			// 	this.pageCount = 1;
			// 	this.requestObjectRedeem.sdate;
			// 	this.requestObjectRedeem.edate;
			// } else 
			if (data.redeemStartDate != null && data.redeemStartDate != "") {

				// this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
				// this.limit = parseInt(data.redeemShowResults);
				this.requestObjectRedeem.sdate = data.redeemStartDate;
				this.requestObjectRedeem.edate = data.redeemEndDate;
				this.sdate = data.redeemStartDate;
				this.edate = data.redeemEndDate;
				this.pageCount = 1;
				this.isStartDate = true;
			}

			if (data.redeemEndDate != null && data.redeemEndDate != "") {

				// this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
				// this.limit = parseInt(data.redeemShowResults);
				this.requestObjectRedeem.sdate = data.redeemStartDate;
				this.requestObjectRedeem.edate = data.redeemEndDate;
				this.sdate = data.redeemStartDate;
				this.edate = data.redeemEndDate;
				this.pageCount = 1;
				this.isEndDate = true;
			}

			if (this.isStartDate && this.isEndDate) {
				var startDate = new Date(this.sdate);
				var endDate = new Date(this.edate);
				this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
					" to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();
			}

			var startDate = new Date(this.sdate);
			var endDate = new Date(this.edate);
			if (this.requestObjectRedeem.sdate != undefined || this.requestObjectRedeem.edate != undefined) {
				this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
					" to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
			} else {
				this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
			}
			console.log("IN VALUE CHANGE REQUESTOBJECTREDEEM : ", this.requestObjectRedeem);

			if (data.orderStatusFilter === "all") {
				this.redeemChangeToAll();
			} else if (data.orderStatusFilter === "pending") {
				this.redeemChangeToPending()
			} else if (data.orderStatusFilter === "confirmed") {
				this.redeemChangeToConfirmed()
			} else if (data.orderStatusFilter === "processing") {
				this.redeemChangeToProcessing()
			} else if (data.orderStatusFilter === "shipped") {
				this.redeemChangeToShipped()
			} else if (data.orderStatusFilter === "completed") {
				this.redeemChangeToCompleted()
			} else if (data.orderStatusFilter === "cancelled") {
				this.redeemChangeToCancelled()
			} else if (data.orderStatusFilter === "returned") {
				this.redeemChangeToReturned();
			} else if (data.orderStatusFilter === "") {
				this.redeemChangeToAll();
			}

			if (data.approvalStatusFilter === "all") {
				this.redeemChangeToAllApprovalStatus();
			} else if (data.approvalStatusFilter === "pending") {
				this.redeemApprovalStatusPending();
			} else if (data.approvalStatusFilter === "approved") {
				this.redeemApprovalStatusApproved();
			} else if (data.approvalStatusFilter === "rejected") {
				this.redeemApprovalStatusRejected();
			} else if (data.approvalStatusFilter === "returned") {
				this.redeemChangeToReturned();
			} else if (data.approvalStatusFilter === "") {
				this.redeemChangeToAllApprovalStatus();
			}

			if (data.periodFilter === "all") {
				this.redeemChangeToLifeTime();
			} else if (data.periodFilter === "thisMonth") {
				this.redeemChangeToMonth();
			} else if (data.periodFilter === "thisWeek") {
				this.redeemChangeToWeek();
			} else if (data.periodFilter === "thisQuarter") {
				this.redeemChangeToQuarter();
			} else if (data.periodFilter === "selectTimePeriod") {
				this.isTimePeriod = true;
				this.redeemChooseStartToEnd();
			} else if (data.periodFilter === "") {
				this.redeemChangeToLifeTime();
			}
			console.log("this.isTimePeriod",this.isTimePeriod);
			console.log("this.filterReedemRequestsForm.value.redeemStartDate",this.filterReedemRequestsForm.value.redeemStartDate);
			console.log("this.filterReedemRequestsForm.value.redeemEndDate",this.filterReedemRequestsForm.value.redeemEndDate);
			if (this.isTimePeriod === true) {
				if (!this.filterReedemRequestsForm.value.redeemStartDate && !this.filterReedemRequestsForm.value.redeemEndDate) {
					this.fromToDateInvalid = true;
				} else {
					this.fromToDateInvalid = false;
				}
			} else {
				this.fromToDateInvalid = false;
			}
			// this.getAllOrders(this.requestObjectRedeem);
		});

	}

	getMonthName(dt: number): String {

		let monthArray: String[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		return monthArray[dt];
	}

	redeemChangeToLifeTime() {
		this.fromTo = false;
		this.pageCount = 1;
		this.skip = 0
		this.requestObjectRedeem.skip = 0;
		this.filterName = "All";
		this.timeline = "All Redeemptions";
		this.statusFilterName = "All";
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showLifeTimeRedeems", this.requestObjectRedeem.programId);
		delete this.requestObjectRedeem.sdate;
		delete this.requestObjectRedeem.edate;
		console.log("Lifetime", this.requestObjectRedeem);
		this.serialSkip = this.requestObjectRedeem.skip;

		console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
	}

	redeemChangeToMonth() {
		this.pageCount = 1;
		this.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showMonthRedeems", this.requestObjectRedeem.programId);
		this.fromTo = false;
		this.filterName = "This Month";

		var month = new Date();
		var currentMonth = month.getMonth();

		var dt = new Date();
		dt.setMonth(dt.getMonth() - 1);

		var date = dt.toISOString();
		console.log("dt.toISOString() : ", date);
		this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
		var startDate = new Date(date);

		this.requestObjectRedeem.skip = 0;
		this.requestObjectRedeem.sdate = startDate;
		this.requestObjectRedeem.edate = new Date();
		this.serialSkip = this.requestObjectRedeem.skip;

		console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
	}

	redeemChangeToWeek() {
		this.pageCount = 1;
		this.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showWeekRedeems", this.requestObjectRedeem.programId);
		this.fromTo = false;
		this.filterName = " This Week";
		var dt = new Date(this.sdate);
		var weekDate = new Date();
		var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);

		this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
		this.requestObjectRedeem.sdate = weekStartDate;
		this.requestObjectRedeem.edate = weekDate;
		this.requestObjectRedeem.skip = 0;
		this.serialSkip = this.requestObjectRedeem.skip;
		console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
	}

	redeemChangeToQuarter() {
		this.pageCount = 1;
		this.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showQuarterRedeems", this.requestObjectRedeem.programId);
		this.fromTo = false;
		this.filterName = "This Quarter";
		var quarterMonth = new Date();
		var dt = new Date();
		var currentMonth = quarterMonth.getMonth();
		dt.setMonth(dt.getMonth() - 3);
		var date = dt.toISOString();
		this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
		this.requestObjectRedeem.sdate = new Date(date);
		this.requestObjectRedeem.edate = new Date();
		this.requestObjectRedeem.skip = 0;
		this.serialSkip = this.requestObjectRedeem.skip;
		console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
	}
	filter() {

		console.log("record msg befoe", this.noRecordMessage);
		this.getAllOrders(this.requestObjectRedeem);
		console.log("record msg after", this.noRecordMessage);
	}
	redeemChooseStartToEnd() {
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showCustomRedeems", this.requestObjectRedeem.programId);
		this.filterName = "Time Period";
		this.fromTo = true;
	}

	redeemChangeToAll() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showAllRedeems", this.requestObjectRedeem.programUserId);


		this.statusFilterName = this.stringService.getStaticContents().redeemAll;
		delete this.requestObjectRedeem.orderStatus;

		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToPending() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showPendingRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
		this.orderStatus = "Pending"
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);

	}

	redeemChangeToConfirmed() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showApprovedRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemConfirmed;
		this.orderStatus = "Confirmed";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToProcessing() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showProcessingRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemProcessing;
		this.orderStatus = "Processing";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToShipped() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showShippedRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemShipped;
		this.orderStatus = "Shipped";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToCompleted() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showCompletedRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemCompleted;
		this.orderStatus = "Completed";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToCancelled() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showCancelledRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemCancelled;
		this.orderStatus = "Cancelled";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToReturned() {
		this.pageCount = 1;
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showReturnedRedeems", this.requestObjectRedeem.programId);
		this.statusFilterName = this.stringService.getStaticContents().redeemReturned;
		this.orderStatus = "Returned";
		this.requestObjectRedeem.orderStatus = this.orderStatus;
		console.log("REQ", this.requestObjectRedeem);
	}

	redeemChangeToAllApprovalStatus() {
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showAllRedeems", this.requestObjectRedeem.programId);


		this.statusFilterName = this.stringService.getStaticContents().redeemAll;
		delete this.requestObjectRedeem.approvalStatus;

		console.log("REQ", this.requestObjectRedeem);
	}

	redeemApprovalStatusPending() {
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showPendingRedeems", this.requestObjectRedeem.programId);

		this.statusFilterName = this.stringService.getStaticContents().redeemApprovalStatusPending;
		this.requestObjectRedeem.approvalStatus = this.statusFilterName;

		console.log("REQ", this.requestObjectRedeem);
	}

	redeemApprovalStatusApproved() {
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showApprovedRedeems", this.requestObjectRedeem.programId);

		this.statusFilterName = this.stringService.getStaticContents().redeemApprovalStatusApproved;
		this.requestObjectRedeem.approvalStatus = this.statusFilterName;

		console.log("REQ", this.requestObjectRedeem);
	}

	redeemApprovalStatusRejected() {
		this.skip = 0;
		this.requestObjectRedeem.skip = 0;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "showRejectedRedeems", this.requestObjectRedeem.programId);

		this.statusFilterName = this.stringService.getStaticContents().redeemApprovalStatusRejected;
		this.requestObjectRedeem.approvalStatus = this.statusFilterName;

		console.log("REQ", this.requestObjectRedeem);
	}


	redeemOnStartDate(startDate) {
		this.redeemProgramStartDate = startDate;
		this.redeemcurrentStartDate = startDate;
	}

	redeemOnPrevClick() {
		this.requestObjectRedeem.skip = this.requestObjectRedeem.skip - this.limit;
		this.requestObjectRedeem.limit = this.limit;
		this.pageCount--;
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "previousPageRedeems", this.requestObjectRedeem.programId);
		this.serialSkip = this.requestObjectRedeem.skip;
		this.getAllOrders(this.requestObjectRedeem);
	}

	redeemOnNextClick() {
		this.requestObjectRedeem.skip = this.requestObjectRedeem.skip + this.limit;
		this.requestObjectRedeem.limit = this.limit;
		this.pageCount++;
		this.serialSkip = this.requestObjectRedeem.skip;
		console.log("skip : ", this.requestObjectRedeem.skip, "limit", this.requestObjectRedeem.limit);
		this.getAllOrders(this.requestObjectRedeem);
		this.googleAnalyticsEventsService.emitEvent("My Redeems", "nextPageRedeems", this.requestObjectRedeem.programId);

	}

	redeemShowResults(limit) {
		this.pageCount = 1;
		if (this.orderStatus == "") {
			this.statusFilterName = "All";
		} else {
			this.statusFilterName = this.orderStatus.replace(/\b\w/g, function (l) { return l.toUpperCase() })
		}


		// this.requestObjectRedeem.limit = parseInt(this.filterReedemRequestsForm.value.redeemShowResults);
		this.requestObjectRedeem.skip = 0;



		this.getAllOrders(this.requestObjectRedeem);
	}

	getAllOrders(requestObj) {

		console.log("GetALLLLORDERSSSSSSSSSSSSSSSSSSSSSSSSSSSsssssssss");
		this.orderArr = [];
		let order: any[] = [];
		requestObj.customerId = this.configService.getloggedInProgramUser().programUserId;
		requestObj.programId = this.configService.getloggedInProgramInfo().programId;
		this.showLoader = true;
		this.redemptionsService.getAllOrders(requestObj)
			.subscribe((responseObject) => {
				console.log("GetALLLLORDERSSSSSSSSSSSSSSSSSSSSSSSSSSSsssssssss", responseObject.message);
				if (responseObject.message == "Record not found") {
					this.noRecordMessage = true;
				}
				else { this.noRecordMessage = false; }

				this.showLoader = false;
				let responseCodes = this.configService.getStatusTokens();
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						break;
					case responseCodes.RESP_SERVER_ERROR:
						break;
					case responseCodes.RESP_SUCCESS:
						this.orderArr = [];
						this.orderArr = responseObject.result;
						console.log("ResponseObjectArray getAllOrders", this.orderArr);
						this.isRecord = false;
						console.log("Is Record Found : ", this.isRecord);
						this.noOfRedeems = responseObject.result.length;

						// Pagination validation
						if (this.orderArr.length < this.limit) {
							this.isNext = true;
						} else {
							this.isNext = false;
						}

						if (this.pageCount == 1) {
							this.isPrevious = true;
						} else {
							this.isPrevious = false;
						}
						if (this.requestObjectRedeem.skip == 0) {
							this.totalRecords = responseObject.count;
						}


						// code to show no. of records on navigation page
						if (this.orderArr.length > 0) {
							this.isCountCanBeShown = true;
							if (this.pageCount > 1) {
								this.startRecord =
									this.pageCount * this.limit - (this.limit - 1);
								this.endRecord =
									this.startRecord + (responseObject.result.length - 1);
								// this.noRecordMessage = false;
							} else {
								this.startRecord = 1;
								this.endRecord =
									this.startRecord + (responseObject.result.length - 1);
								// this.noRecordMessage = true;
							}
						} else {
							this.isCountCanBeShown = false;
						}

						//Disable next button if total records are equal to pagination limit
						if (this.totalRecords === this.pageCount * this.limit) {
							this.isNext = true;
						}
						break;
					case responseCodes.RESP_AUTH_FAIL:
						break;
					case responseCodes.RESP_FAIL:
						this.orderArr = [];
						this.noOfRedeems = 0;
						this.isRecord = true;
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						break;
					case responseCodes.RESP_BLOCKED:
						break;
				}
			},
				err => {
					this.showLoader = false;
				}
			);
	}

	detailClick(info) {
		console.log("infooooo", info);
		this.order = info;

		this.showDetail = true;

		// Code to stop backgroung scrolling 
		var scroll = document.getElementById('body');
		console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
		this.configService.setModalClose(false);

		document.getElementById("body").style.overflow = "hidden";
		console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
	}

	closeModal() {

		// Code to activate backgroung scrolling 
		var scroll = document.getElementById('body');
		console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

		this.configService.setModalClose(true);
		document.getElementById("body").style.overflow = "auto";
		console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
	}
}
