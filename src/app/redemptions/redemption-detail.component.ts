/*
	Author			:	Vaishali Sagvekar
	Description		: 	Component for Redemption Details 
	Date Created	: 	30-Aug-2017
	Date Modified	: 	
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({

  templateUrl: './redemption-detail.component.html',
  styleUrls: ['./redemptions.component.css'],
  providers: [Device]


})
export class RedemptionDetailComponent implements OnInit {


  public order: any = {};

  constructor(public dialogRef: MdDialogRef<RedemptionDetailComponent>,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private stringService: StringService,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private device: Device,
  ) {
    
    this.configService.setRewardGalleryFlag(false);

  }

  ngOnInit() {
    
    if (this.configService.getMyRedemption()) {
      this.order = this.configService.getMyRedemption();
      console.log("this.order",this.order);
    }

    if(this.device.device === "android" || this.device.device === "ios"){
			console.log("inside android and ios");
			(<any>window).ga.trackView('Redemption Details')
		} else {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', "Redemption Details");
          ga('send', 'pageview');
        }
      });
    }
  }

  closeInvoice() {
    this.dialogRef.close(RedemptionDetailComponent);
  }

}
