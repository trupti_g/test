/*
  Author			:	Deepak Terse
  Description		: 	Component for MyClaims ListClaims
  Date Created	: 	07 March 2016
  Date Modified	: 	07 March 2016
*/
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ClaimsService } from "./claims.service";
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { InvoiceClaimsComponent } from './invoice-claims.component';
// import { DigitalClaimsComponent } from './digital-claims.component';
// import { CodeClaimsComponent } from "./code-claims.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  templateUrl: './list-claims.component.html',
  styleUrls: ["../app.component.scss", "./claims.component.scss"],
  providers: [ClaimsService,Device]

})
export class ListClaimsComponent implements OnInit {
  mobilewidth: any;
  displaypc: boolean = true;
  displaymobile: boolean = false;

  fromToDateInvalid: boolean = false;
  showDetail: boolean;
  paymentStatus: string;
  purchaseDetails: any = [];
  claimDetail: any;
  isCountCanBeShown: boolean;
  isPrevious: boolean;
  isNext: boolean;
  endRecord: number;
  startRecord: number;
  totalRecords: number;
  private myClaimsInfo: any = null;
  private showLoader: boolean = false;
  private noOfClaims: number;

  public filterMyClaimForm: FormGroup;
  public filterForm: FormGroup;
  public requestObj: any = {};
  public filterName: string;
  public statusFilterName: string;
  public timeline: string;

  private limit: number;
  private skip: number;
  private serialSkip: number;
  public disablePrev: boolean = true;
  public disableNext: boolean = false;

  public isStartDate: boolean = false;
  public isEndDate: boolean = false;

  public fromTo: boolean = false;
  public noRecordMessage: boolean = false;

  public showInvoice: boolean = false;
  public showIVP: boolean = false; // SHOW INVOICE VALUE PRODUCT
  public showIQP: boolean = false; // SHOW INVOICE Quantity PRODUCT
  public showIBP: boolean = false; // SHOW INVOICE Both PRODUCT
  public showCodeTable: boolean = false; //SHOW CODE 

  public programEndDate: string;
  public programStartDate: string;
  public currentStartDate: string;

  public showDVP: boolean = false; // SHOW Digital VALUE PRODUCT
  public showDQP: boolean = false; // SHOW Digital Quantity PRODUCT
  public showDBP: boolean = false; // SHOW Digital Both PRODUCT


  public showDigital: boolean = false;
  public showBoth: boolean = false;

  public pageCount: number = 1; //to maintain pagecount in pagination
  public date: Date = new Date();
  currentMonth = this.date.getMonth();
  public totalClaimsCount: number;
  public startDate: any;
  public endDate: any;
  public creditPartyId: string;
  public filterStartDate: string;
  public filterEndDate: string;
  public approvalStatus: string;
  public clientColor: any;
  public programUserInfo: any = {};
  public programUserInformation: any = {};
  public businessInfo: any = {};

  public requestObject: any = {
    programId: "",
    clientId: "",
    creditPartyId: "",
    frontendUserInfo: {},
    skip: 0,
    limit: 10,
    sort: { "createdAt": -1 }


  }

  constructor(private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService,
    private claimsService: ClaimsService,
    private stringService: StringService,
    private formBuilder: FormBuilder,
    public dialog: MdDialog,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
	public sidebar: SidebarComponent,
	private device: Device,
  ) {
    this.mobilewidth = window.screen.width;
    console.log("Width:", this.mobilewidth);
    if (this.mobilewidth <= 576) {
      this.displaypc = false;
      this.displaymobile = true;
    }
    else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
      this.displaypc = false;
      this.displaymobile = true;

    }
    else {
      this.displaypc = true;
      this.displaymobile = false;
    }

    this.configService.setRewardGalleryFlag(false);


    this.limit = 10;
    this.skip = 0;
    this.serialSkip = 0;
    this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
    var start = this.date.getUTCFullYear() + "-" + ((this.date.getUTCMonth() + 1) - 1) + "-" + this.date.getUTCDate();


    this.startDate = new Date(start);

    var end = this.date.getUTCFullYear() + "-" + (this.date.getUTCMonth() + 1) + "-" + this.date.getUTCDate();

    this.approvalStatus = "";
    this.endDate = new Date(end).toLocaleDateString();
    this.filterName = " This Month";
    this.statusFilterName = "All";
    this.businessInfo = configService.getloggedInBEInfo();
    this.clientColor = this.configService.getThemeColor();





    this.programUserInfo = configService.getloggedInProgramInfo();
    this.programUserInformation = configService.getloggedInProgramUser();

    this.creditPartyId = this.programUserInformation.programUserId;
    console.log('This ', this.creditPartyId);
    console.log('This ', this.programUserInformation);

    this.filterMyClaimForm = this.formBuilder.group({
      timeFilter: [""],
      startDate: [""],
      endDate: [""],
      statusFilter: [""]
    });
  }

  ngOnInit() {

	if(this.device.device === "android" || this.device.device === "ios"){
		console.log("inside android and ios");
		(<any>window).ga.trackView("My Claims")
	} else {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
			ga('set', 'page', "My Claims");
			ga('send', 'pageview');
			}
		});
	}

    this.sidebar.close();

    this.requestObject.programId = this.programUserInfo.programId;
    this.requestObject.clientId = this.programUserInfo.clientId;
    this.requestObject.frontendUserInfo = {};
    this.requestObject.skip = 0;
    this.requestObject.creditPartyId = this.creditPartyId;
    this.requestObject.limit = this.limit;
    console.log("NGON req", this.requestObject);
    // INVOICE PRODUCT VALUE
    if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
      console.log("THIS IS TRUE");
      this.showIVP = true;
    }
    // INVOICE BRAND VALUE
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
      this.showIVP = true;
      console.log("THIS IS TRUE");
    }
    // INVOICE PRODUCT BOTH  
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
      this.showIVP = true;
      console.log("THIS IS TRUE");
    }
    // INVOICE BRAND BOTH
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
      this.showIVP = true;
      console.log("THIS IS TRUE");
    }
    // INVOICE PRODUCT QUANTITY  
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
      this.showIQP = true;
      console.log("THIS IS TRUE");
    }
    // INVOICE BRAND QUANTITY
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
      this.showIQP = true;
      console.log("THIS IS TRUE");
    }

    // DIGITAL PRODUCT VALUE
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
      this.showDVP = true;
      console.log("THIS IS TRUE");
    }
    // DIGITAL BRAND VALUE
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
      this.showDVP = true;
      console.log("THIS IS TRUE");
    }

    // DIGITAL PRODUCT BOTH
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
      this.showDVP = true;
      console.log("THIS IS TRUE");
    }

    // DIGITAL BRAND BOTH
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
      this.showDVP = true;
      console.log("THIS IS TRUE");
    }

    // DIGITAL PRODUCT QUANTITY
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
      this.showDQP = true;
      console.log("THIS IS TRUE");
    }
    // DIGITAL BRAND QUANTITY
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
      this.showDQP = true;
      console.log("THIS IS TRUE");
    }

    else if (this.programUserInfo.claimEntryType === "code") {
      this.showCodeTable = true;
      console.log("THIS IS TRUE");
    }
    console.log("NGON req LATER", this.requestObject);
    this.getAllTheSecondarySales(this.requestObject);
    this.valueChanges();
  }

  valueChanges() {
    this.filterMyClaimForm.valueChanges.subscribe(data => {
      console.log(">>>>>>><<<<<<<<<<", this.filterMyClaimForm.value.startDate);
      console.log(">>>>>>><<<<<<<<<<", this.filterMyClaimForm.value.endDate);
      // this.requestObj = {};
      this.requestObject.programId = this.programUserInfo.programId;
      this.requestObject.clientId = this.programUserInfo.clientId;
      this.requestObject.creditPartyId = this.creditPartyId;
      this.requestObject.frontendUserInfo = {};
      this.requestObject.skip = this.requestObject.skip;
      this.requestObject.limit = this.requestObject.limit;
      this.requestObject.startDate = this.requestObject.startDate;
      this.requestObject.endDate = this.requestObject.endDate;
      this.requestObject.approvalStatus = this.requestObject.approvalStatus;

      console.log("DATA-------------------------", data)
      if (data.startDate != null && data.startDate != "") {

        this.requestObject.startDate = data.startDate;
        this.requestObject.endDate = data.endDate;
        this.startDate = data.startDate;
        this.startDate = new Date(this.startDate);
        var a = new Date(data.startDate);
        a.setHours(0, 0, 0, 0);
        this.requestObject.startDate = a.toISOString();
        this.pageCount = 1;
        this.isStartDate = true;


      }

      if (data.endDate != null && data.endDate != "") {

        this.endDate = data.endDate;
        var e = new Date(data.endDate);
        e.setHours(23, 59, 59, 999);
        this.requestObject.endDate = e.toISOString();
        this.pageCount = 1;
        this.isEndDate = true;
      }

      if (this.isStartDate && this.isEndDate) {

        var startDate = new Date(this.startDate);
        var endDate = new Date(this.endDate);
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();

      }

      var startDate = new Date(this.startDate);
      var endDate = new Date(this.endDate);
      if (this.requestObject.startDate != undefined || this.requestObject.endDate != undefined) {
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
      } else {
        this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
      }
      console.log("IN VALUE CHANGE this.requestObject.programId ", this.requestObject.programId);

      if (data.statusFilter == "all") {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showAllClaims", this.requestObject.programId);
        this.statusFilterName = this.stringService.getStaticContents().myClaimsAll;
        delete this.requestObject.approvalStatus;
        console.log("REQ ALL", this.requestObject);
      } else if (data.statusFilter == "pending") {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showPendingClaims", this.requestObject.programId);
        this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
        this.approvalStatus = "pending"
        this.requestObject.approvalStatus = "pending";
        console.log("REQ pending", this.requestObject);
      } else if (data.statusFilter == "approved") {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showApprovedClaims", this.requestObject.programId);
        this.statusFilterName = this.stringService.getStaticContents().myClaimsApproved;
        this.approvalStatus = "approved";
        this.requestObject.approvalStatus = "approved";
        console.log("REQ approved", this.requestObject);
      } else if (data.statusFilter == "rejected") {
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showRejectedClaims", this.requestObject.programId);
        this.statusFilterName = this.stringService.getStaticContents().myClaimsRejected;
        this.approvalStatus = "rejected";
        this.requestObject.approvalStatus = "rejected";
        console.log("REQ rejected", this.requestObject);
      } else if(data.statusFilter == "hold"){
        this.skip = 0;
        this.requestObject.skip = 0;
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showHoldClaims", this.requestObject.programId);
        this.statusFilterName = this.stringService.getStaticContents().myClaimsHold;
        this.approvalStatus = "hold";
        this.requestObject.approvalStatus = "hold";
      }
      if (data.timeFilter == "all") {
        this.fromTo = false;
        this.filterName = "All";
        this.timeline = "All Claims";
        this.statusFilterName = "All";
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showLifeTimeClaims", this.requestObject.programId);
        delete this.requestObject.startDate;
        delete this.requestObject.endDate;
        console.log("Lifetime", this.requestObject);
      } else if (data.timeFilter == "thisMonth") {
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showMonthClaims", this.requestObject.programId);
        this.fromTo = false;
        this.filterName = " This Month";
        var month = new Date();
        var dt = new Date();
        var currentMonth = month.getMonth();
        dt.setMonth(dt.getMonth() - 1);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
        var startDate = new Date(date);
        this.requestObject.startDate = startDate;
        this.requestObject.endDate = new Date();
        console.log("month change", this.requestObject);
      } else if (data.timeFilter == "thisWeek") {
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showWeekClaims", this.requestObject.programId);
        this.fromTo = false;
        this.filterName = " This Week";
        var dt = new Date(this.startDate);
        var weekDate = new Date();
        var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
        this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
        this.requestObject.startDate = weekStartDate;
        this.requestObject.endDate = weekDate;
        console.log("week change", this.requestObject);
      } else if (data.timeFilter == "thisQuarter") {
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showQuarterClaims", this.requestObject.programId);
        this.fromTo = false;
        this.filterName = "This Quarter";
        var quarterMonth = new Date();
        var dt = new Date();
        var currentMonth = quarterMonth.getMonth();
        dt.setMonth(dt.getMonth() - 3);
        var date = dt.toISOString();
        this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
        this.requestObject.startDate = new Date(date);
        this.requestObject.endDate = new Date();
        console.log("quarter change", this.requestObject);
      } else if (data.timeFilter == "selectTimePeriod") {
        console.log("data.statusFilter", data.statusFilter);
        this.googleAnalyticsEventsService.emitEvent("My Claims", "showCustomClaims", this.requestObject.programId);
        this.filterName = "Time Period";
        console.log("this.fromTo", this.fromTo);
        this.fromTo = true;
        console.log("this.fromTo after", this.fromTo);
      };

      if (this.fromTo === true) {
        if (!this.filterMyClaimForm.value.startDate && !this.filterMyClaimForm.value.endDate) {
          this.fromToDateInvalid = true;
        } else {
          this.fromToDateInvalid = false;
        }
      } else {
        this.fromToDateInvalid = false;
      }
    });


  }




  getMonthName(dt: number): String {

    let monthArray: String[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return monthArray[dt];
  }

  changeToPending() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showPendingClaims", this.requestObject.programId);
    this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
    this.approvalStatus = "pending"
    this.requestObject.approvalStatus = "pending";
    console.log("REQ pending", this.requestObject);

  }
  changeToApproved() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showApprovedClaims", this.requestObject.programId);
    this.statusFilterName = this.stringService.getStaticContents().myClaimsApproved;
    this.approvalStatus = "approved";
    this.requestObject.approvalStatus = "approved";
    console.log("REQ approved", this.requestObject);
  }
  changeToRejected() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showRejectedClaims", this.requestObject.programId);
    this.statusFilterName = this.stringService.getStaticContents().myClaimsRejected;
    this.approvalStatus = "rejected";
    this.requestObject.approvalStatus = "rejected";
    console.log("REQ rejected", this.requestObject);
  }
  changeToAll() {
    this.skip = 0;
    this.fromTo = false;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showAllClaims", this.requestObject.programId);


    this.statusFilterName = this.stringService.getStaticContents().myClaimsAll;
    delete this.requestObject.approvalStatus;

    console.log("REQ ALL", this.requestObject);
  }

  changeToLifeTime() {
    this.filterName = "All";
    this.timeline = "All Claims";
    this.statusFilterName = "All";
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showLifeTimeClaims", this.requestObject.programId);
    delete this.requestObject.startDate;
    delete this.requestObject.endDate;
    console.log("Lifetime", this.requestObject);
  }

  changeToWeek() {
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showWeekClaims", this.requestObject.programId);
    this.fromTo = false;
    this.filterName = " This Week";
    var dt = new Date(this.startDate);
    var weekDate = new Date();
    var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
    this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
    this.requestObject.startDate = weekStartDate;
    this.requestObject.endDate = weekDate;
    console.log("week change", this.requestObject);
  }





  changeToQuarter() {
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showQuarterClaims", this.requestObject.programId);
    this.fromTo = false;
    this.filterName = "This Quarter";
    var quarterMonth = new Date();
    var dt = new Date();
    var currentMonth = quarterMonth.getMonth();
    dt.setMonth(dt.getMonth() - 3);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
    this.requestObject.startDate = new Date(date);
    this.requestObject.endDate = new Date();
    console.log("quarter change", this.requestObject);
  }

  changeToMonth() {
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showMonthClaims", this.requestObject.programId);
    this.fromTo = false;
    this.filterName = " This Month";
    var month = new Date();
    var dt = new Date();
    var currentMonth = month.getMonth();
    dt.setMonth(dt.getMonth() - 1);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
    var startDate = new Date(date);
    this.requestObject.startDate = startDate;
    this.requestObject.endDate = new Date();
    console.log("month change", this.requestObject);
  }

  chooseStartToEnd() {
    this.googleAnalyticsEventsService.emitEvent("My Claims", "showCustomClaims", this.requestObject.programId);
    this.filterName = "Time Period";
    console.log("this.fromTo", this.fromTo);
    this.fromTo = true;
    console.log("this.fromTo after", this.fromTo);
  }




  changeLimit() {

  }

  onPrevClick() {
    this.requestObject.skip = this.requestObject.skip - this.limit;
    this.requestObject.limit = this.limit;
    this.pageCount--;
    this.googleAnalyticsEventsService.emitEvent("My Claims", "previousPageClaims", this.requestObject.programId);
    this.serialSkip = this.requestObject.skip;
    this.getAllSecondarySales(this.requestObject);
  }


  onNextClick() {
    this.requestObject.skip = this.requestObject.skip + this.limit;
    this.requestObject.limit = this.limit;
    this.pageCount++;
    this.serialSkip = this.requestObject.skip;
    this.getAllSecondarySales(this.requestObject);
    this.googleAnalyticsEventsService.emitEvent("My Claims", "nextPageClaims", this.requestObject.programId);

  }

  onStartDate(startDate) {
    this.programStartDate = startDate;
    this.currentStartDate = startDate;
  }

  claimsFiltering(claimsInfo) {
    for (let i = 0; i < claimsInfo.length; i++) {
      let invoiceDoc = claimsInfo[i].invoiceDocument;
      claimsInfo[i].invoiceDocument = invoiceDoc;
      claimsInfo[i].approvalStatus
      if (claimsInfo[i].approvalStatus == "pending") {
        claimsInfo[i].pointsEarned = "Pending";
      }
      else if (claimsInfo[i].approvalStatus == "rejected") {
        claimsInfo[i].pointsEarned = 0;
      }

    }

    this.myClaimsInfo = claimsInfo;


  }
  downloadDocument() {
    console.log("this.claimDetail.invoiceNumber", this.claimDetail.invoiceNumber);
    this.googleAnalyticsEventsService.emitEvent("My Claims", "downloadClaim", this.claimDetail.invoiceNumber);

  }
  showResults(limit) {
    if (this.approvalStatus == "") {
      this.statusFilterName = "All";
    } else {
      this.statusFilterName = this.approvalStatus.replace(/\b\w/g, function (l) { return l.toUpperCase() })
    }

    this.requestObject.skip = 0;
    var requestObj = this.requestObject;



    this.getAllTheSecondarySales(this.requestObject);
  }



  /***************************************	API CALLS 	****************************************************/
  /*
      METHOD         : getAllSecondarySales()
      DESCRIPTION    : To get all claims
  */
  getAllSecondarySales(requestObject) {
    this.showLoader = true;
    this.noRecordMessage = false;
    console.log("request in API", requestObject);

    if (requestObject.approvalStatus == "") {
      delete requestObject.approvalStatus;
    }
    console.log("trupti");
    console.log("this.programUserInformation.programRole ---", this.programUserInformation.programRole);
    console.log("1 before changes", this.requestObject);
    // send parentUserId in case of ASM, RCM, CM to get respective child's claims  
    if (this.programUserInformation.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
      || this.programUserInformation.programRole === this.stringService.getStaticContents().jswCMProgramRole
      || this.programUserInformation.programRole === this.stringService.getStaticContents().jswRCMProgramRole) {
      delete this.requestObject.creditPartyId;
      this.requestObject.parentUserId = this.programUserInformation.programUserId;
    }else if(this.programUserInformation.programRole === this.stringService.getStaticContents().jswDistributorProgramRole){
      delete this.requestObject.creditPartyId;
      this.requestObject.parentUserId = this.programUserInformation.programUserId;
    }else if(this.programUserInformation.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
    || this.programUserInformation.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
      delete this.requestObject.creditPartyId;
      delete this.requestObject.parentUserId;
    }
    console.log("1 After changesss :::", this.requestObject);

    this.claimsService.getAllSecondarySales(requestObject)
      .subscribe(
        (responseObject) => {
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();

          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_SERVER_ERROR:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_SUCCESS:
              console.log("in get all sec sale in list claim", responseObject);
              this.myClaimsInfo = responseObject.result;
              var resultCount = responseObject.result.count;
              this.noOfClaims = responseObject.result.length;
              var resultCount = responseObject.count;
              console.log("this.myClaimsInfo", this.myClaimsInfo);
              this.configService.setMyClaims(responseObject.result);

              console.log("Length", this.myClaimsInfo.length);
              console.log("Limit", this.limit);
              console.log("Count", resultCount);
              console.log("this.pageCount", this.totalClaimsCount);
              this.serialSkip = this.requestObject.skip;
              if (this.myClaimsInfo.length < this.limit) {
                this.isNext = true;
              } else {
                this.isNext = false;
              }

              if (this.pageCount == 1) {
                this.isPrevious = true;
              } else {
                this.isPrevious = false;
              }
              if (this.requestObject.skip == 0) {                
                this.totalRecords = responseObject.count;                
              }

              // code to show no. of records on navigation page
              if (this.myClaimsInfo.length > 0) {
                this.isCountCanBeShown = true;
                if (this.pageCount > 1) {
                  this.startRecord =
                    this.pageCount * this.limit - (this.limit - 1);
                  this.endRecord =
                    this.startRecord + (responseObject.result.length - 1);
                } else {
                  this.startRecord = 1;
                  this.endRecord =
                    this.startRecord + (responseObject.result.length - 1);
                }
              } else {
                this.isCountCanBeShown = false;
              }

              //Disable next button if total records are equal to pagination limit
              if (this.totalRecords === this.pageCount * this.limit) {
                this.isNext = true;
              }
              console.log("SKIP", this.requestObject.skip);
              this.myClaimsInfo = responseObject.result;
              this.claimsFiltering(responseObject.result);
              console.log("SKIPPER", this.serialSkip);
              break;
            case responseCodes.RESP_AUTH_FAIL:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_FAIL:
              this.myClaimsInfo = null;
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_ALREADY_EXIST:
              this.noRecordMessage = true;
              break;
          }
        },
        err => {
          this.showLoader = false;
        }
      );
  }

  getAllTheSecondarySales(requestObject) {
    console.log("request in API222", requestObject);
    this.showLoader = true;
    this.noRecordMessage = false;

    delete requestObject.startDate;
    delete requestObject.endDate;

    if (requestObject.approvalStatus == "") {
      delete requestObject.approvalStatus;
    }

    console.log(" 2. trupti");
    console.log("this.programUserInformation.programRole", this.programUserInformation.programRole, this.programUserInformation.programUserId);
    console.log("2 before changes", this.requestObject.parentUserId);
    if (this.programUserInformation.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
      || this.programUserInformation.programRole === this.stringService.getStaticContents().jswCMProgramRole
      || this.programUserInformation.programRole === this.stringService.getStaticContents().jswRCMProgramRole ) {
      delete this.requestObject.creditPartyId;
      this.requestObject.parentUserId = this.programUserInformation.programUserId;
    }
    else if(this.programUserInformation.programRole === this.stringService.getStaticContents().jswDistributorProgramRole){

      delete this.requestObject.creditPartyId;
      this.requestObject.parentUserId = this.programUserInformation.programUserId;

        console.log("this.programUserInformation :::", this.programUserInformation);
    }else if(this.programUserInformation.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
    || this.programUserInformation.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
      delete this.requestObject.creditPartyId;
      delete this.requestObject.parentUserId;
    }
    console.log("2 After changesss", this.requestObject.parentUserId);
    console.log("this.programUserInformation", this.programUserInformation);
    this.claimsService.getAllSecondarySales(requestObject)
      .subscribe(
        (responseObject) => {
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();

          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_SERVER_ERROR:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_SUCCESS:
              console.log("@@@@@@@@@@@@@@@@@@@@@@", responseObject);

              this.claimsFiltering(responseObject.result);
              console.log("")
              this.configService.setMyClaims(responseObject.result);
              this.totalClaimsCount = responseObject.count;
              this.noOfClaims = this.totalClaimsCount;
              console.log("this.myClaimsInfo.length", this.myClaimsInfo.length);
              console.log("length", this.totalClaimsCount);


              if (responseObject.result.length < this.limit) {
                this.isNext = true;
              } else {
                this.isNext = false;
              }

              if (this.pageCount == 1) {
                this.isPrevious = true;
              } else {
                this.isPrevious = false;
              }
              if (this.requestObject.skip == 0) {
                this.totalRecords = responseObject.count;
              }

              // code to show no. of records on navigation page
              if (responseObject.result.length > 0) {
                this.isCountCanBeShown = true;
                if (this.pageCount > 1) {
                  this.startRecord =
                    this.pageCount * this.limit - (this.limit - 1);
                  this.endRecord =
                    this.startRecord + (responseObject.result.length - 1);
                } else {
                  this.startRecord = 1;
                  this.endRecord =
                    this.startRecord + (responseObject.result.length - 1);
                }
              } else {
                this.isCountCanBeShown = false;
              }

              //Disable next button if total records are equal to pagination limit
              if (this.totalRecords === this.pageCount * this.limit) {
                this.isNext = true;
              }
              console.log("SKIPPER", this.serialSkip);
              break;
            case responseCodes.RESP_AUTH_FAIL:
              this.noRecordMessage = true;
              break;
            case responseCodes.RESP_FAIL:
              this.showLoader = false;
              this.myClaimsInfo = null;
              this.noRecordMessage = true;
              this.showBoth = false;
              this.showDBP = false;
              this.showDQP = false;
              this.showDVP = false;
              this.showIBP = false;
              this.showIQP = false;

              break;
            case responseCodes.RESP_ALREADY_EXIST:
              this.noRecordMessage = true;
              break;
          }
        },
        err => {
          this.showLoader = false;
        }
      );
  }

  filter() {
    this.pageCount = 1;
    this.totalRecords = 0;
    this.startRecord = 0;
    this.endRecord = 0;
    this.limit = 10;
    this.skip = 0;
    console.log("filter REQ OBJJJ", this.requestObject);
    this.getAllSecondarySales(this.requestObject);
  }
  detailClick(info) {

    // Code to stop backgroung scrolling
    var scroll = document.getElementById('body');
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);


    document.getElementById("body").style.overflow = "hidden";
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

    this.configService.setModalClose(false);

    console.log("infooooo", info);
    this.claimDetail = info;
    this.purchaseDetails = info.purchaseDetails;
    if (info.paymentStatus == "notDone") {
      this.paymentStatus = "Pending";
      info.paymentStatus = this.paymentStatus;
      console.log("this.paymentStatus", this.paymentStatus);


    }
    if (info.paymentStatus == "done") {
      this.paymentStatus = "Done";
      info.paymentStatus = this.paymentStatus;
      console.log("this.paymentStatus", this.paymentStatus);

    }
    if (info.paymentStatus == "partiallyDone") {
      this.paymentStatus = "Partially Done";
      info.paymentStatus = this.paymentStatus;
      console.log("this.paymentStatus", this.paymentStatus);

    }
    if (info.paymentStatus == "pending") {

      this.paymentStatus = "Pending";
      info.paymentStatus = this.paymentStatus;
      console.log("this.paymentStatus", this.paymentStatus);
    };
    this.showDetail = true;
  }

  closeModal() {
    // Code to activate backgroung scrolling
    var scroll = document.getElementById('body');
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);

    this.configService.setModalClose(true);

    document.getElementById("body").style.overflow = "auto";
    console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
  }

  download() {
    console.log("this.claimDetail.invoiceNumber", this.claimDetail.invoiceNumber);
    this.googleAnalyticsEventsService.emitEvent("My Claims", "downloadClaim", this.claimDetail.invoiceNumber);

  }
  /************************************ 	END OF API CALLS 	************************************************/

}
