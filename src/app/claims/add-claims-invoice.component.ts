/*
	Author			:	Deepak Terse
	Description		: 	Component for Add Claim Invoice page
	Date Created	: 	21 March 2017
	Date Modified	: 	27 March 2017
*/

import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { ConfigService } from "../shared-services/config.service";
import { ClaimsService } from "./claims.service";
import { StringService } from "../shared-services/strings.service";
import { S3UploadService } from "../shared-services/s3-upload.service";


import { FeedbackMessageComponent } from "./feedback-message.component";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { ISubscription } from "rxjs/Subscription";
import { SidebarComponent } from '../home/sidebar/sidebar.component'
import { FeedbackMessageComponent1 } from "./feedback-message1.component";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
	templateUrl: './add-claims-invoice.component.html',
	styleUrls: ['./claims.component.scss', "../app.component.scss"],
	providers: [ClaimsService,Device]

})
export class AddClaimsInvoiceComponent implements OnInit, OnDestroy {
	public message1: any = {};
	public allowToAddClaim: boolean = false;
	message: { str: string; };
	dateTodayZ: Date;
	programUserInfo: any;
	formTransactionDate: any;
	transactionPeriodEndDate: any;
	transactionPeriodStartDate: any;
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;

	private claimsForm: FormGroup;
	private showLoader: boolean;
	private subscription: ISubscription;
	private distributors: any[];
	private distributorsInfo: any;
	public clientColor: any;
	private showDistributornotFoundMessage: boolean;
	private showDistributorFoundMessage: boolean;
	private disableSearchDistributer: boolean;
	private disableBtn: boolean;
	public claimPeriodEndDate: Date;
	public claimPeriodStartDate: Date;
	public programInfo: any;
	private resetFlag: boolean = false;
	public todaysDate: any;
	public claimStartDate: any; //This variable is for the validation of 5th of every month scenario. User will only be able to upload claim till 5th of next month.
	public claimEndDate: any;//This variable is for the validation of 5th of every month scenario. User will only be able to upload claim till 5th of next month.
	public canUploadClaim: boolean = true;

	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	@ViewChild(FeedbackMessageComponent1)
	private feedbackMessageComponent1: FeedbackMessageComponent1;

	private isFileSelected: boolean;
	// private disableSearchDistributer: boolean;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private claimsService: ClaimsService,
		private stringService: StringService,
		private s3UploadService: S3UploadService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
		public sidebar: SidebarComponent,
		private device: Device,) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
			// document.getElementById("desktop-div").style.visibility = "visible";
			// document.getElementById("mobile-div").style.visibility = "hidden";
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}
		if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Add Claims")
        } else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Add Claims");
					ga('send', 'pageview');
				}
			});
		}
	
		this.configService.setRewardGalleryFlag(false);
		this.showLoader = false;
		this.disableBtn = true;

		this.showDistributornotFoundMessage = false;
		this.showDistributorFoundMessage = false;
		this.isFileSelected = false;
		this.disableSearchDistributer = false;
		this.distributorsInfo = this.configService.getloggedInProgramUser();
		var distName = this.distributorsInfo;
		this.clientColor = this.configService.getThemeColor();
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.transactionPeriodStartDate = new Date(this.programInfo.transactionPeriodStartDate);
		this.transactionPeriodEndDate = new Date(this.programInfo.transactionPeriodEndDate);
		this.programUserInfo = configService.getloggedInProgramUser();
		this.dateTodayZ = new Date();
		console.log("!!!!!!", this.dateTodayZ);
		var dateTodayZstring = this.dateTodayZ.toISOString();
		this.todaysDate = dateTodayZstring.split('T')
		console.log("this.todaysDatethis.todaysDate", this.todaysDate);
		this.claimStartDate = new Date();
		this.claimStartDate.setDate(0);
		console.log("claimStartDate date", this.claimStartDate);
		this.claimEndDate = new Date();
		this.claimEndDate.setMonth(this.dateTodayZ.getMonth() + 1);
		this.claimEndDate.setDate(4);
		console.log("this.claimEndDate", this.claimEndDate);
		this.claimStartDate.setUTCHours(24, 0, 0, 0); //converting to IST by adding 5 hours and 30 min
		// this.claimStartDate.setUTCMinutes(this.claimStartDate.getUTCMinutes() + 30);
		this.claimEndDate.setUTCHours(24, 0, 0, 0); //converting to IST by adding 5 hours and 30 min
		// this.claimEndDate.setUTCMinutes(this.claimStartDate.getUTCMinutes() + 30);
		this.message = {
			str: "Note: You are making a claim outside the transaction period of the enrolled program"
		}
	}

	ngOnDestroy() {
		console.log("@@@ DESTROY CALLLED @@@")
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	ngOnInit() {
		window.scrollTo(0, 0);
		this.sidebar.close();
		this.initclaimsForm();
		this.valueChange();
		this.claimPeriodStartDate = new Date(this.programInfo.claimPeriodStartDate);
		this.claimPeriodEndDate = new Date(this.programInfo.claimPeriodEndDate);
		console.log("this.claimPeriodStartDate", this.claimPeriodStartDate);
		console.log("this.claimPeriodEndDate", this.claimPeriodEndDate);
		this.distributors = this.configService.getloggedInProgramUser().parentUsersInfo;
		console.log("Distributors in add claim Invoice: ", this.distributors);
		var programUserInfo = this.configService.getloggedInProgramUser();
		console.log("programUserInfo", programUserInfo);

		// disable add invoice button in case of ASM,CM,RSM
		if (programUserInfo.programRole === this.stringService.getStaticContents().jswASMAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswCMProgramRole
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswRCMProgramRole
		   	|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole) {
			this.canUploadClaim = false;
		}
		console.log("this.canUploadClaim", this.canUploadClaim);

		this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
			console.log("Subscription got", value);
			// console.log(this.claimsForm.value);
			if (this.claimsForm.valid) {

				this.claimsForm.patchValue(value);
				if (value.length > 0) {
					let recordInfo: any = {};
					// this.claimsForm.controls['invoiceDocument'].setValue(value);
					if (this.claimsForm.value.debitPartyId != "") {
						//if user is either selected or searched		
						recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
					}
					else {
						//if user is not found in search
						recordInfo.distributorName = this.claimsForm.value.distributorName;
						recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
					}

					//mandatory fields
					recordInfo.invoiceNumber = this.claimsForm.value.invoiceNumber;
					recordInfo.invoiceAmount = this.claimsForm.value.invoiceAmount;
					if (this.claimsForm.value.invoiceAmountExcludingVAT != "") {

						recordInfo.invoiceAmountExcludingVAT = this.claimsForm.value.invoiceAmountExcludingVAT;
					}

					// recordInfo.invoiceAmountExcludingVAT = this.claimsForm.value.invoiceAmountExcludingVAT;
					recordInfo.transactionDate = this.claimsForm.value.transactionDate;
					recordInfo.vatPercentage = 18;
					recordInfo.creditPartyId = this.configService.getloggedInProgramUser().programUserId;
					recordInfo.invoiceDocument = value[0].Location;
					this.addSecondarySale(recordInfo);
				}
			}
		});
	}

	valueChange() {
        // if()
		this.formTransactionDate = this.claimsForm.value.transactionDate;
		this.formTransactionDate = new Date(this.formTransactionDate);
		console.log("valuechange", this.formTransactionDate);
		console.log("this.claimStartDate", this.claimStartDate);
		console.log("this.claimEndDate", this.claimEndDate);
		if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
			if (this.resetFlag === false) {
				this.feedbackMessageComponent.updateMessage(true, this.message.str.valueOf(), "alert-danger");
			}
		};
		var tDate = this.formTransactionDate.getDate();
		var tMonth = this.formTransactionDate.getMonth();
		var todayDate = new Date().getDate();
		var todayMonth = new Date().getMonth();
		console.log("tDate,tMonth,todayDate,todayMonth",tDate,tMonth,todayDate,todayMonth);
	    if( tMonth == 6 || tMonth == 7){
			console.log("tp ravi 1");
			if(todayMonth <= 8){
				console.log("tp ravi 2");
				this.allowToAddClaim = true;
				console.log("allow in temp cond");
			}else{
				console.log("tp ravi 3");
				this.message1 = {
					str: "Transaction not allowed for this date!!"
				};
				this.allowToAddClaim = false;
				if (this.resetFlag === false) {
					this.feedbackMessageComponent.updateMessage(true, this.message1.str.valueOf(), "alert-danger");
				};
			}
		}else{
			console.log("TDate", tDate, "today", todayDate);
			console.log("todayMonth - tMonth === 1", todayMonth - tMonth);
			if ((todayMonth - tMonth === 0)) {
				this.allowToAddClaim = true;
			} else if ((todayMonth - tMonth === 1)) {

				if (todayDate > 5) {
					if (this.formTransactionDate < this.claimStartDate || this.formTransactionDate > this.claimEndDate) {
						this.message = {
							str: "Transaction Date cannot be less than 1st of current month and more than 5th of next month"
						};
						this.allowToAddClaim = false;
						if (this.resetFlag === false && this.formTransactionDate !== "Invalid Date") {
							this.feedbackMessageComponent.updateMessage(true, this.message1.str.valueOf(), "alert-danger");
							// window.scroll(0, 0);
						};
						console.log("ALLOW", this.allowToAddClaim);
					} else {
						this.allowToAddClaim = true;
						console.log("ALLOW2", this.allowToAddClaim);
					}
				} else {
					this.allowToAddClaim = true;
					console.log("here");
				}
			} else if (todayMonth - tMonth > 1) {
				this.message1 = {
					str: "Transaction not allowed for this date!!"
				};
				this.allowToAddClaim = false;
				if (this.resetFlag === false) {
					this.feedbackMessageComponent.updateMessage(true, this.message1.str.valueOf(), "alert-danger");
				};
			}
		}
	}


	tDateChange() {
		if (this.claimsForm.value.invoiceAmount && !this.claimsForm.value.invoiceAmountExcludingVAT) {
			this.claimsForm.patchValue({
				"invoiceAmountExcludingVAT": this.claimsForm.value.invoiceAmount
			})
		} else if (this.claimsForm.value.invoiceAmountExcludingVAT) {

		} else {
			this.claimsForm.patchValue({
				"invoiceAmountExcludingVAT": ""
			})
		}
	}
	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initclaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates invoice form group and assign initial values and validations to its controls
    */
	initclaimsForm() {
		this.claimsForm = this.formBuilder.group({
			invoiceNumber: ["", [Validators.required]],
			transactionDate: ["", [Validators.required]],
			invoiceAmount: [""],
			invoiceAmountExcludingVAT: [""],

			invoiceDocument: [],
			vatPercentage: ["18"],

			debitPartyId: ["", []],
			//OR

			distributorName: ["", []],
			distributorMobileNo: ["", [Validators.pattern('[0-9]{10}')]]
		});
	}

	/*
        METHOD         : clearclaimsForm()
        DESCRIPTION    : Called from onResetFormClick() and after form is submitted
						 Resets the form to null values
    */
	clearclaimsForm() {
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "resetClaim", "form reset");
		this.resetFlag = true;
		this.claimsForm.reset({
		});
		this.resetFlag = false;

	}
	/***************************** 		END OF FORM METHODS 	********************************/





	/************************************* 	ONCLICK METHODS 	********************************/

	onDistributorSelected(selectedDistributor) {

		if (selectedDistributor != "") {
			this.claimsForm.controls['distributorMobileNo'].disable();

			console.log("selectedDistributor", selectedDistributor);


			this.googleAnalyticsEventsService.emitEvent("Add Claims", "select distributor", selectedDistributor);
			this.claimsForm.controls['distributorName'].setValidators(null);
			this.claimsForm.controls['distributorMobileNo'].setValidators(null);
			this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

			this.showDistributorFoundMessage = false;
			this.claimsForm.patchValue({
				"distributorMobileNo": "",
				"distributorName": ""
			});
			this.showDistributornotFoundMessage = false;

			console.log(this.claimsForm.value);

		} else {
			this.claimsForm.controls['distributorMobileNo'].enable();

		}

	}



	onUserFound(userDetails) {
		this.showDistributorFoundMessage = true;

		this.claimsForm.patchValue({
			"distributorName": userDetails.userName,
			"debitPartyId": userDetails.userId
		});
		this.claimsForm.controls["debitPartyId"].setValidators([]);
	}

	onUserNotFound() {
		this.showDistributornotFoundMessage = true;
		this.claimsForm.patchValue({
			"distributorName": "",
			"debitPartyId": ""
		});
	}

	/*
        METHOD         : onAddFormClick()
        DESCRIPTION    : Called when user clicks on "ADD CLAIM" button
				      	 Forms request object and calls addSecondarySale to add new claim
    */
	onAddFormClick() {
		if (new Date() >= this.claimPeriodStartDate && new Date() <= this.claimPeriodEndDate) {
			if (this.showLoader == false) {
				this.showLoader = true;
				if (this.isFileSelected) {
					this.isFileSelected = false;
				} {


					this.s3UploadService.uploadFiles();
					this.isFileSelected = true;

				}

			}
		} else {
			this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't add claim outside of claim period", "alert-danger");

		}
	}

	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls clearclaimsForm() to reset invoice form to null values
    */
	onResetFormClick() {
		this.clearclaimsForm();
		this.claimsForm.patchValue({
			"debitPartyId": ""
		});
		this.isFileSelected = false;
	}


	onSearchDistributorClick() {

		this.claimsForm.controls['distributorName'].setValidators([Validators.required]);;
		this.claimsForm.controls['distributorMobileNo'].setValidators([Validators.required, , Validators.pattern('[0-9]{10}')]);;
		this.claimsForm.controls['debitPartyId'].setValidators(null);
		this.claimsForm.patchValue({
			"debitPartyId": ""
		});

		let mobno: string = this.claimsForm.value.distributorMobileNo as string
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "searchDistributor", mobno);

		console.log("mobile no", mobno.length);
		let reqObj = {
			"searchByMobile": mobno,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"clientId": this.configService.getloggedInProgramInfo().clientId

		};


		this.getProgramUser(reqObj);
	}



	onFileChange(fileInput: any) {

		console.log("File", fileInput);
		this.isFileSelected = true;
		let filePath = "Programs-Data" + "/" + "Invoices" + "/" + "invoice";
		this.s3UploadService.formParams(fileInput, filePath);

	}


	/********************************* 	END OF ONCLICK METHODS 	*********************************/






	/***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : addSecondarySale()
        DESCRIPTION    : To add a claim 
    */
	addSecondarySale(recordInfo) {
		if (recordInfo.distributorName) {

			this.googleAnalyticsEventsService.emitEvent("Add Claims", "addClaim", recordInfo.distributorName);
		}

		this.showLoader = true;
		console.log("recordInfo", recordInfo);
		this.claimsService.addSecondarySale(recordInfo)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;
					window.scroll(0, 0);
					let responseCodes = this.configService.getStatusTokens();

					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.feedbackMessageComponent1.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.feedbackMessageComponent1.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SUCCESS:
							console.log("recordInfo.invoiceNumber", recordInfo.invoiceNumber);
							this.googleAnalyticsEventsService.emitEvent("Add Claims", "addClaimSucceed", responseObject.result.invoiceNumber);
							this.feedbackMessageComponent1.updateMessage(true, "Claim added succesfully", "alert-success");
							this.onResetFormClick();
							break;
						case responseCodes.RESP_AUTH_FAIL:
							this.feedbackMessageComponent1.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_FAIL:
							// this.googleAnalyticsEventsService.emitEvent("Add Claims", recordInfo.programId, "addClaimFailed");
							this.googleAnalyticsEventsService.emitEvent("Add Claims", "addClaimFailed", recordInfo.invoiceNumber);
							this.feedbackMessageComponent1.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							this.feedbackMessageComponent1.updateMessage(true, responseObject.message, "alert-danger");
							break;
					}
				},
				err => {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}

	/*
METHOD         : getProgramUser()
DESCRIPTION    : To add a claim 
*/
	getProgramUser(reqObj) {
		this.showLoader = true;

		this.claimsService.getProgramUser(reqObj)
			.subscribe(
				(responseObject) => {
					this.showLoader = false;

					let responseCodes = this.configService.getStatusTokens();
					this.showDistributornotFoundMessage = false;
					this.showDistributorFoundMessage = false;

					this.claimsForm.patchValue({
						"distributorName": ""
					});
					switch (responseObject.statusCode) {
						case responseCodes.RESP_ROLLBACK_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SERVER_ERROR:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_SUCCESS:
							console.log(responseObject.result[0].userDetails.userId);
							console.log(responseObject.result[0].userDetails.userName);

							if (this.configService.getLoggedInUserInfo().mobileNumber !== responseObject.result[0].userDetails.mobileNumber) {

								this.onUserFound(responseObject.result[0].userDetails);
							} else {
								this.feedbackMessageComponent.updateMessage(true, "You cannot be your own distributor", "alert-danger");
							}
							break;
						case responseCodes.RESP_AUTH_FAIL:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
						case responseCodes.RESP_FAIL:
							this.onUserNotFound();
							break;
						case responseCodes.RESP_ALREADY_EXIST:
							this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
							break;
					}
				},
				err => {
					this.showLoader = false;
					this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
				}
			);
	}
	/************************************ 	END OF API CALLS 	************************************************/

}