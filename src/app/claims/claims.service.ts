import { Injectable, OnInit } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable } from "rxjs";

@Injectable()
export class ClaimsService {
    private headers: any;
    private options: any;
    private frontendUserInfo: any;
    private userInfo;
    private requestObj: any;


    constructor(private http: Http, private configService: ConfigService) {

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this
                .configService
                .getAuthenticationToken()
        });
        this.options = new RequestOptions({ headers: this.headers });

        this.frontendUserInfo = this.configService.getFrontEndUserInfo();


        this.userInfo = this.configService.getLoggedInUserInfo();
        this.requestObj = {
            "programId": this
                .configService
                .getloggedInProgramInfo()
                .programId,
            "clientId": this
                .configService
                .getloggedInProgramInfo()
                .clientId
        }

    }

    setCreditPartyInfo() {
        this.requestObj.recordInfo.creditPartyId = this
            .configService
            .getloggedInBEInfo()
            .businessId;
    }

    setDebitPartyInfo() {
        let parentUsers = this
            .configService
            .getloggedInProgramUser()
            .parentUsers;

        for (let i = 0; i < parentUsers.length; i++) {
            if (this.requestObj.recordInfo.debitPartyId === parentUsers.parentUserId) {
                this.requestObj.recordInfo.debitPartyName = "";
                this.requestObj.recordInfo.debitPartyMobileNo = "";
            }
        }

    }
    /*******************************	API CALLS 	***********************************/
    /*
        METHOD         : getAllSecondarySales()
        DESCRIPTION    : Called from Claims list page inside ngOnInit()
                         Fetches all secondary sales i.e. claims list
    */
    getAllSecondarySales(reqobj): any {
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }


        reqobj.frontendUserInfo = frontEndInfo;
        reqobj.serviceType = "programOperations";




        let url = this
            .configService
            .getApiUrls()
            .getAllSecondarySales;

        return this
            .http
            .post(url, reqobj, this.options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
        METHOD         : addSecondarySale()
        DESCRIPTION    : Called from add claims page on form submission
                         Adds new claims to the database
    */
    addSecondarySale(recordInfo): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;


        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        this.requestObj.frontendUserInfo = frontEndInfo;
        this.requestObj.recordInfo = recordInfo;
        this.requestObj.serviceType = "programOperations";
        this.requestObj.recordInfo.uploadedFrom = "channelApp";  // commented by vaishali
        console.log(this.requestObj);
        let url = this
            .configService
            .getApiUrls()
            .addSecondarySale;

        return this
            .http
            .post(url, this.requestObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
           METHOD         : addSecondarySaleCode()
           DESCRIPTION    : Called from add claims page on form submission
                            Adds new claims to the database
       */
    addSecondarySaleCode(requestObj): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo.appType) delete frontEndInfo.appType;
        var userIds = this.userInfo.userId;


        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }
        this.requestObj = requestObj;
        this.requestObj.frontendUserInfo = frontEndInfo;

        this.requestObj.serviceType = "programOperations";
        this.requestObj.recordInfo.uploadedFrom = "channelApp";
        this.requestObj.programId = this.configService.getloggedInProgramInfo().programId;
        this.requestObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        this.requestObj.userId = this.userInfo.userId;
        console.log(this.requestObj);
        let url = this
            .configService
            .getApiUrls()
            .addSecondarySale;

        return this
            .http
            .post(url, this.requestObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    /*
        METHOD         : getAllPackagingUnits()
        DESCRIPTION    : Called from add claims digital page inside ngOnInit()
                         Get all packaging units to populate in the add claims form
    */
    getAllPackagingUnits(reqObj): any {
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }


        reqObj.frontendUserInfo = frontEndInfo;
        reqObj.serviceType = "client";
        reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;

        let url = this.configService.getApiUrls().getAllPackagingUnits;

        return this.http.post(url, reqObj, this.options).map((res: Response) => {
            return res.json();
        }).catch((error: any) => {
            return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
        });
    }

    /*
        METHOD         : getAllBrands()
        DESCRIPTION    : Called from add claims digital - brands page inside ngOnInit()
                         Get all brands to populate in the add claims form
    */
    getAllBrands(reqObj): any {
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        console.log("fR", frontEndInfo);
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }


        reqObj.frontendUserInfo = frontEndInfo;
        reqObj.serviceType = "client";


        reqObj.clientId = this
            .configService
            .getloggedInProgramInfo()
            .clientId;

        let url = this
            .configService
            .getApiUrls()
            .getAllBrands;
        return this
            .http
            .post(url, reqObj, this.options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
        METHOD         : getAllProducts()
        DESCRIPTION    : Called from add claims digital - products page inside ngOnInit()
                         Get all products to populate in the add claims form
    */
    getAllProducts(reqObj): any {
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }


        reqObj.frontendUserInfo = frontEndInfo;



        reqObj.clientId = this
            .configService
            .getloggedInProgramInfo()
            .clientId;

        let url = this
            .configService
            .getApiUrls()
            .getAllProducts;


        reqObj.serviceType = "client";

        return this
            .http
            .post(url, reqObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    /*
        METHOD         : addSecondarySale()
        DESCRIPTION    : Called from add claims page on form submission
                        Adds new claims to the database
    */
    getProgramUser(reqObj): any {


        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        reqObj.serviceType = "programSetup";
        reqObj.frontendUserInfo = frontEndInfo;

        console.log(reqObj);
        let url = this.configService.getApiUrls().getProgramUser;

        reqObj.serviceType = "programSetup";
        return this
            .http
            .post(url, reqObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    /*
        METHOD         : getAllUniqueCodes()
        DESCRIPTION    :Get all unique Code list.
    */
    getAllUniqueCodes(reqObj): any {

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        reqObj.serviceType = "programSetup";
        reqObj.frontendUserInfo = frontEndInfo;
        reqObj.programId = this.configService.getloggedInProgramInfo().programId;
        reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        console.log(reqObj);
        let url = this.configService.getApiUrls().getAllUniqueCodes;

        reqObj.serviceType = "programSetup";
        return this
            .http
            .post(url, reqObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getProgramScheme(requestObj): any {
        let requestObject: any = {};
        requestObject = requestObj;
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();;
        requestObject.programId = this.configService.getloggedInProgramInfo().programId;
        requestObject.clientId = this.configService.getloggedInProgramInfo().clientId;
        requestObject.serviceType = "programSetup";
        requestObject.frontendUserInfo.appType = "adminApp";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getProgramScheme;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }



    /******************************* 	END OF API CALLS 	**********************************/
}
