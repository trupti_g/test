/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
import { ListClaimsComponent } from "./list-claims.component";
import { InvoiceClaimsComponent } from "./invoice-claims.component";
import { AddClaimsInvoiceComponent } from "./add-claims-invoice.component";
import { FooterModule } from '../footer/footer.module';



import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
import { FeedbackMessageComponent } from "./feedback-message.component";
import { FeedbackMessageComponent1 } from "./feedback-message1.component";

@NgModule({
	declarations: [ListClaimsComponent, FeedbackMessageComponent, AddClaimsInvoiceComponent,
		InvoiceClaimsComponent, FeedbackMessageComponent1

	],

	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule, FooterModule],
	exports: [],

	providers: [],
	entryComponents: [InvoiceClaimsComponent,

	]
})
export class ClaimsModule {
}