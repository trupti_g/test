/*
	Author			:	Pratik Gawand
	Description		: 	Component for Invoice  Popup 
	Date Created	: 	27 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({

  templateUrl: './invoice-claims.component.html',
  selector: 'invoice-claims',
  styleUrls: ['./claims-details.component.scss'],
  providers: [Device]

})
export class InvoiceClaimsComponent implements OnInit {


  private invoiceInfo: any = {};
  public showIVP: boolean = false; // SHOW INVOICE VALUE PRODUCT
  public showIQP: boolean = false; // SHOW INVOICE Quantity PRODUCT
  public showIVB: boolean = false; // SHOW INVOICE VALUE BRAND
  public showIQB: boolean = false; // SHOW INVOICE Quantity BRAND
  private programUserInfo: any = {};
  private purchaseDetails: any;
  public clientColor: any;
  public paymentStatus: string;
  public isPaymentQualificationSet: boolean;


  constructor(public dialogRef: MdDialogRef<InvoiceClaimsComponent>,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private stringService: StringService,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private device: Device,
  ) {
    this.purchaseDetails = [];
    this.clientColor = this.configService.getThemeColor();
    this.configService.setRewardGalleryFlag(false);
    if(this.device.device === "android" || this.device.device === "ios"){
      console.log("inside android and ios");
      (<any>window).ga.trackView("Claims Details")
    } else {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', "Claims Details");
          ga('send', 'pageview');
        }
      });
    }
  

    if (configService.getmyClaims()) {
      this.invoiceInfo = configService.getmyClaims();
      if (this.invoiceInfo.purchaseDetails) {
        this.purchaseDetails = this.invoiceInfo.purchaseDetails;
      }

      if (this.isPaymentQualificationSet = true) {
        console.log('IN THIS FUN');
        console.log("INVOICE", this.invoiceInfo.paymentStatus);
        if (this.invoiceInfo.paymentStatus == "notDone") {
          this.paymentStatus = "Pending";
          this.invoiceInfo.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);

        }
        if (this.invoiceInfo.paymentStatus == "done") {
          this.paymentStatus = "Done";
          this.invoiceInfo.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);

        }
        if (this.invoiceInfo.paymentStatus == "partiallyDone") {
          this.paymentStatus = "Partially Done";
          this.invoiceInfo.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);

        }
        if (this.invoiceInfo.paymentStatus == "pending") {

          this.paymentStatus = "Pending";
          this.invoiceInfo.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);

        }
      }
      console.log(this.invoiceInfo);
      this.programUserInfo = this.configService.getloggedInProgramInfo();
    }


  }
  ngOnInit() {
    console.log("points earned label  invoice    ", this.configService.getTerminology());
    console.log("points field in String ", this.stringService.getStaticContents());
    if (this.stringService.getfieldLabels().points == undefined) {
      this.stringService.getfieldLabels().points = this.configService.getTerminology();
    }
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.invoiceInfo.secondarySalesId, "invoice");
    if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {

      this.showIVP = true;
      console.log("HERe product");
    }
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
      this.showIVB = true;
      console.log("HERe brand");
    }
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
      this.showIVP = true;
      console.log("HERe porduct");
    }
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
      this.showIVB = true;
      console.log("HERe brand");
    }
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
      this.showIQP = true;
      console.log("HERe product");
    }
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
      this.showIQB = true;
      console.log("HERe brand");
    }


  }
  closeInvoice() {
    this.dialogRef.close(InvoiceClaimsComponent);
  }
  download() {
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.invoiceInfo.secondarySalesId, "downloadClaim");

  }
}
