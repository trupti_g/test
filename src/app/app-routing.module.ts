/*
	Author			:	Deepak Terse
	Description		: 	Routes for all the component will be setup here
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/


import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LoginComponent } from "./login/login.component";

import { SignUpComponent } from "./signup/signup.component";

import { HomeComponent } from "./home/home.component";

import { PageNotFoundComponent } from "./page-not-found.component";

import { AuthGuard } from "./shared-services/auth-guard.service";

import { ForgotPasswordComponent } from './forgotPassword/forgot-password.component';

export const appRoutes: Routes = [


	{ path: 'login', pathMatch: 'full', component: LoginComponent },

	{ path: 'signup', pathMatch: 'full', component: SignUpComponent },
	{ path: 'forgotPassword', pathMatch: 'full', component: ForgotPasswordComponent },
	{
		path: 'home', component: HomeComponent,
		loadChildren: 'app/home/home.module#HomeModule',
	},
	{ path: '', pathMatch: 'full', redirectTo: 'login' },
	{ path: '**', component: PageNotFoundComponent }




];

@NgModule({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})
export class AppRoutingModule {

}
