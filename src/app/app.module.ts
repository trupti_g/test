import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from './app.component';
import { HomeModule } from "./home/home.module";
import { LoginModule } from "./login/login.module";
import { FooterModule } from "./footer/footer.module";
import { UserModule } from "./user/user.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PageNotFoundComponent } from "./page-not-found.component";
import { FeedbackMessageComponent } from './feedback-message.component';

import { AuthGuard } from "./shared-services/auth-guard.service";
import { ConfigService } from "./shared-services/config.service";

import { StringService } from "./shared-services/strings.service";
import { S3UploadService } from "./shared-services/s3-upload.service";
import { ProductDetailsService } from "./shared-services/product-details.service";

import { GoogleAnalyticsEventsService } from "./shared-services/google-analytics-events.service";

import { AppService } from "./app.service";
import { WindowRef } from './shared-components/WindowRef';
import { SpecialOffersService } from "./special-offers/special-offers.service";

import { CartService } from "./shared-services/cart.service";
import { CsvService } from "./shared-services/csv.service";

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

// import { KSSwiperModule } from 'angular2-swiper';
import { ExcelService } from "./shared-services/excel.service";

import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        FeedbackMessageComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule,
        AppRoutingModule,
        HomeModule,
        LoginModule,
        UserModule,
        FooterModule,
        AmChartsModule,
        // KSSwiperModule,
        NgbModule.forRoot(),
        // HttpClientModule,
        NgbCarouselModule.forRoot(),
        NgbDropdownModule.forRoot(),
   
    ],

    exports: [],
    providers: [
        AuthGuard,
        ConfigService,
        StringService,
        S3UploadService,
        WindowRef,
        AppService,
        GoogleAnalyticsEventsService,
        ProductDetailsService,
        SpecialOffersService,
        CartService,
        CsvService,
        ExcelService,
        Device
    ],

    bootstrap: [AppComponent]
})
export class AppModule { }
