/*
	Author			:	Deepak Terse
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	07 March 2016
	Date Modified	: 	07 March 2016
*/

import { NgModule } from "@angular/core";
// import { ChannelsHomeComponent } from "./channels-home.component";
import { ChannelsDetailsComponent } from './channels-details.component';
import { ChannelsHierarchyComponent } from './channels-hierarchy.component';
// import { FeedbackMessageComponent } from "./feedback-message.component";

import { AddClaimsInvoiceComponent } from "./channels-add-claims/add-claims-invoice.component";
import { AddClaimsDigitalProductsComponent } from "./channels-add-claims/add-claims-digital-products.component";
import { AddClaimsDigitalBrandsComponent } from "./channels-add-claims/add-claims-digital-brands.component";
import { AddClaimsCodeComponent } from "./channels-add-claims/add-claims-code.component";

import { InvoiceClaimsOperationsComponent } from "./channels-add-claims/invoice-claims-operations.component";
import { DigitalClaimsOperationsComponent } from "./channels-add-claims/digital-claims-operations.component";
import { CodeClaimsOperationsComponent } from "./channels-add-claims/code-claims-operations.component";
import { ChannelsRedemptionDetailComponent } from "./channels-redempton-detail";

import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import { FeedbackMessageComponent } from "./feedback-message.component";
import { MaterialModule } from '@angular/material';
import { FooterModule } from '../footer/footer.module';
// import { ChannelsRoutingModule } from "./channels.routing.module";
import { SharedModule } from "./../shared-components/shared.module";
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'; // For components like alert, model, tab, accordion etc.




import { ClaimsService } from "../claims/claims.service";
import { ChannelsService } from "./channels.service";


@NgModule({
	declarations: [
		ChannelsDetailsComponent,
		ChannelsHierarchyComponent,
		AddClaimsInvoiceComponent,
		AddClaimsDigitalProductsComponent,
		AddClaimsDigitalBrandsComponent,
		InvoiceClaimsOperationsComponent,
		DigitalClaimsOperationsComponent,
		AddClaimsCodeComponent,
		CodeClaimsOperationsComponent,
		ChannelsRedemptionDetailComponent
		//FeedbackMessageComponent
	],

	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule, FooterModule, NgbModule, SharedModule],
	exports: [],

	providers: [ChannelsService, ClaimsService],
	entryComponents: [InvoiceClaimsOperationsComponent, DigitalClaimsOperationsComponent, CodeClaimsOperationsComponent]
})
export class ChannelsModule {
}