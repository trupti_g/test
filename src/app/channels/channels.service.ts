
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions, RequestMethod } from "@angular/http";
import { ConfigService } from "../shared-services/config.service";
import { Observable, Subject } from "rxjs";

@Injectable()
export class ChannelsService {
    channelForCodeVerification: any;
    ChannelProfileForComingBack: any;
    newChannel: any;
    private headers;
    private options;
    private frontendUserInfo: any;
    private userInfo;
    private requestObj: any;
    private programInfo: any;
    private clientId = this.configService.getloggedInProgramInfo().clientId;
    private programId = this.configService.getloggedInProgramInfo().programId;
    private loggedInProgramInfo: any;
    public channelsChain: any = [];
    public channelProfileInfo: any;
    public channelsChainRequestObject: any;
    public channelsBackRequestObject: any;
    public channelsClaimsBackRequestObject: any;
    public channelBack: boolean = false;
    public creditPartyId: any;
    public distributorInfo: any = [];
    public approval = new Subject();
    public selectedCreditPartyId: string = "";


    constructor(private http: Http,
        private configService: ConfigService) {

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this
                .configService
                .getAuthenticationToken(),
        });
        this.options = new RequestOptions({ headers: this.headers });
        this.userInfo = this.configService.getLoggedInUserInfo();
        this.frontendUserInfo = this.configService.getFrontEndUserInfo();

    }

    getmyClaims() {
        this.loggedInProgramInfo = JSON.parse(localStorage.getItem("myChannelsClaimsList"));
        return this.loggedInProgramInfo;
    }

    setMyClaims(myClaims) {
        localStorage.setItem("myChannelsClaimsList", JSON.stringify(myClaims));

    }



    setMyChannelsChain(channelsChain) {
        this.channelsChain = channelsChain;
    }

    getMyChannelsChain(): any {
        return this.channelsChain;
    }

    setMyChannelsChainRequestObject(channelsChain) {
        this.channelsChainRequestObject = channelsChain;
    }

    getMyChannelsChainRequestObject(): any {
        return this.channelsChainRequestObject;
    }

    setMyChannelsBackRequestObject(channelsChain) {
        this.channelsBackRequestObject = channelsChain;
    }

    getMyChannelsBackRequestObject(): any {
        return this.channelsBackRequestObject;
    }

    setMyChannelsClaimsBackRequestObject(channelsChain) {
        this.channelsClaimsBackRequestObject = channelsChain;
    }

    getMyChannelsClaimsBackRequestObject(): any {
        return this.channelsClaimsBackRequestObject;
    }




    setDistributor(distributorInfo) {
        this.distributorInfo = distributorInfo;
    }
    getDistributor() {
        return this.distributorInfo;
    }

    setNewChannel(newChannel) {
        this.newChannel = newChannel;
    }
    getNewChannel() {
        return this.newChannel;
    }

    setMyChannelsProfile(profile) {
        // console.log("in setMyChannelsProfile in channelservice",profile);
        this.channelProfileInfo = profile;
        console.log("in setMyChannelsProfile in channelservice",this.channelProfileInfo);
    }

    getMyChannelsProfile(): any{
        console.log("in getMyChannelsProfile in channelservice",this.channelProfileInfo);
        return this.channelProfileInfo;
    }
    setCreditPartyId(creditPartyId) {
        this.creditPartyId = creditPartyId;
    }

    getCreditPartyId() {
        return this.creditPartyId;
    }


    setSelectedChannelCreditPartyId(creditPartyId) {
        this.selectedCreditPartyId = creditPartyId;
    }

    getSelectedChannelCreditPartyId() {
        return this.selectedCreditPartyId;
    }

    setMyChannelProfileForComingBack(profile) {
        console.log("profile", profile);
        this.ChannelProfileForComingBack = profile;
    }

    getMyChannelProfileForComingBack(): any {
        return this.ChannelProfileForComingBack;
    }

    setProfileForCodeValidation(channelForCodeVerification) {
        this.channelForCodeVerification = channelForCodeVerification;
    }
    getProfileForCodeValidation() {
        return this.channelForCodeVerification;
    }


    searchProduct(searchString: string = ''): Observable<any> {

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj: any = {
            "clientId": this.clientId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "serviceType": "client",
        };

        reqObj.searchProduct = searchString;

        let url = this.configService.getApiUrls().getAllProducts;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                let responseArr = res.json().result;
                return responseArr;
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }

    searchBrand(searchString: string = ''): Observable<any> {

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj: any = {
            "clientId": this.clientId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "serviceType": "client",
        };

        reqObj.searchBrand = searchString;

        let url = this.configService.getApiUrls().getAllBrands;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                let responseArr = res.json().result;

                return responseArr;
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }

    searchProgramScheme(searchString: string = ''): Observable<any> {

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.getAuthenticationToken()
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj: any = {
            "clientId": this.clientId,
            "programId": this.programId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
            "serviceType": "programSetup",
        };

        reqObj.searchName = searchString;

        let url = this.configService.getApiUrls().getProgramScheme;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                let responseArr = res.json().result;
                return responseArr;
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }
    getProduct(requestObj): any {
        requestObj.serviceType = "client";
        requestObj.clientId = this.clientId;

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getAllProducts;

        return this.http.post(url, requestObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getBrands(requestObj): any {
        requestObj.serviceType = "client";
        requestObj.clientId = this.clientId;

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let url = this.configService.getApiUrls().getAllBrands;


        return this.http.post(url, requestObj, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    updateSecondarySale(requestObject): any {
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        requestObject.serviceType = "programOperations";
        requestObject.clientId = this.clientId;

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().updateSecondarySale;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                // var a: any = "";
                // this.approval.next(a);
                return res.json();

            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    getMyChannels(requestObject): any {
        console.log("in channelservice get channels");
        // requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        // requestObject.serviceType = "programSetup";
        // requestObject.clientId = this.clientId;
        // requestObject.programId = this.programId;
        var userIds = this.userInfo.userId;
        var frontEndInfo = this.configService.getFrontEndUserInfo();
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }


        let reqObj = requestObject;

        // console.log("request object",requestObject);
        // reqObj.frontendUserInfo = frontEndInfo;
        console.log("req obj in channelservice",reqObj);

        let url = this.configService.getApiUrls().getMyChannels1;

        return this.http.post(url, reqObj, this.options)
            .map((res: Response) => {
                console.log("inside map RRRRRRRRR")
                return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    /*
       METHOD         : getAllUniqueCodes()
       DESCRIPTION    :Get all unique Code list.
   */
    getAllUniqueCodes(reqObj): any {

        // var frontEndInfo = this.frontendUserInfo;

        var frontEndInfo = this.configService.getFrontEndUserInfo();
        var userIds = this.userInfo.userId;

        // console.log("fR",frontEndInfo);       
        if (frontEndInfo == undefined) {
            frontEndInfo = {
                "userId": userIds
            }
        } else {

            frontEndInfo["userId"] = userIds;
        }

        reqObj.serviceType = "programSetup";
        reqObj.frontendUserInfo = frontEndInfo;
        reqObj.programId = this.configService.getloggedInProgramInfo().programId;
        reqObj.clientId = this.configService.getloggedInProgramInfo().clientId;
        console.log(reqObj);
        let url = this.configService.getApiUrls().getAllUniqueCodes;

        reqObj.serviceType = "programSetup";
        // console.log("Options", this.options);
        // console.log("inside getprogramuser", url, reqObj, this.options);
        return this
            .http
            .post(url, reqObj, this.options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }


    getAllOrder(requestObject): any {
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        console.log("Request Object In Get All Orders: ", requestObject);
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().addOrders;
        console.log("getAllOrder request object in service : ", requestObject);
        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    updateOrder(requestObject) {
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        requestObject.frontendUserInfo.userId = this.configService.loggedInProgramUserInfo.programUserId;
        console.log("get orders req obj ", requestObject);
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });

        let url = this.configService.getApiUrls().updateOrder;
        console.log("before entities before api call", options);

        return this.http.post(url, requestObject, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }

    returnSecondarySale(requestObject) {
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();
        console.log("get orders req obj ", requestObject);

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });

        let options = new RequestOptions({ headers: headers });

        let url = this.configService.getApiUrls().returnSecondarySale;
        console.log("before entities before api call", options);

        return this.http.post(url, requestObject, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || "Server error");
            });
    }
    getProgramScheme(requestObj): any {
        let requestObject: any = {};
        requestObject = requestObj;
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();;
        requestObject.programId = this.configService.getloggedInProgramInfo().programId;
        requestObject.clientId = this.configService.getloggedInProgramInfo().clientId;
        requestObject.serviceType = "programSetup";
        requestObject.frontendUserInfo.appType = "adminApp";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().getProgramScheme;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    updateUniqueCodes(requestObj): any {
        let requestObject: any = {};
        requestObject = requestObj;
        requestObject.frontendUserInfo = this.configService.getFrontEndUserInfo();;
        requestObject.programId = this.configService.getloggedInProgramInfo().programId;
        requestObject.clientId = this.configService.getloggedInProgramInfo().clientId;
        requestObject.serviceType = "programSetup";

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getApiUrls().updateUniqueCodes;

        return this.http.post(url, reqObj, options)
            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    // checkIfApprovedIsClicked() {
    //     let approval = this.approval;
    //     let a: any = "";
    //     approval.next(a);
    //     return;
    // }
}
