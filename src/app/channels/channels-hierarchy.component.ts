/*
	Author			:	Ravi Thokal
	Description		: 	Component for channels hierarchy
	Date Created	: 	14 Aug 2018
	Date Modified	: 	14 Aug 2018
*/

import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ConfigService } from "../shared-services/config.service";
import { ChannelsService } from "./channels.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component';
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";


@Component({
	templateUrl: './channels-hierarchy.component.html',
	styleUrls: [ './channels.component.css','../app.component.scss'],
	providers: [Device]

})
export class ChannelsHierarchyComponent implements OnInit {              
	private channelsFound: boolean = true;
	private showLoader: boolean = false;
	private loginInfo: any;
	private businessInfo: any;
	private channelsInfo: any = [];
	private isClient: boolean;
	public clientColor: any;
	public userType: any;
	public userId: any;
	public totalRecords:number;
	private limit: number;
	private skip: number;
	private serialSkip: number;
	public disablePrev: boolean = true;
	public disableNext: boolean = false;
	public pageCount: number = 1; //to maintain pagecount in pagination
	isPrevious: boolean = true;
	isNext: boolean = true;
	private channelRequestObject: any;
	private channelBackRequestObject: any;
	public channelChain: any = [];
	public math: any;
	public isbackEnable: boolean = false;
	private startRecord: number;
  	private endRecords: number;
	public isCountCanBeShown: boolean= false;	
	mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;

	programRoleInfo: any;
	programUserInfo: any;

	public requestObject = {
		"userInfo": {},
		"programUserId": this.configService.getloggedInProgramUser().programUserId,
		"clientId": this.configService.getloggedInProgramInfo().clientId,
		"programId": this.configService.getloggedInProgramInfo().programId,
		"frontendUserInfo": this.configService.getFrontEndUserInfo(),
		"serviceType": "programSetup",
		"skip": 0,
		"limit": 10
	};


	constructor(private router: Router,
		private channelsservice: ChannelsService,
		public sidebar: SidebarComponent,
		private configService: ConfigService,
    	private stringService: StringService,
		private device: Device,
	) {
		this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
			// document.getElementById("desktop-div").style.visibility = "visible";
			// document.getElementById("mobile-div").style.visibility = "hidden";
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;
		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}

		this.limit = 10;
		this.skip = 0;
		this.serialSkip = 0;

		// this.math = Math;
		this.loginInfo = configService.getLoggedInUserInfo();
		this.businessInfo = configService.getloggedInBEInfo();
		this.channelChain = configService.getMyChannelsChain();
		// let chain = {
		// 	userId: this.userId,
		// 	userType: this.userType,
		// 	userName: userDetail.userName
		// }
		console.log("channel chain", this.channelChain);
		this.clientColor = this.configService.getThemeColor();



		// this.isClient = false;
		// // delete backObject.userId;
		// console.log(backObject);
		// this.getMyChannels(backObject);


	}

	ngOnInit() {

		if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Channels")
        } else {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					ga('set', 'page', "Channels");
					ga('send', 'pageview');
				}
			});
		}
		
		this.sidebar.close();
		console.log("in ng on init RRRR");
		// this.getMychannels(requestObject);
		if (this.channelsservice.channelBack == false) {
			this.channelChain = [];
			if (this.loginInfo.clientId === undefined || this.loginInfo.clientId === null) {
				console.log('Not a client');
				this.isClient = false;

				this.requestObject.userInfo = {
					"userId": this.configService.getloggedInProgramUser().userDetails.userId,
					"userType": "ChannelPartner"
				}
                console.log("RRR in req obj ASm login in IF",this.configService.getloggedInProgramUser().userDetails.userId);
				let chain = {
					userId: this.businessInfo.businessId,
					userType: "ChannelPartner",
					userName: this.businessInfo.businessName
				}

					
				this.programUserInfo = this.configService.getloggedInProgramUser();
				if (this.programUserInfo) {
					this.programRoleInfo = this.programUserInfo.programRoleInfo;
					console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
				  }


				if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
					this.requestObject.userInfo = {
						"userId": "CU1519711408498",
						"userType": "ClientUser"
					}
				}
				this.channelChain.push(chain);
				this.configService.setMyChannelsChain(this.channelChain);
                // this.showLoader = true;
				this.getMychannels(this.requestObject);
			} else {
				this.channelChain = [];
				this.isClient = true;
				console.log('client');
				this.requestObject.userInfo = {
					"userType": "ClientUser",
					"userId": this.loginInfo.clientUserId,
				}
				console.log('rq', this.requestObject);
				let chain = {
					userType: "ClientUser",
					userName: this.loginInfo.fullName,
					userId: this.loginInfo.clientUserId
				};

				console.log(chain, "Chains");
				this.channelChain.push(chain);
				this.configService.setMyChannelsChain(this.channelChain);
				this.requestObject.userInfo = {
					"userId": this.configService.getloggedInProgramUser().userDetails.userId,
					"userType": "ChannelPartner"
				}

				this.programUserInfo = this.configService.getloggedInProgramUser();
				if (this.programUserInfo) {
					this.programRoleInfo = this.programUserInfo.programRoleInfo;
					console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
				  }

				console.log("this.programUserInfo.programRole",this.programUserInfo.programRole);
				console.log("")
				if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
					this.requestObject.userInfo = {
						"userId": "CU1519711408498",
						"userType": "ClientUser"
					}
				}

				console.log("RRR in req obj ASm login in 1st else",this.configService.getloggedInProgramUser().userDetails.userId);
				this.getMychannels(this.requestObject);

			}


		} else {
            console.log("RRRRRR test on main pg");
			this.channelsservice.channelBack = false;
			console.log("Back is true");
			var backObject = this.channelsservice.getMyChannelsBackRequestObject();
			if (backObject.userType === "ChannelPartner") {

				this.requestObject.userInfo = {
					"userId": backObject.userId,
					"userType": "ChannelPartner"
				}
				this.getMychannels(this.requestObject);
			}
			if (backObject.userType === "ClientUser") {
				this.requestObject.userInfo = {
					"userType": "ClientUser",
					"userId": backObject.userId,
				}
				this.requestObject.userInfo = {
					"userId": this.configService.getloggedInProgramUser().userDetails.userId,
					"userType": "ClientUser"
				}
					
				this.programUserInfo = this.configService.getloggedInProgramUser();
				if (this.programUserInfo) {
					this.programRoleInfo = this.programUserInfo.programRoleInfo;
					console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
				  }


				if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
				|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
					this.requestObject.userInfo = {
						"userId": "CU1519711408498",
						"userType": "ClientUser"
					}
				}
				console.log("RRR in req obj ASm login in 2nd else",this.configService.getloggedInProgramUser().userDetails.userId);

				this.getMychannels(this.requestObject);

			}

		}

	}

	viewChannel(userDetail, userType) {
		this.showLoader = true;
		this.isbackEnable = true;
		this.channelsInfo = [];
		// if (this.showLoader == false) {
		// 	// this.channelChain = [];
		// 	this.showLoader = true;
		// this.channelChain = [];
		console.log("userDetail", userDetail);
		this.userId = userDetail.userId;
		this.userType = userType;
		console.log(this.userId);
		console.log(this.userType);
		let chain = {
			userId: this.userId,
			userType: this.userType,
			userName: userDetail.userName
		}

		this.channelChain.push(chain);

		this.configService.setMyChannelsChain(this.channelChain);
		this.configService.getMyChannelsChain();
		console.log("RRRRR channelc chain", this.channelChain);
		this.configService.setMyChannelsBackRequestObject(this.channelChain[this.channelChain.length - 2]);
		this.configService.getMyChannelsBackRequestObject();

		this.requestObject.userInfo = {
			"userId": this.userId,
			"userType": this.userType
		}

		this.requestObject.skip = 0;
		this.pageCount = 1;
		this.serialSkip = 0
		this.limit = 10;
		this.skip = 0;
		this.serialSkip = 0;

		console.log('qqq', this.requestObject);
		this.getMychannels(this.requestObject);

		// }

	} 

	showChainChannels(chain, index) {
		this.showLoader = true;
		// console.log("in show channel chain",chain);
		// console.log("in show channel chain",index);
		
			this.channelChain.splice(index + 1);
			console.log("chain in show  RRRR",this.channelChain);
			this.channelsservice.setMyChannelsChain(this.channelChain);
			this.userId = chain.userId;
			this.userType = chain.userType;
			this.requestObject.userInfo = {
				"userId": this.userId,
				"userType": this.userType
			}

			if (this.programUserInfo) {
				this.programRoleInfo = this.programUserInfo.programRoleInfo;
				console.log("PROGRAM ROLE INFO [HOME COMPONENT] : ", this.programRoleInfo);
			  }
	
			console.log("this.programUserInfo.programRole",this.programUserInfo.programRole);
			console.log("")
			if(this.programUserInfo.programRole === this.stringService.getStaticContents().jswDistributorAdminProgramRole 
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswEngineerAdminProgramRole 
			|| this.programUserInfo.programRole === this.stringService.getStaticContents().jswDealerAdminProgramRole){
				console.log("chain reqObj this.userId",this.userId);
				console.log("this.loginInfo.clientUserId",this.loginInfo.clientUserId);
				if( this.userId === this.loginInfo.clientUserId)
				{	
					this.requestObject.userInfo = {
						"userId": "CU1519711408498",
						"userType": "ClientUser"
					}
				}
			}

			this.requestObject.skip = 0;
			this.pageCount = 1;
			this.serialSkip = 0
			this.limit = 10;
			this.skip = 0;
			this.serialSkip = 0;
			this.getMychannels(this.requestObject);

	}

	backToSalesFlow() {
		this.showLoader = true;
		this.isbackEnable = false;
		this.channelsservice.channelBack = true;
		this.router.navigate(['./home/ChannelsComponent']);
	}

	viewProfile(channel) {
		this.showLoader = true;
		let requestObjectChannel = {
			userInfo: {
				"userId": channel.programUserId,
				"userType": channel.userType
			}
		};
		console.log('chan', channel);
		if (channel.userType === "ChannelPartner") {
			console.log('channel.parentUsersInfo', channel.parentUsersInfo);
			this.channelsservice.setDistributor(channel.parentUsersInfo);
		} else {
			this.channelsservice.setDistributor([]);
		}

		console.log("when clickview profile RRRRRRR", channel);
		this.configService.setMyChannelsProfile(channel);
		this.channelsservice.setMyChannelProfileForComingBack(channel);
		this.configService.setMyChannelsChainRequestObject(requestObjectChannel);
		this.configService.setMyChannelsBackRequestObject(this.channelChain[this.channelChain.length - 1]);
		this.router.navigate(["./home/channel-profile-details"]);

	}


	getMychannels(requestObj) {
		this.showLoader = true;
		console.log("RRRRRRRR", this.requestObject);
		this.channelRequestObject = this.configService.getMyChannelsChainRequestObject();
		this.channelBackRequestObject = this.configService.getMyChannelsBackRequestObject();
		console.log("in ng on getMyChannels RRRR", this.requestObject);


		this.channelsservice.getMyChannels(this.requestObject).subscribe(responseObject => {
			if (responseObject.statusCode == 0) {
				this.showLoader = false;
				console.log("in res my channel", responseObject);
				this.channelsInfo = responseObject.result;
				this.channelsservice.setNewChannel(this.requestObject);
				console.log("Response Object getMychannels : ", this.channelsInfo);
				this.channelsFound = true;
				this.totalRecords = responseObject.count;
				console.log("totalRecords", this.totalRecords);
				console.log("this.pageCount * this.limit", this.pageCount * this.limit);
				console.log()

				if (this.totalRecords < this.pageCount * this.limit) {
					this.isNext = true;
				} else {
					this.isNext = false;
				}

				//Disable next button if total records are equal to pagination limit
				if (this.totalRecords === this.pageCount * this.limit) {
					this.isNext = true;
				}
				console.log("this.pageCount", this.pageCount);
				if (this.pageCount == 1) {
					this.isPrevious = true;
				} else {
					this.isPrevious = false;
				}
				console.log("this.isNext", this.isNext);
				console.log("this.isPrevious", this.isPrevious);
				if (this.channelsInfo.length > 0) {
					this.isCountCanBeShown = true;
					if (this.pageCount > 1) {
					  this.startRecord =
						this.pageCount * this.limit - (this.limit - 1);
					  this.endRecords =
						this.startRecord + (responseObject.result.length - 1);
					} else {
					  this.startRecord = 1;
					  this.endRecords =
						this.startRecord + (responseObject.result.length - 1);
					}
				  } else {
					// this.isCountCanBeShown = false;
				  }
			}
			else if (responseObject.statusCode == 2) {
				this.showLoader = false;
				console.log("channel not found");
				this.channelsFound = false;
			}
		});

	}

	onPrev() {
		this.showLoader = true;
		console.log("IN previous");
		this.requestObject.skip = this.requestObject.skip - this.limit;
		this.requestObject.limit = this.limit;
		this.pageCount--;
		this.serialSkip = this.requestObject.skip;
		this.getMychannels(this.requestObject);
	}

	onNext() {
		this.showLoader = true;
		console.log("In next");
		this.requestObject.skip = this.requestObject.skip + this.limit;
		this.requestObject.limit = this.limit;
		this.pageCount++;
		this.serialSkip = this.requestObject.skip;
		this.getMychannels(this.requestObject);
	}
}

