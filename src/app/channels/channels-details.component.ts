/*
	Author			:	Ravi Thokal
	Description		: 	Component for channels hierarchy
	Date Created	: 	14 Aug 2018
	Date Modified	: 	14 Aug 2018
*/

import { Component, OnInit, ViewChild, Input, NgZone } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { MdDialog, MdDialogRef } from '@angular/material'; 
import { ConfigService } from "../shared-services/config.service";
import { ChannelsService } from "./channels.service";
import { StringService } from "../shared-services/strings.service";
import { GoogleAnalyticsEventsService } from "../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../footer/app-footer.component';
import { ClientFooterComponent } from '../footer/client-footer.component';
import { ClaimsService } from "../claims/claims.service";
import { PlatformLocation } from '@angular/common'
// import { FeedbackMessageComponent } from "../feedback-message.component";
//import { FeedbackMessageComponent } from '../shared-components/feedback-message.component';
import { SidebarComponent } from '../home/sidebar/sidebar.component'



import { InvoiceClaimsOperationsComponent } from "./channels-add-claims/invoice-claims-operations.component";
import { DigitalClaimsOperationsComponent } from "./channels-add-claims/digital-claims-operations.component";
import { CodeClaimsOperationsComponent } from "./channels-add-claims/code-claims-operations.component";
import { ChannelsRedemptionDetailComponent } from "./channels-redempton-detail";

import { LeadManagementService } from "../lead-management-system/lead-management.service";

import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
  templateUrl: './channels-details.component.html',
  styleUrls: ['./channels.component.css', '../app.component.scss'],
  providers: [Device]
})
export class ChannelsDetailsComponent implements OnInit {
  mobilewidth: any;
	displaypc: boolean = true;
	displaymobile: boolean = false;

  previousChannelsInfo: any;
  private ahowActionPopup: boolean = false;
  private addClaimRoute: string;
  private programUserArray: any;
  public math: any;
  public showClaimsTable: boolean = true;
  public showRRTable: boolean = false;
  public showReturnsTable: boolean = false;

  private showClaimsUI: boolean = true;
  private showPointsUI: boolean = false;
  private showApprovalUI: boolean = false;
  private active: string = "claims";
  private channelRequestObject: any;
  private channelBackRequestObject: any;
  private channelsFound: boolean = false;
  private noChannelsFound: boolean = false;
  private parentUserId: string;

  // Claims Declaration 
  private profileInfo: any;
  private redeemRequestsArr: any = [];
  private myClaimsInfo: any = null;
  private showLoader: boolean = false;
  private noOfClaims: number;
  private noOfRedeems: number;
  public filterMyClaimForm: FormGroup;
  public filterReedemRequestsForm: FormGroup;
  public requestObj: any = {};
  public filterName: string;
  public statusFilterName: string;
  public statusFilterName2: string;

  public timeline: string;

  private limit: number = 10;
  private skip: number = 0;
  private serialSkip: number;
  public disablePrev: boolean = true;
  public disableNext: boolean = true;

  public isStartDate: boolean = false;
  public isEndDate: boolean = false;

  public fromTo: boolean = false;
  public noRecordMessage: boolean = false;

  public showInvoice: boolean = false;
  public showIVP: boolean = false; // SHOW INVOICE VALUE PRODUCT
  public showIQP: boolean = false; // SHOW INVOICE Quantity PRODUCT
  public showIBP: boolean = false; // SHOW INVOICE Both PRODUCT
  public showCodeTable: boolean = false; //SHOW CODE 

  public programEndDate: string;
  public programStartDate: string;
  public currentStartDate: string;
  public redeemProgramStartDate: string;
  public redeemcurrentStartDate: string;

  public showDVP: boolean = false; // SHOW Digital VALUE PRODUCT
  public showDQP: boolean = false; // SHOW Digital Quantity PRODUCT
  public showDBP: boolean = false; // SHOW Digital Both PRODUCT


  public showDigital: boolean = false;
  public showBoth: boolean = false;

  private channelLimit: number = 10;
  private channelSkip: number = 0;
  public channelPageCount: number = 1; //to maintain pagecount in pagination
  isPrevious: boolean = true;
  isNext: boolean = true;
  public channelstartRecord:number;
  public channelendRecord:number;
  public pageCount: number = 1; //to maintain pagecount in pagination
  public date: Date = new Date();
  currentMonth = this.date.getMonth();
  public totalClaimsCount: number;
  public totalRedeemsCount: number;
  private startDate: any;
  private endDate: any;
  private sdate: any;
  private edate: any;
  private creditPartyId: string;
  private filterStartDate: string;
  private filterEndDate: string;
  private approvalStatus: string;
  private orderStatus: string;
  private isReturn: boolean;
  public clientColor: any;
  private programUserInfo: any = {};
  private businessInfo: any = {};
  private channelsInfo: any;
  private canClaimForChild: boolean = false;
  private roleInfo: any;
  noOfSubordinates: number;
  private requestObject: any = {
    programId: "",
    clientId: "",
    creditPartyId: "",
    parentUserId: "",
    frontendUserInfo: {},
    skip: 0,
    limit: 10

  }

  private requestObjectRedeem: any = {
    sort: { createdAt: -1 },
    customerId: "",
    programId: "",
    frontendUserInfo: {},
    skip: 0,
    limit: 10
  }

  public returnRequestObject: any = {};

  // for leads
  public noRecordMessage1: boolean = false;
  public leadDuration;
  public leadRequestObject: any = {};
  private summarylist: any[];
  private leadlimit: number =10;
  private leadisPrevious: boolean = false;
  private leadisNext: boolean = false;
  private isCountCanBeShown: boolean;
  private leadpageCount: number = 1; //to maintain pagecount in pagination
  private totalRecords: number;
  private startRecord: number;
  private endRecord: number;
  private leadskip: number =0;
  public FilterForm: FormGroup;
  isTimePeriod: boolean = false;
  isNoRecords: boolean;
  generateCsv: boolean;
  headerArray: string[];
  printingArray: any[];
  fromToDateInvalid: boolean;
  public showLeadTable: boolean;
  public showLead: boolean = false;
  public leadfilterName:string;
  public leadstatusFilterName:string;
  public leadtotalRecords: number = 0;
  public leadtimeline: string;
  public leadcurrentStartDate;
  public leadisStartDate;
  public leadisEndDate;
  public leadserialSkip: number;
  public channelTotalRecords:number;
  public showTables: boolean = true;

  public claimDetail;
  public purchaseDetails;
  public paymentStatus;
  public showDetail;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService,
    private channelsService: ChannelsService,
    private stringService: StringService,
    public dialog: MdDialog,
    private formBuilder: FormBuilder,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private claimsService: ClaimsService,
    private location: PlatformLocation,
    public sidebar: SidebarComponent,
    private leadService: LeadManagementService,
    // private zone: NgZone
	private device: Device,


  ) {
    this.mobilewidth = window.screen.width;
		console.log("Width:", this.mobilewidth);
		if (this.mobilewidth <= 576) {
			this.displaypc = false;
			this.displaymobile = true;
			// document.getElementById("desktop-div").style.visibility = "visible";
			// document.getElementById("mobile-div").style.visibility = "hidden";
		}
		else if (this.mobilewidth > 576 && this.mobilewidth <= 825) {
			this.displaypc = false;
			this.displaymobile = true;

		}
		else {
			this.displaypc = true;
			this.displaymobile = false;
		}
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    this.clientColor = this.configService.getThemeColor();
    this.location.onPopState(() => {
      this.channelsService.channelBack = true;
      this.router.navigate(['./home/channels/sales-flow']);

      console.log('pressed back!');

    });


    this.math = Math;
    this.isReturn = false;


    // console.log("profile info", this.profileInfo);
  }

  ngOnInit() {

    if(this.device.device === "android" || this.device.device === "ios"){
          console.log("inside android and ios");
          (<any>window).ga.trackView("Channels-details")
      } else {
		this.router.events.subscribe(event => {
		if (event instanceof NavigationEnd) {
			ga('set', 'page', "Channels-details");
			ga('send', 'pageview');
		}
		});
    }

    this.sidebar.close();
    console.log("msg RRRR", this.stringService.getStaticContents().myClaimsViewFor);
    this.showClaimsTable = true;
    this.profileInfo = this.configService.getMyChannelsProfile();
    this.creditPartyId = this.profileInfo.programUserId;
    this.parentUserId = this.profileInfo.programUserId;
    console.log("PROFILE info  RRRRRRRRRRRRRR", this.profileInfo.userDetails.userName);
    console.log("this.profileInfo", this.profileInfo);

    // to hide tables in case of ASM,CM,RCM,distributor
    if(this.profileInfo.programRole != this.stringService.getStaticContents().jswEngineerProgramRole && 
    this.profileInfo.programRole != this.stringService.getStaticContents().jswDealerProgramRole){
      this.showTables = false;
      this.showClaimsTable = false;
      this.showLeadTable = false;
      this.showRRTable = false;
    }

    if(this.profileInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole){
      this.showLead = true;
    }
    this.programUserArray = this.configService.getloggedInProgramUser();
    this.programUserInfo = this.configService.getloggedInProgramInfo();
    console.log("userDetaisl of channel partner Id", this.configService.getMyChannelsProfile());
    // this.configService.setSelectedChannelCreditPartyId(this.configService.getMyChannelsProfile().userDetails.userId);
    // this.setAddClaimRoutes();
    ///List  Claims Starts 

    this.channelRequestObject = {
      "userInfo": {},
      "programUserId": this.configService.getloggedInProgramUser().programUserId,
      "clientId": this.programUserInfo.clientId,
      "programId": this.programUserInfo.programId,
      "frontendUserInfo": {
        "userId": "",
        "appType": "adminApp"
      },
      "serviceType": "programSetup",
      "skip": 0,
      "limit": 10
    };
    console.log("this.profileInfo.userDetails AAAA", this.profileInfo.userDetails);
    this.requestObjectRedeem = {
      sort: { createdAt: -1 },

      customerId: this.profileInfo.userDetails.userId,
      programId: this.programUserInfo.programId,
      // clientId: this.programUserInfo.clientId,
      frontendUserInfo: {},
      skip: 0,
      creditPartyId: this.creditPartyId,
      limit: this.limit,
      sdate: this.sdate,
      edate: this.edate,
      orderStatus: this.orderStatus,
      // parentUserId: this.parentUserId
    }
    console.log("RRRRRRRRRR,", this.programUserInfo);
    // INVOICE PRODUCT VALUE
    if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
      console.log("THIS IS TRUE 1");
      this.showIVP = true;
    }
    // INVOICE BRAND VALUE
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
      this.showIVP = true;
      console.log("THIS IS TRUE 2");
    }
    // INVOICE PRODUCT BOTH  
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
      this.showIVP = true;
      console.log("THIS IS TRUE 3");
    }
    // INVOICE BRAND BOTH
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
      this.showIVP = true;
      console.log("THIS IS TRUE 4");
    }
    // INVOICE PRODUCT QUANTITY  
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
      this.showIQP = true;
      console.log("THIS IS TRUE 5");
    }
    // INVOICE BRAND QUANTITY
    else if (this.programUserInfo.claimEntryType === "invoice" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
      this.showIQP = true;
      console.log("THIS IS TRUE 6");
    }

    // DIGITAL PRODUCT VALUE
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "product") {
      this.showDVP = true;
      console.log("THIS IS TRUE 7");
    }
    // DIGITAL BRAND VALUE
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "value" && this.programUserInfo.basedOn === "brand") {
      this.showDVP = true;
      console.log("THIS IS TRUE 8");
    }

    // DIGITAL PRODUCT BOTH
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "product") {
      this.showDVP = true;
      console.log("THIS IS TRUE 9");
    }

    // DIGITAL BRAND BOTH
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "both" && this.programUserInfo.basedOn === "brand") {
      this.showDVP = true;
      console.log("THIS IS TRUE 10");
    }

    // DIGITAL PRODUCT QUANTITY
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "product") {
      this.showDQP = true;
      console.log("THIS IS TRUE  11");
    }
    // DIGITAL BRAND QUANTITY
    else if (this.programUserInfo.claimEntryType === "digital" && this.programUserInfo.pointCalculationType === "quantity" && this.programUserInfo.basedOn === "brand") {
      this.showDQP = true;

      console.log("THIS IS TRUE 12", this.myClaimsInfo);
    }
    else if (this.programUserInfo.claimEntryType === "code") {
      this.showCodeTable = true;
      console.log("THIS IS TRUE 13");
    }
    this.profileInfo = this.configService.getMyChannelsProfile();

    this.roleInfo = this.configService.getloggedInProgramUser();
    // console.log("PROFILE info  RRRRRRRRRRRRRR", this.profileInfo.userDetails.userName);
    this.parentUserId = this.roleInfo.programUserId;
    if (this.roleInfo.programRoleInfo.canClaimForChild == true) {
      this.canClaimForChild = true;
    } else {
      this.canClaimForChild = false;
    }





    this.channelBackRequestObject = this.configService.getMyChannelsBackRequestObject();
    console.log("channelRequestObject RRRRRRRRR ", this.channelRequestObject);
    console.log("channelBackRequestObject RRRRRRR", this.channelBackRequestObject);
    this.getMyChannels(this.channelRequestObject);


    console.log("profile info", this.profileInfo);

    // List  Claims Starts
    this.limit = 10;
    this.skip = 0;
    this.serialSkip = 0;
    this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
    var start = this.date.getUTCFullYear() + "-" + ((this.date.getUTCMonth() + 1) - 1) + "-" + this.date.getUTCDate();
    this.creditPartyId = this.profileInfo.programUserId;


    this.sdate = new Date(start);
    this.startDate = new Date(start);

    var end = this.date.getUTCFullYear() + "-" + (this.date.getUTCMonth() + 1) + "-" + this.date.getUTCDate();

    this.approvalStatus = "";
    this.orderStatus = "";
    this.edate = new Date(end).toLocaleDateString();
    this.endDate = new Date(end).toLocaleDateString();
    this.filterName = " This Month";
    this.statusFilterName = "All";
    this.statusFilterName2 = "All";
    this.businessInfo = this.configService.getloggedInBEInfo();



    this.programUserInfo = this.configService.getloggedInProgramInfo();
    this.filterMyClaimForm = this.formBuilder.group({
      "showResults": ["10"],
      "next": [""],
      "previous": [""],
      "startDate": [""],
      "endDate": [""]
    });
    this.filterReedemRequestsForm = this.formBuilder.group({
      "redeemShowResults": ["10"],
      "redeemNext": [""],
      "redeemPrevious": [""],
      "redeemStartDate": [""],
      "redeemEndDate": [""]
    });

    console.log("this.profileInfo.programRole", this.profileInfo.programRole);
    console.log("this.stringService.getStaticContents().jswDealerProgramRole", this.stringService.getStaticContents().jswDealerProgramRole);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.showClaimsTable = true;
      this.requestObject = {
        programId: this.programUserInfo.programId,
        clientId: this.programUserInfo.clientId,
        frontendUserInfo: {},
        skip: 0,
        creditPartyId: this.profileInfo.programUserId,
        limit: this.limit,
        startDate: this.startDate,
        endDate: this.endDate,
        approvalStatus: this.approvalStatus,
        parentUserId: this.profileInfo.programUserId
      }

      this.getAllTheSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }

    this.filterMyClaimForm.valueChanges.subscribe(data => {
      console.log("in fiter my claim RRRRRRR");
      this.requestObj = {};
      this.requestObj.programId = this.programUserInfo.programId;
      this.requestObj.clientId = this.programUserInfo.clientId;
      this.requestObj.creditPartyId = this.creditPartyId;
      this.requestObj.frontendUserInfo = {};
      this.requestObj.skip = this.requestObject.skip;
      this.requestObj.limit = this.requestObject.limit;
      this.requestObj.startDate = this.requestObject.startDate;
      this.requestObj.endDate = this.requestObject.endDate;
      this.requestObj.approvalStatus = this.requestObject.approvalStatus;
      this.requestObj.parentUserId = this.requestObject.parentUserId;

      if (data.showResults != null && data.showResults != "") {
        this.requestObj.limit = parseInt(data.showResults);
        this.limit = parseInt(data.showResults);
        this.requestObject.limit = parseInt(data.showResults);

        this.pageCount = 1;
        this.requestObj.startDate;
        this.requestObj.endDate;
      } else if (data.startDate != null && data.startDate != "") {

        this.requestObj.limit = parseInt(data.showResults);
        this.limit = parseInt(data.showResults);
        this.requestObject.limit = parseInt(data.showResults);
        this.requestObj.startDate = data.startDate;
        this.requestObj.endDate = data.endDate;
        this.startDate = data.startDate;
        this.startDate = new Date(this.startDate);
        this.requestObject.startDate = data.startDate;
        this.endDate = data.endDate;
        this.endDate = new Date(this.endDate);
        this.requestObject.endDate = data.endDate;
        this.pageCount = 1;
        this.isStartDate = true;


      }

      if (data.endDate != null && data.endDate != "") {

        this.requestObj.limit = parseInt(data.showResults);
        this.limit = parseInt(data.showResults);
        this.requestObject.limit = parseInt(data.showResults);
        this.requestObj.startDate = data.startDate;
        this.requestObj.endDate = data.endDate;
        this.startDate = data.startDate;
        this.requestObject.startDate = data.startDate;
        this.endDate = data.endDate;
        this.requestObject.endDate = data.endDate;
        this.pageCount = 1;
        this.isEndDate = true;
      }

      if (this.isStartDate && this.isEndDate) {

        var startDate = new Date(this.startDate);
        var endDate = new Date(this.endDate);
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();

      }

      var startDate = new Date(this.startDate);
      var endDate = new Date(this.endDate);
      if (this.requestObj.startDate != undefined || this.requestObject.endDate != undefined) {
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
      } else {
        this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
      }


      console.log("IN VALUE CHANGE in filter RRRRRRR", this.requestObj);

      // make get claims api call in case of dealer
      if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
        this.getAllSecondarySales(this.requestObj);
      } else {
        this.noRecordMessage = true;
      }

      // this.getAllSecondarySales(this.requestObj);


    });

    this.filterReedemRequestsForm.valueChanges.subscribe(data => {
      this.requestObjectRedeem = {};
      this.requestObjectRedeem.sort = { createdAt: -1 };
      this.requestObjectRedeem.programId = this.programUserInfo.programId;
      this.requestObjectRedeem.clientId = this.programUserInfo.clientId;
      // this.requestObjectRedeem.creditPartyId = this.creditPartyId;
      this.requestObjectRedeem.frontendUserInfo = {};
      this.requestObjectRedeem.skip = this.requestObjectRedeem.skip;
      this.requestObjectRedeem.limit = this.requestObjectRedeem.limit;
      this.requestObjectRedeem.sdate = this.requestObjectRedeem.sdate;
      this.requestObjectRedeem.edate = this.requestObjectRedeem.edate;
      this.requestObjectRedeem.orderStatus = this.requestObjectRedeem.orderStatus;
      // this.requestObjectRedeem.parentUserId = this.requestObjectRedeem.parentUserId;

      if (data.redeemShowResults != null && data.redeemShowResults != "") {
        this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
        this.limit = parseInt(data.redeemShowResults);
        this.requestObjectRedeem.skip=0;
        this.skip=0
        this.pageCount = 1;
        this.requestObjectRedeem.sdate;
        this.requestObjectRedeem.edate;
      } else if (data.redeemStartDate != null && data.redeemStartDate != "") {

        this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
        this.limit = parseInt(data.redeemShowResults);
        this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
        this.requestObjectRedeem.sdate = data.redeemStartDate;
        this.requestObjectRedeem.edate = data.redeemEndDate;
        this.sdate = data.redeemStartDate;
        this.sdate = new Date(this.sdate);
        this.requestObjectRedeem.sdate = data.redeemStartDate;
        this.edate = data.edate;
        this.edate = new Date(this.edate);
        this.requestObjectRedeem.edate = data.redeemEndDate;
        this.pageCount = 1;
        this.isStartDate = true;


      }

      if (data.redeemEndDate != null && data.redeemEndDate != "") {

        this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
        this.limit = parseInt(data.redeemShowResults);
        this.requestObjectRedeem.limit = parseInt(data.redeemShowResults);
        this.requestObjectRedeem.sdate = data.redeemStartDate;
        this.requestObjectRedeem.edate = data.redeemEndDate;
        this.sdate = data.redeemStartDate;
        this.requestObjectRedeem.startDate = data.redeemStartDate;
        this.edate = data.redeemEndDate;
        this.requestObjectRedeem.edate = data.redeemEndDate;
        this.pageCount = 1;
        this.isEndDate = true;
      }

      if (this.isStartDate && this.isEndDate) {

        var startDate = new Date(this.sdate);
        var endDate = new Date(this.edate);
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();

      }

      var startDate = new Date(this.sdate);
      var endDate = new Date(this.edate);
      if (this.requestObjectRedeem.sdate != undefined || this.requestObjectRedeem.edate != undefined) {
        this.timeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
          " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
      } else {
        this.timeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
      }
      console.log("IN VALUE CHANGE", this.requestObjectRedeem);
      
      if(this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole || this.profileInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole){
        this.getAllOrder(this.requestObjectRedeem);
      } else {
        this.noRecordMessage = true;
      }
    });

    this.FilterForm = this.formBuilder.group({
        showResults: ["10"],
        timeFilter: [""],
        startDate: [""],
        endDate: [""],
        statusFilter: [""]
      });
  
      this.FilterForm.valueChanges.subscribe(data => {
        console.log("data",data);
        if (data.showResults != null && data.showResults != "") {
          console.log("in showResults",data.showResults );
          this.leadRequestObject.limit = parseInt(data.showResults);
          this.leadlimit = parseInt(data.showResults);
          this.leadRequestObject.limit = parseInt(data.showResults);
  
          this.leadpageCount = 1;
        }
        else if (data.timeFilter === "thisMonth") {
          this.isTimePeriod = false;
          var endDate = new Date();
          endDate.setDate(endDate.getDate() + 1);
          var startDate = new Date();
          startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
          this.leadRequestObject.startDate = startDate.toISOString();
          this.leadRequestObject.endDate = endDate.toISOString();
          delete this.leadRequestObject.isPending;
          delete this.leadRequestObject.isQualified;
          this.leadDuration = null;
        }
        if (data.timeFilter === "lastMonth") {
          this.isTimePeriod = false;
          var endDate = new Date();
          endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1);
          var startDate = new Date(endDate);
          startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
          this.leadRequestObject.startDate = startDate.toISOString();
          this.leadRequestObject.endDate = endDate.toISOString();
          delete this.leadRequestObject.isPending;
          delete this.leadRequestObject.isQualified;
          this.leadDuration = null;
        }
        if (data.timeFilter === "selectTimePeriod") {
          this.isTimePeriod = true;
          this.leadDuration = null;
          this.thisSelectTimePeriod();
          delete this.leadRequestObject.isPending;
          delete this.leadRequestObject.isQualified;
        }
        if (data.timeFilter === "all" || data.timeFilter === "") {
          this.isTimePeriod = false;
          delete this.leadRequestObject.startDate;
          delete this.leadRequestObject.endDate;
          delete this.leadRequestObject.isPending;
          delete this.leadRequestObject.isQualified;
          this.leadDuration = null;
        }
        // if (data.statusFilter !== "" && data.statusFilter !== null && data.statusFilter !== undefined && data.statusFilter !== "all") {
        //   if (data.statusFilter === "qualified") {
        //     this.requestObj.qualified = true;
        //     this.requestObj.rejected = false;
        //     this.requestObj.pending = false;
        //   }
        //   if (data.statusFilter === "pending") {
        //     this.requestObj.qualified = false;
        //     this.requestObj.rejected = false;
        //     this.requestObj.pending = true;
        //   }
        //   if (data.statusFilter === "rejected") {
        //     this.requestObj.qualified = false;
        //     this.requestObj.rejected = true;
        //     this.requestObj.pending = false;
        //   }
        // } else {
        //   delete this.requestObj.qualified;
        //   delete this.requestObj.rejected;
        //   delete this.requestObj.pending;
        // }

        // New code
        if (data.startDate != null && data.endDate != "") {
          console.log("in start date",data.startDate);
          this.leadRequestObject.limit = parseInt(data.showResults);
          this.limit = parseInt(data.showResults);
          this.leadRequestObject.limit = parseInt(data.showResults);
          this.leadRequestObject.startDate = data.startDate;
          this.leadRequestObject.endDate = data.endDate;
          this.leadpageCount = 1;
          this.leadisStartDate = true;
  
  
        }
  
        if (data.endDate != null && data.endDate != "") {
          console.log("in end date",data.endDate);
          this.leadRequestObject.limit = 10;
          this.limit = parseInt(data.showResults);
          this.leadRequestObject.limit = 10;
          this.leadRequestObject.startDate = data.startDate;
          this.leadRequestObject.endDate = data.endDate;
          this.leadpageCount = 1;
          this.leadisEndDate = true;
        }
  
        if (this.leadisStartDate && this.leadisEndDate) {
  
          var startDate = new Date(this.sdate);
          var endDate = new Date(this.edate);
          this.leadtimeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth() + 1) + " " + startDate.getFullYear() +
            " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth() + 1) + " " + endDate.getFullYear();
  
        }
  
        var startDate = new Date(this.sdate);
        var endDate = new Date(this.edate);
        if (this.requestObjectRedeem.sdate != undefined || this.requestObjectRedeem.edate != undefined) {
          this.leadtimeline = startDate.getDate() + " " + this.getMonthName(startDate.getMonth()) + " " + startDate.getFullYear() +
            " to " + endDate.getDate() + " " + this.getMonthName(endDate.getMonth()) + " " + endDate.getFullYear();
        } else {
          this.leadtimeline = this.getMonthName(this.currentMonth) + " " + this.date.getFullYear();
        }
         // New 
  
        if (this.isTimePeriod === true) {
          if (!this.FilterForm.value.startDate && !this.FilterForm.value.endDate) {
            this.fromToDateInvalid = true;
          } else {
            this.fromToDateInvalid = false;
          }
        } else {
          this.fromToDateInvalid = false;
        }
        
        this.leadserialSkip=0;
        if(this.profileInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole){
          this.getLead();
        }
      });
    // for leads
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswEngineerProgramRole) {
      this.showLeadTable=true;
      this.showClaimsTable=false;
      this.showReturnsTable=false;
      this.leadRequestObject.providedby = this.profileInfo.programUserId;
      this.leadRequestObject.programId = this.configService.getprogramInfo().programId;
      this.leadRequestObject.clientId = this.configService.getprogramInfo().clientId;
      this.leadRequestObject.sort = { createdAt: -1 };
      this.leadfilterName = "All";
      this.leadstatusFilterName = "All";
      this.leadtimeline = "All Leads";
      this.getLead();
    }

    console.log("this.showTables",this.showTables );
    console.log(" this.showClaimsTable", this.showClaimsTable );
    console.log("this.showLeadTable",this.showLeadTable);
    console.log("this.showRRTable",this.showRRTable);
  }

  getMyChannels(requestObject) {
    this.showLoader=true;
    console.log('req', requestObject);
    // if (requestObject.approvalStatus == "") {
    //   delete requestObject.approvalStatus;
    // }
    console.log("calling getMyChannels", )
    console.log("this.skip", this.skip);
    console.log("this.limit", this.limit);

    requestObject.userInfo = this.configService.getMyChannelsChainRequestObject().userInfo,


      console.log('getMyChannels reqObj', requestObject);

    this.channelsService.getMyChannels(requestObject)
      .subscribe(
        (responseObject) => {
          console.log("Res main in detailsRRRRR", responseObject);
          console.log("Res main in detailsRRRRR", responseObject.responseCodes);
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();
          // console.log("Res main in detailsRRRRR",responseObject);
          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              console.log("rollback err");
              break;
            case responseCodes.RESP_SERVER_ERROR:
              console.log("server err");
              break;
            case responseCodes.RESP_SUCCESS:
              console.log("in get my channels response in deatails page", responseObject.result);
              this.channelsInfo = responseObject.result;
              this.noOfSubordinates = responseObject.noOfSubordinates;
              console.log("Can Claim for child : ", this.canClaimForChild);
              console.log("Channel Info", this.channelsInfo);
              this.channelsFound = true;
              this.channelTotalRecords = responseObject.count;
              console.log("totalRecords", this.channelTotalRecords);
              console.log("this.channelPageCount * this.channelLimit", this.channelPageCount * this.channelLimit);

              if (this.channelTotalRecords < this.channelPageCount * this.limit) {
                this.isNext = true;
              } else {
                this.isNext = false;
              }

              //Disable next button if total records are equal to pagination limit
              if (this.channelTotalRecords === this.channelPageCount * this.channelLimit) {
                this.isNext = true;
              }
              console.log("this.channelPageCount", this.channelPageCount);
              if (this.channelPageCount == 1) {
                this.isPrevious = true;
              } else {
                this.isPrevious = false;
              }
              if (responseObject.count > 0) {
                this.isCountCanBeShown = true;
                if (this.channelPageCount > 1) {
                  this.channelstartRecord =
                    this.channelPageCount * this.channelLimit - (this.channelLimit - 1);
                  this.endRecord =
                    this.channelstartRecord + (responseObject.count - 1);
                } else {
                  this.channelstartRecord = 1;
                  this.channelendRecord =
                    this.channelstartRecord + (responseObject.count - 1);
                }
              } else {
                // this.isCountCanBeShown = false;
              }
              console.log("this.isNext", this.isNext);
              console.log("channelTotalRecords, startRec, end",this.channelTotalRecords, this.channelstartRecord, this.channelendRecord);

              break;
            case responseCodes.RESP_AUTH_FAIL:
              console.log("in res auth failerr")
              break;
            case responseCodes.RESP_FAIL:
              console.log("in res failerr")
              this.channelsFound = false;
              this.channelsInfo = [];
              this.noOfSubordinates = 0;

              break;
            case responseCodes.RESP_ALREADY_EXIST:
              console.log("already exit errr");
              break;
          }
        },
        err => {
          this.showLoader = false;
        }
      );
  }

  backToSalesFlow() {
    console.log("RRR test");
    this.showLoader = true;
    this.channelsService.channelBack = true;
    this.router.navigate(['./home/ChannelsComponent']);
  }
  changeToClaims() {
    console.log("HERE");
    this.active = "claims";
    this.fromTo = false; //todo
    this.showClaimsTable = true;
    this.showReturnsTable = false;
    this.showRRTable = false;
    this.showLeadTable = false;
  }

  changeToRedem() {
    this.active = "redem";
    this.fromTo = false; //todo
    this.showClaimsTable = false;
    this.showLeadTable = false;
    this.showReturnsTable = false;
    this.showRRTable = true;
    console.log("this.profileInfo.userDetails RRR", this.profileInfo.userDetails);
    this.requestObjectRedeem = {
      sort: { createdAt: -1 },
      customerId: this.profileInfo.userDetails.userId,
      programId: this.programUserInfo.programId,
      clientId: this.programUserInfo.clientId,
      frontendUserInfo: {},
      skip: 0,
      // creditPartyId: this.creditPartyId,
      limit: this.limit,
      // sdate: this.sdate,
      // edate: this.edate,
      // orderStatus: this.orderStatus,
      // parentUserId: this.parentUserId
    }
    console.log("RequestObjectRedeem In changeToRedem : ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToRet() {
    this.active = "returns";
    this.fromTo = false; //todo
    this.showClaimsTable = false;
    this.showReturnsTable = true;
    this.showRRTable = false;
    this.showLeadTable = false;
  }


  getAllTheSecondarySales(requestObject) {
    console.log("in getAllTheSecondarySales", requestObject);
    this.showLoader = true;
    this.noRecordMessage = false;

    delete requestObject.startDate;
    delete requestObject.endDate;

    if (requestObject.approvalStatus == "") {
      delete requestObject.approvalStatus;
    }

    this.claimsService.getAllSecondarySales(requestObject)
      .subscribe(
        (responseObject) => {
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();

          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              break;
            case responseCodes.RESP_SERVER_ERROR:
              break;
            case responseCodes.RESP_SUCCESS:
              this.claimsFiltering(responseObject.result);

              this.configService.setMyClaims(responseObject.result);
              this.totalClaimsCount = responseObject.count;
              this.noOfClaims = this.totalClaimsCount;
              console.log("**********getAllTheSecondarySales*************");
              console.log("myClaimsInfo.length : ", this.myClaimsInfo);
              console.log("totalClaimsCount : ", this.totalClaimsCount);
              console.log("pageCount * limit : ", this.pageCount * this.limit);
              console.log("Limit : ", this.limit);
              console.log("this.pageCount : ", this.pageCount);

              if ((this.pageCount) * (this.limit) >= this.totalClaimsCount) {
                this.disableNext = true;
              } else {
                this.disableNext = false;
              }

              this.serialSkip = this.requestObject.skip;

              if (this.pageCount == 1) {
                this.disablePrev = true;
              } else {
                this.disablePrev = false;
              }
              
              console.log("SKIP", this.requestObject.skip);
              console.log("SKIPPER", this.serialSkip);
              break;
            case responseCodes.RESP_AUTH_FAIL:
              break;
            case responseCodes.RESP_FAIL:
              this.showLoader = false;
              this.myClaimsInfo = null;
              this.noRecordMessage = true;
              this.showBoth = false;
              this.showDBP = false;
              this.showDQP = false;
              this.showDVP = false;
              this.showIBP = false;
              this.showIQP = false;

              break;
            case responseCodes.RESP_ALREADY_EXIST:
              break;
          }
        },
        err => {
          this.showLoader = false;
        }
      );
  }

  getAllSecondarySales(requestObject) {
    console.log("in get sec sale fun in ts requestObject", requestObject);
    this.showLoader = true;
    this.noRecordMessage = false;


    if (requestObject.approvalStatus == "") {
      delete requestObject.approvalStatus;
    }

    this.claimsService.getAllSecondarySales(requestObject)
      .subscribe(
        (responseObject) => {
          // setTimeout(() => { this.showLoader = false }, 1000)
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();

          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              break;
            case responseCodes.RESP_SERVER_ERROR:
              break;
            case responseCodes.RESP_SUCCESS:

              console.log(" in get sec sale sccess", responseObject.result);
              this.myClaimsInfo = responseObject.result;
              var resultCount = responseObject.result.count;
              this.noOfClaims = responseObject.result.length;
              var resultCount = responseObject.count;

              this.configService.setMyClaims(responseObject.result);
              console.log("**********getAllSecondarySales*************");
              console.log("Length", this.myClaimsInfo);
              console.log("Limit", this.limit);
              console.log("Count", resultCount);
              console.log("this.totalClaimsCount", this.totalClaimsCount);
              console.log("this.pageCount * this.limit", this.pageCount * this.limit);
              this.serialSkip = this.requestObject.skip;

              if ((this.pageCount) * (this.limit) >= this.totalClaimsCount) {
                this.disableNext = true;
              } else {
                this.disableNext = false;
              }
              console.log("Result", this.disableNext);
              if (this.pageCount == 1) {
                this.disablePrev = true;
              } else {
                this.disablePrev = false;
              }
              console.log("this.disableNext", this.disableNext);
              console.log("this.disablePrev", this.disablePrev);
              console.log("SKIP", this.requestObject.skip);
              // this.myClaimsInfo = responseObject.result;
              this.claimsFiltering(responseObject.result);
              console.log("SKIPPER", this.serialSkip);
              break;
            case responseCodes.RESP_AUTH_FAIL:

              // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
              break;
            case responseCodes.RESP_FAIL:
              this.myClaimsInfo = null;
              this.noRecordMessage = true;
              // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
              break;
            case responseCodes.RESP_ALREADY_EXIST:
              // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
              break;
          }
        },
        err => {
          this.showLoader = false;
          // this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
        }
      );
  }

  getAllOrder(requestObject) {
    // let requestObject: any = {};
    this.showLoader = true;
    this.redeemRequestsArr = [];
    let redeem: any[] = [];
    console.log("Profile Info getallorder : ", this.profileInfo);
    console.log("userDetails", this.profileInfo.userDetails);
    requestObject.customerId = this.profileInfo.userDetails.userId;
    if (requestObject.orderStatus == "") {
      delete requestObject.orderStatus;
    }
    console.log("getAllOrders request Object : ", this.requestObject);
    this.channelsService.getAllOrder(requestObject)
      .subscribe(
        (responseObject) => {
          this.showLoader = false;
          let responseCodes = this.configService.getStatusTokens();

          switch (responseObject.statusCode) {
            case responseCodes.RESP_ROLLBACK_ERROR:
              break;
            case responseCodes.RESP_SERVER_ERROR:
              break;
            case responseCodes.RESP_SUCCESS:
              this.redeemRequestsArr = responseObject.result;
              // var resultCount = responseObject.result.count;
              this.noOfRedeems = responseObject.result.length;
              this.totalRedeemsCount = responseObject.count;
              this.noRecordMessage = false;
              // this.configService.setRedeems(responseObject.result);
              // var resultCount = responseObject.count;
              // this.redeemRequestsArr = [];
              // for (let i = 0; i < responseObject.result.length; i++) {
              //   redeem[i] = responseObject.result[i];
              // }
              // this.redeemRequestsArr = redeem;
              console.log("ResponseObjectArray getAllOrder", this.redeemRequestsArr);

              this.serialSkip = this.requestObjectRedeem.skip;
              // this.serialSkip = requestObject.skip;

              if ((this.pageCount) * (this.limit) >= this.totalRedeemsCount) {
                this.disableNext = true;
              } else {
                this.disableNext = false;
              }
              console.log("Mul-->", this.pageCount * this.limit);

              console.log("Result", this.disableNext);
              if (this.pageCount == 1) {
                this.disablePrev = true;
              } else {
                this.disablePrev = false;
              }
              console.log("SKIP", this.requestObjectRedeem.skip);
              console.log("SKIPPER", this.serialSkip);
              // var req = {};
              // this.getAllSecondarySales(req);
              break;
            case responseCodes.RESP_AUTH_FAIL:
              break;
            case responseCodes.RESP_FAIL:
              this.noRecordMessage = true;
              this.noOfRedeems = 0;
              // this.redeemRequestsArr = [];
              break;
            case responseCodes.RESP_ALREADY_EXIST:
              break;
          }
        },
        err => {
          this.showLoader = false;
        }
      );
  }

  // LIst Claim methods
  getMonthName(dt: number): String {

    let monthArray: String[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return monthArray[dt];
  }


  changeToPending() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showPendingClaims");
    this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
    this.approvalStatus = "pending"
    this.requestObject.approvalStatus = this.approvalStatus;
    console.log("REQ", this.requestObject);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);

  }

  redeemChangeToPending() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showPendingRedeems");
    this.statusFilterName = this.stringService.getStaticContents().myClaimsPending;
    this.orderStatus = "Pending"
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);

  }

  changeToApproved() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showApprovedClaims");
    this.statusFilterName = this.stringService.getStaticContents().myClaimsApproved;
    this.approvalStatus = "approved";
    this.requestObject.approvalStatus = this.approvalStatus;
    console.log("REQ", this.requestObject);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToConfirmed() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showApprovedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemConfirmed;
    this.orderStatus = "Confirmed";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToRejected() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showRejectedClaims");
    this.statusFilterName = this.stringService.getStaticContents().myClaimsRejected;
    this.approvalStatus = "rejected";
    this.requestObject.approvalStatus = this.approvalStatus;
    console.log("REQ", this.requestObject);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToProcessing() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemProcessing;
    this.orderStatus = "Processing";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemChangeToShipped() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemShipped;
    this.orderStatus = "Shipped";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemChangeToCompleted() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemCompleted;
    this.orderStatus = "Completed";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemChangeToCancelled() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemCancelled;
    this.orderStatus = "Cancelled";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemChangeToReturned() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showRejectedRedeems");
    this.statusFilterName = this.stringService.getStaticContents().redeemReturned;
    this.orderStatus = "Returned";
    this.requestObjectRedeem.orderStatus = this.orderStatus;
    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToAll() {
    this.skip = 0;
    this.requestObject.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showAllClaims");


    this.statusFilterName = this.stringService.getStaticContents().redeemAll;
    delete this.requestObject.approvalStatus;

    console.log("REQ", this.requestObject);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToAll() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showAllRedeems");


    this.statusFilterName = this.stringService.getStaticContents().redeemAll;
    delete this.requestObjectRedeem.orderStatus;

    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToLifeTime() {
    this.requestObject = {
      programId: this.programUserInfo.programId,
      clientId: this.programUserInfo.clientId,
      frontendUserInfo: {},
      skip: 0,
      creditPartyId: this.creditPartyId,
      limit: this.limit,
      startDate: this.startDate,
      endDate: this.endDate,
      approvalStatus: this.approvalStatus,
      parentUserId: this.parentUserId
    }
    console.log("in changeToLifeTime RRRR", this.requestObject);
    this.filterName = "All";
    this.timeline = "All Claims";
    this.statusFilterName = "All";
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showLifeTimeClaims");
    delete this.requestObject.startDate;
    delete this.requestObject.endDate;
    console.log("Lifetime RRRRRRr", this.requestObject);
    this.getAllTheSecondarySales(this.requestObject);
  }

  redeemChangeToLifeTime() {
    console.log("in redeemChangeToLifeTime");
    this.requestObjectRedeem.skip = 0;
    this.filterName = "All";
    this.timeline = "All Redemptions";
    this.statusFilterName = "All";
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showLifeTimeRedeems");
    delete this.requestObjectRedeem.sdate;
    delete this.requestObjectRedeem.edate;
    console.log("Lifetime", this.requestObjectRedeem);
    this.serialSkip = this.requestObjectRedeem.skip;
    // this.filterReedemRequestsForm.patchValue({
    //   "redeemStartDate": [""],
    //   "redeemEndDate": [""]
    // });
    console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToWeek() {
    console.log("in changeToWeek");
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showWeekClaims");
    this.fromTo = false;
    this.filterName = " This Week";
    var dt = new Date(this.startDate);
    var weekDate = new Date();
    var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
    this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
    this.requestObject.startDate = weekStartDate;
    this.requestObject.endDate = weekDate;
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToWeek() {
    console.log("in redeemChangeToWeek");
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showWeekRedeems");
    this.fromTo = false;
    this.filterName = " This Week";
    var dt = new Date(this.sdate);
    var weekDate = new Date();
    var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
    this.timeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
    this.requestObjectRedeem.sdate = weekStartDate;
    this.requestObjectRedeem.edate = weekDate;
    this.serialSkip = this.requestObjectRedeem.skip;
    // this.filterReedemRequestsForm.patchValue({
    //   "redeemStartDate": [""],
    //   "redeemEndDate": [""]
    // });
    console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToQuarter() {
    console.log("in changeToQuarter");
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showQuarterClaims");
    this.fromTo = false;
    this.filterName = "This Quarter";
    var quarterMonth = new Date();
    var dt = new Date();
    var currentMonth = quarterMonth.getMonth();
    dt.setMonth(dt.getMonth() - 3);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
    this.requestObject.startDate = new Date(date);
    this.requestObject.endDate = new Date();
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToQuarter() {
    console.log("in changeToQuarter");
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showQuarterRedeems");
    this.fromTo = false;
    this.filterName = "This Quarter";
    var quarterMonth = new Date();
    var dt = new Date();
    var currentMonth = quarterMonth.getMonth();
    dt.setMonth(dt.getMonth() - 3);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(dt.getMonth()) + " " + dt.getFullYear() + " to " + this.getMonthName(currentMonth) + " " + this.date.getFullYear();
    this.requestObjectRedeem.sdate = new Date(date);
    this.requestObjectRedeem.edate = new Date();
    this.serialSkip = this.requestObjectRedeem.skip;
    // this.filterReedemRequestsForm.patchValue({
    //   "redeemStartDate": [""],
    //   "redeemEndDate": [""]
    // });
    console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  changeToMonth() {
    console.log("in changeToQuarter");
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showMonthClaims");
    this.fromTo = false;
    this.filterName = " This Month";
    var month = new Date();
    var dt = new Date();
    var currentMonth = month.getMonth();
    dt.setMonth(dt.getMonth() - 1);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
    var startDate = new Date(date);
    this.requestObject.startDate = startDate;
    this.requestObject.endDate = new Date();
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  redeemChangeToMonth() {
    console.log("in changeToQuarter");
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showMonthRedeems");
    this.fromTo = false;
    this.filterName = " This Month";
    var month = new Date();
    var dt = new Date();
    var currentMonth = month.getMonth();
    dt.setMonth(dt.getMonth() - 1);
    var date = dt.toISOString();
    this.timeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();
    var startDate = new Date(date);
    this.requestObjectRedeem.sdate = startDate;
    this.requestObjectRedeem.edate = new Date();
    console.log("Request Object : ", this.requestObjectRedeem);
    this.serialSkip = this.requestObjectRedeem.skip;
    // this.filterReedemRequestsForm.patchValue({
    //   "redeemStartDate": [""],
    //   "redeemEndDate": [""]
    // });
    console.log("RequestObjectRedeem : ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  chooseStartToEnd() {
    console.log("in changeToQuarter");
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showCustomClaims");
    this.filterName = "Time Period";
    this.fromTo = true;
  }

  redeemChooseStartToEnd() {
    console.log("in redeemChooseStartToEnd");
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showCustomRedeems");
    this.filterName = "Time Period";
    this.fromTo = true;
  }




  changeLimit() {
    console.log("in changeLimit");
    this.requestObject.limit = this.filterMyClaimForm.value.showResults;
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  onPrevClick() {
    console.log("in onPrevClick");
    this.requestObject.skip = this.requestObject.skip - this.limit;
    this.requestObject.limit = this.limit;
    this.pageCount--;
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "previousPageClaims", this.pageCount);
    this.serialSkip = this.requestObject.skip;
    console.log("this.profileInfo.programRole",this.profileInfo.programRole);
    console.log("this.stringService.getStaticContents().jswDealerProgramRole",this.stringService.getStaticContents().jswDealerProgramRole);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
  }

  onPrev() {
    console.log("IN previous");
    this.channelRequestObject.skip = this.channelRequestObject.skip - this.channelLimit;
    this.channelRequestObject.limit = this.channelLimit;
    this.channelPageCount--;
    this.serialSkip = this.channelRequestObject.skip;
    this.getMyChannels(this.channelRequestObject);
  }

  onNext() {
    console.log("In next");
    this.channelRequestObject.skip = this.channelRequestObject.skip + this.channelLimit;
    this.channelRequestObject.limit = this.channelLimit;
    this.channelPageCount++;
    this.serialSkip = this.requestObject.skip;
    this.getMyChannels(this.channelRequestObject);
  }

  onPrevLead() {
    console.log("IN previous");
    this.leadRequestObject.skip = this.leadRequestObject.skip - this.leadlimit;
    this.leadRequestObject.limit = this.leadlimit;
    this.leadpageCount--;
    this.leadserialSkip = this.channelRequestObject.skip;
    this.getLead();
  }

  onNextLead() {
    console.log("In next");
    this.leadRequestObject.skip = this.leadRequestObject.skip + this.leadlimit;
    this.leadRequestObject.limit = this.leadlimit;
    this.leadpageCount++;
    this.leadserialSkip = this.requestObject.skip;
    this.getLead();
  }

  redeemOnPrevClick() {
    console.log("in redeemOnPrevClick");
    this.requestObjectRedeem.skip = this.requestObjectRedeem.skip - this.limit;
    this.requestObjectRedeem.limit = this.limit;
    this.pageCount--;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "previousPageClaims", this.pageCount);
    this.serialSkip = this.requestObjectRedeem.skip;
    this.getAllOrder(this.requestObjectRedeem);
  }


  onNextClick() {
    console.log("in onNextClick");
    this.requestObject.skip = this.requestObject.skip + this.limit;
    this.requestObject.limit = this.limit;
    this.pageCount++;
    this.serialSkip = this.requestObject.skip;
    console.log("this.profileInfo.programRole",this.profileInfo.programRole);
    console.log("this.stringService.getStaticContents().jswDealerProgramRole",this.stringService.getStaticContents().jswDealerProgramRole);
    // make get claims api call in case of dealer
    if (this.profileInfo.programRole === this.stringService.getStaticContents().jswDealerProgramRole) {
      this.getAllSecondarySales(this.requestObject);
    } else {
      this.noRecordMessage = true;
    }
    // this.getAllSecondarySales(this.requestObject);
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "nextPageClaims", this.pageCount);

  }

  redeemOnNextClick() {
    console.log("in redeemOnNextClick");
    this.requestObjectRedeem.skip = this.requestObjectRedeem.skip + this.limit;
    this.requestObjectRedeem.limit = this.limit;
    this.pageCount++;
    this.serialSkip = this.requestObjectRedeem.skip;
    this.getAllOrder(this.requestObjectRedeem);
    this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObjectRedeem.programId, "nextPageClaims", this.pageCount);

  }

  onStartDate(startDate) {
    console.log("in onStartDate");
    this.programStartDate = startDate;
    this.currentStartDate = startDate;
  }

  redeemOnStartDate(startDate) {
    this.redeemProgramStartDate = startDate;
    this.redeemcurrentStartDate = startDate;
  }

  redeemChangeToAllApprovalStatus() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "showAllRedeems");


    this.statusFilterName2 = this.stringService.getStaticContents().redeemAll;
    delete this.requestObjectRedeem.approvalStatus;

    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemApprovalStatusPending() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Pending Approval Status Redeem");

    this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusPending;
    this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemApprovalStatusApproved() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Approved Approval Status Redeem");

    this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusApproved;
    this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  redeemApprovalStatusRejected() {
    this.skip = 0;
    this.requestObjectRedeem.skip = 0;
    this.googleAnalyticsEventsService.emitEvent("My Redeems", this.requestObjectRedeem.programId, "Rejected Approval Status Redeem");

    this.statusFilterName2 = this.stringService.getStaticContents().redeemApprovalStatusRejected;
    this.requestObjectRedeem.approvalStatus = this.statusFilterName2;

    console.log("REQ", this.requestObjectRedeem);
    this.getAllOrder(this.requestObjectRedeem);
  }

  claimsFiltering(claimsInfo) {
    console.log("in claimsFiltering");
    for (let i = 0; i < claimsInfo.length; i++) {
      let invoiceDoc = claimsInfo[i].invoiceDocument;
      claimsInfo[i].invoiceDocument = invoiceDoc;
      // claimsInfo[i].approvalStatus
      if (claimsInfo[i].approvalStatus == "pending") {
        claimsInfo[i].pointsEarned = "Pending";
      }
      else if (claimsInfo[i].approvalStatus == "rejected") {
        claimsInfo[i].pointsEarned = 0;
      }

    }
    let userInfo = this.configService.getloggedInProgramUser();
    console.log("USER INFO : ", claimsInfo);
    console.log("USER INFO : ", userInfo);

    if (claimsInfo) {

      for (let i = 0; i < claimsInfo.length; i++) {
        claimsInfo[i].isReturn = false;
        if (claimsInfo[i].debitPartyId === userInfo.programUserId) {
          claimsInfo[i].isReturn = true;
        }
        console.log(claimsInfo[i].debitPartyId);

      }
      console.log("^^^^^  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", claimsInfo);
      console.log(userInfo.programUserId);
    }
    this.myClaimsInfo = claimsInfo;
    console.log("Claims Info : ", this.myClaimsInfo);

  }

  openDialog(index) {
    // this.configService.setMyClaims(this.myClaimsInfo[index]);
    this.channelsService.setMyClaims(this.myClaimsInfo[index]);
    let dialogRef = this.dialog.open(InvoiceClaimsOperationsComponent, {
      height: '55vh',
      width: '55vw'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAllTheSecondarySales(this.requestObject);
      setTimeout(() => {
        var Obj = {
          "frontendUserInfo": this.configService.getFrontEndUserInfo(),
          "programId": this.configService.getprogramInfo().programId,
          "clientId": this.configService.getprogramInfo().clientId,
          "creditPartyId": this.creditPartyId,
          "parentUserId": this.parentUserId,
          "skip": this.skip,
          "limit": this.limit
        };
        console.log("timeout");
        this.getAllSecondarySales(Obj)
      }, 5000);
    });
  }

  openDigital(index) {
    console.log("THIS IS CALLED");
    // this.configService.setMyClaims(this.myClaimsInfo[index]);
    this.channelsService.setMyClaims(this.myClaimsInfo[index]);
    let dialogRef = this.dialog.open(DigitalClaimsOperationsComponent, {
      height: '55vh',
      width: '55vw'
    });
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        var Obj = {
          "frontendUserInfo": this.configService.getFrontEndUserInfo(),
          "programId": this.configService.getprogramInfo().programId,
          "clientId": this.configService.getprogramInfo().clientId,
          "creditPartyId": this.creditPartyId,
          "parentUserId": this.parentUserId,
          "skip": this.skip,
          "limit": this.limit
        };
        console.log("timeout");
        this.getAllSecondarySales(Obj)
      }, 5000);
      // this.getAllTheSecondarySales(this.requestObject);
    });
  }

  openCode(index) {
    console.log("THIS IS CALLED");
    // this.configService.setMyClaims(this.myClaimsInfo[index]);
    this.channelsService.setMyClaims(this.myClaimsInfo[index]);
    let dialogRef = this.dialog.open(CodeClaimsOperationsComponent, {
      height: '55vh',
      width: '85vw'
    });
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        var Obj = {
          "frontendUserInfo": this.configService.getFrontEndUserInfo(),
          "programId": this.configService.getprogramInfo().programId,
          "clientId": this.configService.getprogramInfo().clientId,
          "creditPartyId": this.creditPartyId,
          "parentUserId": this.parentUserId,
          "skip": this.skip,
          "limit": this.limit
        };
        console.log("timeout");
        this.getAllSecondarySales(Obj)
      }, 5000);
      // this.getAllTheSecondarySales(this.requestObject);
    });
  }

  // for leads
  getLead() {
    console.log("this.leadRequestObject",this.leadRequestObject);
    this.showLoader = true;
    this.leadRequestObject.providedby = this.profileInfo.programUserId;
    this.leadRequestObject.programId = this.configService.getprogramInfo().programId;
    this.leadRequestObject.clientId = this.configService.getprogramInfo().clientId;
    this.leadRequestObject.skip = this.leadskip;
    this.leadRequestObject.limit = this.leadlimit;
    this.leadRequestObject.sort = { createdAt: -1 };

    if (this.leadDuration === 'currentMonthLeads') {
      var endDate = new Date();
      endDate.setDate(endDate.getDate() + 1);
      var startDate = new Date();
      startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
      this.leadRequestObject.startDate = startDate.toISOString().slice(0, 10);
      this.leadRequestObject.endDate = endDate.toISOString().slice(0, 10);
      this.leadRequestObject.isPending = true;
      this.leadRequestObject.isQualified = true;
    } else if (this.leadDuration === 'lastMonthLeads') {
      var endDate = new Date();
      endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1);
      var startDate = new Date(endDate);
      startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
      this.leadRequestObject.startDate = startDate.toISOString().slice(0, 10);
      this.leadRequestObject.endDate = endDate.toISOString().slice(0, 10);
      this.leadRequestObject.isPending = true;
      this.leadRequestObject.isQualified = true;
    }

    // if (this.generateCsv === true) {
    //   delete this.requestObj.limit;
    //   delete this.requestObj.skip;
    // }
    
    console.log("this.leadRequestObject.requestObj", this.leadRequestObject);
    this.leadService.getLead(this.leadRequestObject).subscribe(
      responseObject => {

        // this.myVar = true
        let responseCodes = this.configService.getStatusTokens();
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            this.showLoader = false;
            break;
          case responseCodes.RESP_SERVER_ERROR:
            this.showLoader = false;
            break;
          case responseCodes.RESP_SUCCESS:
            this.showLoader = false;
            this.noRecordMessage1 = false
            console.log("channel Info in home component", responseObject);



            //For IE10
            for (var a = 0; a < responseObject.result.length; a++) {
              console.log("CreatedAt", responseObject.result[a].data.createdAt);
              var date = new Date(responseObject.result[a].data.createdAt);
              var createdAt = date.toDateString();
              responseObject.result[a].data.createdAt = createdAt;
            }

            this.summarylist = responseObject.result;
            console.log("this.summarylist", this.summarylist);
            this.leadserialSkip=this.leadRequestObject.skip;
            // this.configService.setLeadDashboardLink(null);
            // if (this.generateCsv === true) {
            //   delete this.requestObj.limit;
            //   delete this.requestObj.skip;
            //   this.generateLeadCsv();
            // }
            if (this.summarylist.length < this.leadlimit) {
              this.isNext = true;
            } else {
              this.isNext = false;
            }

            if (this.leadpageCount == 1) {
              this.isPrevious = true;
            } else {
              this.isPrevious = false;
            }
            if (this.leadRequestObject.skip == 0) {
              this.leadtotalRecords = responseObject.count;
            }
            console.log("this.leadtotalRecords",this.leadtotalRecords);
            // code to show no. of records on navigation page
            if (this.summarylist.length > 0) {
              this.isCountCanBeShown = true;
              if (this.leadpageCount > 1) {
                this.startRecord =
                  this.pageCount * this.leadlimit - (this.leadlimit - 1);
                this.endRecord =
                  this.startRecord + (responseObject.result.length - 1);
              } else {
                this.startRecord = 1;
                this.endRecord =
                  this.startRecord + (responseObject.result.length - 1);
              }
            } else {
              // this.isCountCanBeShown = false;
            }

            //Disable next button if total records are equal to pagination limit
            if (this.leadtotalRecords === this.leadpageCount * this.leadlimit) {
              // //console.log("counts true");
              this.leadisNext = true;
            }
            break;
          case responseCodes.RESP_AUTH_FAIL:
            this.summarylist = [];
            this.showLoader = false;
            break;
          case responseCodes.RESP_FAIL:
            this.summarylist = [];
            this.showLoader = false;
            this.noRecordMessage1 = true
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => { 
        this.showLoader = false;
      }
    );
  }

  thisSelectTimePeriod() {
    this.leadRequestObject.startDate = this.FilterForm.value.startDate;
    this.leadRequestObject.endDate = this.FilterForm.value.endDate;
    console.log("this.leadRequestObject.startDate", this.leadRequestObject.startDate, "this.leadRequestObject.endDate", this.leadRequestObject.endDate);
  }
  changeToLeads() {
    console.log("HERE");
    this.active = "leads";
    this.fromTo = false; //todo
    this.showClaimsTable = false;
    this.showReturnsTable = false;
    this.showRRTable = false;
    this.showLeadTable = true;
    this.skip = 0;
    this.limit = 10;
    this.getLead();
  }
  changeToLeadLifeTime(){
      this.leadfilterName = "All";
      this.leadtimeline = "All Leads";
      this.leadstatusFilterName = "All";
      delete this.leadRequestObject.startDate;
      delete this.leadRequestObject.endDate;
      console.log("Lifetime RRRRRRr", this.leadRequestObject);
      this.getLead();
    }

    changeToLeadMonth() {
        this.fromTo = false;
        this.leadfilterName = " This Month";
        var month = new Date();
        var dt = new Date();
        var currentMonth = month.getMonth();
        dt.setMonth(dt.getMonth() - 1);
        var date = dt.toISOString();
        this.leadtimeline = this.getMonthName(currentMonth) + " " + dt.getFullYear();

        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
        this.leadRequestObject.startDate = startDate.toISOString();
        this.leadRequestObject.endDate = endDate.toISOString();
        delete this.leadRequestObject.isPending;
        delete this.leadRequestObject.isQualified;
        this.getLead();
        // this.getAllSecondarySales(this.requestObject);
      }
      changeToLeadLastMonth() {
        this.fromTo = false;
        this.leadfilterName = " Last Month";

        var endDate = new Date();
        endDate.setDate(endDate.getDate() - (endDate.getDate() - 1) - 1);

        var lastmonth = endDate.getMonth();
        console.log("lastmonth",lastmonth);
        this.leadtimeline = this.getMonthName(lastmonth) + " " + endDate.getFullYear();

        console.log("trupti",this.leadtimeline);

        var startDate = new Date(endDate);
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1));
        this.leadRequestObject.startDate = startDate.toISOString();
        this.leadRequestObject.endDate = endDate.toISOString();
        delete this.leadRequestObject.isPending;
        delete this.leadRequestObject.isQualified;
        this.getLead();
        // this.getAllSecondarySales(this.requestObject);
      }

      changeToLeadWeek() {
        console.log("in changeToWeek");
        this.googleAnalyticsEventsService.emitEvent("My Claims", this.requestObject.programId, "showWeekClaims");
        this.fromTo = false;
        this.leadfilterName = " This Week";
        var dt = new Date(this.startDate);
        var weekDate = new Date();
        var weekStartDate = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
        this.leadtimeline = weekStartDate.getDate() + " " + this.getMonthName(weekStartDate.getMonth()) + " " + weekStartDate.getFullYear() + " to " + weekDate.getDate() + " " + this.getMonthName(weekDate.getMonth()) + " " + weekDate.getFullYear();
        this.leadRequestObject.startDate = weekStartDate;
        this.leadRequestObject.endDate = weekDate;
        this.getLead();
        // this.getAllSecondarySales(this.requestObject);
      }

      leadchooseStartToEnd() {
        console.log("in lead changeToQuarter");
        this.leadfilterName = "Time Period";
        this.fromTo = true;
      }

      leadchangeToAll() {
        this.leadRequestObject.skip = 0;
        this.leadstatusFilterName = this.stringService.getStaticContents().redeemAll;
        delete this.leadRequestObject.qualified;
        delete this.leadRequestObject.rejected;
        delete this.leadRequestObject.pending;
    
        console.log("leadRequestObject", this.leadRequestObject);
        this.getLead();
      }

      changeToLeadPending() {
        this.leadRequestObject.skip = 0;
        this.leadstatusFilterName = this.stringService.getStaticContents().myClaimsPending;
        this.leadRequestObject.qualified = false;
        this.leadRequestObject.rejected = false;
        this.leadRequestObject.pending = true;
        console.log("this.leadRequestObject", this.leadRequestObject);
        this.getLead();
      }

      leadchangeToApproved() {
        this.leadRequestObject.skip = 0;
        this.leadstatusFilterName = this.stringService.getStaticContents().myClaimsApproved;
        this.leadRequestObject.qualified = true;
        this.leadRequestObject.rejected = false;
        this.leadRequestObject.pending = false;
        console.log("leadRequestObject", this.leadRequestObject);
        this.getLead();
      }

      changeToLeadRejected() {
        this.leadRequestObject.skip = 0;
        this.leadstatusFilterName = this.stringService.getStaticContents().myClaimsRejected;
        this.leadRequestObject.qualified = false;
        this.leadRequestObject.rejected = true;
        this.leadRequestObject.pending = false;
        console.log("leadRequestObject", this.leadRequestObject);
        this.getLead();
      }

      leadStartDate(startDate) {
        console.log("in leadStartDate",startDate)
        this.redeemProgramStartDate = startDate;
        this.leadcurrentStartDate = startDate;
      }

      detailClick(info) {

        // Code to stop backgroung scrolling
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    
    
        document.getElementById("body").style.overflow = "hidden";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    
        this.configService.setModalClose(false);
    
        console.log("infooooo", info);
        this.claimDetail = info;
        this.purchaseDetails = info.purchaseDetails;
        if (info.paymentStatus == "notDone") {
          this.paymentStatus = "Pending";
          info.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);
    
    
        }
        if (info.paymentStatus == "done") {
          this.paymentStatus = "Done";
          info.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);
    
        }
        if (info.paymentStatus == "partiallyDone") {
          this.paymentStatus = "Partially Done";
          info.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);
    
        }
        if (info.paymentStatus == "pending") {
    
          this.paymentStatus = "Pending";
          info.paymentStatus = this.paymentStatus;
          console.log("this.paymentStatus", this.paymentStatus);
        };
        this.showDetail = true;
      }
    
      closeModal() {
        // Code to activate backgroung scrolling
        var scroll = document.getElementById('body');
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
    
        // this.configService.setModalClose(true);
    
        document.getElementById("body").style.overflow = "auto";
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTT", scroll);
      }
    
      download() {
        console.log("this.claimDetail.invoiceNumber", this.claimDetail.invoiceNumber);
        this.googleAnalyticsEventsService.emitEvent("My Claims", "downloadClaim", this.claimDetail.invoiceNumber);
    
      }
    

}
