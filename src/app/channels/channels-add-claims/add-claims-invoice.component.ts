/*
	Author			:	Deepak Terse
	Description		: 	Component for Add Claim Invoice page
	Date Created	: 	21 March 2017
	Date Modified	: 	27 March 2017
*/

import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { ConfigService } from "../../shared-services/config.service";
import { ClaimsService } from "../../claims/claims.service";
import { StringService } from "../../shared-services/strings.service";
import { S3UploadService } from "../../shared-services/s3-upload.service";


import { FeedbackMessageComponent } from '../../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../../footer/app-footer.component';
import { ClientFooterComponent } from '../../footer/client-footer.component';
import { ChannelsService } from "../../channels/channels.service";
import { ISubscription } from "rxjs/Subscription";

@Component({
	templateUrl: './add-claims-invoice.component.html',
	styleUrls: ['channels-claims.component.css'],
	providers: [ClaimsService]

})
export class AddClaimsInvoiceComponent implements OnInit, OnDestroy {
	formTransactionDate: Date;
	transactionPeriodEndDate: any;
	transactionPeriodStartDate: any;
	programInfo: any;
	toSetParentChannelCreditPartyId: any;

	private claimsForm: FormGroup;
	private showLoader: boolean;
	private subscription: ISubscription;
	private distributors: any[];
	private distributorsInfo: any;

	private showDistributornotFoundMessage: boolean;
	private showDistributorFoundMessage: boolean;
	private disableSearchDistributer: boolean;
	private disableBtn: boolean;
	private channelBackRequestObject: any;
	public clientColor: any;
	public claimPeriodEndDate: Date;
	public claimPeriodStartDate: Date;
	public paymentStatus: string;
	public isPaymentQualificationSet: boolean;
	private resetFlag: boolean = false;

	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	private isFileSelected: boolean;
	// private disableSearchDistributer: boolean;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private claimsService: ClaimsService,
		private channelService: ChannelsService,
		private stringService: StringService,
		private s3UploadService: S3UploadService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService) {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Add Claims");
				ga('send', 'pageview');
			}
		});

		this.showLoader = false;
		this.disableBtn = true;
		this.clientColor = this.configService.getThemeColor();
		this.showDistributornotFoundMessage = false;
		this.showDistributorFoundMessage = false;
		this.isFileSelected = false;
		this.disableSearchDistributer = false;
		this.distributorsInfo = this.channelService.getDistributor();
		console.log("Dist info", this.distributorsInfo);
		var distName = this.distributorsInfo;
		this.channelBackRequestObject = this.channelService.getMyChannelsClaimsBackRequestObject();
		console.log('Back object', this.channelBackRequestObject);
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.transactionPeriodStartDate = new Date(this.programInfo.transactionPeriodStartDate);
		this.transactionPeriodEndDate = new Date(this.programInfo.transactionPeriodEndDate);

	}

	ngOnDestroy() {
		console.log("@@@ DESTROY CALLLED @@@")
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	ngOnInit() {
		this.initclaimsForm();
		this.valueChange();
		// this.distributors = this.configService.getloggedInProgramUser().parentUsersInfo;
		let programInfo = this.configService.getloggedInProgramInfo();
		this.claimPeriodStartDate = new Date(programInfo.claimPeriodStartDate);
		this.claimPeriodEndDate = new Date(programInfo.claimPeriodEndDate);
		console.log("this.claimPeriodStartDate", this.claimPeriodStartDate);
		console.log("this.claimPeriodEndDate", this.claimPeriodEndDate);

		this.distributors = this.channelService.getDistributor();
		console.log("Dist info", this.distributors);
		var distName = this.distributors;
		console.log(this.distributors);

		if (this.distributors != null) {
			this.claimsForm.patchValue({
				"debitPartyId": this.distributors[0].userDetails.userId
			});
		}



		this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
			console.log("Subscription got", value);
			// console.log(this.claimsForm.value);
			if (this.claimsForm.valid) {

				this.claimsForm.patchValue(value);
				if (value.length > 0) {
					let recordInfo: any = {};
					// this.claimsForm.controls['invoiceDocument'].setValue(value);
					if (this.claimsForm.value.debitPartyId != "") {
						//if user is either selected or searched		
						recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
					}
					else {
						//if user is not found in search
						recordInfo.distributorName = this.claimsForm.value.distributorName;
						recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
					}

					//mandatory fields
					recordInfo.invoiceNumber = this.claimsForm.value.invoiceNumber;
					recordInfo.invoiceAmount = this.claimsForm.value.invoiceAmount;
					recordInfo.invoiceAmountExcludingVAT = this.claimsForm.value.invoiceAmountExcludingVAT;
					recordInfo.transactionDate = this.claimsForm.value.transactionDate;
					recordInfo.vatPercentage = this.claimsForm.value.vatPercentage;
					recordInfo.creditPartyId = this.channelService.getSelectedChannelCreditPartyId();
					recordInfo.invoiceDocument = value[0].Location;

					console.log("RI", recordInfo);
					this.addSecondarySale(recordInfo);
				}
			}


		});
	}

	valueChange() {


		this.formTransactionDate = this.claimsForm.value.transactionDate;
		this.formTransactionDate = new Date(this.formTransactionDate);
		console.log("valuechange", this.formTransactionDate);
		if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
			var message = {
				str: "Note: You are making a claim outside the transaction period of the enrolled program"
			}
			if (this.resetFlag === false) {
				this.feedbackMessageComponent.updateMessage(true, message.str, "alert-danger")
			}
		}

		// this.claimsForm.valueChanges.subscribe(data => {
		// 	this.formTransactionDate = new Date(data.transactionDate);
		// 	console.log("valuechange", this.formTransactionDate);
		// 	if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
		// 		var message = {
		// 			str: "Note: You are making a claim outside the transaction period of the enrolled program"
		// 		}
		// 		if (data.transactionDate !== "Invalid Date" && (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate)) {
		// 			this.feedbackMessageComponent.updateMessage(true, message.str, "alert-danger")
		// 		}
		// 	}
		// })
	}



	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initclaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates invoice form group and assign initial values and validations to its controls
    */
	initclaimsForm() {
		this.claimsForm = this.formBuilder.group({
			invoiceNumber: ["", [Validators.required]],
			transactionDate: ["", [Validators.required]],
			invoiceAmount: ["", [Validators.required]],
			invoiceAmountExcludingVAT: ["", [Validators.required]],

			invoiceDocument: [],
			vatPercentage: ["0", [Validators.required]],

			debitPartyId: ["", []],
			//OR

			distributorName: ["", []],
			distributorMobileNo: ["", [Validators.required, Validators.pattern('[0-9]{10}')]]
		});
		this.claimsForm.patchValue({
			transactionDate: new Date().toISOString().split('T')[0]
		});
	}


	/*
        METHOD         : clearclaimsForm()
        DESCRIPTION    : Called from onResetFormClick() and after form is submitted
						 Resets the form to null values
    */
	clearclaimsForm() {
		this.resetFlag = true;
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
		this.claimsForm.reset({
		});
		this.resetFlag = false;
	}
	/***************************** 		END OF FORM METHODS 	********************************/


	backToSalesFlow() {
		this.toSetParentChannelCreditPartyId = this.channelService.getMyChannelProfileForComingBack();
		this.channelService.setCreditPartyId(this.toSetParentChannelCreditPartyId.userDetails.userId);
		this.channelService.setDistributor(this.toSetParentChannelCreditPartyId.parentUsersInfo);
		console.log("object GOT BACKKK", this.toSetParentChannelCreditPartyId);
		this.router.navigate(['./home/channels/profile-details']);
	}


	/************************************* 	ONCLICK METHODS 	********************************/
	// onDistributorSelected(selectedDistributor) {

	// 	if (selectedDistributor != "") {
	// 		console.log("selectedDistributor", selectedDistributor);
	// 		this.claimsForm.controls['distributorName'].disable({ onlySelf: false });
	// 		this.claimsForm.controls['distributorMobileNo'].disable({ onlySelf: false });
	// 		this.claimsForm.controls['debitPartyId'].enable({ onlySelf: true });
	// 		this.showDistributorFoundMessage = false;
	// 		console.log(this.claimsForm.value);
	// 		this.claimsForm.patchValue({ "distributorMobileNo": "" });

	// 	}
	// 	else {
	// 		this.disableSearchDistributer = true;

	// 		console.log("selectedDistributor", selectedDistributor)
	// 		this.claimsForm.controls['distributorName'].enable({ onlySelf: true });
	// 		this.claimsForm.controls['distributorMobileNo'].enable({ onlySelf: true });
	// 		// this.claimsForm.controls['debitPartyId'].disable({ onlySelf: false });
	// 		console.log(this.claimsForm.value);

	// 	}
	// }
	onDistributorSelected(selectedDistributor) {

		if (selectedDistributor != "") {
			this.claimsForm.controls['distributorMobileNo'].disable();

			console.log("selectedDistributor", selectedDistributor);


			this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
			this.claimsForm.controls['distributorName'].setValidators(null);
			this.claimsForm.controls['distributorMobileNo'].setValidators(null);
			this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

			this.showDistributorFoundMessage = false;
			this.claimsForm.patchValue({
				"distributorMobileNo": "",
				"distributorName": ""
			});
			this.showDistributornotFoundMessage = false;

			console.log(this.claimsForm.value);

		} else {
			this.claimsForm.controls['distributorMobileNo'].enable();

		}

	}



	onUserFound(userDetails) {
		this.showDistributorFoundMessage = true;

		this.claimsForm.patchValue({
			"distributorName": userDetails.userName,
			"debitPartyId": userDetails.userId
		});
		this.claimsForm.controls["debitPartyId"].setValidators([]);
	}

	onUserNotFound() {
		this.showDistributornotFoundMessage = true;
		this.claimsForm.patchValue({
			"distributorName": "",
			"debitPartyId": ""
		});
	}

	/*
        METHOD         : onAddFormClick()
        DESCRIPTION    : Called when user clicks on "ADD CLAIM" button
				      	 Forms request object and calls addSecondarySale to add new claim
    */
	onAddFormClick() {
		if (new Date() >= this.claimPeriodStartDate && new Date() <= this.claimPeriodEndDate) {

			if (this.showLoader == false) {
				this.showLoader = true;
				if (this.isFileSelected) {
					this.isFileSelected = false
				} {


					this.s3UploadService.uploadFiles();
					this.isFileSelected = true;

				}

			}
		}
		else {
			this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't add claim outside of claim period", "alert-danger")
		}
	}

	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls clearclaimsForm() to reset invoice form to null values
    */
	onResetFormClick() {
		this.clearclaimsForm();
	}


	onSearchDistributorClick() {

		this.claimsForm.controls['distributorName'].setValidators([Validators.required]);;
		this.claimsForm.controls['distributorMobileNo'].setValidators([Validators.required, , Validators.pattern('[0-9]{10}')]);;
		this.claimsForm.controls['debitPartyId'].setValidators(null);
		this.claimsForm.patchValue({
			"debitPartyId": ""
		});

		let mobno: string = this.claimsForm.value.distributorMobileNo as string
		this.googleAnalyticsEventsService.emitEvent("Add Claims", mobno, "searchDistributor");

		console.log("mobile no", mobno.length);
		let reqObj = {
			"searchByMobile": mobno,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"clientId": this.configService.getloggedInProgramInfo().clientId

		};


		this.getProgramUser(reqObj);
	}



	onFileChange(fileInput: any) {

		console.log("File", fileInput);
		this.isFileSelected = true;
		let filePath = "Programs-Data" + "/" + "Invoices" + "/" + "invoice";
		this.s3UploadService.formParams(fileInput, filePath);

	}


	/********************************* 	END OF ONCLICK METHODS 	*********************************/






	/***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : addSecondarySale()
        DESCRIPTION    : To add a claim 
    */
	addSecondarySale(recordInfo) {
		if (recordInfo.distributorName) {

			this.googleAnalyticsEventsService.emitEvent("Add Claims", recordInfo.distributorName, "addClaim");
		}

		this.showLoader = true;
		console.log(recordInfo);
		this.claimsService.addSecondarySale(recordInfo)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						this.onResetFormClick();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						// this.googleAnalyticsEventsService.emitEvent("Add Claims", recordInfo.programId, "addClaimFailed");
						this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyName);
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
METHOD         : getProgramUser()
DESCRIPTION    : To add a claim 
*/
	getProgramUser(reqObj) {
		this.showLoader = true;

		this.claimsService.getProgramUser(reqObj)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				this.showDistributornotFoundMessage = false;
				this.showDistributorFoundMessage = false;

				this.claimsForm.patchValue({
					"distributorName": ""
				});
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						console.log(responseObject.result[0].userDetails.userId);
						console.log(responseObject.result[0].userDetails.userName);

						if (this.configService.getLoggedInUserInfo().mobileNumber !== responseObject.result[0].userDetails.mobileNumber) {

							this.onUserFound(responseObject.result[0].userDetails);
						} else {
							this.feedbackMessageComponent.updateMessage(true, "You cannot be your own distributor", "alert-danger");
						}
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.onUserNotFound();

						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/************************************ 	END OF API CALLS 	************************************************/

}