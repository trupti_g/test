/*
	Author			:	Pratik Gawand
	Description		: 	Component for Invoice  Popup 
	Date Created	: 	27 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ConfigService } from "../../shared-services/config.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { ChannelsService } from '../channels.service';
import { Observable, Subject, ReplaySubject } from "rxjs";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";

@Component({

    templateUrl: './invoice-claims-operations.component.html',
    styleUrls: ['./claims-details-operations.component.css'],
    providers: [],


})
export class InvoiceClaimsOperationsComponent implements OnInit {
    public invoiceForm: FormGroup;
    private addItems: boolean = false;
    private showLoader: boolean;
    private programInfo: any;
    private invoiceInfo: any;


    // Admin
    public productArr: any = [];
    public brandArr: any = [];

    public distributorArr: any = [];
    public requestObj: any = {};
    public programUser: any = [];
    public secondarySaleUserBeingProcessed: any = {};
    public isProduct: boolean;
    public isValue: boolean;
    public productSearching: boolean;
    public searchProductFailed: boolean;
    public brandSearching: boolean;
    public searchBrandFailed: boolean;
    public showSearchField: boolean[] = [];
    public isReject: boolean;
    public isApprove: boolean;
    public isRejected: boolean;
    public isApproved: boolean;
    public isReturned: boolean;
    public isEditPrefilledProduct: boolean;
    public isDebitPartyMapped: boolean;
    public isCreditPartyMapped: boolean;

    public userMobileSearching: boolean;
    public userMobileSearchFailed: boolean;
    public searchSuccess: boolean;
    public mobileNoErr: boolean;
    public isSearch: boolean;
    public parentObj: any = {};
    public packagingUnitArr: any[][] = [];
    public index: number;
    private isBrochureError: boolean;
    private invoiceSelectCount: number;
    public apiCounter: number;
    private clientId = this.configService.getloggedInProgramInfo().clientId;
    private programId = this.configService.getloggedInProgramInfo().programId;
    private roleInfo = this.configService.getloggedInProgramUser();
    private canApprove: boolean;
    public clientColor: any;
    // private is = this.configService.getloggedInProgramInfo().programId;

    public paymentStatus: string;
    public isPaymentQualificationSet: boolean = this.configService.getloggedInProgramInfo().isPaymentQualificationSet;

    //Admin




    constructor(public dialogRef: MdDialogRef<InvoiceClaimsOperationsComponent>,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private channelsService: ChannelsService,

    ) {
        this.clientColor = this.configService.getThemeColor();
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                ga('set', 'page', "Claims Details");
                ga('send', 'pageview');
            }
        });

        this.isApproved = false;
        this.isRejected = false;
        this.isReturned = false;




        //   this.invoiceForm = this.formBuilder.group({
        //       "productId": "",
        //       "productName": ["", [Validators.required]],
        //       "packagingUnitId": ["", [Validators.required]],
        //       "packagingUnit": "",
        //       "quantity": ["", [Validators.required]],
        //       "MRP": ["", [Validators.required]],
        //       "discount": ["", [Validators.required]],
        //       "totalPrice": ["", [Validators.required]],
        //   });

        console.log('this.roleInfo.programRoleInfo.canApproveClaims', this.roleInfo.programRoleInfo.canApproveClaims);
        if (this.roleInfo.programRoleInfo.canApproveClaims) {
            console.log('in true');
            this.canApprove = false;

        } else {
            this.canApprove = true;
            console.log('in false');
        }

        this.invoiceInfo = this.channelsService.getmyClaims();
        if (this.invoiceInfo.approvalStatus === "approved") {
            this.isApproved = true;
        }

        if (this.invoiceInfo.approvalStatus === "rejected") {
            this.isRejected = true;
        }

        if (this.invoiceInfo.approvalStatus === "returned") {
            this.isReturned = true;
        }





        console.log("invoice in fo", this.invoiceInfo);
        this.programInfo = this.configService.getloggedInProgramInfo();
        if (this.programInfo.basedOn === "product") {
            this.isProduct = true;
        }
        else {
            this.isProduct = false;
        }
        if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
            this.isValue = true;
        }
        else {
            this.isValue = false;
        }

        if (this.programInfo.basedOn === "product") {
            this.isProduct = true;

            this.getProduct();
        }
        else {
            this.isProduct = false;

            this.getAllClientBrands();
        }


    }
    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        // this.invoiceForm = this.formBuilder.group({
        //     "creditPartyId": this.invoiceInfo.creditPartyId,
        //     "creditPartyName": "",
        //     "isCreditPartyMapped": this.invoiceInfo.isCreditPartyMapped,
        //     "debitPartyId": this.invoiceInfo.debitPartyId,
        //     "debitPartyName": "",
        //     "isDebitPartyMapped": this.invoiceInfo.isDebitPartyMapped,
        //     "invoiceNumber": [this.invoiceInfo.invoiceNumber, [Validators.required]],
        //     "invoiceAmount": [this.invoiceInfo.invoiceAmount, [Validators.required]],
        //     "invoiceAmountExcludingVAT": [this.invoiceInfo.invoiceAmountExcludingVAT, [Validators.required]],
        //     "invoiceDate": [this.invoiceInfo.transactionDate, [Validators.required]],
        //     "vatPercentage": [this.invoiceInfo.vatPercentage, [Validators.required]],
        //     "approvalStatus": this.invoiceInfo.approvalStatus,
        //     "reasonForRejection": "",

        //     "purchaseDetails": this.formBuilder.array([
        //         this.isProduct ? this.initProductFormArr() : this.initBrandFormArr()
        //     ]),
        // });

        this.invoiceForm = this.formBuilder.group({
            "creditPartyId": this.invoiceInfo.creditPartyId,
            "creditPartyName": "",
            "isCreditPartyMapped": this.invoiceInfo.isCreditPartyMapped,
            "debitPartyId": this.invoiceInfo.debitPartyId,
            "debitPartyName": "",
            "isDebitPartyMapped": this.invoiceInfo.isDebitPartyMapped,
            "invoiceNumber": [this.invoiceInfo.invoiceNumber,],
            "invoiceAmount": [this.invoiceInfo.invoiceAmount],
            "invoiceAmountExcludingVAT": [this.invoiceInfo.invoiceAmountExcludingVAT],
            "invoiceDate": [this.invoiceInfo.transactionDate],
            "vatPercentage": [this.invoiceInfo.vatPercentage],
            "approvalStatus": this.invoiceInfo.approvalStatus,
            "reasonForRejection": this.invoiceInfo.comment,
            "invoicePaymentStatus": "",


            "purchaseDetails": this.formBuilder.array([
                this.isProduct ? this.initProductFormArr() : this.initBrandFormArr()
            ]),
        });



        if (this.invoiceInfo.purchaseDetails !== undefined && this.invoiceInfo.purchaseDetails !== null) {
            if (this.invoiceInfo.purchaseDetails.length !== 0) {
                for (let index = 0; index < this.invoiceInfo.purchaseDetails.length - 1; index++) {
                    this.addFormClick();
                }
            }

            for (let index = 0; index < this.invoiceInfo.purchaseDetails.length; index++) {
                this.showSearchField[index] = false;
                if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
                    if (this.programInfo.basedOn === "product") {

                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "productId": this.invoiceInfo.purchaseDetails[index].productId,
                            "productName": this.invoiceInfo.purchaseDetails[index].productName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                            "MRP": this.invoiceInfo.purchaseDetails[index].MRP,
                            "discount": this.invoiceInfo.purchaseDetails[index].discount,
                            "totalPrice": this.invoiceInfo.purchaseDetails[index].totalPrice,
                        });
                        console.log(this.invoiceInfo.purchaseDetails[index].packagingUnitId, "aa");
                    }
                    else {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "brandId": this.invoiceInfo.purchaseDetails[index].brandId,
                            "brandName": this.invoiceInfo.purchaseDetails[index].brandName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                            "MRP": this.invoiceInfo.purchaseDetails[index].MRP,
                            "discount": this.invoiceInfo.purchaseDetails[index].discount,
                            "totalPrice": this.invoiceInfo.purchaseDetails[index].totalPrice,
                        });
                    }
                }
                else {
                    if (this.programInfo.basedOn === "product") {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "productId": this.invoiceInfo.purchaseDetails[index].productId,
                            "productName": this.invoiceInfo.purchaseDetails[index].productName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                        });
                    }
                    else {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "brandId": this.invoiceInfo.purchaseDetails[index].brandId,
                            "brandName": this.invoiceInfo.purchaseDetails[index].brandName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                        });
                    }
                }
            }
            if (this.isPaymentQualificationSet = true) {
                if (this.invoiceInfo.paymentStatus == "notDone") {
                    this.paymentStatus = "Pending";
                }
                if (this.invoiceInfo.paymentStatus == "done") {
                    this.paymentStatus = "Done";
                }
                if (this.invoiceInfo.paymentStatus == "partiallyDone") {
                    this.paymentStatus = "Partially Done";
                }
                if (this.invoiceInfo.paymentStatus == "pending") {
                    this.paymentStatus = "Pending";
                }

                this.invoiceForm.patchValue({
                    "invoicePaymentStatus": this.paymentStatus,
                });
            }
        }
    }


    /**
     * METHOD   : initProductFormArr
     * DESC     : It builds the formbuilder for adding product 
     */
    initProductFormArr() {
        if (this.isValue) {
            return this.formBuilder.group({
                "productId": "",
                "productName": ["", [Validators.required]],
                "packagingUnitId": ["", [Validators.required]],
                "packagingUnit": "",
                "quantity": ["", [Validators.required]],
                "MRP": ["", [Validators.required]],
                "discount": ["", [Validators.required]],
                "totalPrice": ["", [Validators.required]],

            });
        }
        else {
            return this.formBuilder.group({
                "productId": "",
                "productName": ["", [Validators.required]],
                "packagingUnitId": ["", [Validators.required]],
                "packagingUnit": "",
                "quantity": ["", [Validators.required]],
                "MRP": [""],
                "discount": [""],
                "totalPrice": [""],
            });
        }
    }

    /**
    * METHOD   : initBrandFormArr
    * DESC     : It builds the formbuilder for adding brand 
    */
    initBrandFormArr() {
        //console.log("Brandform::");
        if (this.isValue) {
            return this.formBuilder.group({
                "brandId": "",
                "brandName": ["", [Validators.required]],
                "packagingUnitId": ["", [Validators.required]],
                "packagingUnit": "",
                "quantity": ["", [Validators.required]],
                "MRP": ["", [Validators.required]],
                "discount": ["", [Validators.required]],
                "totalPrice": ["", [Validators.required]],

            });
        }
        else {
            return this.formBuilder.group({
                "brandId": "",
                "brandName": ["", [Validators.required]],
                "packagingUnitId": ["", [Validators.required]],
                "packagingUnit": "",
                "quantity": ["", [Validators.required]],
                "MRP": [""],
                "discount": [""],
                "totalPrice": [""],
            });
        }
    }

    addItemsBox() {
        this.addItems = true;
    }


    closeInvoice() {
        this.dialogRef.close(InvoiceClaimsOperationsComponent);
    }
    /**
 * METHOD   : addProductClick
 * DESC     : It listens addFormClick & adds form .
 */
    addFormClick() {
        const control = <FormArray>this.invoiceForm.controls['purchaseDetails'];
        this.isProduct ? control.push(this.initProductFormArr()) : control.push(this.initBrandFormArr());
    }

    /**
     * METHOD   : removeFormClick
     * DESC     : It listens remove form click & removes form.
     */
    removeFormClick(i: number) {
        const control = <FormArray>this.invoiceForm.controls['purchaseDetails'];
        control.removeAt(i);
        this.packagingUnitArr.splice(i, 1);
    }

    /**
  * METHOD   : indexSet
  * DESC     : method to get and set the index of the clicked form of form array.
  *
  */
    indexSet(index) {
        this.index = index;
    }


    /**
     * METHOD   : showSearch
     * DESC     : method to show search field.
     *
     */
    showSearch(i) {
        this.isEditPrefilledProduct = true;
        this.showSearchField[i] = true;
    }

    /**
 * METHOD   : setPackagingName
 * DESC     : method to patch name to packagingUnit.
 *
 */
    setPackagingName(i, packagingId) {
        // console.log("PackagingId:", packagingId);
        for (let pkg_index = 0; pkg_index < this.packagingUnitArr[i].length; pkg_index++) {
            if (packagingId === this.packagingUnitArr[i][pkg_index].packagingId) {
                this.invoiceForm['controls']['purchaseDetails']['controls'][i].patchValue({
                    "packagingUnit": this.packagingUnitArr[i][pkg_index].packagingName,
                });
            }
        }
    }
    /**
 * METHOD   : calcPrice
 * DESC     : method to calculate price.
 *
 */
    calcPrice(i) {
        if (this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.quantity !== "" && this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.MRP !== "" && this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.discount !== "") {
            this.invoiceForm['controls']['purchaseDetails']['controls'][i].patchValue({
                "totalPrice": (this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.quantity * (this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.MRP -
                    ((this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.discount / 100) * this.invoiceForm['controls']['purchaseDetails']['controls'][i].value.MRP)))
            });
        }
    }

    /**
* METHOD   : approveClick
* DESC     : APPROVE THE CLAIM
*/
    approveClick() {
        this.requestObjectFill();
        this.updateSecondarySale();
    }


    /**
   * METHOD   : rejectClick
    * DESC     : REJECT THE CLAIM
   */
    rejectClick() {
        this.requestObj = {};
        this.requestObj.updateInfo = {};
        this.requestObj.updateInfo.approvalStatus = "rejected";
        this.requestObj.updateInfo.comment = this.invoiceForm.value.reasonForRejection;
        this.requestObj.programId = this.programId;
        this.requestObj.clientId = this.clientId;
        this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
        this.requestObj.recordId = this.invoiceInfo.secondarySalesId;
        this.updateSecondarySale();
    }




    /**
    * METHOD   : requestObjectFill
    * DESC     : methods to fill the requestObj before API call.
    *
    */
    requestObjectFill() {
        this.requestObj = {}
        this.requestObj.updateInfo = {};
        this.requestObj.updateInfo.purchaseDetails = [{}];
        this.requestObj.updateInfo.approvalStatus = "approved";
        this.requestObj.updateInfo.comment = this.invoiceForm.value.reasonForRejection;
        this.requestObj.programId = this.programId;
        this.requestObj.clientId = this.clientId;
        this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
        this.requestObj.recordId = this.invoiceInfo.secondarySalesId;


        if (this.invoiceForm.value.creditPartyId !== this.invoiceInfo.creditPartyId) {
            this.requestObj.updateInfo.creditPartyId = this.invoiceForm.value.creditPartyId;
        }
        if (this.invoiceForm.value.debitPartyId !== this.invoiceInfo.debitPartyId) {
            this.requestObj.updateInfo.debitPartyId = this.invoiceForm.value.debitPartyId;
        }
        if (this.invoiceForm.value.invoiceNumber !== this.invoiceInfo.invoiceNumber) {
            this.requestObj.updateInfo.invoiceNumber = this.invoiceForm.value.invoiceNumber;
        }
        if (this.invoiceForm.value.invoiceAmount !== this.invoiceInfo.invoiceAmount) {
            this.requestObj.updateInfo.invoiceAmount = this.invoiceForm.value.invoiceAmount;
        }
        if (this.invoiceForm.value.invoiceAmountExcludingVAT !== this.invoiceInfo.invoiceAmountExcludingVAT) {
            this.requestObj.updateInfo.invoiceAmountExcludingVAT = this.invoiceForm.value.invoiceAmountExcludingVAT;
        }
        if (this.invoiceForm.value.invoiceDate !== this.invoiceInfo.transactionDate) {
            this.requestObj.updateInfo.transactionDate = this.invoiceForm.value.invoiceDate;
        }
        if (this.invoiceForm.value.vatPercentage !== this.invoiceInfo.vatPercentage) {
            this.requestObj.updateInfo.vatPercentage = this.invoiceForm.value.vatPercentage;
        }

        if (this.isProduct === true) {
            if (this.isValue) {
                this.requestObj.updateInfo.purchaseDetails = this.invoiceForm.value.purchaseDetails;

                for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
                    if (this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName) {
                        this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName;
                    }
                    this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName;
                }
            }
            else {

                this.requestObj.updateInfo.purchaseDetails = this.invoiceForm.value.purchaseDetails;

                for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
                    if (this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName) {
                        this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName;
                    }
                    this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].MRP;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].discount;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].totalPrice;
                }
            }
        }
        else {
            if (this.isValue) {
                this.requestObj.updateInfo.purchaseDetails = this.invoiceForm.value.purchaseDetails;
                for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
                    if (this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName) {
                        this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName;
                    }
                    this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName;
                }

            }
            else {
                this.requestObj.updateInfo.purchaseDetails = this.invoiceForm.value.purchaseDetails;
                for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
                    if (this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName) {
                        this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName;
                    }
                    this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].MRP;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].discount;
                    delete this.requestObj.updateInfo.purchaseDetails[p_index].totalPrice;
                }
            }
        }

        // console.log("Request Object:", this.requestObj);

    }

    formFill() {
        this.invoiceForm.patchValue({
            "creditPartyId": this.invoiceInfo.creditPartyId === "" ? "Not Mapped" : this.invoiceInfo.creditPartyId,
            "creditPartyName": "",
            "isCreditPartyMapped": this.invoiceInfo.isCreditPartyMapped ? "Yes" : "No",
            "debitPartyId": this.invoiceInfo.debitPartyId === "" ? "Not Mapped" : this.invoiceInfo.debitPartyId,
            "debitPartyName": "",
            "isDebitPartyMapped": this.invoiceInfo.isDebitPartyMapped ? "Yes" : "No",
            "invoiceNumber": this.invoiceInfo.invoiceNumber,
            "invoiceAmount": this.invoiceInfo.invoiceAmount,
            "invoiceAmountExcludingVAT": this.invoiceInfo.invoiceAmountExcludingVAT,
            "invoiceDate": this.invoiceInfo.transactionDate.slice(0, 10),
            "vatPercentage": this.invoiceInfo.vatPercentage,
            "approvalStatus": this.invoiceInfo.approvalStatus,
        });


        if (this.invoiceInfo.isCreditPartyMapped) {
            this.invoiceForm.patchValue({
                "creditPartyName": this.invoiceInfo.creditPartyInfo.userDetails.userName,
            });
        }
        else {
            this.invoiceForm.patchValue({
                "creditPartyName": this.invoiceInfo.creditPartyName,
            });
        }

        if (this.invoiceInfo.isDebitPartyMapped) {
            this.invoiceForm.patchValue({
                "debitPartyName": this.invoiceInfo.debitPartyInfo.userDetails.userName,
            });
        }
        else {
            this.invoiceForm.patchValue({
                "debitPartyName": this.invoiceInfo.debitPartyName,
            });
        }
        // console.log("S.S Details::", this.invoiceInfo);
        // this.invoiceDownloadLink = this.invoiceInfo.invoiceDocument;
        if (this.invoiceInfo.purchaseDetails !== undefined) {
            if (this.invoiceInfo.purchaseDetails.length !== 0) {
                for (let index = 0; index < this.invoiceInfo.purchaseDetails.length - 1; index++) {
                    // console.log('This.');
                    // this.addFormClick();
                }
            }

            for (let index = 0; index < this.invoiceInfo.purchaseDetails.length; index++) {
                this.showSearchField[index] = false;
                if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
                    if (this.programInfo.basedOn === "product") {

                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "productId": this.invoiceInfo.purchaseDetails[index].productId,
                            "productName": this.invoiceInfo.purchaseDetails[index].productName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                            "MRP": this.invoiceInfo.purchaseDetails[index].MRP,
                            "discount": this.invoiceInfo.purchaseDetails[index].discount,
                            "totalPrice": this.invoiceInfo.purchaseDetails[index].totalPrice,
                        });

                    }
                    else {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "brandId": this.invoiceInfo.purchaseDetails[index].brandId,
                            "brandName": this.invoiceInfo.purchaseDetails[index].brandName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                            "MRP": this.invoiceInfo.purchaseDetails[index].MRP,
                            "discount": this.invoiceInfo.purchaseDetails[index].discount,
                            "totalPrice": this.invoiceInfo.purchaseDetails[index].totalPrice,
                        });
                    }
                }
                else {
                    if (this.programInfo.basedOn === "product") {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "productId": this.invoiceInfo.purchaseDetails[index].productId,
                            "productName": this.invoiceInfo.purchaseDetails[index].productName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                        });
                    }
                    else {
                        this.invoiceForm['controls']['purchaseDetails']['controls'][index].patchValue({
                            "brandId": this.invoiceInfo.purchaseDetails[index].brandId,
                            "brandName": this.invoiceInfo.purchaseDetails[index].brandName,
                            "packagingUnitId": this.invoiceInfo.purchaseDetails[index].packagingUnitId,
                            "packagingUnit": this.invoiceInfo.purchaseDetails[index].packagingUnit,
                            "quantity": this.invoiceInfo.purchaseDetails[index].quantity,
                        });
                    }
                }
            }
        }

        if (this.isRejected) {
            this.isReject = true;
            this.invoiceForm.patchValue({
                "reasonForRejection": this.invoiceInfo.comment,
            });
        } else if (this.isApproved) {
            this.isApprove = true;
            this.invoiceForm.patchValue({
                "reasonForRejection": this.invoiceInfo.comment,
            });
        }

        if (this.invoiceInfo.approvalStatus === "approved" || this.invoiceInfo.approvalStatus === "rejected") {
            let updatedAt: any = new Date(this.invoiceInfo.updatedAt).toUTCString();
            updatedAt = new Date(updatedAt);
            // console.log("Formatted Date and Time", updatedAt)
            updatedAt = String(updatedAt);
            this.invoiceForm.patchValue({
                "date": updatedAt.slice(0, 16),
                "time": updatedAt.slice(16, 25),
            });
        }
    }


    /**
     * METHOD   : searchProduct
     * DESC     : methods to search the string input for the product
     *
     */
    searchProduct = (text$: Observable<string>, i: number) =>

        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .do(() => { this.productSearching = true })
            .switchMap((term) =>

                this.channelsService.searchProduct(term)
                    // this.channelsService.searchProgramScheme(term)
                    .do(() => {
                        this.searchProductFailed = false;
                        //console.log("Parent fetch");
                    })
                    .catch(() => {
                        this.searchProductFailed = true;
                        return Observable.of([]);
                    })
            )
            .do(() => this.productSearching = false);

    productNameResultFormatter = (z: { productName: string }) => {
        return z.productName || '';
    }
    productNameInputFormatter = (z: { productName: string, }) => {
        this.parentObj = z;

        if (this.parentObj.packagingUnitArray) {
            //console.log("Brand obj::", this.parentObj);
            if (this.packagingUnitArr[this.index]) {
                this.packagingUnitArr[this.index] = this.parentObj.packagingUnitArray;
            }
            else {
                this.packagingUnitArr.push(this.parentObj.packagingUnitArray);
            }

            console.log("this.packagingUnitArr", this.packagingUnitArr);
            //console.log("packagingUnitArr obj::", this.packagingUnitArr);
            this.invoiceForm['controls']['purchaseDetails']['controls'][this.index].patchValue({
                "productId": this.parentObj.productId,
                "MRP": this.parentObj.MRP
            });
            return z.productName || '';
        }
        else {
            //console.log("Do Nothing!!");
        }
    }


    /**
     * METHOD   : searchBrand
     * DESC     : methods to search the string input for the brand
     *
     */
    searchBrand = (text$: Observable<string>, i: number) =>

        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .do(() => { this.brandSearching = true })
            .switchMap((term) =>

                this.channelsService.searchBrand(term)
                    // this.channelsService.searchProgramScheme(term)
                    .do(() => {
                        this.searchBrandFailed = false;
                        //console.log("Parent fetch");
                    })
                    .catch(() => {
                        this.searchBrandFailed = true;
                        return Observable.of([]);
                    })
            )
            .do(() => this.brandSearching = false);

    brandNameResultFormatter = (z: { brandName: string }) => {
        return z.brandName || '';
    }
    brandNameInputFormatter = (z: { brandName: string, }) => {

        this.parentObj = z;
        if (this.parentObj.packagingUnitArray) {
            //console.log("Brand obj::", this.parentObj);

            if (this.packagingUnitArr[this.index]) {
                this.packagingUnitArr[this.index] = this.parentObj.packagingUnitArray;
            }
            else {
                this.packagingUnitArr.push(this.parentObj.packagingUnitArray);
            }
            //console.log("packagingUnitArr obj::", this.packagingUnitArr);
            this.invoiceForm['controls']['purchaseDetails']['controls'][this.index].patchValue({
                "brandName": this.parentObj.brandName,
                "brandId": this.parentObj.brandId, "MRP": this.parentObj.MRP
            });
            return z.brandName || '';
        }
        else {
            //console.log("Do Nothing!!");
        }
    }

    updateSecondarySale() {
        // console.log("req obj::", this.requestObj);
        // console.log("calling function");
        this.showLoader = true;
        this.channelsService.updateSecondarySale(this.requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                // console.log("response ", responseObject);
                let responseCodes = this.configService.getStatusTokens();
                // console.log("responseObject.statusCode ", responseObject.statusCode);
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        if (responseObject.result.approvalStatus === "approved") {
                            this.invoiceInfo.approvalStatus = "approved";
                            // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
                            // updatedAt = new Date(updatedAt);
                            // // console.log("Formatted Date and Time", updatedAt)
                            // updatedAt = String(updatedAt);
                            // this.invoiceForm.patchValue({
                            //     "date": updatedAt.slice(0, 16),
                            //     "time": updatedAt.slice(16, 25),
                            //     "approvalStatus": responseObject.result.approvalStatus,
                            // });
                            // this.isApproved = true;
                        }
                        else if (responseObject.result.approvalStatus === "rejected") {
                            this.invoiceInfo.approvalStatus = "rejected";
                            // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
                            // updatedAt = new Date(updatedAt);
                            // // console.log("Formatted Date and Time", updatedAt)
                            // updatedAt = String(updatedAt);
                            // this.invoiceForm.patchValue({
                            //     "date": updatedAt.slice(0, 16),
                            //     "time": updatedAt.slice(16, 25),
                            //     "approvalStatus": responseObject.result.approvalStatus,
                            // });
                            this.isRejected = true;
                        }
                        this.dialogRef.close(InvoiceClaimsOperationsComponent);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:

                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.showLoader = false;

            }
            );
    }
    getProduct() {
        let requestObj = {
            "clientId": this.clientId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
        };
        this.productArr = [];
        // console.log("calling function", requestObj);

        this.showLoader = true;
        this.channelsService.getProduct(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                // console.log("response ", responseObject);

                let responseCodes = this.configService.getStatusTokens();
                // console.log("responseObject.statusCode ", responseObject.statusCode);
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.productArr = [];
                        this.productArr = responseObject.result;
                        // console.log(this.productArr);

                        // console.log("this.invoiceInfo.purchaseDetails.length", this.invoiceInfo.purchaseDetails.length);

                        this.packagingUnitArr = [];
                        if (this.invoiceInfo.purchaseDetails !== undefined) {
                            for (let purchase_index = 0; purchase_index < this.invoiceInfo.purchaseDetails.length; purchase_index++) {
                                for (let product_index = 0; product_index < this.productArr.length; product_index++) {
                                    if (this.productArr[product_index].productId === this.invoiceInfo.purchaseDetails[purchase_index].productId) {
                                        this.packagingUnitArr.push(this.productArr[product_index].packagingUnitArray);
                                        this.invoiceInfo.purchaseDetails[purchase_index].productName = this.productArr[product_index].productName;
                                    }
                                }
                            }
                        }
                        // console.log("pkg array::", this.packagingUnitArr);
                        // console.log("secondary sale:::", this.invoiceInfo);

                        this.formFill();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        this.productArr = [];
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
            );
    }

    getAllClientBrands() {
        let requestObj = {
            "clientId": this.clientId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
        };
        this.brandArr = [];
        this.showLoader = true;
        this.channelsService.getBrands(requestObj)
            .subscribe(
            (responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();

                // console.log(responseObject);

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.brandArr = responseObject.result;
                        // console.log(this.brandArr);
                        if (this.invoiceInfo.purchaseDetails !== undefined) {
                            for (let purchase_index = 0; purchase_index < this.invoiceInfo.purchaseDetails.length; purchase_index++) {
                                for (let brand_index = 0; brand_index < this.brandArr.length; brand_index++) {
                                    if (this.brandArr[brand_index].brandId === this.invoiceInfo.purchaseDetails[purchase_index].brandId) {
                                        this.packagingUnitArr.push(this.brandArr[brand_index].packagingUnitArray);
                                        this.invoiceInfo.purchaseDetails[purchase_index].brandName = this.brandArr[brand_index].brandName;
                                    }
                                }
                            }
                        }
                        // console.log("pkg array::", this.packagingUnitArr);
                        // console.log("secondary sale:::", this.invoiceInfo);
                        this.formFill();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;

            }
            );

    }



    /*
	METHOD         : getProgramScheme()
	DESCRIPTION    : get All scheme setup of the program
*/

    getProgramScheme() {
        let requestObj = {
            "clientId": this.clientId,
            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
        };
        this.brandArr = [];
        this.showLoader = true;
        this.channelsService.getProgramScheme(requestObj)
            .subscribe(
            (responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();

                // console.log(responseObject);

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.productArr = [];
                        this.brandArr = [];
                        if (this.programInfo.basedOn === "product") {
                            console.log("inside if");
                            this.productArr = responseObject.result;
                            this.packagingUnitArr = [];
                            if (this.invoiceInfo.purchaseDetails !== undefined) {
                                for (let purchase_index = 0; purchase_index < this.invoiceInfo.purchaseDetails.length; purchase_index++) {
                                    for (let product_index = 0; product_index < this.productArr.length; product_index++) {
                                        if (this.productArr[product_index].productId === this.invoiceInfo.purchaseDetails[purchase_index].productId) {
                                            this.packagingUnitArr.push(this.productArr[product_index].packagingUnitArray);
                                            this.invoiceInfo.purchaseDetails[purchase_index].productName = this.productArr[product_index].productName;
                                        }
                                    }
                                }
                            }
                            this.formFill();
                        }
                        else {
                            this.brandArr = responseObject.result;
                            console.log("inside else");
                            if (this.invoiceInfo.purchaseDetails !== undefined) {
                                for (let purchase_index = 0; purchase_index < this.invoiceInfo.purchaseDetails.length; purchase_index++) {
                                    for (let brand_index = 0; brand_index < this.brandArr.length; brand_index++) {
                                        if (this.brandArr[brand_index].brandId === this.invoiceInfo.purchaseDetails[purchase_index].brandId) {
                                            this.packagingUnitArr.push(this.brandArr[brand_index].packagingUnitArray);
                                            this.invoiceInfo.purchaseDetails[purchase_index].brandName = this.brandArr[brand_index].brandName;
                                        }
                                    }
                                }
                            }
                            this.formFill();
                        }
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                }
            },
            err => {
                this.showLoader = false;

            }
            );

    }

}
