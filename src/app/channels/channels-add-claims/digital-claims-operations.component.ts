/*
	Author			:	Pratik Gawand
	Description		: 	Component for Invoice  Popup 
	Date Created	: 	27 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ConfigService } from "../../shared-services/config.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { ChannelsService } from '../channels.service';
import { Observable, Subject, ReplaySubject } from "rxjs";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";



@Component({

  templateUrl: './digital-claims-operations.component.html',

  styleUrls: ['./claims-details-operations.component.css'],
  providers: [],


})
export class DigitalClaimsOperationsComponent implements OnInit {
  isReturned: boolean;
  public purchasedProduct: any = [];
  public schemeArray: any = [];

  public digitalForm: FormGroup;
  private addItems: boolean = false;
  private showLoader: boolean;
  private programInfo: any;
  private digitalInfo: any;


  // Admin
  public productArr: any = [];
  public brandArr: any = [];

  public distributorArr: any = [];
  public requestObj: any = {};
  public programUser: any = [];
  public secondarySaleUserBeingProcessed: any = {};
  public isProduct: boolean;
  public isValue: boolean;
  public productSearching: boolean;
  public searchProductFailed: boolean;
  public brandSearching: boolean;
  public searchBrandFailed: boolean;
  public showSearchField: boolean[] = [];
  public isReject: boolean;
  public isApprove: boolean;
  public isRejected: boolean;
  public isApproved: boolean;
  public isEditPrefilledProduct: boolean;
  public isDebitPartyMapped: boolean;
  public isCreditPartyMapped: boolean;

  public userMobileSearching: boolean;
  public userMobileSearchFailed: boolean;
  public searchSuccess: boolean;
  public mobileNoErr: boolean;
  public isSearch: boolean;
  public parentObj: any = {};
  public packagingUnitArr: any[][] = [];
  public index: number;
  private isBrochureError: boolean;
  private invoiceSelectCount: number;
  public apiCounter: number;
  private clientId = this.configService.getloggedInProgramInfo().clientId;
  private programId = this.configService.getloggedInProgramInfo().programId;
  public clientColor: any;
  private roleInfo = this.configService.getloggedInProgramUser();
  private canApprove: boolean;
  public paymentStatus: string;
  public isPaymentQualificationSet: boolean = this.configService.getloggedInProgramInfo().isPaymentQualificationSet;



  //Admin





  constructor(public dialogRef: MdDialogRef<DigitalClaimsOperationsComponent>,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private stringService: StringService,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    private channelsService: ChannelsService) {
    this.clientColor = this.configService.getThemeColor();
    this.router.events.subscribe(event => {
 


      //   this.digitalForm = this.formBuilder.group({
      //       "productId": "",
      //       "productName": ["", [Validators.required]],
      //       "packagingUnitId": ["", [Validators.required]],
      //       "packagingUnit": "",
      //       "quantity": ["", [Validators.required]],
      //       "MRP": ["", [Validators.required]],
      //       "discount": ["", [Validators.required]],
      //       "totalPrice": ["", [Validators.required]],
      //   });


      console.log('this.roleInfo.programRoleInfo.canApproveClaims', this.roleInfo.programRoleInfo.canApproveClaims);
      if (this.roleInfo.programRoleInfo.canApproveClaims) {
        console.log('in true');
        this.canApprove = false;

      } else {
        this.canApprove = true;
        console.log('in false');
      }



      this.digitalInfo = this.channelsService.getmyClaims();
      console.log("digi", this.digitalInfo);
      if (this.digitalInfo.approvalStatus === "approved") {
        this.isApproved = true;
      }

      if (this.digitalInfo.approvalStatus === "rejected") {
        this.isRejected = true;
      }

      if (this.digitalInfo.approvalStatus === "returned") {
        this.isReturned = true;
      }


      console.log("invoice in fo", this.digitalInfo);
      this.programInfo = this.configService.getloggedInProgramInfo();
      if (this.programInfo.basedOn === "product") {
        this.isProduct = true;
      }
      else {
        this.isProduct = false;
      }
      if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
        this.isValue = true;
      }
      else {
        this.isValue = false;
      }

      if (this.programInfo.basedOn === "product") {
        this.isProduct = true;
        this.getProduct();
      }
      else {
        this.isProduct = false;
        this.getAllClientBrands();
      }

    });

  }

  ngOnInit() {
    
    // this.getProgramScheme();
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.digitalForm = this.formBuilder.group({
      "creditPartyId": this.digitalInfo.creditPartyId,
      // "creditPartyName": "",
      "isCreditPartyMapped": this.digitalInfo.isCreditPartyMapped,
      "debitPartyId": this.digitalInfo.debitPartyId,
      // "debitPartyName": "",
      "isDebitPartyMapped": this.digitalInfo.isDebitPartyMapped,
      // "invoiceNumber": [this.digitalInfo.invoiceNumber, [Validators.required]],
      // "invoiceAmount": [this.digitalInfo.invoiceAmount, [Validators.required]],
      // "invoiceAmountExcludingVAT": [this.digitalInfo.invoiceAmountExcludingVAT, [Validators.required]],
      // "invoiceDate": [this.digitalInfo.transactionDate, [Validators.required]],
      "vatPercentage": [this.digitalInfo.vatPercentage, [Validators.required]],
      "approvalStatus": this.digitalInfo.approvalStatus,
      "reasonForRejection": this.digitalInfo.comment,
      "digitalPaymentStatus": "",

      "purchaseDetails": this.formBuilder.array([
        this.isProduct ? this.initProductFormArr() : this.initBrandFormArr()
      ]),
    });



    if (this.digitalInfo.purchaseDetails !== undefined) {
      if (this.digitalInfo.purchaseDetails.length !== 0) {
        for (let index = 0; index < this.digitalInfo.purchaseDetails.length - 1; index++) {
          this.addFormClick();
        }
      }

      for (let index = 0; index < this.digitalInfo.purchaseDetails.length; index++) {
        this.showSearchField[index] = false;
        if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
          if (this.programInfo.basedOn === "product") {

            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].productId === this.schemeArray[a].itemInfo.productId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "productName": this.schemeArray[a].itemInfo.productName
            //     });
            //   }
            // }


            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "productId": this.digitalInfo.purchaseDetails[index].productId,
              "productName": this.digitalInfo.purchaseDetails[index].productName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
              "MRP": this.digitalInfo.purchaseDetails[index].MRP,
              "discount": this.digitalInfo.purchaseDetails[index].discount,
              "totalPrice": this.digitalInfo.purchaseDetails[index].totalPrice,
            });
            console.log(this.digitalInfo.purchaseDetails[index].packagingUnitId, "aa");
          }
          else {
            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].brandId === this.schemeArray[a].itemInfo.brandId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "brandName": this.schemeArray[a].itemInfo.brandName
            //     })
            //   }
            // }
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "brandId": this.digitalInfo.purchaseDetails[index].brandId,
              "brandName": this.digitalInfo.purchaseDetails[index].brandName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
              "MRP": this.digitalInfo.purchaseDetails[index].MRP,
              "discount": this.digitalInfo.purchaseDetails[index].discount,
              "totalPrice": this.digitalInfo.purchaseDetails[index].totalPrice,
            });
          }
        }
        else {
          //new
          // for (var a = 0; a < this.schemeArray.length; a++) {
          //   if (this.digitalInfo.purchaseDetails[index].productId === this.schemeArray[a].itemInfo.productId) {
          //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
          //       "productName": this.schemeArray[a].itemInfo.productName
          //     })
          //   }
          // }

          if (this.programInfo.basedOn === "product") {
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "productId": this.digitalInfo.purchaseDetails[index].productId,
              "productName": this.digitalInfo.purchaseDetails[index].productName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
            });

          }
          else {
            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].brandId === this.schemeArray[a].itemInfo.brandId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "brandName": this.schemeArray[a].itemInfo.brandName
            //     })
            //   }
            // }
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "brandId": this.digitalInfo.purchaseDetails[index].brandId,
              "brandName": this.digitalInfo.purchaseDetails[index].brandName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
            });
          }
        }
      }
      if (this.isPaymentQualificationSet = true) {
        if (this.digitalInfo.paymentStatus == "notDone") {
          this.paymentStatus = "Pending";
        }
        if (this.digitalInfo.paymentStatus == "done") {
          this.paymentStatus = "Done";
        }
        if (this.digitalInfo.paymentStatus == "partiallyDone") {
          this.paymentStatus = "Partially Done";
        }
        if (this.digitalInfo.paymentStatus == "pending") {
          this.paymentStatus = "Pending";
        }

        this.digitalForm.patchValue({
          "digitalPaymentStatus": this.paymentStatus,
        });
      }
    }
  }

  ngAfterContentChecked() {
    console.log("approved??", this.isApproved);
    console.log("rejected??", this.isRejected);
    console.log("FORM??", this.digitalForm.valid);
  }
  /**
   * METHOD   : initProductFormArr
   * DESC     : It builds the formbuilder for adding product 
   */
  initProductFormArr() {
    if (this.isValue) {
      return this.formBuilder.group({
        "productId": "",
        "productName": ["", []],
        "packagingUnitId": ["", []],
        "packagingUnit": "",
        "quantity": ["", []],
        "MRP": ["", []],
        "discount": ["", []],
        "totalPrice": ["", []],

      });
    }
    else {
      return this.formBuilder.group({
        "productId": "",
        "productName": ["", []],
        "packagingUnitId": ["", []],
        "packagingUnit": "",
        "quantity": ["", []],
        "MRP": [""],
        "discount": [""],
        "totalPrice": [""],
      });
    }
  }

  /**
  * METHOD   : initBrandFormArr
  * DESC     : It builds the formbuilder for adding brand 
  */
  initBrandFormArr() {
    //console.log("Brandform::");
    if (this.isValue) {
      return this.formBuilder.group({
        "brandId": "",
        "brandName": ["", [Validators.required]],
        "packagingUnitId": ["", [Validators.required]],
        "packagingUnit": "",
        "quantity": ["", [Validators.required]],
        "MRP": ["", [Validators.required]],
        "discount": ["", [Validators.required]],
        "totalPrice": ["", [Validators.required]],

      });
    }
    else {
      return this.formBuilder.group({
        "brandId": "",
        "brandName": ["", [Validators.required]],
        "packagingUnitId": ["", [Validators.required]],
        "packagingUnit": "",
        "quantity": ["", [Validators.required]],
        "MRP": [""],
        "discount": [""],
        "totalPrice": [""],
      });
    }
  }

  closeInvoice() {

    this.dialogRef.close(DigitalClaimsOperationsComponent);
  }
  /**
* METHOD   : addProductClick
* DESC     : It listens addFormClick & adds form .
*/
  addFormClick() {
    const control = <FormArray>this.digitalForm.controls['purchaseDetails'];
    this.isProduct ? control.push(this.initProductFormArr()) : control.push(this.initBrandFormArr());
  }

  /**
   * METHOD   : removeFormClick
   * DESC     : It listens remove form click & removes form.
   */
  removeFormClick(i: number) {
    const control = <FormArray>this.digitalForm.controls['purchaseDetails'];
    control.removeAt(i);
    this.packagingUnitArr.splice(i, 1);
  }

  /**
* METHOD   : indexSet
* DESC     : method to get and set the index of the clicked form of form array.
*
*/
  indexSet(index) {
    this.index = index;
  }


  /**
   * METHOD   : showSearch
   * DESC     : method to show search field.
   *
   */
  showSearch(i) {
    this.isEditPrefilledProduct = true;
    this.showSearchField[i] = true;

    for (let index = 0; index < this.digitalInfo.purchaseDetails.length; index++) {
      this.showSearchField[index] = false;
      if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
        if (this.programInfo.basedOn === "product") {

          //new
          for (var a = 0; a < this.schemeArray.length; a++) {
            if (this.digitalInfo.purchaseDetails[index].productId === this.schemeArray[a].itemInfo.productId) {
              this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
                "productName": this.digitalInfo.purchaseDetails[index].productName,
              });
            }
          }
        }
        else {
          //new
          for (var a = 0; a < this.schemeArray.length; a++) {
            if (this.digitalInfo.purchaseDetails[index].brandId === this.schemeArray[a].itemInfo.brandId) {
              this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
                "brandName": this.digitalInfo.purchaseDetails[index].brandName,
              });
            }
          }
        }
      }
    }
  }

  /**
* METHOD   : setPackagingName
* DESC     : method to patch name to packagingUnit.
*
*/
  setPackagingName(i, packagingId) {
    // console.log("PackagingId:", packagingId);
    for (let pkg_index = 0; pkg_index < this.packagingUnitArr[i].length; pkg_index++) {
      if (packagingId === this.packagingUnitArr[i][pkg_index].packagingId) {
        this.digitalForm['controls']['purchaseDetails']['controls'][i].patchValue({
          "packagingUnit": this.packagingUnitArr[i][pkg_index].packagingName,
        });
      }
    }
  }
  /**
* METHOD   : calcPrice
* DESC     : method to calculate price.
*
*/
  calcPrice(i) {
    console.log('In cal');
    if (this.digitalForm['controls']['purchaseDetails']['controls'][i].value.quantity !== "" && this.digitalForm['controls']['purchaseDetails']['controls'][i].value.MRP !== "" && this.digitalForm['controls']['purchaseDetails']['controls'][i].value.discount !== "") {
      this.digitalForm['controls']['purchaseDetails']['controls'][i].patchValue({
        "totalPrice": (this.digitalForm['controls']['purchaseDetails']['controls'][i].value.quantity * (this.digitalForm['controls']['purchaseDetails']['controls'][i].value.MRP -
          ((this.digitalForm['controls']['purchaseDetails']['controls'][i].value.discount / 100) * this.digitalForm['controls']['purchaseDetails']['controls'][i].value.MRP)))
      });
    }
  }

  /**
* METHOD   : approveClick
* DESC     : APPROVE THE CLAIM
*/
  approveClick() {
    this.requestObjectFill();
    this.updateSecondarySale();
  }


  /**
 * METHOD   : rejectClick
  * DESC     : REJECT THE CLAIM
 */
  rejectClick() {
    this.requestObj = {};
    this.requestObj.updateInfo = {};
    this.requestObj.updateInfo.approvalStatus = "rejected";
    this.requestObj.updateInfo.comment = this.digitalForm.value.reasonForRejection;
    this.requestObj.programId = this.programId;
    this.requestObj.clientId = this.clientId;
    this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
    this.requestObj.recordId = this.digitalInfo.secondarySalesId;
    this.updateSecondarySale();
  }




  /**
  * METHOD   : requestObjectFill
  * DESC     : methods to fill the requestObj before API call.
  *
  */
  requestObjectFill() {
    this.requestObj = {}
    this.requestObj.updateInfo = {};
    this.requestObj.updateInfo.purchaseDetails = [{}];
    this.requestObj.updateInfo.approvalStatus = "approved";
    this.requestObj.updateInfo.comment = this.digitalForm.value.reasonForRejection;
    this.requestObj.programId = this.programId;
    this.requestObj.clientId = this.clientId;
    this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
    this.requestObj.recordId = this.digitalInfo.secondarySalesId;


    if (this.digitalForm.value.creditPartyId !== this.digitalInfo.creditPartyId) {
      this.requestObj.updateInfo.creditPartyId = this.digitalForm.value.creditPartyId;
    }
    if (this.digitalForm.value.debitPartyId !== this.digitalInfo.debitPartyId) {
      this.requestObj.updateInfo.debitPartyId = this.digitalForm.value.debitPartyId;
    }
    if (this.digitalForm.value.invoiceNumber !== this.digitalInfo.invoiceNumber) {
      this.requestObj.updateInfo.invoiceNumber = this.digitalForm.value.invoiceNumber;
    }
    if (this.digitalForm.value.invoiceAmount !== this.digitalInfo.invoiceAmount) {
      this.requestObj.updateInfo.invoiceAmount = this.digitalForm.value.invoiceAmount;
    }
    if (this.digitalForm.value.invoiceAmountExcludingVAT !== this.digitalInfo.invoiceAmountExcludingVAT) {
      this.requestObj.updateInfo.invoiceAmountExcludingVAT = this.digitalForm.value.invoiceAmountExcludingVAT;
    }
    if (this.digitalForm.value.invoiceDate !== this.digitalInfo.transactionDate) {
      this.requestObj.updateInfo.transactionDate = this.digitalForm.value.invoiceDate;
    }
    if (this.digitalForm.value.vatPercentage !== this.digitalInfo.vatPercentage) {
      this.requestObj.updateInfo.vatPercentage = this.digitalForm.value.vatPercentage;
    }

    if (this.isProduct === true) {
      if (this.isValue) {
        this.requestObj.updateInfo.purchaseDetails = this.digitalForm.value.purchaseDetails;

        for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
          if (this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName) {
            this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName;
          }
          this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName;
        }
      }
      else {

        this.requestObj.updateInfo.purchaseDetails = this.digitalForm.value.purchaseDetails;

        for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
          if (this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName) {
            this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName.productName;
          }
          this.requestObj.updateInfo.purchaseDetails[p_index].productName = this.requestObj.updateInfo.purchaseDetails[p_index].productName;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].MRP;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].discount;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].total;
        }
      }
    }
    else {
      if (this.isValue) {
        this.requestObj.updateInfo.purchaseDetails = this.digitalForm.value.purchaseDetails;
        for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
          if (this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName) {
            this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName;
          }
          this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName;
        }

      }
      else {
        this.requestObj.updateInfo.purchaseDetails = this.digitalForm.value.purchaseDetails;
        for (let p_index = 0; p_index < this.requestObj.updateInfo.purchaseDetails.length; p_index++) {
          if (this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName) {
            this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName.brandName;
          }
          this.requestObj.updateInfo.purchaseDetails[p_index].brandName = this.requestObj.updateInfo.purchaseDetails[p_index].brandName;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].MRP;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].discount;
          delete this.requestObj.updateInfo.purchaseDetails[p_index].total;
        }
      }
    }

    // console.log("Request Object:", this.requestObj);

  }

  formFill() {
    this.digitalForm.patchValue({
      "creditPartyId": this.digitalInfo.creditPartyId === "" ? "Not Mapped" : this.digitalInfo.creditPartyId,
      "creditPartyName": "",
      "isCreditPartyMapped": this.digitalInfo.isCreditPartyMapped ? "Yes" : "No",
      "debitPartyId": this.digitalInfo.debitPartyId === "" ? "Not Mapped" : this.digitalInfo.debitPartyId,
      "debitPartyName": "",
      "isDebitPartyMapped": this.digitalInfo.isDebitPartyMapped ? "Yes" : "No",
      // "invoiceNumber": this.digitalInfo.invoiceNumber,
      // "invoiceAmount": this.digitalInfo.invoiceAmount,
      // "invoiceAmountExcludingVAT": this.digitalInfo.invoiceAmountExcludingVAT,
      // "invoiceDate": this.digitalInfo.transactionDate.slice(0, 10),
      "vatPercentage": this.digitalInfo.vatPercentage,
      "approvalStatus": this.digitalInfo.approvalStatus,
    });


    if (this.digitalInfo.isCreditPartyMapped) {
      this.digitalForm.patchValue({
        "creditPartyName": this.digitalInfo.creditPartyInfo.userDetails.userName,
      });
    }
    else {
      this.digitalForm.patchValue({
        "creditPartyName": this.digitalInfo.creditPartyName,
      });
    }

    if (this.digitalInfo.isDebitPartyMapped) {
      this.digitalForm.patchValue({
        "debitPartyName": this.digitalInfo.debitPartyInfo.userDetails.userName,
      });
    }
    else {
      this.digitalForm.patchValue({
        "debitPartyName": this.digitalInfo.debitPartyName,
      });
    }
    // console.log("S.S Details::", this.digitalInfo);
    // this.invoiceDownloadLink = this.digitalInfo.invoiceDocument;
    if (this.digitalInfo.purchaseDetails !== undefined) {
      if (this.digitalInfo.purchaseDetails.length !== 0) {
        for (let index = 0; index < this.digitalInfo.purchaseDetails.length - 1; index++) {
          // console.log('This.');
          // this.addFormClick();
        }
      }

      for (let index = 0; index < this.digitalInfo.purchaseDetails.length; index++) {
        this.showSearchField[index] = false;
        if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
          if (this.programInfo.basedOn === "product") {
            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].productId === this.schemeArray[a].itemInfo.productId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "productName": this.schemeArray[a].itemInfo.productName
            //     });
            //   }
            // }

            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "productId": this.digitalInfo.purchaseDetails[index].productId,
              "productName": this.digitalInfo.purchaseDetails[index].productName,
              // "productName": this.purchasedProduct[a],
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
              "MRP": this.digitalInfo.purchaseDetails[index].MRP,
              "discount": this.digitalInfo.purchaseDetails[index].discount,
              "totalPrice": this.digitalInfo.purchaseDetails[index].total,
            });



          }
          else {
            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].brandId === this.schemeArray[a].itemInfo.brandId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "brandName": this.schemeArray[a].itemInfo.brandName
            //     })
            //   }
            // }
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "brandId": this.digitalInfo.purchaseDetails[index].brandId,
              "brandName": this.digitalInfo.purchaseDetails[index].brandName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
              "MRP": this.digitalInfo.purchaseDetails[index].MRP,
              "discount": this.digitalInfo.purchaseDetails[index].discount,
              "totalPrice": this.digitalInfo.purchaseDetails[index].totalPrice,
            });

          }
        }
        else {
          //new
          // for (var a = 0; a < this.schemeArray.length; a++) {
          //   if (this.digitalInfo.purchaseDetails[index].productId === this.schemeArray[a].itemInfo.productId) {
          //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
          //       "productName": this.schemeArray[a].itemInfo.productName
          //     })
          //   }
          // }
          if (this.programInfo.basedOn === "product") {
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "productId": this.digitalInfo.purchaseDetails[index].productId,
              "productName": this.digitalInfo.purchaseDetails[index].productName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
            });
          }
          else {
            //new
            // for (var a = 0; a < this.schemeArray.length; a++) {
            //   if (this.digitalInfo.purchaseDetails[index].brandId === this.schemeArray[a].itemInfo.brandId) {
            //     this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
            //       "brandName": this.schemeArray[a].itemInfo.brandName
            //     })
            //   }
            // }
            this.digitalForm['controls']['purchaseDetails']['controls'][index].patchValue({
              "brandId": this.digitalInfo.purchaseDetails[index].brandId,
              "brandName": this.digitalInfo.purchaseDetails[index].brandName,
              "packagingUnitId": this.digitalInfo.purchaseDetails[index].packagingUnitId,
              "packagingUnit": this.digitalInfo.purchaseDetails[index].packagingUnit,
              "quantity": this.digitalInfo.purchaseDetails[index].quantity,
            });
          }
        }
      }
    }

    if (this.isRejected) {
      this.isReject = true;
      this.digitalForm.patchValue({
        "reasonForRejection": this.digitalInfo.comment,
      });
    } else if (this.isApproved) {
      this.isApprove = true;
      this.digitalForm.patchValue({
        "reasonForRejection": this.digitalInfo.comment,
      });
    }

    if (this.digitalInfo.approvalStatus === "approved" || this.digitalInfo.approvalStatus === "rejected") {
      let updatedAt: any = new Date(this.digitalInfo.updatedAt).toUTCString();
      updatedAt = new Date(updatedAt);
      // console.log("Formatted Date and Time", updatedAt)
      updatedAt = String(updatedAt);
      this.digitalForm.patchValue({
        "date": updatedAt.slice(0, 16),
        "time": updatedAt.slice(16, 25),
      });
    }
  }


  /**
   * METHOD   : searchProduct
   * DESC     : methods to search the string input for the product
   *
   */
  searchProduct = (text$: Observable<string>, i: number) =>

    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .do(() => { this.productSearching = true })
      .switchMap((term) =>

        this.channelsService.searchProduct(term)
          // this.channelsService.searchProgramScheme(term)
          .do(() => {
            this.searchProductFailed = false;
            //console.log("Parent fetch");
          })
          .catch(() => {
            this.searchProductFailed = true;
            return Observable.of([]);
          })
      )
      .do(() => this.productSearching = false);

  productNameResultFormatter = (z: { productName: string }) => {
    console.log("zzzzzz", z);
    console.log("productNAMEEEEE", z.productName);
    return z.productName || '';
  }
  productNameInputFormatter = (z: { productName: string }) => {
    console.log("zzzzzz", z);
    console.log("productNAMEEEEE", z.productName);
    this.parentObj = z;

    if (this.parentObj.packagingUnitArray) {
      //console.log("Brand obj::", this.parentObj);
      if (this.packagingUnitArr[this.index]) {
        this.packagingUnitArr[this.index] = this.parentObj.packagingUnitArray;
      }
      else {
        this.packagingUnitArr.push(this.parentObj.packagingUnitArray);
      }

      console.log("this.packagingUnitArr", this.packagingUnitArr);
      //console.log("packagingUnitArr obj::", this.packagingUnitArr);
      this.digitalForm['controls']['purchaseDetails']['controls'][this.index].patchValue({
        "productId": this.parentObj.productId,
        "MRP": this.parentObj.MRP
      });
      return this.parentObj.productName || '';
    }
    else {
      //console.log("Do Nothing!!");
    }
  }


  /**
   * METHOD   : searchBrand
   * DESC     : methods to search the string input for the brand
   *
   */
  searchBrand = (text$: Observable<string>, i: number) =>

    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .do(() => { this.brandSearching = true })
      .switchMap((term) =>

        this.channelsService.searchBrand(term)
          // this.channelsService.searchProgramScheme(term)
          .do(() => {
            this.searchBrandFailed = false;
            //console.log("Parent fetch");
          })
          .catch(() => {
            this.searchBrandFailed = true;
            return Observable.of([]);
          })
      )
      .do(() => this.brandSearching = false);

  brandNameResultFormatter = (z: { brandName: string }) => {
    return z.brandName || '';
  }
  brandNameInputFormatter = (z: { brandName: string }) => {

    this.parentObj = z;
    if (this.parentObj.packagingUnitArray) {
      //console.log("Brand obj::", this.parentObj);

      if (this.packagingUnitArr[this.index]) {
        this.packagingUnitArr[this.index] = this.parentObj.packagingUnitArray;
      }
      else {
        this.packagingUnitArr.push(this.parentObj.packagingUnitArray);
      }
      //console.log("packagingUnitArr obj::", this.packagingUnitArr);
      this.digitalForm['controls']['purchaseDetails']['controls'][this.index].patchValue({
        "brandName": this.parentObj.brandName,
        "brandId": this.parentObj.brandId, "MRP": this.parentObj.MRP
      });
      return this.parentObj.brandName || '';
    }
    else {
      //console.log("Do Nothing!!");
    }
  }

  updateSecondarySale() {
    // console.log("req obj::", this.requestObj);
    // console.log("calling function");
    this.showLoader = true;
    this.channelsService.updateSecondarySale(this.requestObj)
      .subscribe((responseObject) => {
        this.showLoader = false;
        // console.log("response ", responseObject);
        let responseCodes = this.configService.getStatusTokens();
        // console.log("responseObject.statusCode ", responseObject.statusCode);
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:

            if (responseObject.result.approvalStatus === "approved") {
              this.digitalInfo.approvalStatus = "approved";

              // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
              // updatedAt = new Date(updatedAt);
              // // console.log("Formatted Date and Time", updatedAt)
              // updatedAt = String(updatedAt);
              // this.digitalForm.patchValue({
              //     "date": updatedAt.slice(0, 16),
              //     "time": updatedAt.slice(16, 25),
              //     "approvalStatus": responseObject.result.approvalStatus,
              // });
              // this.isApproved = true;
            }
            else if (responseObject.result.approvalStatus === "rejected") {
              this.digitalInfo.approvalStatus = "rejected";
              // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
              // updatedAt = new Date(updatedAt);
              // // console.log("Formatted Date and Time", updatedAt)
              // updatedAt = String(updatedAt);
              // this.digitalForm.patchValue({
              //     "date": updatedAt.slice(0, 16),
              //     "time": updatedAt.slice(16, 25),
              //     "approvalStatus": responseObject.result.approvalStatus,
              // });
              this.isRejected = true;
            }
            this.dialogRef.close(DigitalClaimsOperationsComponent);
            // var a: any = "";
            // this.channelsService.approval.next(a);
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            break;
          case responseCodes.RESP_ALREADY_EXIST:

            break;
          case responseCodes.RESP_BLOCKED:
            break;
        }
      },
      err => {
        this.showLoader = false;

      }
      );
  }
  getProduct() {
    let requestObj = {
      "clientId": this.clientId,
      "frontendUserInfo": this.configService.getFrontEndUserInfo(),
    };
    this.productArr = [];
    // console.log("calling function", requestObj);

    this.showLoader = true;
    this.channelsService.getProduct(requestObj)
      .subscribe((responseObject) => {
        this.showLoader = false;
        // console.log("response ", responseObject);

        let responseCodes = this.configService.getStatusTokens();
        // console.log("responseObject.statusCode ", responseObject.statusCode);
        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:
            this.productArr = [];
            this.productArr = responseObject.result;
            // console.log(this.productArr);

            // console.log("this.digitalInfo.purchaseDetails.length", this.digitalInfo.purchaseDetails.length);

            this.packagingUnitArr = [];
            if (this.digitalInfo.purchaseDetails !== undefined) {
              for (let purchase_index = 0; purchase_index < this.digitalInfo.purchaseDetails.length; purchase_index++) {
                for (let product_index = 0; product_index < this.productArr.length; product_index++) {
                  if (this.productArr[product_index].productId === this.digitalInfo.purchaseDetails[purchase_index].productId) {
                    this.packagingUnitArr.push(this.productArr[product_index].packagingUnitArray);
                    this.digitalInfo.purchaseDetails[purchase_index].productName = this.productArr[product_index].productName;
                  }
                }
              }
            }
            // console.log("pkg array::", this.packagingUnitArr);
            // console.log("secondary sale:::", this.digitalInfo);

            // this.formFill();
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            this.productArr = [];
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
          case responseCodes.RESP_BLOCKED:
            break;
        }
      },
      err => {
        this.showLoader = false;
      }
      );
  }

  getAllClientBrands() {
    let requestObj = {
      "clientId": this.clientId,
      "frontendUserInfo": this.configService.getFrontEndUserInfo(),
    };
    this.brandArr = [];
    this.showLoader = true;
    this.channelsService.getBrands(requestObj)
      .subscribe(
      (responseObject) => {
        this.showLoader = false;
        let responseCodes = this.configService.getStatusTokens();

        // console.log(responseObject);

        switch (responseObject.statusCode) {
          case responseCodes.RESP_ROLLBACK_ERROR:
            break;
          case responseCodes.RESP_SERVER_ERROR:
            break;
          case responseCodes.RESP_SUCCESS:
            this.brandArr = responseObject.result;
            // console.log(this.brandArr);
            if (this.digitalInfo.purchaseDetails !== undefined) {
              for (let purchase_index = 0; purchase_index < this.digitalInfo.purchaseDetails.length; purchase_index++) {
                for (let brand_index = 0; brand_index < this.brandArr.length; brand_index++) {
                  if (this.brandArr[brand_index].brandId === this.digitalInfo.purchaseDetails[purchase_index].brandId) {
                    this.packagingUnitArr.push(this.brandArr[brand_index].packagingUnitArray);
                    this.digitalInfo.purchaseDetails[purchase_index].brandName = this.brandArr[brand_index].brandName;
                  }
                }
              }
            }
            // console.log("pkg array::", this.packagingUnitArr);
            // console.log("secondary sale:::", this.digitalInfo);
            // this.formFill();
            break;
          case responseCodes.RESP_AUTH_FAIL:
            break;
          case responseCodes.RESP_FAIL:
            break;
          case responseCodes.RESP_ALREADY_EXIST:
            break;
        }
      },
      err => {
        this.showLoader = false;

      }
      );

  }

  // getProgramScheme() {
  //   let requestObj = {};
  //   this.brandArr = [];
  //   this.showLoader = true;
  //   this.channelsService.getProgramScheme(requestObj)
  //     .subscribe(
  //     (responseObject) => {
  //       this.showLoader = false;
  //       let responseCodes = this.configService.getStatusTokens();

  //       // console.log(responseObject);

  //       switch (responseObject.statusCode) {
  //         case responseCodes.RESP_ROLLBACK_ERROR:
  //           break;
  //         case responseCodes.RESP_SERVER_ERROR:
  //           break;
  //         case responseCodes.RESP_SUCCESS:
  //           this.schemeArray = responseObject.result;
  //           if (this.programInfo.basedOn === "product") {
  //             for (var i = 0; i < this.digitalInfo.purchaseDetails.length; i++) {
  //               for (var j = 0; j < this.schemeArray.length; j++) {
  //                 if (this.digitalInfo.purchaseDetails[i].productId === this.schemeArray[j].itemInfo.productId) {
  //                   this.purchasedProduct.push(this.schemeArray[j].itemInfo.productName);
  //                 }
  //               }
  //             }
  //             this.formFill();
  //           }
  //           else {
  //             for (var i = 0; i < this.digitalInfo.purchaseDetails.length; i++) {
  //               for (var j = 0; j < this.schemeArray.length; j++) {
  //                 if (this.digitalInfo.purchaseDetails[i].brandId === this.schemeArray[j].itemInfo.brandId) {
  //                   this.purchasedProduct.push(this.schemeArray[j].itemInfo.brandName);
  //                 }
  //               }
  //             }
  //             this.formFill();
  //           }
  //           console.log("purchased products", this.purchasedProduct);
  //           break;
  //         case responseCodes.RESP_AUTH_FAIL:
  //           break;
  //         case responseCodes.RESP_FAIL:
  //           break;
  //         case responseCodes.RESP_ALREADY_EXIST:
  //           break;
  //       }
  //     },
  //     err => {
  //       this.showLoader = false;

  //     }
  //     );

  // }
}
