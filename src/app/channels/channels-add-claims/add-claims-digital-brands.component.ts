/*
	Author			:	Deepak Terse
	Description		: 	Component for Add Digital Brand Claim page
	Date Created	: 	24 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { ConfigService } from "../../shared-services/config.service";
import { ClaimsService } from "../../claims/claims.service";
import { StringService } from "../../shared-services/strings.service";
import { S3UploadService } from "../../shared-services/s3-upload.service";
import { ChannelsService } from "../../channels/channels.service";


import { FeedbackMessageComponent } from '../../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../../footer/app-footer.component';
import { ClientFooterComponent } from '../../footer/client-footer.component';


@Component({
	templateUrl: './add-claims-digital-brands.component.html',
	styleUrls: ['channels-claims.component.css'],
	providers: [ClaimsService]

})
export class AddClaimsDigitalBrandsComponent implements OnInit {
	formTransactionDate: Date;
	transactionPeriodEndDate: any;
	transactionPeriodStartDate: any;
	programInfo: any;
	toSetParentChannelCreditPartyId: any;

	private claimsForm: FormGroup;
	private brandsForm: FormGroup;

	private brandsArray: any[] = [];
	private packagingUnitsArray: any[] = [];

	private brandDetailsArray: any[] = []

	private showLoader: boolean = false;
	private showFields: boolean = true;
	private showDistributornotFoundMessage: boolean;
	private showDistributorFoundMessage: boolean;
	private disableSearchDistributer: boolean;
	private validateSelectDistributor: boolean;
	private distributors: any[];
	private channelBackRequestObject: any;
	public clientColor: any;
	public claimPeriodEndDate: Date;
	public claimPeriodStartDate: Date;
	public resetFlag: boolean = false;
	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private claimsService: ClaimsService,
		private channelService: ChannelsService,
		private stringService: StringService,
		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService) {
		this.showDistributornotFoundMessage = false;
		this.showDistributorFoundMessage = false;
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Add Claims");
				ga('send', 'pageview');
			}
		});
		this.channelBackRequestObject = this.channelService.getMyChannelsClaimsBackRequestObject();
		this.clientColor = this.configService.getThemeColor();
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.transactionPeriodStartDate = new Date(this.programInfo.transactionPeriodStartDate);
		this.transactionPeriodEndDate = new Date(this.programInfo.transactionPeriodEndDate);
	}

	ngOnInit() {
		this.initClaimsForm();
		this.initBrandsForm();
		this.valueChange()
		let programInfo: any = this.configService.getloggedInProgramInfo();
		this.claimPeriodStartDate = new Date(programInfo.claimPeriodStartDate);
		this.claimPeriodEndDate = new Date(programInfo.claimPeriodEndDate);

		if (programInfo.pointCalculationType === "quantity") {

			//  this.removeFields();
		}

		let reqObj: any = {};

		// this.getAllPackagingUnits(reqObj);
		this.getAllBrands(reqObj);
		// this.getProgramScheme(reqObj)

		// this.distributors = this.channelService.getDistributor();
		this.distributors = this.channelService.getDistributor();
		var distName = this.distributors;
		if (this.distributors != null) {
			this.claimsForm.patchValue({
				"debitPartyId": this.distributors[0].userDetails.userId
			});
		}


	}

	valueChange() {

		this.formTransactionDate = this.claimsForm.value.transactionDate;
		this.formTransactionDate = new Date(this.formTransactionDate);
		if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
			var message = {
				str: "Note: You are making a claim outside the transaction period of the enrolled program"
			}
			if (this.resetFlag === false) {
				this.feedbackMessageComponent.updateMessage(true, message.str, "alert-danger")
			}
		}
	}
	backToSalesFlow() {
		this.toSetParentChannelCreditPartyId = this.channelService.getMyChannelProfileForComingBack();
		this.channelService.setCreditPartyId(this.toSetParentChannelCreditPartyId.userDetails.userId);
		this.channelService.setDistributor(this.toSetParentChannelCreditPartyId.parentUsersInfo);
		this.router.navigate(['./home/channels/profile-details']);
	}

	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initClaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates claims form group and assign initial values and validations to its controls
    */
	initClaimsForm() {
		this.claimsForm = this.formBuilder.group({
			transactionDate: ["", [Validators.required]],
			vatPercentage: ["0", [Validators.required]],

			debitPartyId: ["", [Validators.required]],
			//OR
			distributorName: ["", [Validators.required]],
			distributorMobileNo: ["", [Validators.required, Validators.pattern('[0-9]{10}')]]
		});
		this.claimsForm.patchValue({
			transactionDate: new Date().toISOString().split('T')[0]
		});

	}

	/*
        METHOD         : initClaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates brands form group and assign initial values and validations to its controls
    */
	initBrandsForm() {
		this.brandsForm = this.formBuilder.group({
			brandName: ["", [Validators.required]],
			// sku: ["", [Validators.required]],
			packagingUnit: ["", [Validators.required]],
			quantity: ["", [Validators.required]],
			discount: ["0", [Validators.required]],
			MRP: ["1", [Validators.required]],
			totalPrice: ["", [Validators.required]]
		});
	}

	removeFields() {
		this.brandsForm.removeControl("discount");
		this.brandsForm.removeControl("MRP");
		this.showFields = false;
	}

	/*
        METHOD         : clearClaimsForm()
        DESCRIPTION    : Called from onResetFormClick() and after form is submitted
						 Resets the form to null values
    */
	clearClaimsForm() {
		this.resetFlag = true;
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
		this.claimsForm.reset({
		});
		this.resetFlag = false;
	}

	/*
        METHOD         : clearBrandsForm()
        DESCRIPTION    : Called from onResetFormClick(), when brand is added to the table and after form is submitted
						 Resets the form to null values
    */
	clearBrandsForm() {
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetBrand");
		this.brandsForm.reset();
	}
	/***************************** 		END OF FORM METHODS 	********************************/





	/***************************** 		ONCLICK METHODS 	********************************/

	onDistributorSelected(selectedDistributor) {
		if (selectedDistributor != "") {
			this.claimsForm.controls['distributorMobileNo'].disable();

			this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
			this.claimsForm.controls['distributorName'].setValidators(null);
			this.claimsForm.controls['distributorMobileNo'].setValidators(null);
			this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

			this.showDistributorFoundMessage = false;
			this.claimsForm.patchValue({
				"distributorMobileNo": "",
				"distributorName": ""
			});
			this.showDistributornotFoundMessage = false;


		} else {
			this.claimsForm.controls['distributorMobileNo'].enable();
		}
	}


	onBrandSelected(selectedBrand) {
		this.brandsForm.patchValue({
			"packagingUnit": ""
		});
		for (let i = 0; i < this.brandsArray.length; i++) {
			if (this.brandsArray[i].brandName === selectedBrand) {

				this.packagingUnitsArray = this.brandsArray[i].packagingUnitArray;
			}
			if (this.brandsArray[i].packagingUnitArray.length == 1) {
				this.brandsForm.patchValue({
					"packagingUnit": this.brandsArray[i].packagingUnitArray[0].packagingName
				});
			}
		}
	}

	calculateTotal() {
		let quantity = this.brandsForm.value.quantity;
		let discount = this.brandsForm.value.discount;
		let MRP = this.brandsForm.value.MRP;
		if ((quantity != "") && (discount != "") && (MRP != "")) {
			this.brandsForm.patchValue({ "totalPrice": MRP * quantity - ((discount / 100) * MRP * quantity) });
		}
		if ((quantity !== "") && (discount === 0) && (MRP !== "")) {
			this.brandsForm.patchValue({ "totalPrice": MRP * quantity - ((discount / 100) * MRP * quantity) });
		}
	}

	onUserFound(userDetails) {
		this.showDistributorFoundMessage = true;
		this.claimsForm.patchValue({
			"distributorName": userDetails.userName,
			"debitPartyId": userDetails.userId

		});
	}

	onUserNotFound() {
		this.showDistributornotFoundMessage = true;

	}

	onSearchDistributorClick() {

		this.claimsForm.controls['distributorName'].setValidators([Validators.required]);;
		this.claimsForm.controls['distributorMobileNo'].setValidators([Validators.required, Validators.pattern('[0-9]{10}')]);
		this.claimsForm.controls['debitPartyId'].setValidators(null);
		this.claimsForm.patchValue({
			"debitPartyId": ""
		});


		let mobno: string = this.claimsForm.value.distributorMobileNo as string
		let reqObj = {
			"searchByMobile": mobno,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"clientId": this.configService.getloggedInProgramInfo().clientId
		};
		this.getProgramUser(reqObj);
	}

	/*
        METHOD         : onAddClaimsFormClick()
        DESCRIPTION    : Called when user clicks on "ADD CLAIM" button
				      	 Forms request object and calls addSecondarySale to add new claim
    */
	onAddClaimsFormClick() {
		if (new Date() >= this.claimPeriodStartDate && new Date() <= this.claimPeriodEndDate) {

			if (this.showLoader == false) {
				this.showLoader = true;
				let recordInfo: any = {};

				// recordInfo = this.claimsForm.value;

				if (this.claimsForm.value.debitPartyId != "") {
					//if user is either selected or searched		
					recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyId, "addClaim");
				}
				else {
					//if user is not found in search
					recordInfo.distributorName = this.claimsForm.value.distributorName;
					recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.distributorName, "addClaim");
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.distributorName, "searchDistributor");
				}

				//mandatory fields
				recordInfo.transactionDate = this.claimsForm.value.transactionDate;
				recordInfo.vatPercentage = this.claimsForm.value.vatPercentage;
				recordInfo.creditPartyId = this.channelService.creditPartyId;
				recordInfo.purchaseDetails = this.brandDetailsArray;

				this.addSecondarySale(recordInfo);
			}
		} else {
			this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't add claim outside of claim period", "alert-danger")
		}
	}

	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls clearClaimsForm() and clearBrandsForm() to reset claims form
				      	 and products form to null values
    */
	onResetClaimsFormClick() {
		this.clearClaimsForm();
		this.clearBrandsForm();
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetBrand");
	}

	/*
        METHOD         : onAddProductClick()
        DESCRIPTION    : Called when user clicks on "ADD PRODUCT" button
				      	 Add brand form values to brand table.
    */
	onAddBrandClick() {
		document.getElementById('brandName').focus();
		let brandObject: any = this.brandsForm.value;
		this.googleAnalyticsEventsService.emitEvent("Add Claims", brandObject.brandName, "addBrand");
		for (let i = 0; i < this.brandsArray.length; i++) {
			if (this.brandsArray[i].brandName === brandObject.brandName)
				brandObject.brandId = this.brandsArray[i].brandId;
		}

		for (let i = 0; i < this.packagingUnitsArray.length; i++) {
			if (this.packagingUnitsArray[i].packagingName === brandObject.packagingUnit)
				brandObject.packagingUnitId = this.packagingUnitsArray[i].packagingId;
		}

		//  // console.log("brandObject", brandObject);
		this.brandDetailsArray.push(brandObject);
		this.clearBrandsForm();
	}

	/*
        METHOD         : onRemoveBrandClick()
        DESCRIPTION    : Called when user clicks on "REMOVE" button
						 Removes selected brand form values from the table
    */
	onRemoveBrandClick(index) {
		this.googleAnalyticsEventsService.emitEvent("Add Claims", this.brandDetailsArray[index].brandName, "removeBrand");
		this.brandDetailsArray.splice(index, 1);
	}
	/***************************** 		END OF ONCLICK METHODS 	********************************/





	/***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : updateClientUser()
        DESCRIPTION    : To update client user info
    */
	getAllPackagingUnits(requestObject) {
		this.showLoader = true;
		this.packagingUnitsArray = [];

		this.claimsService.getAllPackagingUnits(requestObject)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.packagingUnitsArray = responseObject.result;
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
        METHOD         : updateClientUser()
        DESCRIPTION    : To update client user info
    */
	getAllBrands(requestObject) {
		this.showLoader = true;
		this.brandsArray = [];

		this.claimsService.getAllBrands(requestObject)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.brandsArray = responseObject.result;
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyName, "addClaimFailed");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
        METHOD         : addSecondarySale()
        DESCRIPTION    : To add a claim 
    */
	addSecondarySale(recordInfo) {
		this.showLoader = true;

		this.claimsService.addSecondarySale(recordInfo)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						this.brandDetailsArray = [];
						this.brandsForm.reset();
						this.onResetClaimsFormClick();
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
	METHOD         : getProgramUser()
	DESCRIPTION    : To add a claim 
	*/
	getProgramUser(reqObj) {
		this.showLoader = true;

		this.claimsService.getProgramUser(reqObj)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				this.showDistributornotFoundMessage = false;
				this.showDistributorFoundMessage = false;

				this.claimsForm.patchValue({
					"distributorName": ""
				});
				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						if (this.configService.getLoggedInUserInfo().mobileNumber !== responseObject.result[0].userDetails.mobileNumber) {

							this.onUserFound(responseObject.result[0].userDetails);
						} else {
							this.feedbackMessageComponent.updateMessage(true, "You cannot be your own distributor", "alert-danger");
						}

						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.showDistributornotFoundMessage = true;
						this.onUserNotFound();
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
METHOD         : getProgramScheme()
DESCRIPTION    : get All scheme setup of the program
*/
	// getProgramScheme(reqObj) {
	// 	this.showLoader = true;

	// 	this.channelService.getProgramScheme(reqObj)
	// 		.subscribe(
	// 		(responseObject) => {
	// 			this.showLoader = false;

	// 			let responseCodes = this.configService.getStatusTokens();
	// 			switch (responseObject.statusCode) {
	// 				case responseCodes.RESP_ROLLBACK_ERROR:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_SERVER_ERROR:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_SUCCESS:

	// 					this.brandsArray = responseObject.result;
	// 					break;
	// 				case responseCodes.RESP_AUTH_FAIL:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_FAIL:
	// 					this.onUserNotFound();

	// 					// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_ALREADY_EXIST:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 			}
	// 		},
	// 		err => {
	// 			this.showLoader = false;
	// 			this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
	// 		}
	// 		);
	// }
	/************************************ 	END OF API CALLS 	************************************************/
}
