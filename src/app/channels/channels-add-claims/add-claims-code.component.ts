import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { ConfigService } from "../../shared-services/config.service";
import { ClaimsService } from "../../claims/claims.service";
import { ChannelsService } from "../../channels/channels.service";
import { StringService } from "../../shared-services/strings.service";
import { S3UploadService } from "../../shared-services/s3-upload.service";


import { FeedbackMessageComponent } from '../../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../../footer/app-footer.component';
import { ClientFooterComponent } from '../../footer/client-footer.component';
import { ISubscription } from "rxjs/Subscription";
import { Device } from "ng2-device-detector/dist/services/ng2-device.service";

@Component({
    templateUrl: './add-claims-code.component.html',
    styleUrls: ['./channels-claims.component.css'],
    providers: [ClaimsService, Device],
})

export class AddClaimsCodeComponent implements OnInit, OnDestroy {
    formTransactionDate: Date;
    transactionPeriodEndDate: any;
    transactionPeriodStartDate: any;
    toSetParentChannelCreditPartyId: any;
    private claimsForm: FormGroup;
    private showLoader: boolean;
    public isValidForm: boolean;
    private distributors: any[];
    private distributorsInfo: any;
    private finalUniqueCodeArr: any = [];
    private uniqueCodeArr: any = {};
    public apiCounter: number;
    public clientColor: any;
    private showDistributornotFoundMessage: boolean;
    private showDistributorFoundMessage: boolean;
    private disableSearchDistributer: boolean;
    private disableBtn: boolean;
    private invalidCode: boolean;
    private subscription: ISubscription;
    private channelBackRequestObject: any;
    public claimPeriodEndDate: Date;
    public claimPeriodStartDate: Date;
    public programInfo: any;
    public resetFlag: boolean = false;
    //Reusable component for showing response messages of api calls
    @ViewChild(FeedbackMessageComponent)
    private feedbackMessageComponent: FeedbackMessageComponent;

    private isFileSelected: boolean;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private claimsService: ClaimsService,
        private channelService: ChannelsService,
        private stringService: StringService,
        private s3UploadService: S3UploadService,
        private formBuilder: FormBuilder,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private device: Device,) {
       
        this.channelBackRequestObject = this.channelService.getMyChannelsClaimsBackRequestObject();
        console.log('Back object', this.channelBackRequestObject)
        this.clientColor = this.configService.getThemeColor();
        this.showLoader = false;
        this.disableBtn = true;
        this.apiCounter = 0;
        this.showDistributornotFoundMessage = false;
        this.showDistributorFoundMessage = false;
        this.isFileSelected = false;
        this.disableSearchDistributer = false;
        this.distributorsInfo = this.configService.getloggedInProgramUser();

        // console.log("THIS IS DIST->", this.distributorsInfo);
        var distName = this.distributorsInfo;
        this.programInfo = this.configService.getloggedInProgramInfo();
        this.transactionPeriodStartDate = new Date(this.programInfo.transactionPeriodStartDate);
        this.transactionPeriodEndDate = new Date(this.programInfo.transactionPeriodEndDate);
        // console.log("DIST NAME",distName);

        if (this.distributors != null) {
            this.claimsForm.patchValue({
                "debitPartyId": this.distributors[0].userDetails.userId
            });
        }

    }

    ngOnInit() {
        if(this.device.device === "android" || this.device.device === "ios"){
            console.log("inside android and ios");
            (<any>window).ga.trackView("Add Claims")
        } else {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    ga('set', 'page', "Add Claims");
                    ga('send', 'pageview');
                }
            });
        }
        this.initclaimsForm();
        this.valueChange();
        this.claimPeriodStartDate = new Date(this.programInfo.claimPeriodStartDate);
        this.claimPeriodEndDate = new Date(this.programInfo.claimPeriodEndDate);
        this.distributors = this.channelService.getDistributor();

    }
    valueChange() {

        this.formTransactionDate = this.claimsForm.value.transactionDate;
        this.formTransactionDate = new Date(this.formTransactionDate);
        if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
            var message = {
                str: "Note: You are making a claim outside the transaction period of the enrolled program"
            }
            if (this.resetFlag === false) {
                this.feedbackMessageComponent.updateMessage(true, message.str, "alert-danger")
            }
        }
    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngAfterContentChecked() {
        this.setValidators();
    }


    submitInvoice() {
        this.subscription = this.s3UploadService.fileUploaded.subscribe((value) => {
            if (this.claimsForm.valid) {

                this.claimsForm.patchValue(value);
                if (value.length > 0) {
                    this.isFileSelected = false;
                    let requestObj: any = {};
                    requestObj.recordInfo = {};
                    // this.claimsForm.controls['invoiceDocument'].setValue(value);
                    if (this.claimsForm.value.debitPartyId != "") {
                        //if user is either selected or searched		
                        requestObj.recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
                    }
                    else {
                        //if user is not found in search
                        requestObj.recordInfo.distributorName = this.claimsForm.value.distributorName;
                        requestObj.recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
                    }
                    //mandatory fields
                    requestObj.recordInfo.transactionDate = this.claimsForm.value.transactionDate;
                    requestObj.recordInfo.creditPartyId = this.channelService.getCreditPartyId();
                    if (this.finalUniqueCodeArr.length > 0) {
                        requestObj.uniqueCodes = this.finalUniqueCodeArr;
                        requestObj.status = "Used";
                    }
                    if (requestObj.recordInfo !== undefined && this.apiCounter === 0) {
                        console.log("in If")
                        requestObj.recordInfo.invoiceDocument = value[0].Location;
                        this.addSecondarySale(requestObj);
                        this.apiCounter = 1;
                    }
                }
            }
        });
    }

    /********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initclaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates invoice form group and assign initial values and validations to its controls
    */
    initclaimsForm() {
        this.claimsForm = this.formBuilder.group({
            transactionDate: ["", [Validators.required]],
            invoiceDocument: [],
            uniqueCode: "",
            debitPartyId: ["", []],
            //OR
            distributorName: ["", []],
            distributorMobileNo: ["", [Validators.required, Validators.pattern('[0-9]{10}')]],

        });
    }

	/*
        METHOD         : clearclaimsForm()
        DESCRIPTION    : Called from onResetFormClick() and after form is submitted
						 Resets the form to null values
    */
    clearclaimsForm() {
        this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
        this.claimsForm.controls['distributorMobileNo'].enable();
        this.resetFlag = true;
        this.claimsForm.reset({
        });
        this.resetFlag = false;
        this.finalUniqueCodeArr = [];
    }
    /***************************** 		END OF FORM METHODS 	********************************/
    onDistributorSelected(selectedDistributor) {

        if (selectedDistributor != "") {
            this.claimsForm.controls['distributorMobileNo'].disable();

            console.log("selectedDistributor", selectedDistributor);


            this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
            this.claimsForm.controls['distributorName'].setValidators(null);
            this.claimsForm.controls['distributorMobileNo'].setValidators(null);
            this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

            this.showDistributorFoundMessage = false;
            this.claimsForm.patchValue({
                "distributorMobileNo": "",
                "distributorName": ""
            });
            this.showDistributornotFoundMessage = false;

            console.log(this.claimsForm.value);

        } else {
            this.claimsForm.controls['distributorMobileNo'].enable();

        }

    }



    onUserFound(userDetails) {
        this.showDistributorFoundMessage = true;

        this.claimsForm.patchValue({
            "distributorName": userDetails.userName,
            "debitPartyId": userDetails.userId
        });
        this.claimsForm.controls["debitPartyId"].setValidators([]);
    }

    onUserNotFound() {
        this.showDistributornotFoundMessage = true;
        this.claimsForm.patchValue({
            "distributorName": "",
            "debitPartyId": ""
        });
    }

    requestObjectFill() {
        let requestObj: any = {};
        requestObj.recordInfo = {};
        // this.claimsForm.controls['invoiceDocument'].setValue(value);
        if (this.claimsForm.value.debitPartyId != "") {
            //if user is either selected or searched		
            requestObj.recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
        }
        else {
            //if user is not found in search
            requestObj.recordInfo.distributorName = this.claimsForm.value.distributorName;
            requestObj.recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
        }
        //mandatory fields
        requestObj.recordInfo.transactionDate = this.claimsForm.value.transactionDate;
        requestObj.recordInfo.creditPartyId = this.channelService.getCreditPartyId();
        requestObj.uniqueCodes = this.finalUniqueCodeArr;
        requestObj.status = "Used";
        this.addSecondarySale(requestObj);
    }

    removeSingleCharacter() {
        console.log("inside removeSingleCharacter");
        console.log(this.claimsForm.value.uniqueCode.substr(0, this.claimsForm.value.uniqueCode.length - 1));
        this.claimsForm.patchValue({
            "uniqueCode": ""
        })

    }

	/*
        METHOD         : onAddFormClick()
        DESCRIPTION    : Called when user clicks on "ADD CLAIM" button
				      	 Forms request object and calls addSecondarySale to add new claim
    */
    onAddFormClick() {
        if (new Date() >= this.claimPeriodStartDate && new Date() <= this.claimPeriodEndDate) {
            if (this.isFileSelected === true) {
                this.apiCounter = 0;
                this.submitInvoice();
                this.s3UploadService.uploadFiles();
            }
            else {
                this.requestObjectFill();
            }
        } else {
            this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't add claim outside of claim period", "alert-danger")
        }

    }

	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls clearclaimsForm() to reset invoice form to null values
    */
    onResetFormClick() {
        this.clearclaimsForm();
        this.setValidators();
        this.onUserNotFound();
        this.invalidCode = true;
        this.finalUniqueCodeArr = []; //Empty the list of table array
        this.isFileSelected = false;
    }


    onSearchDistributorClick() {

        this.claimsForm.controls['distributorName'].setValidators([Validators.required]);;
        this.claimsForm.controls['distributorMobileNo'].setValidators([Validators.required, , Validators.pattern('[0-9]{10}')]);;
        this.claimsForm.controls['debitPartyId'].setValidators(null);
        this.claimsForm.patchValue({
            "debitPartyId": ""
        });

        let mobno: string = this.claimsForm.value.distributorMobileNo as string
        this.googleAnalyticsEventsService.emitEvent("Add Claims", mobno, "searchDistributor");

        console.log("mobile no", mobno.length);
        let reqObj = {
            "searchByMobile": mobno,
            "programId": this.configService.getloggedInProgramInfo().programId,
            "clientId": this.configService.getloggedInProgramInfo().clientId
        };


        this.getProgramUser(reqObj);
    }


    setValidators() {
        console.log("****called****");
        if (this.claimsForm.valid === true && (this.isFileSelected === true || this.finalUniqueCodeArr.length > 0)) {
            console.log("****called in****");
            this.isValidForm = true;
        }
        else {
            this.isValidForm = false;
        }
    }



    onFileChange(fileInput: any) {

        console.log("File", fileInput);
        this.isFileSelected = true;
        let filePath = "Programs-Data" + "/" + "Invoices" + "/" + "invoice";
        this.s3UploadService.formParams(fileInput, filePath);
        this.setValidators();
    }


    /**
* METHOD   : addCode
* DESC     : ADDS THE PARENT IN THE TABLE SOURCE ARRAY .
*
*/
    addCode() {
        let foundFlag: boolean = false;
        let requestObj: any = {};
        requestObj = {};
        requestObj.uniqueCodes = [];
        requestObj.uniqueCodes.push(this.claimsForm.value.uniqueCode);
        requestObj.state = this.channelService.getProfileForCodeValidation().userDetails.state;
        requestObj.city = this.channelService.getProfileForCodeValidation().userDetails.city;
        requestObj.status = "Active";
        if (this.finalUniqueCodeArr.length > 0) {
            for (let i = 0; i < this.finalUniqueCodeArr.length; i++) {
                if (this.finalUniqueCodeArr[i] == this.claimsForm.value.uniqueCode) {
                    console.log("this.finalUniqueCodeArr[i]:", this.finalUniqueCodeArr[i])
                    this.feedbackMessageComponent.updateMessage(true, "This code has already been used or is an invalid code. Please enter a valid code !", "alert-danger");
                    foundFlag = true;
                }
            }
            if (foundFlag === false) {
                console.log("in else")
                this.getAllUniqueCodes(requestObj);
            }
        }
        else {
            console.log("in main else")

            this.getAllUniqueCodes(requestObj);
        }
        this.setValidators();
    }


    /**
     * METHOD   : deleteCode
     * DESC     : DELETE A CODE FROM THE CODE TABLE.
     *
     */
    deleteCode(index) {
        this.finalUniqueCodeArr.splice(index, 1);
        this.setValidators();
    }


    /********************************* 	END OF ONCLICK METHODS 	*********************************/






    /***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : addSecondarySale()
        DESCRIPTION    : To add a claim 
    */
    addSecondarySale(requestObj) {
        if (requestObj.recordInfo.distributorName) {

            this.googleAnalyticsEventsService.emitEvent("Add Claims", requestObj.recordInfo.distributorName, "addClaim");
        }

        this.showLoader = true;
        console.log(requestObj);
        this.claimsService.addSecondarySaleCode(requestObj)
            .subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();

                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
                        this.onResetFormClick();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_FAIL:
                        // this.googleAnalyticsEventsService.emitEvent("Add Claims", requestObj.programId, "addClaimFailed");
                        this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyName, "addClaimFailed");
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                }
            },
            err => {
                this.showLoader = false;
                this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
            }
            );
    }

	/*
METHOD         : getProgramUser()
DESCRIPTION    : To add a claim 
*/
    getProgramUser(reqObj) {
        this.showLoader = true;

        this.claimsService.getProgramUser(reqObj)
            .subscribe(
            (responseObject) => {
                this.showLoader = false;

                let responseCodes = this.configService.getStatusTokens();
                this.showDistributornotFoundMessage = false;
                this.showDistributorFoundMessage = false;

                this.claimsForm.patchValue({
                    "distributorName": ""
                });
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        console.log(responseObject.result[0].userDetails.userId);
                        console.log(responseObject.result[0].userDetails.userName);

                        if (this.configService.getLoggedInUserInfo().mobileNumber !== responseObject.result[0].userDetails.mobileNumber) {

                            this.onUserFound(responseObject.result[0].userDetails);
                        } else {
                            this.feedbackMessageComponent.updateMessage(true, "You cannot be your own distributor", "alert-danger");
                        }
                        this.setValidators();
                        // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_FAIL:
                        this.onUserNotFound();
                        this.setValidators();
                        // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                }
            },
            err => {
                this.showLoader = false;
                this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
            }
            );
    }

    /**
    * METHOD   : getAllUniqueCodes
    * DESC     : calls 'getAllUniqueCodesAPI' from programOperationsService, and handles the response
    *
    */
    getAllUniqueCodes(requestObj) {
        this.showLoader = true;
        this.claimsService.getAllUniqueCodes(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.uniqueCodeArr = [];
                        this.uniqueCodeArr = responseObject.result;

                        // this.codeTableObj = {
                        //     "uniqueCode": this.uniqueCodeArr[0].uniqueCodeArray[0].uniqueCode,
                        // };
                        //this.codeArr.push(this.codeTableObj);

                        this.finalUniqueCodeArr.push(this.uniqueCodeArr[0].uniqueCodeArray[0].uniqueCode);
                        this.claimsForm.patchValue({
                            "uniqueCode": "",
                        });
                        if (this.finalUniqueCodeArr.length > 1) {
                            if (this.finalUniqueCodeArr[this.finalUniqueCodeArr.length - 1] === this.finalUniqueCodeArr[this.finalUniqueCodeArr.length - 2]) {
                                this.finalUniqueCodeArr.splice(this.finalUniqueCodeArr.length - 1, 1)
                            }
                        }
                        this.setValidators();
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_FAIL:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                    case responseCodes.RESP_BLOCKED:
                        this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
                        break;
                }
            },
            err => {
                this.showLoader = false;
                this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
            }
            );
    }

    /************************************ 	END OF API CALLS 	************************************************/

    backToSalesFlow() {
        this.toSetParentChannelCreditPartyId = this.channelService.getMyChannelProfileForComingBack();
        this.channelService.setCreditPartyId(this.toSetParentChannelCreditPartyId.userDetails.userId);
        this.channelService.setDistributor(this.toSetParentChannelCreditPartyId.parentUsersInfo);
        console.log("object GOT BACKKK", this.toSetParentChannelCreditPartyId);
        this.router.navigate(['./home/channels/profile-details']);
    }
}