/*
	Author			:	Pratik Gawand
	Description		: 	Component for Invoice  Popup 
	Date Created	: 	27 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, Input } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { StringService } from "../../shared-services/strings.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ConfigService } from "../../shared-services/config.service";
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { ChannelsService } from '../channels.service';
import { Observable, Subject, ReplaySubject } from "rxjs";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";

@Component({

    templateUrl: './code-claims-operations.component.html',
    styleUrls: ['./claims-details-operations.component.css'],
    providers: [],


})
export class CodeClaimsOperationsComponent implements OnInit {
    public invoiceForm: FormGroup;
    private addItems: boolean = false;
    private showLoader: boolean;
    private programInfo: any;
    private invoiceInfo: any;
    public deletedCodeArr: any = [];


    // Admin
    public productArr: any = [];
    public brandArr: any = [];

    public distributorArr: any = [];
    public requestObj: any = {};
    public programUser: any = [];
    public secondarySaleUserBeingProcessed: any = {};
    public isProduct: boolean;
    public isValue: boolean;
    public productSearching: boolean;
    public searchProductFailed: boolean;
    public brandSearching: boolean;
    public searchBrandFailed: boolean;
    public showSearchField: boolean[] = [];
    public isReject: boolean;
    public isApprove: boolean;
    public isRejected: boolean;
    public isApproved: boolean;
    public isEditPrefilledProduct: boolean;
    public isDebitPartyMapped: boolean;
    public isCreditPartyMapped: boolean;

    public userMobileSearching: boolean;
    public userMobileSearchFailed: boolean;
    public searchSuccess: boolean;
    public mobileNoErr: boolean;
    public isSearch: boolean;
    public parentObj: any = {};
    public packagingUnitArr: any[][] = [];
    public index: number;
    private isBrochureError: boolean;
    private invoiceSelectCount: number;
    public apiCounter: number;
    private clientId = this.configService.getloggedInProgramInfo().clientId;
    private programId = this.configService.getloggedInProgramInfo().programId;
    private roleInfo = this.configService.getloggedInProgramUser();
    private canApprove: boolean;
    private finalUniqueCodeArr: any = [];
    private uniqueCodeArr: any = {};
    public clientColor: any;
    //Admin




    constructor(public dialogRef: MdDialogRef<CodeClaimsOperationsComponent>,
        private configService: ConfigService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private stringService: StringService,
        public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        private channelsService: ChannelsService,

    ) {
      

        this.clientColor = this.configService.getThemeColor();




        //   this.invoiceForm = this.formBuilder.group({
        //       "productId": "",
        //       "productName": ["", [Validators.required]],
        //       "packagingUnitId": ["", [Validators.required]],
        //       "packagingUnit": "",
        //       "quantity": ["", [Validators.required]],
        //       "MRP": ["", [Validators.required]],
        //       "discount": ["", [Validators.required]],
        //       "totalPrice": ["", [Validators.required]],
        //   });

        console.log('this.roleInfo.programRoleInfo.canApproveClaims', this.roleInfo.programRoleInfo.canApproveClaims);
        if (this.roleInfo.programRoleInfo.canApproveClaims) {
            console.log('in true');
            this.canApprove = false;

        } else {
            this.canApprove = true;
            console.log('in false');
        }

        this.invoiceInfo = this.channelsService.getmyClaims();
        if (this.invoiceInfo.approvalStatus === "approved") {
            this.isApproved = true;
        }

        if (this.invoiceInfo.approvalStatus === "rejected") {
            this.isRejected = true;
        }





        console.log("invoice in fo", this.invoiceInfo);
        this.programInfo = this.configService.getloggedInProgramInfo();
        if (this.programInfo.basedOn === "product") {
            this.isProduct = true;
        }
        else {
            this.isProduct = false;
        }
        if (this.programInfo.pointCalculationType === "value" || this.programInfo.pointCalculationType === "both" || this.programInfo.pointCalculationType === "quantity") {
            this.isValue = true;
        }
        else {
            this.isValue = false;
        }

    }
    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                ga('set', 'page', "Claims Details");
                ga('send', 'pageview');
            }
        });
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.invoiceForm = this.formBuilder.group({
            "creditPartyId": this.invoiceInfo.creditPartyId,
            "creditPartyName": "",
            "isCreditPartyMapped": this.invoiceInfo.isCreditPartyMapped,
            "debitPartyId": this.invoiceInfo.debitPartyId,
            "debitPartyName": "",
            "isDebitPartyMapped": this.invoiceInfo.isDebitPartyMapped,
            "invoiceDate": [this.invoiceInfo.transactionDate, [Validators.required]],
            "approvalStatus": this.invoiceInfo.approvalStatus,
            "reasonForRejection": this.invoiceInfo.comment,
            "uniqueCode": ""
        });

        this.finalUniqueCodeArr = this.invoiceInfo.uniqueCodes;



    }


    addItemsBox() {
        this.addItems = true;
    }


    closeInvoice() {
        this.dialogRef.close(CodeClaimsOperationsComponent);
    }



    /**
* METHOD   : approveClick
* DESC     : APPROVE THE CLAIM
*/
    approveClick() {
        this.requestObjectFill();
        this.updateSecondarySale();
    }


    /**
   * METHOD   : rejectClick
    * DESC     : REJECT THE CLAIM
   */
    rejectClick() {
        this.requestObj = {};
        this.requestObj.updateInfo = {};
        this.requestObj.updateInfo.approvalStatus = "rejected";
        this.requestObj.updateInfo.comment = this.invoiceForm.value.reasonForRejection;
        this.requestObj.programId = this.programId;
        this.requestObj.clientId = this.clientId;
        this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
        this.requestObj.recordId = this.invoiceInfo.secondarySalesId;
        this.requestObj.uniqueCodes = this.finalUniqueCodeArr;
        this.requestObj.status = "Active";
        this.updateSecondarySale();
    }




    /**
    * METHOD   : requestObjectFill
    * DESC     : methods to fill the requestObj before API call.
    *
    */
    requestObjectFill() {
        this.requestObj = {}
        this.requestObj.updateInfo = {};
        this.requestObj.updateInfo.approvalStatus = "approved";
        this.requestObj.updateInfo.comment = this.invoiceForm.value.reasonForRejection;
        this.requestObj.programId = this.programId;
        this.requestObj.clientId = this.clientId;
        this.requestObj.frontendUserInfo = this.configService.getFrontEndUserInfo();
        this.requestObj.recordId = this.invoiceInfo.secondarySalesId;


        if (this.invoiceForm.value.creditPartyId !== this.invoiceInfo.creditPartyId) {
            this.requestObj.updateInfo.creditPartyId = this.invoiceForm.value.creditPartyId;
        }
        if (this.invoiceForm.value.debitPartyId !== this.invoiceInfo.debitPartyId) {
            this.requestObj.updateInfo.debitPartyId = this.invoiceForm.value.debitPartyId;
        }
        if (this.invoiceForm.value.invoiceDate !== this.invoiceInfo.transactionDate) {
            this.requestObj.updateInfo.transactionDate = this.invoiceForm.value.invoiceDate;
        }

        this.requestObj.uniqueCodes = this.finalUniqueCodeArr;
        this.requestObj.status = "Used";
        if (this.invoiceForm.value.reasonForRejection != undefined)
            this.requestObj.updateInfo.comment = this.invoiceForm.value.reasonForRejection;
        console.log("Request Object:", this.requestObj);

    }

    formFill() {
        this.invoiceForm.patchValue({
            "creditPartyId": this.invoiceInfo.creditPartyId === "" ? "Not Mapped" : this.invoiceInfo.creditPartyId,
            "creditPartyName": "",
            "isCreditPartyMapped": this.invoiceInfo.isCreditPartyMapped ? "Yes" : "No",
            "debitPartyId": this.invoiceInfo.debitPartyId === "" ? "Not Mapped" : this.invoiceInfo.debitPartyId,
            "debitPartyName": "",
            "isDebitPartyMapped": this.invoiceInfo.isDebitPartyMapped ? "Yes" : "No",
            "invoiceDate": this.invoiceInfo.transactionDate.slice(0, 10),
            "approvalStatus": this.invoiceInfo.approvalStatus,
            "reasonForRejection": this.invoiceInfo.comment
        });


        if (this.invoiceInfo.isCreditPartyMapped) {
            this.invoiceForm.patchValue({
                "creditPartyName": this.invoiceInfo.creditPartyInfo.userDetails.userName,
            });
        }
        else {
            this.invoiceForm.patchValue({
                "creditPartyName": this.invoiceInfo.creditPartyName,
            });
        }

        if (this.invoiceInfo.isDebitPartyMapped) {
            this.invoiceForm.patchValue({
                "debitPartyName": this.invoiceInfo.debitPartyInfo.userDetails.userName,
            });
        }
        else {
            this.invoiceForm.patchValue({
                "debitPartyName": this.invoiceInfo.debitPartyName,
            });
        }
        // console.log("S.S Details::", this.invoiceInfo);
        // this.invoiceDownloadLink = this.invoiceInfo.invoiceDocument;
        if (this.invoiceInfo.purchaseDetails !== undefined) {
            if (this.invoiceInfo.purchaseDetails.length !== 0) {
                for (let index = 0; index < this.invoiceInfo.purchaseDetails.length - 1; index++) {
                    // console.log('This.');
                    // this.addFormClick();
                }
            }
        }

        if (this.isRejected) {
            this.isReject = true;
        } else if (this.isApproved) {
            this.isApprove = true;
        }
        console.log("envoice info on this tab", this.invoiceInfo);
        this.invoiceForm.patchValue({
            "reasonForRejection": this.invoiceInfo.comment,
        });

        if (this.invoiceInfo.approvalStatus === "approved" || this.invoiceInfo.approvalStatus === "rejected") {
            let updatedAt: any = new Date(this.invoiceInfo.updatedAt).toUTCString();
            updatedAt = new Date(updatedAt);
            // console.log("Formatted Date and Time", updatedAt)
            updatedAt = String(updatedAt);
            this.invoiceForm.patchValue({
                "date": updatedAt.slice(0, 16),
                "time": updatedAt.slice(16, 25),
            });
        }
    }


    /**
* METHOD   : addCode
* DESC     : ADDS THE PARENT IN THE TABLE SOURCE ARRAY .
*
*/
    addCode() {
        let foundFlag: boolean = false;
        let requestObj: any = {};
        requestObj = {};
        requestObj.uniqueCodes = [];
        requestObj.uniqueCodes.push(this.invoiceForm.value.uniqueCode);
        requestObj.state = this.channelsService.getProfileForCodeValidation().userDetails.state;
        requestObj.city = this.channelsService.getProfileForCodeValidation().userDetails.city;
        requestObj.status = "Active";
        if (this.finalUniqueCodeArr) {
            if (this.finalUniqueCodeArr.length > 0) {
                for (let i = 0; i < this.finalUniqueCodeArr.length; i++) {
                    if (this.finalUniqueCodeArr[i] == this.invoiceForm.value.uniqueCode) {
                        console.log("this.finalUniqueCodeArr[i]:", this.finalUniqueCodeArr[i])
                        // this.feedbackMessageComponent.updateMessage(true, "This code has already been used or is an invalid code. Please enter a valid code !", "alert-danger");
                        foundFlag = true;
                    }
                }
                if (foundFlag === false) {
                    console.log("in else")
                    this.getAllUniqueCodes(requestObj);
                }
            }
            else {
                console.log("in main else")

                this.getAllUniqueCodes(requestObj);
            }
        }
    }


    /**
     * METHOD   : deleteCode
     * DESC     : DELETE A CODE FROM THE CODE TABLE.
     *
     */
    deleteCode(index) {
        this.deletedCodeArr.push(this.finalUniqueCodeArr[index]);
        this.finalUniqueCodeArr.splice(index, 1);
        console.log("deleted code", this.deletedCodeArr);
    }


    updateSecondarySale() {
        // console.log("req obj::", this.requestObj);
        // console.log("calling function");
        this.showLoader = true;
        this.channelsService.updateSecondarySale(this.requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                // console.log("response ", responseObject);
                let responseCodes = this.configService.getStatusTokens();
                // console.log("responseObject.statusCode ", responseObject.statusCode);
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        if (responseObject.result.approvalStatus === "approved") {
                            this.invoiceInfo.approvalStatus = "approved";
                            // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
                            // updatedAt = new Date(updatedAt);
                            // // console.log("Formatted Date and Time", updatedAt)
                            // updatedAt = String(updatedAt);
                            // this.invoiceForm.patchValue({
                            //     "date": updatedAt.slice(0, 16),
                            //     "time": updatedAt.slice(16, 25),
                            //     "approvalStatus": responseObject.result.approvalStatus,
                            // });
                            // this.isApproved = true;
                        }
                        else if (responseObject.result.approvalStatus === "rejected") {
                            this.invoiceInfo.approvalStatus = "rejected";
                            // let updatedAt: any = new Date(responseObject.result.updatedAt).toUTCString();
                            // updatedAt = new Date(updatedAt);
                            // // console.log("Formatted Date and Time", updatedAt)
                            // updatedAt = String(updatedAt);
                            // this.invoiceForm.patchValue({
                            //     "date": updatedAt.slice(0, 16),
                            //     "time": updatedAt.slice(16, 25),
                            //     "approvalStatus": responseObject.result.approvalStatus,
                            // });
                            this.isRejected = true;
                        }
                        this.dialogRef.close(CodeClaimsOperationsComponent);
                        var reqObj = {
                            "programId": this.programId,
                            "frontendUserInfo": this.configService.getFrontEndUserInfo(),
                            "recordInfo": {
                                "uniqueCodes": this.deletedCodeArr,
                                "status": "Active"
                            }
                        }
                        console.log("calllling update code*******")
                        this.updateUniqueCodes(reqObj);
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:

                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.showLoader = false;

            }
            );
    }


    updateUniqueCodes(reqObj) {
        // console.log("req obj::", this.requestObj);
        // console.log("calling function");
        console.log("in update code*******")
        this.showLoader = true;
        this.channelsService.updateUniqueCodes(reqObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                console.log("after update code*******", responseObject)

                // console.log("response ", responseObject);
                // let responseCodes = this.configService.getStatusTokens();
                // console.log("responseObject.statusCode ", responseObject.statusCode);
                // switch (responseObject.statusCode) {
                //     case responseCodes.RESP_ROLLBACK_ERROR:
                //         break;
                //     case responseCodes.RESP_SERVER_ERROR:
                //         break;
                //     case responseCodes.RESP_SUCCESS:
                //         break;
                //     case responseCodes.RESP_AUTH_FAIL:
                //         break;
                //     case responseCodes.RESP_FAIL:
                //         break;
                //     case responseCodes.RESP_ALREADY_EXIST:
                //         break;
                //     case responseCodes.RESP_BLOCKED:
                //         break;
                // }
            },
            err => {
                this.showLoader = false;

            }
            );
    }


    /**
   * METHOD   : getAllUniqueCodes
   * DESC     : calls 'getAllUniqueCodesAPI' from programOperationsService, and handles the response
   *
   */
    getAllUniqueCodes(requestObj) {
        this.showLoader = true;
        this.channelsService.getAllUniqueCodes(requestObj)
            .subscribe((responseObject) => {
                this.showLoader = false;
                let responseCodes = this.configService.getStatusTokens();
                switch (responseObject.statusCode) {
                    case responseCodes.RESP_ROLLBACK_ERROR:
                        break;
                    case responseCodes.RESP_SERVER_ERROR:
                        break;
                    case responseCodes.RESP_SUCCESS:
                        this.uniqueCodeArr = [];
                        this.uniqueCodeArr = responseObject.result;

                        // this.codeTableObj = {
                        //     "uniqueCode": this.uniqueCodeArr[0].uniqueCodeArray[0].uniqueCode,
                        // };
                        //this.codeArr.push(this.codeTableObj);

                        this.finalUniqueCodeArr.push(this.uniqueCodeArr[0].uniqueCodeArray[0].uniqueCode);
                        this.invoiceForm.patchValue({
                            "uniqueCode": "",
                        });
                        break;
                    case responseCodes.RESP_AUTH_FAIL:
                        break;
                    case responseCodes.RESP_FAIL:
                        break;
                    case responseCodes.RESP_ALREADY_EXIST:
                        break;
                    case responseCodes.RESP_BLOCKED:
                        break;
                }
            },
            err => {
                this.showLoader = false;
            }
            );
    }
}
