/*
	Author			:	Deepak Terse
	Description		: 	Component for Add Digital Claim page
	Date Created	: 	22 March 2016
	Date Modified	: 	27 March 2016
*/

import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { ConfigService } from "../../shared-services/config.service";
import { ClaimsService } from "../../claims/claims.service";
import { StringService } from "../../shared-services/strings.service";
import { S3UploadService } from "../../shared-services/s3-upload.service";
import { ChannelsService } from "../../channels/channels.service";



import { FeedbackMessageComponent } from '../../shared-components/feedback-message.component';
import { GoogleAnalyticsEventsService } from "../../shared-services/google-analytics-events.service";
import { AppFooterComponent } from '../../footer/app-footer.component';
import { ClientFooterComponent } from '../../footer/client-footer.component';

@Component({
	templateUrl: './add-claims-digital-products.component.html',
	styleUrls: ['./channels-claims.component.css'],
	providers: [ClaimsService]
})
export class AddClaimsDigitalProductsComponent implements OnInit {
	formTransactionDate: Date;
	transactionPeriodEndDate: any;
	transactionPeriodStartDate: any;
	programInfo: any;
	toSetParentChannelCreditPartyId: any;

	public claimsForm: FormGroup;
	public productsForm: FormGroup;

	private productsArray: any[];
	private packagingUnitsArray: any[];

	private productDetailsArray: any[];
	private showLoader: boolean;
	private showFields: boolean = true;

	private distributors: any[];
	private showDistributornotFoundMessage: boolean;
	private showDistributorFoundMessage: boolean;
	private disableSearchDistributer: boolean;
	private channelBackRequestObject: any;
	public clientColor: any;
	public claimPeriodEndDate: Date;
	public claimPeriodStartDate: Date;
	public resetFlag: boolean = false;
	//Reusable component for showing response messages of api calls
	@ViewChild(FeedbackMessageComponent)
	private feedbackMessageComponent: FeedbackMessageComponent;

	constructor(private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private claimsService: ClaimsService,
		private stringService: StringService,
		private channelService: ChannelsService,

		private formBuilder: FormBuilder,
		public googleAnalyticsEventsService: GoogleAnalyticsEventsService) {

	

		this.productsArray = [];
		this.packagingUnitsArray = [];

		this.showLoader = false;
		this.clientColor = this.configService.getThemeColor();
		this.showDistributornotFoundMessage = false;
		this.showDistributorFoundMessage = false;
		this.channelBackRequestObject = this.channelService.getMyChannelsClaimsBackRequestObject();
		console.log('Back object', this.channelBackRequestObject)
		this.programInfo = this.configService.getloggedInProgramInfo();
		this.transactionPeriodStartDate = new Date(this.programInfo.transactionPeriodStartDate);
		this.transactionPeriodEndDate = new Date(this.programInfo.transactionPeriodEndDate);
	}

	ngOnInit() {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				ga('set', 'page', "Add Claims");
				ga('send', 'pageview');
			}
		});
		this.initClaimsForm();
		this.initProductsForm();
		this.valueChange();
		let programInfo: any = this.configService.getloggedInProgramInfo();
		this.claimPeriodStartDate = new Date(programInfo.claimPeriodStartDate);
		this.claimPeriodEndDate = new Date(programInfo.claimPeriodEndDate);
		console.log("this.claimPeriodStartDate", this.claimPeriodStartDate);
		console.log("this.claimPeriodEndDate", this.claimPeriodEndDate);

		if (programInfo.pointCalculationType === "quantity") {
			// this.removeFields();
		}

		let reqObj: any = {};


		// this.getProgramScheme(reqObj);
		this.getAllProducts(reqObj);
		this.productDetailsArray = [];
		// this.distributors = this.channelService.getDistributor();
		this.distributors = this.channelService.getDistributor();
		console.log("Dist info", this.distributors);
		var distName = this.distributors;

		if (this.distributors != null) {
			this.claimsForm.patchValue({
				"debitPartyId": this.distributors[0].userDetails.userId
			});
		}


	}

	valueChange() {

		this.formTransactionDate = this.claimsForm.value.transactionDate;
		this.formTransactionDate = new Date(this.formTransactionDate);
		console.log("valuechange", this.formTransactionDate);
		if (this.formTransactionDate < this.transactionPeriodStartDate || this.formTransactionDate > this.transactionPeriodEndDate) {
			var message = {
				str: "Note: You are making a claim outside the transaction period of the enrolled program"
			}
			if (this.resetFlag === false) {
				this.feedbackMessageComponent.updateMessage(true, message.str, "alert-danger")
			}
		}
	}
	backToSalesFlow() {
		this.toSetParentChannelCreditPartyId = this.channelService.getMyChannelProfileForComingBack();
		this.channelService.setCreditPartyId(this.toSetParentChannelCreditPartyId.userDetails.userId);
		this.channelService.setDistributor(this.toSetParentChannelCreditPartyId.parentUsersInfo);
		console.log("object GOT BACKKK", this.toSetParentChannelCreditPartyId);
		this.router.navigate(['./home/channels/profile-details']);
	}



	/********************************* 	FORM METHODS 	**************************************/
	/*
        METHOD         : initClaimsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates claims form group and assign initial values and validations to its controls
    */
	initClaimsForm() {
		this.claimsForm = this.formBuilder.group({
			transactionDate: ["", [Validators.required]],
			vatPercentage: ["0", [Validators.required]],

			debitPartyId: ["", [Validators.required]],
			//OR
			distributorName: ["", [Validators.required]],
			distributorMobileNo: ["", [Validators.required, Validators.pattern('[0-9]{10}')]]
		});
	}

	/*
        METHOD         : initProductsForm()
        DESCRIPTION    : Called from ngOnInit when the component is loaded
				      	 Creates products form group and assign initial values and validations to its controls
    */
	initProductsForm() {
		this.productsForm = this.formBuilder.group({
			productName: ["", [Validators.required]],
			sku: ["", [Validators.required]],
			packagingUnit: ["", [Validators.required]],
			quantity: ["", [Validators.required]],
			discount: ["0", [Validators.required]],
			MRP: ["1", [Validators.required]],
			totalPrice: ["", [Validators.required]]
		});
	}

	removeFields() {
		// this.productsForm.removeControl("discount");
		// this.productsForm.removeControl("MRP");
		// this.claimsForm.removeControl("vatPercentage");
		// this.productsForm.removeControl("vatPercentage");
		// this.showFields = false;
	}

	/*
        METHOD         : clearClaimsForm()
        DESCRIPTION    : Called from onResetFormClick() and after form is submitted
						 Resets the form to null values
    */
	clearClaimsForm() {
		this.resetFlag = true;
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
		this.claimsForm.reset({
		});
		this.resetFlag = false;
	}

	/*
        METHOD         : clearProductsForm()
        DESCRIPTION    : Called from onResetFormClick(), when product is added to the table and after form is submitted
						 Resets the form to null values
    */
	clearProductsForm() {
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetProduct");
		this.productsForm.reset();
	}
	/***************************** 		END OF FORM METHODS 	********************************/





	/***************************** 		ONCLICK METHODS 	********************************/
	// onDistributorSelected(selectedDistributor) {
	// 	if (selectedDistributor != "") {
	// 		console.log("selectedDistributor", selectedDistributor)
	// 		this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
	// 		this.claimsForm.controls['distributorName'].disable({ onlySelf: false });
	// 		this.claimsForm.controls['distributorMobileNo'].disable({ onlySelf: false });
	// 		this.claimsForm.controls['debitPartyId'].enable({ onlySelf: true });	
	// 		this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

	// 		this.showDistributorFoundMessage = false;
	// 		console.log(this.claimsForm.value);
	// 	}
	// 	else {
	// 		this.disableSearchDistributer = true;
	// 		this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
	// 		console.log("selectedDistributor", selectedDistributor)			
	// 		this.claimsForm.controls['debitPartyId'].disable({ onlySelf: false });
	// 		this.claimsForm.controls['distributorName'].enable({ onlySelf: true });
	// 		this.claimsForm.controls['distributorMobileNo'].enable({ onlySelf: true });
	// 		console.log(this.claimsForm.value);
	// 		this.claimsForm.removeControl("debitPartyId");
	// 	}
	// }

	onDistributorSelected(selectedDistributor) {
		if (selectedDistributor != "") {
			console.log("selectedDistributor", selectedDistributor);
			this.claimsForm.controls['distributorMobileNo'].disable();

			this.googleAnalyticsEventsService.emitEvent("Add Claims", selectedDistributor, "selectedDistributor");
			this.claimsForm.controls['distributorName'].setValidators(null);
			this.claimsForm.controls['distributorMobileNo'].setValidators(null);
			this.claimsForm.controls['debitPartyId'].setValidators([Validators.required]);

			this.showDistributorFoundMessage = false;
			this.claimsForm.patchValue({
				"distributorMobileNo": "",
				"distributorName": ""
			});
			this.showDistributornotFoundMessage = false;

			console.log(this.claimsForm.value);

		} else {
			this.claimsForm.controls['distributorMobileNo'].enable();
		}
	}


	onProductSelected(selectedProduct) {
		this.productsForm.patchValue({
			"packagingUnit": ""
		});
		for (let i = 0; i < this.productsArray.length; i++) {
			if (this.productsArray[i].productName === selectedProduct) {
				this.productsForm.patchValue({
					"sku": this.productsArray[i].SKU,
					"packagingUnit": this.productsArray[i].packagingUnitArray[0]
				});
				if (this.productsArray[i].packagingUnitArray.length == 1) {
					this.productsForm.patchValue({
						"packagingUnit": this.productsArray[i].packagingUnitArray[0].packagingName
					});
				}
				console.log('this.packagingUnitsArray', this.packagingUnitsArray);
				this.packagingUnitsArray = this.productsArray[i].packagingUnitArray;
			}
		}
	}

	calculateTotal() {
		let quantity = this.productsForm.value.quantity;
		let discount = this.productsForm.value.discount;
		let MRP = this.productsForm.value.MRP;
		console.log("this.productsForm.value.quantity", this.productsForm.value.quantity);
		console.log("this.productsForm.value.discount", this.productsForm.value.discount);
		console.log("this.productsForm.value.MRP", this.productsForm.value.MRP);

		if ((quantity != "") && (discount != "") && (MRP != "")) {
			this.productsForm.patchValue({ "totalPrice": MRP * quantity - ((discount / 100) * MRP * quantity) });
		}
		if ((quantity != "") && (discount == 0) && (MRP != "")) {
			this.productsForm.patchValue({ "totalPrice": MRP * quantity - ((discount / 100) * MRP * quantity) });
		}

	}

	onUserFound(userDetails) {
		this.showDistributorFoundMessage = true;
		this.claimsForm.patchValue({
			"distributorName": userDetails.userName,
			"debitPartyId": userDetails.userId
		});
	}

	onUserNotFound() {
		// this.claimsForm.controls['debitPartyId'].disable({ onlySelf: true });ss
		this.claimsForm.controls['debitPartyId'].setValidators(null);
		// this.claimsForm.removeControl("debitPartyId");

		this.showDistributornotFoundMessage = true;
		this.claimsForm.patchValue({
			"distributorName": "",
			"debitPartyId": ""
		});
	}

	onSearchDistributorClick() {

		this.claimsForm.controls['distributorName'].setValidators([Validators.required]);;
		this.claimsForm.controls['distributorMobileNo'].setValidators([Validators.required, , Validators.pattern('[0-9]{10}')]);;
		this.claimsForm.controls['debitPartyId'].setValidators(null);
		this.claimsForm.patchValue({
			"debitPartyId": ""
		});

		let reqObj = {
			"searchByMobile": this.claimsForm.value.distributorMobileNo,
			"programId": this.configService.getloggedInProgramInfo().programId,
			"clientId": this.configService.getloggedInProgramInfo().clientId
		};
		this.getProgramUser(reqObj);
	}

	/*
        METHOD         : onAddClaimsFormClick()
        DESCRIPTION    : Called when user clicks on "ADD CLAIM" button
				      	 Forms request object and calls addSecondarySale to add new claim
    */
	onAddClaimsFormClick() {
		if (new Date() >= this.claimPeriodStartDate && new Date() <= this.claimPeriodEndDate) {

			if (this.showLoader == false) {
				this.showLoader = true;

				let recordInfo: any = {};
				// recordInfo = this.claimsForm.value;
				console.log(this.claimsForm.value);
				console.log(recordInfo);
				if (this.claimsForm.value.debitPartyId != "") {
					//if user is either selected or searched		
					recordInfo.debitPartyId = this.claimsForm.value.debitPartyId;
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyId, "addClaim");
				}
				else {
					//if user is not found in search
					console.log("this.claimsForm.value.distributorName", this.claimsForm.value.distributorName);
					recordInfo.distributorName = this.claimsForm.value.distributorName;
					recordInfo.distributorMobileNo = this.claimsForm.value.distributorMobileNo;
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.distributorName, "addClaim");
					this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.distributorName, "searchDistributor");
				}


				recordInfo.transactionDate = this.claimsForm.value.transactionDate;
				recordInfo.purchaseDetails = this.productDetailsArray;
				recordInfo.creditPartyId = this.channelService.creditPartyId;
				recordInfo.vatPercentage = this.claimsForm.value.vatPercentage;
				// console.log(this.configService.getloggedInBEInfo().businessId);

				console.log("recordInfo@!@!", recordInfo);
				this.addSecondarySale(recordInfo);


			}
		} else {
			this.feedbackMessageComponent.updateMessage(true, "Sorry! You can't add claim outside of claim period", "alert-danger")
		}
	}

	/*
        METHOD         : onResetFormClick()
        DESCRIPTION    : Called when user clicks on "RESET" button
				      	 Calls clearClaimsForm() and clearProductsForm() to reset claims form
				      	 and products form to null values
    */
	onResetClaimsFormClick() {
		this.clearProductArray();
		this.clearClaimsForm();
		this.clearProductsForm();
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetClaim");
		this.googleAnalyticsEventsService.emitEvent("Add Claims", "", "resetProduct");
	}

	/*
        METHOD         : onAddProductClick()
        DESCRIPTION    : Called when user clicks on "ADD PRODUCT" button
				      	 Add product form values to product table.
    */
	onAddProductClick() {
		document.getElementById("productName").focus();
		let productObject: any = this.productsForm.value;

		for (let i = 0; i < this.productsArray.length; i++) {
			if (this.productsArray[i].productName === productObject.productName)
				productObject.productId = this.productsArray[i].productId;
		}

		console.log(this.packagingUnitsArray);
		for (let i = 0; i < this.packagingUnitsArray.length; i++) {
			if (this.packagingUnitsArray[i].packagingName === productObject.packagingUnit)
				productObject.packagingUnitId = this.packagingUnitsArray[i].packagingId;
		}

		console.log("productObject", productObject);
		this.productDetailsArray.push(productObject);

	}

	/*
        METHOD         : onRemoveProductClick()
        DESCRIPTION    : Called when user clicks on "REMOVE" button
						 Removes selected product form values from the table
    */
	onRemoveProductClick(index) {
		this.googleAnalyticsEventsService.emitEvent("Add Claims", this.productDetailsArray[index].productName, "removeProduct");
		this.productDetailsArray.splice(index, 1);
	}

	clearProductArray() {
		for (var a = 0; this.productDetailsArray.length; a++) {
			this.productDetailsArray.pop();
		}
	}
	/********************************* 	END OF ONCLICK METHODS 	*********************************/






	/***************************************	API CALLS 	****************************************************/
	/*
        METHOD         : updateClientUser()
        DESCRIPTION    : To update client user info
    */
	getAllPackagingUnits(requestObject) {
		this.showLoader = true;
		this.packagingUnitsArray = [];

		this.claimsService.getAllPackagingUnits(requestObject)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.packagingUnitsArray = responseObject.result;
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						this.googleAnalyticsEventsService.emitEvent("Add Claims", this.claimsForm.value.debitPartyName, "addClaimFailed");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}


	/*
        METHOD         : getAllProducts()
        DESCRIPTION    : To update client user info
    */
	getAllProducts(requestObject) {
		this.showLoader = true;
		this.productsArray = [];
		this.claimsService.getAllProducts(requestObject)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.productsArray = responseObject.result;
						this.productDetailsArray.splice(0, this.productDetailsArray.length);
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
        METHOD         : addSecondarySale()
        DESCRIPTION    : To add a claim 
    */
	addSecondarySale(recordInfo) {
		this.showLoader = true;

		this.claimsService.addSecondarySale(recordInfo)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						this.onResetClaimsFormClick();
						//	this.clearProductArray();
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
	METHOD         : getProgramUser()
	DESCRIPTION    : To add a claim 
*/
	getProgramUser(reqObj) {
		this.showLoader = true;

		this.claimsService.getProgramUser(reqObj)
			.subscribe(
			(responseObject) => {
				this.showLoader = false;

				let responseCodes = this.configService.getStatusTokens();
				this.showDistributornotFoundMessage = false;
				this.showDistributorFoundMessage = false;

				switch (responseObject.statusCode) {
					case responseCodes.RESP_ROLLBACK_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SERVER_ERROR:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_SUCCESS:
						console.log(responseObject.result[0].userDetails.userId);
						console.log(responseObject.result[0].userDetails.userName);
						if (this.configService.getLoggedInUserInfo().mobileNumber !== responseObject.result[0].userDetails.mobileNumber) {
							this.onUserFound(responseObject.result[0].userDetails);
						} else {
							this.feedbackMessageComponent.updateMessage(true, "You cannot be your own distributor", "alert-danger");
						}
						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-success");
						break;
					case responseCodes.RESP_AUTH_FAIL:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_FAIL:
						this.onUserNotFound();

						// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
					case responseCodes.RESP_ALREADY_EXIST:
						this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
						break;
				}
			},
			err => {
				this.showLoader = false;
				this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
			}
			);
	}

	/*
	METHOD         : getProgramScheme()
	DESCRIPTION    : get All scheme setup of the program
*/
	// getProgramScheme(reqObj) {
	// 	this.showLoader = true;

	// 	this.channelService.getProgramScheme(reqObj)
	// 		.subscribe(
	// 		(responseObject) => {
	// 			this.showLoader = false;

	// 			let responseCodes = this.configService.getStatusTokens();
	// 			switch (responseObject.statusCode) {
	// 				case responseCodes.RESP_ROLLBACK_ERROR:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_SERVER_ERROR:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_SUCCESS:

	// 					this.productsArray = responseObject.result;
	// 					this.productDetailsArray.splice(0, this.productDetailsArray.length);
	// 					break;
	// 				case responseCodes.RESP_AUTH_FAIL:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_FAIL:
	// 					this.onUserNotFound();

	// 					// this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 				case responseCodes.RESP_ALREADY_EXIST:
	// 					this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger");
	// 					break;
	// 			}
	// 		},
	// 		err => {
	// 			this.showLoader = false;
	// 			this.feedbackMessageComponent.updateMessage(true, err, "alert-danger");
	// 		}
	// 		);
	// }
	/************************************ 	END OF API CALLS 	************************************************/
}


