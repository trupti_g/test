import { ClientwebappPage } from './app.po';

describe('clientwebapp App', function() {
  let page: ClientwebappPage;

  beforeEach(() => {
    page = new ClientwebappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
